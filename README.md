# Set up

## Requisites:
- node v12.20+
- yarn

### Install yarn using npm
```
npm install --global yarn
npm upgrade --global yarn 
```

### Install yarn using brew
```
brew install yarn
brew upgrade yarn
```

### IF Errors w/ certificate

```
yarn config set "strict-ssl" false
NODE_TLS_REJECT_UNAUTHORIZED=0 yarn
```


## Build Order Ahead:

```
yarn
yarn build-oa
```