export const LABELS = {
    FOOTER_SUBTITLE: 'NOT A MEMBER?',
    FOOTER_TITLE: 'JOIN OUR BLAZIN’ REWARDS!',
};

export const SOCIAL_SIGNIN = {
    FACEBOOK: false,
    APPLE: false,
};

export const genericSignInErrorMessage = `We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly.`;
