const InfoTextMap = {
    signUp: {
        TITLE: 'Earn points and get',
        MAIN_TEXT: 'Blazin’ rewards',
        DESCRIPTION: '',
        BACKGROUND_COLOR: 'inherit',
    },
    signIn: {
        TITLE: 'Earn points and get',
        MAIN_TEXT: 'Blazin’ rewards',
        DESCRIPTION: '',
        BACKGROUND_COLOR: 'inherit',
    },
    forgotPassword: {
        TITLE: 'Earn points and get',
        MAIN_TEXT: 'Blazin’ rewards',
        DESCRIPTION: '',
        BACKGROUND_COLOR: 'inherit',
    },
};

export default InfoTextMap;
