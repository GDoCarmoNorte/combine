import React, { useContext, useState } from 'react';
import { Field, Form, Formik, FormikConfig } from 'formik';
import classNames from 'classnames';
import CheckIcon from '@material-ui/icons/Check';
import { StoreContext } from '../../../../auth0/src/store/index';
import { InspireButton } from '../../../../auth0/src/components/button';
import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';
import FormikInput from '../../../../components/organisms/formikInput';
import FormikPasswordInput from '../../../../components/organisms/formikPasswordInput';
import {
    unmaskPhoneField,
    validateFirstNameField,
    validateLastNameField,
    validateEmailField,
    validatePhoneField,
    validateTermsField,
    validatePasswordField,
    validatePasswordRequired,
} from '../../../../auth0/src/components/signupForm/validation';
import getBrandInfo from '../../../../lib/brandInfo';
import { isSignUpPhoneTooltip } from '../../../../lib/getFeatureFlags';
import { autoCorrectedDatePipe } from '../../../../common/helpers/autoCorrectedDatePipe';
import { validateDateOfBirth, validateZipCode } from '../../../../common/helpers/complexValidateHelper';
import BrandLoader from '../../../../components/atoms/BrandLoader/brandLoader';
import styles from './index.module.css';

export const validateZipCodeField = validateZipCode('Zip Code');
const validateDOBField = validateDateOfBirth('Birthdate');

const SignUpForm = (props: FormikConfig<any> | any): JSX.Element => {
    const APP_URL = process.env.NEXT_PUBLIC_APP_URL;
    const { handleSignUpEmail, loading } = props;

    const { brandName } = getBrandInfo();
    const {
        state: { email },
    } = useContext(StoreContext);

    const [showPasswordHint, setShowPasswordHint] = useState(true);

    const initialFormValues = {
        firstName: '',
        lastName: '',
        email,
        phone: '',
        birthDate: '',
        zipCode: '',
        password: '',
        termsAgreed: false,
        marketingOptIn: true,
    };

    const passwordValidationCoordinator = (value) => {
        setShowPasswordHint(true);

        let error = validatePasswordRequired(value);
        if (error) {
            return error;
        }

        error = validatePasswordField(value);
        if (error) {
            setShowPasswordHint(false);
        }
        return error;
    };

    return (
        <Formik initialValues={initialFormValues} {...props}>
            {({ setFieldValue, isValid }) => (
                <Form>
                    <FormikInput
                        label="First Name"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="firstName"
                        maxLength={256}
                        placeholder="Enter first name"
                        required
                        validate={validateFirstNameField}
                    />
                    <FormikInput
                        label="Last Name"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="lastName"
                        maxLength={256}
                        placeholder="Enter last name"
                        required
                        validate={validateLastNameField}
                    />
                    <FormikInput
                        label="Email"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="email"
                        onChange={(e) => {
                            setFieldValue('email', e.target.value);
                            handleSignUpEmail(e.target.value);
                        }}
                        placeholder="Enter email address"
                        required
                        validate={validateEmailField}
                    />
                    <FormikInput
                        label="Phone Number"
                        type="tel"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="phone"
                        placeholder="Enter phone number"
                        required
                        validate={validatePhoneField}
                        onChange={(e) => {
                            setFieldValue('phone', unmaskPhoneField(e.target.value));
                        }}
                        mask={['(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                        withTooltip={isSignUpPhoneTooltip()}
                        tooltipTitle="Your mobile phone will be used as your member ID"
                        tooltipClassName={styles.tooltip}
                    />
                    <FormikInput
                        label="Date Of Birth"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="birthDate"
                        placeholder="MM / DD / YYYY"
                        required
                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                        validate={validateDOBField}
                        pipe={autoCorrectedDatePipe}
                        withTooltip
                        tooltipTitle="We need to verify you're eligible to receive member benefits."
                        tooltipClassName={styles.tooltip}
                    />
                    <FormikInput
                        label="ZIP Code"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="zipCode"
                        placeholder="Enter ZIP code"
                        required
                        validate={validateZipCodeField}
                    />
                    <FormikPasswordInput
                        label="Password"
                        labelClassName={styles.label}
                        inputClassName={styles.input}
                        name="password"
                        placeholder="Enter password"
                        description={
                            showPasswordHint
                                ? '(Min. 8 characters and must contain: one upper case letter, one lower case letter, a number and a special character.)'
                                : ''
                        }
                        required
                        validate={passwordValidationCoordinator}
                        showErrorImmediately={true}
                        showHideButton={true}
                    />
                    <Field name="marketingOptIn">
                        {({ field: { value } }) => (
                            <div className={styles.termsContainer}>
                                <input
                                    className={styles.checkbox}
                                    type="checkbox"
                                    id="emailShare"
                                    checked={value}
                                    onChange={(e) => setFieldValue('marketingOptIn', e.target.checked)}
                                />
                                <div
                                    tabIndex={0}
                                    className={styles.styledCheckbox}
                                    onClick={() => setFieldValue('marketingOptIn', !value)}
                                    onKeyPress={({ key }) => key === 'Enter' && setFieldValue('marketingOptIn', !value)}
                                >
                                    <CheckIcon className={styles.checkIcon} />
                                </div>
                                <label
                                    htmlFor="emailShare"
                                    className={classNames(styles.label, styles.termsLabel, 't-paragraph-hint')}
                                >
                                    Yes! I want to receive offers and marketing messages from {brandName}. Please see
                                    our&nbsp;
                                    <InspireSimpleLink
                                        type="secondary"
                                        link={`${APP_URL}/privacy-policy`}
                                        newtab
                                        className={styles.termsLink}
                                    >
                                        privacy policy
                                    </InspireSimpleLink>
                                    &nbsp;for more information.
                                </label>
                            </div>
                        )}
                    </Field>
                    <Field name="termsAgreed" validate={validateTermsField}>
                        {({ field: { value } }) => (
                            <div className={styles.termsContainer}>
                                <input
                                    className={styles.checkbox}
                                    type="checkbox"
                                    id="terms"
                                    checked={value}
                                    onChange={(e) => setFieldValue('termsAgreed', e.target.checked)}
                                />
                                <div
                                    tabIndex={0}
                                    className={styles.styledCheckbox}
                                    onClick={() => setFieldValue('termsAgreed', !value)}
                                    onKeyPress={({ key }) => key === 'Enter' && setFieldValue('termsAgreed', !value)}
                                >
                                    <CheckIcon className={styles.checkIcon} />
                                </div>
                                <label
                                    htmlFor="terms"
                                    className={classNames(styles.label, styles.termsLabel, 't-paragraph-hint')}
                                >
                                    I have read and agree to the&nbsp;
                                    <InspireSimpleLink
                                        type="secondary"
                                        link={`${APP_URL}/terms-of-use`}
                                        newtab
                                        className={styles.termsLink}
                                    >
                                        terms and conditions
                                    </InspireSimpleLink>
                                    .
                                </label>
                            </div>
                        )}
                    </Field>
                    <InspireButton className={styles.submitButton} submit disabled={!isValid || loading} tabIndex={0}>
                        {loading ? <BrandLoader className={styles.loader} /> : 'CREATE AN ACCOUNT'}
                    </InspireButton>
                </Form>
            )}
        </Formik>
    );
};

export default SignUpForm;
