import { IPaymentRequest } from '../../../../components/clientOnly/paymentInfoContainer/paymentInfo';

export const paymentInfoRequest: IPaymentRequest = {
    sessionKey: '{{sessionKey}}',
    paymentKeys: ['1'],
    cardIssuer: 'Visa',
    maskedCardNumber: '411111XXXXXX1111',
    chFirstName: 'Franz',
    chLastName: 'Kafka',
};
