import React from 'react';
import styles from './index.module.css';
import { IPaymentRequest } from '../../../../components/clientOnly/paymentInfoContainer/paymentInfo';
import { InspireButton } from '../../../../components/atoms/button';
import { paymentInfoRequest } from './mock';

interface IPaymentInfoProps {
    isActive?: boolean;
    onSubmit?: (request: IPaymentRequest) => void;
    onError?: (error: { message: string }) => void;
    onFormStatusChange?: (isValid: boolean) => void;
    submitError?: Error;
}

const PaymentInfo = (props: IPaymentInfoProps): JSX.Element => {
    const { onSubmit } = props;

    const handleOrderRequest = (): void => {
        onSubmit(paymentInfoRequest);
    };

    return (
        <div className={styles.PaymentInfoContainer}>
            <InspireButton text="Order Now" onClick={handleOrderRequest} />
        </div>
    );
};

export default PaymentInfo;
