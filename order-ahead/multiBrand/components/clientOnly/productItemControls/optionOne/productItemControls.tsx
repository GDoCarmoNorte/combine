import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import classnames from 'classnames';
import Button from '@material-ui/core/Button';

import { IProduct } from '../../../../../@generated/@types/contentful';
import { ItemModel } from '../../../../../@generated/webExpApi';

import { Divider } from '../../../../../components/atoms/divider';
import { useDomainMenu, usePdp } from '../../../../../redux/hooks';
import Loader from '../../../../../components/atoms/Loader';
import { GTM_MODIFY_PRODUCT } from '../../../../../common/services/gtmService/constants';
import { InspireLink } from '../../../../../components/atoms/link';
import { InspireButton, InspireButtonType } from '../../../../../components/atoms/button';

import styles from '../../../../../components/clientOnly/productItemControls/productItemControls.module.css';
import { useProductHasRequiredModifiers } from '../../../../../common/hooks/useProductHasRequiredModifiers';

interface IProductItemPriceProps {
    isOrderAheadAvailable: boolean;
    product: IProduct;
    productDetails: ItemModel;
    isSaleable: boolean;
    isAvailable: boolean;
    isCombo?: boolean;
    onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    gtmId?: string;
    viewType?: 'link' | 'button';
    buttonType?: InspireButtonType;
    buttonClassName?: string;
    onModify?: () => void;
    addClass?: string;
    modifyClass?: string;
}

const ProductItemControls = (props: IProductItemPriceProps): JSX.Element => {
    const {
        onClick,
        isOrderAheadAvailable,
        product,
        productDetails,
        isSaleable,
        isAvailable,
        isCombo,
        gtmId,
        viewType = 'link',
        buttonType = 'primary',
        buttonClassName,
        onModify,
        addClass,
        modifyClass,
    } = props;

    const { loading } = useDomainMenu();
    const dispatch = useDispatch();

    const labels = {
        VIEW_ITEM: 'VIEW ITEM',
        ADD_TO_BAG: 'ADD TO BAG',
        MODIFY: 'MODIFY',
    };
    const pdp = usePdp();

    const handleModifyLinkClick = useCallback(() => {
        dispatch({ type: GTM_MODIFY_PRODUCT });
        pdp.actions.resetPdpState();
        if (onModify) {
            onModify();
        }
    }, [dispatch, onModify, pdp.actions]);

    const productHasRequiredModifier = useProductHasRequiredModifiers(productDetails?.id);

    if (loading) {
        return (
            <span>
                <Loader size={20} />
            </span>
        );
    }

    const productHasModifiers = productDetails?.itemModifierGroups?.length;
    const selectedModifiers =
        productHasModifiers && productDetails.itemModifierGroups.filter((x) => x.itemModifiers?.length);

    const link = (
        <InspireLink
            link={product}
            className={classnames(buttonClassName, styles.modifyButton)}
            gtmId={gtmId}
            type="secondary"
        >
            {labels.VIEW_ITEM}
        </InspireLink>
    );

    const customButton = (
        <Button
            data-gtm-id={gtmId}
            onClick={onClick}
            classes={{ root: classnames('link-secondary-active', buttonClassName, styles.orderButton, addClass) }}
            variant="text"
            disableRipple
        >
            <span data-gtm-id={gtmId}>{labels.ADD_TO_BAG}</span>
        </Button>
    );

    const linkButton = (
        <InspireButton
            link={product}
            text={labels.VIEW_ITEM}
            type={buttonType}
            gtmId={gtmId}
            className={buttonClassName}
        />
    );
    const button = (
        <InspireButton
            onClick={onClick}
            text={labels.ADD_TO_BAG}
            type={buttonType}
            gtmId={gtmId}
            className={classnames(addClass, buttonClassName)}
        />
    );

    const modifyButton = (
        <InspireLink
            type="secondary"
            link={product}
            className={classnames(modifyClass, styles.modifyButton, buttonClassName)}
            onClick={handleModifyLinkClick}
            gtmId={gtmId}
        >
            {labels.MODIFY}
        </InspireLink>
    );

    if (!isSaleable || !isAvailable || !isOrderAheadAvailable) {
        return viewType === 'link' ? link : linkButton;
    }

    if (!productHasModifiers && !isCombo) {
        return viewType === 'link' ? customButton : button;
    }

    if (!selectedModifiers?.length && productHasRequiredModifier) {
        return modifyButton;
    }

    return viewType === 'link' ? (
        <>
            {customButton}
            <Divider />
            {modifyButton}
        </>
    ) : (
        button
    );
};

export default ProductItemControls;
