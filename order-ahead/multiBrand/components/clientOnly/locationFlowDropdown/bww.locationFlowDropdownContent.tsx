import React from 'react';
import { InspireButton } from '../../../../components/atoms/button';
import { ILocationFlowDropdownContentProps } from '../../../../components/clientOnly/locationFlowDropdown/types';

import styles from '../../../../components/clientOnly/locationFlowDropdown/locationFlowDropdown.module.css';
import bwwStyles from './bww.locationFlowDropdown.module.css';
import LocationSelect from '../../../../components/organisms/navigation/locationSelect';
import { useRouter } from 'next/router';
import { useMediaQuery } from '@material-ui/core';
import classNames from 'classnames';
import useStartAnOrderLink from '../../../../common/hooks/useStartAnOrderLink';

const LocationFlowDropdownContent = (props: ILocationFlowDropdownContentProps): JSX.Element => {
    const { navRef, isLocationSelected, isOAEnabled } = props;
    const { query } = useRouter();
    const isPdp = 'productDetailsPageUrl' in query;
    const isMobile = useMediaQuery('(max-width: 960px)');
    const startAnOrderLink = useStartAnOrderLink();

    return (
        <div className={bwwStyles.wrapper}>
            {isLocationSelected && isOAEnabled && <LocationSelect navRef={navRef} />}
            {!(isPdp && isMobile) && (
                <InspireButton
                    className={classNames(styles.startOrderButton, bwwStyles.startOrderButton, {
                        [bwwStyles.locationIsSelected]: isLocationSelected && isOAEnabled,
                    })}
                    type="small"
                    text="start an order"
                    disabled={!isOAEnabled}
                    link={startAnOrderLink}
                />
            )}
        </div>
    );
};

export default LocationFlowDropdownContent;
