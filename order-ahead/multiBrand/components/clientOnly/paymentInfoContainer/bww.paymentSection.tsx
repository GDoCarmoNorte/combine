import React, { FC } from 'react';
import { FulfillmentTypeEnumModel } from '../../../../@generated/webExpApi';
import { usePayment } from '../../../../common/hooks/usePayment';
import PaymentInfo from '../../../../components/clientOnly/paymentInfoContainer/paymentInfo';
import { TInitialPaymentTypes } from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { IPaymentAndGiftCardInfoContainerProps } from '../../../../components/clientOnly/paymentInfoContainer/types';

export interface IPaymentSectionProps extends IPaymentAndGiftCardInfoContainerProps {
    paymentType: TInitialPaymentTypes;
    fulfillmentType?: FulfillmentTypeEnumModel;
    locationId?: string;
}

const PaymentSection: FC<IPaymentSectionProps> = ({ paymentType, locationId, fulfillmentType, ...props }) => {
    const paymentPayload = usePayment(paymentType, locationId, fulfillmentType);

    if (paymentType === TInitialPaymentTypes.CREDIT_OR_DEBIT) {
        const innnerHtml = paymentPayload.payment && { __html: paymentPayload.payment.iframeHtml };

        return (
            <PaymentInfo {...props} paymentType={paymentType} paymentPayload={paymentPayload} innnerHtml={innnerHtml} />
        );
    }

    return null;
};

export default PaymentSection;
