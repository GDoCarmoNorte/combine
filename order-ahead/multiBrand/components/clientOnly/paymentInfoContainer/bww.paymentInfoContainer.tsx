import React, { FC, useEffect, useState } from 'react';
import classNames from 'classnames';
import { isGiftCardPayEnabled } from '../../../../lib/getFeatureFlags';

import { InspireButton } from '../../../../components/atoms/button';
import GiftCardSection from '../../../../components/clientOnly/paymentInfoContainer/giftCardSection/giftCardSection';
import PayInStoreInfo from '../../../../components/clientOnly/paymentInfoContainer/payInStoreInfo';
import PaymentSection from '../../../../components/clientOnly/paymentInfoContainer/paymentSection';
import PaymentTypeSelectBox from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/paymentTypeDropdown';
import SavedCard from '../../../../components/clientOnly/paymentInfoContainer/savedCard/savedCard';
import GooglePaySection from '../../../../components/clientOnly/paymentInfoContainer/googlePaySection/googlePaySection';
import ApplePaySection from '../../../../components/clientOnly/paymentInfoContainer/applePaySection/applePaySection';

import { ButtonTypeEnumModel } from '../../../../@generated/webExpApi';
import {
    IPaymentState,
    TInitialPaymentTypes,
    TPaymentMethodTypes,
} from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import {
    IGiftCard,
    IPaymentAndGiftCardInfoContainerProps,
} from '../../../../components/clientOnly/paymentInfoContainer/types';
import {
    ALL_INITIAL_PAYMENT_TYPES,
    CARD_ON_FILE,
} from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/constants';

import styles from '../../../../components/clientOnly/paymentInfoContainer/index.module.css';
import { useAppDispatch } from '../../../../redux/store';
import { GTM_CHECKOUT_PAYMENT_METHOD_TYPE_EVENT } from '../../../../common/services/gtmService/constants';
import Loader from '../../../../components/atoms/Loader';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

export interface IPaymentGCRequest {
    sessionKey: string;
    paymentKeys: string[];
    cardIssuer?: string;
    maskedCardNumber?: string;
}

export interface IPaymentRequest extends IPaymentGCRequest {
    chFirstName: string;
    chLastName: string;
}

interface IPaymentInfoContainerProps extends IPaymentAndGiftCardInfoContainerProps {
    paymentInfo: IPaymentState;
    setPaymentInfo: (state: IPaymentState) => void;
    isUnavailableItems: boolean;
}

const PaymentInfoContainer: FC<IPaymentInfoContainerProps> = ({
    totalPrice,
    isActive,
    onSubmit,
    paymentInfo,
    setPaymentInfo,
    submitError,
    isUnavailableItems,
    ...props
}) => {
    const {
        configuration: { isApplePayEnabled, isGooglePayEnabled },
    } = useConfiguration();
    const [giftCards, setGiftCards] = useState<IGiftCard[]>([]);
    const [googlePayAppeared, setGooglePayAppeared] = useState<boolean>(false);
    const [googlePaymentInProgress, setGooglePaymentInProgress] = useState<boolean>(false);
    const [applePaymentInProgress, setApplePaymentInProgress] = useState<boolean>(false);
    const [applePayCompatible, setApplePayCompatibility] = useState<boolean>(false);

    useEffect(() => {
        if (submitError || isUnavailableItems) {
            setGooglePaymentInProgress(false);
            setApplePaymentInProgress(false);
        }
    }, [submitError, isUnavailableItems]);

    const totalGiftCardBalance = giftCards.reduce((total: number, curr: IGiftCard) => total + (curr.balance || 0), 0);
    const totalToPay =
        totalGiftCardBalance >= totalPrice ? 0 : parseFloat((totalPrice - totalGiftCardBalance).toFixed(2));

    useEffect(() => {
        if (totalToPay === 0) {
            setPaymentInfo(ALL_INITIAL_PAYMENT_TYPES[TInitialPaymentTypes.PLACEHOLDER]);
        }
    }, [totalToPay, setPaymentInfo]);

    const handleSubmitGiftCardPay = () => {
        paymentMethodClicked(TInitialPaymentTypes.GIFT_CARD);
        return onSubmit(undefined, giftCards);
    };

    const handleSubmitPay = (request: IPaymentRequest) => {
        paymentMethodClicked(TInitialPaymentTypes.CREDIT_OR_DEBIT);
        return onSubmit(request, giftCards);
    };

    const handleInStorePayment = (request: IPaymentRequest) => {
        paymentMethodClicked(TInitialPaymentTypes.PAY_IN_STORE);
        return onSubmit(request);
    };

    const handleSubmitGooglePay = (request: IPaymentRequest) => {
        paymentMethodClicked(TInitialPaymentTypes.GOOGLE_PAY);
        setGooglePaymentInProgress(true);
        return onSubmit(request, giftCards);
    };

    const handleSubmitApplePay = (request: IPaymentRequest) => {
        setApplePaymentInProgress(true);
        paymentMethodClicked(TInitialPaymentTypes.APPLE_PAY);
        return onSubmit(request, giftCards);
    };

    const handleSubmitCardOnFilePay = (request: IPaymentRequest) => {
        paymentMethodClicked(CARD_ON_FILE);
        return onSubmit(request, giftCards);
    };

    const dispatch = useAppDispatch();

    const paymentMethodClicked = (paymentMethod: TPaymentMethodTypes) => {
        dispatch({ type: GTM_CHECKOUT_PAYMENT_METHOD_TYPE_EVENT, payload: paymentMethod });
    };

    return (
        <>
            {paymentInfo.type !== TInitialPaymentTypes.PAY_IN_STORE && (
                <>
                    {isGiftCardPayEnabled() && (
                        <GiftCardSection
                            {...props}
                            totalToPay={totalToPay}
                            setGiftCards={setGiftCards}
                            giftCards={giftCards}
                        />
                    )}
                    {!!totalToPay && (
                        <>
                            <div
                                className={classNames(styles.googleAndApplePaymentContainer, {
                                    [styles.googleAndApplePaymentActiveContainer]:
                                        applePayCompatible || googlePayAppeared,
                                })}
                            >
                                {isGooglePayEnabled && !(googlePaymentInProgress || applePaymentInProgress) && (
                                    <GooglePaySection
                                        {...props}
                                        className={classNames({
                                            [styles.paymentOverlay]: !isActive,
                                            [styles.googlePayOverlay]: !isActive,
                                        })}
                                        totalAmount={totalToPay}
                                        onSubmit={handleSubmitGooglePay}
                                        onPaymentMethodSupportChange={(isSupported) =>
                                            setGooglePayAppeared(isSupported)
                                        }
                                    />
                                )}
                                {isApplePayEnabled && !(googlePaymentInProgress || applePaymentInProgress) && (
                                    <ApplePaySection
                                        onSubmit={handleSubmitApplePay}
                                        onError={props.onError}
                                        className={classNames({
                                            [styles.paymentOverlay]: !isActive,
                                            [styles.applePayOverlay]: !isActive,
                                        })}
                                        totalAmount={totalToPay}
                                        setCompatibility={setApplePayCompatibility}
                                    />
                                )}
                                {(googlePaymentInProgress || applePaymentInProgress) && (
                                    <div className={styles.paymentLoader}>
                                        <Loader size={20} />
                                    </div>
                                )}
                            </div>
                            {(applePayCompatible || googlePayAppeared) && (
                                <div className={styles.buttonsDivider}>
                                    <span className={styles.dividerLine} />
                                    <p className={classNames(styles.orWord, 't-paragraph-small')}>or</p>
                                    <span className={styles.dividerLine} />
                                </div>
                            )}
                        </>
                    )}
                </>
            )}
            {!!totalToPay && <PaymentTypeSelectBox onChange={setPaymentInfo} selectedMethod={paymentInfo} />}
            {paymentInfo.type === TInitialPaymentTypes.PLACEHOLDER && (
                <InspireButton
                    className={styles.buttonPay}
                    onClick={handleSubmitGiftCardPay}
                    text={ButtonTypeEnumModel.Pay}
                    fullWidth
                    disabled={!isActive || !!totalToPay}
                />
            )}
            {paymentInfo.type === TInitialPaymentTypes.CREDIT_OR_DEBIT && (
                <PaymentSection
                    {...props}
                    paymentType={paymentInfo.type}
                    onSubmit={handleSubmitPay}
                    isActive={isActive}
                />
            )}
            {paymentInfo.type === TInitialPaymentTypes.PAY_IN_STORE && (
                <PayInStoreInfo onSubmit={handleInStorePayment} isActive={isActive} />
            )}
            {paymentInfo.type === CARD_ON_FILE && (
                <SavedCard token={paymentInfo.token} onSubmit={handleSubmitCardOnFilePay} isActive={isActive} />
            )}
        </>
    );
};

export default PaymentInfoContainer;
