export const TITLE_TEXT = 'Delete My Account';
export const PRE_LINK_TEXT = 'Something went wrong with your request. Please try again, or contact ';
export const LINK_TEXT = 'customer service.';
export const LINK = '../contact-us';
export const RETRY_BUTTON_TEXT = 'TRY AGAIN';
