export const SHOULD_SHOW_TWO_BUTTONS = true;
export const SHOULD_SHOW_INFO_BLOCK = true;

export const TYPOGRAPHY_CLASS = {
    HEADER: 't-header-h1',
    CONTENT_HEADER: 't-header-h3',
    CONTENT_DESCRIPTION: 't-paragraph',
    PHONE_NUMBER: 't-subheader-small',
};
