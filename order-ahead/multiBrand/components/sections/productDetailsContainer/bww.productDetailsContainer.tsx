import React from 'react';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import classNames from 'classnames';

import AboutSection from '../../../../components/sections/aboutSection/aboutSection';
import usePdp from '../../../../redux/hooks/usePdp';
import { useLocalization } from '../../../../common/hooks/useLocalization';

import {
    useDomainProductByContentfulFields,
    useNutrition,
    useProductSizes,
    useSelectedModifiers,
} from '../../../../redux/hooks/domainMenu';

import Footer from '../../../../components/organisms/footer';
import BaseHeader from '../../../../components/organisms/header/baseHeader';
import Breadcrumbs from '../../../../components/atoms/Breadcrumbs';
import getBrandInfo from '../../../../lib/brandInfo';
import { FeatureFlagsContext } from '../../../../redux/hooks/useFeatureFlags';
import { useBwwDisplayProduct } from '../../../../redux/hooks/pdp';
import { useProductReadyToAddToBag } from '../../../../common/hooks/useProductReadyToAddToBag';
import { useBag, useConfiguration } from '../../../../redux/hooks';
import { getPdpPageTitle } from '../../../../common/helpers/getPageTitle';
import { getPDPPageDescription } from '../../../../common/helpers/getPageDescription';
import { ModifierGroupType, ProductTypesEnum } from '../../../../redux/types';
import { PageContentWrapper } from '../../../../components/sections/PageContentWrapper';
import { InspireButton } from '../../../../components/atoms/button';
import FloatingBanner from '../../../../components/organisms/floatingBanner';
import { useProductIsSaleable } from '../../../../common/hooks/useProductIsSaleable';
import { NOT_SALEABLE_BUTTON_TEXT, NOT_SALEABLE_BUTTON_LINK } from '../../../../common/constants/product';
import { useProductIsAvailable } from '../../../../common/hooks/useProductIsAvailable';
import schemaPdpAndPlp from '../../../../common/helpers/schemaPdpAndPlp';
import styles from './bww.productDetailsContainer.module.css';
import { IProductDetailsContainerProps } from '../../../../components/sections/productDetailsContainer/types';

const ProductBanner = dynamic(import('../../organisms/productBanner/bww.productBanner'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default function ProductDetailsContainer(props: IProductDetailsContainerProps): JSX.Element {
    const brandInfo = getBrandInfo();
    const {
        product: defaultProduct,
        currentCategory,
        productNutritionLink,
        canonicalPath,
        featureFlags,
        isPreviewMode,
        caloriesLegal,
    } = props;
    const { productsById, modifierItemsById } = props.globalProps;

    const pdp = usePdp();
    const pdpTallyItem = pdp.pdpTallyItem;
    const { productId } = pdpTallyItem;

    const product = productsById[productId] || defaultProduct; // defaultProduct needs in case Product for some size is not specified in contentul
    const domainProduct = useDomainProductByContentfulFields(defaultProduct.fields);

    pdp.useInitTallyItem(domainProduct?.id);

    const { locationLinkText } = useLocalization(productId);
    const { isSaleable } = useProductIsSaleable(productId);
    const { isAvailable } = useProductIsAvailable(productId);
    const displayProduct = useBwwDisplayProduct();

    const productIdForChildSize = pdpTallyItem?.childItems?.length && pdpTallyItem.childItems[0].productId; // TODO: Improve this
    const sizeSelections = useProductSizes(productId);

    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    const productInfo = {
        price: !locationLinkText && displayProduct.totalPrice ? displayProduct.totalPrice : null,
        calories: displayProduct?.calories,
        name: displayProduct?.displayName,
    };

    const isPromo = displayProduct?.productType === ProductTypesEnum.Promo;

    const { alertBanners, navigation, footer, userAccountMenu } = props.globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const isProductReadyToAddToBag = useProductReadyToAddToBag();

    const bag = useBag();

    const { categoryName } = currentCategory?.fields || { categoryName: '' };
    const { name } = product.fields;

    const isUpdating = !!pdpTallyItem && !!pdpTallyItem.lineItemId;

    const addToBagBtnTxt = isUpdating
        ? pdpTallyItem.quantity > 1
            ? `Update (${pdpTallyItem.quantity} items)`
            : 'Update'
        : 'Add to bag';

    const selectedModifiers = useSelectedModifiers(pdpTallyItem);

    const sauce = selectedModifiers.reduce((acc, modifier) => {
        if (modifier.metadata?.MODIFIER_GROUP_TYPE !== ModifierGroupType.SAUCES) return acc;
        const numberSauces = Object.keys(acc).length;
        return {
            ...acc,
            [`sauce${numberSauces + 1}`]: modifier.name || 'none',
        };
    }, {});
    const handleAddToBag = () => {
        bag.actions.putToBag({ pdpTallyItem, category: categoryName, name, sauce });
        // After item is added to bag, return PDP to default state
        pdp.actions.resetPdpState();
    };

    const nutrition = useNutrition(productId);
    const dataSchema = schemaPdpAndPlp(currentCategory, brandInfo, product, nutrition);

    const renderButtons = () => {
        if (!isSaleable || !isAvailable || !isOAEnabled) {
            return (
                <InspireButton
                    disabled={!isOAEnabled}
                    link={NOT_SALEABLE_BUTTON_LINK}
                    type="primary"
                    text={NOT_SALEABLE_BUTTON_TEXT}
                />
            );
        }

        if (locationLinkText) {
            return <InspireButton link={'/locations'} type="primary" text={locationLinkText} />;
        }

        return (
            <InspireButton
                className={styles.addToBagButton}
                onClick={handleAddToBag}
                type="primary"
                text={addToBagBtnTxt}
                disabled={!isProductReadyToAddToBag}
            />
        );
    };

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                isPreviewMode={isPreviewMode}
                userAccountMenu={userAccountMenu}
            />
            <div className={classNames('container', styles.container)}>
                <Head>
                    <title>{getPdpPageTitle(brandInfo.brandName, product.fields.name)}</title>
                    <link rel="canonical" href={canonicalPath} />
                    <meta name="description" content={getPDPPageDescription(product)} />
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(dataSchema) }}
                    />
                </Head>

                <div className={styles.breadcrumbs}>
                    <Breadcrumbs />
                </div>

                <PageContentWrapper>
                    <div className={styles.detailsWrapper}>
                        <ProductBanner
                            product={product}
                            displayName={displayProduct.displayName}
                            productInfo={productInfo}
                            selections={sizeSelections}
                            onSizeSelect={(productId) => {
                                pdp.actions.editTallyItemSize(productId);
                            }}
                            productType={displayProduct?.productType}
                            childProductId={productIdForChildSize}
                            productSections={displayProduct.productSections}
                            modifierItemsById={modifierItemsById}
                            caloriesLegal={caloriesLegal}
                        />
                        <div>
                            <AboutSection
                                category={currentCategory}
                                isPromo={isPromo}
                                productNutritionLink={productNutritionLink}
                            />
                        </div>
                    </div>
                </PageContentWrapper>
                <FloatingBanner
                    offset={0}
                    product={product}
                    onAddToBag={handleAddToBag}
                    productInfo={productInfo}
                    domainProduct={domainProduct}
                    addToBagBtnText={addToBagBtnTxt}
                    addToBagBtnDisabled={!isProductReadyToAddToBag}
                    showInitially
                />
                <div className={styles.buttonsBlock}>{renderButtons()}</div>
                {footer && <Footer footer={footer} />}
            </div>
        </FeatureFlagsContext.Provider>
    );
}
