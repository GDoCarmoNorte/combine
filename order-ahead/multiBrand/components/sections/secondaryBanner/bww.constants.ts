export const TYPOGRAPHY_CLASS = {
    MAIN_TEXT: 't-subheader-universal',
    TITLE: 't-header-card-title',
    DESCRIPTION: 't-paragraph',
};
