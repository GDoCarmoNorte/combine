export const TYPOGRAPHY_CLASS = {
    TOP_TEXT: 't-subheader',
    MAIN_TEXT: 't-header-hero',
    MAIN_TEXT_H2_SIZE: 't-header-h1',
};

export const IMAGES_CLASS = {
    maxWidth: 793,
    objectFit: 'cover',
};
