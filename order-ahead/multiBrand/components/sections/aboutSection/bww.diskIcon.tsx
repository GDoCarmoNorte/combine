import classNames from 'classnames';
import React from 'react';
import styles from './bww.diskIcon.module.css';

const DiskIcon: React.FC = () => {
    return <span className={classNames(styles.diskIcon, 't-subheader-universal-smaller')}>•</span>;
};

export default DiskIcon;
