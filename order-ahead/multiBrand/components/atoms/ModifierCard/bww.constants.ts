export const TYPOGRAPHY_CLASS = {
    NAME: 't-subheader-smaller',
    MODIFIERS_TEXT: 't-paragraph-hint',
};

export const CHECKBOX_SIZE = 'standard';
