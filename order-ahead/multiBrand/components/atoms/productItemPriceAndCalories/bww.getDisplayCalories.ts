const getDisplayCalories = (calories?: number): string =>
    Number.isFinite(calories) ? `${calories} cal` : 'Calorie info unavailable';

export default getDisplayCalories;
