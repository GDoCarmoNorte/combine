import React, { FC } from 'react';
import styles from './jj.divider.module.css';

const Divider: FC = () => {
    return <div className={styles.divider} />;
};

export default Divider;
