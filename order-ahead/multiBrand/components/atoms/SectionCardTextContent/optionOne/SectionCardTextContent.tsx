import React from 'react';
import styles from './SectionCardTextContent.module.css';
import { ISectionCardTextContentProps } from '../../../../../components/atoms/SectionCardTextContent/types';
import classnames from 'classnames';

const SectionCardTextContent = ({ gtmId, description }: ISectionCardTextContentProps): JSX.Element => {
    return (
        <>
            <span data-gtm-id={gtmId} className={classnames(styles.description, 'truncate-at-2')}>
                {description}
            </span>
        </>
    );
};

export default SectionCardTextContent;
