import React, { FC, RefObject } from 'react';
import classNames from 'classnames';

import styles from './bww.brandLoader.module.css';

interface ILoaderProps {
    className?: string;
}

const BrandLoader: FC<ILoaderProps> = React.forwardRef(function BrandLoader(
    { className },
    ref: RefObject<HTMLDivElement>
) {
    return (
        <div role="progressbar" className={classNames(styles.container, className)} ref={ref}>
            <svg className={styles.loadersvg} x="0px" y="0px" viewBox="0 0 226.1 226.1">
                <g id="base">
                    <circle className={styles.st0} cx="113.1" cy="113.1" r="102.1" />
                    <path
                        className={classNames(styles.st1, styles.stroke)}
                        d="M94.9,1.5c61.6-10.1,119.7,31.6,129.8,93.2c3.5,21.2,0.8,43-7.6,62.8c-25.4,57.2-92.4,82.9-149.6,57.5
		C19.6,193.6-7.4,142.2,2.2,90.7C11.5,44.6,48.4,9,94.9,1.5 M96.9,12.5c-42.8,7-76.6,40.3-84.2,83c-8.8,50.1,20.7,99,69.1,114.5
		c49.6,15.7,103.1-8.2,124.4-55.6c21.6-49.2,1.5-106.6-46-131.7C140.7,12.6,118.5,9,96.9,12.5z"
                    />
                    <path
                        className={classNames(styles.st2, styles.section)}
                        d="M12.7,95.5c7.6-42.7,41.4-76,84.2-83c7.2-1.2,14.5-1.5,21.7-1.1V0.1c-7.8-0.4-15.7,0.1-23.7,1.4
		C48.4,9,11.5,44.6,2.2,90.7c-1.9,10.1-2.3,20.3-1.5,30.2h10.8C10.8,112.6,11.2,104.1,12.7,95.5z"
                    />
                </g>
                <g className={styles.buffalowing}>
                    <g className={styles.buffalo}>
                        <path
                            className={styles.st3}
                            d="M92.3,68.4c9.7-3.6,20.6-2.1,29.1,3.8c7.9,5,14.5,11.9,22.8,16.2l28.7,19.4c4.3,2.6,3.5,8.5,2.9,12.8
			c-0.8,7-5.6,13.7-3.5,20.9c0.6,2.8,3.2,4.2,5.1,6.1c-2,7.1-2.9,14.7-0.9,22l-2.7,5.7l-13.4-1.9c2-2.7,4.3-5,6.1-7.9
			c1.2-4.6-0.1-9.5-1.4-14c-8.8-1.6-16.1-7.7-19.2-16.1c-12.1,0.2-22.8,6.9-34.8,8c-1.3-4-4.1-7.1-5.8-10.9
			c-0.9,5.5,3.8,11.4,0,16.3c-0.6-2.1-1.8-4-3.9-4.9c-0.4,4.2-0.1,8.5,0.7,12.7c0.7,3-1.2,5.6-2.1,8.3l-13.8-2
			c2.1-2.5,4.5-4.7,6.4-7.3c0.9-3.3-0.6-6.5-1.3-9.7c-1.4,2-3.6,3.2-6,3.4c2.1-4.8-1-9.3-3.3-13.3c-2-3.2-2.7-7-3.3-10.7
			c-2.2,3.1-2.9,7.5-6.7,9.2c0.1-2.6-0.4-5.1-2.7-6.7c0.4,4.4-1.2,9.2-5.5,11.1c0.3-2.1,1.1-4.5-0.4-6.3c-2.4-3.2-5.2-6.2-5.8-10.3
			c-3.7-1-7.7-3.1-7.8-7.4c-2.1-10.7-1-21.7,3-31.8c5.5-10.2,20.4-10.9,28.9-4C83.6,74.2,87.4,70.3,92.3,68.4 M73.8,79.2
			c0.5,2.5,1.9,5.5,0.2,7.9c-1.8,2.7-5.2,2.9-7.8,4.2c-2.4,1.7-0.9,6,2.1,5.8c3.5-0.2,7.1-1.6,9.2-4.6c2.7-4.4,1.4-10.2-3.1-12.9
			C74.2,79.4,74,79.3,73.8,79.2 M54.7,100.1c2.5,1.2,5.4,0.8,7.5-1C60,97,56.2,97.4,54.7,100.1z"
                        />
                        <path
                            className={styles.st2}
                            d="M73.8,79.2c4.5,2.2,6.4,7.7,4.2,12.2c-0.2,0.4-0.4,0.8-0.7,1.1c-2.2,2.8-5.6,4.5-9.2,4.6
			c-3,0.2-4.5-4.1-2.1-5.8c2.6-1.3,6.1-1.5,7.8-4.2C75.7,84.7,74.3,81.8,73.8,79.2 M54.7,100.1c1.5-2.7,5.3-3.1,7.5-1
			C60.1,100.9,57.2,101.3,54.7,100.1z"
                        />
                    </g>
                    <path
                        className={classNames(styles.wing, styles.st2)}
                        d="M125,31.3c5.3-1.8,11-2.5,16.6-2.2c-4.6,2.7-8.6,6.4-11.6,10.8c16.1-6,34.7-0.3,47.3,10.6
		c-2.6,0.4-5.2,0.8-7.7,1.9c-10.5,4.5-19.8,12-31,14.7c4.7,0.9,9,3,12.7,6c-6.3,3.9-13.4,6.4-20.7,7.5c2.4,3,4.9,6,6.3,9.5
		c-9.4,2.8-19.5,2.2-28.6-1.6c0.4-6.3-1.1-13-5.8-17.4c-4-4.2-10.5-6.5-12.2-12.5c-1.1-5.8,2.4-11.3,6.1-15.5
		c4.1-4.5,9.2-7.9,14.5-10.7c-4.9,5.4-8.4,12.7-7.1,20.2c3.6-2.7,7.3-5.1,11.6-6.7c-1,4.1-2.8,8-5,11.6c4.7-1.5,9.5-2.7,14.5-2.6
		c-2.2,3.2-5,5.8-8.4,7.6c2.4,1.8,5.3,3.6,6.5,6.4c-3.2,1.1-6.6,1.5-10,1.4c1.9,5,2.6,10.3,2.6,15.6c1.7-3.9,2.4-8.2,1.5-12.4
		c4.1,0,8-1.6,11.6-3.4c-1.6-2.7-3.7-5-6.1-7c4.7-3.1,8-7.7,10.9-12.5c-5.1-0.7-10.4-0.5-15.4,1c1.8-3.7,2.5-7.7,3.1-11.8
		c-4.7,1.2-9.1,3.2-13.1,5.9C110.5,38.3,117.7,33.6,125,31.3L125,31.3z"
                    />
                </g>
            </svg>
        </div>
    );
});

export default BrandLoader;
