import React, { FC } from 'react';
import classnames from 'classnames';
import { IProductFields } from '../../../../../@generated/@types/contentful';
import { Entry } from 'contentful';
import ContentfulImage from '../../../../../components/atoms/ContentfulImage';
import styles from './bww.nonexistentItem.module.css';

interface NonexistentItemProps {
    contentfulProduct: Entry<IProductFields>;
    quantity: number;
}

const NonexistentItem: FC<NonexistentItemProps> = ({ contentfulProduct, quantity }): JSX.Element => (
    <div className={styles.simpleCard}>
        <ContentfulImage className={styles.simpleCardImage} asset={contentfulProduct?.fields?.image} />
        <div className={classnames('t-subheader-smaller', styles.simpleCardInfo)}>
            {`${quantity > 1 ? quantity + 'X ' : ''}${contentfulProduct?.fields?.name}`}
        </div>
    </div>
);

export default NonexistentItem;
