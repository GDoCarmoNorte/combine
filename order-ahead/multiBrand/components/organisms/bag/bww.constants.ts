export const typeButton = 'primary';

export const TYPOGRAPHY_CLASS = {
    NAME: 't-subheader-smaller',
};

export const PLUS_MINUS_ICONS_ACTIVE_VARIANT = 'dark';

export const SHOW_NUMBER_ITEMS = false;
