import { TInitialPaymentTypes } from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import {
    isCreditOrDebitPayEnabled,
    isGiftCardPayEnabled,
    isCardOnFilePayEnabled,
} from '../../../../lib/getFeatureFlags';
import { ConfigurationValues } from '../../../../redux/configuration';

export const getInitialPaymentType = (configuration: ConfigurationValues) => {
    const isPayInStoreOnly = [
        configuration.isGooglePayEnabled,
        configuration.isApplePayEnabled,
        isGiftCardPayEnabled(),
        isCreditOrDebitPayEnabled(),
        isCardOnFilePayEnabled(),
    ].every((featureFlag) => !featureFlag);

    return isPayInStoreOnly ? TInitialPaymentTypes.PAY_IN_STORE : TInitialPaymentTypes.PLACEHOLDER;
};
