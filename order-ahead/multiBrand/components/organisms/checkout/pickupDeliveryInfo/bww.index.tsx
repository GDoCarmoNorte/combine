import React, { useCallback, useEffect, useState, useMemo } from 'react';
import {
    isSameDay,
    parseISO,
    isAfter,
    differenceInMinutes,
    zonedTimeToUtc,
    utcToZonedTime,
} from '../../../../../common/helpers/dateTime';

import { IAvailableTimesModel, TServiceTypeModel } from '../../../../../@generated/webExpApi';
import { useBag, useOrderLocation } from '../../../../../redux/hooks';
import { resolveOpeningHours } from '../../../../../lib/locations';
import {
    addASAPFieldToRange,
    enhanceDaysWithLabel,
    enhanceTimesWithLabel,
} from '../../../../../common/helpers/checkoutHelpers';

import { Form, Formik } from 'formik';

import styles from '../../../../../components/organisms/checkout/pickupDeliveryInfo/index.module.css';
import { WorkingHours } from '../../../../../redux/bag';
import useLocationAvailableTimes from '../../../../../common/hooks/useLocationAvailableTimes';
import FormikInput from '../../../../../components/organisms/formikInput';
import { createConnector } from '../../../../../components/clientOnly/formik/connector';
import Dropdown from '../../../../../components/organisms/checkout/pickupDeliveryInfo/dropdown';
import classnames from 'classnames';

export interface IDeliveryInfo {
    place: string;
    instructions: string;
}

const FormikConnector = createConnector<IDeliveryInfo>();
const noop = (): void => null;

interface IPickupInfoProps {
    onChange: (payload: { time?: string; asap?: boolean }) => void;
    onInvalidTimeUpdate: (error: { title: string; message: string }) => void;
    loading: boolean;
    onDeliveryInfoChange: (values: IDeliveryInfo) => void;
    fulfilmentTime?: Date;
}

const ADDRESS_TEXT_MAX_LENGTH = 50;
const TODAY_PLACEHOLDER = 'Today';
const ASAP_PLACEHOLDER = 'ASAP';
const DELIVERY_INSTRUCTIONS_MAX_LENGTH = 100;

function PickupDeliveryInfo(props: IPickupInfoProps): JSX.Element {
    const { onChange, onInvalidTimeUpdate, onDeliveryInfoChange, loading, fulfilmentTime } = props;
    const {
        orderTime,
        orderTimeType,
        pickupTimeValues,
        pickupTimeIsValid,
        actions: { setPickupTime, setPickupTimeValues },
    } = useBag();
    const { getLocationAvailableTimeSlots } = useLocationAvailableTimes();
    const {
        method,
        isPickUp,
        deliveryLocationTimeSlots,
        pickupLocationTimeSlots,
        deliveryAddress,
        currentLocation: location,
        actions: { setDeliveryLocationAvailableTimeSlots, setPickupLocationAvailableTimeSlots },
    } = useOrderLocation();

    const pickupDeliveryLabel = isPickUp ? 'Pickup' : 'Delivery';
    const initialDeliveryPlace = deliveryAddress?.deliveryLocation?.addressLine2;

    const [pickupDay, setPickupDay] = useState<{ day: string; timeRange: string[] }>(null);
    const [updateAvailableTimeSlotsInProgress, setUpdateAvailableTimeSlotsInProgress] = useState<boolean>(false);
    const [initValuesSet, setInitValuesSet] = useState(false);
    const [initValuesChanged, setInitValuesChanged] = useState(false);
    const [fieldLevelError, setFieldLevelError] = useState<string>(null);
    const [lastAvailableTimeUpdate, setLastAvailableTimeUpdate] = useState<Date>(null);

    useEffect(() => {
        updateAvailableTimes();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const locationTimeZone = useMemo(() => location.timezone, [location]);
    const storeIsOpenNow = useMemo(() => {
        const resolvedOpeningHours = resolveOpeningHours(location, TServiceTypeModel.OrderAhead);

        return !!resolvedOpeningHours && resolvedOpeningHours.isOpen;
    }, [location]);

    const timeValues = useMemo(() => {
        if (!isPickUp && orderTimeType === 'asap') {
            return (
                pickupTimeValues?.map(({ day, timeRange }) => {
                    const fulfilmentTimeString = fulfilmentTime.toISOString();
                    return isSameDay(new Date(day), fulfilmentTime)
                        ? { day: fulfilmentTimeString, timeRange: [fulfilmentTimeString, ...timeRange] }
                        : { day, timeRange };
                }) || []
            );
        }

        return pickupTimeValues;
    }, [pickupTimeValues, fulfilmentTime]);

    const timeValuesAvailable = timeValues && timeValues.length > 0;
    const deliveryTimesNotAvailable = !timeValuesAvailable && !isPickUp;
    const pickupDayLabel = useMemo(() => (deliveryTimesNotAvailable ? 'Unavailable' : pickupDay?.day), [
        pickupDay,
        deliveryTimesNotAvailable,
    ]);

    const timeOptions = useMemo(() => {
        if (!pickupDay) return [];
        let options = enhanceTimesWithLabel(pickupDay.timeRange, locationTimeZone);

        if (storeIsOpenNow) {
            options = addASAPFieldToRange({
                timeRange: options,
                selectedDay: pickupDay.day,
                timezone: locationTimeZone,
                prepTime: location.additionalFeatures?.prepTime,
                asapWithoutBrackets: true,
                isAdditionalASAP: orderTimeType !== 'asap' && !isPickUp,
            });
        }

        return options;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pickupDay, locationTimeZone, storeIsOpenNow]);

    const pickupTimeLabel = useMemo(
        () =>
            deliveryTimesNotAvailable ? 'Unavailable' : orderTimeType === 'asap' ? timeOptions[0]?.value : orderTime,
        [orderTimeType, timeOptions, orderTime, deliveryTimesNotAvailable]
    );

    useEffect(() => {
        setPickupTimeValues(getAvailableTimeSlots());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isPickUp, pickupLocationTimeSlots, deliveryLocationTimeSlots]);

    useEffect(() => {
        if (!timeValues || timeValues.length === 0 || !lastAvailableTimeUpdate) return;
        if (orderTimeType === 'asap' && !initValuesSet) {
            setPickupDay(timeValues[0]);
            setInitValuesSet(true);
            return;
        }

        if (orderTime && !initValuesSet) {
            if (!pickupTimeIsValid) {
                const pickupDay = timeValues.reduce((acc, item) => {
                    if (isSameDay(parseISO(item.day), parseISO(orderTime))) {
                        return item.timeRange.find((time) => {
                            if (time === orderTime) return true;
                            if (isAfter(parseISO(time), parseISO(orderTime))) return true;
                        });
                    }
                    return acc;
                }, '');

                pickupDay
                    ? setPickupTime({ time: pickupDay, method })
                    : setPickupTime({ time: timeValues[0].timeRange[0], method });
                setFieldLevelError(
                    `Previously selected ${pickupDeliveryLabel} time is not valid. Please review your selection`
                );
                setInitValuesSet(true);
                return;
            }

            setInitValuesSet(true);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, orderTimeType, timeValues, pickupTimeIsValid, lastAvailableTimeUpdate]);

    useEffect(() => {
        if (!timeValues || timeValues.length === 0) return;

        if (!orderTime) {
            setPickupDay(timeValues[0]);
            return;
        }

        const pickupDay = timeValues.find((day) => {
            const selectedDayWithTimezone = utcToZonedTime(new Date(day.day), locationTimeZone);
            const pickupTimeWithTimezone = utcToZonedTime(new Date(orderTime), locationTimeZone);
            return isSameDay(selectedDayWithTimezone, pickupTimeWithTimezone);
        });

        if (!pickupDay) return;
        setPickupDay(pickupDay);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [timeValues]);

    const onChangeCallback = useCallback(() => {
        if (!timeValues || !initValuesSet || !(orderTime || orderTimeType === 'asap')) return;

        if (!initValuesChanged) {
            setInitValuesChanged(true);
            return;
        }

        if (typeof onChange === 'function') {
            if (orderTimeType === 'asap') {
                onChange({ asap: true });
            } else if (orderTimeType === 'future') {
                const localStoreTime = zonedTimeToUtc(orderTime, location.timezone);

                onChange({ time: localStoreTime.toISOString() });
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, orderTimeType, initValuesSet]);

    useEffect(onChangeCallback, [orderTime, orderTimeType, initValuesSet, onChangeCallback]);

    const getTimeValues = (availableTimeSlots: IAvailableTimesModel): WorkingHours => {
        return Object.entries(availableTimeSlots?.byDay || {}).map(([_, timeRanges]) => ({
            day: `${timeRanges[0].utc}`,
            timeRange: timeRanges.map((time) => `${time.utc}`),
        }));
    };

    const recheckTime = async () => {
        if (!initValuesSet || !timeValues || timeValues.length === 0) return;

        const closestPickupTime = timeValues[0].timeRange[0];
        const currentDate = new Date(Date.now());
        const shouldUpdatePickupTimeValues =
            differenceInMinutes(currentDate, lastAvailableTimeUpdate || currentDate) >= 5 &&
            differenceInMinutes(parseISO(closestPickupTime), currentDate) < 15;
        if (shouldUpdatePickupTimeValues && !updateAvailableTimeSlotsInProgress) {
            await updateAvailableTimes();
            if (orderTimeType === 'asap') return;

            if (isAfter(parseISO(orderTime), parseISO(closestPickupTime))) return;

            onInvalidTimeUpdate({
                title: `${pickupDeliveryLabel} Time Update`,
                message: 'Selected date and time must be valid',
            });

            if (!fieldLevelError) {
                setFieldLevelError(`Scheduled ${pickupDeliveryLabel} time is invalid. Please review your selection`);
            }
        }
    };

    useEffect(() => {
        const interval = setInterval(recheckTime, 1000);
        return () => clearInterval(interval);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, orderTimeType, timeValues, initValuesSet]);

    const getAvailableTimeSlots = () => {
        return isPickUp
            ? getTimeValues(pickupLocationTimeSlots?.pickup)
            : getTimeValues(deliveryLocationTimeSlots?.delivery);
    };

    const updateAvailableTimes = async () => {
        setUpdateAvailableTimeSlotsInProgress(true);
        const setAvailableTimeSlots = isPickUp
            ? setPickupLocationAvailableTimeSlots
            : setDeliveryLocationAvailableTimeSlots;
        const locationId = location.id;
        await getLocationAvailableTimeSlots({
            locationId,
        })
            .then((availableTimeSlots) => {
                setAvailableTimeSlots(availableTimeSlots);
            })
            .catch(() => setAvailableTimeSlots(undefined))
            .finally(() => {
                setUpdateAvailableTimeSlotsInProgress(false);
                setLastAvailableTimeUpdate(new Date(Date.now()));
            });
    };

    const handlePickupDayChange = (value: string) => {
        const pickupDay = timeValues.find((day) => day.day === value);
        setPickupDay(pickupDay);
        setPickupTime({ time: null, method });
        setFieldLevelError(null);
    };

    const handleOrderTimeChange = (value: string) => {
        if ((timeOptions && timeOptions.length > 0 && value === timeOptions[0]?.value) || value === null) {
            setPickupTime({ asap: true, time: null, method });
        } else {
            setPickupTime({ time: value, asap: false, method });
        }

        setFieldLevelError(null);
    };

    return (
        <>
            <div className={styles.container}>
                <Dropdown
                    value={pickupDayLabel}
                    label={`${pickupDeliveryLabel} Date`}
                    labelClassName={styles.label}
                    name="day"
                    placeholder={timeValuesAvailable ? `${pickupDeliveryLabel} day` : TODAY_PLACEHOLDER}
                    options={
                        timeValues
                            ? enhanceDaysWithLabel(
                                  timeValues.map((days) => days.day),
                                  location.timezone
                              )
                            : []
                    }
                    onChange={handlePickupDayChange}
                    disabled={loading}
                    readOnly={deliveryTimesNotAvailable}
                    readOnlyErrorState={true}
                />
                <Dropdown
                    value={pickupTimeLabel}
                    label={`${pickupDeliveryLabel} Time`}
                    labelClassName={styles.label}
                    name="time"
                    placeholder={
                        timeValuesAvailable ? timeOptions[0]?.label || `${pickupDeliveryLabel} time` : ASAP_PLACEHOLDER
                    }
                    options={timeValuesAvailable ? timeOptions : []}
                    onChange={handleOrderTimeChange}
                    disabled={loading}
                    error={!orderTime && !(orderTimeType === 'asap') && 'Select time'}
                    readOnly={deliveryTimesNotAvailable}
                    readOnlyErrorState={true}
                />
            </div>
            {fieldLevelError && !deliveryTimesNotAvailable && <p className={styles.info}>{fieldLevelError}</p>}
            {loading && <p className={classnames('t-paragraph', styles.info)}>Refreshing order data</p>}
            {deliveryTimesNotAvailable && (
                <p className={classnames('t-paragraph', styles.deliveryTimesErrors)}>
                    Select either pickup or a different location
                </p>
            )}
            {!isPickUp && (
                <Formik
                    onSubmit={noop}
                    initialValues={{ place: initialDeliveryPlace, instructions: '' }}
                    enableReinitialize
                >
                    <Form>
                        <FormikInput
                            name="place"
                            label="APT/Suite #"
                            type="text"
                            placeholder="APT/Suite #"
                            optional
                            maxLength={ADDRESS_TEXT_MAX_LENGTH}
                            labelClassName={styles.labelInput}
                        />
                        <FormikInput
                            name="instructions"
                            label="Delivery Instructions"
                            type="text"
                            placeholder="Enter instructions"
                            optional
                            maxLength={DELIVERY_INSTRUCTIONS_MAX_LENGTH}
                            labelClassName={styles.labelInput}
                        />
                        <FormikConnector onChange={onDeliveryInfoChange} />
                    </Form>
                </Formik>
            )}
        </>
    );
}

export default PickupDeliveryInfo;
