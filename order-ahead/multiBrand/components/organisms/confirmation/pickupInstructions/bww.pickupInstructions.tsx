import React from 'react';
import { IPickupInstructionsItem } from '../../../../../@generated/@types/contentful';
import NumberedList, { NumberedListVariantsEnum } from '../../../../../components/sections/numberedList/numberedList';
import { INumberedListData } from '../../../../../components/sections/numberedList/numberedListSection';

interface PickupInstructionsProps {
    pickupInstructions: IPickupInstructionsItem[] | undefined;
}

function PickupInstructions(props: PickupInstructionsProps): JSX.Element {
    const items = props.pickupInstructions || [];

    const data: INumberedListData = {
        listItems: items.map((item) => ({
            itemTitle: item.fields.instruction,
        })),
    };

    return <NumberedList data={data} withoutShadow variant={NumberedListVariantsEnum.STYLIZED} />;
}

export default PickupInstructions;
