import React, { FC } from 'react';
import styles from './locationsListItemLayout.module.css';

import { ILocationsListItemLayout } from '../../../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItemLayout';

const LocationsListItemLayout: FC<ILocationsListItemLayout> = ({
    title,
    primaryCta,
    secondaryCta,
    address,
    openingHours,
    distance,
    services,
    storeId,
}) => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.leftColumn}>
                <div className={styles.title}>{title}</div>
                <div className={styles.address}>{address}</div>
                <div className={styles.storeId}>{storeId}</div>
                <div>{openingHours}</div>
                <div>{services}</div>
                <div className={styles.secondaryCta}>{secondaryCta}</div>
            </div>
            <div className={styles.rightColumn}>
                {distance}
                {primaryCta}
            </div>
        </div>
    );
};

export default LocationsListItemLayout;
