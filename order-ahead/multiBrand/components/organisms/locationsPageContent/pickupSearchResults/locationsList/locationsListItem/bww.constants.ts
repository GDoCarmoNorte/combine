export const ADDRESS_LINE_CLASSNAME = 'truncate-at-2 t-paragraph-small';
export const UNAVAILABLE_MESSAGE = 'Restaurant hours currently unavailable';
export const SHOW_UNAVAILABLE = true;
