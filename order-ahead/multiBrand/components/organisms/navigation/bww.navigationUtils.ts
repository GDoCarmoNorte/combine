import { LocationWithDetailsModel } from '../../../../common/services/locationService/types';
import { useOrderLocation } from '../../../../redux/hooks';

export const CheckLocation = (): LocationWithDetailsModel => {
    const { currentLocation: location } = useOrderLocation();
    return location;
};
