import { ICaloriesLegal, IProductFields } from '../../../../@generated/@types/contentful';

import { IDisplayProductSection, ISizeSelection } from '../../../../redux/types';
import { IPriceAndCalories } from '../../../../lib/tallyItem';
import { IModifierItemById } from '../../../../common/services/globalContentfulProps';
import { InspireCmsEntry } from '../../../../common/types';

export interface IProductBannerProps {
    productInfo: IPriceAndCalories;
    modifierItemsById: IModifierItemById;
    product: InspireCmsEntry<IProductFields>;
    productSections: IDisplayProductSection[];
    selections?: ISizeSelection[];
    onSizeSelect?: (productId: string) => void;
    displayName: string;
    showMakeItAMealButton?: boolean;
    productType: string;
    childProductId?: string;
    caloriesLegal?: ICaloriesLegal;
}
