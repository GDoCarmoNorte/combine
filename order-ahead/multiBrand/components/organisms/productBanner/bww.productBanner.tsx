import React, { useMemo } from 'react';

import { IProductBannerProps } from './bww.types';
import styles from './bww.productBanner.module.css';
import ContentfulImage from '../../../../components/atoms/ContentfulImage';
import TextWithTrademark from '../../../../components/atoms/textWithTrademark';
import { ProductItemPriceAndCalories } from '../../../../components/atoms/productItemPriceAndCalories';
import Extras from '../../../../components/organisms/extras/extras';
import { useMediaQuery } from '@material-ui/core';
import classNames from 'classnames';
import { useConfiguration, usePdp } from '../../../../redux/hooks';
import { useLocalization } from '../../../../common/hooks/useLocalization';
import SizeSelection from '../../../../components/atoms/sizeSelection';
import ModifiersSelection, { SauceModifiersSelection } from '../../../../components/organisms/modifiersSelection';
import { InspireButton } from '../../../../components/atoms/button';
import { getFormattedExtrasModifications, getFormattedModifications } from '../../../../lib/domainProduct';
import { useProductIsSaleable } from '../../../../common/hooks/useProductIsSaleable';
import {
    GTM_NOT_SALEABLE_DESCRIPTION,
    GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
    NOT_AVAILABLE_DESCRIPTION,
    NOT_SALEABLE_BUTTON_LINK,
    NOT_SALEABLE_BUTTON_TEXT,
    NOT_SALEABLE_DESCRIPTION,
} from '../../../../common/constants/product';
import { useSodiumWarning } from '../../../../common/hooks/useSodiumWarning';
import { useTallyItemHasSodiumWarning } from '../../../../common/hooks/useTallyItemHasSodiumWarning';
import { useSodiumLegalWarning } from '../../../../common/hooks/useSodiumLegalWarning';
import { SodiumWarningAreaEnumModel } from '../../../../@generated/webExpApi';
import { useBwwSelectionsText, useSelectedExtras } from '../../../../redux/hooks/pdp';
import {
    useDefaultModifiersByModifierGroupType,
    useNoItemProductIds,
    useProductItemGroup,
    useSelectedModifiersByModifierGroupTypes,
} from '../../../../redux/hooks/domainMenu';
import { getChangedModifiers } from '../../../../common/helpers/getChangedModifiers';
import WingTypeSelection from '../../../../components/organisms/wingTypeSelection';
import { ModifierGroupType } from '../../../../redux/types';
import { useProductIsAvailable } from '../../../../common/hooks/useProductIsAvailable';
import { GtmErrorEvent } from '../../../../common/services/gtmService/types';
import { useDispatch } from 'react-redux';
import { GTM_ERROR_EVENT } from '../../../../common/services/gtmService/constants';
import CaloriesLegalSection from '../../../../components/organisms/caloriesLegalSection/caloriesLegelSection';
import { ORDER_AHEAD_NOT_AVAIABLE_MESSAGE } from '../../../../common/constants/orderAhead';

export default function ProductBanner(props: IProductBannerProps): JSX.Element {
    const {
        product: contentfulProduct,
        productInfo,
        displayName,
        selections,
        onSizeSelect,
        productSections,
        caloriesLegal,
    } = props;
    const { pdpTallyItem } = usePdp();
    const { productId } = pdpTallyItem;
    const { locationLinkText, descriptionText, isProductAvailable, isLocationOrderAheadAvailable } = useLocalization(
        productId
    );
    const productItemGroup = useProductItemGroup(productId);
    const selectionsText = useBwwSelectionsText(pdpTallyItem, productItemGroup?.name);

    const isSmallVariant = useMediaQuery('(max-width: 767px)');
    const { isSaleable } = useProductIsSaleable(productId);
    const { isAvailable } = useProductIsAvailable(productId);
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    const dispatch = useDispatch();

    if (!isSaleable || !isAvailable || locationLinkText) {
        const payload = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: !isAvailable
                ? NOT_AVAILABLE_DESCRIPTION
                : !isSaleable
                ? GTM_NOT_SALEABLE_DESCRIPTION
                : descriptionText,
        } as GtmErrorEvent;
        dispatch({ type: GTM_ERROR_EVENT, payload });
    }

    const selectedExtras = useSelectedExtras();
    const extrasModifications = useMemo(() => selectedExtras && getFormattedExtrasModifications(selectedExtras), [
        selectedExtras,
    ]);

    const selectedModifiers = useSelectedModifiersByModifierGroupTypes(pdpTallyItem, [
        ModifierGroupType.MODIFICATIONS,
        ModifierGroupType.SAUCES,
        ModifierGroupType.WINGTYPE,
    ]);
    const defaultModifiers = useDefaultModifiersByModifierGroupType(productId, [
        ModifierGroupType.MODIFICATIONS,
        ModifierGroupType.SAUCES,
    ]);

    const noItemProductIds = useNoItemProductIds(selectedModifiers.map((i) => i.productId));

    const { addedModifiers, removedDefaultModifiers, modifiersIsChanged } = useMemo(
        () => getChangedModifiers(selectedModifiers, defaultModifiers, null, true),
        [selectedModifiers, defaultModifiers]
    );

    const modifiers = useMemo(
        () => getFormattedModifications(addedModifiers, removedDefaultModifiers, true, noItemProductIds),
        [addedModifiers, removedDefaultModifiers, noItemProductIds]
    );

    const sodiumWarning = useSodiumWarning();
    const sodiumLegalWarning = useSodiumLegalWarning();
    const isNycArea = sodiumLegalWarning?.additionalProperties.area === SodiumWarningAreaEnumModel.Nyc;

    const pdpHasSodiumWarning = useTallyItemHasSodiumWarning(pdpTallyItem);
    const showSodiumWarning = !!sodiumWarning && pdpHasSodiumWarning;

    const showModifiers = isSaleable && isAvailable && isProductAvailable && isLocationOrderAheadAvailable;

    const renderButtons = () => {
        if (!isOAEnabled) {
            return (
                <>
                    <p
                        className={classNames(
                            't-paragraph',
                            styles.orderAheadDescription,
                            styles.notSaleableDescription
                        )}
                        dangerouslySetInnerHTML={{
                            __html: ORDER_AHEAD_NOT_AVAIABLE_MESSAGE,
                        }}
                    />

                    <InspireButton type="primary" disabled={!isOAEnabled} text={NOT_SALEABLE_BUTTON_TEXT} />
                </>
            );
        }

        if (!isSaleable || !isAvailable) {
            return (
                <>
                    <p
                        className={classNames('t-paragraph', styles.notSaleableDescription)}
                        dangerouslySetInnerHTML={{
                            __html: isAvailable ? NOT_SALEABLE_DESCRIPTION : NOT_AVAILABLE_DESCRIPTION,
                        }}
                    />
                    <InspireButton link={NOT_SALEABLE_BUTTON_LINK} type="primary" text={NOT_SALEABLE_BUTTON_TEXT} />
                </>
            );
        }

        if (locationLinkText) {
            return (
                <>
                    <p className={classNames('t-paragraph')} dangerouslySetInnerHTML={{ __html: descriptionText }} />
                    <InspireButton link={'/locations'} type="primary" text={locationLinkText} />
                </>
            );
        }

        return null;
    };

    return (
        <div className={styles.container} aria-label="Product Main Section">
            {!isSmallVariant && (
                <div className={styles.bannerImageContainer}>
                    <ContentfulImage asset={contentfulProduct?.fields.image} />
                </div>
            )}
            <div className={styles.descriptionContainer}>
                <div>
                    <TextWithTrademark
                        tag="h1"
                        text={displayName || contentfulProduct.fields.name}
                        className={classNames(styles.menuItemName, 't-header-h1')}
                        afterContent={
                            showSodiumWarning &&
                            isNycArea && (
                                <img
                                    alt="Sodium warning label"
                                    className={styles.sodiumWarningIcon}
                                    src={sodiumWarning.fields.icon.fields.icon.fields.file.url}
                                />
                            )
                        }
                    />
                    {showSodiumWarning && !isNycArea && (
                        <img
                            alt="Sodium warning label"
                            className={classNames(styles.sodiumWarningIcon, styles.sodiumWarningPhillyIcon)}
                            src={sodiumWarning.fields.icon.fields.icon.fields.file.url}
                        />
                    )}
                </div>

                {showModifiers && (
                    <div className={classNames('t-paragraph-hint-strong', styles.modifiersContainer)}>
                        {selectionsText && <div title={selectionsText}>{`Selections: ${selectionsText}`}</div>}
                        {modifiersIsChanged && (
                            <div className={classNames('truncate', styles.modifications)} title={modifiers}>
                                {'Modifications: '}
                                {modifiers}
                            </div>
                        )}
                        {extrasModifications && (
                            <div className="truncate" title={extrasModifications}>
                                {`Extras: ${extrasModifications}`}
                            </div>
                        )}
                    </div>
                )}

                <ProductItemPriceAndCalories
                    className={classNames(styles.priceContainer, 't-paragraph-hint')}
                    price={isSaleable ? productInfo.price : null}
                    calories={productInfo.calories}
                />
                {isSmallVariant && (
                    <div className={styles.bannerImageContainer}>
                        <ContentfulImage asset={contentfulProduct?.fields.image} />
                    </div>
                )}

                {showModifiers && (
                    <>
                        {selections?.length > 1 && (
                            <>
                                <div className={styles.sidesHeader}>
                                    {selections[0].isGroupedBySize ? 'SIZE' : 'SELECT OPTIONS'}
                                </div>
                                <div className={styles.sizeSelectionWrapper}>
                                    <SizeSelection
                                        selections={selections}
                                        selectedProductId={productId}
                                        onSelect={onSizeSelect}
                                        className={styles.sizeSelectionContainer}
                                        itemClassName={styles.sizeSelectionItem}
                                    />
                                </div>
                            </>
                        )}

                        {productSections?.map((section) => {
                            if (section.productSectionType === 'sauce') {
                                return (
                                    <SauceModifiersSelection
                                        key={section.productGroupId}
                                        className={styles.modifiersSelection}
                                        title={section.productSectionDisplayName}
                                        productGroupId={section.productGroupId}
                                        productId={productId}
                                    />
                                );
                            }

                            if (section.productSectionType === ModifierGroupType.WINGTYPE) {
                                return (
                                    <WingTypeSelection
                                        key={section.productGroupId}
                                        productGroupId={section.productGroupId}
                                    />
                                );
                            }

                            return (
                                <ModifiersSelection
                                    key={section.productGroupId}
                                    className={styles.modifiersSelection}
                                    title={section.productSectionDisplayName}
                                    productGroupId={section.productGroupId}
                                    productId={productId}
                                />
                            );
                        })}
                        <Extras className={styles.extras} />
                    </>
                )}
                {renderButtons() && <div className={styles.buttonBlock}>{renderButtons()}</div>}
                <CaloriesLegalSection prop={caloriesLegal}></CaloriesLegalSection>
            </div>
        </div>
    );
}
