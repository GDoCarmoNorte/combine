# Conventions

## Inspire-OA build-time component toggling

- Confluence page -> https://inspirebrands.atlassian.net/wiki/spaces/DBBP/pages/1622409293/Inspire-OA+build-time+component+toggling
- GitLab MR -> https://gitlab.com/inspire1/digital-platform/poc/webapp/inspire-oa/-/merge_requests/1118

You can replace any file inside `/components` folder on build-time using the power of the webpack.The replacement policy based on the environment.

To replace a file you need to add a corresponding config in `/multiBrand/config/{ENVIRONMENT_NAME}.json`

Create the `{ENVIRONMENT_NAME}.json` if it’s not exists.

Your config file should look like this.
```json
{
    "toggleComponents": {
        "atoms/button.tsx": "atoms/arb-button.tsx",
        "atoms/button.module.css": "atoms/arb-and-jje-button.module.css",
        "organisms/checkout.tsx": "organisms/arb111-checkout.tsx"
    }
}
```

The component replacement policies are inside toggleComponents object.

**NOTE:** The 3rd line will replace the `{projectRoot}/components/atoms/button.tsx` with `{projectRoot}/multiBrand/components/atoms/arb-button.tsx`

This will restrict usage of this future outside of `/components` folder(as a file to replace) and /multiBrand/components folder(for replacement).

*To replace a component you should*
1. Create a component file inside `/multiBrand/components` folder.
2. Create the env config file inside `/multiBrand/config/` folder -> like `jje-qa01.json` (Skip if exists)
3. Add a rule inside `toggleComponents` field.
4. Don’t forget to write unit tests for your component.

---

## Inspire-OA feature flagging

- Confluence page -> https://inspirebrands.atlassian.net/wiki/spaces/DBBP/pages/1638925278/Inspire-OA+feature+flagging
- GitLab MR -> https://gitlab.com/inspire1/digital-platform/poc/webapp/inspire-oa/-/merge_requests/1168

You can turn off pieces of code via config file, using this feature.

To turn on the feature you need to add a corresponding config in `/multiBrand/config/{ENVIRONMENT_NAME}.json` all **features are disabled by default**

Create the `{ENVIRONMENT_NAME}.json` if it’s not exists.


Your config file should look like this.
```json
{
    "toggleFeatures": {
        "account/login": {
            "enabled": false,
            "description": "account login using in Customer Service Domain"
        }
    }
}
```
The feature flags are inside `toggleFeatures` object.
**NOTE:** You can either write `“enabled“: false` or did not write anything, features are disabled by default.

Usage of feature account/login
```js
function BaseHeader({ alertBanners, webNavigation }: HeaderProps): JSX.Element {
    const { featureFlags } = useFeatureFlags();

    return (
        <div className="sticky-top">
            {!!featureFlags['account/login'] && <p>feature flag is active | account/login</p>}
            {alertBanners && <AlertBanners alertBanners={alertBanners} />}
            {webNavigation && <Navigation navigation={webNavigation} alertBanners={alertBanners} />}
        </div>
    );
}
```
Getting feature flags from context at `2nd` line.
Usage of `account/login` at `6th` line.
