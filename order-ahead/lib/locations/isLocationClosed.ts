import { ILocationWithDetailsModel, TLocationStatusModel } from '../../@generated/webExpApi/models';

const isLocationClosed = (location: ILocationWithDetailsModel): boolean =>
    location?.status === TLocationStatusModel.Closed || location?.status === TLocationStatusModel.TempClosed;

export default isLocationClosed;
