import {
    isBefore,
    addDays,
    parse,
    setDay,
    isEqual,
    isWithinInterval,
    addWeeks,
    areIntervalsOverlapping,
    closestTo,
    formatRelative,
    differenceInHours,
    differenceInMinutes,
    formatTZ,
    utcToZonedTime,
} from '../../common/helpers/dateTime';

import { PickupAddress } from '../../redux/orderLocation';
import { IHoursModel, TServiceTypeModel } from '../../@generated/webExpApi/models';

import getLocationService from './getLocationService';
import { weekDayIndexMap } from './constants';

interface IResolveOpeningHoursResult {
    isOpen: boolean;
    isTwentyFourHourService: boolean;
    openTime: string | null;
    closeTime: string | null;
    isOpeningSoon: boolean;
    isClosingSoon: boolean;
    openedTimeRanges: IDateRange[];
    storeTimezone: string;
}

export interface IDateRange {
    start: Date;
    end: Date;
}

const isTwentyFourHoursService = (dateRange: IDateRange): boolean =>
    differenceInHours(dateRange.end, dateRange.start) === 24;

const preventOverlappingRanges = (sortedOpenedTimeRanges: IDateRange[]): IDateRange[] => {
    const firstTimeRange = sortedOpenedTimeRanges.slice(0, 1)[0];

    const combinedIntervals = sortedOpenedTimeRanges.slice(1).reduce<IDateRange[]>(
        (resultRanges, currentRange): IDateRange[] => {
            if (isTwentyFourHoursService(currentRange)) {
                return [...resultRanges, currentRange];
            }

            const lastRangeInResult = resultRanges.slice(-1)[0];

            const isOverlapping = areIntervalsOverlapping(lastRangeInResult, currentRange, { inclusive: true });

            if (isOverlapping) {
                return [
                    ...resultRanges.slice(0, -1),
                    {
                        start: lastRangeInResult.start,
                        end: currentRange.end,
                    },
                ];
            }

            return [...resultRanges, currentRange];
        },
        [firstTimeRange]
    );

    return combinedIntervals;
};

/*
 * week = -1 - previous week
 * week = 0 - current week
 * week = 1 - next week
 */
const getWeekOpenedTimeRanges = (serviceHoursSorted: IHoursModel[], week, timezone: string): IDateRange[] => {
    return serviceHoursSorted.map((serviceHours) => {
        const localStoreDate = utcToZonedTime(Date.now(), timezone);
        const localStoreWeekDayDate = setDay(localStoreDate, weekDayIndexMap[serviceHours.dayOfWeek]);

        const timezoneOffset = formatTZ(new Date(Date.now()), 'XXX', { timeZone: timezone });

        if (serviceHours.isTwentyFourHourService) {
            const start = parse('00:00 ' + timezoneOffset, 'HH:mm XXX', localStoreWeekDayDate);
            const end = addDays(start, 1);

            return {
                start: addWeeks(start, week),
                end: addWeeks(end, week),
            };
        }

        const start = parse(serviceHours.startTime + ' ' + timezoneOffset, 'HH:mm XXX', localStoreWeekDayDate);
        let end = parse(serviceHours.endTime + ' ' + timezoneOffset, 'HH:mm XXX', localStoreWeekDayDate);

        // if close time is before open time. Example:
        // openTime: "10:00 AM"
        // closeTime: "01:00 AM"
        if (isBefore(end, start) || isEqual(end, start)) {
            end = addDays(end, 1);
        }

        return {
            start: addWeeks(start, week),
            end: addWeeks(end, week),
        };
    });
};

const getOpenedTimeRanges = (serviceHours: IHoursModel[], timezone: string): IDateRange[] => {
    const currentWeekOpenedTimeRanges = getWeekOpenedTimeRanges(serviceHours, 0, timezone);

    const currentWeekLastOpenedTimeRange = currentWeekOpenedTimeRanges.slice(-1)[0];

    const previousWeekOpenedTimeRange = {
        start: addWeeks(currentWeekLastOpenedTimeRange.start, -1),
        end: addWeeks(currentWeekLastOpenedTimeRange.end, -1),
    };

    const nextWeekOpenedTimeRanges = getWeekOpenedTimeRanges(serviceHours, 1, timezone);

    const resultOpenedTimeRanges = [
        previousWeekOpenedTimeRange,
        ...currentWeekOpenedTimeRanges,
        ...nextWeekOpenedTimeRanges,
    ];

    return preventOverlappingRanges(resultOpenedTimeRanges);
};

export const getHoursWithTimezone = (hours: IHoursModel[], storeTimezone: string): IResolveOpeningHoursResult => {
    const result = {
        isOpen: false,
        isTwentyFourHourService: false,
        openTime: null,
        closeTime: null,
        isOpeningSoon: false,
        isClosingSoon: false,
    };
    const utcTimeNow = new Date(Date.now());
    const localStoreTimeWithTimezone = utcToZonedTime(utcTimeNow.toISOString(), storeTimezone);
    const openedTimeRanges = getOpenedTimeRanges(hours, storeTimezone);
    const currentOpenedTimeRange = openedTimeRanges.find((range) => isWithinInterval(utcTimeNow, range));

    // store closed
    if (!currentOpenedTimeRange) {
        const openedTimes = openedTimeRanges.map((range) => range.start).filter((date) => isBefore(utcTimeNow, date));

        const openTime = closestTo(utcTimeNow, openedTimes);
        const openTimeWithTimezone = utcToZonedTime(openTime, storeTimezone);

        return {
            ...result,
            openTime: formatRelative(openTimeWithTimezone, localStoreTimeWithTimezone),
            isOpeningSoon: differenceInMinutes(openTime, utcTimeNow) <= 30, // 30 minutes
            storeTimezone,
            openedTimeRanges,
        };
    }

    const currentOpenedTimeRangeWithTimezone = {
        start: utcToZonedTime(currentOpenedTimeRange.start, storeTimezone),
        end: utcToZonedTime(currentOpenedTimeRange.end, storeTimezone),
    };

    // store opened
    return {
        ...result,
        isOpen: !!currentOpenedTimeRange,
        openTime: formatRelative(currentOpenedTimeRangeWithTimezone.start, localStoreTimeWithTimezone),
        closeTime: formatRelative(currentOpenedTimeRangeWithTimezone.end, localStoreTimeWithTimezone),
        isClosingSoon: differenceInMinutes(currentOpenedTimeRange.end, utcTimeNow) <= 30, // 30 minutes
        isTwentyFourHourService: isTwentyFourHoursService(currentOpenedTimeRangeWithTimezone),
        storeTimezone,
        openedTimeRanges,
    };
};

const resolveOpeningHours = (
    location: PickupAddress,
    serviceType: TServiceTypeModel
): IResolveOpeningHoursResult | null => {
    if (!location) {
        return null;
    }

    const { timezone: storeTimezone } = location;

    if (!storeTimezone) {
        return null;
    }

    const locationService = getLocationService(location, serviceType);
    if (!locationService) {
        return null;
    }

    const { hours } = locationService;

    return getHoursWithTimezone(hours, storeTimezone);
};

export default resolveOpeningHours;
