import { TServiceTypeModel } from '../../@generated/webExpApi';
import { PickupAddress } from '../../redux/orderLocation';
import getLocationService from './getLocationService';

const isLocationOrderAheadAvailable = (pickupAddress: PickupAddress, isOAEnabled: boolean): boolean => {
    return (
        isOAEnabled &&
        !!pickupAddress?.isDigitallyEnabled &&
        !!getLocationService(pickupAddress, TServiceTypeModel.OrderAhead)
    );
};

export default isLocationOrderAheadAvailable;
