import { DeliveryAddress } from '../../redux/orderLocation';

const getDeliveryAddressText = (address: DeliveryAddress): string =>
    `${address.deliveryLocation.addressLine1}, ${address.deliveryLocation.state}, ${address.deliveryLocation.zipCode}`;

export default getDeliveryAddressText;
