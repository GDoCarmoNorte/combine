import { TServiceTypeModel } from '../../@generated/webExpApi';
import { LocationWithDetailsModel } from '../../common/services/locationService/types';
import resolveOpeningHours from './resolveOpeningHours';

export function getOpeningHoursInfoString(location: LocationWithDetailsModel): string {
    const resolvedOpeningHours = resolveOpeningHours(location, TServiceTypeModel.Store);

    if (!resolvedOpeningHours) {
        return null;
    }

    const { isOpen, openTime, closeTime, isTwentyFourHourService } = resolvedOpeningHours;

    if (isOpen && isTwentyFourHourService) {
        return ' Open 24H';
    }

    if (isOpen) {
        return `Open Now - Closes ${closeTime}`;
    }

    return `Closed Now - Opens ${openTime}`;
}
