import { IDeliveryLocation, LocationWithDetailsModel } from '../../common/services/locationService/types';
import { ILocationAddressDetailsModel } from '../../@generated/webExpApi';

export function getLocationAddressString(location: LocationWithDetailsModel): string {
    const address = location?.contactDetails?.address;

    if (!address) return '';

    return getString(
        [
            getString([address.line1, address.line2, address.line3], ' '),
            address.city,
            getString([address.stateProvinceCode, address.postalCode], ' '),
        ],
        ', '
    );
}

export function getDeliveryAddressString(location: IDeliveryLocation): string {
    if (!location) return '';

    return getString(
        [
            getString([location.addressLine1, location.addressLine2], ' '),
            location.city,
            getString([location.state, location.zipCode], ' '),
        ],
        ', '
    );
}

export function getLocationAddressDetailsString(address: ILocationAddressDetailsModel): string {
    if (!address) return '';

    return getString(
        [address.line, address.cityName, getString([address.stateProvinceCode, address.postalCode], ' ')],
        ', '
    );
}

const getString = (arr: string[], separator: string): string => arr.filter(Boolean).join(separator);
