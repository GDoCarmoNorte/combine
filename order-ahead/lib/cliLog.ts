import chalk from 'chalk';

export const logInit = (
    prefix: string
): { log: (text: string, value?: string, color?: string) => void; logWarning: (text: string) => void } => {
    const log = (text: string, value?: string, color = 'green'): void =>
        console.log(prefix, chalk[color](text), value || '');
    const logWarning = (text: string): void => log(text, null, 'yellow');

    return { log, logWarning };
};
