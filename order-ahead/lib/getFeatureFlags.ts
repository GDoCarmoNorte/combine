export enum TFeatures {
    account,
    mainDealsBanner,
    accountDealsPage,
    showLocationsAsOrderedList,
    locationSpecificMenuCategories,
    signUpPhoneTooltip,
    isDeliveryEnabled,
    rewards,
    locationPages,
    localTapList,
    shoppingBagLocationSelect,
    locationTimeSlotsEnabled,
    customerPaymentMethod,
    placeholderPaymentOption,
    orderHistory,
    isTippingEnabled,
    isDineTimeEnabled,
    isOpenTableEnabled,
    isGooglePayEnabled,
    isApplePayEnabled,
    personalization,
    isOAEnabled,
    isFraudCheckEnabled,
    isPlayExperienceEnabled,
    isDineInOrdersEnabled,
    isCaloriesDisplayEnabled,
    isBreadcrumbSchemaOn,
    isGiftCardPayEnabled,
    isCreditOrDebitPayEnabled,
    isCardOnFilePayEnabled,
    isPayAtStoredEnabled,
    isSingUpBannerOnConfirmationEnabled,
    isMyTeamsEnabled,
    isMarketingPreferencesSignupEnabled,
}

type TFeaturesValues = keyof typeof TFeatures;

export type IFeatureFlags = {
    [K in TFeaturesValues]: boolean;
};

declare const FEATURE_FLAGS: IFeatureFlags;

export const getFeatureFlags = (): IFeatureFlags => {
    // please consider that webpack wil literally replace `FEATURE_FLAGS` with `{}` object
    return FEATURE_FLAGS;
};

export const getFeature = (name: TFeaturesValues): boolean => {
    return FEATURE_FLAGS[name] || false;
};

export const isAccountOn = (): boolean => getFeature('account');

export const isMainDealsBannerOn = (): boolean => getFeature('mainDealsBanner');

export const isAccountDealsPageOn = (): boolean => getFeature('accountDealsPage');

export const isLocationsShowAsOrderedList = (): boolean => getFeature('showLocationsAsOrderedList');

export const locationSpecificMenuCategoriesEnabled = (): boolean => getFeature('locationSpecificMenuCategories');

export const isSignUpPhoneTooltip = (): boolean => getFeature('signUpPhoneTooltip');

export const isDeliveryFlowEnabled = (): boolean => getFeature('isDeliveryEnabled');

export const isRewardsOn = (): boolean => getFeature('rewards');

export const isLocationPagesOn = (): boolean => getFeature('locationPages');

export const isLocalTapListOn = (): boolean => getFeature('localTapList');

export const isShoppingBagLocationSelectComponentEnabled = (): boolean => getFeature('shoppingBagLocationSelect');

export const locationTimeSlotsEnabled = (): boolean => getFeature('locationTimeSlotsEnabled');

export const isCustomerPaymentMethodEnabled = (): boolean => getFeature('customerPaymentMethod');

export const isPlaceholderPaymentOptionEnabled = (): boolean => getFeature('placeholderPaymentOption');

export const isOrdersHistoryOn = (): boolean => getFeature('orderHistory');

export const isTippingEnabled = (): boolean => getFeature('isTippingEnabled');

export const isDineTimeEnabled = (): boolean => getFeature('isDineTimeEnabled');

export const isOpenTableEnabled = (): boolean => getFeature('isOpenTableEnabled');

export const isGooglePayEnabled = (): boolean => getFeature('isGooglePayEnabled');

export const isApplePayEnabled = (): boolean => getFeature('isApplePayEnabled');

export const isPersonalizationEnabled = (): boolean => getFeature('personalization');

export const isOAEnabled = (): boolean => getFeature('isOAEnabled');

export const isFraudCheckEnabled = (): boolean => getFeature('isFraudCheckEnabled');

export const isPlayExperienceEnabled = (): boolean => getFeature('isPlayExperienceEnabled');

export const isDineInOrdersEnabled = (): boolean => getFeature('isDineInOrdersEnabled');

export const isCaloriesDisplayEnabled = (): boolean => getFeature('isCaloriesDisplayEnabled');

export const isBreadcrumbSchemaOn = (): boolean => getFeature('isBreadcrumbSchemaOn');

export const isGiftCardPayEnabled = (): boolean => getFeature('isGiftCardPayEnabled');

export const isCreditOrDebitPayEnabled = (): boolean => getFeature('isCreditOrDebitPayEnabled');

export const isCardOnFilePayEnabled = (): boolean => getFeature('isCardOnFilePayEnabled');

export const isPayAtStoredEnabled = (): boolean => getFeature('isPayAtStoredEnabled');

export const isSingUpBannerOnConfirmationEnabled = (): boolean => getFeature('isSingUpBannerOnConfirmationEnabled');

export const isMyTeamsEnabled = (): boolean => getFeature('isMyTeamsEnabled');

export const isMarketingPreferencesSignupEnabled = (): boolean => getFeature('isMarketingPreferencesSignupEnabled');

export default getFeatureFlags;
