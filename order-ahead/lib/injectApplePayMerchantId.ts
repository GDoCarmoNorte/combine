import fs from 'fs';
import dotenv from 'dotenv';

(() => {
    dotenv.config();

    const brand = process.env.NEXT_PUBLIC_BRAND;
    const env = process.env.ENVIRONMENT_NAME;
    const merchantId = process.env.NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID;

    console.log(
        '[POST-CONFIG]',
        'injecting apple pay merchant id:',
        `\n    - brand: "${brand}"`,
        `\n    - environment: "${env}"`
    );

    if (!merchantId) {
        return console.log(
            '[POST-CONFIG]',
            'Skipping this step since no "NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID" variables in env'
        );
    }

    if (!fs.existsSync('./public/.well-known')) {
        console.log('[POST-CONFIG]', 'Creating ".well-known" folder, since it is not exists');
        fs.mkdirSync('./public/.well-known');
    }
    fs.writeFileSync(`./public/.well-known/apple-developer-merchantid-domain-association.txt`, merchantId);

    console.log('[POST-CONFIG]', 'Apple pay merchant id injected successfully.');
})();
