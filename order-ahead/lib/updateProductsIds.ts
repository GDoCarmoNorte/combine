import { ContentfulManagement } from './contentfulManagement';
import Excel from 'exceljs';
import { IProductFields } from '../@generated/@types/contentful';
import { Entry } from 'contentful';

const updateProductId = async (product: Entry<IProductFields>, newProductId: string): Promise<void> => {
    try {
        const oldId = product.fields.productId['en-US'];
        // eslint-disable-next-line no-param-reassign
        product.fields.productId['en-US'] = newProductId;
        product.update();
        console.log(`${product?.fields?.name?.['en-US']} updated, ${oldId} -> ${newProductId}`);
        return;
    } catch (err) {
        console.error(err);
    }
};

async function updateProductsIds() {
    const environment = await ContentfulManagement.getEnvironment();

    const products = await environment.getEntries({
        content_type: 'product',
        limit: 1000,
    });

    const productModifiers = await environment.getEntries({
        content_type: 'productModifier',
        limit: 1000,
    });

    const PRODUCT_ID_KEYS_MAP = {
        'al a cart products': 'A',
        'combo-only products': 'A',
        meals: 'B',
        modifiers: 'A',
    };

    const MENU2_ID_KEYS_MAP = {
        'al a cart products': 'H',
        'combo-only products': 'G',
        meals: 'H',
        modifiers: 'G',
    };

    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile('menuIds.xlsx');

    workbook.eachSheet((ws) => {
        const wsName = ws.name;
        const productIdKey = PRODUCT_ID_KEYS_MAP[wsName];
        const menu2IdKey = MENU2_ID_KEYS_MAP[wsName];

        ws.eachRow(async (row, rowNumber) => {
            // rowNumber starts from 1, 1st row is cell names
            if (rowNumber > 1) {
                const productId = row.getCell(productIdKey).value;
                const newMenu2Id = row.getCell(menu2IdKey).value;

                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                const productOrModifier: any =
                    products.items.find((item) => item?.fields?.productId?.['en-US'] === productId) ||
                    productModifiers.items.find((item) => item?.fields?.productId?.['en-US'] === productId);

                if (productOrModifier && newMenu2Id) {
                    await updateProductId(productOrModifier, newMenu2Id as string);
                }
            }
        });
    });
}

updateProductsIds();
