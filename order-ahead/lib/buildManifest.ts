import { getAndWriteContentfulProps } from '../common/services/globalContentfulProps/getAndWriteContentfulProps';
import { getAndWriteContentfulConfiguration } from '../common/services/contentfulConfiguration/contentfulConfigurationNode';

Promise.all([
    getAndWriteContentfulProps({ preview: false }),
    getAndWriteContentfulConfiguration({ preview: false }),
]).catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
