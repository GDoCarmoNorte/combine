import fs from 'fs';
import xmlbuilder from 'xmlbuilder';

import { format } from '../common/helpers/dateTime';
import {
    getBaseUrlWithoutLocale,
    groupSiteMapData,
    ISiteMapCategory,
    isRegionSitemapSupported,
    getBaseUrl,
} from '../common/helpers/siteMapHelper';
import { getGlobalContentfulProps } from '../common/services/globalContentfulProps';
import { ContentfulDelivery } from './contentfulDelivery';
import fetch from 'node-fetch';
import { isLocationPagesOn } from './getFeatureFlags';

const context = {
    preview: false,
};

const BRAND = process.env.NEXT_PUBLIC_BRAND;

const BASE_LOCATIONS_URL = `${getBaseUrl()}/locations`;
const ALL_LOCATIONS_URL = `${getBaseUrl()}/locations/all`;
const STATIC_FILE_NAME = 'static.xml';
const LOCATIONS_FILE_NAME = 'locations.xml';
const REGION_SITEMAP_FILE_NAME = 'region_sitemap.xml';
const PUBLIC_DIRECTORY = './public';

const supportedLocales = ['en-ca'];

export const getLocationsUrls = async () => {
    const locationUrlResponse = await fetch(`${process.env.NEXT_PUBLIC_WEB_EXP_API}/v1/location/list/urls`);
    const locationUrlObject = await locationUrlResponse.json();
    return locationUrlObject;
};

export const getLocationsSitemap = async (): Promise<Array<string>> => {
    const locationUrlObject = await getLocationsUrls();
    const urls = locationUrlObject.urls || [];

    const locationUrlsStateOrProvinceCode = urls.map(({ countryCode, stateOrProvinceCode }) => {
        return `${BASE_LOCATIONS_URL}/${countryCode}/${stateOrProvinceCode}`;
    });
    const locationUrlsCityName = urls.map(({ countryCode, stateOrProvinceCode, cityName }) => {
        return `${BASE_LOCATIONS_URL}/${countryCode}/${stateOrProvinceCode}/${cityName}`;
    });
    const locationUrlsStoreId = urls.map(({ countryCode, stateOrProvinceCode, cityName, address, storeId }) => {
        return `${BASE_LOCATIONS_URL}/${countryCode}/${stateOrProvinceCode}/${cityName}/${address}/${storeId}`;
    });

    const locationUrlList = [
        BASE_LOCATIONS_URL,
        ALL_LOCATIONS_URL,
        ...locationUrlsStateOrProvinceCode,
        ...locationUrlsCityName,
        ...locationUrlsStoreId,
    ];

    const uniqueLocationUrlList = [...new Set(locationUrlList)];

    return uniqueLocationUrlList;
};

const getStaticSitemap = async () => {
    const globalContentfulProps = await getGlobalContentfulProps(context);
    const menuPage = await ContentfulDelivery(context.preview).getPage('menu');
    const siteMapCategories = await ContentfulDelivery().getSiteMapCategories();
    const siteMap = groupSiteMapData(menuPage, siteMapCategories, globalContentfulProps);

    return siteMap;
};

const parseSitemap = (siteMap: ISiteMapCategory[]): string[] => {
    const links = siteMap.reduce((links: string[], category) => {
        const mainLink = category.mainLink;

        const childLinks =
            category.linkBlocks?.reduce((links: string[], block) => {
                const mainLink = block.nameInUrl;
                const childLinks = block.links.map((link) => link.nameInUrl);

                const newLinks = [...links, ...childLinks];

                if (mainLink) newLinks.push(mainLink);

                return newLinks;
            }, []) || [];

        return [...links, mainLink, ...childLinks];
    }, []);

    return links;
};

const filterExternalLinks = (links: string[]): string[] => {
    return links.filter((link) => link && link[0] === '/');
};

const prependHostname = (links: string[]): string[] => {
    return links.map((link) => getBaseUrl() + link);
};

export const generateXMLString = (links: string[]): string => {
    const xmlDocument = xmlbuilder
        .create('urlset', { encoding: 'utf-8' })
        .att('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

    links.forEach((link) => {
        const updatedLink =
            link.includes('/menu/') || link.includes('/product/') ? `${link.replace(/\/+$/, '')}/` : link;
        xmlDocument
            .ele('url')
            .ele('loc', updatedLink)
            .up()
            .ele('lastmod', format(new Date(), 'yyyy-MM-dd'))
            .up()
            .ele('changefreq', 'weekly');
    });

    return xmlDocument.end({ pretty: true });
};

const generateGeneralXMLString = (links: string[]) => {
    const xmlDocument = xmlbuilder
        .create('sitemapindex', { encoding: 'utf-8' })
        .att('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

    links.forEach((link) => {
        xmlDocument.ele('sitemap').ele('loc', `${link}`).up();
    });

    return xmlDocument.end({ pretty: true });
};

const saveXMLAsPublicAsset = (xml: string, filename = 'sitemap.xml'): string => {
    const directoryPath = PUBLIC_DIRECTORY;
    if (!fs.existsSync(directoryPath)) {
        throw new Error(`Can't find ${directoryPath} directory and write ${filename}`);
    }
    fs.writeFileSync(`${directoryPath}/${filename}`, xml);

    return xml;
};

export const generateStaticSitemapXML = async (): Promise<string> => {
    const staticSiteMap = await getStaticSitemap();
    if (!staticSiteMap) return Promise.reject("Can't retrieve valid sitemap");

    const parsedSitemap = parseSitemap(staticSiteMap);
    const filteredSitemap = filterExternalLinks(parsedSitemap);
    const enhancedWithHostname = prependHostname(filteredSitemap);

    const staticXMLString = generateXMLString(enhancedWithHostname);

    return saveXMLAsPublicAsset(staticXMLString, STATIC_FILE_NAME);
};

export const generateLocationsSitemapXML = async (): Promise<string> => {
    const locationSitemap = await getLocationsSitemap();

    if (!locationSitemap) return Promise.reject("Can't retrieve valid sitemap");

    const locationsXMLString = generateXMLString(locationSitemap);

    return saveXMLAsPublicAsset(locationsXMLString, LOCATIONS_FILE_NAME);
};

const getLinksForRegionSitemap = (brand: string) => {
    const path = getBaseUrl();
    switch (brand) {
        case 'arbys': {
            return [`${path}/${STATIC_FILE_NAME}`, 'https://locations.arbys.com/sitemap.xml'];
        }
        // for bww and others:
        default: {
            const linksForGeneralSitemap = [`${path}/${STATIC_FILE_NAME}`];
            if (isLocationPagesOn()) {
                linksForGeneralSitemap.push(`${path}/${LOCATIONS_FILE_NAME}`);
            }
            return linksForGeneralSitemap;
        }
    }
};

export const generateRegionSitemapXML = async () => {
    const linksForRegionSitemap = getLinksForRegionSitemap(BRAND);
    const sitemapXMLString = generateGeneralXMLString(linksForRegionSitemap);
    return saveXMLAsPublicAsset(sitemapXMLString, REGION_SITEMAP_FILE_NAME);
};

const getLinksForGeneralSitemap = () => {
    const baseUrlWithoutLocale = getBaseUrlWithoutLocale(getBaseUrl());
    const generalSitemapLink = [`${baseUrlWithoutLocale}/${REGION_SITEMAP_FILE_NAME}`]; //en-us
    supportedLocales.forEach((locale) => {
        generalSitemapLink.push(`${baseUrlWithoutLocale}/${locale}/${REGION_SITEMAP_FILE_NAME}`); // all other supported locales, for example en-ca
    });
    return generalSitemapLink;
};

export const generateGeneralSitemapXML = async () => {
    const linksForGeneralSitemap = isRegionSitemapSupported(BRAND)
        ? getLinksForGeneralSitemap()
        : getLinksForRegionSitemap(BRAND);
    const sitemapXMLString = generateGeneralXMLString(linksForGeneralSitemap);
    return saveXMLAsPublicAsset(sitemapXMLString);
};

generateStaticSitemapXML().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});

isLocationPagesOn() &&
    generateLocationsSitemapXML().catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });

isRegionSitemapSupported(BRAND) &&
    generateRegionSitemapXML().catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });

generateGeneralSitemapXML().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});
