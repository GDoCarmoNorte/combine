import UAParser from 'ua-parser-js';
import supportedBrowsers from '../common/constants/supportedBrowsers';

const checkSupportBrowser = (): {
    isCurrentBrowserSupported: boolean;
    isMobile: boolean;
} => {
    const parser = new UAParser();

    const { name, version } = parser.getBrowser();
    const { name: osName } = parser.getOS();

    const { type, model } = parser.getDevice();

    const majorVersion = version?.split('.')[0];

    const checkOsName = model === 'Xbox' ? 'Windows Xbox' : osName;

    const supportedCurrentBrowser = supportedBrowsers.find(
        (browser) => browser.name === name && browser.os === checkOsName
    );

    const isMobile = ['mobile', 'tablet'].includes(type);

    if (supportedCurrentBrowser && (!supportedCurrentBrowser?.major || !majorVersion)) {
        return {
            isCurrentBrowserSupported: true,
            isMobile,
        };
    }

    const isCurrentBrowserSupported =
        supportedCurrentBrowser && Number(supportedCurrentBrowser.major.split('.')[0]) <= Number(majorVersion);

    return {
        isCurrentBrowserSupported,
        isMobile,
    };
};

export default checkSupportBrowser;
