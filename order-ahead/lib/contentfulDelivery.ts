import { ContentfulClientApi, Entry, EntryCollection } from 'contentful';
import { createClient, createPreviewClient } from './contentfulClient';
import {
    IAlertBannerFields,
    IBrandThemeFields,
    IDealFields,
    IEmptyShoppingBagFields,
    IFooterFields,
    ILocationNewsSectionFields,
    IMenuCategoryFields,
    IMenuFields,
    IMenuSubcategoryFields,
    INavigationFields,
    IPageFields,
    IProduct,
    IProductFields,
    IProductModifierFields,
    IProductNutritionLinkFields,
    ISiteMapCategoryFields,
    ISodiumWarningFields,
    IUserAccountMenuFields,
    ITapListMenuCategoryFields,
    IActionParameterFields,
    ICaloriesLegal,
    ICheckoutLegalFields,
} from '../@generated/@types/contentful';
import { getContentfulProductIdsByFields } from '../common/helpers/getContentfulProductIdsByFields';
import { getContentfulMenuCategoryIdsByFields } from '../common/helpers/menuCategoryHelper';

// TODO: add pagination and API rate limit support
const DEFAULT_LIMIT = 1000; // contentful maximum

export const sortCategoriesBySortOrder = (categories: string[], sortOrder: string[]): string[] => {
    return categories.sort((a, b) => {
        const indexA = sortOrder.indexOf(a);
        const indexB = sortOrder.indexOf(b);

        if (indexA !== -1 && indexB !== -1) {
            return indexA - indexB;
        }

        if (indexA === -1 && indexB === -1) {
            return 0;
        }

        if (indexA === -1) {
            return 1;
        }

        return -1;
    });
};

interface IProductDetailsPageDetails {
    menuCategoryUrl: string;
    productDetailsPageUrl: string;
    productIds: string[];
    menuCategoryIds: string[];
}

export interface IProductDetailsPagePath extends IProductDetailsPageDetails {
    productPath: string;
    canonicalPath: string;
}

interface IAllProductCategories {
    [productId: string]: string[];
}

class Contentful {
    client: ContentfulClientApi;

    constructor(isPreview = false) {
        this.client = isPreview ? createPreviewClient() : createClient();
    }

    getEntry(id: string): Promise<Entry<IPageFields>> {
        return this.client.getEntry(id, { include: 5 }); // TODO: What value do we need here?
    }

    async _getEntries<T>(contentType: string, limit: number, skip: number): Promise<EntryCollection<T>> {
        return this.client.getEntries({
            content_type: contentType,
            limit,
            skip,
        });
    }

    async getAllEntries<T>(contentType: string, limit = DEFAULT_LIMIT): Promise<EntryCollection<T>> {
        let step = limit;
        let skip = 0;
        const result = await this._getEntries<T>(contentType, limit, skip);

        if (result.total > step) {
            const requests = [];

            while (result.total > step) {
                step += limit;
                skip += limit;
                requests.push(this._getEntries<T>(contentType, limit, skip));
            }

            const data = await Promise.all(requests);
            data.forEach((response) => result.items.push(...response.items));
        }

        return result;
    }

    getAlertBanners(): Promise<EntryCollection<IAlertBannerFields>> {
        return this.client.getEntries({
            content_type: 'alertBanner',
        });
    }

    getPages(): Promise<EntryCollection<IPageFields>> {
        return this.client.getEntries({
            content_type: 'page',
            limit: DEFAULT_LIMIT,
        });
    }

    getSiteMapCategories(): Promise<EntryCollection<ISiteMapCategoryFields>> {
        return this.client.getEntries({
            content_type: 'siteMapCategory',
            limit: DEFAULT_LIMIT,
            include: 2,
        });
    }

    async getPage(nameInUrl?: string | undefined): Promise<Entry<IPageFields>> {
        // TODO: Try to memoize this as this is inefficient to call so many times
        const pages = await this.getPages();
        const page = pages.items.find((item) => item.fields.link.fields.nameInUrl === nameInUrl);
        if (!page) {
            throw new Error(`

            ----------------------------- ERROR ----------------------------
            Page "${nameInUrl} (nameInUrl)" does not exist in CMS, please check the content.
            ----------------------------------------------------------------

            `);
        }

        return this.getEntry(page.sys.id);
    }

    async getMenu(): Promise<Entry<IMenuFields>> {
        const menuEntries: EntryCollection<IMenuFields> = await this.client.getEntries({
            content_type: 'menu',
            include: 2,
        });

        return menuEntries.items[0];
    }

    async getCaloriesLegal(): Promise<Entry<ICaloriesLegal>> {
        const calLegalwhole = await this.client.getEntries<ICaloriesLegal>({
            content_type: 'caloriesLegal',
            limit: 1,
        });

        return calLegalwhole?.items[0] || null;
    }

    async getMenuCategories(): Promise<EntryCollection<IMenuCategoryFields>> {
        return this.client.getEntries<IMenuCategoryFields>({
            content_type: 'menuCategory',
            include: 10,
        });
    }

    async getMenuCategory(nameInUrl: string): Promise<Entry<IMenuCategoryFields>> {
        // TODO: Try to memoize this as this is inefficient to call so many times
        const categories = await this.getMenuCategories();
        const item = categories.items.find((item) => item.fields.link.fields.nameInUrl === nameInUrl);
        return item;
    }

    getTapListMenuCategories(): Promise<EntryCollection<ITapListMenuCategoryFields>> {
        return this.client.getEntries<ITapListMenuCategoryFields>({
            content_type: 'tapListMenuCategory',
            include: 10,
        });
    }

    async getTapListMenuCategory(nameInUrl: string): Promise<Entry<ITapListMenuCategoryFields>> {
        const categories = await this.getTapListMenuCategories();
        const item = categories.items.find((item) => item.fields.link.fields.nameInUrl === nameInUrl);

        return item;
    }

    getMenuSubcategories(): Promise<EntryCollection<IMenuSubcategoryFields>> {
        return this.client.getEntries({
            content_type: 'menuSubcategory',
        });
    }

    getProducts(): Promise<EntryCollection<IProductFields>> {
        return this.client.getEntries({
            content_type: 'product',
            limit: DEFAULT_LIMIT,
        });
    }

    getSodiumWarnings(): Promise<EntryCollection<ISodiumWarningFields>> {
        return this.client.getEntries({
            content_type: 'sodiumWarning',
            limit: DEFAULT_LIMIT,
        });
    }

    getProductNutritionLink(): Promise<EntryCollection<IProductNutritionLinkFields>> {
        return this.client.getEntries({
            content_type: 'productNutritionLink',
            limit: 1,
        });
    }

    async getProduct(nameInUrl: string): Promise<Entry<IProductFields>> {
        // TODO: Try to memoize this as this is inefficient to call so many times
        const menuItems = await this.getProducts();
        const item = menuItems.items.find((item) => item.fields.nameInUrl === nameInUrl);

        return item;
    }

    getDeals(): Promise<EntryCollection<IDealFields>> {
        return this.client.getEntries({
            content_type: 'deal',
            limit: DEFAULT_LIMIT,
        });
    }

    async getDeal(offerId: string): Promise<Entry<IDealFields>> {
        // TODO: Try to memoize this as this is inefficient to call so many times
        const dealItems = await this.getDeals();
        const item = dealItems.items.find((item) => item.fields.offerId === offerId);

        return item;
    }

    getProductModifiers(): Promise<EntryCollection<IProductModifierFields>> {
        return this.getAllEntries<IProductModifierFields>('productModifier');
    }

    getLocationNews(): Promise<EntryCollection<ILocationNewsSectionFields>> {
        return this.client.getEntries<ILocationNewsSectionFields>({
            content_type: 'locationNewsSection',
            limit: DEFAULT_LIMIT,
        });
    }

    getBrandTheme(): Promise<EntryCollection<IBrandThemeFields>> {
        return this.client.getEntries({
            content_type: 'brandTheme',
        });
    }

    getNavigation(): Promise<EntryCollection<INavigationFields>> {
        return this.client.getEntries({
            content_type: 'navigation',
        });
    }

    async getUserAccountMenu(): Promise<Entry<IUserAccountMenuFields>> {
        const userAccountMenuEntries: EntryCollection<IUserAccountMenuFields> = await this.client.getEntries({
            content_type: 'userAccountMenu',
            include: 3,
        });
        return userAccountMenuEntries?.items[0];
    }

    async getEmptyShoppingBag(): Promise<Entry<IEmptyShoppingBagFields>> {
        const emptyShoppingBagEntries: EntryCollection<IEmptyShoppingBagFields> = await this.client.getEntries({
            content_type: 'emptyShoppingBag',
        });
        return emptyShoppingBagEntries?.items[0];
    }

    async getProductDetailsPagePaths(): Promise<IProductDetailsPagePath[]> {
        const paths: IProductDetailsPageDetails[] = [];

        const menuCategories = await this.getMenuCategories();
        const fullProductMenu = await this.getMenu();

        const canonicalSortOrder = fullProductMenu.fields.categories.map((cat) => cat.fields.link.fields.nameInUrl);
        const allProductCategories: IAllProductCategories = {};

        const getProductId = (ids: string[]) => {
            return ids.join('-');
        };

        menuCategories.items.forEach((menuCategory) => {
            const menuCategoryUrl = menuCategory.fields.link.fields.nameInUrl;

            const addToPath = (p: IProduct) => {
                if (!p?.fields) return; // Prevent empty reference to deleted product
                const {
                    fields: { nameInUrl },
                } = p;

                const productIds = getContentfulProductIdsByFields(p.fields);

                // We need to track allProductCategories to find a canonicalPath
                allProductCategories[getProductId(productIds)] = allProductCategories[getProductId(productIds)] || [];
                allProductCategories[getProductId(productIds)].push(menuCategoryUrl);

                paths.push({
                    menuCategoryUrl,
                    productDetailsPageUrl: nameInUrl,
                    productIds,
                    menuCategoryIds: getContentfulMenuCategoryIdsByFields(menuCategory.fields),
                });
            };

            // Add category products
            menuCategory.fields.products?.forEach(addToPath);

            // Add subcategory products
            menuCategory.fields.subcategories &&
                menuCategory.fields.subcategories.forEach((subcategory) => {
                    subcategory.fields.products && subcategory.fields.products.forEach(addToPath);
                });
        });

        return paths.map((path) => {
            const canonicalCategory = sortCategoriesBySortOrder(
                allProductCategories[getProductId(path.productIds)],
                canonicalSortOrder
            )[0];

            return {
                ...path,
                productPath: `/menu/${canonicalCategory}/${path.productDetailsPageUrl}`,
                canonicalPath: `${process.env.NEXT_PUBLIC_APP_URL}/menu/${canonicalCategory}/${path.productDetailsPageUrl}`,
            };
        });
    }

    async getFooter(): Promise<Entry<IFooterFields>> {
        const footerEntries: EntryCollection<IFooterFields> = await this.client.getEntries({
            content_type: 'footer',
            include: 3,
        });

        return footerEntries?.items?.[0];
    }

    getActionParameters(): Promise<EntryCollection<IActionParameterFields>> {
        return this.client.getEntries({
            content_type: 'actionParameter',
            include: 5,
        });
    }

    async getCheckoutLegal(): Promise<Entry<ICheckoutLegalFields>> {
        const checkoutLegalEntries = await this.client.getEntries({
            content_type: 'checkoutLegal',
        });

        return checkoutLegalEntries?.items[0];
    }

    // don't use it in runtime
    async getConfiguration() {
        if (typeof window === 'object') throw new Error('don\'t use "getConfiguration" in runtime');
        try {
            const configuration = await this.client.getEntries({
                content_type: 'configuration',
                'fields.name': 'Global',
            });

            if (!configuration.items.length) {
                return { error: 'No data' };
            }
            return { response: configuration.items[0] };
        } catch (error) {
            return { error };
        }
    }
}

const instances: Map<boolean, Contentful> = new Map();

const ContentfulDelivery = (isPreview?: boolean): Contentful => {
    let instance = instances.get(!!isPreview);

    if (!instance) {
        instance = new Contentful(isPreview);
        instances.set(!!isPreview, instance);
    }

    return instance;
};

export { ContentfulDelivery };
