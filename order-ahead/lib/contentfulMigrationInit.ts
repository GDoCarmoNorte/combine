import fs from 'fs';
import { exec } from 'child_process';

import { getContentfullEnv, getContentfulSpace } from './contentfulClient';
import { initMigrationFile, logPrefix, migrationsPath } from './contentful.const';
import { logInit } from './cliLog';
import { getArgv, CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN } from './cli';

export const contentfulMigrationInit = (): void => {
    const contentfulEnv = getContentfullEnv();
    const contentfulSpace = getContentfulSpace();
    const token = getArgv(CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN);
    const { log, logWarning } = logInit(logPrefix);

    log('ENV: ', contentfulEnv);
    log('SPACE ID: ', contentfulSpace);

    if (!token) {
        logWarning('! token is missing, please use "--c-token=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (fs.existsSync(initMigrationFile)) {
        logWarning('! init migration file already exist');
        process.exitCode = 1;
        return;
    }

    !fs.existsSync(migrationsPath) && fs.mkdirSync(migrationsPath);

    const command = `contentful space generate migration \
--management-token ${token} \
-s ${contentfulSpace} \
-e ${contentfulEnv} \
-f ${initMigrationFile}`;

    log('Running command: ', command);

    exec(command, (error, data) => {
        if (error) {
            console.error('error: ', error.message);
            process.exitCode = 1;
            return;
        }

        log('data log: ', data);
    });
};

contentfulMigrationInit();
