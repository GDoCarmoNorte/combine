import fs from 'fs';
import isEqual from 'lodash/isEqual';
import { v4 as uuid } from 'uuid';

import { ContentfulManagement } from './contentfulManagement';
import { ContentType } from 'contentful-management/dist/typings/entities/content-type';
import { getContentfullEnv } from './contentfulClient';
import { getArgv, CONTENTFUL_DEST_KEY } from './cli';

type ContentTypeEntry = {
    contentType: ContentType;
    isNew: boolean;
};

const updateContentType = (source: ContentType, target: ContentType): boolean => {
    let needsToUpdate = false;

    Object.keys(source).forEach((key) => {
        if (key !== 'sys') {
            if (!isEqual(source[key], target[key])) {
                needsToUpdate = true;
            }
            // eslint-disable-next-line no-param-reassign
            target[key] = source[key];
        }
    });

    return needsToUpdate;
};

const getUpdatedContentTypes = async (env: string, filenames: string[]): Promise<ContentTypeEntry[]> => {
    const updatedContentTypes: ContentTypeEntry[] = [];

    console.log('\n[CONTENTFUL]', 'compare models with environment:', env);

    for (let i = 0; i < filenames.length; i++) {
        const filename = filenames[i];
        const file = JSON.parse(fs.readFileSync('./models/' + filename, 'utf-8')) as ContentType;
        const {
            sys: { id },
        } = file;

        try {
            const contentType = await ContentfulManagement.getContentType(id, env);
            const needsToUpdate = updateContentType(file, contentType);

            console.log('[CONTENTFUL]', id, 'model is changed:', needsToUpdate);

            if (needsToUpdate) {
                updatedContentTypes.push({ contentType, isNew: false });
            }
        } catch {
            updatedContentTypes.push({ contentType: file, isNew: true });
        }
    }

    return updatedContentTypes;
};

let isTemporary = false;
let destination = getArgv(CONTENTFUL_DEST_KEY);

const source = getContentfullEnv();

if (!destination) {
    isTemporary = true;
    destination = `tmp-${uuid()}`;
}

(async (source, destination, isTemporary) => {
    const filenames = fs.readdirSync('./models');
    const environment = isTemporary ? source : destination;
    const updatedContentTypes = await getUpdatedContentTypes(environment, filenames);

    if (isTemporary) {
        console.log('\n[CONTENTFUL]', 'creating temporary environment:', destination);

        await ContentfulManagement.createEnvironment(destination, source);

        console.log('[CONTENTFUL]', 'temporary environment created:', destination, '\n');
    }

    if (updatedContentTypes.length < 1) {
        console.log('\n[CONTENTFUL]', 'nothing to update in:', environment);

        return;
    }

    console.log('\n[CONTENTFUL]', 'pushing model changes to:', destination);

    for (let i = 0; i < updatedContentTypes.length; i++) {
        let contentType;
        const entry: ContentTypeEntry = updatedContentTypes[i];

        if (entry.isNew) {
            contentType = await ContentfulManagement.createContentType(
                entry.contentType.sys.id,
                entry.contentType.name,
                destination
            );
            updateContentType(entry.contentType, contentType);
        } else {
            contentType = entry.contentType;
        }

        console.log('[CONTENTFUL]', contentType.name, 'model updating in:', destination);

        const updatedContentType = await contentType.update();
        await updatedContentType.publish();

        console.log('[CONTENTFUL]', contentType.name, 'model is updated in:', destination);
    }

    console.log('\n[CONTENTFUL]', 'environment models are updated in:', destination);

    if (isTemporary) {
        console.log('[CONTENTFUL]', 'destroying temporary environment:', destination);

        await ContentfulManagement.destroyEnvironment(destination);

        console.log('[CONTENTFUL]', 'temporary environment destroyed:', destination, '\n');
    }
})(source, destination, isTemporary);
