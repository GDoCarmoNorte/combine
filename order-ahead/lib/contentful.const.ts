import path from 'path';

export const logPrefix = '[CONTENTFUL]';

export const nonFeatureEnvs = ['master', 'uat-stg', 'qa', 'dev'];

export const initMigtarionFileName = '0_init_migration.js';
export const migrationsDirName = 'migrations';
export const migrationsPath = path.join('.', migrationsDirName);
export const initMigrationFile = path.join(migrationsPath, initMigtarionFileName);

export const migrationsVersionTrackingContentType = {
    name: 'Migrations Version Tracking',
    description: "Internal Technical Type. Please don't touch!",
    displayField: 'version',
    fields: [
        {
            id: 'version',
            name: 'version',
            required: true,
            localized: false,
            type: 'Text',
        },
        {
            id: 'fileName',
            name: 'file name',
            required: true,
            localized: false,
            type: 'Text',
        },
        {
            id: 'date',
            name: 'date',
            required: true,
            localized: false,
            type: 'Text',
        },
    ],
};
