import fs from 'fs';

import { getContentfullEnv } from './contentfulClient';
import { ContentfulManagement } from './contentfulManagement';

const contentfulEnv = getContentfullEnv();

(async () => {
    console.log('[CONTENTFUL]', 'pulling changes from:', contentfulEnv, '\n');

    const sourceCollection = await ContentfulManagement.getContentTypes();

    !fs.existsSync('./models') && fs.mkdirSync('./models');

    sourceCollection.map((item) => {
        fs.writeFileSync('./models/' + item.sys.id + '.json', JSON.stringify(item, null, 2));

        console.log('[CONTENTFUL]', 'model', item.sys.id, 'pulled from:', contentfulEnv);
    });

    console.log('\n[CONTENTFUL]', 'pulling changes from succeeded:', contentfulEnv);
})();
