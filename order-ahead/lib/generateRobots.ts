import dotenv from 'dotenv';
import fs from 'fs';

dotenv.config();

const BRAND = process.env.NEXT_PUBLIC_BRAND;
const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;

const robotsDisallowContent = {
    arbys: `Disallow: /checkout
Disallow: /account/delete
Disallow: /confirmation`,

    bww: `Disallow: /login
Disallow: /checkout
Disallow: /confirmation
Disallow: /account/delete
Disallow: /cdn-cgi/l/email-protection`,
};

const content = `User-agent: *
${robotsDisallowContent[BRAND]}

Sitemap: ${BASE_URL}/sitemap.xml`;

const saveRobotsAsPublicAsset = (): string => {
    const directoryPath = './public';
    if (!fs.existsSync(directoryPath)) {
        throw new Error(`Can't find ${directoryPath} directory and write robots.txt`);
    }
    fs.writeFileSync(`${directoryPath}/robots.txt`, content);

    return content;
};

export const generateRobotsTxt = (): void => {
    try {
        saveRobotsAsPublicAsset();
    } catch (error) {
        console.error(error);
        process.exitCode = 1;
    }
};

generateRobotsTxt();
