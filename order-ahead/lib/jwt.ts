import jwt from 'jsonwebtoken';

const ISSUER = process.env.NEXT_PUBLIC_APP_URL,
    AUDIENCE = process.env.NEXT_PUBLIC_APP_URL,
    PUBLIC_KEY = process.env.NEXT_PUBLIC_AUTH_0_PROGRESSIVE_PROFILE_JWT_PUBLIC_KEY;

export function verifyJWT(token: string) {
    return jwt.verify(token, PUBLIC_KEY, { issuer: ISSUER, audience: AUDIENCE });
}
