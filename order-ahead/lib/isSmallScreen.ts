import useWidth from '../common/hooks/useWidth';

// TODO Fix usage of hooks
// eslint-disable-next-line react-hooks/rules-of-hooks
const isSmallScreen = () => useWidth() === 'sm';

export default isSmallScreen;
