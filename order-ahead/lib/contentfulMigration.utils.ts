import branch from 'git-branch';
import chalk from 'chalk';

import { format } from '../common/helpers/dateTime';
import { Environment } from 'contentful-management/dist/typings/entities/environment';
import { Space } from 'contentful-management/dist/typings/entities/space';

import { logInit } from './cliLog';
import { nonFeatureEnvs, logPrefix } from './contentful.const';
import { addApiKeys } from './contentfulManagement';

interface IVersionContentTypePayload {
    fields: {
        version: {
            [x: string]: string;
        };
        fileName: {
            [x: string]: string;
        };
        date: {
            [x: string]: string;
        };
    };
}

const { log, logWarning } = logInit(logPrefix);

export const getStringDate = (): string => format(new Date(), 'MM-dd-yy');
export const getVersionOfFile = (file: string): string => file.split('_')[0];
export const getFormattedName = (name: string): string => name.replace(/[^a-zA-Z0-9 ]/g, '-');
export const getTagName = (): string => (process.env.CI_APPLICATION_TAG || '').split('-')[0];

export const getVersionContentTypePayload = (
    defaultLocale: string,
    version: string,
    fileName: string
): IVersionContentTypePayload => ({
    fields: {
        version: {
            [defaultLocale]: version,
        },
        fileName: {
            [defaultLocale]: fileName,
        },
        date: {
            [defaultLocale]: getStringDate(),
        },
    },
});

export const getPreparedEnvironment = async (
    space: Space,
    destination: string,
    isReCreateFeatureEnv: boolean
): Promise<{ environment: Environment; envId: string }> => {
    let envId: string;
    let environment: Environment;

    // On CI branch.sync() returns null
    const branchName = getFormattedName(branch.sync() || getTagName());
    const isFeatureEnv = !nonFeatureEnvs.includes(destination);

    if (!isFeatureEnv) {
        log(` - Updating ${chalk.white(destination)} alias.`);
        envId = `${destination}-${branchName}-${getStringDate()}`;
    } else {
        log(' - Running on feature branch');
        envId = `${destination}-${branchName}`;
    }

    log(' - ENV ID: ', envId);
    log(' - Checking for existing versions of environment: ', envId);

    try {
        environment = await space.getEnvironment(envId);
    } catch (e) {
        logWarning(' - Environment not found');
    }

    if (environment && !isReCreateFeatureEnv) {
        throw new Error(
            'Feature Environment already exist, please use "--c-re-create" param to re-create feature env or remove existing env manually for non-feature envs'
        );
    }

    if (isFeatureEnv && environment && isReCreateFeatureEnv) {
        await environment.delete();
        log(' - Environment deleted');
    }

    log(' - Creating environment: ', envId);

    environment = await space.createEnvironmentWithId(
        envId,
        {
            name: envId,
        },
        isFeatureEnv ? 'dev' : destination
    );

    const delay = 10000;
    const maxNumberOfTries = 10;
    let count = 0;

    log(' - Waiting for environment processing... ');

    let status: string;

    while (count < maxNumberOfTries) {
        status = (await space.getEnvironment(environment.sys.id)).sys.status.sys.id;

        if (status === 'ready' || status === 'failed') {
            status === 'ready'
                ? log(' - Successfully processed new environment: ', envId)
                : logWarning(' - Environment creation failed');

            break;
        }

        await new Promise((resolve) => setTimeout(resolve, delay));
        count++;
    }

    if (status !== 'ready') {
        throw new Error('Error occurred while processed new environment. Exiting...');
    }

    await addApiKeys(space, envId);

    return { environment, envId };
};

export const reCreateQaEnvFromDev = async (space: Space): Promise<void> => {
    const envId = 'qa';
    let environment: Environment;

    log(' - ENV ID: ', envId);

    try {
        environment = await space.getEnvironment(envId);
    } catch (e) {
        throw new Error('Environment not found');
    }

    await environment.delete();
    log(' - Environment deleted');

    log(' - Creating environment from DEV: ', envId);

    environment = await space.createEnvironmentWithId(envId, { name: envId }, 'dev');

    const delay = 10000;
    const maxNumberOfTries = 10;
    let count = 0;

    log(' - Waiting for environment processing... ');

    let status: string;

    while (count < maxNumberOfTries) {
        status = (await space.getEnvironment(environment.sys.id)).sys.status.sys.id;

        if (status === 'ready' || status === 'failed') {
            status === 'ready'
                ? log(' - Successfully processed new environment: ', envId)
                : logWarning(' - Environment creation failed');

            break;
        }

        await new Promise((resolve) => setTimeout(resolve, delay));
        count++;
    }

    if (status !== 'ready') {
        throw new Error('Error occurred while processed new environment. Exiting...');
    }

    await addApiKeys(space, envId);
};
