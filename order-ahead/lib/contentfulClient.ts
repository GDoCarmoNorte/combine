import dotenv from 'dotenv';

import { ContentfulClientApi, createClient as createContentfulClient } from 'contentful';
import { createClient as createContentfulManageClient } from 'contentful-management';
import { ClientAPI } from 'contentful-management/dist/typings/create-contentful-api';

import { logPrefix } from './contentful.const';
import { logInit } from './cliLog';

dotenv.config();
const { log } = logInit(logPrefix);

const CONTENTFUL_PREVIEW_HOST = 'preview.contentful.com';

const {
    CONTENTFUL_ENV,
    CONTENTFUL_SPACE,
    CONTENTFUL_DELIVERY_TOKEN,
    CONTENTFUL_PREVIEW_TOKEN,
    CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN,
} = process.env;

export const getContentfulSpace = (): string => CONTENTFUL_SPACE;

export const getContentfullEnv = (): string => CONTENTFUL_ENV;

export const getContentfulMgmtAccessToken = (): string => CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN;

let clientApi: ContentfulClientApi;

export const createClient = (): ContentfulClientApi => {
    if (!clientApi) {
        clientApi = createContentfulClient({
            space: CONTENTFUL_SPACE,
            environment: CONTENTFUL_ENV,
            accessToken: CONTENTFUL_DELIVERY_TOKEN,
            removeUnresolved: true,
        });
    }

    return clientApi;
};

let previewClientApi: ContentfulClientApi;

export const createPreviewClient = (): ContentfulClientApi => {
    if (!previewClientApi) {
        previewClientApi = createContentfulClient({
            space: CONTENTFUL_SPACE,
            environment: CONTENTFUL_ENV,
            accessToken: CONTENTFUL_PREVIEW_TOKEN,
            host: CONTENTFUL_PREVIEW_HOST,
            removeUnresolved: true,
        });
    }

    return previewClientApi;
};

let manageClientApi: ClientAPI;

export const createManageClient = (): ClientAPI => {
    if (!manageClientApi) {
        log(` - Create Contentful Manage Client - token: ***${CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN.slice(-4)}`);
        manageClientApi = createContentfulManageClient({
            accessToken: CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN,
        });
    }

    return manageClientApi;
};
