import util from 'util';
import { readdir } from 'fs';
import chalk from 'chalk';

import { logPrefix, migrationsPath } from './contentful.const';
import { logInit } from './cliLog';
import { getVersionOfFile } from './contentfulMigration.utils';

const readdirAsync = util.promisify(readdir);
const { log } = logInit(logPrefix);

(async () => {
    log('- start migration versions check');
    const migrationFiles = await readdirAsync(migrationsPath);
    const sorter = (a: string, b: string) => Number(a) - Number(b);

    const availableMigrations = migrationFiles.map((file) => getVersionOfFile(file)).sort(sorter);

    let error = false;

    availableMigrations.forEach((value, index, migrations) => {
        const currentValue = Number(value);
        const prevValue = Number(migrations[index - 1]);
        const prevExpectedVersion = currentValue - 1;
        const legacyMissedVersion = 35;
        const isIncorrectVersion =
            currentValue !== 0 && prevExpectedVersion !== legacyMissedVersion && prevExpectedVersion !== prevValue;

        const isPrevVersionMissing = isIncorrectVersion && prevValue < prevExpectedVersion;
        const isVersionAlreadyExist = isIncorrectVersion && currentValue === prevValue;

        if (isPrevVersionMissing) {
            console.error(chalk.white.bgRed.bold(`!!!  Migration version is missing: ${prevExpectedVersion} `));
            error = true;
            return;
        }

        if (isVersionAlreadyExist) {
            console.error(chalk.white.bgRed.bold(`!!!  Migration version already exist: ${currentValue} `));
            error = true;
            return;
        }
    });

    if (error) {
        process.exitCode = 1;
        return;
    }

    log('✓ Migration check done!');
})();
