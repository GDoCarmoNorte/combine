const fs = require('fs');
const path = require('path');
const yargs = require('yargs');
const { hideBin } = require('yargs/helpers');

const options = yargs(hideBin(process.argv)).option({
    brandId: { describe: 'Official Brand ID', type: 'string', demandOption: true },
    brandIdOther: { describe: 'Other Brand ID', type: 'string', demandOption: true },
    locale: { describe: 'Locale', type: 'string', demandOption: false },
}).argv;

const DEFAULT_LOCALE = 'en-us';

const BRAND_ID = options.brandId.toLowerCase();
const BRAND_ID_OTHER = options.brandIdOther.toLowerCase();
const LOCALE_SELECTED = options.locale ? options.locale.toLowerCase() : DEFAULT_LOCALE;

const TEMPLATE_FILE = path.join(__dirname, '..', 'config', 'arb-dev01', 'arbys.env');

const ENVIRONMENTS = ['dev01', 'qa01', 'uat01', 'stg01', 'prd01', 'dem01'];

const isDefaultLocale = LOCALE_SELECTED === DEFAULT_LOCALE;

ENVIRONMENTS.forEach((env) => {
    const BRAND_CONFIG_FOLDER = path.join(__dirname, '..', 'config', `${BRAND_ID_OTHER}-${env}`);
    const BRAND_PUBLIC_FOLDER = path.join(__dirname, '..', 'public', 'brands', BRAND_ID);
    const MULTI_BRAND_CONFIG_FOLDER = path.join(__dirname, '..', 'multiBrand', 'config', `${BRAND_ID_OTHER}-${env}`);

    if (!fs.existsSync(BRAND_PUBLIC_FOLDER)) {
        fs.mkdirSync(BRAND_PUBLIC_FOLDER);
        fs.mkdirSync(path.join(BRAND_PUBLIC_FOLDER, 'fonts'));
    }

    if (!fs.existsSync(BRAND_CONFIG_FOLDER)) {
        fs.mkdirSync(BRAND_CONFIG_FOLDER);
    }
    if (!fs.existsSync(MULTI_BRAND_CONFIG_FOLDER)) {
        fs.mkdirSync(MULTI_BRAND_CONFIG_FOLDER);
    }

    if (isDefaultLocale) {
        fs.copyFileSync(TEMPLATE_FILE, path.join(BRAND_CONFIG_FOLDER, `${BRAND_ID}.env`));
        fs.closeSync(fs.openSync(path.join(MULTI_BRAND_CONFIG_FOLDER, `${BRAND_ID}.env`), 'a'));
    } else {
        fs.copyFileSync(TEMPLATE_FILE, path.join(BRAND_CONFIG_FOLDER, `${BRAND_ID}-${LOCALE_SELECTED}.env`));
        fs.closeSync(fs.openSync(path.join(MULTI_BRAND_CONFIG_FOLDER, `${BRAND_ID}-${LOCALE_SELECTED}.env`), 'a'));
    }
});

console.log('Review all files just created and adjust as needed!');
