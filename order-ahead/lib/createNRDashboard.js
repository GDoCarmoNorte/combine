const fs = require('fs');
const path = require('path');
const yargs = require('yargs');
const { hideBin } = require('yargs/helpers');

const template = require('../new-relic/dashboards/template.json');
const { generateNRDashboardJSON } = require('./createNRDashboard.util');

const options = yargs(hideBin(process.argv)).option({
    brand: { describe: 'Brand ID', type: 'string', demandOption: true },
    locale: { describe: 'Locale', type: 'string', demandOption: false },
    rollout: { describe: "Whether it's a production rollout or not", type: 'boolean', demandOption: false },
}).argv;

/**
 * Reads configuration files (only for QA, STG & PROD environments) for a given brand
 * to retrieve specific environment values to be used to create a NR dashboard.
 * @param {string} brand
 * @param {string} locale
 * @returns {object} configuration -
 */
const getConfiguration = (brand, locale) => {
    const environmentConfig = [];

    const APP_ID_STRING = 'NEXT_PUBLIC_NEWRELIC_APPLICATION_ID';
    const APP_HOSTNAME = 'NEXT_PUBLIC_APP_URL';
    const APP_SERVICE_HOSTNAME = 'NEXT_PUBLIC_WEB_EXP_API';

    const environments = [
        {
            name: 'PRODUCTION',
            id: 'prd01',
        },
        {
            name: 'STG',
            id: 'stg01',
        },
        {
            name: 'QA',
            id: 'qa01',
        },
    ];

    environments.forEach(({ id, name }) => {
        let suffix = `${brand}-${id}`;

        const isARBPROD = brand === 'arb' && id === 'prd01';
        const isARBSTG = brand === 'arb' && id === 'stg01';

        if (isARBPROD) suffix = 'prd01';
        if (isARBSTG) suffix = 'stg01';

        let brandFileName = brand;
        if (brand === 'arb') {
            brandFileName = 'arbys';
        }

        if (locale !== DEFAULT_LOCALE) brandFileName = `${brandFileName}-${locale}`;

        const environmentConfigString = fs.readFileSync(
            path.join(__dirname, '..', 'config', suffix, `${brandFileName}.env`),
            'utf8'
        );

        const appIdStartIdx = environmentConfigString.indexOf(APP_ID_STRING) + APP_ID_STRING.length + 1;
        const APP_ID = environmentConfigString.substring(
            appIdStartIdx,
            environmentConfigString.indexOf('\n', appIdStartIdx)
        );

        const appHostnameStartIdx = environmentConfigString.indexOf(APP_HOSTNAME) + APP_HOSTNAME.length + 1;
        const APP_HOST = environmentConfigString
            .substring(appHostnameStartIdx, environmentConfigString.indexOf('\n', appHostnameStartIdx))
            .substr(8);

        const appServiceHostnameStartIdx =
            environmentConfigString.indexOf(APP_SERVICE_HOSTNAME) + APP_SERVICE_HOSTNAME.length + 1;
        let APP_SERVICE_HOST = environmentConfigString
            .substring(appServiceHostnameStartIdx, environmentConfigString.indexOf('\n', appServiceHostnameStartIdx))
            .substr(8);
        APP_SERVICE_HOST = APP_SERVICE_HOST.substring(0, APP_SERVICE_HOST.indexOf('/'));

        environmentConfig.push({
            id,
            name,
            config: {
                appId: APP_ID,
                appHost: APP_HOST,
                appServiceHost: APP_SERVICE_HOST,
                newRelicServiceSuffix: suffix,
            },
        });
    });
    return environmentConfig;
};

const DEFAULT_LOCALE = 'en-us';
const OUTPUT_FILEPATH = path.join(__dirname, 'new-relic-dashboard.json');

const BRAND_ID = options.brand.toLowerCase();
const LOCALE_SELECTED = options.locale ? options.locale.toLowerCase() : DEFAULT_LOCALE;
const IS_BRAND_ROLLOUT = Boolean(options.rollout);

console.log(
    '\n[NEW RELIC DASHBOARD]',
    `generating NR dashboard for brand "${BRAND_ID}" and locale "${LOCALE_SELECTED}"`
);

const output = generateNRDashboardJSON(
    BRAND_ID,
    LOCALE_SELECTED,
    template,
    getConfiguration(BRAND_ID, LOCALE_SELECTED),
    IS_BRAND_ROLLOUT
);

fs.writeFileSync(OUTPUT_FILEPATH, JSON.stringify(output));
console.log(`\n[NEW RELIC DASHBOARD] NR dashboard generated successfully at "${OUTPUT_FILEPATH}"!`);
