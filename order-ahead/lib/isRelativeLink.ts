export default function isRelativeLink(link = ''): boolean {
    return !(link.indexOf('http://') === 0 || link.indexOf('https://') === 0);
}
