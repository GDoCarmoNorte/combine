import { Entry, EntryCollection } from 'contentful';
import { InspireCmsEntry, InspireCmsEntryCollection } from '../common/types';

export const optimizeContentfulEntry = <T>(entry: Entry<T>): InspireCmsEntry<T> => {
    return {
        fields: entry.fields,
        sys: {
            id: entry.sys.id,
            contentType: entry.sys.contentType || null,
        },
    };
};

export const optimizeContentfulCollection = <T>(collection: EntryCollection<T>): InspireCmsEntryCollection<T> => {
    return {
        items: collection.items.map(optimizeContentfulEntry),
    };
};

export const isEntry = (item) => typeof item === 'object' && item !== null && 'sys' in item && 'fields' in item;

export const optimizeContentfullData = (item) => {
    if (isEntry(item)) {
        return {
            ...optimizeContentfulEntry(item),
            fields: optimizeContentfullData(item.fields),
        };
    }

    return Object.entries(item).reduce((prev, [key, value]) => {
        if (isEntry(value)) {
            return { ...prev, [key]: optimizeContentfullData(value) };
        }

        if (Array.isArray(value)) {
            return {
                ...prev,
                [key]: value.map((it) => (isEntry(it) ? optimizeContentfullData(it) : it)),
            };
        }

        return { ...prev, [key]: value };
    }, {});
};
