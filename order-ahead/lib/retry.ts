export default async function retry<T>(fn: () => Promise<T>, attempts = 3, delay = 0): Promise<T> {
    let i = 0;
    let result: T;
    let error: Error;

    while (!result && i < attempts) {
        try {
            result = await fn();

            return result;
        } catch (err) {
            i++;
            error = err;
            result = null;
            if (delay > 0) {
                await new Promise((resolve) => setTimeout(resolve, delay));
            }
        }
    }

    throw error;
}
