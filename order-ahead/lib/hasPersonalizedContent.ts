import { Entry } from 'contentful';

const hasPersonalizedContent = (sections: Entry<unknown>[]): boolean => {
    return !!sections?.some((it) => it.sys.contentType.sys.id === 'action');
};

export default hasPersonalizedContent;
