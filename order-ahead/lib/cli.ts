import parser from 'yargs-parser';

export const BRAND_KEY = 'brand';
export const CONTENTFUL_ENV_KEY = 'contentful-env';
export const CONTENTFUL_SPACE_KEY = 'contentful-space';
export const CONTENTFUL_DEST_KEY = 'contentful-destination';
export const CONTENTFUL_DUMP_FILE = 'contentful-dump-file';
export const CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN = 'contentful-api-token';
export const CONTENTFUL_RE_CREATE_FEATURE_ENV = 'contentful-re-create-feature-env';
export const ENVIRONMENT_NAME_KEY = 'environment-name'; // [dev, qa]
export const ENVIRONMENT_TYPE_KEY = 'environment-type'; // [fast, slow]
export const AKV_TOKEN_SOURCE_KEY = 'akv-token-source'; // [api, cli]
export const LOCALE_KEY = 'locale'; // en-us

const argv = parser(process.argv.slice(2), {
    alias: {
        [BRAND_KEY]: ['b'],
        [CONTENTFUL_ENV_KEY]: ['e'],
        [CONTENTFUL_SPACE_KEY]: ['s'],
        [CONTENTFUL_DEST_KEY]: ['d'],
        [CONTENTFUL_DUMP_FILE]: ['f'],
        [ENVIRONMENT_NAME_KEY]: ['n'],
        [ENVIRONMENT_TYPE_KEY]: ['t'],
        [LOCALE_KEY]: ['l'],
        [AKV_TOKEN_SOURCE_KEY]: ['a'],
        [CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN]: ['c-token'],
        [CONTENTFUL_RE_CREATE_FEATURE_ENV]: ['c-re-create'],
    },
});

export const getArgv = (key: string): string => argv[key];
