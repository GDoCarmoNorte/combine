const APP_ID_REGEX = /\[APP_ID\]/g;
const APP_HOST_REGEX = /\[APP_HOST\]/g;
const SERVICE_HOST_REGEX = /\[SERVICE_HOST\]/g;
const NR_SERVICE_SUFFIX = /\[NEW_RELIC_SERVICE_SUFFIX\]/g;

const getWidgets = (widgetsString, environmentConfig) => {
    return [
        ...JSON.parse(
            widgetsString
                .replace(APP_ID_REGEX, environmentConfig.appId)
                .replace(APP_HOST_REGEX, environmentConfig.appHost)
                .replace(SERVICE_HOST_REGEX, environmentConfig.appServiceHost)
                .replace(NR_SERVICE_SUFFIX, environmentConfig.newRelicServiceSuffix)
        ),
    ];
};

const createEnvironmentPages = (pageTemplate, environmentConfig) => {
    const widgetsString = JSON.stringify(pageTemplate.widgets);

    return environmentConfig.map(({ name, config }) => ({
        ...pageTemplate,
        name,
        widgets: getWidgets(widgetsString, config),
    }));
};

const generateNRDashboardJSON = (brand, locale, template, environmentConfig, isRollout) => {
    const dashboardJSON = {
        ...template,
        name: `${brand.toUpperCase()} Order Ahead (${locale})`,
        description: `Order Ahead Webapp dashboard for brand: '${brand.toUpperCase()}' and locale: '${locale}'`,
        pages: createEnvironmentPages(template.pages[0], environmentConfig),
    };

    if (isRollout) {
        console.debug('Creating KPIs tabs for rollout...');
        const ENV_CONFIG = environmentConfig[0].config; //PROD
        console.debug('Using configuration...', ENV_CONFIG);

        const KPI_PAGE_APP_ID = ENV_CONFIG.appId;
        const KPI_PAGE_UI_HOSTNAME = ENV_CONFIG.appHost;
        const KPI_PAGE_SUFFIX = ENV_CONFIG.newRelicServiceSuffix;
        const KPI_WIDGET_NUMBER_WIDTH = 2;
        const KPI_WIDGET_SERIES_WIDTH = 10;
        const KPI_WIDGET_HEIGHT = 2;
        return {
            ...dashboardJSON,
            pages: [
                ...dashboardJSON.pages,
                {
                    name: `KPIs - Rollout (${KPI_PAGE_SUFFIX})`,
                    description: null,
                    widgets: [
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 1, height: 3, width: 3 },
                            title: 'Online Orders - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 4, row: 1, height: 3, width: 3 },
                            title: 'Online Orders - Total Amount',
                            rawConfiguration: {
                                dataFormatters: [
                                    {
                                        name: '$',
                                        precision: 2,
                                        type: 'decimal',
                                    },
                                ],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT sum(amount) as '$' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 7, row: 1, height: 3, width: 2 },
                            title: 'Online Orders (Pick up) - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND type = 'PickUp' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 9, row: 1, height: 3, width: 2 },
                            title: 'Online Orders (Delivery) - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND type = 'Delivery' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 11, row: 1, height: 3, width: 2 },
                            title: 'Online Orders (Loyalty) Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND isGuestUser IS TRUE COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 4, height: 3, width: 3 },
                            title: 'JS Errors - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) FROM JavaScriptError WHERE appId = ${KPI_PAGE_APP_ID} COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 4, row: 4, height: 3, width: 3 },
                            title: 'Backend - Requests w/ Errors - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Error Requests' FROM JavaScriptError WHERE errorMessage LIKE 'Error while invoking domain service%' and appId = ${KPI_PAGE_APP_ID} COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 7, row: 4, height: 3, width: 6 },
                            title: 'CDN - 404 error pages - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from Log WHERE ClientRequestHost like '%${KPI_PAGE_UI_HOSTNAME}' WHERE EdgeResponseStatus = 404 and ClientRequestPath not like '%.%' and ClientRequestPath not like '%exp-api%' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 7, height: 3, width: 3 },
                            title: 'Iframe (not displayed) - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'payment_iframe_not_displayed' COMPARE WITH 1 week ago`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 4, row: 7, height: 3, width: 3 },
                            title: 'Iframe (freedom pay hook) - Error Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from JavaScriptError WHERE errorMessage = 'payment_iframe_error' COMPARE WITH 1 week ago`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 7, row: 7, height: 3, width: 3 },
                            title: 'Error Boundary - Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'error_boundary' COMPARE WITH 1 week ago`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 10, row: 7, height: 3, width: 3 },
                            title: 'Unsupported Browser Page - compared w/ 1 week ago',
                            rawConfiguration: {
                                facet: { showOtherSeries: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'unsupported_browser' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 10, height: 3, width: 6 },
                            title: 'order-tally-service (tally) - Avg Duration (sec) - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT average(duration) from Transaction WHERE appName = 'order-tally-service-v3-${KPI_PAGE_SUFFIX}' WHERE name = 'WebTransaction/SpringController/TallyOrderControllerV3/validateAndCalculateTax' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 7, row: 10, height: 3, width: 6 },
                            title:
                                'order-management-service (saveAndExportOrder) - Avg Duration (sec) - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT average(duration) from Transaction WHERE appName = 'order-management-service-v3-${KPI_PAGE_SUFFIX}' WHERE name = 'WebTransaction/SpringController/order/validate (POST)' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 13, height: 3, width: 4 },
                            title: 'Page Views - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) from PageView WHERE appId = ${KPI_PAGE_APP_ID} compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 5, row: 13, height: 3, width: 4 },
                            title: 'Backend - Total Requests - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Total Requests' FROM PageAction WHERE appId = ${KPI_PAGE_APP_ID} and actionName like 'Domain api latency for %' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 9, row: 13, height: 3, width: 4 },
                            title: 'CDN traffic - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Requests' from Log WHERE ClientRequestHost = '${KPI_PAGE_UI_HOSTNAME}' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                    ],
                },
                {
                    name: `KPIs - Rollout (${KPI_PAGE_SUFFIX}) - More info`,
                    description: null,
                    widgets: [
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 1, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'JS Errors - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) FROM JavaScriptError WHERE appId = ${KPI_PAGE_APP_ID} COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 1, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'JS Errors - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                legend: { enabled: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) FROM JavaScriptError WHERE appId = ${KPI_PAGE_APP_ID} TIMESERIES 1 hour COMPARE WITH 1 week ago`,
                                    },
                                ],
                                yAxisLeft: { zero: true },
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 3, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Backend - Requests w/ Errors - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Error Requests' FROM JavaScriptError WHERE errorMessage LIKE 'Error while invoking domain service%' and appId = ${KPI_PAGE_APP_ID} COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 3, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'Backend - Requests w/ Errors - compared w/ 1 week ago',
                            rawConfiguration: {
                                legend: { enabled: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Error Requests' FROM JavaScriptError WHERE errorMessage LIKE 'Error while invoking domain service%' and appId = ${KPI_PAGE_APP_ID} TIMESERIES 1 HOUR COMPARE WITH 1 week ago`,
                                    },
                                ],
                                yAxisLeft: { zero: true },
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 5, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'CDN - 404 error pages - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from Log WHERE ClientRequestHost like '%${KPI_PAGE_UI_HOSTNAME}' WHERE EdgeResponseStatus = 404 and ClientRequestPath not like '%.%' and ClientRequestPath not like '%exp-api%' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 5, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'CDN - 404 error pages - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from Log WHERE ClientRequestHost like '%${KPI_PAGE_UI_HOSTNAME}' WHERE EdgeResponseStatus = 404 and ClientRequestPath not like '%.%' and ClientRequestPath not like '%exp-api%' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 9, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Iframe (not displayed) - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'payment_iframe_not_displayed' COMPARE WITH 1 week ago`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 9, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'Iframe (not displayed) - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'payment_iframe_not_displayed' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 11, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Iframe (freedom pay hook) - Error Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from JavaScriptError WHERE errorMessage = 'payment_iframe_error' COMPARE WITH 1 week ago`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 11, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'Iframe (freedom pay hook) - Error Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from JavaScriptError WHERE errorMessage = 'payment_iframe_error' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 13, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Error Boundary - Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'error_boundary' COMPARE WITH 1 week ago`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 13, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'Error Boundary - Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'error_boundary' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                thresholds: [],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 15, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Unsupported Browser Page - compared w/ 1 week ago',
                            rawConfiguration: {
                                facet: { showOtherSeries: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'unsupported_browser' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: { column: 4, row: 15, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_SERIES_WIDTH },
                            title: 'Unsupported Browser Page - compared w/ 1 week ago',
                            rawConfiguration: {
                                facet: { showOtherSeries: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'unsupported_browser' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 17, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'order-tally-service (tally) - Avg Duration (sec) - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT average(duration) from Transaction WHERE appName = 'order-tally-service-v3-${KPI_PAGE_SUFFIX}' WHERE name = 'WebTransaction/SpringController/TallyOrderControllerV3/validateAndCalculateTax' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_NUMBER_WIDTH,
                                row: 17,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'order-tally-service (tally) - Avg Duration (sec) - compared w/ 1 week ago',
                            rawConfiguration: {
                                legend: { enabled: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT average(duration) from Transaction WHERE appName = 'order-tally-service-v3-${KPI_PAGE_SUFFIX}' WHERE name = 'WebTransaction/SpringController/TallyOrderControllerV3/validateAndCalculateTax' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                yAxisLeft: { zero: true },
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 19, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title:
                                'order-management-service (saveAndExportOrder) - Avg Duration (sec) - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT average(duration) from Transaction WHERE appName = 'order-management-service-v3-${KPI_PAGE_SUFFIX}' WHERE name = 'WebTransaction/SpringController/order/validate (POST)' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_NUMBER_WIDTH,
                                row: 19,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title:
                                'order-management-service (saveAndExportOrder) - Avg Duration (sec) - compared w/ 1 week ago',
                            rawConfiguration: {
                                legend: { enabled: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT average(duration) from Transaction WHERE appName = 'order-management-service-v3-${KPI_PAGE_SUFFIX}' WHERE name = 'WebTransaction/SpringController/order/validate (POST)' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                yAxisLeft: { zero: true },
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 21, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Page Views - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) from PageView WHERE appId = ${KPI_PAGE_APP_ID} compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 21,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'Page Views - compared w/ 1 week ago',
                            rawConfiguration: {
                                legend: { enabled: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) from PageView WHERE appId = ${KPI_PAGE_APP_ID} TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                yAxisLeft: { zero: true },
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 23, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Backend - Total Requests - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Total Requests' FROM PageAction WHERE appId = ${KPI_PAGE_APP_ID} and actionName like 'Domain api latency for %' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 23,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'Backend - Total Requests - compared w/ 1 week ago',
                            rawConfiguration: {
                                legend: { enabled: true },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Total Requests' FROM PageAction WHERE appId = ${KPI_PAGE_APP_ID} and actionName like 'Domain api latency for %' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                                yAxisLeft: { zero: true },
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 25, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'CDN traffic - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Requests' from Log WHERE ClientRequestHost = '${KPI_PAGE_UI_HOSTNAME}' compare with 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 25,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'CDN traffic - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Requests' from Log WHERE ClientRequestHost = '${KPI_PAGE_UI_HOSTNAME}' TIMESERIES 1 HOUR COMPARE WITH 1 week AGO`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 27, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Online Orders - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 27,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'Online Orders - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' TIMESERIES 1 HOUR COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 29, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Online Orders (Pick up) - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND type = 'PickUp' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 29,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'Online Orders (Pick up) - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND type = 'PickUp' TIMESERIES 1 HOUR COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 31, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Online Orders (Delivery) - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND type = 'Delivery' COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 31,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'Online Orders (Delivery) - Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND type = 'Delivery' TIMESERIES 1 HOUR COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.billboard' },
                            layout: { column: 1, row: 33, height: KPI_WIDGET_HEIGHT, width: KPI_WIDGET_NUMBER_WIDTH },
                            title: 'Online Orders (Loyalty) Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND isGuestUser IS TRUE COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                        {
                            visualization: { id: 'viz.line' },
                            layout: {
                                column: 1 + KPI_WIDGET_SERIES_WIDTH,
                                row: 33,
                                height: KPI_WIDGET_HEIGHT,
                                width: KPI_WIDGET_SERIES_WIDTH,
                            },
                            title: 'Online Orders (Loyalty) Total Count - compared w/ 1 week ago',
                            rawConfiguration: {
                                dataFormatters: [],
                                facet: { showOtherSeries: false },
                                nrqlQueries: [
                                    {
                                        accountId: 2879290,
                                        query: `SELECT count(*) as 'Count' from PageAction WHERE appId = ${KPI_PAGE_APP_ID} WHERE actionName = 'order_placed' AND isGuestUser IS TRUE TIMESERIES 1 HOUR COMPARE WITH 1 week ago`,
                                    },
                                ],
                            },
                            linkedEntityGuids: null,
                        },
                    ],
                },
            ],
        };
    }

    return dashboardJSON;
};

module.exports = { generateNRDashboardJSON };
