import { createManageClient, getContentfulSpace, getContentfullEnv } from './contentfulClient';
import { Environment } from 'contentful-management/dist/typings/entities/environment';
import { Space } from 'contentful-management/dist/typings/entities/space';
import { MetaLinkProps } from 'contentful-management/dist/typings/common-types';
import { Entry } from 'contentful-management/dist/typings/entities/entry';
import { ContentType } from 'contentful-management/dist/typings/entities/content-type';
import { logPrefix } from './contentful.const';
import { logInit } from './cliLog';

const client = createManageClient();
const contentfulSpace = getContentfulSpace();
const contentfulEnv = getContentfullEnv();
const { log } = logInit(logPrefix);

export const addApiKeys = async (space: Space, env: string): Promise<void> => {
    const environment: { sys: MetaLinkProps } = {
        sys: {
            type: 'Link',
            linkType: 'Environment',
            id: env,
        },
    };

    const { items } = await space.getApiKeys();

    await Promise.all(
        items.map((key) => {
            log(` - Updating - ${key.sys.id} key`);
            key.environments.push(environment);
            return key.update();
        })
    );
};

export class ContentfulManagement {
    static _space: Space;

    static async getSpace(): Promise<Space> {
        if (!this._space) {
            log(` - Get space - ${contentfulSpace}`);
            this._space = await client.getSpace(contentfulSpace);
        }
        return this._space;
    }

    static async getEnvironment(id = contentfulEnv): Promise<Environment> {
        const space = await this.getSpace();

        return space.getEnvironment(id);
    }

    static async getEnvironments(): Promise<Environment[]> {
        const space = await this.getSpace();
        const res = await space.getEnvironments();

        return res.items;
    }

    static async getContentTypes(env?: string): Promise<ContentType[]> {
        const environment = await this.getEnvironment(env);
        const res = await environment.getContentTypes();

        return res.items;
    }

    static async getContentType(contentTypeId: string, contentEnv: string): Promise<ContentType> {
        const environment = await this.getEnvironment(contentEnv);

        return environment.getContentType(contentTypeId);
    }

    static async createContentType(id: string, name: string, contentEnv: string): Promise<ContentType> {
        const environment = await this.getEnvironment(contentEnv);
        return environment.createContentTypeWithId(id, {
            name,
            description: 'tmp description | waiting for update',
            fields: [],
        });
    }

    static async createProduct(productName: string): Promise<Entry> {
        const environment = await this.getEnvironment();

        return environment.createEntry('product', {
            fields: {
                name: { 'en-US': productName },
                productId: { 'en-US': productName },
            },
        });
    }

    static async createEnvironment(env: string, source: string): Promise<Environment> {
        const space = await this.getSpace();
        const environment = await space.createEnvironmentWithId(env, { name: env }, source);

        await addApiKeys(space, env);

        return environment;
    }

    static async destroyEnvironment(env: string): Promise<void> {
        const space = await this.getSpace();
        const environment = await space.getEnvironment(env);

        await environment.delete();
    }
}
