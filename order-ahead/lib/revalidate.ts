const SSG_DELAY = 10; // seconds

const { SSG } = process.env;

// console.log('\n[SSG]', 'server-side generation is:', SSG === 'true' ? 'ON' : 'OFF');

export default (): number | undefined => (SSG === 'true' ? SSG_DELAY : undefined);
