// Convert "012-345-6789" to "(012) 345-6789"
export function formatPhoneNumber(phoneNumber: string): string {
    const parts = phoneNumber.split('-');

    return parts.length > 2 ? `(${parts[0]}) ${parts[1]}-${parts[2]}` : phoneNumber;
}
