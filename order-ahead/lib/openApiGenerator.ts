import dotenv from 'dotenv';
import { resolve } from 'path';
import { exec } from 'child_process';
import { readFileSync, writeFileSync, existsSync } from 'fs';
import camelcase from 'camelcase';

dotenv.config();

const openApi = JSON.parse(String(readFileSync(resolve(process.cwd(), 'config/openapi.json'), { encoding: 'utf-8' })));

const generate = async (output, url, apis) => {
    const cwd = resolve(__dirname, '..');
    const path = resolve(__dirname, `../@generated/${output}`);
    const command = `npx openapi-generator-cli generate -i ${url} -g typescript-fetch -o ${path} --skip-validate-spec --model-name-suffix=Model --global-property models,supportingFiles,apis=${apis} --minimal-update --additional-properties=typescriptThreePlus=true,ensureUniqueParams=false`;

    console.log('[OPENAPI]', command);

    return new Promise((resolve) => {
        const cp = exec(command, { cwd });

        cp.stdout.on('data', (data) => {
            process.stdout.write(data);
        });
        cp.stderr.on('data', (data) => {
            process.stderr.write(data);
        });

        cp.on('exit', (code) => resolve(code));
    });
};

const fixRuntime = (key: string, url: string, output: string): void => {
    const path = resolve(__dirname, `../@generated/${output}`, 'runtime.ts');

    if (existsSync(path)) {
        let content = readFileSync(path, { encoding: 'utf-8' });

        content = content.replace(
            'window.fetch.bind(window)',
            "(typeof window === 'undefined' ? fetch : window.fetch.bind(window))"
        );
        content = content.replace(/BASE_PATH = "[^"]+"/, `BASE_PATH = (process.env.${key})`);

        writeFileSync(path, content);
        console.log('[OPENAPI]', `runtime.ts updated at ${path}`);
    }
};

const run = async (): Promise<void> => {
    for (const key in openApi) {
        const { URL, APIS } = openApi[key];
        const output = camelcase(key.replace('NEXT_PUBLIC', ''));
        const code = await generate(output, URL, APIS);

        if (code != 0) throw Error(`[OPENAPI] ${key} API generation failed!`);

        fixRuntime(key, openApi[key], output);
        console.log('[OPENAPI]', `${key} API generation: 'succeeded'`);
    }
};

console.log('\n[OPENAPI]', 'start api generation');

run()
    .then(() => {
        console.log('\n[OPENAPI]', 'api generation completed\n');
        process.exit(); // TODO: add exit code if required
    })
    .catch((e) => {
        console.error('\n[OPENAPI]', 'api generation incomplete\n', e);
        process.exit(-1);
    });
