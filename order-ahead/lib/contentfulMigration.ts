import util from 'util';
import { readdir, existsSync } from 'fs';
import path from 'path';

import chalk from 'chalk';
import { runMigration } from 'contentful-migration/built/bin/cli';
import { Environment } from 'contentful-management/dist/typings/entities/environment';
import { ContentType } from 'contentful-management/dist/typings/entities/content-type';
import { Space } from 'contentful-management/dist/typings/entities/space';

import { ContentfulManagement } from './contentfulManagement';
import { getContentfulMgmtAccessToken, getContentfulSpace } from './contentfulClient';
import {
    getArgv,
    CONTENTFUL_DEST_KEY,
    CONTENTFUL_RE_CREATE_FEATURE_ENV,
    CONTENTFUL_SPACE_KEY,
    CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN,
} from './cli';
import {
    initMigtarionFileName,
    logPrefix,
    migrationsDirName,
    migrationsPath,
    migrationsVersionTrackingContentType,
    nonFeatureEnvs,
} from './contentful.const';
import { logInit } from './cliLog';
import {
    getPreparedEnvironment,
    getVersionContentTypePayload,
    getVersionOfFile,
    reCreateQaEnvFromDev,
} from './contentfulMigration.utils';

const readdirAsync = util.promisify(readdir);
const { log, logWarning } = logInit(logPrefix);

(async () => {
    process.on('unhandledRejection', (reason, p) => {
        console.error('Unhandled Rejection at: Promise', p, 'reason:', reason);
        process.exitCode = 1;
        return;
    });

    const versionContentTypeId = 'versionTracking';
    const destination = getArgv(CONTENTFUL_DEST_KEY);
    const contentfulSpace = getArgv(CONTENTFUL_SPACE_KEY) || getContentfulSpace();
    const managementToken = getArgv(CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN) || getContentfulMgmtAccessToken();
    const isReCreateFeatureEnv = !!getArgv(CONTENTFUL_RE_CREATE_FEATURE_ENV);
    const isFeatureEnv = !nonFeatureEnvs.includes(destination);

    // ------------------------------------------------------------------------------

    log('Check migration directory and destination env value - START');

    if (!existsSync(migrationsPath)) {
        logWarning(' - ! migration directory does not exist, please run init script');
        process.exitCode = 1;
        return;
    }

    if (!destination) {
        logWarning(' - ! destination env is missing, please use "--d=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!contentfulSpace) {
        logWarning(' - ! contentful space is missing, please use "--s=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!managementToken) {
        logWarning(' - ! contentful management token is missing, please use "--c-token=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    log(' - DESTINATION ID: ', destination);
    log(' - SPACE ID: ', contentfulSpace);

    log('Check migration directory and destination env value - END');

    // ------------------------------------------------------------------------------

    log('Check migrations - START');

    log(' - Read all the available migrations from the file system');

    let space: Space = null;

    try {
        space = await ContentfulManagement.getSpace();
    } catch (e) {
        console.error(logPrefix, `Get contentful space (${contentfulSpace}) is failed. Exiting... Error details: `);
        console.error(e);
        process.exitCode = 1;
        return;
    }

    const migrationFiles = await readdirAsync(migrationsPath);
    const sorter = (a: string, b: string) => Number(a) - Number(b);

    const availableMigrations = migrationFiles.map((file) => getVersionOfFile(file)).sort(sorter);
    const migrationFilesMap = migrationFiles.reduce((acc, file) => ({ ...acc, [getVersionOfFile(file)]: file }), {});

    log(' - Figure out latest ran migration of the contentful space');

    // dev contentful env for some feature, qa and dev envs
    const destEnvName = isFeatureEnv ? 'dev' : destination;

    let destEnvironment: Environment = null;

    try {
        destEnvironment = await space.getEnvironment(destEnvName);
    } catch (e) {
        console.error(logPrefix, `Get destination environemnt (${destEnvName}) is failed. Exiting... Error details: `);
        console.error(e);
        process.exitCode = 1;
        return;
    }

    const defaultDestLocale = (await destEnvironment.getLocales()).items.find((locale) => locale.default).code;

    const { items: versions } = await destEnvironment.getEntries({
        content_type: versionContentTypeId,
    });

    const appliedMigrations = versions
        .map((item) => {
            return item?.fields?.version[defaultDestLocale];
        })
        .sort(sorter);

    log(' - Previously applied migrations: ', appliedMigrations.join(', '));

    const migrationsToRun = availableMigrations.filter((m) => !appliedMigrations.includes(m));

    log(' - Migrations to apply: ', migrationsToRun.join(', '));

    if (!migrationsToRun.length) {
        log(' - Nothing to migrate');
        return;
    }

    log('Check migrations - END');

    // ------------------------------------------------------------------------------

    if (destination === 'qa') {
        log('Re-create QA environemnt from DEV - START');

        try {
            await reCreateQaEnvFromDev(space);
        } catch (e) {
            console.error(logPrefix, 'Re-creation a new QA environemnt from DEV failed. Exiting... Error details: ');
            console.error(e);
            process.exitCode = 1;
            return;
        }

        log('Re-create QA environemnt from DEV - END');
        log('🎉 Successfully processed a new QA environment with new migrations! 🎉');

        return;
    }

    // ------------------------------------------------------------------------------

    log('Prepare new environemnt - START');

    let envId: string;
    let environment: Environment;

    try {
        const envData = await getPreparedEnvironment(space, destination, isReCreateFeatureEnv);
        environment = envData.environment;
        envId = envData.envId;
    } catch (e) {
        console.error(logPrefix, 'Preparation a new environemnt failed. Exiting... Error details: ');
        console.error(e);
        process.exitCode = 1;
        return;
    }

    log('Prepare new environemnt - END');

    // ------------------------------------------------------------------------------

    log(`Check and create "${chalk.white(versionContentTypeId)}" content type - START`);

    const isEmptyEnv = !(await environment.getContentTypes()).items.length;
    const defaultLocale = (await environment.getLocales()).items.find((locale) => locale.default).code;

    let versionContentType: ContentType;

    try {
        if (!isEmptyEnv) {
            versionContentType = await ContentfulManagement.getContentType(versionContentTypeId, envId);
        }
    } catch (e) {
        logWarning(` - Content type "${chalk.white(versionContentTypeId)}" not found`);
    }

    log(` - Creating "${chalk.white(versionContentTypeId)}" content type`);

    try {
        if (!versionContentType) {
            versionContentType = await environment.createContentTypeWithId(
                versionContentTypeId,
                migrationsVersionTrackingContentType
            );

            await versionContentType.publish();

            if (!isEmptyEnv) {
                const entry = await environment.createEntryWithId(
                    versionContentTypeId,
                    'migration0',
                    getVersionContentTypePayload(defaultLocale, '0', initMigtarionFileName)
                );

                await entry.publish();
            }
        } else {
            log(` - "${chalk.white(versionContentTypeId)}" content type already exist`);
        }
    } catch (e) {
        console.error(logPrefix, 'Content type creation failed. Exiting... Error details: ');
        console.error(e);
        process.exitCode = 1;
        return;
    }

    log(`Check and create "${chalk.white(versionContentTypeId)}" content type - END`);

    // ------------------------------------------------------------------------------

    log('Run migrations - START');

    const migrationOptions = {
        spaceId: contentfulSpace,
        environmentId: envId,
        accessToken: getContentfulMgmtAccessToken(),
        yes: true,
    };

    log(' - Run migrations and update version entry');

    let isMigrationSucceeded = true;

    const startMigration = async () => {
        for (let index = 0; index < migrationsToRun.length; index++) {
            const migration = migrationsToRun[index];
            const migrationFileName = migrationFilesMap[migration];
            const filePath = path.join(__dirname, '..', migrationsDirName, migrationFileName);

            log(' - Running: ', filePath);

            try {
                await runMigration({ ...migrationOptions, filePath });
            } catch (e) {
                console.error(logPrefix, 'Migration failed. Error details: ');
                console.error(e);
                process.exitCode = 1;
                break;
            }

            try {
                log(' - Updating migration version info');

                const entry = await environment.createEntryWithId(
                    versionContentTypeId,
                    `migration${migration}`,
                    getVersionContentTypePayload(defaultLocale, migration, migrationFileName)
                );

                await entry.publish();
            } catch (e) {
                console.error(logPrefix, 'Updating migration version info failed. Error details: ');
                console.error(e);
                process.exitCode = 1;
                isMigrationSucceeded = false;
                return;
            }

            log(` - Migration: "${chalk.white(migration)}" succeeded`);
        }
    };

    await startMigration();

    if (!isMigrationSucceeded) {
        return;
    }

    log('Run migrations - END');

    // ------------------------------------------------------------------------------

    log('Update alias - START');

    log('- Checking if we need to update an alias');

    if (!isFeatureEnv) {
        log('- Running on :', destination);
        log('- Updating alias: ', destination);

        try {
            await space
                .getEnvironmentAlias(destination)
                .then((alias) => {
                    // TODO remove comment, fix and check if it's working
                    // eslint-disable-next-line no-param-reassign
                    alias.environment.sys.id = envId;
                    return alias.update();
                })
                .then((alias) => log(`- Alias ${alias.sys.id} updated.`));
        } catch (e) {
            console.error(logPrefix, 'Updating alias failed. Error details: ');
            console.error(e);
            process.exitCode = 1;
            return;
        }
    } else {
        log('- Running on feature branch');
        log('- No alias changes required');
    }

    log('Update alias - END');

    // ------------------------------------------------------------------------------

    log('🎉 All done! 🎉');
})();
