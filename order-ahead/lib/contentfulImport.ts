import { exec } from 'child_process';

import { logPrefix } from './contentful.const';
import { logInit } from './cliLog';
import {
    getArgv,
    CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN,
    CONTENTFUL_DEST_KEY,
    CONTENTFUL_SPACE_KEY,
    CONTENTFUL_DUMP_FILE,
} from './cli';

const token = getArgv(CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN);
const dest = getArgv(CONTENTFUL_DEST_KEY);
const space = getArgv(CONTENTFUL_SPACE_KEY);
const file = getArgv(CONTENTFUL_DUMP_FILE);

const { log, logWarning } = logInit(logPrefix);

(async () => {
    log('DESTINATION ID: ', dest);
    log('SPACE ID: ', space);
    log('DUMP FILE: ', file);

    if (!token) {
        logWarning('! token is missing, please use "--c-token=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!space) {
        logWarning('! space key is missing, please use "--s=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!dest) {
        logWarning('! destination key is missing, please use "--d=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!file) {
        logWarning('! file is missing, please use "--f=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    const command = `contentful space import \
--management-token ${token} \
--space-id ${space} \
--environment-id ${dest} \
--content-file=export_data/${file}`;

    log('Running command: ', command);

    exec(command, (error, data) => {
        if (error) {
            console.error('error: ', error.message);
            process.exitCode = 1;
            return;
        }

        log('data log: ', data);
    });
})();
