import fetch from 'node-fetch';

import { AzureCliCredentials } from '@azure/ms-rest-nodeauth';
import keyVault from 'dotenv-keyvault';

const makeRequest = async (url, options): Promise<{ access_token: string }> => {
    const res = await fetch(url, options);

    if (res.status === 200) {
        const result = await res.json();

        return result;
    }

    throw new Error(`Invalid status code <${res.status} ${res.statusText}>'`);
};

const getAccessTokenRemote = async () => {
    const res = await makeRequest(
        'http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https://vault.azure.net',
        {
            method: 'GET',
            headers: {
                Metadata: 'true',
            },
        }
    );

    return res.access_token;
};

// Please make sure you have logged in via Azure CLI `az login` before executing this script.
const getAccessTokenCli = async () => {
    const credentials = await AzureCliCredentials.create({ resource: 'https://vault.azure.net' });

    console.log('[CONFIG]', 'Subscription associated with the access token: ', credentials.tokenInfo.subscription);

    return credentials.tokenInfo.accessToken;
};

const TOKEN_SOURCES = ['api', 'cli'];

function getAccessToken(source: string): Promise<string> {
    if (TOKEN_SOURCES.includes(source)) {
        return source === 'api' ? getAccessTokenRemote() : getAccessTokenCli();
    }

    throw new Error(`Unknown AKV_TOKEN_SOURCE: '${source}'`);
}

export const loadSecureKeys = async (
    source: string,
    config: { parsed?: { [key: string]: string } }
): Promise<{ [key: string]: string }> => {
    const parsed: { [key: string]: string } = config.parsed || {};
    const secureKeys = Object.keys(parsed).filter((key) => parsed[key].match(/^kv:/));

    console.log(
        '[CONFIG]',
        'Get properties from Azure KV: ',
        secureKeys === undefined || secureKeys.length == 0 ? 'N/A' : secureKeys.join(', ')
    );

    const vaultKeyClient = keyVault.config({
        aadAccessToken: secureKeys === undefined || secureKeys.length == 0 ? 'N/A' : getAccessToken.bind(null, source),
    });

    return vaultKeyClient(config);
};
