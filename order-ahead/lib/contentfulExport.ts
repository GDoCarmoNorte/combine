import { exec } from 'child_process';

import { logPrefix } from './contentful.const';
import { logInit } from './cliLog';
import { getArgv, CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN, CONTENTFUL_ENV_KEY, CONTENTFUL_SPACE_KEY } from './cli';

const token = getArgv(CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN);
const env = getArgv(CONTENTFUL_ENV_KEY);
const space = getArgv(CONTENTFUL_SPACE_KEY);
const { log, logWarning } = logInit(logPrefix);

(async () => {
    log('ENV ID: ', env);
    log('SPACE ID: ', space);

    if (!token) {
        logWarning('! token is missing, please use "--c-token=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!space) {
        logWarning('! space key is missing, please use "--s=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!env) {
        logWarning('! env key is missing, please use "--e=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    const command = `contentful space export \
--management-token ${token} \
--space-id ${space} \
--environment-id ${env} \
--skip-webhooks \
--skip-roles \
--download-assets \
--export-dir=export_data`;

    log('Running command: ', command);

    exec(command, (error, data) => {
        if (error) {
            console.error('error: ', error.message);
            process.exitCode = 1;
            return;
        }

        log('data log: ', data);
    });
})();
