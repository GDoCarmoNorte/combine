import setFeatureFlags from '../webpack/setFeatureFlags';
import dotenv from 'dotenv';

dotenv.config();

const FEATURE_FLAGS = JSON.parse(setFeatureFlags({ plugins: [] }).plugins[0].definitions.FEATURE_FLAGS);

globalThis.FEATURE_FLAGS = FEATURE_FLAGS;
