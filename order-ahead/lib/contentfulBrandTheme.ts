import fs from 'fs';

import { getContentfullEnv, getContentfulSpace } from './contentfulClient';
import { ContentfulDelivery } from './contentfulDelivery';

const contentfulEnv = getContentfullEnv();
const contentfulSpace = getContentfulSpace();

(async () => {
    const brand = process.env.NEXT_PUBLIC_BRAND;
    console.log(
        '[CONTENTFUL]',
        `generating theme from env: "${contentfulEnv}" - space: "${contentfulSpace}" - brand: "${brand}"`
    );

    const brandThemes = await ContentfulDelivery().getBrandTheme();
    const brandTheme = brandThemes.items[0];

    const colorNames = Object.keys(brandTheme.fields)
        .filter((n) => n !== 'name')
        .map((colorName) => {
            const colorValue = brandTheme.fields[colorName].fields.hexColor;
            return `  --col--${colorName}: #${colorValue};`;
        })
        .join('\n');

    fs.writeFileSync(
        `./public/brands/${brand}/theme.css`,
        `
body {
${colorNames}
}
`
    );

    console.log('[CONTENTFUL]', 'theme is generated.');
})();
