export function spaces2underscores(text = ''): string {
    return text.replace(/ /gi, '_');
}

export function getGtmIdByName(componentName = '', mainText = ''): string {
    return `${componentName}${componentName && mainText && '-'}${spaces2underscores(mainText)}`;
}
