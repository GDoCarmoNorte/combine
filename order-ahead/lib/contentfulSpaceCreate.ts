import { exec } from 'child_process';

import { logPrefix } from './contentful.const';
import { logInit } from './cliLog';
import { getArgv, CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN, CONTENTFUL_SPACE_KEY } from './cli';

export const contentfulSpaceCreate = (): void => {
    const token = getArgv(CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN);
    const space = getArgv(CONTENTFUL_SPACE_KEY);
    const { log, logWarning } = logInit(logPrefix);

    log('SPACE ID: ', space);

    if (!token) {
        logWarning('! token is missing, please use "--c-token=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    if (!space) {
        logWarning('! space key is missing, please use "--s=xxx" param to provide');
        process.exitCode = 1;
        return;
    }

    const command = `contentful space create \
--management-token ${token} \
--name ${space}`;

    log('Running command: ', command);

    exec(command, (error, data) => {
        if (error) {
            console.error('error: ', error.message);
            process.exitCode = 1;
            return;
        }

        log('data log: ', data);
    });
};

contentfulSpaceCreate();
