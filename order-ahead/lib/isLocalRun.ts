const isLocalRun = () => window?.location?.hostname === 'localhost';

export default isLocalRun;
