# Testing Strategy

## Layers

### Layer #1 - UNIT

-   to be done for common/shared/reusable pieces of code
-   only 1 method/function under test
-   mock dependencies

#### Examples

    /common
    /components/atoms
    /lib
    /redux

### Layer #2 - INTEGRATION

-   to be done for sections of a page or a whole page
-   more than 1 component interacting together
-   mock dependencies such as **`redux store`** , **`domain services`** and **`contentful`**. More info [below](#mock-dependencies)

#### Examples

    /pages
    /organisms/navigation/bag

## Tools

### Jest

`Jest` is being used as a JavaScript testing framework. It acts as a test runner, assertion library, and mocking library.

Jest also provides snapshot testing functionality, it allows to create a rendered snapshot of a component and compare it to a previously saved snapshot. The test will pass if snapshots match, otherwise it will fail. Snapshots have .test.js.snap or .spec.js.snap suffixes and they are collected inside the **snapshots** directory which is created inside the same folder as the test file.

### React Testing Library

The `React Testing Library` is a very light-weight solution for testing React components. It provides light utility functions on top of react-dom and react-dom/test-utils, in a way that encourages better testing practices.

> Visit https://testing-library.com/docs/react-testing-library/intro/ for more information

### Enzyme

> This is being deprecated in favor of React Testing library

Enzyme is a JavaScript testing utility for React applications that makes it easier to assert, manipulate, and traverse your React Components’ output.

## Writing tests

### Filenames

-   Folder structure of the test should mimic the folder structure of the file being tested.
-   test files should be placed inside <rootDir>/tests directory.
-   Unit test files should have .test.ts(x) suffix.
-   Integration test files should have .integration.test.tsx suffix.

### Guidelines for unit tests

-   do NOT abuse of snapshots.
-   use snapshots for specific cases and amount of code. Avoid huge snapshots.
-   if component renders nothing/null, do NOT use snapshots. Instead write the expectation against empty/null.

### Guidelines for integrations tests

-   refer to existing integration tests.
-   include one `a11y` test per file (only at page level, not at component level).
-   use react testing library queries to look for elements/text on the page - [Cheatsheet | Testing Library](https://testing-library.com/docs/react-testing-library/cheatsheet/#queries)
-   avoid `ByTestId` queries.
-   avoid importing queries from `render` method:

    -   non-preferred:

              const { getByText } = render(<LogoutButton className="randomClassName" />);
              expect(getByText(/logout/i)).toHaveClass('randomClassName');

    -   preferred:

              expect(screen.getByText(/logout/i)).toHaveClass('randomClassName');

-   **favor `getBy` instead of `queryBy` queries:**

    -   non-preferred:

              expect(screen.queryByText(/Total/i)).toBeInTheDocument();

    -   preferred:

              screen.getByText(/Total/i)

-   **use regex instead of specific text to account for differences in case:**

    -   non-preferred:

              screen.getByText('Total')

    -   preferred:

              screen.getByText(/Total/i)

-   **specify what element to look for:**

    -   non-preferred:

              screen.getByRole('link')

    -   preferred:

              screen.getByRole('link', {name: /menu category/i })

-   **specify what container to look from:**

    -   non-preferred:

              screen.getByText(/1080 calories/i)

    -   preferred:

              const doubleRoastBeefMealContainer = screen.getByLabelText(/^Double Roast Beef Meal/i);
              within(doubleRoastBeefMealContainer).getByText(/1080 calories/i);

#### Mock dependencies:

-   **`redux store`** (navigate to the page where the component is used or page to be tested and grab a copy/snapshot of current redux store. Create a file with this content and use it on the test file)

-   **`domain services`** (use `jest-fetch-mock` to mock these service calls - package already setup at `jest/setupTests.js`). Specific endpoints need to be mocked as follows:

```js
beforeAll(() => {
    fetch.mockResponse((req) => {
        if (
            req.url ===
            'https://maps.googleapis.com/maps/api/geocode/json?address=47586&key=AIzaSyAMCSHXxIq3SpVQxB-IaM5brHPz4qfEM9o'
        )
            return Promise.resolve(JSON.stringify({ results: [{ geometry: { location: { lat: 10, lng: 20 } } }] }));
        if (req.url.match(/\/domainLocationPath\/apis\/v1\.0\/brand\/ARBYS\/location\/latitude\/*/))
            return Promise.resolve(JSON.stringify([mockStoreValues.location]));

        return Promise.reject(new Error(`URL not mocked! - ${req.url}`));
    });
});
```

-   **`contentful`** (grab a copy/snapshot of contentful responses and create a file for each response and use it on the test file. See tests/mock-data/server/mockServer.ts )
    -   mock contentful calls using `msw` package (already setup at `tests/mock-data/server/mockServer.ts`)
    -   should the mock file not exist yet, it'll need to be created. Open the following file -`node_modules/contentful/dist/es-modules/create-contentful-api.js-` (`getEntry` function) and add the new line to write the response to a file.
    -   move the file generated above to `tests/mock-data/contentful`.
    -   update the `tests/mock-data/server/mockServer.ts` file with the new mock response.

```js
return this.getEntries(
    _objectSpread(
        {
            'sys.id': id,
        },
        query
    )
).then(function (response) {
    fs.writeFileSync(`sys.id-${id}.json`, JSON.stringify(response)); // ---> add this line
    if (response.items.length > 0) {
        return wrapEntry(response.items[0]);
    }
});
```
