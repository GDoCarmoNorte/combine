import { getGtmIdByName } from '../../../lib/gtm';

describe('getGtmIdByName', () => {
    it('should return gtm id with correct format', () => {
        expect(getGtmIdByName('secondaryBanner', '2 for $6 Everyday value')).toBe(
            'secondaryBanner-2_for_$6_Everyday_value'
        );
    });

    it('should return gtm id with correct format for different text', () => {
        expect(getGtmIdByName('mainProductBanner', "Double Beef 'N Cheddar")).toEqual(
            "mainProductBanner-Double_Beef_'N_Cheddar"
        );
    });

    it('should return correct gtm id with empty main text', () => {
        expect(getGtmIdByName('mainProductBanner', '')).toEqual('mainProductBanner');
    });

    it('should return correct gtm id with empty component name', () => {
        expect(getGtmIdByName('', 'Double Beef')).toEqual('Double_Beef');
    });

    it('should return correct gtm id without main text', () => {
        expect(getGtmIdByName('Component', undefined)).toEqual('Component');
    });

    it('should return empty string if component name and main text is empty string', () => {
        expect(getGtmIdByName('', '')).toEqual('');
    });

    it('should return empty string if component name and main text does not exist', () => {
        expect(getGtmIdByName(undefined, undefined)).toEqual('');
    });
});
