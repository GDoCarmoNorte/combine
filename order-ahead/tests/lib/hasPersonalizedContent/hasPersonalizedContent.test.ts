import hasPersonalizedContent from '../../../lib/hasPersonalizedContent';

const actionSectionMock = { sys: { contentType: { sys: { id: 'action' } } } } as any;
const nonActionSectionMock = { sys: { contentType: { sys: { id: 'mainBanner' } } } } as any;

describe('hasPersonalizedContent', () => {
    it('should return true if sections has "action" section', () => {
        const result = hasPersonalizedContent([nonActionSectionMock, actionSectionMock]);

        expect(result).toBe(true);
    });

    it('should return false if sections do not have "action" section', () => {
        const result = hasPersonalizedContent([nonActionSectionMock]);

        expect(result).toBe(false);
    });

    it('should return false if sections do not provided', () => {
        const result = hasPersonalizedContent(undefined);

        expect(result).toBe(false);
    });
});
