import { gtmServicePush } from '../../../lib/gtmServicePush';
import { MAX_PAYLOAD_SIZE } from '../../../common/services/gtmService/constants';

describe('gtmServicePush', () => {
    const mockEvent = 'mockEvent';
    const gtmServicePushMock = jest.fn(gtmServicePush);
    const push = jest.fn();

    beforeEach(() => {
        (window as any).dataLayer = { push };
        push.mockReset();
    });

    it('should log error message', () => {
        const consoleWArnMessageMock = {
            error: { message: 'GTM max payload size exceed for event mockEvent' },
        };
        const tooBigPayload = { mockData: new Array(MAX_PAYLOAD_SIZE + 1) };
        const consoleWarnSpy = jest.spyOn(console, 'warn');

        gtmServicePushMock(mockEvent, tooBigPayload);

        expect(consoleWarnSpy).toBeCalledWith(consoleWArnMessageMock);
        consoleWarnSpy.mockRestore();
    });

    it('should push the object with data and event to dataLayer', () => {
        const data = { mockData: 'mockData' };

        gtmServicePushMock(mockEvent, data);

        expect((window as any).dataLayer.push.mock.calls[0][0]).toEqual({ ...data, event: mockEvent });
    });
});
