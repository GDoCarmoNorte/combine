import fs from 'fs';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';

jest.mock('fs', () => ({
    existsSync: jest.fn(),
    writeFileSync: jest.fn(),
    mkdirSync: jest.fn(),
}));

jest.mock('../../../lib/contentfulDelivery', () => ({
    ContentfulDelivery: jest.fn(),
}));

describe('contentfulBrandTheme function', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should generate "./public/arb/theme.css" file with color names from contentful', async () => {
        (ContentfulDelivery as jest.Mock).mockReturnValueOnce({
            getBrandTheme: jest.fn().mockReturnValueOnce({
                items: [
                    {
                        fields: {
                            primary: { fields: { hexColor: 'D71920' } },
                            primaryVariant: { fields: { hexColor: 'AC1319' } },
                            secondary: { fields: { hexColor: 'D71920' } },
                            secondaryVariant: { fields: { hexColor: 'AC131' } },
                        },
                    },
                ],
            }),
        });

        await require('../../../lib/contentfulBrandTheme');

        expect((fs.writeFileSync as jest.Mock).mock.calls[0][0]).toEqual('./public/brands/arbys/theme.css');
        expect((fs.writeFileSync as jest.Mock).mock.calls[0][1]).toMatchSnapshot();
    });
});
