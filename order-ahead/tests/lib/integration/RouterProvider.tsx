import React, { FC, useState, useCallback } from 'react';
import { RouterContext } from 'next/dist/shared/lib/router-context';
import { parseRoute } from './utils';
import { NextRouter } from 'next/router';
import { UrlObject } from 'url';
import { IPageInfo } from './getPageInfo';

interface IRouterProviderProps {
    children: JSX.Element;
    pageInfo: IPageInfo;
}

type Url = string | UrlObject;

export const RouterProvider: FC<IRouterProviderProps> = ({ children: initialChildren, pageInfo }) => {
    const [children] = useState(initialChildren);

    const pushHandler = useCallback((route: Url) => {
        setRouter(makeRouterFromRoute(route, pushHandler));
        return Promise.resolve(true);
    }, []);

    const [router, setRouter] = useState(makeRouterFromRoute(pageInfo.route, pushHandler));

    return <RouterContext.Provider value={router}>{children}</RouterContext.Provider>;
};

const makeRouterFromRoute = (route: Url, pushHandler: (r: Url) => Promise<boolean>): NextRouter => {
    const { query, pathname } = parseRoute(route as string);

    return { ...makeDefaultRouterMock(), query, pathname: pathname, asPath: route as string, push: pushHandler };
};

export function makeDefaultRouterMock(): NextRouter {
    const routerMock: NextRouter = {
        basePath: '',
        pathname: '/',
        route: '/',
        asPath: '/',
        query: {},
        push: async () => {
            return true;
        },
        replace: async () => {
            return true;
        },
        reload: () => undefined,
        back: () => undefined,
        prefetch: async () => undefined,
        beforePopState: () => undefined,
        events: {
            on: () => undefined,
            off: () => undefined,
            emit: () => undefined,
        },
        isFallback: false,
        isLocaleDomain: false,
        isReady: true,
        isPreview: false,
    };

    return routerMock;
}
