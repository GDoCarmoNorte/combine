import React from 'react';
import { render } from '@testing-library/react';
import { getPagesDir } from './utils';
import { RouterProvider } from './RouterProvider';
import { getPageInfo } from './getPageInfo';

interface IGetPageFunc {
    (args: { route: string }): Promise<{
        render: () => ReturnType<typeof render>;
    }>;
}

export const getPage: IGetPageFunc = async ({ route }) => {
    if (!route.startsWith('/')) {
        throw new Error('"route" option should start with "/"');
    }

    const pageInfo = await getPageInfo(route);
    const pageModule = await import(`${getPagesDir()}/${pageInfo.pagePath}`);
    const context = { params: pageInfo.params, preview: false };

    const getStaticProps = pageModule.getStaticProps;
    const Component = pageModule.default;
    const App = (await import(`${getPagesDir()}/_app`)).default;

    let props = {};
    if (getStaticProps) {
        props = (await getStaticProps(context)).props;
    }

    return {
        render: () => {
            document.head.innerHTML = '';
            return render(
                <RouterProvider pageInfo={pageInfo}>{<App Component={Component} pageProps={props} />}</RouterProvider>
            );
        },
    };
};
