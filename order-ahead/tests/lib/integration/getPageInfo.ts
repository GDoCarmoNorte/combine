import normalizePath from 'normalize-path';
import { recursiveReadDir } from 'next/dist/lib/recursive-readdir';
import { getSortedRoutes } from 'next/dist/shared/lib/router/utils/sorted-routes';
import path from 'path';
import { getPagesDir, pagePathToRouteRegex, makeParamsObject, parseRoute } from './utils';

export interface IPageInfo {
    pagePath: string;
    params: Record<string, string | string[]>;
    route: string;
}
export interface IGetPageInfoFunc {
    (route: string): Promise<IPageInfo>;
}

export const getPageInfo: IGetPageInfoFunc = async (route: string) => {
    const pageExtensions = ['tsx'];
    const extensionsRegex = new RegExp(`.(${pageExtensions.join('|')})$`);
    const { pathname } = parseRoute(route);
    const pagesDir = path.normalize(getPagesDir());

    const files = (await recursiveReadDir(pagesDir, new RegExp('.+')))
        .map((path) => normalizePath(path))
        .filter((filePath) => filePath.match(extensionsRegex))
        .map((filePath) => filePath.replace(extensionsRegex, ''));

    const pagePaths = getSortedRoutes(files);

    for (const pagePath of pagePaths) {
        const { regex: pagePathRegex, paramTypes } = pagePathToRouteRegex(pagePath);
        const result = pathname.match(pagePathRegex);

        if (result) {
            return {
                pagePath,
                params: makeParamsObject({ routeRegexCaptureGroups: result.groups, paramTypes }),
                route,
            };
        }
    }

    throw new Error(`Page not found for route ${route}`);
};
