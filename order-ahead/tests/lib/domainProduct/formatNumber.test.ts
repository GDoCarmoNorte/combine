import { formatNumber } from '../../../lib/domainProduct';

describe('formatNumber', () => {
    it('should return number with commas as thousands separators', () => {
        const result = formatNumber(2500000);

        expect(result).toBe('2,500,000');
    });

    it('should return the same number', () => {
        const result = formatNumber(100);

        expect(result).toBe('100');
    });
});
