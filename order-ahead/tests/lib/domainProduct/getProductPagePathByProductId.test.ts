import { getProductPagePathByProductId } from '../../../lib/domainProduct';

const productDetailsPagePaths = [
    {
        productPath: '/menu/chicken/chicken-bacon-swiss-sandwich',
        canonicalPath: 'localhost:3000/menu/chicken/chicken-bacon-swiss-sandwich',
        menuCategoryUrl: 'meals',
        menuCategoryId: 'menuCagegory1',
        productDetailsPageUrl: 'chicken-bacon-swiss-sandwich',
        productIds: ['ARBYS-WEBOA-640213044'],
    },
    {
        productPath: '/menu/chicken/roast-chicken-bacon-swiss-sandwich',
        canonicalPath: 'localhost:3000/menu/chicken/roast-chicken-bacon-swiss-sandwich',
        menuCategoryUrl: 'meals',
        menuCategoryId: 'menuCagegory1',
        productDetailsPageUrl: 'roast-chicken-bacon-swiss-sandwich',
        productIds: ['ARBYS-WEBOA-123456789'],
    },
    {
        productPath: '/menu/chicken/classic-crispy-chicken-sandwich',
        canonicalPath: 'localhost:3000/menu/chicken/classic-crispy-chicken-sandwich',
        menuCategoryUrl: 'meals',
        menuCategoryId: 'menuCagegory1',
        productDetailsPageUrl: 'classic-crispy-chicken-sandwich',
        productIds: ['ARBYS-WEBOA-640212522'],
    },
];

describe('domainProduct', () => {
    describe('getProductPagePathByProductId', () => {
        it('should return productDetailsPagePath for existing productId', () => {
            const productId = 'ARBYS-WEBOA-123456789';
            const productPagePath = getProductPagePathByProductId(productId, productDetailsPagePaths);

            expect(productPagePath).toEqual({
                canonicalPath: 'localhost:3000/menu/chicken/roast-chicken-bacon-swiss-sandwich',
                productPath: '/menu/chicken/roast-chicken-bacon-swiss-sandwich',
                menuCategoryUrl: 'meals',
                menuCategoryId: 'menuCagegory1',
                productDetailsPageUrl: 'roast-chicken-bacon-swiss-sandwich',
                productIds: ['ARBYS-WEBOA-123456789'],
            });
        });

        it('should return undefined for non-existing productId', () => {
            const productId = 'ARBYS-WEBOA-0';
            const productPagePath = getProductPagePathByProductId(productId, productDetailsPagePaths);

            expect(productPagePath).toEqual(undefined);
        });

        it('should return undefined for empty productId', () => {
            const productId = '';
            const productPagePath = getProductPagePathByProductId(productId, productDetailsPagePaths);

            expect(productPagePath).toEqual(undefined);
        });
    });
});
