import { NO_SAUCE_CATEGORY_ID } from '../../../common/constants/product';
import { getModifierCardType } from '../../../lib/domainProduct';
import { IDomainProductItem, ModifierCardSelectorType } from '../../../redux/types';

describe('getModifierCardType', () => {
    it('should return initial card type if category ID does not contatin "No Sauce"', () => {
        const domainProduct: IDomainProductItem = {
            isSaleable: false,
            categoryIds: ['1', '2'],
        };
        const selectorType: ModifierCardSelectorType = 'quantity';
        const result = getModifierCardType(domainProduct, selectorType);

        expect(result).toBe('quantity');
    });

    it('should return "default" card type if category ID contatins "No Sauce"', () => {
        const domainProduct: IDomainProductItem = {
            isSaleable: false,
            categoryIds: [NO_SAUCE_CATEGORY_ID],
        };
        const selectorType: ModifierCardSelectorType = 'quantity';
        const result = getModifierCardType(domainProduct, selectorType);

        expect(result).toBe('default');
    });
});
