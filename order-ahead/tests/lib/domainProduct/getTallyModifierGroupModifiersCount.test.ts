import { getTallyModifierGroupModifiersCount } from '../../../lib/domainProduct';

describe('getTallyModifierGroupModifiersCount', () => {
    const mock = {
        modifiers: [
            {
                quantity: 3,
            },
            {
                quantity: undefined,
            },
            {
                quantity: 2,
            },
        ],
    } as any;

    it('should return correct result', () => {
        expect(getTallyModifierGroupModifiersCount(mock)).toEqual(5);
    });

    it('should return 0 for undefined group', () => {
        expect(getTallyModifierGroupModifiersCount(undefined)).toEqual(0);
    });

    it('should return 0 for undefined modifiers', () => {
        expect(getTallyModifierGroupModifiersCount({ modifiers: undefined } as any)).toEqual(0);
    });
});
