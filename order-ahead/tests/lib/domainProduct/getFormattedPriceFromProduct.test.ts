import { LocationWithDetailsModel } from '../../../common/services/locationService/types';
import { getFormattedPriceFromProduct } from '../../../lib/domainProduct';
import domainProductMock from '../../mocks/domainProduct.mock';
import locationsMock from '../../mocks/expLocations.mock';

describe('getFormattedPriceFromProduct', () => {
    const locationMock = (locationsMock.locations[0] as unknown) as LocationWithDetailsModel;

    it('should return null if product is not provided', () => {
        const result = getFormattedPriceFromProduct(null, locationMock);

        expect(result).toBe(null);
    });

    it('should return null if price is 0', () => {
        const result = getFormattedPriceFromProduct({ price: { currentPrice: 0 } } as any, locationMock);

        expect(result).toBe(null);
    });

    it('should return formatted price', () => {
        const result = getFormattedPriceFromProduct(domainProductMock, locationMock);

        expect(result).toBe('$6.99');
    });
});
