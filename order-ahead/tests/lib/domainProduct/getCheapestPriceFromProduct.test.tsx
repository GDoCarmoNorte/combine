import { OtherPriceTypeEnumModel } from '../../../@generated/webExpApi';
import { LocationWithDetailsModel } from '../../../common/services/locationService/types';
import { getCheapestPriceFromProduct } from '../../../lib/domainProduct';
import domainProductMock from '../../mocks/domainProduct.mock';
import locationsMock from '../../mocks/expLocations.mock';

describe('getCheapestPriceFromProduct', () => {
    const locationMock = (locationsMock.locations[0] as unknown) as LocationWithDetailsModel;

    it('should return null if product is not provided', () => {
        const result = getCheapestPriceFromProduct(null, null);

        expect(result).toBe(null);
    });

    it('should return null if price is not provided', () => {
        const result = getCheapestPriceFromProduct({ ...domainProductMock, price: null }, locationMock);

        expect(result).toBe(null);
    });

    it('should return price for meal', () => {
        const result = getCheapestPriceFromProduct(
            {
                ...domainProductMock,
                hasChildItems: true,
                price: {
                    currentPrice: 0,
                    otherPrices: {
                        MEAL: {
                            price: 7.09,
                            priceType: OtherPriceTypeEnumModel.Meal,
                        },
                    },
                },
            },
            locationMock
        );

        expect(result).toBe(7.09);
    });

    it('should return price for promo', () => {
        const result = getCheapestPriceFromProduct(
            {
                ...domainProductMock,
                hasChildItems: false,
                price: {
                    currentPrice: 10.09,
                    otherPrices: {
                        PROMO: {
                            price: 7.09,
                            priceType: OtherPriceTypeEnumModel.Promo,
                        },
                    },
                },
            },
            locationMock
        );

        expect(result).toBe(7.09);
    });

    it('should return regular price for happy hour if data is not within time range', () => {
        const result = getCheapestPriceFromProduct(
            {
                ...domainProductMock,
                hasChildItems: false,
                price: {
                    currentPrice: 10.09,
                    otherPrices: {
                        HAPPY_HOUR: {
                            price: 7.29,
                            priceType: OtherPriceTypeEnumModel.Promo,
                        },
                    },
                },
            },
            locationMock
        );

        expect(result).toBe(10.09);
    });

    it('should return price for standalone product', () => {
        const result = getCheapestPriceFromProduct(domainProductMock, locationMock);

        expect(result).toBe(6.99);
    });
});
