import { getFormattedExtrasModifications } from '../../../lib/domainProduct';

describe('getFormattedExtrasModifications', () => {
    it('should return empty string', () => {
        const selectedExtarsMock = [];

        const result = getFormattedExtrasModifications(selectedExtarsMock);

        expect(result).toBe('');
    });

    it('should return one added extras item', () => {
        const selectedExtarsMock = [
            {
                name: 'Regular Cheddar Cheese Curds',
                price: 5.79,
            },
        ];

        const result = getFormattedExtrasModifications(selectedExtarsMock);

        expect(result).toBe('Regular Cheddar Cheese Curds (+$5.79)');
    });

    it('should return two added extras items', () => {
        const selectedExtarsMock = [
            {
                name: 'Regular Cheddar Cheese Curds',
                price: 5.79,
            },
            {
                name: 'Chocolate Fudge Cake',
                price: 6,
            },
        ];

        const result = getFormattedExtrasModifications(selectedExtarsMock);

        expect(result).toBe('Regular Cheddar Cheese Curds (+$5.79), Chocolate Fudge Cake (+$6.00)');
    });
});
