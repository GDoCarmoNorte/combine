import { getDisplayModifierGroupDefaultModifiersCount } from '../../../lib/domainProduct';

describe('getDisplayModifierGroupDefaultModifiersCount', () => {
    const mock = {
        modifiers: [
            {
                displayProductDetails: {
                    defaultQuantity: 3,
                },
            },
            {
                displayProductDetails: {
                    defaultQuantity: undefined,
                },
            },
            {
                displayProductDetails: {
                    defaultQuantity: 2,
                },
            },
        ],
    } as any;

    it('should return correct result', () => {
        expect(getDisplayModifierGroupDefaultModifiersCount(mock)).toEqual(5);
    });

    it('should return 0 for undefined group', () => {
        expect(getDisplayModifierGroupDefaultModifiersCount(undefined)).toEqual(0);
    });

    it('should return 0 for undefined modifiers', () => {
        expect(getDisplayModifierGroupDefaultModifiersCount({ modifiers: undefined } as any)).toEqual(0);
    });
});
