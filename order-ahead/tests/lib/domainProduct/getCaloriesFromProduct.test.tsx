import { getCaloriesFromProduct } from '../../../lib/domainProduct';
import domainProductMock from '../../mocks/domainProduct.mock';

describe('getCaloriesFromProduct', () => {
    it('should return null if product is not provided', () => {
        const result = getCaloriesFromProduct(null);

        expect(result).toBe(null);
    });

    it('should return null if nutrition is not provided', () => {
        const result = getCaloriesFromProduct(domainProductMock);

        expect(result).toBe(null);
    });

    it('should return null if total calories is not provided', () => {
        const result = getCaloriesFromProduct({ ...domainProductMock, nutrition: {} });

        expect(result).toBe(null);
    });

    it('should return calories', () => {
        const result = getCaloriesFromProduct({ ...domainProductMock, nutrition: { totalCalories: 630 } });

        expect(result).toBe(630);
    });
});
