import { getFormattedModifications } from '../../../lib/domainProduct';
import { ModifierGroupType } from '../../../redux/types';

describe('getFormattedModifications', () => {
    it('should return empty string', () => {
        const added: any = [];

        const removed: any = [];

        const result = getFormattedModifications(added, removed);

        expect(result).toBe('');
    });

    it('should return added and removed modifiers', () => {
        const added: any = [
            {
                name: 'Ketchup Packet',
            },
        ];

        const removed: any = [
            {
                name: 'Pepper Bacon',
            },
        ];

        const result = getFormattedModifications(added, removed);

        expect(result).toBe('Add Ketchup Packet, remove Pepper Bacon');
    });

    it('should return added and removed modifiers, many', () => {
        const added: any = [
            {
                name: 'Ketchup',
            },
            {
                name: 'Cheddar',
            },
            {
                name: 'Parmesan',
            },
        ];

        const removed: any = [
            {
                name: 'Pepper',
            },
            {
                name: 'Sauce',
            },
        ];

        const result = getFormattedModifications(added, removed);

        expect(result).toBe('Add Ketchup, add Cheddar, add Parmesan, remove Pepper, remove Sauce');
    });

    it('should return only added modifiers', () => {
        const added: any = [
            {
                name: 'Ketchup Packet',
            },
        ];

        const removed: any = [];

        const result = getFormattedModifications(added, removed);

        expect(result).toBe('Add Ketchup Packet');
    });

    it('should return only added modifiers including quantity', () => {
        const added: any = [
            {
                name: 'Ketchup Packet',
                quantity: 5,
            },
        ];

        const removed: any = [];

        const includeQuantity = true;

        const result = getFormattedModifications(added, removed, includeQuantity);

        expect(result).toBe('Add Ketchup Packet x 5');
    });

    it('should return only removed modifiers', () => {
        const added: any = [];

        const removed: any = [
            {
                name: 'Pepper Bacon',
            },
        ];

        const result = getFormattedModifications(added, removed);

        expect(result).toBe('Remove Pepper Bacon');
    });

    it('should return modifiers when selection was changed', () => {
        const added: any = [
            {
                name: 'Tartar Sauce',
                quantity: 1,
                productId: 'arb-itm-009-030',
                price: 0,
                selection: 'Extra',
                relatedSelections: ['arb-itm-009-027', 'arb-itm-009-028', 'arb-itm-009-029'],
            },
        ];
        const removed: any = [
            {
                name: 'Tartar Sauce',
                defaultQuantity: 1,
                minQuantity: 0,
                maxQuantity: 1,
                productId: 'arb-itm-009-027',
                selection: 'Regular',
                relatedSelections: ['arb-itm-009-030', 'arb-itm-009-028', 'arb-itm-009-029'],
            },
            {
                name: 'Cheddar Sauce',
                defaultQuantity: 1,
                minQuantity: 0,
                maxQuantity: 1,
                productId: 'arb-itm-007-014',
                relatedSelections: ['arb-itm-007-015'],
                selection: 'Regular',
            },
        ];

        const result = getFormattedModifications(added, removed);

        expect(result).toBe('Add Tartar Sauce(Extra), remove Cheddar Sauce(Regular)');
    });

    it('should handle wingtype modifiers', () => {
        const added: any = [
            {
                name: 'All Drums',
                quantity: 1,
                productId: 'arb-itm-009-030',
                price: 0,
                metadata: { MODIFIER_GROUP_TYPE: ModifierGroupType.WINGTYPE },
                relatedSelections: ['arb-itm-009-027', 'arb-itm-009-028', 'arb-itm-009-029'],
            },
            {
                name: 'Ketchup Packet',
                quantity: 5,
            },
        ];
        const removed: any = [];

        const result = getFormattedModifications(added, removed, true);

        expect(result).toBe('All Drums, add Ketchup Packet x 5');
    });

    it('should handle no item modifiers', () => {
        const added: any = [
            {
                name: 'No Sauce',
                quantity: 1,
                productId: 'arb-itm-009-030',
                price: 0,
                metadata: { MODIFIER_GROUP_TYPE: ModifierGroupType.SAUCES },
                relatedSelections: ['arb-itm-009-027', 'arb-itm-009-028', 'arb-itm-009-029'],
            },
            {
                name: 'Ketchup Packet',
                quantity: 5,
            },
        ];
        const removed: any = [];

        const result = getFormattedModifications(added, removed, true, ['arb-itm-009-030']);

        expect(result).toBe('No Sauce, add Ketchup Packet x 5');
    });
});
