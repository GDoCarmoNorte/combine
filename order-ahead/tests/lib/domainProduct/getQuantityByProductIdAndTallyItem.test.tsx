import { getQuantityByProductIdAndTallyItem } from '../../../lib/domainProduct';

describe('getQuantityByProductIdAndTallyItem', () => {
    it('should return quantity of child item', () => {
        const productId = 'Modifier-1000';
        const childIndex = 0;
        const quantity = 3;
        const tallyItem: any = {
            childItems: [
                {
                    productId,
                    quantity,
                },
            ],
        };
        const result = getQuantityByProductIdAndTallyItem(productId, tallyItem, childIndex);

        expect(result).toBe(quantity);
    });

    it('should return quantity of modifier', () => {
        const productId = 'Modifier-1000';
        const childIndex = 0;
        const quantity = 4;
        const tallyItem: any = {
            modifierGroups: [
                {
                    modifiers: [
                        {
                            productId,
                            quantity,
                        },
                    ],
                },
            ],
        };
        const result = getQuantityByProductIdAndTallyItem(productId, tallyItem, childIndex);

        expect(result).toBe(quantity);
    });
});
