import { exec } from 'child_process';

import { getArgv } from '../../../lib/cli';
import { contentfulSpaceCreate } from '../../../lib/contentfulSpaceCreate';

jest.mock('child_process', () => ({
    exec: jest.fn(),
}));
jest.mock('../../../lib/cliLog', () => ({
    logInit: () => ({ log: () => null, logWarning: () => null }),
}));
jest.mock('../../../lib/cli', () => ({
    getArgv: jest.fn(),
}));

describe('contentfulSpaceCreate function', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should run space create with corrrect params', () => {
        (getArgv as jest.Mock).mockReturnValueOnce('token');
        (getArgv as jest.Mock).mockReturnValueOnce('space');

        contentfulSpaceCreate();

        expect(getArgv).toBeCalled();
        expect(exec).toBeCalledWith(
            'contentful space create --management-token token --name space',
            expect.any(Function)
        );
    });

    it('should throw error when token does not exist', () => {
        (getArgv as jest.Mock).mockReturnValueOnce(undefined);
        (getArgv as jest.Mock).mockReturnValueOnce('space');

        contentfulSpaceCreate();

        expect(exec).not.toBeCalled();
    });

    it('should throw error when space does not exist', () => {
        (getArgv as jest.Mock).mockReturnValueOnce('token');
        (getArgv as jest.Mock).mockReturnValueOnce(undefined);

        contentfulSpaceCreate();

        expect(exec).not.toBeCalled();
    });
});
