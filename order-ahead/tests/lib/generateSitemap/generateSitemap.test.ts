import fs from 'fs';

import { getGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import {
    groupSiteMapData,
    isRegionSitemapSupported,
    getBaseUrlWithoutLocale,
    getBaseUrl,
} from '../../../common/helpers/siteMapHelper';
import {
    generateLocationsSitemapXML,
    generateStaticSitemapXML,
    generateXMLString,
    getLocationsUrls,
    getLocationsSitemap,
    generateGeneralSitemapXML,
} from '../../../lib/generateSitemap';

jest.spyOn(global.console, 'error').mockImplementation(() => null);
jest.mock('fs', () => ({
    existsSync: jest.fn().mockReturnValue(true),
    writeFileSync: jest.fn(),
}));
jest.mock('../../../common/services/globalContentfulProps', () => ({
    getGlobalContentfulProps: jest.fn(),
}));
jest.mock('../../../common/helpers/siteMapHelper', () => ({
    groupSiteMapData: jest.fn(),
    getSitemapDirectory: jest.fn(),
    isRegionSitemapSupported: jest.fn(),
    getBaseUrlWithoutLocale: jest.fn(),
    getBaseUrl: jest.fn(),
}));
jest.mock('../../../lib/contentfulDelivery', () => ({
    ContentfulDelivery: jest.fn().mockReturnValue({
        getPages: jest.fn(),
        getPage: jest.fn(),
        getExternalLinks: jest.fn(),
        getDocumentLinks: jest.fn(),
        getSocialMediaLinks: jest.fn(),
        getSiteMapCategories: jest.fn(),
    }),
}));

const realDate = Date;

beforeAll(() => {
    const mockedDate = new Date('2019-01-01T00:00:01Z');
    (global.Date as any) = class {
        constructor() {
            return mockedDate;
        }
    };
    (getBaseUrl as jest.Mock).mockReturnValue('http://www.test.app.url');
});

afterAll(() => {
    global.Date = realDate;
});

const staticSitemapMock = [
    {
        mainLink: '/menu',
        name: 'Menu',
        linkBlocks: [
            {
                links: [
                    {
                        isExternal: false,
                        name: 'Fries',
                        nameInUrl: '/menu/fries/',
                    },
                ],
                name: 'Sides',
                nameInUrl: '/menu/sides/',
            },
        ],
    },
    {
        mainLink: '/contact-us',
        name: 'Contact',
        linkBlocks: [
            {
                links: [
                    {
                        isExternal: true,
                        name: 'External',
                        nameInUrl: 'http://external.com',
                    },
                    {
                        isExternal: false,
                        name: 'Home',
                        nameInUrl: '/',
                    },
                ],
            },
        ],
    },
];
const getLocationsSitemapMock = [
    'http://test.app.url/locations',
    'http://test.app.url/locations/all',
    'http://test.app.url/locations/us/md',
    'http://test.app.url/locations/us/ga/augusta',
    'http://test.app.url/locations/us/tx/huntsville/203-ih-45-s/sports-bar-1368',
];

describe('generateStaticSitemapXML', () => {
    it('should reject with error when contentful data fetch returns invalid value', async () => {
        (getGlobalContentfulProps as jest.Mock).mockImplementationOnce(() => {
            return Promise.reject(new Error());
        });

        await expect(generateStaticSitemapXML()).rejects.toThrow();
    });

    it('should return valid xml string', async () => {
        (groupSiteMapData as jest.Mock).mockReturnValueOnce(staticSitemapMock);
        const xmlstring = await generateStaticSitemapXML();
        expect(xmlstring).toMatchSnapshot();
    });

    it('generateXMLString should return correct xml where menu/product urls should have trailing slashes', async () => {
        (groupSiteMapData as jest.Mock).mockReturnValueOnce(staticSitemapMock);
        const links: string[] = ['http://test.app.url/menu/fries', 'http://test.app.url/product/sides'];
        const xmlString = await generateXMLString(links);
        const parser = new DOMParser();
        const doc = parser.parseFromString(xmlString, 'application/xml');
        const urls = [].map.call(doc.getElementsByTagName('loc'), (z) => z.innerHTML);
        /**For menu urls ('/menu/')**/
        expect(urls[0].includes('/menu/')).toBeTruthy();
        expect(urls[0].endsWith('/')).toBeTruthy();
        /**For product  urls ('/product/')**/
        expect(urls[1].includes('/product/')).toBeTruthy();
        expect(urls[1].endsWith('/')).toBeTruthy();
    });

    it('urlset tag should have xmlns attribute with defined value', async () => {
        (groupSiteMapData as jest.Mock).mockReturnValueOnce(staticSitemapMock);
        const nameSpace = 'http://www.sitemaps.org/schemas/sitemap/0.9';
        const xmlString = await generateStaticSitemapXML();
        const parser = new DOMParser();
        const doc = parser.parseFromString(xmlString, 'application/xml');
        const namespaceURI = doc.getElementsByTagName('urlset')[0].namespaceURI;
        expect(namespaceURI).toEqual(nameSpace);
    });

    it('should throw when public directory does not exist', async () => {
        (fs.existsSync as jest.Mock).mockReturnValueOnce(false);
        (groupSiteMapData as jest.Mock).mockReturnValueOnce(staticSitemapMock);

        await expect(generateStaticSitemapXML()).rejects.toThrow();
    });
});

describe('generateStaticSitemapXML for canada', () => {
    it('should return valid xml string with locale', async () => {
        (groupSiteMapData as jest.Mock).mockReturnValueOnce(staticSitemapMock);
        (getBaseUrl as jest.Mock).mockReturnValue('http://www.test.app.url/en-ca');
        const xmlstring = await generateStaticSitemapXML();
        expect(xmlstring).toMatchSnapshot();
    });
});

describe('generateLocationsSitemapXML', () => {
    it('should reject with error when locations data fetch returns invalid value', async () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (getLocationsUrls as jest.Mock) = jest.fn().mockImplementationOnce(() => {
            return Promise.reject(new Error());
        });

        await expect(generateLocationsSitemapXML()).rejects.toThrow();
    });

    it('should return valid xml string', async () => {
        (getLocationsSitemap as jest.Mock) = jest.fn().mockImplementationOnce(() => {
            return Promise.resolve(getLocationsSitemapMock);
        });

        const xmlstring = await generateLocationsSitemapXML();
        expect(xmlstring).toMatchSnapshot();
    });

    it('urlset tag should have xmlns attribute with defined value', async () => {
        (getLocationsSitemap as jest.Mock) = jest.fn().mockImplementationOnce(() => {
            return Promise.resolve(getLocationsSitemapMock);
        });
        const nameSpace = 'http://www.sitemaps.org/schemas/sitemap/0.9';
        const xmlstring = await generateLocationsSitemapXML();
        const parser = new DOMParser();
        const doc = parser.parseFromString(xmlstring, 'application/xml');
        const namespaceURI = doc.getElementsByTagName('urlset')[0].namespaceURI;
        expect(namespaceURI).toEqual(nameSpace);
    });

    it('should throw when public directory does not exist', async () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (getLocationsSitemap as jest.Mock) = jest.fn().mockImplementationOnce(() => {
            return Promise.resolve(getLocationsSitemapMock);
        });
        (fs.existsSync as jest.Mock).mockReturnValue(false);

        await expect(generateLocationsSitemapXML()).rejects.toThrow();
    });
});

describe('generateGeneralSitemap', () => {
    it('should generate a normal sitemap if arbys is the brand', async () => {
        (isRegionSitemapSupported as jest.Mock) = jest.fn().mockReturnValue(false);
        (getBaseUrlWithoutLocale as jest.Mock) = jest.fn().mockReturnValue('http://www.test.app.url');
        (getBaseUrl as jest.Mock).mockReturnValue('http://www.test.app.url');
        (fs.existsSync as jest.Mock).mockReturnValue(true);
        (fs.writeFileSync as jest.Mock).mockImplementation(() => {
            console.log('create file');
        });
        const xml = await generateGeneralSitemapXML();
        expect(xml).toMatchSnapshot();
    });

    it('should generate a sitemap for a brand that supports multiple regions', async () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (isRegionSitemapSupported as jest.Mock) = jest.fn().mockReturnValue(true);
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (getBaseUrlWithoutLocale as jest.Mock) = jest.fn().mockReturnValue('http://www.test.app.url');
        (fs.writeFileSync as jest.Mock).mockImplementation(() => {
            console.log('create file');
        });
        (fs.existsSync as jest.Mock).mockReturnValue(true);
        const xml = await generateGeneralSitemapXML();
        expect(xml).toMatchSnapshot();
    });
});
