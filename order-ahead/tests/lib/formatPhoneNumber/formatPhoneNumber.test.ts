import { formatPhoneNumber } from '../../../lib/formatPhoneNumber';

describe('formatPhoneNumber', () => {
    it('should return formetted phone number for 012-345-6789 (3 part)', () => {
        expect(formatPhoneNumber('012-345-6789')).toBe('(012) 345-6789');
    });

    it('should return empty string', () => {
        expect(formatPhoneNumber('')).toBe('');
    });

    it('should return the same phome number if 2 parts', () => {
        expect(formatPhoneNumber('345-6789')).toBe('345-6789');
    });

    it('should return the same phome number if 1 part', () => {
        expect(formatPhoneNumber('6789')).toBe('6789');
    });
});
