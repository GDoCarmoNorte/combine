import {
    isEntry,
    optimizeContentfulCollection,
    optimizeContentfulEntry,
    optimizeContentfullData,
} from '../../../lib/cmsOptimization';
import { entryCollectionMock, entryMock, nestedEntryMock } from './cmsOptimizationMocks';

describe('cmsOptimization', () => {
    it('optimizeContentfulEntry', () => {
        expect(optimizeContentfulEntry(entryMock as any)).toEqual({
            sys: {
                id: '5S8IPLqtfAr3Dl7AtKW6P5',
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'infoBanner',
                    },
                },
            },
            fields: {
                title: 'test title',
                header: 'test header',
            },
        });
    });

    it('optimizeContentfulCollection', () => {
        expect(optimizeContentfulCollection(entryCollectionMock as any)).toEqual({
            items: [
                {
                    sys: {
                        id: '5S8IPLqtfAr3Dl7AtKW6P5',
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'infoBanner',
                            },
                        },
                    },
                    fields: {
                        title: 'test title',
                        header: 'test header',
                    },
                },
                {
                    sys: {
                        id: '5S8IPLqtfAr3Dl7AtKW6P5',
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'infoBanner',
                            },
                        },
                    },
                    fields: {
                        title: 'test title',
                        header: 'test header',
                    },
                },
            ],
        });
    });

    it('isEntry', () => {
        expect(isEntry(entryMock)).toEqual(true);
        expect(isEntry(entryCollectionMock)).toEqual(false);
    });

    it('optimizeContentfullData', () => {
        expect(optimizeContentfullData(nestedEntryMock)).toEqual({
            sys: {
                id: '7IgHVqtwV19H94igaEOrbK',
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'page',
                    },
                },
            },
            fields: {
                name: 'Home',
                section: [
                    {
                        sys: {
                            id: '1quSg4gSQbNkpamfUyDifI',
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'recentOrdersSection',
                                },
                            },
                        },
                        fields: {
                            name: 'test name',
                            mainText: 'test main text',
                            image: {
                                sys: {
                                    id: 'j70lnbXnfKW0XyUKGDz1A',
                                    contentType: null,
                                },
                                fields: {
                                    title: 'test title',
                                    description: '',
                                    prodfile: {},
                                },
                            },
                        },
                    },
                ],
                metaDescription: 'test description',
                metaTitle: 'Buffalo Wild Wings',
                link: {
                    sys: {
                        id: '7xhVqDWiSDsAydvEbfzyR9',
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'pageLink',
                            },
                        },
                    },
                    fields: {
                        name: 'Home',
                    },
                },
            },
        });
    });
});
