export const entryMock = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '5S8IPLqtfAr3Dl7AtKW6P5',
        type: 'Entry',
        createdAt: '2020-12-02T06:42:25.261Z',
        updatedAt: '2020-12-02T07:39:23.514Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 3,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'infoBanner',
            },
        },
        locale: 'en-US',
    },
    fields: {
        title: 'test title',
        header: 'test header',
    },
};

export const entryCollectionMock = {
    items: [entryMock, entryMock],
};

export const nestedEntryMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '7IgHVqtwV19H94igaEOrbK',
        type: 'Entry',
        createdAt: '2021-07-09T16:03:27.645Z',
        updatedAt: '2022-01-10T20:16:36.398Z',
        environment: {
            sys: {
                id: 'qa',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 25,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'page',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Home',
        section: [
            {
                metadata: { tags: [] },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '1quSg4gSQbNkpamfUyDifI',
                    type: 'Entry',
                    createdAt: '2022-01-10T20:16:29.450Z',
                    updatedAt: '2022-01-10T20:16:29.450Z',
                    environment: {
                        sys: {
                            id: 'qa',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'recentOrdersSection',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    name: 'test name',
                    mainText: 'test main text',
                    image: {
                        metadata: { tags: [] },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: 'j70lnbXnfKW0XyUKGDz1A',
                            type: 'Asset',
                            createdAt: '2022-01-10T20:16:23.339Z',
                            updatedAt: '2022-01-10T20:16:23.339Z',
                            environment: {
                                sys: {
                                    id: 'qa',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'test title',
                            description: '',
                            prodfile: {},
                        },
                    },
                },
            },
        ],
        metaDescription: 'test description',
        metaTitle: 'Buffalo Wild Wings',
        link: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '7xhVqDWiSDsAydvEbfzyR9',
                type: 'Entry',
                createdAt: '2021-07-09T16:03:10.143Z',
                updatedAt: '2021-07-09T16:03:10.143Z',
                environment: {
                    sys: {
                        id: 'qa',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'pageLink',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'Home',
            },
        },
    },
};
