import fs from 'fs';
import { generateRobotsTxt } from '../../../lib/generateRobots';

const consoleErrorMock = jest.spyOn(global.console, 'error').mockImplementation(() => null);

jest.mock('fs', () => ({
    existsSync: jest.fn(),
    writeFileSync: jest.fn(),
}));

const expectedString = `User-agent: *
Disallow: /checkout
Disallow: /account/delete
Disallow: /confirmation

Sitemap: http://www.test.app.url/sitemap.xml`;

describe('generateRobotsTxt function', () => {
    beforeEach(() => {
        fs.existsSync = jest.fn();
        fs.writeFileSync = jest.fn();
    });
    afterAll(() => {
        jest.restoreAllMocks();
    });
    it('should write file with correct value when public folder exists', () => {
        (fs.existsSync as jest.Mock).mockReturnValue(true);

        generateRobotsTxt();

        expect(fs.writeFileSync).toBeCalledWith('./public/robots.txt', expectedString);
    });

    it('should throw error when public folder does not exist', () => {
        (fs.existsSync as jest.Mock).mockReturnValue(false);

        generateRobotsTxt();

        expect(fs.writeFileSync).not.toBeCalled();
        expect(consoleErrorMock).toBeCalled();
    });
});
