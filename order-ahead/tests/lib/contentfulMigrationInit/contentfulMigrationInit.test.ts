import fs from 'fs';
import { exec } from 'child_process';

import { getArgv } from '../../../lib/cli';
import { contentfulMigrationInit } from '../../../lib/contentfulMigrationInit';
import { initMigrationFile } from '../../../lib/contentful.const';

jest.spyOn(global.console, 'error').mockImplementation(() => null);

jest.mock('child_process', () => ({
    exec: jest.fn(),
}));
jest.mock('fs');
jest.mock('../../../lib/cliLog', () => ({
    logInit: () => ({ log: () => null, logWarning: () => null }),
}));
jest.mock('../../../lib/cli', () => ({
    getArgv: jest.fn(),
}));
jest.mock('../../../lib/contentfulClient', () => ({
    getContentfullEnv: () => 'dev',
    getContentfulSpace: () => 'space',
}));

describe('contentfulMigrationInit function', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should run generate migration with corrrect params', () => {
        (getArgv as jest.Mock).mockReturnValue('token');

        contentfulMigrationInit();

        expect(fs.existsSync).toBeCalled();
        expect(getArgv).toBeCalled();
        expect(exec).toBeCalledWith(
            `contentful space generate migration --management-token token -s space -e dev -f ${initMigrationFile}`,
            expect.any(Function)
        );
    });

    it('should throw error when token does not exist', () => {
        (getArgv as jest.Mock).mockReturnValue(undefined);

        contentfulMigrationInit();

        expect(exec).not.toBeCalled();
    });

    it('should throw error when folder already exist', () => {
        (getArgv as jest.Mock).mockReturnValue('token');
        (fs.existsSync as jest.Mock).mockReturnValue(true);

        contentfulMigrationInit();

        expect(exec).not.toBeCalled();
    });
});
