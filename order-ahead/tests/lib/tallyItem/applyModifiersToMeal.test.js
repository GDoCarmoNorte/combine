import { applyModifiersToMeal } from '../../../lib/tallyItem';
import mealTallyItemMock from '../../mocks/mealTallyItem.mock';
import tallyItemMock from '../../mocks/tallyItem.mock';
import mealTallyItemWithSelectedModifiersMock from '../../mocks/mealTallyItemWithSelectedModifiers.mock';
import tallyItemWithModifiersByDefaultMock from '../../mocks/tallyItemWithModifiersByDefault.mock';

const singleTallyItemProductId = 'arb-itm-000-001';

describe('applyModifiersToMeal', () => {
    it('should apply modifiers from single tally item to meal', () => {
        const updatedMealPdpTallyItem = applyModifiersToMeal(tallyItemMock, mealTallyItemMock);

        expect(
            updatedMealPdpTallyItem.childItems.find((child) => child.productId === singleTallyItemProductId)
                .modifierGroups
        ).toEqual(tallyItemMock.modifierGroups);
    });

    it('should apply modifiers from single tally item with mods by default to meal with preselected mods', () => {
        const updatedMealPdpTallyItem = applyModifiersToMeal(
            tallyItemWithModifiersByDefaultMock,
            mealTallyItemWithSelectedModifiersMock
        );

        expect(
            updatedMealPdpTallyItem.childItems.find((child) => child.productId === singleTallyItemProductId)
                .modifierGroups
        ).toEqual(tallyItemWithModifiersByDefaultMock.modifierGroups);
    });
});
