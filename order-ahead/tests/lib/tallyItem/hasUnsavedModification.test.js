import { hasUnsavedModifications } from '../../../lib/tallyItem';
import mealTallyItemMock from '../../mocks/mealTallyItem.mock';
import tallyItemMock, { promoTallyItemMock } from '../../mocks/tallyItem.mock';

describe('hasUnsavedModifications', () => {
    it('should return false if default item is not provided', () => {
        const result = hasUnsavedModifications(undefined, tallyItemMock);

        expect(result).toBe(false);
    });

    it('should return false if current item is not provided', () => {
        const result = hasUnsavedModifications(tallyItemMock, undefined);

        expect(result).toBe(false);
    });

    it('should return true if productIds are different', () => {
        const defaultItem = { ...tallyItemMock, productId: '1' };
        const currentItem = { ...tallyItemMock, productId: '2' };

        const result = hasUnsavedModifications(defaultItem, currentItem);

        expect(result).toEqual(true);
    });

    it('should return true if quantity are different', () => {
        const defaultItem = { ...tallyItemMock, quantity: 1 };
        const currentItem = { ...tallyItemMock, quantity: 2 };

        const result = hasUnsavedModifications(defaultItem, currentItem);

        expect(result).toEqual(true);
    });

    it('should return false if products does not have childItems and modifiers', () => {
        const result = hasUnsavedModifications(
            { ...tallyItemMock, modifierGroups: [] },
            { ...tallyItemMock, modifierGroups: undefined }
        );

        expect(result).toBe(false);
    });

    it('should return false for single product if items are equal', () => {
        const result = hasUnsavedModifications(tallyItemMock, tallyItemMock);

        expect(result).toBe(false);
    });

    it('should return true for single products if quantity in a modifier is different', () => {
        const modifiedModifierGroup = tallyItemMock.modifierGroups[0];
        const modifiedModifier = {
            ...modifiedModifierGroup.modifiers[0],
            quantity: 2,
        };
        const currentTallyItemMock = {
            ...tallyItemMock,
            modifierGroups: [
                {
                    ...modifiedModifierGroup,
                    modifiers: [modifiedModifier, ...modifiedModifierGroup.modifiers.slice(1)],
                },
                ...tallyItemMock.modifierGroups.slice(1),
            ],
        };

        const result = hasUnsavedModifications(tallyItemMock, currentTallyItemMock);

        expect(result).toBe(true);
    });

    it('should return true for single products if modifiers are different', () => {
        const modifiedModifierGroup = tallyItemMock.modifierGroups[0];
        const currentTallyItemMock = {
            ...tallyItemMock,
            modifierGroups: [
                {
                    ...modifiedModifierGroup,
                    modifiers: modifiedModifierGroup.modifiers.slice(1),
                },
                ...tallyItemMock.modifierGroups.slice(1),
            ],
        };

        const result = hasUnsavedModifications(tallyItemMock, currentTallyItemMock);

        expect(result).toBe(true);
    });

    it('should return false for meal if items are equal', () => {
        const result = hasUnsavedModifications(mealTallyItemMock, mealTallyItemMock);

        expect(result).toBe(false);
    });

    it('should return true for meal if child items are different', () => {
        const currentTallyItemMock = {
            ...mealTallyItemMock,
            childItems: [
                { ...mealTallyItemMock.childItems[0], productId: 'mockId' },
                ...mealTallyItemMock.childItems.slice(1),
            ],
        };

        const result = hasUnsavedModifications(mealTallyItemMock, currentTallyItemMock);

        expect(result).toBe(true);
    });

    it('should return true for meal if modifiers are different', () => {
        const modifiedChildItem = mealTallyItemMock.childItems[0];
        const currentTallyItemMock = {
            ...mealTallyItemMock,
            childItems: [
                {
                    ...modifiedChildItem,
                    modifierGroups: [
                        {
                            ...modifiedChildItem.modifierGroups[0],
                            modifiers: modifiedChildItem.modifierGroups[0].modifiers.slice(1),
                        },
                        ...modifiedChildItem.modifierGroups.slice(1),
                    ],
                },
                ...mealTallyItemMock.childItems.slice(1),
            ],
        };

        const result = hasUnsavedModifications(mealTallyItemMock, currentTallyItemMock);

        expect(result).toBe(true);
    });

    it('should return false for promo if items are equal', () => {
        const result = hasUnsavedModifications(promoTallyItemMock, promoTallyItemMock);

        expect(result).toBe(false);
    });

    it('should return true for promo if child items are different', () => {
        const currentTallyItemMock = {
            ...promoTallyItemMock,
            childItems: [
                { ...promoTallyItemMock.childItems[0], productId: 'mockId' },
                ...promoTallyItemMock.childItems.slice(1),
            ],
        };

        const result = hasUnsavedModifications(promoTallyItemMock, currentTallyItemMock);

        expect(result).toBe(true);
    });

    it('should return true for promo if modifiers are different', () => {
        const modifiedChildItem = promoTallyItemMock.childItems[0];
        const currentTallyItemMock = {
            ...promoTallyItemMock,
            childItems: [
                {
                    ...modifiedChildItem,
                    modifierGroups: [
                        {
                            ...modifiedChildItem.modifierGroups[0],
                            modifiers: modifiedChildItem.modifierGroups[0].modifiers.slice(1),
                        },
                        ...modifiedChildItem.modifierGroups.slice(1),
                    ],
                },
                ...promoTallyItemMock.childItems.slice(1),
            ],
        };

        const result = hasUnsavedModifications(promoTallyItemMock, currentTallyItemMock);

        expect(result).toBe(true);
    });
});
