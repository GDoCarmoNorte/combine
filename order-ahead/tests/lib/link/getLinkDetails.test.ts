import {
    IDocumentLink,
    IExternalLink,
    IMenuCategoryLink,
    IPageLink,
    IPhoneNumberLink,
    IProduct,
} from '../../../@generated/@types/contentful';
import { getLinkDetails } from '../../../lib/link';

describe('link', () => {
    describe('getLinkDetails', () => {
        it('should return correct link details for string link', () => {
            const link = 'menu';
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: false,
                isPhone: false,
                href: link,
            });
        });

        it('should return correct link details for external link', () => {
            const link: IExternalLink = externalLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: true,
                isPhone: false,
                href: link.fields.nameInUrl,
                name: link.fields.name,
            });
        });

        it('should return correct link details for document link', () => {
            const link: IDocumentLink = documentLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: true,
                isPhone: false,
                href: link.fields.document.fields.file.url,
                name: link.fields.name,
            });
        });

        it('should return correct link details for menu category link', () => {
            const link: IMenuCategoryLink = menuCategoryLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: false,
                isPhone: false,
                href: `/menu/${link.fields.nameInUrl}`,
                name: link.fields.name,
            });
        });

        it('should return correct link details for page link', () => {
            const link: IPageLink = pageLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: false,
                isPhone: false,
                href: `/${link.fields.nameInUrl}`,
                name: link.fields.name,
            });
        });

        it('should return correct link details for phone number link', () => {
            const link: IPhoneNumberLink = phoneNumberLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: false,
                isPhone: true,
                href: link.fields.phoneNumber,
                name: link.fields.name,
            });
        });

        it('should return correct link details for product link', () => {
            const link: IProduct = productLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths });

            expect(details).toEqual({
                isExternal: false,
                isPhone: false,
                href: productDetailsPagePaths[0].productPath,
                name: link.fields.name,
            });
        });

        it('should return correct link details for product link with currentCategoryUrl', () => {
            const link: IProduct = productLink as any;
            const details = getLinkDetails(link, { productDetailsPagePaths, currentCategoryUrl: 'limited-time' });

            expect(details).toEqual({
                isExternal: false,
                isPhone: false,
                href: '/menu/limited-time/cranberry-deep-fried-turkey-wrap',
                name: link.fields.name,
            });
        });

        it('should return correct link details for product link without paths', () => {
            const link: IProduct = productLink as any;
            const details = getLinkDetails(link);

            expect(details).toEqual({
                isExternal: false,
                isPhone: false,
                href: '/',
                name: link.fields.name,
            });
        });
    });
});

const productDetailsPagePaths = [
    {
        menuCategoryUrl: 'kids-menu',
        productDetailsPageUrl: 'cranberry-deep-fried-turkey-wrap',
        menuCategoryId: 'testCategory',
        productIds: ['ARBYS-WEBOA-676477835'],
        productPath: '/menu/kids-menu/cranberry-deep-fried-turkey-wrap',
        canonicalPath: 'localhost:3000/menu/kids-menu/cranberry-deep-fried-turkey-wrap',
    },
];

const documentLink = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '3UjzFRfbdfYxX8nBxZolt6',
        type: 'Entry',
        createdAt: '2020-11-02T07:30:14.303Z',
        updatedAt: '2020-11-02T07:30:14.303Z',
        environment: {
            sys: {
                id: 'DBBP-3141',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'documentLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Test Document Link',
        document: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3Q0szDGnIsJd4cX6eQbrQP',
                type: 'Asset',
                createdAt: '2020-10-30T10:50:15.995Z',
                updatedAt: '2020-10-30T10:50:15.995Z',
                environment: {
                    sys: {
                        id: 'DBBP-3141',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'dummy PDF',
                file: {
                    url:
                        '//assets.ctfassets.net/o19mhvm9a2cm/3Q0szDGnIsJd4cX6eQbrQP/f15738c57418fc6df39c9561cb04f0e5/dummy.pdf',
                    details: {
                        size: 13264,
                    },
                    fileName: 'dummy.pdf',
                    contentType: 'application/pdf',
                },
            },
        },
    },
};

const externalLink = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '4VM6lXrtfld6fSd61fZjVY',
        type: 'Entry',
        createdAt: '2020-09-22T16:37:06.989Z',
        updatedAt: '2020-10-14T18:43:25.508Z',
        environment: {
            sys: {
                id: 'DBBP-3141',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 7,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'externalLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Do Not Sell My Info',
        nameInUrl: 'https://arbys.truyo.com/consumer/donotsell',
    },
};

const pageLink = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '4lY1FkjN5fitJuUjnthfJR',
        type: 'Entry',
        createdAt: '2020-09-22T15:24:45.780Z',
        updatedAt: '2020-09-22T15:24:45.780Z',
        environment: {
            sys: {
                id: 'DBBP-3141',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'pageLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'About Our Ads',
        nameInUrl: 'about-our-ads',
    },
};

const menuCategoryLink = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '3qUTmyM5nbbkdpKqeqI7ax',
        type: 'Entry',
        createdAt: '2020-07-18T13:00:13.060Z',
        updatedAt: '2020-10-23T19:02:00.832Z',
        environment: {
            sys: {
                id: 'DBBP-3141',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 15,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'menuCategoryLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Kids Menu',
        nameInUrl: 'kids-menu',
    },
};

const phoneNumberLink = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '5GsNFoRffZHZU49Inh6QSg',
        type: 'Entry',
        createdAt: '2020-10-28T15:57:59.219Z',
        updatedAt: '2020-10-28T15:57:59.219Z',
        environment: {
            sys: {
                id: 'DBBP-3141',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'phoneNumberLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Phone',
        phoneNumber: '1-770-519-5367',
    },
};

const productLink = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '1yTkV0uPdVyHeVtiAmNItM',
        type: 'Entry',
        createdAt: '2020-10-27T16:32:03.165Z',
        updatedAt: '2020-10-27T16:32:03.165Z',
        environment: {
            sys: {
                id: 'DBBP-3141',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'product',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Cranberry Deep Fried Turkey Wrap',
        nameInUrl: 'cranberry-deep-fried-turkey-wrap',
        image: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '5RpUECRqBBtwKaqbAGQ4sq',
                type: 'Asset',
                createdAt: '2020-10-09T20:33:48.471Z',
                updatedAt: '2020-10-09T20:33:48.471Z',
                environment: {
                    sys: {
                        id: 'DBBP-3141',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'LTO Deep Fried Turkey Wrap Cranberry',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/5RpUECRqBBtwKaqbAGQ4sq/d450f2dd6e9d621db7604f660bd8590a/Website_LTO_DFT_Wrap_Cranberry.png',
                    details: {
                        size: 3452325,
                        image: {
                            width: 4000,
                            height: 3000,
                        },
                    },
                    fileName: 'Website_LTO_DFT_Wrap_Cranberry.png',
                    contentType: 'image/png',
                },
            },
        },
        productId: 'ARBYS-WEBOA-676477835',
    },
};
