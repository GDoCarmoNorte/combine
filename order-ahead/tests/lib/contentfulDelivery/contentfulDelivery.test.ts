import * as contentfulClient from '../../../lib/contentfulClient';
import { sortCategoriesBySortOrder, ContentfulDelivery } from '../../../lib/contentfulDelivery';
import MockLocationsNewsSectionData from '../../mock-data/contentful/contentful-getEntities-locationNewsSection.json';
import MockProductModifiersData from '../../mock-data/contentful/contentful-getEntries-productModifier.json';

const productNutritionLinkMock = 'nutLink';
describe('contentfulDelivery', () => {
    const getEntriesStub = jest.fn();
    beforeAll(() => {
        jest.spyOn(contentfulClient, 'createClient').mockReturnValue({
            getEntries: getEntriesStub,
        } as never);
    });

    afterEach(() => jest.clearAllMocks());

    describe('sortCategoriesBySortOrder', () => {
        const sortOrder = [
            'signature',
            'market-fresh',
            'roast-beef',
            'chicken',
            'sliders',
            'sides',
            'kids-menu',
            'desserts',
            'meals',
            'drinks',
            'limited-time',
            'top-picks',
        ];
        it('should return sorted categories with existing names', () => {
            const categories = ['top-picks', 'limited-time', 'meals'];

            const sorted = sortCategoriesBySortOrder(categories, sortOrder);

            expect(sorted).toEqual(['meals', 'limited-time', 'top-picks']);
        });

        it('should return sorted categories with some non-existing names', () => {
            const categories = ['5', 'drinks', '3', '8'];

            const sorted = sortCategoriesBySortOrder(categories, sortOrder);

            expect(sorted).toEqual(['drinks', '5', '3', '8']);
        });
    });

    describe('getLocationNews', () => {
        it('should return location news sections', async () => {
            getEntriesStub.mockReturnValue(MockLocationsNewsSectionData);
            const locationNewsSections = await ContentfulDelivery().getLocationNews();
            expect(locationNewsSections).toEqual(MockLocationsNewsSectionData);
            expect(getEntriesStub).toHaveBeenCalledTimes(1);
            expect(getEntriesStub).toHaveBeenCalledWith({
                content_type: 'locationNewsSection',
                limit: 1000,
            });
        });
    });

    describe('getProductNutritionLink', () => {
        it('should call a contentful SDK with proper arguments', async () => {
            getEntriesStub.mockReturnValue(productNutritionLinkMock);
            const productNutritionLink = await ContentfulDelivery().getProductNutritionLink();
            expect(productNutritionLink).toEqual(productNutritionLinkMock);
            expect(getEntriesStub).toHaveBeenCalledTimes(1);
            expect(getEntriesStub).toHaveBeenCalledWith({
                content_type: 'productNutritionLink',
                limit: 1,
            });
        });
    });

    describe('getProductModifiers', () => {
        it('should return product modifiers for the single request', async () => {
            getEntriesStub.mockReturnValue(MockProductModifiersData);
            const productModifiersData = await ContentfulDelivery().getProductModifiers();
            expect(productModifiersData).toEqual(MockProductModifiersData);
            expect(getEntriesStub).toHaveBeenCalledTimes(1);
            expect(getEntriesStub).toHaveBeenCalledWith({
                content_type: 'productModifier',
                limit: 1000,
                skip: 0,
            });
        });

        it('should return product modifiers for several requests', async () => {
            getEntriesStub.mockReturnValue({ ...MockProductModifiersData, total: 2019 });
            const productModifiersData = await ContentfulDelivery().getProductModifiers();
            expect(productModifiersData).toEqual({ ...MockProductModifiersData, total: 2019 });
            expect(getEntriesStub).toHaveBeenCalledTimes(3);
            expect(getEntriesStub.mock.calls).toEqual([
                [
                    {
                        content_type: 'productModifier',
                        limit: 1000,
                        skip: 0,
                    },
                ],
                [
                    {
                        content_type: 'productModifier',
                        limit: 1000,
                        skip: 1000,
                    },
                ],
                [
                    {
                        content_type: 'productModifier',
                        limit: 1000,
                        skip: 2000,
                    },
                ],
            ]);
        });
    });
});
