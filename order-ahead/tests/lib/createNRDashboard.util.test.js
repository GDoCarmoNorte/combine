const util = require('../../lib/createNRDashboard.util');
const template = require('../../new-relic/dashboards/template.json');

describe('createNRDashboard util', () => {
    let result;
    const config = [
        {
            id: 'prd01',
            name: 'PRODUCTION',
            config: {
                appId: '1001',
                appHost: 'something.com',
                appServiceHost: 'api.something.com',
                newRelicServiceSuffix: 'bww-prd01',
            },
        },
        {
            id: 'stg01',
            name: 'STAGE',
            config: {
                appId: '2222',
                appHost: 'something.com',
                appServiceHost: 'api.something.com',
                newRelicServiceSuffix: 'bww-stg01',
            },
        },
        {
            id: 'qa01',
            name: 'QA',
            config: {
                appId: '3000',
                appHost: 'something.com',
                appServiceHost: 'api.something.com',
                newRelicServiceSuffix: 'bww-qa01',
            },
        },
    ];

    beforeAll(() => (result = util.generateNRDashboardJSON('bww', 'en-us', template, config)));

    test('have "name" = "BWW Order Ahead (en-us)"', () => {
        expect(result.name).toEqual('BWW Order Ahead (en-us)');
    });
    test('have "description" = "Order Ahead Webapp dashboard for brand: \'BWW\' and locale \'en-us\'"', () => {
        expect(result.description).toEqual("Order Ahead Webapp dashboard for brand: 'BWW' and locale: 'en-us'");
    });
    test('have "permissions" = "PUBLIC_READ_ONLY"', () => {
        expect(result.permissions).toEqual('PUBLIC_READ_ONLY');
    });
    test('widgets JSON', () => {
        expect(result.pages.length).toEqual(3);
        expect(result.pages).toMatchSnapshot();
    });
});
