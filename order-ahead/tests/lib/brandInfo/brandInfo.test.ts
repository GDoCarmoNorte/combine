import getBrandInfo from '../../../lib/brandInfo';

// see jest/setupTestsAfterEnv.js for process env mocking

describe('brand info', () => {
    test('brandName should be defined', () => {
        const brandInfo = getBrandInfo();
        expect(brandInfo.brandName).toBe("Arby's");
        expect(brandInfo.brandId).toBe('Arbys');
    });
});
