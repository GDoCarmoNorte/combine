import jwt from 'jsonwebtoken';
import { verifyJWT } from '../../../lib/jwt';

jest.mock('jsonwebtoken', () => ({
    verify: jest.fn(),
}));

const token = '12345';

describe('verifyJWT function', () => {
    it('should return without throwing when token is valid', () => {
        (jwt.verify as jest.Mock).mockReturnValue('');

        expect(verifyJWT(token)).toEqual('');
    });

    it('should throw when token is invalid', () => {
        (jwt.verify as jest.Mock).mockImplementation(() => {
            throw new Error('message');
        });

        expect(() => verifyJWT(token)).toThrowError(new Error('message'));
    });
});
