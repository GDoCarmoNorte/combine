import isLocationOrderAheadAvailable from '../../../lib/locations/isLocationOrderAheadAvailable';
import getLocationService from '../../../lib/locations/getLocationService';
import configFeatureFlagsMocks from '../../mocks/configFeatureFlag.mock';

jest.mock('../../../lib/locations/getLocationService');
jest.mock('../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

describe('isLocationOrderAheadAvailable', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('should return false if not digitally enabled', () => {
        (getLocationService as jest.Mock).mockReturnValueOnce('serviceMock');

        const result = isLocationOrderAheadAvailable(
            { isDigitallyEnabled: false } as any,
            configFeatureFlagsMocks.configuration.isOAEnabled
        );

        expect(result).toBe(false);
    });

    it('should return false if OA service is not found', () => {
        (getLocationService as jest.Mock).mockReturnValueOnce(undefined);

        const result = isLocationOrderAheadAvailable(
            { isDigitallyEnabled: true } as any,
            configFeatureFlagsMocks.configuration.isOAEnabled
        );

        expect(result).toBe(false);
    });

    it('should return false if location is not provided', () => {
        (getLocationService as jest.Mock).mockReturnValueOnce('serviceMock');

        const result = isLocationOrderAheadAvailable(null, configFeatureFlagsMocks.configuration.isOAEnabled);

        expect(result).toBe(false);
    });

    it('should return true if OA service available and digitally enabled', () => {
        (getLocationService as jest.Mock).mockReturnValueOnce('serviceMock');

        const result = isLocationOrderAheadAvailable(
            { isDigitallyEnabled: true } as any,
            configFeatureFlagsMocks.configuration.isOAEnabled
        );

        expect(result).toBe(true);
    });
});
