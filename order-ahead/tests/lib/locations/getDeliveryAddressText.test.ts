import getDeliveryAddressText from '../../../lib/locations/getDeliveryAddressText';
import addressMock from '../../mocks/deliveryAddess.mock';

describe('getDeliveryAddressText', () => {
    it('should correctly format address', () => {
        expect(getDeliveryAddressText(addressMock as any)).toEqual(
            `${addressMock.deliveryLocation.addressLine1}, ${addressMock.deliveryLocation.state}, ${addressMock.deliveryLocation.zipCode}`
        );
    });
});
