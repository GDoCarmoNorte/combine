import {
    getLocationAddressString,
    getLocationAddressDetailsString,
    getDeliveryAddressString,
} from '../../../lib/locations/getLocationAddressString';
import locationsMock from '../../mocks/expLocations.mock';
import { locationDataMock } from '../../components/organisms/LocationHeader/locationHeader.mock';
import deliveryLocationMock from '../../mocks/deliveryAddess.mock';

const location = locationsMock.locations[0];
const address = locationDataMock.contactDetails.address;
const deliveryAddress = deliveryLocationMock.deliveryLocation;

describe('getLocationAddressString', () => {
    it('should return correct location address string', () => {
        expect(getLocationAddressString(location as any)).toBe('417 Water St, Chardon, OH 44024');
    });

    it('should return empty string if location does not exist', () => {
        expect(getLocationAddressString(undefined)).toBe('');
    });

    it('should return empty string if location data does not exist', () => {
        expect(getLocationAddressString({} as any)).toBe('');
    });
});

describe('getLocationAddressDetailsString', () => {
    it('should return correct address string', () => {
        expect(getLocationAddressDetailsString(address)).toBe('1887 Pulaski Hwy, Bear, DE 19701-1731');
    });

    it('should return empty string if address does not exist', () => {
        expect(getLocationAddressDetailsString(undefined)).toBe('');
    });
});

describe('getDeliveryAddressString', () => {
    it('should return correct address string', () => {
        expect(getDeliveryAddressString(deliveryAddress)).toBe('7947 Golden Valley Rd, Minneapolis, MN 55427-4402');
    });

    it('should return empty string if address does not exist', () => {
        expect(getLocationAddressDetailsString(undefined)).toBe('');
    });
});
