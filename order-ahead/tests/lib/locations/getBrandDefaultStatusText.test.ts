import getBrandDefaultStatusText from '../../../lib/locations/getBrandDefaultStatusText';
import getBrandInfo from '../../../lib/brandInfo';

jest.mock('../../../lib/brandInfo');

describe('getBrandDefaultStatusText', () => {
    it('should get correct status text for brand', () => {
        (getBrandInfo as jest.Mock).mockReturnValue('Arbys');
        expect(getBrandDefaultStatusText()).toMatchSnapshot();

        (getBrandInfo as jest.Mock).mockReturnValue('Bww');
        expect(getBrandDefaultStatusText()).toMatchSnapshot();

        (getBrandInfo as jest.Mock).mockReturnValue('Jje');
        expect(getBrandDefaultStatusText()).toMatchSnapshot();
    });
});
