import { TServiceTypeModel } from '../../../@generated/webExpApi';
import getLocationService from '../../../lib/locations/getLocationService';

import locationsPageMock from '../../mocks/expLocations.mock';

const locationMock = locationsPageMock.locations[0] as any;

describe('getLocationService', () => {
    it('should return location service by type', () => {
        const result = getLocationService(locationMock, TServiceTypeModel.Pickup);

        expect(result).toEqual(locationMock.services[0]);
    });

    it('should return undefined if no service is found', () => {
        const result = getLocationService(locationMock, TServiceTypeModel.Outer);

        expect(result).toBeUndefined();
    });

    it('should return undefined if no services in location', () => {
        const result = getLocationService({ ...locationMock, services: [] }, TServiceTypeModel.Pickup);

        expect(result).toBeUndefined();
    });

    it('should return undefined if location is provided', () => {
        const result = getLocationService(null, TServiceTypeModel.Pickup);

        expect(result).toBeUndefined();
    });
});
