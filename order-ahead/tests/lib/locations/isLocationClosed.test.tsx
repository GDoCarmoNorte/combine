import { TLocationStatusModel } from '../../../@generated/webExpApi/models';
import isLocationClosed from '../../../lib/locations/isLocationClosed';
import locationsPageMock from '../../mocks/expLocations.mock';

const locationMock = locationsPageMock.locations[0];

describe('isLocationClosed', () => {
    it('should return true if location status is CLOSE', () => {
        expect(isLocationClosed({ ...locationMock, status: TLocationStatusModel.Closed } as any)).toBe(true);
    });

    it('should return true if location status is Temporaty Closed', () => {
        expect(isLocationClosed({ ...locationMock, status: TLocationStatusModel.TempClosed } as any)).toBe(true);
    });

    it('should return false if location status is OPEN', () => {
        expect(isLocationClosed({ ...locationMock, status: TLocationStatusModel.Open } as any)).toBe(false);
    });

    it('should return false if location status is undefined', () => {
        expect(isLocationClosed(locationMock as any)).toBe(false);
    });
});
