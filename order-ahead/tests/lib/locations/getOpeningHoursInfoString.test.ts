import resolveOpeningHours from '../../../lib/locations/resolveOpeningHours';
import { getOpeningHoursInfoString } from '../../../lib/locations/getOpeningHoursInfoString';

jest.mock('../../../lib/locations/resolveOpeningHours');

describe('getOpeningHoursInfoString', () => {
    it('should return "Open Now - Closes 10 PM" string', () => {
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: true,
            openTime: '10 AM',
            closeTime: '10 PM',
            isTwentyFourHourService: false,
        });

        expect(getOpeningHoursInfoString(undefined)).toBe('Open Now - Closes 10 PM');
    });

    it('should return "Closed Now - Opens 10 AM" string', () => {
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: false,
            openTime: '10 AM',
            closeTime: '10 PM',
            isTwentyFourHourService: false,
        });

        expect(getOpeningHoursInfoString(undefined)).toBe('Closed Now - Opens 10 AM');
    });

    it('should return " Open 24H" string', () => {
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: true,
            openTime: '10 AM',
            closeTime: '10 AM',
            isTwentyFourHourService: true,
        });

        expect(getOpeningHoursInfoString(undefined)).toBe(' Open 24H');
    });

    it('should return null if resolveOpeningHours returns null', () => {
        (resolveOpeningHours as jest.Mock).mockReturnValue(null);

        expect(getOpeningHoursInfoString(undefined)).toBe(null);
    });
});
