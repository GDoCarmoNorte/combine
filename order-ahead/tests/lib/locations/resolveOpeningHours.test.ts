import { isFriday } from '../../../common/helpers/dateTime';
import { TServiceTypeModel } from '../../../@generated/webExpApi';
import resolveOpeningHours from '../../../lib/locations/resolveOpeningHours';
import getLocationService from '../../../lib/locations/getLocationService';

const locationMock = {
    id: '5398',
    brandName: "Arby's",
    displayName: 'Chardon – Water St',
    timezone: 'America/New_York',
    distance: {
        amount: 0,
        unit: 'mi',
    },
    contactDetails: {
        address: {
            line1: '417 Water St',
            line2: '',
            line3: '',
            postalCode: '44024',
            stateProvinceCode: 'OH',
            countryCode: 'US',
            city: 'Chardon',
        },
        email: '',
        phone: '(440) 286-7515',
    },
    services: [
        {
            type: 'STORE',
            hours: [
                {
                    dayOfWeek: 'SUNDAY',
                    startTime: '10:00',
                    endTime: '22:00',
                    isTwentyFourHourService: null,
                },
                {
                    dayOfWeek: 'MONDAY',
                    startTime: '10:00',
                    endTime: '23:00',
                    isTwentyFourHourService: null,
                },
                {
                    dayOfWeek: 'TUESDAY',
                    startTime: '10:00',
                    endTime: '23:00',
                    isTwentyFourHourService: null,
                },
                {
                    dayOfWeek: 'WEDNESDAY',
                    startTime: '10:00',
                    endTime: '23:00',
                    isTwentyFourHourService: null,
                },
                {
                    dayOfWeek: 'THURSDAY',
                    startTime: '10:00',
                    endTime: '23:00',
                    isTwentyFourHourService: null,
                },
                {
                    dayOfWeek: 'FRIDAY',
                    startTime: '10:00',
                    endTime: '00:00',
                    isTwentyFourHourService: null,
                },
                {
                    dayOfWeek: 'SATURDAY',
                    startTime: '10:00',
                    endTime: '00:00',
                    isTwentyFourHourService: null,
                },
            ],
        },
    ],
    details: {
        latitude: 41.5804712,
        longitude: -81.2104162,
    },
} as any;

const storeTimezone = 'America/New_York';

const RealDate = Date.now;

jest.mock('../../../lib/locations/getLocationService');

beforeEach(() => {
    global.Date.now = jest.fn(() => new Date('Wed Nov 04 2020 17:29:00 GMT+0300').getTime());
});

afterAll(() => {
    global.Date.now = RealDate;
});

describe('resolveOpeningHours', () => {
    beforeEach(() => {
        (getLocationService as jest.Mock).mockReturnValue(locationMock.services[0]);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should return null if location undefined', () => {
        expect(resolveOpeningHours(undefined, TServiceTypeModel.Store)).toEqual(null);
    });

    it('should return null if storeTimezone is undefined', () => {
        expect(resolveOpeningHours({ ...locationMock, timezone: null }, TServiceTypeModel.Store)).toEqual(null);
    });

    it('should return null if service is not found', () => {
        (getLocationService as jest.Mock).mockReturnValueOnce(undefined);

        expect(resolveOpeningHours(locationMock, TServiceTypeModel.Store)).toEqual(null);
    });

    it('should return hours when service is closed', () => {
        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Store);

        expect(result).toMatchObject({
            closeTime: null,
            isTwentyFourHourService: false,
            isOpen: false,
            openTime: 'today at 10:00 AM',
            isClosingSoon: false,
            isOpeningSoon: false,
            storeTimezone,
        });
    });

    it('should return correct data when service is open', () => {
        global.Date.now = jest.fn(() => new Date('Wed Nov 04 2020 22:29:00 GMT+0300').getTime());

        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Store);

        expect(result).toMatchObject({
            closeTime: 'today at 11:00 PM',
            isTwentyFourHourService: false,
            isOpen: true,
            openTime: 'today at 10:00 AM',
            isClosingSoon: false,
            isOpeningSoon: false,
            storeTimezone,
        });
    });

    it('should return correct data when service is open but closing within 30 minutes', () => {
        global.Date.now = jest.fn(() => new Date('Wed Nov 04 2020 06:39:00 GMT+0300').getTime());

        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Store);

        expect(result).toMatchObject({
            closeTime: 'today at 11:00 PM',
            isTwentyFourHourService: false,
            isOpen: true,
            openTime: 'today at 10:00 AM',
            isClosingSoon: true,
            isOpeningSoon: false,
            storeTimezone,
        });
    });

    it('should return correct data when store is closed but opening within 30 minutes', () => {
        global.Date.now = jest.fn(() => new Date('Wed Nov 04 2020 17:39:00 GMT+0300').getTime());

        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Store);

        expect(result).toMatchObject({
            closeTime: null,
            isTwentyFourHourService: false,
            isOpen: false,
            openTime: 'today at 10:00 AM',
            isClosingSoon: false,
            isOpeningSoon: true,
            storeTimezone,
        });
    });

    it('should return correct data when service is 24h', () => {
        (getLocationService as jest.Mock).mockReturnValue({
            type: 'DELIVERY',
            hours: [
                {
                    dayOfWeek: 'SUNDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'MONDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'TUESDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'WEDNESDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'THURSDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'FRIDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'SATURDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
            ],
        });
        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Delivery);

        expect(result).toMatchObject({
            closeTime: 'tomorrow at 12:00 AM',
            isTwentyFourHourService: true,
            isOpen: true,
            openTime: 'today at 12:00 AM',
            isClosingSoon: false,
            isOpeningSoon: false,
            storeTimezone,
        });
    });

    it('should return correct data when service is not always 24h', () => {
        (getLocationService as jest.Mock).mockReturnValue({
            type: 'DELIVERY',
            hours: [
                {
                    dayOfWeek: 'SUNDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
                {
                    dayOfWeek: 'MONDAY',
                    startTime: '00:00',
                    endTime: '23:00',
                    isTwentyFourHourService: false,
                },
                {
                    dayOfWeek: 'TUESDAY',
                    startTime: '00:00',
                    endTime: '22:00',
                    isTwentyFourHourService: false,
                },
                {
                    dayOfWeek: 'WEDNESDAY',
                    startTime: '00:00',
                    endTime: '23:00',
                    isTwentyFourHourService: false,
                },
                {
                    dayOfWeek: 'THURSDAY',
                    startTime: '10:00',
                    endTime: '23:00',
                    isTwentyFourHourService: false,
                },
                {
                    dayOfWeek: 'FRIDAY',
                    startTime: '00:00',
                    endTime: '23:00',
                    isTwentyFourHourService: false,
                },
                {
                    dayOfWeek: 'SATURDAY',
                    startTime: '00:00',
                    endTime: '00:00',
                    isTwentyFourHourService: true,
                },
            ],
        });
        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Delivery);

        expect(result).toMatchObject({
            isOpen: true,
            isTwentyFourHourService: false,
            openTime: 'today at 12:00 AM',
            closeTime: 'today at 11:00 PM',
            isOpeningSoon: false,
            isClosingSoon: false,
            storeTimezone: 'America/New_York',
        });
    });

    it('should return correct openedTimeRanges', () => {
        const result = resolveOpeningHours(locationMock, TServiceTypeModel.Store);

        expect(result.openedTimeRanges[0]).toEqual({
            start: new Date('2020-10-31T15:00:00.000Z'),
            end: new Date('2020-11-01T05:00:00.000Z'),
        });
    });

    it('should return correct openedTimeRanges if some days has equal openTime, closeTime and orderCutOffTime (store closed)', () => {
        global.Date.now = jest.fn(() => new Date('Wed Nov 04 2020 22:29:00 GMT+0300').getTime());
        (getLocationService as jest.Mock).mockReturnValue({
            type: 'STORE',
            hours: [
                {
                    dayOfWeek: 'SUNDAY',
                    startTime: '02:00',
                    endTime: '11:00',
                },
                {
                    dayOfWeek: 'MONDAY',
                    startTime: '02:00',
                    endTime: '11:00',
                },
                {
                    dayOfWeek: 'TUESDAY',
                    startTime: '03:00',
                    endTime: '11:00',
                },
                {
                    dayOfWeek: 'WEDNESDAY',
                    startTime: '02:00',
                    endTime: '11:00',
                },
                {
                    dayOfWeek: 'THURSDAY',
                    startTime: '03:00',
                    endTime: '11:00',
                },
                {
                    dayOfWeek: 'FRIDAY',
                    startTime: '22:00',
                    endTime: '22:00',
                },
                {
                    dayOfWeek: 'SATURDAY',
                    startTime: '02:00',
                    endTime: '11:00',
                },
            ],
        });
        const openingHours = resolveOpeningHours(locationMock, TServiceTypeModel.Store);

        const fridayDates = openingHours.openedTimeRanges.filter((day) => isFriday(day.start));
        expect(fridayDates).toHaveLength(0);
    });
});
