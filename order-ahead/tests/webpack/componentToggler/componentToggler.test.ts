import componentToggler from '../../../webpack/componentToggler';
import { emptyConfigs, fullConfigs } from '../getConfig/getConfig.mock';

jest.mock('../../../webpack/getConfig');
const getConfig = require('../../../webpack/getConfig');

jest.mock('webpack', () => ({
    NormalModuleReplacementPlugin: jest.fn(),
}));

describe('componentToggler(webpack)', () => {
    test('should replace component config', () => {
        getConfig.mockImplementation(jest.fn().mockReturnValue(fullConfigs));

        const config = { plugins: [] };
        const toggleConfig = componentToggler(config);
        expect(toggleConfig).toBeInstanceOf(Object);
        expect(toggleConfig.plugins.length).toBe(1);
    });

    test('should return untouched webpack config if no toggling components', () => {
        getConfig.mockImplementation(jest.fn().mockReturnValue(emptyConfigs));

        const config = { plugins: [] };
        const toggleConfig = componentToggler(config);
        expect(toggleConfig).toBe(config);
        expect(toggleConfig.plugins.length).toBe(0);
    });
});
