import getConfig from '../../../webpack/getConfig';

import { fullConfigs, configsWithUnusedData, emptyConfigs } from './getConfig.mock';

describe('getConfig(webpack)', () => {
    beforeEach(() => {
        process.env.ENVIRONMENT_NAME = 'arb-dev01';
        process.env.NEXT_PUBLIC_BRAND = 'arbys';
        process.env.LOCALE = 'en-us';
    });
    afterEach(() => {
        jest.unmock('../../../multiBrand/config/arb-dev01/arbys.json');
        jest.resetModules();
    });

    test('should return configs from arbys dev json file', () => {
        jest.mock('../../../multiBrand/config/arb-dev01/arbys.json', jest.fn().mockReturnValue(fullConfigs), {
            virtual: true,
        });
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(info).toBeCalledTimes(2);
        expect(config).toEqual(fullConfigs);
    });

    test('should return configs from "bww-en-ca.json" file', () => {
        process.env.ENVIRONMENT_NAME = 'bww-dev01';
        process.env.NEXT_PUBLIC_BRAND = 'bww';
        process.env.LOCALE = 'en-ca';
        jest.mock('../../../multiBrand/config/bww-dev01/bww-en-ca.json', jest.fn().mockReturnValue(fullConfigs), {
            virtual: true,
        });
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(info).toBeCalledTimes(2);
        expect(config).toEqual(fullConfigs);
    });

    test('should return "toggleFeatures" config from "bww-qa01" environment and "toggleComponents" from "bww-dev01" environment', () => {
        process.env.ENVIRONMENT_NAME = 'bww-qa01';
        process.env.NEXT_PUBLIC_BRAND = 'bww';

        jest.mock(
            '../../../multiBrand/config/bww-qa01/bww.json',
            jest.fn().mockReturnValue({
                toggleFeatures: {
                    showLocationsAsOrderedList: {
                        enabled: true,
                        description: 'Shows locations on the search page as an ordered list',
                    },
                },
            }),
            {
                virtual: true,
            }
        );
        jest.mock(
            '../../../multiBrand/config/bww-dev01/bww.json',
            jest.fn().mockReturnValue({
                toggleComponents: {
                    'some/path': 'other/path/her',
                },
                toggleFeatures: {
                    testOne: {
                        enabled: true,
                    },
                },
            }),
            {
                virtual: true,
            }
        );
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(info).toBeCalledTimes(2);
        expect(config).toEqual({
            toggleFeatures: {
                showLocationsAsOrderedList: {
                    enabled: true,
                    description: 'Shows locations on the search page as an ordered list',
                },
            },
            toggleComponents: {
                'some/path': 'other/path/her',
            },
        });
    });

    test('should return "toggleFeatures" config from "prd01" environment and "toggleComponents" from "arb-dev01" environment', () => {
        process.env.ENVIRONMENT_NAME = 'prd01';
        process.env.NEXT_PUBLIC_BRAND = 'arbys';

        jest.mock(
            '../../../multiBrand/config/prd01/arbys.json',
            jest.fn().mockReturnValue({
                toggleFeatures: {
                    enableFeatureX: {
                        enabled: true,
                    },
                },
            }),
            {
                virtual: true,
            }
        );
        jest.mock(
            '../../../multiBrand/config/arb-dev01/arbys.json',
            jest.fn().mockReturnValue({
                toggleComponents: {
                    'some/path': 'other/path/her',
                },
                toggleFeatures: {
                    testOne: {
                        enabled: true,
                    },
                },
            }),
            {
                virtual: true,
            }
        );
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(info).toBeCalledTimes(2);
        expect(config).toEqual({
            toggleFeatures: {
                enableFeatureX: {
                    enabled: true,
                },
            },
            toggleComponents: {
                'some/path': 'other/path/her',
            },
        });
    });

    test('should return only specified from json file', () => {
        jest.mock('../../../multiBrand/config/arb-dev01/arbys.json', jest.fn().mockReturnValue(configsWithUnusedData), {
            virtual: true,
        });
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(info).toBeCalledTimes(2);
        expect(config).toEqual(fullConfigs);
    });

    test('should return standard empty object if no config data', () => {
        jest.mock('../../../multiBrand/config/arb-dev01/arbys.json', jest.fn().mockReturnValue({}), {
            virtual: true,
        });
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(info).toBeCalledTimes(2);
        expect(config).toEqual(emptyConfigs);
    });

    test('should return standard empty object if no config file', () => {
        jest.mock(
            '../../../multiBrand/config/arb-dev01/arbys.json',
            jest.fn().mockImplementation(() => {
                throw Error('MODULE_NOT_FOUND');
            }),
            {
                virtual: true,
            }
        );
        const info = jest.fn();
        const warn = jest.fn();
        const logger = { info, warn };

        const config = getConfig({ logger });
        expect(warn).toBeCalledTimes(2);
        expect(config).toEqual(emptyConfigs);
    });
});
