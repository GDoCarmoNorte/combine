export const emptyConfigs = {
    toggleComponents: {},
    toggleFeatures: {},
};

export const fullConfigs = {
    toggleComponents: {
        'atoms/button.tsx': 'atoms/arb-button',
    },
    toggleFeatures: {
        'account/signUp': true,
    },
};

export const configsWithUnusedData = {
    ...fullConfigs,
    unused: 'data',
    that: 'will be',
    ignored: 'in getConfigs',
};
