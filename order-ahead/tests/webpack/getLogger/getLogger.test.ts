import getLogger from '../../../webpack/getLogger';

jest.spyOn(console, 'log');
jest.spyOn(console, 'warn');

describe('getLogger(webpack)', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    test('no postfix, info should call console.log', () => {
        const logger = getLogger('Testing');
        logger.info('no postfix, info should call console.log');

        expect(console.log).toHaveBeenCalledTimes(1);
    });

    test('no postfix, warn should call console.warn', () => {
        const logger = getLogger('Testing');
        logger.warn('no postfix, warn should call console.warn');

        expect(console.warn).toHaveBeenCalledTimes(1);
    });

    test('with postfix, info should call console.log', () => {
        const logger = getLogger('Testing', 'test01');
        logger.info('no postfix, warn should call console.warn');

        expect(console.log).toHaveBeenCalledTimes(1);
    });

    test('br should call console.log with empty string', () => {
        const logger = getLogger('Testing', 'test01');
        logger.br();

        expect(console.log).toHaveBeenCalledTimes(1);
        expect(console.log).toHaveBeenCalledWith('');
    });
});
