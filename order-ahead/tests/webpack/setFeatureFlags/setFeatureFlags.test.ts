import setFeatureFlags from '../../../webpack/setFeatureFlags';
import { emptyConfigs, fullConfigs } from '../getConfig/getConfig.mock';

jest.mock('../../../webpack/getConfig');
const getConfig = require('../../../webpack/getConfig');

jest.mock('webpack', () => ({
    DefinePlugin: jest.fn(),
}));

describe('setFeatureFlags(webpack)', () => {
    test('should add empty object as feature flags if no configs', () => {
        getConfig.mockImplementation(jest.fn().mockReturnValue(emptyConfigs));

        const config = { plugins: [] };
        const featureFlagsConfig = setFeatureFlags(config);

        expect(featureFlagsConfig).toBeInstanceOf(Object);
        expect(featureFlagsConfig.plugins.length).toBe(1);
    });

    test('should configs object is feature flags configs provided', () => {
        getConfig.mockImplementation(jest.fn().mockReturnValue(fullConfigs));

        const config = { plugins: [] };
        const featureFlagsConfig = setFeatureFlags(config);

        expect(featureFlagsConfig).toBeInstanceOf(Object);
        expect(featureFlagsConfig.plugins.length).toBe(1);
    });
});
