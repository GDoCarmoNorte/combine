import { ItemModel, IntervalTypeEnumModel, OtherPriceTypeEnumModel } from '../../@generated/webExpApi';

const item: ItemModel = {
    id: 'arb-itm-004-003',
    name: 'Salted Caramel & Chocolate Cookie',
    metadata: {
        SOURCE_PRODUCT_ID: '640212815',
        HAPPY_HOUR_ITEM_ID: '640243447',
    },
    sizeGroupId: 'arb-sig-999-999',
    itemGroupId: 'arb-itg-000-001',
    productGroupId: 'arb-prg-000-009',
    makeItAMeal: false,
    hasChildItems: false,
    availability: {
        isAvailable: true,
        validity: {
            Date: {
                value: '2010-01-01/2099-12-31',
                intervalType: IntervalTypeEnumModel.Date,
                dayOfTheWeek: 'SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY',
            },
            Time: {
                value: '00:00/23:59',
                intervalType: IntervalTypeEnumModel.Time,
                dayOfTheWeek: 'SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY',
            },
        },
    },
    sequence: 1,
    description:
        'Salted caramel and Ghirardelli chocolate baked into a warm cookie. So simple yet so good, we called it exactly what it is.​ *Manufactured in a facility that processes peanuts or tree nuts.',
    price: {
        currentPrice: 1.59,
        metadata: {
            SOURCE_PRODUCT_ID: '640250227',
        },
        otherPrices: {
            HAPPY_HOUR: {
                price: 1,
                priceType: OtherPriceTypeEnumModel.HappyHour,
                metadata: {
                    PLU: '032540',
                    SOURCE_PRODUCT_ID: '640243447',
                },
            },
        },
    },
    nutrition: {
        totalCalories: 430,
        allergenInformation:
            'Contains egg, milk, soy, wheat. Manufactured in a facility that processes peanuts or tree nuts. ',
    },
};

export default item;
