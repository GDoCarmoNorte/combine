export const locationsPage1 = {
    metadata: {
        totalElements: 12,
        totalPages: 2,
        pageNumber: 0,
        pageSize: 10,
        isLastPage: false,
    },
    locations: [
        {
            id: '5398',
            brandName: "Arby's",
            displayName: 'Chardon – Water St',
            timezone: 'America/New_York',
            distance: {
                amount: 0,
                unit: 'mi',
            },
            contactDetails: {
                address: {
                    line1: '417 Water St',
                    line2: '',
                    line3: '',
                    postalCode: '44024',
                    stateProvinceCode: 'OH',
                    countryCode: 'US',
                    city: 'Chardon',
                },
                email: '',
                phone: '(440) 286-7515',
            },
            services: [
                {
                    type: 'PICKUP',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '22:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                    ],
                },
                {
                    type: 'STORE',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '22:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                    ],
                },
                {
                    type: 'DELIVERY',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '22:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                    ],
                },
            ],
            details: {
                latitude: 41.5804712,
                longitude: -81.2104162,
            },
        },
        {
            id: '99982',
            brandName: "Arby's",
            displayName: '5398, Unit',
            status: 'OPEN',
            timezone: 'America/New_York',
            isDigitallyEnabled: true,
            distance: {
                amount: 0.0021575615615117803,
                unit: 'mi',
            },
            contactDetails: {
                address: {
                    line1: '417 WATER ST',
                    line2: '',
                    line3: '',
                    postalCode: '44024',
                    stateProvinceCode: 'OH',
                    countryCode: 'US',
                    city: 'CHARDON',
                },
                email: 'a05398@arbysstores.com',
                phone: '440-286-7515',
            },
            services: [
                {
                    type: 'ORDER_AHEAD',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '22:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
            ],
            details: {
                latitude: 41.5805,
                longitude: -81.2104,
            },
        },
        {
            id: '7838',
            brandName: "Arby's",
            displayName: 'Painesville Township – Mentor Avenue',
            status: 'OPEN',
            timezone: 'America/New_York',
            isDigitallyEnabled: true,
            distance: {
                amount: 8.360809616275075,
                unit: 'mi',
            },
            contactDetails: {
                address: {
                    line1: '2219 MENTOR AVENUE',
                    line2: '',
                    line3: '',
                    postalCode: '44077',
                    stateProvinceCode: 'OH',
                    countryCode: 'US',
                    city: 'PAINESVILLE TOWNSHIP',
                },
                email: 'A07838@arbysstores.com',
                phone: '440-354-8392',
            },
            services: [
                {
                    type: 'ORDER_AHEAD',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'PICKUP',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'DELIVERY',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'HAPPY_HOUR',
                    channel: 'CarryOut',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'STORE',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '23:00',
                            isTwentyFourHourService: null,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: null,
                        },
                    ],
                },
            ],
            details: {
                latitude: 41.6848,
                longitude: -81.2925,
            },
        },
        {
            id: '1830',
            brandName: "Arby's",
            displayName: 'Mentor – Reynolds Rd',
            status: 'OPEN',
            timezone: 'America/New_York',
            isDigitallyEnabled: true,
            distance: {
                amount: 10.27115677284412,
                unit: 'mi',
            },
            contactDetails: {
                address: {
                    line1: '7715 REYNOLDS RD',
                    line2: '',
                    line3: '',
                    postalCode: '44060-5320',
                    stateProvinceCode: 'OH',
                    countryCode: 'US',
                    city: 'MENTOR',
                },
                phone: '440-942-2432',
            },
            services: [
                {
                    type: 'ORDER_AHEAD',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'PICKUP',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'DELIVERY',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'HAPPY_HOUR',
                    channel: 'CarryOut',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'STORE',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '00:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
            ],
            details: {
                latitude: 41.6609,
                longitude: -81.3773,
            },
        },
    ],
};

export const locationsPage2 = {
    metadata: {
        totalElements: 10,
        totalPages: 2,
        pageNumber: 1,
        pageSize: 10,
        isLastPage: true,
    },
    locations: [
        {
            id: '1832',
            brandName: "Arby's",
            displayName: 'Willowick – Vine St',
            status: 'OPEN',
            timezone: 'America/New_York',
            isDigitallyEnabled: true,
            distance: {
                amount: 14.071533506283101,
                unit: 'mi',
            },
            contactDetails: {
                address: {
                    line1: '31219 VINE ST',
                    line2: '',
                    line3: '',
                    postalCode: '44095-3555',
                    stateProvinceCode: 'OH',
                    countryCode: 'US',
                    city: 'WILLOWICK',
                },
                email: 'A01832@arbysstores.com',
                phone: '440-944-7717',
            },
            services: [
                {
                    type: 'ORDER_AHEAD',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'PICKUP',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'DELIVERY',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'HAPPY_HOUR',
                    channel: 'CarryOut',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'STORE',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '01:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
            ],
            details: {
                latitude: 41.6428,
                longitude: -81.4691,
            },
        },
        {
            id: '1399',
            brandName: "Arby's",
            displayName: 'Euclid – Euclid Ave',
            status: 'OPEN',
            timezone: 'America/New_York',
            isDigitallyEnabled: true,
            distance: {
                amount: 15.070429620727111,
                unit: 'mi',
            },
            contactDetails: {
                address: {
                    line1: '26000 EUCLID AVE',
                    line2: '',
                    line3: '',
                    postalCode: '44132-2702',
                    stateProvinceCode: 'OH',
                    countryCode: 'US',
                    city: 'EUCLID',
                },
                email: 'A01399@arbysstores.com',
                phone: '216-731-5394',
            },
            services: [
                {
                    type: 'ORDER_AHEAD',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'PICKUP',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'DELIVERY',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'HAPPY_HOUR',
                    channel: 'CarryOut',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '13:50',
                            endTime: '17:10',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
                {
                    type: 'STORE',
                    hours: [
                        {
                            dayOfWeek: 'SUNDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'MONDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'TUESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'WEDNESDAY',
                            startTime: '10:00',
                            endTime: '02:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'THURSDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'FRIDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                        {
                            dayOfWeek: 'SATURDAY',
                            startTime: '10:00',
                            endTime: '03:00',
                            isTwentyFourHourService: false,
                        },
                    ],
                },
            ],
            details: {
                latitude: 41.5879,
                longitude: -81.5011,
            },
        },
    ],
};

export const allLocationsMock = {
    totalCount: 12,
    locationsByCountry: [
        {
            countryCode: 'US',
            countryName: 'United States',
            count: 9,
            statesOrProvinces: [
                {
                    code: 'mn',
                    name: 'Minnesota',
                    count: 4,
                    cities: [
                        {
                            name: 'Minneapolis',
                            displayName: 'minneapolis',
                            count: 2,
                        },
                        {
                            name: 'Test',
                            displayName: 'test',
                            count: 2,
                        },
                    ],
                },
                {
                    code: 'oh',
                    name: 'Ohio',
                    count: 7,
                    cities: [
                        {
                            name: 'A St. Louis park',
                            displayName: 'a-st--louis-park',
                            count: 3,
                        },
                        {
                            name: 'Omaha',
                            displayName: 'omaha',
                            count: 2,
                        },
                        {
                            name: 'St. Louis park',
                            displayName: 'st--louis-park',
                            count: 2,
                        },
                    ],
                },
                {
                    code: 'test',
                    name: 'TEST',
                    count: 3,
                    cities: [
                        {
                            name: 'St. Louis park',
                            displayName: 'st--louis-park',
                            count: 3,
                        },
                    ],
                },
            ],
        },
        {
            countryCode: 'CA',
            countryName: 'Canada',
            count: 3,
            statesOrProvinces: [
                {
                    code: 'on',
                    name: 'Ontario',
                    count: 3,
                    cities: [
                        {
                            name: 'East Gwillimbury',
                            displayName: 'east-gwillimbury',
                            count: 1,
                        },
                        {
                            name: 'Guelph',
                            displayName: 'guelph',
                            count: 1,
                        },
                        {
                            name: 'Oshawa',
                            displayName: 'oshawa',
                            count: 1,
                        },
                    ],
                },
            ],
        },
    ],
};

export const emptyAllLocationsMock = {
    totalCount: 0,
    locationsByCountry: [],
};

export const locationDetailsUrlMock =
    'us/oh/gahanna/1380-cherry-bottom-rd--village-square-at-cherry-bottom/sports-bar-12';

export const locationUrlsMock = {
    urls: [
        {
            address: '1380-cherry-bottom-rd--village-square-at-cherry-bottom',
            cityName: 'gahanna',
            countryCode: 'us',
            stateOrProvinceCode: 'oh',
            storeId: 'sports-bar-12',
        },
    ],
};

export const locationUrlsMock2 = {
    urls: [
        {
            cityName: 'gahanna',
            countryCode: 'us',
            stateOrProvinceCode: 'oh',
        },
    ],
};

export const emptyLocationsByStateMock = {
    countryCode: 'US',
    countryName: 'United States',
};

export const locationDescriptionMapping = {
    GO_LOCATION: 'Go location description',
    EXPRESS_LOCATION: 'Express location description',
    GENERAL_LOCATION: 'General location description',
};

export const locationsByStateMock = {
    countryCode: 'US',
    countryName: 'United States',
    stateOrProvince: {
        code: 'oh',
        name: 'Ohio',
        count: 1,
        cities: [
            {
                name: 'Gahanna',
                displayName: 'gahanna',
                count: 1,
                locations: [
                    {
                        id: '12',
                        displayName: 'Gahanna store',
                        url: 'us/oh/gahanna/1380-cherry-bottom-rd--village-square-at-cherry-bottom/sports-bar-12',
                        contactDetails: {
                            address: {
                                line: '1380 Cherry Bottom Rd. Village Square at Cherry Bottom',
                                postalCode: '43230-6771',
                                cityName: 'Gahanna',
                                stateProvinceCode: 'OH',
                            },
                            phone: '614-478-7972',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=1380%20Cherry%20Bottom%20Rd.%20Village%20Square%20at%20Cherry%20Bottom%2C%20Gahanna%2C%20OH%2C%2043230-6771',
                        services: ['UFC_VIEWING'],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '23:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 40.055344,
                            longitude: -82.884732,
                        },
                        timezone: 'America/New_York',
                    },
                ],
            },
        ],
    },
    locationDescriptionMapping,
};

export const tapListResponseMock: any = {
    beersByType: {
        CRAFT: {
            count: 1,
            items: [
                {
                    name: 'Angry Orchard Crisp Apple Cider',
                    category: 'Ciders & Specialty',
                    alcoholPercentage: '5% ABV',
                    caloriesBySize: {
                        REGULAR: 230,
                        TALL: 320,
                    },
                },
            ],
        },
    },
};

export default locationsPage1;
