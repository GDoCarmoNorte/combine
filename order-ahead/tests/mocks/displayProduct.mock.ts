export default {
    productType: 'MEAL',
    displayName: 'Beef n Cheddar',
    productId: 'arb-itm-000-021',
    mainProductId: 'arb-itm-000-021',
    mainProductSectionName: 'Sandwich',
    productSections: [
        {
            productSectionType: 'main',
            productSectionDisplayName: 'Sandwich',
            productGroupId: 'arb-prg-000-019',
        },
        {
            productSectionType: 'side',
            productSectionDisplayName: 'Side',
            productGroupId: 'arb-prg-000-008',
        },
        {
            productSectionType: 'drink',
            productSectionDisplayName: 'Drink',
            productGroupId: 'arb-prg-000-010',
        },
    ],
    displayProductDetails: {
        calories: 410,
        defaultQuantity: 0,
        displayName: 'Curly Fries',
        maxQuantity: 1,
        minQuantity: 0,
        price: 1.89,
        productId: 'arb-itm-002-002',
        quantity: 1,
    },
    price: 6.49,
    calories: 1020,
};
