const singleProductMock = {
    lineItemId: 201,
    description: 'Chicken Bacon & Swiss Sandwich',
    price: 5.39,
    productId: 'arb-itm-000-075',
    childItems: [],
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            modifiers: [
                {
                    productId: 'arb-itm-006-015',
                    price: 0.99,
                    quantity: 1,
                },
                {
                    productId: 'arb-itm-006-002',
                    price: 0.79,
                    actionCode: 'ADD',
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            modifiers: [
                {
                    productId: 'arb-itm-007-007',
                    price: 0.49,
                    quantity: 1,
                },
                {
                    productId: 'arb-itm-007-001',
                    price: 0.49,
                    actionCode: 'ADD',
                    quantity: 1,
                },
                {
                    productId: 'arb-itm-007-004',
                    price: 0.49,
                    actionCode: 'ADD',
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-003',
            modifiers: [
                {
                    productId: 'arb-itm-008-011',
                    price: 0.29,
                    quantity: 1,
                },
                {
                    productId: 'arb-itm-008-005',
                    price: 0.29,
                    quantity: 1,
                },
                {
                    productId: 'arb-itm-008-004',
                    price: 0,
                    actionCode: 'ADD',
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-004',
            modifiers: [
                {
                    productId: 'arb-itm-009-014',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-005',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-006',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-008',
            modifiers: [
                {
                    productId: 'arb-itm-013-003',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
    quantity: 1,
};

const mealProductMock = {
    lineItemId: 101,
    description: 'Roast Beef',
    price: 0,
    productId: 'arb-itm-000-009',
    childItems: [
        {
            lineItemId: 102,
            description: 'Roast Beef',
            price: 4.01,
            productId: 'arb-itm-000-002',
            childItems: [],
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-006-023',
                            price: 2,
                            actionCode: 'ADD',
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            actionCode: 'ADD',
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-007-002',
                            price: 0.49,
                            actionCode: 'ADD',
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            quantity: 1,
        },
        {
            lineItemId: 103,
            description: 'Curly Fries',
            price: 1.89,
            productId: 'arb-itm-002-002',
            childItems: [],
            modifierGroups: [
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
            ],
            quantity: 1,
        },
        {
            lineItemId: 104,
            description: 'Coca-Cola®',
            price: 1.69,
            productId: 'arb-itm-003-001',
            childItems: [],
            modifierGroups: [
                {
                    productId: 'arb-prg-001-011',
                    modifiers: [
                        {
                            productId: 'arb-itm-016-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-012',
                    modifiers: [
                        {
                            productId: 'arb-itm-017-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            quantity: 1,
        },
    ],
    modifierGroups: [],
    quantity: 1,
};

export { singleProductMock, mealProductMock };
