import { ConfigurationValues } from '../../redux/configuration';

export default {
    isOAEnabled: true,
    isApplePayEnabled: true,
    isDeliveryEnabled: true,
    isGooglePayEnabled: true,
    isTippingEnabled: true,
    personalizationRefreshFrequency: 10,
    configurationRefreshFrequency: 10,
} as ConfigurationValues;
