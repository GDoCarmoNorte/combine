export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: 'Dw0iOxJ4mHPwcT97b8HyB',
        type: 'Entry',
        createdAt: '2020-09-23T14:10:02.265Z',
        updatedAt: '2020-10-30T19:54:59.138Z',
        environment: {
            sys: {
                id: 'DBBP-5902',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 5,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'product',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: "Classic Beef 'N Cheddar Meal",
        nameInUrl: 'classic-beef-n-cheddar-meal',
        image: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '102zZE5kuKGSWzDzpS7SiB',
                type: 'Asset',
                createdAt: '2020-09-25T17:50:04.467Z',
                updatedAt: '2020-09-25T17:50:04.467Z',
                environment: {
                    sys: {
                        id: 'DBBP-5902',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'Meals Beef N Cheddar',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/102zZE5kuKGSWzDzpS7SiB/f7fe8a5e6444761a55e8468a81067526/Website_Meals_BNC.png',
                    details: {
                        size: 2736605,
                        image: {
                            width: 4000,
                            height: 3000,
                        },
                    },
                    fileName: 'Website_Meals_BNC.png',
                    contentType: 'image/png',
                },
            },
        },
        productId: 'ARBYS-WEBOA-640239548',
        showProductBadge: true,
    },
};
