export default {
    quantity: 1,
    productId: 'arb-itm-000-001',
    price: 3.49,
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            modifiers: [
                {
                    productId: 'arb-itm-006-001',
                    price: 2,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-003',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-004',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-005',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-006',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-008',
            modifiers: [
                {
                    price: 0,
                    productId: 'arb-itm-013-001',
                    quantity: 1,
                },
            ],
        },
    ],
};
