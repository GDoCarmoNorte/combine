export default {
    brandId: 'ARB',
    sellingChannel: 'WEBOA',
    subTotal: 10.77,
    total: 11.5,
    items: [
        {
            lineItemId: 101,
            description: 'Greek Gyro',
            price: 0,
            productId: 'arb-itm-000-059',
            childItems: [
                {
                    lineItemId: 102,
                    description: 'Greek Gyro',
                    price: 3.11,
                    productId: 'arb-itm-000-055',
                    childItems: [],
                    modifierGroups: [
                        {
                            productId: 'arb-prg-001-001',
                            modifiers: [
                                {
                                    productId: 'arb-itm-006-011',
                                    price: 2,
                                    quantity: 1,
                                },
                                {
                                    productId: 'arb-itm-006-012',
                                    price: 2,
                                    actionCode: 'ADD',
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'arb-prg-001-002',
                            modifiers: [
                                {
                                    productId: 'arb-itm-007-004',
                                    price: 0.49,
                                    actionCode: 'ADD',
                                    quantity: 1,
                                },
                                {
                                    productId: 'arb-itm-007-005',
                                    price: 0.49,
                                    actionCode: 'ADD',
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'arb-prg-001-003',
                            modifiers: [
                                {
                                    productId: 'arb-itm-008-011',
                                    price: 0.29,
                                    quantity: 1,
                                },
                                {
                                    productId: 'arb-itm-008-004',
                                    price: 0,
                                    quantity: 1,
                                },
                                {
                                    productId: 'arb-itm-008-005',
                                    price: 0.29,
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'arb-prg-001-004',
                            modifiers: [
                                {
                                    productId: 'arb-itm-009-013',
                                    price: 0,
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'arb-prg-001-005',
                            modifiers: [],
                        },
                        {
                            productId: 'arb-prg-001-006',
                            modifiers: [],
                        },
                        {
                            productId: 'arb-prg-001-008',
                            modifiers: [
                                {
                                    productId: 'arb-itm-013-007',
                                    price: 0,
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'arb-prg-001-010',
                            modifiers: [
                                {
                                    productId: 'arb-itm-015-001',
                                    price: 0,
                                    quantity: 1,
                                },
                            ],
                        },
                    ],
                    quantity: 1,
                },
                {
                    lineItemId: 103,
                    description: 'Curly Fries',
                    price: 2.69,
                    productId: 'arb-itm-002-004',
                    childItems: [],
                    modifierGroups: [
                        {
                            productId: 'arb-prg-001-006',
                            modifiers: [],
                        },
                        {
                            productId: 'arb-prg-001-005',
                            modifiers: [],
                        },
                    ],
                    quantity: 1,
                },
                {
                    lineItemId: 104,
                    description: 'Coca-Cola®',
                    price: 1.99,
                    productId: 'arb-itm-003-003',
                    childItems: [],
                    modifierGroups: [
                        {
                            productId: 'arb-prg-001-011',
                            modifiers: [
                                {
                                    productId: 'arb-itm-016-001',
                                    price: 0,
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'arb-prg-001-012',
                            modifiers: [
                                {
                                    productId: 'arb-itm-017-001',
                                    price: 0,
                                    quantity: 1,
                                },
                            ],
                        },
                    ],
                    quantity: 1,
                },
            ],
            modifierGroups: [],
            quantity: 1,
        },
    ],
    fulfillment: {
        fulfillmentType: 'PICKUP',
        asap: true,
        storeLocation: {
            locationId: '99982',
        },
    },
    tallyTime: '2021-03-01T11:22:40.602Z',
    tax: 0.73,
};
