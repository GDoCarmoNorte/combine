export default {
    rootProduct: {
        displayName: 'Double Roast Beef Meal',
        sizeSelections: [
            {
                displayName: 'Double Roast Beef',
                name: 'Small',
                size: 'SM',
                isSelected: true,
                productId: 'ARBYS-WEBOA-640225416',
                enhancedAttributes: {
                    calories: '510',
                    nutritionalFacts: [
                        {
                            name: 'Calories',
                            value: '510',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '210',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '95',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '2',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '24',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '38',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '9',
                        },
                        {
                            name: 'Serving Weight (g)',
                            value: '239',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1610',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '5',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '38',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '1.5',
                        },
                    ],
                    allergicInformation: 'Contains soy, wheat. ',
                    ingredientStatement:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                },
            },
            {
                displayName: 'Double Roast Beef',
                name: 'Medium',
                size: 'MD',
                isSelected: false,
                productId: 'ARBYS-WEBOA-640224053',
                enhancedAttributes: {
                    calories: '510',
                    nutritionalFacts: [
                        {
                            name: 'Calories',
                            value: '510',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '210',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '95',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '2',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '24',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '38',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '9',
                        },
                        {
                            name: 'Serving Weight (g)',
                            value: '239',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1610',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '5',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '38',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '1.5',
                        },
                    ],
                    allergicInformation: 'Contains soy, wheat. ',
                    ingredientStatement:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                },
            },
            {
                displayName: 'Double Roast Beef',
                name: 'Large',
                size: 'LG',
                isSelected: false,
                productId: 'ARBYS-WEBOA-640224054',
                enhancedAttributes: {
                    calories: '510',
                    nutritionalFacts: [
                        {
                            name: 'Calories',
                            value: '510',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '210',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '95',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '2',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '24',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '38',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '9',
                        },
                        {
                            name: 'Serving Weight (g)',
                            value: '239',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1610',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '5',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '38',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '1.5',
                        },
                    ],
                    allergicInformation: 'Contains soy, wheat. ',
                    ingredientStatement:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                },
            },
        ],
    },
    childItems: [
        {
            displayName: 'Double Roast Beef',
            sizeSelections: [
                {
                    displayName: 'Double Roast Beef',
                    name: 'Small',
                    size: 'SM',
                    isSelected: false,
                    productId: 'ARBYS-WEBOA-640224052',
                    enhancedAttributes: {
                        calories: '510',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '510',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '210',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '95',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '2',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '24',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '38',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '9',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '239',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '1610',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '5',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '38',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '1.5',
                            },
                        ],
                        allergicInformation: 'Contains soy, wheat. ',
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
                {
                    displayName: 'Double Roast Beef',
                    name: 'Medium',
                    size: 'MD',
                    isSelected: false,
                    productId: 'ARBYS-WEBOA-640224053',
                    enhancedAttributes: {
                        calories: '510',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '510',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '210',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '95',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '2',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '24',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '38',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '9',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '239',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '1610',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '5',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '38',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '1.5',
                            },
                        ],
                        allergicInformation: 'Contains soy, wheat. ',
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
                {
                    displayName: 'Double Roast Beef',
                    name: 'Large',
                    size: 'LG',
                    isSelected: true,
                    productId: 'ARBYS-WEBOA-640224054',
                    enhancedAttributes: {
                        calories: '510',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '510',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '210',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '95',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '2',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '24',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '38',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '9',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '239',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '1610',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '5',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '38',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '1.5',
                            },
                        ],
                        allergicInformation: 'Contains soy, wheat. ',
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
            ],
        },
        {
            displayName: 'Curly Fries',
            sizeSelections: [
                {
                    displayName: 'Curly Fries',
                    name: 'Small',
                    size: 'SM',
                    isSelected: false,
                    productId: 'ARBYS-WEBOA-640223380',
                    enhancedAttributes: {
                        calories: '410',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '410',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '200',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '0',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '5',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '22',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '5',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '3',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '128',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '940',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '0',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '49',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '0',
                            },
                        ],
                        allergicInformation:
                            'Contains wheat. Menu item is cooked in the same oil as other items that contains egg, milk, soy, fish (where available). ',
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
                {
                    displayName: 'Curly Fries',
                    name: 'Medium',
                    size: 'MD',
                    isSelected: false,
                    productId: 'ARBYS-WEBOA-640223381',
                    enhancedAttributes: {
                        calories: '550',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '550',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '260',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '0',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '6',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '29',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '6',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '4',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '170',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '1250',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '0',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '65',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '0',
                            },
                        ],
                        allergicInformation:
                            'Contains wheat. Menu item is cooked in the same oil as other items that contains egg, milk, soy, fish (where available). ',
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
                {
                    displayName: 'Curly Fries',
                    name: 'Large',
                    size: 'LG',
                    isSelected: true,
                    productId: 'ARBYS-WEBOA-640223382',
                    enhancedAttributes: {
                        calories: '650',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '650',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '310',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '0',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '7',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '35',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '8',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '5',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '201',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '1480',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '0',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '77',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '0',
                            },
                        ],
                        allergicInformation:
                            'Contains wheat. Menu item is cooked in the same oil as other items that contains egg, milk, soy, fish (where available). ',
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
            ],
        },
        {
            displayName: 'Coca-Cola®',
            sizeSelections: [
                {
                    displayName: 'Coca-Cola®',
                    name: 'Small',
                    size: 'SM',
                    isSelected: false,
                    productId: 'ARBYS-WEBOA-640691952-SM',
                    enhancedAttributes: {
                        calories: '160',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '160',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '0',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '0',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '0',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '0',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '0',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '0',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '0',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '35',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '44',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '44',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '0',
                            },
                        ],
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
                {
                    displayName: 'Coca-Cola®',
                    name: 'Medium',
                    size: 'MD',
                    isSelected: false,
                    productId: 'ARBYS-WEBOA-640691952-MD',
                    enhancedAttributes: {
                        calories: '220',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '220',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '0',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '0',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '0',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '0',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '0',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '0',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '0',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '50',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '60',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '60',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '0',
                            },
                        ],
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
                {
                    displayName: 'Coca-Cola®',
                    name: 'Large',
                    size: 'LG',
                    isSelected: true,
                    productId: 'ARBYS-WEBOA-640691952-LG',
                    enhancedAttributes: {
                        calories: '300',
                        nutritionalFacts: [
                            {
                                name: 'Calories',
                                value: '300',
                            },
                            {
                                name: 'Calories from Fat',
                                value: '0',
                            },
                            {
                                name: 'Cholesterol (mg)',
                                value: '0',
                            },
                            {
                                name: 'Dietary Fiber (g)',
                                value: '0',
                            },
                            {
                                name: 'Fat - Total (g)',
                                value: '0',
                            },
                            {
                                name: 'Proteins (g)',
                                value: '0',
                            },
                            {
                                name: 'Saturated Fat (g)',
                                value: '0',
                            },
                            {
                                name: 'Serving Weight (g)',
                                value: '0',
                            },
                            {
                                name: 'Sodium (mg)',
                                value: '300',
                            },
                            {
                                name: 'Sugars (g)',
                                value: '82',
                            },
                            {
                                name: 'Total Carbohydrates (g)',
                                value: '82',
                            },
                            {
                                name: 'Trans Fat (g)',
                                value: '0',
                            },
                        ],
                        ingredientStatement:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
                    },
                },
            ],
        },
    ],
};
