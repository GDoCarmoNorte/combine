import { ILocationByStateOrProvinceDetailsModel, IServiceTypeModel } from '../../@generated/webExpApi';

export const locationDetailsMock: ILocationByStateOrProvinceDetailsModel = {
    id: '438',
    displayName: 'Bear, DE',
    url: 'us/de/bear/1887-pulaski-hwy/sports-bar-438',
    contactDetails: {
        address: {
            line: '1887 Pulaski Hwy',
            postalCode: '19701-1731',
            cityName: 'Bear',
            stateProvinceCode: 'DE',
        },
        phone: '302-832-3900',
    },
    addressMapLink:
        'https://www.google.com/maps?hl=en&saddr=current+location&daddr=1887%20Pulaski%20Hwy%2C%20Bear%2C%20DE%2C%2019701-1731',
    services: [IServiceTypeModel.Wifi, IServiceTypeModel.BlazingRewards, IServiceTypeModel.ExpressLocation],
    paymentMethods: [],
    hoursByDay: {
        Mon: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Tue: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Wed: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Thu: {
            start: '11:00',
            end: '23:00',
            isOpen24Hs: false,
        },
        Sat: {
            start: '11:00',
            end: '20:00',
            isOpen24Hs: true,
        },
        Sun: {
            start: '12:00',
            end: '23:00',
            isOpen24Hs: false,
        },
    },
    timezone: 'America/New_York',
    isOnlineOrderAvailable: true,
    isClosed: false,
};
