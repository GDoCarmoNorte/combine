export default {
    productDetailsPagePaths: [
        {
            menuCategoryUrl: 'chicken',
            productDetailsPageUrl: 'chicken-bacon-swiss-sandwich',
            productIds: ['ARBYS-WEBOA-640239548'],
            canonicalPath: 'http://localhost:3000/menu/chicken/chicken-bacon-swiss-sandwich',
            productPath: '/menu/chicken/chicken-bacon-swiss-sandwich',
        },
    ],
    dealItemsById: {
        default: {
            fields: {
                image: 'mocked',
            },
        },
    },
};
