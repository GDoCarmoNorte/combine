export default {
    idpCustomerId: 'dd69f7bd-9551-4a55-8dc2-a373a1aeee96',
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@mail.com',
    birthDate: '11/11',
    references: [
        { type: 'UNKNOWN', id: 'auth0|60acff74733e230070e7b316' },
        {
            type: 'AUTH0',
            id: 'auth0|60acff74733e230070e7b316',
        },
    ],
    phones: [{ number: '123-123-1231', isPreferred: true }],
    preferences: { postalCode: '00000', marketing: [{ type: 'EMAIL' }] },
    termsAndConditions: {
        type: 'WEBOA',
        version: '0.0',
        isAccepted: true,
        acceptedTime: '2021-06-02T07:54:21.377+00:00',
        link: 'http://arbys.dev.irb.digital/terms-of-use',
    },
};
