export default {
    entry: {
        modifierGroups: [
            {
                productId: 'IDPModifierGroup-0001',
                metadata: { MODIFIER_GROUP_TYPE: 'Extras' },
                isOnSideChecked: false,
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-0002',
                metadata: { MODIFIER_GROUP_TYPE: 'Extras' },
                isOnSideChecked: false,
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-0003',
                metadata: { MODIFIER_GROUP_TYPE: 'Extras' },
                isOnSideChecked: false,
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-12305',
                metadata: { MODIFIER_GROUP_ID: '12305', MODIFIER_GROUP_TYPE: 'Modifications' },
                isOnSideChecked: false,
                modifiers: [
                    { productId: 'IDPModifier-16411', quantity: 1, price: 0 },
                    { productId: 'IDPModifier-16410', quantity: 1, price: 0 },
                    { productId: 'IDPModifier-17317', quantity: 1, price: 0 },
                    { productId: 'IDPModifier-16414', quantity: 1, price: 0 },
                    { productId: 'IDPModifier-27449', quantity: 1, price: 0 },
                    { productId: 'IDPModifier-16409', quantity: 1, price: 0 },
                ],
                isOnSideOptionDisabled: false,
            },
            {
                productId: 'IDPModifierGroup-14295',
                metadata: { MODIFIER_GROUP_ID: '14295', MODIFIER_GROUP_TYPE: 'Sauces' },
                isOnSideChecked: false,
                modifiers: [{ productId: 'IDPModifier-23901', price: 0, quantity: 1 }],
            },
            {
                productId: 'IDPModifierGroup-16051',
                metadata: { MODIFIER_GROUP_ID: '16051', MODIFIER_GROUP_TYPE: 'Selections' },
                isOnSideChecked: false,
                modifiers: [
                    {
                        productId: 'IDPModifier-5874',
                        quantity: 1,
                        price: 0,
                        modifierGroups: [
                            {
                                productId: 'IDPModifierGroup-11513',
                                metadata: {
                                    MODIFIER_GROUP_ID: '11513',
                                    MODIFIER_GROUP_TYPE: 'Modifications',
                                },
                                isOnSideChecked: false,
                                modifiers: [
                                    { productId: 'IDPModifier-13117', quantity: 1, price: 0.75 },
                                    { productId: 'IDPModifier-13118', quantity: 1, price: 1.35 },
                                ],
                                isOnSideOptionDisabled: false,
                            },
                        ],
                    },
                ],
            },
            {
                productId: 'IDPModifierGroup-9124',
                metadata: { MODIFIER_GROUP_ID: '9124' },
                isOnSideChecked: false,
                modifiers: [{ productId: 'IDPModifier-3697', quantity: 1, price: 0 }],
            },
        ],
        price: 15.79,
        productId: 'IDPSalesItem-4858',
        quantity: 2,
        childExtras: [],
        lineItemId: 1,
    },
    isAvailable: true,
    markedAsRemoved: false,
    contentfulProduct: {
        fields: {
            name: 'Buffalo Bleu Burger',
            nameInUrl: 'buffalo-bleu-burger',
            image: {
                metadata: { tags: [] },
                fields: {
                    title: 'Buffalo Bleu Burger',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/5XBSFNCiNb6b1zWpjqF484/c2721aeb43c29f17d47a825a46194f3b/Buffalo_Bleu_Burger.png',
                        details: { size: 1560319, image: { width: 4000, height: 3000 } },
                        fileName: 'Buffalo Bleu Burger.png',
                        contentType: 'image/png',
                    },
                },
            },
            productId: 'IDPSalesItem-4858',
            metaDescription:
                'Enjoy our Buffalo Bleu Burger when you order delivery or pick it up yourself from the nearest Buffalo Wild Wings to you.',
            isVisible: true,
        },
    },
    entryPath: {
        href: '/menu/[menuCategoryUrl]/[productDetailsPageUrl]',
        as: '/menu/burgers/buffalo-bleu-burger',
    },
};
