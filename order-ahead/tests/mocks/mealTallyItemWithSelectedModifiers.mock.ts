export default {
    price: 5.99,
    productId: 'arb-itm-000-005',
    quantity: 1,
    childItems: [
        {
            price: 2.21,
            productId: 'arb-itm-000-001',
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            price: 0,
                            productId: 'arb-itm-013-001',
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            price: 1.99,
            productId: 'arb-itm-002-002',
            quantity: 1,
            modifierGroups: [
                {
                    modifiers: [],
                    productId: 'arb-prg-001-006',
                },
                {
                    modifiers: [],
                    productId: 'arb-prg-001-005',
                },
            ],
        },
        {
            price: 1.79,
            productId: 'arb-itm-003-001',
            quantity: 1,
            modifierGroups: [
                { productId: 'arb-prg-001-011', modifiers: { productId: 'arb-itm-016-001', price: 0, quantity: 1 } },
                { productId: 'arb-prg-001-012', modifiers: { productId: 'arb-itm-017-001', price: 0, quantity: 1 } },
            ],
        },
    ],
};
