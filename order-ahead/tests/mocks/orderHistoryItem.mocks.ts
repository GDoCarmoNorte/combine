export const productsByIdMock = {
    'IDPSalesItem-5289': {
        metadata: {
            tags: [],
        },
        sys: {
            space: {
                sys: {
                    type: 'Link',
                    linkType: 'Space',
                    id: 'l5fkpck1mwg3',
                },
            },
            id: '53KcHxtih5mhpk5qGLSbYY',
            type: 'Entry',
            createdAt: '2021-08-03T13:35:05.081Z',
            updatedAt: '2021-09-21T22:06:07.995Z',
            environment: {
                sys: {
                    id: 'qa',
                    type: 'Link',
                    linkType: 'Environment',
                },
            },
            revision: 3,
            contentType: {
                sys: {
                    type: 'Link',
                    linkType: 'ContentType',
                    id: 'product',
                },
            },
            locale: 'en-US',
        },
        fields: {
            name: 'BOGO 10 Traditional Wings (20 Total)',
            nameInUrl: 'bogo-10-traditional-wings',
            image: {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '3QYdRv2ydHBMlshKEUogWo',
                    type: 'Asset',
                    createdAt: '2021-09-13T19:25:30.976Z',
                    updatedAt: '2021-09-13T19:25:30.976Z',
                    environment: {
                        sys: {
                            id: 'qa',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'Traditional Wings',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/3QYdRv2ydHBMlshKEUogWo/975b1b974285226f06e89b3470e49f37/Traditional_Wings.png',
                        details: {
                            size: 1195833,
                            image: {
                                width: 4000,
                                height: 3000,
                            },
                        },
                        fileName: 'Traditional Wings.png',
                        contentType: 'image/png',
                    },
                },
            },
            productId: 'IDPSalesItem-5289',
            metaDescription: 'coming soon',
            isVisible: true,
        },
    },
};

export const orderHistoryItemMock = {
    id: '33016',
    dateTime: new Date('2021-10-12T12:09:09.820Z'),
    statusText: 'FULFILLED',
    paymentMethods: [{ type: 'TOKEN', cardIssuer: 'AmericanExpress', maskedCardNumber: '0005' }],
    fulfillment: {
        type: 'PickUp',
        location: {
            id: '4',
            displayName: 'SB Lab 9017',
            url: 'us/mn/richfield/84-w-78th-st/sports-bar-4',
            contactDetails: {
                address: {
                    line1: '84 W 78th St.',
                    postalCode: '55423',
                    stateProvinceCode: 'MN',
                    countryCode: 'US',
                },
            },
        },
    },
    subTotalBeforeDiscounts: 9.72,
    discountsAmount: 3.37,
    tax: 0.65,
    total: 13.37,
    storeTip: 3,
    products: [
        {
            id: 'IDPSalesItem-5289',
            description: 'BOGO 10 Traditional Wings (20 Total)',
            quantity: 1,
            price: 12.99,
            children: [
                {
                    lineItemId: 1,
                    productId: 'IDPSalesItem-5289',
                    price: 12.99,
                    quantity: 1,
                    description: 'BOGO 10 Traditional Wings (20 Total)',
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup12564',
                            modifiers: [
                                {
                                    lineItemId: 2,
                                    productId: 'IDPModifier17273',
                                    price: 0,
                                    quantity: 1,
                                    description: 'Parmesan Garlic',
                                    displayName: 'Parmesan Garlic',
                                },
                            ],
                        },
                        {
                            productId: 'IDPModifierGroup11686',
                            modifiers: [
                                {
                                    lineItemId: 3,
                                    productId: 'IDPModifier27451',
                                    price: 0,
                                    quantity: 1,
                                    description: 'No Dips & Veggies',
                                    displayName: 'No Dips & Veggies',
                                },
                            ],
                        },
                    ],
                },
            ],
            priceWithModifiersBeforeDiscounts: 12.99,
            priceWithModifiersAfterDiscounts: 9.620000000000001,
            lineItemId: 1,
            modifiers: [
                { id: 'IDPModifier17273', unitPrice: 0, totalPrice: 0, quantity: 1 },
                { id: 'IDPModifier27451', unitPrice: 0, totalPrice: 0, quantity: 1 },
            ],
            discounts: [{ amount: 3.37, quantity: 1 }],
        },
    ],
};

export const orderHistoryItemMockNotFulfilled = {
    id: '33016',
    dateTime: new Date('2021-10-12T12:09:09.820Z'),
    statusText: 'CREATED',
    paymentMethods: [{ type: 'TOKEN', cardIssuer: 'AmericanExpress', maskedCardNumber: '0005' }],
    fulfillment: {
        type: 'PickUp',
        location: {
            id: '4',
            displayName: 'SB Lab 9017',
            url: 'us/mn/richfield/84-w-78th-st/sports-bar-4',
            contactDetails: {
                address: {
                    line1: '84 W 78th St.',
                    postalCode: '55423',
                    stateProvinceCode: 'MN',
                    countryCode: 'US',
                },
            },
        },
    },
    subTotalBeforeDiscounts: 9.72,
    discountsAmount: 3.37,
    tax: 0.65,
    total: 13.37,
    storeTip: 3,
    products: [
        {
            id: 'IDPSalesItem-5289',
            description: 'BOGO 10 Traditional Wings (20 Total)',
            quantity: 1,
            price: 12.99,
            children: [
                {
                    lineItemId: 1,
                    productId: 'IDPSalesItem-5289',
                    price: 12.99,
                    quantity: 1,
                    description: 'BOGO 10 Traditional Wings (20 Total)',
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup12564',
                            modifiers: [
                                {
                                    lineItemId: 2,
                                    productId: 'IDPModifier17273',
                                    price: 0,
                                    quantity: 1,
                                    description: 'Parmesan Garlic',
                                    displayName: 'Parmesan Garlic',
                                },
                            ],
                        },
                        {
                            productId: 'IDPModifierGroup11686',
                            modifiers: [
                                {
                                    lineItemId: 3,
                                    productId: 'IDPModifier27451',
                                    price: 0,
                                    quantity: 1,
                                    description: 'No Dips & Veggies',
                                    displayName: 'No Dips & Veggies',
                                },
                            ],
                        },
                    ],
                },
            ],
            priceWithModifiersBeforeDiscounts: 12.99,
            priceWithModifiersAfterDiscounts: 9.620000000000001,
            lineItemId: 1,
            modifiers: [
                { id: 'IDPModifier17273', unitPrice: 0, totalPrice: 0, quantity: 1 },
                { id: 'IDPModifier27451', unitPrice: 0, totalPrice: 0, quantity: 1 },
            ],
            discounts: [{ amount: 3.37, quantity: 1 }],
        },
    ],
};

export const domainProductsMock = {
    'IDPSalesItem-5289': {
        id: 'IDPSalesItem-5289',
        name: 'Traditional Wings',
        metadata: { SOURCE_PRODUCT_ID: 'SalesItem-5289', POS_ID: '17797' },
        categoryIds: ['IDPSubMenu-307'],
        sizeGroupId: 'IDPSizeGroup-0020',
        itemGroupId: 'IDPItemGroup-0001',
        productGroupId: 'IDPProductGroup-0006',
        makeItAMeal: false,
        hasChildItems: false,
        sequence: 1,
        description: 'Lorem Ipsum',
        itemModifierGroups: [
            {
                productGroupId: 'IDPModifierGroup-0001',
                metadata: { MODIFIER_GROUP_TYPE: 'Extras' },
                sequence: 60,
                max: 10,
                min: 0,
                itemModifiers: {
                    'IDPSalesItem-4878': {
                        sequence: 8,
                        max: 10,
                        min: 0,
                        defaultQuantity: 0,
                        itemId: 'IDPSalesItem-4878',
                        overridePrice: {},
                    },
                    'IDPSalesItem-4879': {
                        sequence: 9,
                        max: 10,
                        min: 0,
                        defaultQuantity: 0,
                        itemId: 'IDPSalesItem-4879',
                        overridePrice: {},
                    },
                },
            },
        ],
        nutrition: { totalCalories: 1440, caloriesPerServing: 1440 },
        isSaleable: true,
    },
};

export const transformedOrderItemMock = {
    id: '33016',
    dateTime: new Date('2021-10-12T12:09:09.820Z'),
    statusText: 'FULFILLED',
    paymentMethods: [{ type: 'TOKEN', cardIssuer: 'AmericanExpress', maskedCardNumber: '0005' }],
    fulfillment: {
        type: 'PickUp',
        location: {
            id: '4',
            displayName: 'SB Lab 9017',
            url: 'us/mn/richfield/84-w-78th-st/sports-bar-4',
            contactDetails: {
                address: { line1: '84 W 78th St.', postalCode: '55423', stateProvinceCode: 'MN', countryCode: 'US' },
            },
        },
    },
    subTotalBeforeDiscounts: 9.72,
    discountsAmount: 3.37,
    tax: 0.65,
    total: 13.37,
    storeTip: 3,
    products: [
        {
            id: 'IDPSalesItem-5289',
            description: 'BOGO 10 Traditional Wings (20 Total)',
            quantity: 1,
            price: 12.99,
            children: [
                {
                    lineItemId: 1,
                    productId: 'IDPSalesItem-5289',
                    price: 12.99,
                    quantity: 1,
                    description: 'BOGO 10 Traditional Wings (20 Total)',
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup12564',
                            modifiers: [
                                {
                                    lineItemId: 2,
                                    productId: 'IDPModifier17273',
                                    price: 0,
                                    quantity: 1,
                                    description: 'Parmesan Garlic',
                                    displayName: 'Parmesan Garlic',
                                },
                            ],
                        },
                        {
                            productId: 'IDPModifierGroup11686',
                            modifiers: [
                                {
                                    lineItemId: 3,
                                    productId: 'IDPModifier27451',
                                    price: 0,
                                    quantity: 1,
                                    description: 'No Dips & Veggies',
                                    displayName: 'No Dips & Veggies',
                                },
                            ],
                        },
                    ],
                },
            ],
            priceWithModifiersBeforeDiscounts: 12.99,
            priceWithModifiersAfterDiscounts: 9.620000000000001,
            lineItemId: 1,
            modifiers: [
                { id: 'IDPModifier17273', unitPrice: 0, totalPrice: 0, quantity: 1 },
                { id: 'IDPModifier27451', unitPrice: 0, totalPrice: 0, quantity: 1 },
            ],
            discounts: [{ amount: 3.37, quantity: 1 }],
            domainProduct: {
                id: 'IDPSalesItem-5289',
                name: 'Traditional Wings',
                metadata: { SOURCE_PRODUCT_ID: 'SalesItem-5289', POS_ID: '17797' },
                categoryIds: ['IDPSubMenu-307'],
                sizeGroupId: 'IDPSizeGroup-0020',
                itemGroupId: 'IDPItemGroup-0001',
                productGroupId: 'IDPProductGroup-0006',
                makeItAMeal: false,
                hasChildItems: false,
                sequence: 1,
                description: 'Lorem Ipsum',
                itemModifierGroups: [
                    {
                        productGroupId: 'IDPModifierGroup-0001',
                        metadata: { MODIFIER_GROUP_TYPE: 'Extras' },
                        sequence: 60,
                        max: 10,
                        min: 0,
                        itemModifiers: {
                            'IDPSalesItem-4878': {
                                sequence: 8,
                                max: 10,
                                min: 0,
                                defaultQuantity: 0,
                                itemId: 'IDPSalesItem-4878',
                                overridePrice: {},
                            },
                            'IDPSalesItem-4879': {
                                sequence: 9,
                                max: 10,
                                min: 0,
                                defaultQuantity: 0,
                                itemId: 'IDPSalesItem-4879',
                                overridePrice: {},
                            },
                        },
                    },
                ],
                nutrition: { totalCalories: 1440, caloriesPerServing: 1440 },
                isSaleable: true,
            },
            sizeLabel: 'size',
        },
    ],
};
