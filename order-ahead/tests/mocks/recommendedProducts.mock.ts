export const recommendedProductsFull = {
    recommendedProducts: {
        'arb-itm-003-101': {
            id: 'arb-itm-003-101',
            name: 'Jamocha Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640212517',
                HAPPY_HOUR_ITEM_ID: '640243390',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                "The flavors of chocolate and coffee were made to go together. And humans were made to drink milkshakes. We took these two undeniable truths and created our famous Jamocha Shake. It's a frosty, chocolate-y, coffee-y treat. ",
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250013',
                },
                otherPrices: {
                    PROMO: {
                        price: 1,
                        priceType: 'PROMO',
                        metadata: {
                            COMPOSITE_ID: '675869819',
                            PRODUCT_GROUP_ID: '675869599',
                            SOURCE_PRODUCT_ID: '675869474',
                        },
                    },
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243390',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 592,
                macroNutrients: {
                    Proteins: {
                        label: 'Proteins',
                        weight: {
                            value: 14,
                            unit: 'g',
                        },
                    },
                    'Trans Fat': {
                        label: 'Trans Fat',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Serving Weight': {
                        label: 'Serving Weight',
                        weight: {
                            value: 413,
                            unit: 'g',
                        },
                    },
                    Sodium: {
                        label: 'Sodium',
                        weight: {
                            value: 356,
                            unit: 'mg',
                        },
                    },
                    'Total Carbohydrates': {
                        label: 'Total Carbohydrates',
                        weight: {
                            value: 97,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 157,
                            unit: 'cal',
                        },
                    },
                    'Fat - Total': {
                        label: 'Fat - Total',
                        weight: {
                            value: 17,
                            unit: 'g',
                        },
                    },
                    'Dietary Fiber': {
                        label: 'Dietary Fiber',
                        weight: {
                            value: 1,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat': {
                        label: 'Saturated Fat',
                        weight: {
                            value: 11,
                            unit: 'g',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 592,
                            unit: 'cal',
                        },
                    },
                    Cholesterol: {
                        label: 'Cholesterol',
                        weight: {
                            value: 56,
                            unit: 'mg',
                        },
                    },
                    Sugars: {
                        label: 'Sugars',
                        weight: {
                            value: 82,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation:
                    'Contains milk, soy. Manufactured in a facility that processes peanuts or tree nuts. ',
            },
        },
        'arb-itm-003-093': {
            id: 'arb-itm-003-093',
            name: 'Chocolate Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640213096',
                HAPPY_HOUR_ITEM_ID: '640243389',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                'The indulgent Chocolate Shake is made with real, premium Ghirardelli chocolate and drizzled with even more real, premium Ghirardelli chocolate. What we’re saying is Arby’s keeps it real. And premium.',
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250010',
                },
                otherPrices: {
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243389',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 590,
                macroNutrients: {
                    'Dietary Fiber (g)': {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 1,
                            unit: 'g',
                        },
                    },
                    'Trans Fat (g)': {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 160,
                            unit: 'cal',
                        },
                    },
                    'Total Carbohydrates (g)': {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 95,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat (g)': {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 12,
                            unit: 'g',
                        },
                    },
                    'Proteins (g)': {
                        label: 'Proteins (g)',
                        weight: {
                            value: 13,
                            unit: 'g',
                        },
                    },
                    'Fat - Total (g)': {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 18,
                            unit: 'g',
                        },
                    },
                    'Sodium (mg)': {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 350,
                            unit: 'mg',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 590,
                            unit: 'cal',
                        },
                    },
                    'Cholesterol (mg)': {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 55,
                            unit: 'mg',
                        },
                    },
                    'Serving Weight (g)': {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 413,
                            unit: 'g',
                        },
                    },
                    'Sugars (g)': {
                        label: 'Sugars (g)',
                        weight: {
                            value: 83,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation:
                    'Contains milk, soy. Manufactured in a facility that processes peanuts or tree nuts. ',
            },
        },
        'arb-itm-003-085': {
            id: 'arb-itm-003-085',
            name: 'Vanilla Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640213216',
                HAPPY_HOUR_ITEM_ID: '640243388',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                'You like to keep things simple. You know what you like and why you like it. Your horoscope reads "Today will be nice" every single day. This vanilla shake is for you.',
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250020',
                },
                otherPrices: {
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243388',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 0,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 490,
                macroNutrients: {
                    'Trans Fat (g)': {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Dietary Fiber (g)': {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 160,
                            unit: 'cal',
                        },
                    },
                    'Total Carbohydrates (g)': {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 72,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat (g)': {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 11,
                            unit: 'g',
                        },
                    },
                    'Proteins (g)': {
                        label: 'Proteins (g)',
                        weight: {
                            value: 13,
                            unit: 'g',
                        },
                    },
                    'Fat - Total (g)': {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 17,
                            unit: 'g',
                        },
                    },
                    'Sodium (mg)': {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 310,
                            unit: 'mg',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 490,
                            unit: 'cal',
                        },
                    },
                    'Cholesterol (mg)': {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 55,
                            unit: 'mg',
                        },
                    },
                    'Serving Weight (g)': {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 376,
                            unit: 'g',
                        },
                    },
                    'Sugars (g)': {
                        label: 'Sugars (g)',
                        weight: {
                            value: 66,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation: 'Contains milk. ',
            },
        },
    },
    recommendedProductIds: ['arb-itm-003-101', 'arb-itm-003-093', 'arb-itm-003-085'],
    correlationId: '99fea6ea-ef12-4636-9556-7b757c801710',
};

export const recommendedProductsSingle = {
    recommendedProducts: {
        'arb-itm-003-101': {
            id: 'arb-itm-003-101',
            name: 'Jamocha Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640212517',
                HAPPY_HOUR_ITEM_ID: '640243390',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                "The flavors of chocolate and coffee were made to go together. And humans were made to drink milkshakes. We took these two undeniable truths and created our famous Jamocha Shake. It's a frosty, chocolate-y, coffee-y treat. ",
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250013',
                },
                otherPrices: {
                    PROMO: {
                        price: 1,
                        priceType: 'PROMO',
                        metadata: {
                            COMPOSITE_ID: '675869819',
                            PRODUCT_GROUP_ID: '675869599',
                            SOURCE_PRODUCT_ID: '675869474',
                        },
                    },
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243390',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 592,
                macroNutrients: {
                    Proteins: {
                        label: 'Proteins',
                        weight: {
                            value: 14,
                            unit: 'g',
                        },
                    },
                    'Trans Fat': {
                        label: 'Trans Fat',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Serving Weight': {
                        label: 'Serving Weight',
                        weight: {
                            value: 413,
                            unit: 'g',
                        },
                    },
                    Sodium: {
                        label: 'Sodium',
                        weight: {
                            value: 356,
                            unit: 'mg',
                        },
                    },
                    'Total Carbohydrates': {
                        label: 'Total Carbohydrates',
                        weight: {
                            value: 97,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 157,
                            unit: 'cal',
                        },
                    },
                    'Fat - Total': {
                        label: 'Fat - Total',
                        weight: {
                            value: 17,
                            unit: 'g',
                        },
                    },
                    'Dietary Fiber': {
                        label: 'Dietary Fiber',
                        weight: {
                            value: 1,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat': {
                        label: 'Saturated Fat',
                        weight: {
                            value: 11,
                            unit: 'g',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 592,
                            unit: 'cal',
                        },
                    },
                    Cholesterol: {
                        label: 'Cholesterol',
                        weight: {
                            value: 56,
                            unit: 'mg',
                        },
                    },
                    Sugars: {
                        label: 'Sugars',
                        weight: {
                            value: 82,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation:
                    'Contains milk, soy. Manufactured in a facility that processes peanuts or tree nuts. ',
            },
        },
    },
    recommendedProductIds: ['arb-itm-003-101'],
    correlationId: '99fea6ea-ef12-4636-9556-7b757c801710',
};

export const recommendedProductsEmpty = {
    recommendedProducts: {},
    recommendedProductIds: [],
    correlationId: '99fea6ea-ef12-4636-9556-7b757c801710',
};

export const recommendedProductsWithOneInvalid = {
    recommendedProducts: {
        'arb-itm-003-101': {
            id: 'arb-itm-003-101',
            name: 'Jamocha Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640212517',
                HAPPY_HOUR_ITEM_ID: '640243390',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                "The flavors of chocolate and coffee were made to go together. And humans were made to drink milkshakes. We took these two undeniable truths and created our famous Jamocha Shake. It's a frosty, chocolate-y, coffee-y treat. ",
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250013',
                },
                otherPrices: {
                    PROMO: {
                        price: 1,
                        priceType: 'PROMO',
                        metadata: {
                            COMPOSITE_ID: '675869819',
                            PRODUCT_GROUP_ID: '675869599',
                            SOURCE_PRODUCT_ID: '675869474',
                        },
                    },
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243390',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 592,
                macroNutrients: {
                    Proteins: {
                        label: 'Proteins',
                        weight: {
                            value: 14,
                            unit: 'g',
                        },
                    },
                    'Trans Fat': {
                        label: 'Trans Fat',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Serving Weight': {
                        label: 'Serving Weight',
                        weight: {
                            value: 413,
                            unit: 'g',
                        },
                    },
                    Sodium: {
                        label: 'Sodium',
                        weight: {
                            value: 356,
                            unit: 'mg',
                        },
                    },
                    'Total Carbohydrates': {
                        label: 'Total Carbohydrates',
                        weight: {
                            value: 97,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 157,
                            unit: 'cal',
                        },
                    },
                    'Fat - Total': {
                        label: 'Fat - Total',
                        weight: {
                            value: 17,
                            unit: 'g',
                        },
                    },
                    'Dietary Fiber': {
                        label: 'Dietary Fiber',
                        weight: {
                            value: 1,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat': {
                        label: 'Saturated Fat',
                        weight: {
                            value: 11,
                            unit: 'g',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 592,
                            unit: 'cal',
                        },
                    },
                    Cholesterol: {
                        label: 'Cholesterol',
                        weight: {
                            value: 56,
                            unit: 'mg',
                        },
                    },
                    Sugars: {
                        label: 'Sugars',
                        weight: {
                            value: 82,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation:
                    'Contains milk, soy. Manufactured in a facility that processes peanuts or tree nuts. ',
            },
        },
        'arb-itm-003-093': {
            id: 'arb-itm-003-093',
            name: 'Chocolate Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640213096',
                HAPPY_HOUR_ITEM_ID: '640243389',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                'The indulgent Chocolate Shake is made with real, premium Ghirardelli chocolate and drizzled with even more real, premium Ghirardelli chocolate. What we’re saying is Arby’s keeps it real. And premium.',
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250010',
                },
                otherPrices: {
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243389',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 590,
                macroNutrients: {
                    'Dietary Fiber (g)': {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 1,
                            unit: 'g',
                        },
                    },
                    'Trans Fat (g)': {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 160,
                            unit: 'cal',
                        },
                    },
                    'Total Carbohydrates (g)': {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 95,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat (g)': {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 12,
                            unit: 'g',
                        },
                    },
                    'Proteins (g)': {
                        label: 'Proteins (g)',
                        weight: {
                            value: 13,
                            unit: 'g',
                        },
                    },
                    'Fat - Total (g)': {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 18,
                            unit: 'g',
                        },
                    },
                    'Sodium (mg)': {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 350,
                            unit: 'mg',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 590,
                            unit: 'cal',
                        },
                    },
                    'Cholesterol (mg)': {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 55,
                            unit: 'mg',
                        },
                    },
                    'Serving Weight (g)': {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 413,
                            unit: 'g',
                        },
                    },
                    'Sugars (g)': {
                        label: 'Sugars (g)',
                        weight: {
                            value: 83,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation:
                    'Contains milk, soy. Manufactured in a facility that processes peanuts or tree nuts. ',
            },
        },
        'arb-itm-003-085': {
            id: 'arb-itm-003-085',
            name: 'Vanilla Shake',
            metadata: {
                SOURCE_PRODUCT_ID: '640213216',
                HAPPY_HOUR_ITEM_ID: '640243388',
            },
            categoryIds: ['arb-cat-000-009'],
            sizeGroupId: 'arb-sig-000-002',
            itemGroupId: 'arb-itg-000-001',
            productGroupId: 'arb-prg-000-012',
            makeItAMeal: false,
            hasChildItems: false,
            availability: {
                isAvailable: true,
            },
            sequence: 1,
            description:
                'You like to keep things simple. You know what you like and why you like it. Your horoscope reads "Today will be nice" every single day. This vanilla shake is for you.',
            price: {
                currentPrice: 2.59,
                metadata: {
                    SOURCE_PRODUCT_ID: '640250020',
                },
                otherPrices: {
                    HAPPY_HOUR: {
                        price: 1,
                        priceType: 'HAPPY_HOUR',
                        metadata: {
                            SOURCE_PRODUCT_ID: '640243388',
                        },
                    },
                },
            },
            itemModifierGroups: [
                {
                    productGroupId: 'arb-prg-001-013',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    sequence: 1,
                    max: 20,
                    min: 0,
                    itemModifiers: {
                        'arb-itm-016-003': {
                            sequence: 2,
                            max: 1,
                            min: 0,
                            defaultQuantity: 0,
                            itemId: 'arb-itm-016-003',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                        'arb-itm-016-002': {
                            sequence: 1,
                            max: 1,
                            min: 0,
                            defaultQuantity: 1,
                            itemId: 'arb-itm-016-002',
                            overridePrice: {
                                currentPrice: 0,
                            },
                        },
                    },
                },
            ],
            nutrition: {
                totalCalories: 490,
                macroNutrients: {
                    'Trans Fat (g)': {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Dietary Fiber (g)': {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    'Calories from Fat': {
                        label: 'Calories from Fat',
                        weight: {
                            value: 160,
                            unit: 'cal',
                        },
                    },
                    'Total Carbohydrates (g)': {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 72,
                            unit: 'g',
                        },
                    },
                    'Saturated Fat (g)': {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 11,
                            unit: 'g',
                        },
                    },
                    'Proteins (g)': {
                        label: 'Proteins (g)',
                        weight: {
                            value: 13,
                            unit: 'g',
                        },
                    },
                    'Fat - Total (g)': {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 17,
                            unit: 'g',
                        },
                    },
                    'Sodium (mg)': {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 310,
                            unit: 'mg',
                        },
                    },
                    Calories: {
                        label: 'Calories',
                        weight: {
                            value: 490,
                            unit: 'cal',
                        },
                    },
                    'Cholesterol (mg)': {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 55,
                            unit: 'mg',
                        },
                    },
                    'Serving Weight (g)': {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 376,
                            unit: 'g',
                        },
                    },
                    'Sugars (g)': {
                        label: 'Sugars (g)',
                        weight: {
                            value: 66,
                            unit: 'g',
                        },
                    },
                },
                allergenInformation: 'Contains milk. ',
            },
        },
    },
    recommendedProductIds: ['arb-itm-INVALID', 'arb-itm-003-093', 'arb-itm-003-085'],
    correlationId: '99fea6ea-ef12-4636-9556-7b757c801710',
};

export const recommendedProductsTitle = 'MockRecommendedProductsTitle';
