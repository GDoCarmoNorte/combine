export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '5XC1tpOOPfUJF4Ymi45Mag',
        type: 'Entry',
        createdAt: '2020-08-28T13:10:27.726Z',
        updatedAt: '2020-08-28T19:14:05.327Z',
        environment: {
            sys: {
                id: 'DBBP-5902',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 2,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'externalLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Careers',
        nameInUrl: 'https://careers.arbys.com/us/en',
    },
};
