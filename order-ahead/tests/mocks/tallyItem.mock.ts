export default {
    quantity: 1,
    productId: 'arb-itm-000-001',
    price: 3.49,
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            modifiers: [
                { productId: 'arb-itm-006-001', quantity: 1, price: 2 },
                { productId: 'arb-itm-006-002', quantity: 1, price: 0.79 },
                { productId: 'arb-itm-006-021', quantity: 1, price: 2 },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            modifiers: [
                { productId: 'arb-itm-007-001', quantity: 1, price: 0.49 },
                { productId: 'arb-itm-007-002', quantity: 1, price: 0.49 },
                { productId: 'arb-itm-007-004', quantity: 1, price: 0.49 },
                { productId: 'arb-itm-007-005', quantity: 1, price: 0.49 },
            ],
        },
        {
            productId: 'arb-prg-001-003',
            modifiers: [
                { productId: 'arb-itm-008-001', quantity: 1, price: 0.29 },
                { productId: 'arb-itm-008-004', quantity: 1, price: 0 },
                { productId: 'arb-itm-008-002', quantity: 1, price: 0 },
                { productId: 'arb-itm-008-005', quantity: 1, price: 0.29 },
                { productId: 'arb-itm-008-006', quantity: 1, price: 0 },
            ],
        },
        {
            productId: 'arb-prg-001-004',
            modifiers: [{ productId: 'arb-itm-009-001', quantity: 1, price: 0 }],
        },
        {
            productId: 'arb-prg-001-005',
            modifiers: [
                { productId: 'arb-itm-010-001', quantity: 1, price: 0 },
                { productId: 'arb-itm-010-003', quantity: 1, price: 0 },
                { productId: 'arb-itm-010-002', quantity: 1, price: 0 },
            ],
        },
        {
            productId: 'arb-prg-001-006',
            modifiers: [{ productId: 'arb-itm-011-001', quantity: 1, price: 1.59 }],
        },
        {
            productId: 'arb-prg-001-008',
            modifiers: [{ productId: 'arb-itm-013-001', price: 0, quantity: 1 }],
        },
    ],
};

export const promoTallyItemMock = {
    name: '2 for $6 Everyday Value',
    productId: 'arb-itm-000-186',
    price: 6,
    quantity: 1,
    childItems: [
        {
            productId: 'arb-itm-000-017',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-002',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            name: "Classic Beef 'n Cheddar",
        },
        {
            productId: 'arb-itm-000-017',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-002',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            name: "Classic Beef 'n Cheddar",
        },
    ],
};
