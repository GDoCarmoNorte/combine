export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '4pLkYgTzsGWV6BVrul3UP0',
        type: 'Entry',
        createdAt: '2020-11-05T16:06:18.991Z',
        updatedAt: '2020-11-05T16:06:18.991Z',
        environment: {
            sys: {
                id: 'DBBP-5902',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'phoneNumberLink',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'I am the phone link contact us',
        phoneNumber: '11111111111',
    },
};
