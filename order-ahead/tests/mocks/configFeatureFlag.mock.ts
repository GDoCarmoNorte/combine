import { ConfigurationState } from '../../redux/configuration';

export default {
    configuration: {
        isOAEnabled: true,
        isDeliveryEnabled: true,
        isApplePayEnabled: true,
        isGooglePayEnabled: true,
        isTippingEnabled: true,
        configurationRefreshFrequency: 30,
        personalizationRefreshFrequency: 30,
    },
    lastSyncedAt: new Date('2018-01-01T00:00:00.000Z'),
} as ConfigurationState;
