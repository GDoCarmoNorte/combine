import { ItemModel, IntervalTypeEnumModel } from '../../@generated/webExpApi';

const item: ItemModel = {
    id: 'arb-itm-000-030',
    name: 'Half Pound Beef n Cheddar Medium Meal',
    metadata: {
        SOURCE_PRODUCT_ID: '640239550',
    },
    sizeGroupId: 'arb-sig-000-003',
    itemGroupId: 'arb-itg-000-006',
    productGroupId: 'arb-prg-000-001',
    makeItAMeal: false,
    availability: {
        isAvailable: true,
        validity: {
            Date: {
                value: '2010-01-01/2099-12-31',
                intervalType: IntervalTypeEnumModel.Date,
                dayOfTheWeek: 'SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY',
            },
            Time: {
                value: '00:00/23:59',
                intervalType: IntervalTypeEnumModel.Time,
                dayOfTheWeek: 'SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY',
            },
        },
    },
    sequence: 18,
    description:
        'We put a half pound of America’s favorite roast beef on this Half Pound Beef ‘n Cheddar. That doesn’t include the weight of the other stuff, though, but we’re certain the onion bun, cheddar cheese sauce, and red ranch make it even heavier.',
    price: {
        currentPrice: 6.99,
    },
    itemModifierGroups: [
        {
            productGroupId: 'arb-prg-000-019',
            metadata: {
                PRODUCT_GROUP_ID: '640240807',
                COMPOSITE_ID: '640239571',
            },
            sequence: 1,
            max: 1,
            min: 1,
            defaultItemModifierId: 'arb-itm-000-019',
            itemModifiers: {
                'arb-itm-000-019': {
                    metadata: {
                        SOURCE_PRODUCT_ID: '640224101',
                    },
                    sequence: 1,
                    max: 1,
                    min: 0,
                    defaultQuantity: 1,
                    itemId: 'arb-itm-000-019',
                    overridePrice: {
                        currentPrice: 5.51,
                        metadata: {
                            SOURCE_PRODUCT_ID: '640247744',
                        },
                    },
                },
            },
        },
        {
            productGroupId: 'arb-prg-000-008',
            metadata: {
                PRODUCT_GROUP_ID: '640213609',
                COMPOSITE_ID: '640239577',
            },
            sequence: 2,
            max: 1,
            min: 1,
            defaultItemModifierId: 'arb-itm-002-003',
            itemModifiers: {
                'arb-itm-002-031': {
                    metadata: {
                        SOURCE_PRODUCT_ID: '640240793',
                    },
                    sequence: 6,
                    max: 1,
                    min: 0,
                    defaultQuantity: 0,
                    itemId: 'arb-itm-002-031',
                    overridePrice: {
                        currentPrice: 1.79,
                        metadata: {
                            SOURCE_PRODUCT_ID: '640249184',
                        },
                    },
                },
                'arb-itm-002-019': {
                    metadata: {
                        SOURCE_PRODUCT_ID: '640212609',
                    },
                    sequence: 3,
                    max: 1,
                    min: 0,
                    defaultQuantity: 0,
                    itemId: 'arb-itm-002-019',
                    overridePrice: {
                        currentPrice: 4.49,
                        metadata: {
                            SOURCE_PRODUCT_ID: '640248950',
                        },
                    },
                },
            },
        },
        {
            productGroupId: 'arb-prg-000-010',
            metadata: {
                PRODUCT_GROUP_ID: '640213701',
                COMPOSITE_ID: '640240913',
            },
            sequence: 3,
            max: 1,
            min: 1,
            defaultItemModifierId: 'arb-itm-003-002',
            itemModifiers: {
                'arb-itm-003-031': {
                    metadata: {
                        MODIFIER_GROUP_ID: '640219114',
                        SOURCE_PRODUCT_ID: '640223398',
                        MODIFIER_ID: '640691955',
                    },
                    sequence: 5,
                    max: 1,
                    min: 0,
                    defaultQuantity: 0,
                    itemId: 'arb-itm-003-031',
                    overridePrice: {
                        currentPrice: 1.99,
                        metadata: {
                            SOURCE_PRODUCT_ID: '640249358',
                        },
                    },
                },
                'arb-itm-003-052': {
                    metadata: {
                        MODIFIER_GROUP_ID: '640219114',
                        SOURCE_PRODUCT_ID: '640223398',
                        MODIFIER_ID: '640691961',
                    },
                    sequence: 8,
                    max: 1,
                    min: 0,
                    defaultQuantity: 0,
                    itemId: 'arb-itm-003-052',
                    overridePrice: {
                        currentPrice: 1.99,
                        metadata: {
                            SOURCE_PRODUCT_ID: '640249358',
                        },
                    },
                },
            },
        },
    ],
    upsellRelationships: {
        SIZE: ['arb-itm-000-022', 'arb-itm-000-026'],
    },
};

export default item;
