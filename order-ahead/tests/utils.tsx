import React from 'react';
import { render as rtlRender, RenderOptions, screen } from '@testing-library/react';
import { renderHook as rtlRenderHook, RenderHookOptions } from '@testing-library/react-hooks';

import { Provider } from 'react-redux';

import domainMenuMock from './mock-data/provider/domainMenu.mock.json';
import rewardsMock from './mock-data/provider/rewards.mock.json';
import accountMock from './mock-data/provider/account.mock.json';
import orderLocationMock from './mock-data/provider/orderLocation.mock.json';
import configurationMock from './mock-data/provider/configuration.mock.json';

import { createTestStore, RootState } from '../redux/store';

const DefaultStateProvider = ({ state, children }) => {
    const store = createTestStore({
        domainMenu: domainMenuMock,
        rewards: rewardsMock,
        account: accountMock,
        orderLocation: orderLocationMock,
        configuration: configurationMock,
        ...state,
    });

    return <Provider store={store}>{children}</Provider>;
};

type Options = { state?: Partial<RootState>; renderOptions?: Omit<RenderOptions, 'queries'> };

export const render = (component: React.ReactElement, options: Options = { state: {}, renderOptions: {} }) => {
    return rtlRender(component, {
        // eslint-disable-next-line react/display-name
        wrapper: ({ children }) => <DefaultStateProvider state={options.state}>{children}</DefaultStateProvider>,
        ...options.renderOptions,
    });
};

type HookOptions<T> = { state?: Partial<RootState>; renderHookOptions?: RenderHookOptions<T> };

export const renderHook = (
    callback: (props: any) => any,
    options: HookOptions<any> = { state: {}, renderHookOptions: {} }
) => {
    return rtlRenderHook(callback, {
        // eslint-disable-next-line react/display-name
        wrapper: ({ children }) => <DefaultStateProvider state={options.state}>{children}</DefaultStateProvider>,
        ...options.renderHookOptions,
    });
};

export const getTextWithoutHtmlTags = (text: string) => {
    return screen.getByText((content, node) => {
        // eslint-disable-next-line testing-library/no-node-access
        return node.textContent === text && Array.from(node.children).every((child) => child.textContent !== text);
    });
};
export * from '@testing-library/react';
