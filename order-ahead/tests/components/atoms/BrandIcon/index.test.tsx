import React from 'react';
import { render } from '@testing-library/react';

import Icon from '../../../../components/atoms/BrandIcon';

describe('brand icon component', () => {
    it('should match snapshot with default props', () => {
        const { container } = render(<Icon icon="Bag" />);

        expect(container).toMatchSnapshot();
    });

    it('should match snapshot with custom className', () => {
        const { container } = render(<Icon icon="Bag" className="custom-className" />);

        expect(container).toMatchSnapshot();
    });

    it('should match snapshot with custom size', () => {
        const { container } = render(<Icon icon="Bag" size="l" />);

        expect(container).toMatchSnapshot();
    });

    it('should pass light className to span element when light variant prop is passed', () => {
        const { container } = render(<Icon icon="Bag" variant="light" />);
        expect(container).toMatchSnapshot();
    });

    it('should pass colorful className to span element when light variant prop is passed', () => {
        const { container } = render(<Icon icon="Bag" variant="colorful" />);
        expect(container).toMatchSnapshot();
    });
});
