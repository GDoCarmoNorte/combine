import React from 'react';
import { InspireTabSet, InspireTab, InspireTabPanel } from '../../../../components/atoms/tabset';
import { render, screen } from '@testing-library/react';

describe('InspireTabSet', () => {
    it('should render TabSet with tabs and panels', () => {
        const { container } = render(
            <div>
                <InspireTabSet value={0}>
                    <InspireTab label="tab 1" />
                    <InspireTab label="tab 2" />
                </InspireTabSet>
                <InspireTabPanel value={0} index={0}>
                    Content 1
                </InspireTabPanel>
                <InspireTabPanel value={0} index={1}>
                    Content 2
                </InspireTabPanel>
            </div>
        );

        expect(container).toMatchSnapshot();
    });

    it('should render disabled tab', () => {
        render(
            <div>
                <InspireTabSet value={0}>
                    <InspireTab label="tab 1" disabled={true} />
                </InspireTabSet>
                <InspireTabPanel value={0} index={0}>
                    Content 1
                </InspireTabPanel>
            </div>
        );
        const tab = screen.getByRole('tab');
        expect(tab).toBeDisabled();
    });
});
