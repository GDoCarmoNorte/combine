import React from 'react';
import { render, screen } from '@testing-library/react';

import AllLocationsLink from '../../../../components/atoms/allLocationsLink';

jest.mock('../../../../components/atoms/link', () => ({
    InspireLink: jest.fn((props) => <a data-testid="inspire-link-mock">props: {JSON.stringify(props, null, 2)}</a>),
}));

describe('AllLocationsLink component', () => {
    test('should render "All Locations" link', () => {
        const { container } = render(<AllLocationsLink />);

        expect(container).toMatchSnapshot();
    });

    test('should not render "All Locations" link', () => {
        const valueEnv = process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN;
        process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = 'undefined';

        render(<AllLocationsLink />);

        expect(screen.queryByTestId('inspire-link-mock')).not.toBeInTheDocument();

        process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = valueEnv;
    });
});
