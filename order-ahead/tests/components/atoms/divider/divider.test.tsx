import React from 'react';
import { Divider } from '../../../../components/atoms/divider';
import { render } from '@testing-library/react';

describe('Divider', () => {
    it('should render component', () => {
        const { container } = render(<Divider />);

        expect(container).toMatchSnapshot();
    });
});
