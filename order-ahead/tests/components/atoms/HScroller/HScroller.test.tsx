import React from 'react';
import { shallow } from 'enzyme';

import HScroller from '../../../../components/atoms/HScroller';
import { IconButton } from '@material-ui/core';

jest.mock('@material-ui/icons/ChevronLeft');
jest.mock('@material-ui/icons/ChevronRight');

const currentMock = {
    current: {
        clientWidth: 123,
        scrollWidth: 100,
        scrollLeft: -10,
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        scrollTo: jest.fn(),
    },
};

describe('HScroller component', () => {
    const setState = jest.fn();
    const useStateMock: any = (initState: any) => [initState, setState];

    jest.spyOn(React, 'useRef').mockReturnValue(currentMock);
    jest.spyOn(React, 'useEffect').mockImplementation((f) => f());
    jest.spyOn(React, 'useState').mockImplementation(useStateMock);
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    jest.spyOn(window, 'addEventListener').mockImplementation(() => {});

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should match snapshot for HScroller ans call proper methods', () => {
        const wrapper = shallow(
            <HScroller>
                <div />
                <div />
            </HScroller>
        );

        expect(setState).toHaveBeenCalledTimes(6);
        expect(currentMock.current.addEventListener).toHaveBeenCalledTimes(1);
        expect(window.addEventListener).toHaveBeenCalledTimes(1);
        expect(wrapper).toMatchSnapshot();
    });

    it('should call scrollTo after click on prevButton', () => {
        const wrapper = shallow(
            <HScroller>
                <div />
                <div />
            </HScroller>
        );

        const prevButton = wrapper.find(IconButton).at(0);
        prevButton.simulate('click');

        expect(setState).toHaveBeenCalledTimes(6);
        expect(currentMock.current.scrollTo).toHaveBeenCalledTimes(1);
        expect(currentMock.current.scrollTo).toHaveBeenCalledWith({ behavior: 'smooth', left: 0 });
    });

    it('should call scrollTo after click on nextButton', () => {
        const wrapper = shallow(
            <HScroller>
                <div />
                <div />
            </HScroller>
        );

        const nextButton = wrapper.find(IconButton).at(1);
        nextButton.simulate('click');

        expect(currentMock.current.scrollTo).toHaveBeenCalledTimes(1);
        expect(currentMock.current.scrollTo).toHaveBeenCalledWith({ behavior: 'smooth', left: 0 });
    });

    it('should not call scrollTo after click on prevButton in current does not exist', () => {
        jest.spyOn(React, 'useRef').mockReturnValue({ current: undefined });

        const wrapper = shallow(
            <HScroller>
                <div />
                <div />
            </HScroller>
        );

        const prevButton = wrapper.find(IconButton).at(0);
        prevButton.simulate('click');

        expect(currentMock.current.scrollTo).toHaveBeenCalledTimes(0);
    });
});
