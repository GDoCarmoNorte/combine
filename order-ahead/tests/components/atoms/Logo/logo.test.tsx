import React from 'react';
import { render } from '@testing-library/react';
import Logo from '../../../../components/atoms/Logo';

describe('Logo component', () => {
    it('should match snapshot', () => {
        const { container } = render(<Logo urlLogo="" urlLogoDesktop="" className="" />);
        expect(container).toMatchSnapshot();
    });
});
