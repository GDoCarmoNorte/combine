import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import { InspireButton } from '../../../../components/atoms/button';
import globalPropsMock from '../../../mocks/globalProps.mock';
import phoneLinkMock from '../../../mocks/phoneLink.mock';
import externalLinkMock from '../../../mocks/externalLink.mock';
import productMock from '../../../mocks/contentfulProduct.mock';

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

beforeEach(() => {
    jest.spyOn(React, 'useContext').mockReturnValueOnce({});
});

describe('button', () => {
    it('should render button product link', () => {
        const wrapper = shallow(<InspireButton link={productMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render simple button properly', () => {
        const wrapper = shallow(<InspireButton text="text" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render button with size properly', () => {
        const wrapper = shallow(<InspireButton size="small" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render simple button with full width', () => {
        const wrapper = shallow(<InspireButton text="text" fullWidth />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render button external link', () => {
        const wrapper = shallow(<InspireButton link={externalLinkMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render button phone link', () => {
        const wrapper = shallow(<InspireButton link={phoneLinkMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render button with children', () => {
        const wrapper = shallow(<InspireButton text="text">children</InspireButton>);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render button link with gtm id', () => {
        const wrapper = shallow(<InspireButton link={externalLinkMock as any} gtmId="gtmId" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should call onClick after click on button', () => {
        const onClickMock = jest.fn();
        const wrapper = shallow(<InspireButton onClick={onClickMock} />);

        wrapper.simulate('click');

        expect(onClickMock).toHaveBeenCalledTimes(1);
    });

    it('should disabled link with disabled styles', () => {
        render(<InspireButton type="small" text={'start an order'} link="/locations" disabled={true} />);
        const link = screen.getByRole('link');
        expect(link).toHaveClass('disabled');
    });
});
