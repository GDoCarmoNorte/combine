import React from 'react';
import { shallow } from 'enzyme';

import InspirePhoneLink from '../../../../components/atoms/phoneLink';
import entryMock from '../../../mocks/phoneLink.mock';

describe('phoneLink', () => {
    it('should not render the phone link if fields not exist', () => {
        const wrapper = shallow(<InspirePhoneLink phone={{} as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the phone link properly', () => {
        const wrapper = shallow(<InspirePhoneLink phone={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the phone link with text value', () => {
        const wrapper = shallow(<InspirePhoneLink phone="012-345-6789" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should not render the phone link if text not exist', () => {
        const wrapper = shallow(<InspirePhoneLink phone="" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the phone link with secondary variant', () => {
        const wrapper = shallow(<InspirePhoneLink phone="012-345-6789" variant="secondary" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the phone link with class names', () => {
        const wrapper = shallow(
            <InspirePhoneLink phone="012-345-6789" className="className" iconClassName="iconClassName" />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
