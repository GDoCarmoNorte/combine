import React from 'react';
import { shallow } from 'enzyme';

import TextWithTrademark from '../../../../components/atoms/textWithTrademark';

describe('TextWithTrademark', () => {
    it('should render TextWithTrademark correctly', () => {
        const wrapper = shallow(<TextWithTrademark tag="span" text="Roast Turkey & Swiss Sandwich" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with tag "p" correctly', () => {
        const wrapper = shallow(<TextWithTrademark tag="p" text="Roast Turkey & Swiss Sandwich" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with classname correctly', () => {
        const wrapper = shallow(
            <TextWithTrademark tag="span" text="Roast Turkey & Swiss Sandwich" className="className" />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with gtm id correctly', () => {
        const wrapper = shallow(
            <TextWithTrademark tag="span" text="Roast Turkey & Swiss Sandwich" data-gtm-id="gtmId" />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should wrap "®" to the <sup> tag', () => {
        const wrapper = shallow(<TextWithTrademark tag="span" text="Roast Turkey & Swiss Sandwich®" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should wrap each "®" to the <sup> tag', () => {
        const wrapper = shallow(<TextWithTrademark tag="span" text="Roast® Turkey® & Swiss® Sandwich®" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render long text with different punctuation correctly', () => {
        const wrapper = shallow(
            <TextWithTrademark
                tag="span"
                text="Oh, what?® You’re surprised Arby’s® has the best and biggest fish sandwiches in the market between their Crispy Fish and their King's Hawaiian® Fish Deluxe? You shouldn’t be."
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component and not crash if text is undefined', () => {
        const wrapper = shallow(<TextWithTrademark tag="span" text={undefined} />);

        expect(wrapper).toMatchSnapshot();
    });
});
