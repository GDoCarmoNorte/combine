import React from 'react';
import { render, screen } from '@testing-library/react';
import { TextDivider } from '../../../../components/atoms/textDivider';

describe('extras component', () => {
    it('should render text inside divider', () => {
        const textMock = 'test';

        render(<TextDivider text={textMock} />);

        screen.getByText(textMock);
    });
});
