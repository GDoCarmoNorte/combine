export default {
    fields: {
        leftSideTopText: 'come and get it',
        leftSideMainText: 'top picks',
        items: [
            {
                fields: {
                    name: 'Roast Turkey & Swiss Sandwich',
                    nameInUrl: 'roast-turkey-swiss',
                    image: 'mockImage',
                    productId: '640213024',
                },
            },
        ],
        backgroundColor: 'C1C1C1',
        textColor: 'EEEEEE',
        isSideScroll: true,
    },
};
