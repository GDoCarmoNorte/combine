import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import VerticalProductCard from '../../../../components/atoms/verticalProductCard';
import entryMock from './verticalProductCard.mock';
import productMock from '../../../mocks/contentfulProduct.mock';
import ProductItemControls from '../../../../components/clientOnly/productItemControls';
import globalPropsMock from '../../../mocks/globalProps.mock';
import { ItemGroupEnum } from '../../../../redux/types';
import { useDomainMenuSelectors, useProductItemGroup } from '../../../../redux/hooks/domainMenu';
import { ISodiumWarningFields } from '../../../../@generated/@types/contentful';
import { InspireCmsEntry } from '../../../../common/types';

const item = entryMock.fields.items[0];

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

const addToBagMock = jest.fn();

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProductByContentfulFields: () => ({ name: 'Buffalo Chicken Slider', id: '640213024' }),
    useDiscountPriceAndCalories: () => ({ price: 5, calories: 100 }),
    useProductIsCombo: () => true,
    useProductCategory: () => ({
        id: 'arb-cat-000-002',
        name: 'Limited Time',
        sequence: 1,
        parentCategoryId: 'arb-cat-000-000',
    }),
    useDomainMenuSelectors: jest.fn().mockImplementation(() => ({
        selectProductSize: jest.fn(),
    })),
    useProductItemGroup: jest.fn(),
    useDefaultTallyItem: () => ({
        name: 'Test',
        quantity: 1,
        productId: 'IDPItem-1234',
        price: 1,
        modifierGroups: [],
        childExtras: [],
    }),
    useTallyPriceAndCalories: () => ({
        calories: 100,
    }),
}));

jest.mock('../../../../redux/hooks', () => ({
    useBag: () => ({
        bagEntries: [],
        actions: {
            addDefaultToBag: addToBagMock,
        },
    }),
    useDomainMenu: jest.fn().mockResolvedValue(false),
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalPropsMock),
    usePdp: jest.fn(),
}));

jest.mock('../../../../common/hooks/useProductOrderAheadAvailability', () => ({
    useProductOrderAheadAvailability: () => ({ isOrderAheadAvailable: true, product: 'productMock' }),
}));

jest.mock('../../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: jest.fn().mockReturnValue({ isSaleable: true }),
}));

jest.mock('../../../../common/hooks/useProductIsAvailable', () => ({
    useProductIsAvailable: jest.fn().mockReturnValue({ isAvailable: true }),
}));

jest.mock('../../../../common/hooks/useSodiumLegalWarning', () => ({
    useSodiumLegalWarning: () => null,
}));

jest.mock('../../../../common/hooks/useTallyItemHasSodiumWarning', () => ({
    useTallyItemHasSodiumWarning: () => true,
}));

describe('VerticalProductCard', () => {
    it('should render the product cart correctly with domain name', () => {
        const wrapper = shallow(<VerticalProductCard isSideScroll category={'top-picks'} item={item as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render with classname correctly', () => {
        const wrapper = shallow(
            <VerticalProductCard isSideScroll category={'top-picks'} item={item as any} className="className" />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should render styles with falsy isSideScroll correctly', () => {
        const wrapper = shallow(<VerticalProductCard isSideScroll={false} category={'top-picks'} item={item as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render with gtm id correctly', () => {
        const wrapper = shallow(
            <VerticalProductCard isSideScroll category={'top-picks'} item={item as any} gtmId="gtmId" />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should call onAddToBag after click on controls', () => {
        const wrapper = shallow(<VerticalProductCard isSideScroll category={'top-picks'} item={item as any} />);
        wrapper.find(ProductItemControls).simulate('click');
        expect(addToBagMock).toHaveBeenCalledTimes(1);
    });

    it('should render product badge correctly with "Limited Time" badge', () => {
        render(<VerticalProductCard isSideScroll={false} category={'top-picks'} item={productMock as any} />);

        screen.getByText('Limited Time');
    });

    it('should render bogo 50% off correctly', () => {
        (useProductItemGroup as jest.Mock).mockReturnValueOnce({
            name: ItemGroupEnum.BOGO_50_OFF,
        });

        render(<VerticalProductCard isSideScroll category={'top-picks'} item={item as any} />);
        screen.getByText(/BOGO 50% OFF/i);
    });

    it('should render bogo x2 free correctly', () => {
        (useProductItemGroup as jest.Mock).mockReturnValueOnce({
            name: ItemGroupEnum.BOGO,
        });

        (useDomainMenuSelectors as jest.Mock).mockReturnValueOnce({
            selectProductSize: jest.fn().mockReturnValueOnce('15'),
        });

        render(<VerticalProductCard isSideScroll category={'top-picks'} item={item as any} />);
        screen.getByText(/BOGO/i);
    });

    it('should show sodium warning', () => {
        const sodiumWarning = {
            fields: {
                name: 'test',
                icon: {
                    fields: {
                        icon: {
                            fields: {
                                file: {
                                    url: 'test',
                                },
                            },
                        },
                    },
                },
            } as ISodiumWarningFields,
            sys: {
                id: 'test',
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'Test',
                    },
                },
            },
        } as InspireCmsEntry<ISodiumWarningFields>;

        render(
            <VerticalProductCard
                isSideScroll={false}
                category={'top-picks'}
                item={item as any}
                sodiumWarning={sodiumWarning}
            />
        );

        expect(screen.getByAltText('Sodium warning label')).toBeInTheDocument();
    });
});
