import React from 'react';
import { shallow } from 'enzyme';

import ContentfulImage from '../../../../components/atoms/ContentfulImage';

const assetMock = {
    fields: {
        file: {
            contentType: 'image/svg+xml',
            details: {
                image: {
                    height: 24,
                    width: 45,
                },
                size: 3552,
            },
            fileName: 'pig.svg',
            url: '//images.cdn.net/pig.svg',
        },
        title: 'Pig for the footer ',
    },
    sys: {
        createdAt: '2020-11-04T11:18:10.393Z',
        environment: { sys: { id: 'dev', type: 'Link', linkType: 'Environment' } },
        id: '5apmBWrEEArTSPszw6xcdL',
        locale: 'en-US',
        revision: 2,
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        type: 'Asset',
        updatedAt: '2020-12-17T09:40:01.656Z',
    },
} as any;

describe('ContentfulImage component', () => {
    it('should match snapshot with only asset', () => {
        const wrapper = shallow(<ContentfulImage asset={assetMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with asset and custom image alt', () => {
        const wrapper = shallow(<ContentfulImage asset={assetMock} imageAlt="high priority alt" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with asset and empty image alt', () => {
        const wrapper = shallow(<ContentfulImage asset={assetMock} imageAlt="" />);

        expect(wrapper).toMatchSnapshot();
    });
});
