import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, screen } from '../../../utils';
import { usePurchaseOffer } from '../../../../common/hooks/usePurchaseOffer';
import RecommendedCertificate from '../../../../components/atoms/recommendedRewardsCertificate';
import { TErrorCodeModel } from '../../../../@generated/webExpApi';

jest.mock('../../../../redux/hooks/useRewards');
jest.mock('../../../../common/hooks/usePurchaseOffer');

const mockProps = {
    id: 'ea5c47a7-1ae3-461c-a54f-736ddbc40aac',
    type: 'ON_DEMAND',
    code: '58',
    status: 'ACTIVE',
    title: 'Boneless - 20 Wings',
    points: 2150,
    imageUrl:
        '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BWW_BonelessWings_Sauce.jpg',
    startDateTime: '2015-06-01T00:00:00Z',
    endDateTime: '2051-01-01T05:59:59Z',
    isSideScroll: false,
    setPurchaseLoadingState: () => true,
};

describe('recommended certificate', () => {
    it('should render correctly Recommended Certificate', () => {
        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn().mockImplementation(() => Promise.resolve()),
            lastPurchaseData: null,
            cleanLastPurchaseData: jest.fn(),
            success: false,
            error: null,
            cleanErrorCode: jest.fn(),
        });
        const { container } = render(<RecommendedCertificate {...mockProps} />);
        expect(container).toMatchSnapshot();
    });

    it('should render error modal on redeeming certificate and when email not verified', () => {
        const purchaseOffer = jest.fn().mockImplementation(() => Promise.resolve());
        const cleanErrorCode = jest.fn();

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer,
            lastPurchaseData: {},
            cleanLastPurchaseData: jest.fn(),
            success: false,
            errorCode: TErrorCodeModel.EmailNotVerified,
            cleanErrorCode,
            error: 'Email not verified',
        });

        render(<RecommendedCertificate {...mockProps} />);

        const errorTitle = screen.getByText(/EMAIL UNVERIFIED/i);
        const closeButton = screen.getByText(/GO BACK/i);

        expect(errorTitle).toBeInTheDocument();
        expect(
            screen.getByText(
                /We need to verify your email address before you can redeem a reward. Check your email now./i
            )
        ).toBeInTheDocument();
        expect(closeButton).toBeInTheDocument();

        userEvent.click(closeButton);

        expect(errorTitle).not.toBeInTheDocument();
        expect(cleanErrorCode).toHaveBeenCalled();
    });
});
