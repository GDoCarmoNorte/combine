export const textSelections = [
    {
        sizeGroup: {
            id: 'arb-sig-000-002',
            name: 'Small',
            sequence: 2,
            parentGroupId: '0',
        },
        product: {
            id: 'arb-itm-000-057',
            name: 'Greek Gyro Meal',
        },
        disabled: false,
        isGroupedBySize: true,
    },
    {
        sizeGroup: {
            id: 'arb-sig-000-003',
            name: 'Medium',
            sequence: 3,
            parentGroupId: '0',
        },
        product: {
            id: 'arb-itm-000-058',
            name: 'Greek Gyro Meal',
        },
        disabled: false,
        isGroupedBySize: true,
    },
    {
        sizeGroup: {
            id: 'arb-sig-000-004',
            name: 'Large',
            sequence: 4,
            parentGroupId: '0',
        },
        product: {
            id: 'arb-itm-000-059',
            name: 'Greek Gyro Meal',
        },
        disabled: false,
        isGroupedBySize: true,
    },
];

export const nonSizedSelections = [
    {
        sizeGroup: {
            id: 'arb-sig-000-002',
            name: 'Small',
            sequence: 2,
            parentGroupId: '0',
        },
        product: {
            id: 'arb-itm-000-057',
            name: 'Greek Gyro Meal',
        },
        disabled: false,
        isGroupedBySize: false,
    },
    {
        sizeGroup: {
            id: 'arb-sig-000-003',
            name: 'Medium',
            sequence: 3,
            parentGroupId: '0',
        },
        product: {
            id: 'arb-itm-000-058',
            name: 'Greek',
        },
        disabled: false,
        isGroupedBySize: false,
    },
];

export const numericSelections = [
    {
        sizeGroup: {
            id: 'IDPSizeGroup-0019',
            name: '10',
            sequence: 1,
            parentGroupId: '0',
        },
        product: {
            id: 'IDPSalesItem-5288',
            name: '15 Traditional Wings',
        },
        disabled: false,
        isGroupedBySize: true,
    },
    {
        sizeGroup: {
            id: 'IDPSizeGroup-0020',
            name: '15',
            sequence: 2,
            parentGroupId: '0',
        },
        product: {
            id: 'IDPSalesItem-5289',
            name: '15 Traditional Wings',
        },
        disabled: false,
        isGroupedBySize: true,
    },
    {
        sizeGroup: {
            id: 'IDPSizeGroup-0021',
            name: '30',
            sequence: 3,
            parentGroupId: '0',
        },
        product: {
            id: 'IDPSalesItem-5290',
            name: '15 Traditional Wings',
        },
        disabled: false,
        isGroupedBySize: true,
    },
];
