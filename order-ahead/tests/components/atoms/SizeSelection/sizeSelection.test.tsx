import React from 'react';
import { shallow } from 'enzyme';
import SizeSelection from '../../../../components/atoms/sizeSelection/index';
import { ISizeSelection, ItemGroupEnum } from '../../../../redux/types';

import { useProductHasOtherSizes } from '../../../../common/hooks/useProductHasOtherSizes';

import { textSelections, numericSelections, nonSizedSelections } from './sizeSelection.mock';
import { useProductItemGroup } from '../../../../redux/hooks/domainMenu';

jest.mock('../../../../common/hooks/useProductHasOtherSizes', () => ({
    useProductHasOtherSizes: jest.fn().mockReturnValue(true),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useProductItemGroup: jest.fn().mockReturnValue({}),
}));

describe('SizeSelection component', () => {
    it('should render SizeSection component properly', () => {
        const wrapper = shallow(
            <SizeSelection
                selections={textSelections as ISizeSelection[]}
                selectedProductId={'arb-itm-000-057'}
                onSelect={jest.fn()}
                className="className"
                itemClassName="itemClassName"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('SizeSelection component should render SizeSection component properly for non sized groups 1', () => {
        const wrapper = shallow(
            <SizeSelection
                selections={nonSizedSelections as ISizeSelection[]}
                selectedProductId={'arb-itm-000-057'}
                onSelect={jest.fn()}
                className="className"
                itemClassName="itemClassName"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render SizeSection for BOGO product', () => {
        (useProductItemGroup as jest.Mock).mockReturnValueOnce({ name: ItemGroupEnum.BOGO });
        const wrapper = shallow(
            <SizeSelection
                selections={numericSelections as ISizeSelection[]}
                selectedProductId={numericSelections[1].product.id}
                onSelect={jest.fn()}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render SizeSection if product does not have other sizes', () => {
        (useProductHasOtherSizes as jest.Mock).mockReturnValueOnce(false);
        const wrapper = shallow(
            <SizeSelection
                selections={textSelections as ISizeSelection[]}
                selectedProductId="arb-itm-000-057"
                onSelect={jest.fn()}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should trigger setSelectedItem after click', () => {
        const onSelect = jest.fn();
        const wrapper = shallow(
            <SizeSelection
                selections={textSelections as ISizeSelection[]}
                selectedProductId="arb-itm-000-058"
                onSelect={onSelect}
            />
        );
        wrapper.find('.selectionItem').at(1).simulate('click');
        expect(onSelect).toHaveBeenCalledTimes(1);
    });

    it('should not trigger setSelectedItem after click if size is disabled', () => {
        const onSelect = jest.fn();
        const selections = [...textSelections];
        selections[2] = { ...textSelections[2], disabled: true };
        const wrapper = shallow(
            <SizeSelection
                selections={selections as ISizeSelection[]}
                selectedProductId="arb-itm-000-057"
                onSelect={onSelect}
            />
        );
        wrapper.find('.selectionItem').at(2).simulate('click');
        expect(onSelect).not.toBeCalled();
    });

    it('should not trigger setSelectedItem after click if product is disabled', () => {
        const onSelect = jest.fn();
        const wrapper = shallow(
            <SizeSelection
                selections={textSelections as ISizeSelection[]}
                disabled
                selectedProductId="arb-itm-000-057"
                onSelect={onSelect}
            />
        );
        wrapper.find('.selectionItem').at(1).simulate('click');
        expect(onSelect).not.toBeCalled();
    });
});
