import React from 'react';
import { shallow } from 'enzyme';

import Dropdown from '../../../../components/atoms/dropdown';

describe('Dropdown component', () => {
    it('should render the component without props', () => {
        const wrapper = shallow(<Dropdown />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the component with props', () => {
        const wrapper = shallow(
            <Dropdown label="tests">
                {[<p key="item 1">item 1</p>, <p key="item 2">item 2</p>, <p key="item 3">item 3</p>]}
            </Dropdown>
        );

        expect(wrapper).toMatchSnapshot();
    });
});
