import React from 'react';
import { render, screen } from '@testing-library/react';
import InspireTextArea from '../../../../components/atoms/TextArea';

describe('InspireTextArea', () => {
    it('should render component', () => {
        render(<InspireTextArea value="textArea" placeholder="placeholder" ariaLabel="ariaLabel" />);
        expect(screen.getByPlaceholderText('placeholder')).not.toBeUndefined();
        expect(screen.getByText('textArea')).not.toBeUndefined();
        expect(screen.getByLabelText('ariaLabel')).not.toBeUndefined();
    });
});
