import React from 'react';
import { render, screen } from '@testing-library/react';
import ModifiersModal from '../../../../components/atoms/modifiersModal';
import globalPropsMock from '../../../mocks/globalProps.mock';

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('ModifiersModal', () => {
    it('should render component', () => {
        render(
            <ModifiersModal isOpen onClose={jest.fn()} title="Choose" subtitle="Sauce">
                Content
            </ModifiersModal>
        );

        const modal = screen.queryByRole('presentation');

        expect(modal).toMatchSnapshot();
    });

    it('should render component with custom footer content', () => {
        render(
            <ModifiersModal
                isOpen
                onClose={jest.fn()}
                title="Choose"
                subtitle="Sauce"
                footerContent={<div data-testid="custom-footer-content">Custom footer content</div>}
            >
                Content
            </ModifiersModal>
        );

        const footerContent = screen.queryByTestId('custom-footer-content');

        expect(footerContent).toBeInTheDocument();
    });
});
