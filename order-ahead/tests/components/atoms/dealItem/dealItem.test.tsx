import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import DealItem from '../../../../components/atoms/dealItem';
import globalPropsMock from '../../../mocks/globalProps.mock';

const onClickMock = jest.fn();
const mockItem = {
    id: 'eaf972b7-f04f-44db-af1b-a67ace13f77c',
    userOfferId: 'eaf972b7-f04f-44db-af1b-a67ace13f77a',
    name: 'Free Cookie',
    type: 'BUY_X_GET_Y_SET_PRICE',
    startDateTime: '2021-06-08T08:04:58.329Z',
    endDateTime: '2021-06-22T08:04:58.329Z',
    terms:
        "Offer not valid with slider purchases. Offer is not transferrable. Offer cannot be combined with any other coupon or offer. Limit one coupon per person, per order, per visit. No cash value. Offer valid at participating U.S. locations only. Offer and participation in Hawaii and Alaska may vary. TM & © 2019 Arby's IP Holder, LLC. All Rights Reserved. ",
    isRedeemableInStoreOnly: false,
    isRedeemableOnlineOnly: false,
    status: 'OPEN',
    termsLanguageCode: 'en',
    description: 'with Purchase of Any Sandwich',
    applicability: {},
};

const mockDispatch = jest.fn();

jest.mock('../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch,
}));

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalPropsMock),
    useOrderLocation: () => ({ locationEntry: { storeId: '99984' } }),
}));

describe('Deal Item component', () => {
    it('should match snapshot', () => {
        const { container } = render(<DealItem onClick={onClickMock} item={mockItem as any} />);
        expect(container).toMatchSnapshot();
    });

    it('should match snapshot when unavailable', () => {
        const { container } = render(<DealItem onClick={onClickMock} item={mockItem as any} isUnavailable />);
        expect(container).toMatchSnapshot();
    });

    it('should contain title, redeem button and description', () => {
        render(<DealItem onClick={onClickMock} item={mockItem as any} />);

        const regexp = new RegExp(mockItem.description, 'i');

        expect(screen.getByText(regexp)).toBeInTheDocument();
        expect(screen.getByText(mockItem.name)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: /redeem/i })).toBeInTheDocument();
    });

    it('should call onClick event with specified id', () => {
        render(<DealItem item={mockItem as any} onClick={onClickMock} />);
        const redeemButton = screen.getByText(/redeem/i);
        userEvent.click(redeemButton);

        expect(onClickMock).toBeCalledWith('eaf972b7-f04f-44db-af1b-a67ace13f77a');
    });

    it('should not call onClick', () => {
        render(<DealItem item={mockItem as any} onClick={undefined} />);
        const redeemButton = screen.getByText(/redeem/i);
        userEvent.click(redeemButton);

        expect(redeemButton).toBeInTheDocument();
    });
});
