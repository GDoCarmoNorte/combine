import React from 'react';
import { render, screen } from '@testing-library/react';
import RadioButton from '../../../../components/atoms/radioButton';

describe('RadioButton', () => {
    it('should render component', () => {
        render(<RadioButton inputProps={{ 'aria-label': 'Radio' }} />);

        screen.getByRole('radio', { name: /Radio/i });
    });
});
