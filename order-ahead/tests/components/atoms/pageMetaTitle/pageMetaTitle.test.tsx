import React from 'react';
import { render, screen } from '@testing-library/react';
import PageMetaTitle from '../../../../components/atoms/pageMetaTitle';

const mockText = 'BWW';

describe('PageMetaTitle component', () => {
    it('should correct render', () => {
        render(<PageMetaTitle text={mockText} />);

        expect(screen.getByText(mockText)).toBeInTheDocument();
    });
});
