import { ISectionCardTextContentProps } from '../../../../components/atoms/SectionCardTextContent/types';

export const mockProps: ISectionCardTextContentProps = {
    gtmId: '',
    productCount: 1,
    linkText: '',
};
