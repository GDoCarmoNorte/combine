import React from 'react';
import { render } from '@testing-library/react';
import SectionCardTextContent from '../../../../components/atoms/SectionCardTextContent';
import { mockProps } from './SectionCardTextContent.mock';

describe('SectionCardTextContent', () => {
    it('should match with snapshot', () => {
        const { container } = render(<SectionCardTextContent {...mockProps} />);

        expect(container).toMatchSnapshot();
    });
});
