import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import Checkbox from '../../../../components/atoms/checkbox';
import userEvent from '@testing-library/user-event';

describe('Checkbox', () => {
    it('should render component', () => {
        render(<Checkbox onClick={jest.fn()} fieldName="checkbox" />);

        screen.getByRole('checkbox');
    });

    it('should render component with aria-label', () => {
        render(<Checkbox onClick={jest.fn()} fieldName="checkbox" ariaLabel="label" />);

        screen.getByRole('checkbox', { name: /label/i });
    });

    it('should call onClick', () => {
        const onClickMock = jest.fn();
        render(<Checkbox onClick={onClickMock} fieldName="checkbox" />);

        const checkbox = screen.getByRole('checkbox');
        fireEvent.click(checkbox);

        expect(onClickMock).toHaveBeenCalledWith('checkbox');
    });

    it('should call onKeyUp', () => {
        const onKeyUpMock = jest.fn();
        render(<Checkbox onKeyUp={onKeyUpMock} fieldName="checkbox" />);

        const checkbox = screen.getByRole('checkbox');
        userEvent.type(checkbox, '{enter}');

        expect(onKeyUpMock).toHaveBeenCalled();
    });
});
