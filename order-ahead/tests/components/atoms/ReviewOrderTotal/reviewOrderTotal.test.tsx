import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';

import ReviewOrderTotal from '../../../../components/atoms/ReviewOrderTotal';
import { MAX_LIMIT_ORDER, LIMIT_FOR_SUBTOTAL_ORDER } from '../../../../common/constants/bag';

describe('ReviewOrderTotal', () => {
    it('should render ReviewOrderTotal properly', async () => {
        render(
            <ReviewOrderTotal
                totalDiscount={1}
                totalPrice={1}
                totalTax={1}
                totalTaxLabel="custom label tax"
                additionalFees="TBD"
                subtotal={1}
                tooltipText="test title"
            />
        );

        await waitFor(() => {
            expect(screen.getByText(/Subtotal/i)).toBeInTheDocument();
        });

        await waitFor(() => {
            expect(screen.getByText(/TBD/i)).toBeInTheDocument();
        });

        expect(screen.getAllByText(/Total/i)).toHaveLength(2);
        expect(screen.queryByText(/tooltipTextError/i)).not.toBeInTheDocument();
    });

    it(`should render tooltip with message if subtotal is more that ${MAX_LIMIT_ORDER}`, async () => {
        render(
            <ReviewOrderTotal
                totalDiscount={1}
                totalPrice={1}
                totalTax={1}
                totalTaxLabel="custom label tax"
                subtotal={MAX_LIMIT_ORDER + 1}
                isOverLimitForOrder
                tooltipText={LIMIT_FOR_SUBTOTAL_ORDER}
            />
        );

        await waitFor(() => {
            expect(screen.getByText(/Subtotal/i)).toBeInTheDocument();
        });
        expect(screen.getAllByText(/Total/i)).toHaveLength(2);
        expect(screen.getByText(LIMIT_FOR_SUBTOTAL_ORDER)).toBeInTheDocument();
    });
});
