import React from 'react';
import { screen, render, cleanup, fireEvent } from '@testing-library/react';

import CategorySelect from '../../../../components/atoms/categorySelect';
import { useRouter } from 'next/router';
import { useUnavailableCategories } from '../../../../common/hooks/useUnavailableCategories';
import { useDomainMenuCategories } from '../../../../redux/hooks/domainMenu';

jest.mock('next/router');

jest.mock('../../../../common/hooks/useUnavailableCategories', () => ({
    useUnavailableCategories: jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainMenuCategories: jest.fn().mockReturnValue({}),
}));

const categories = [
    {
        sys: {
            contentType: {
                sys: {
                    id: 'menuCategory',
                },
            },
        },
        fields: {
            categoryName: 'Chicken',
            link: {
                fields: {
                    categoryIdList: ['1'],
                    nameInUrl: 'chicken',
                    name: 'Chicken',
                },
            },
        },
    },
    {
        sys: {
            contentType: {
                sys: {
                    id: 'menuCategory',
                },
            },
        },
        fields: {
            categoryName: 'Desserts',
            link: {
                fields: {
                    categoryIdList: ['2'],
                    nameInUrl: 'desserts',
                    name: 'Desserts',
                },
            },
        },
    },
];

describe('categorySelect', () => {
    afterEach(cleanup);

    test('should render the CategorySelect correctly', () => {
        render(<CategorySelect currentCategoryUrl="desserts" categories={categories as any} />);

        expect(screen.getByLabelText(/Category Select Section/)).toMatchSnapshot();
    });

    test('should render the expanded CategorySelect correctly', () => {
        render(<CategorySelect currentCategoryUrl="desserts" categories={categories as any} />);

        fireEvent.mouseDown(screen.getByRole('button'));

        expect(screen.getByRole('presentation')).toMatchSnapshot();
    });

    test('should call router push on select change', () => {
        const pushMock = jest.fn();
        (useRouter as jest.Mock).mockReturnValue({
            push: pushMock,
        });

        render(<CategorySelect currentCategoryUrl="desserts" categories={categories as any} />);

        fireEvent.mouseDown(screen.getByRole('button'));

        fireEvent.click(screen.getByText('Chicken'));

        expect(pushMock).toHaveBeenCalled();
    });

    test('should not show unavailable categories', () => {
        (useUnavailableCategories as jest.Mock).mockReturnValue(['1']);
        (useDomainMenuCategories as jest.Mock).mockReturnValue({ '1': {} });

        render(<CategorySelect currentCategoryUrl="desserts" categories={categories as any} />);

        expect(screen.queryByText('Chicken')).not.toBeInTheDocument();
        expect(screen.getByText('Desserts')).toBeInTheDocument();
    });
});
