import React from 'react';
import { render, screen } from '@testing-library/react';
import StartOrderButton from '../../../../components/atoms/startOrderButton/startOrderButton';
import useOrderLocation from '../../../../redux/hooks/useOrderLocation';

jest.mock('../../../../redux/hooks/useOrderLocation');

describe('StartOrderButton component', () => {
    it('StartOrderButton render properly', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({ currentLocation: { storeId: '99984' } });

        render(<StartOrderButton />);
        screen.getByText(/Start an order/i);
    });
});
