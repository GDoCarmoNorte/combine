import React from 'react';
import { render, screen } from '@testing-library/react';
import LegalMessage from '../../../../components/atoms/legalMessage';
import userEvent from '@testing-library/user-event';

const contentfulLegalMessageMock: any = {
    fields: {
        name: 'LM MainBanner',
        termsApplyTitle: 'Custom Terms Apply.',
        viewDetailsTitle: 'Custom View Details.',
        message: 'Some message',
    },
};

const contentfulLegalMessageNoTitlesMock: any = {
    fields: {
        name: 'LM MainBanner',
        message: 'Some message',
    },
};

describe('LegalMessage', () => {
    it('should render component with custom titles', () => {
        render(<LegalMessage legalMessage={contentfulLegalMessageMock} />);

        screen.getByText('Custom Terms Apply.');
        screen.getByText('Custom View Details.');
        expect(screen.queryByText('Some message')).not.toBeInTheDocument();
        expect(screen.queryByText('Legal terms and conditions')).not.toBeInTheDocument();

        userEvent.click(screen.getByText(/view details/i));

        screen.getByText('Some message');
        screen.getByText('Legal terms and conditions');

        userEvent.click(screen.getByText(/back/i));

        expect(screen.queryByText('Some message')).not.toBeInTheDocument();
        expect(screen.queryByText('Legal terms and conditions')).not.toBeInTheDocument();
    });

    it('should render component with default titles', () => {
        render(<LegalMessage legalMessage={contentfulLegalMessageNoTitlesMock} />);

        screen.getByText('Terms Apply.');
        screen.getByText('View Details.');
    });
});
