import React from 'react';
import { render, screen } from '@testing-library/react';
import RewardItem from '../../../../components/atoms/rewardItem';
import userEvent from '@testing-library/user-event';

describe('reward item component', () => {
    const props = {
        title: 'Loyalty Incentive',
        label: 'Limited Time Offer',
        labelText: 'valid 08/23-12/31/2099',
        imageUrl: '',
        description: 'Chips & Queso, Chips & Salsa',
        activeCTAText: 'Activate Offer',
        notificationTitle: 'Test notification title',
        defaultCTAText: 'Active',
        activeCTAGtmId: 'activeCTAGtmId_123',
        defaultCTAGtmId: 'defaultCTAGtmId_123',
        notificationDescription: 'Test notification description',
        defaultCtaClick: jest.fn(),
        isNotification: false,
        onNotificationToggle: jest.fn(),
    };

    it('should renders correctly with active cta button', () => {
        render(<RewardItem {...props} defaultCTAText={undefined} />);
        screen.getByText(/Activate Offer/i);
        screen.getByText(/view details/i);
        screen.getByText(/Limited Time Offer/i);
        screen.getByText(/valid 08\/23-12\/31\/2099/i);
    });

    it('defaultCtaClick should be called after click on cta button', () => {
        const defaultCtaClick = jest.fn();
        render(<RewardItem {...props} defaultCTAText={undefined} onActiveCTAClick={defaultCtaClick} />);
        const button = screen.getByText(/Activate Offer/i);
        userEvent.click(button);
        expect(defaultCtaClick).toHaveBeenCalled();
    });

    it('onNotificationToggle should be called after click on cta button', () => {
        const onNotificationToggle = jest.fn();
        render(<RewardItem {...props} onNotificationToggle={onNotificationToggle} />);
        const button = screen.getByText(/Active/i);
        userEvent.click(button);
        expect(onNotificationToggle).toBeCalled();
    });

    it('notification should be visible', () => {
        render(<RewardItem {...props} notificationOpen={true} />);
        screen.getByText(/Test notification title/i);
        screen.getByText(/Test notification description/i);
    });

    it('default CTA should have default GTM tag', () => {
        render(<RewardItem {...props} activeCTAText={null} />);
        expect(screen.getByRole('button', { name: props.defaultCTAText })).toHaveAttribute(
            'data-gtm-id',
            props.defaultCTAGtmId
        );
    });

    it('active CTA should have active GTM tag', () => {
        render(<RewardItem {...props} defaultCTAText={null} />);
        expect(screen.getByRole('button', { name: props.activeCTAText })).toHaveAttribute(
            'data-gtm-id',
            props.activeCTAGtmId
        );
    });
});
