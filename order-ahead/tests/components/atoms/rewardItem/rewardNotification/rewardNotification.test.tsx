import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import RewardNotification from './../../../../../components/atoms/rewardItem/rewardNotification/rewardNotification';

describe('reward item component', () => {
    const props = {
        isOpen: true,
        title: 'Test title',
        description: 'Test description',
        onOpen: jest.fn(),
    };

    it('should renders corretly', async () => {
        render(
            <RewardNotification {...props}>
                <button>test button</button>
            </RewardNotification>
        );

        await waitFor(() => {
            expect(screen.getByText(/Test title/i)).toBeInTheDocument();
        });
        expect(screen.getByText(/Test description/i)).toBeInTheDocument();
    });
});
