import React from 'react';
import { render, screen } from '@testing-library/react';
import PaymentCard from '../../../../components/atoms/paymentCard/index';
import { TAccountPaymentMethodTypeModel } from '../../../../@generated/webExpApi';

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({
        productDetailsPagePaths: {},
    }),
}));

describe('Payment card component', () => {
    it('should render properly', () => {
        const { container } = render(
            <PaymentCard
                cardType={'GIFT'}
                onRemove={jest.fn()}
                title="Gift Card"
                accountBalance={12.99}
                cardNumber="3086500000200000025"
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should has ".zeroBalance" class when accountBalance equals 0', () => {
        render(
            <PaymentCard
                cardType={'GIFT'}
                onRemove={jest.fn()}
                title="Gift Card Name"
                accountBalance={0}
                cardNumber="3086500000200000025"
            />
        );

        expect(screen.getByText(/\$0/i)).toHaveClass('zeroBalance');
    });

    it('should not to be in document when accountBalance undefined', () => {
        render(
            <PaymentCard
                cardType={'GIFT'}
                onRemove={jest.fn()}
                title="Gift Card Name"
                cardNumber="3086500000200000025"
            />
        );
        expect(screen.queryByText('Balance:')).not.toBeInTheDocument();
    });

    it('should show masked gift card number', () => {
        render(
            <PaymentCard
                cardType={'GIFT'}
                onRemove={jest.fn()}
                title="Gift Card Name"
                accountBalance={12.99}
                cardNumber="3086500000200000025"
            />
        );

        expect(screen.getByText('***************0025')).toBeInTheDocument();
    });

    it('should show masked credit card number', () => {
        render(
            <PaymentCard
                cardType={TAccountPaymentMethodTypeModel.Ax}
                onRemove={jest.fn()}
                title="Gift Card Name"
                accountBalance={12.99}
                cardNumber="3086500000200000025"
            />
        );

        expect(screen.getByText('**** 0025')).toBeInTheDocument();
    });
});
