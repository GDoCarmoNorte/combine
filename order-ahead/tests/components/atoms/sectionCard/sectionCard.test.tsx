import React from 'react';
import { render, screen } from '@testing-library/react';

import SectionCard from '../../../../components/atoms/sectionCard';
import { cardMock, cardMockWithProducts, cardMockWithInvisibleProducts } from './sectionCard.mock';

jest.mock('../../../../common/hooks/useUnavailableCategories');

describe('SectionCard', () => {
    it('should match with snapshot', () => {
        const { container } = render(<SectionCard isLocationOrderAvailable={true} card={cardMock} />);

        expect(container).toMatchSnapshot();
    });

    it('should render with the "ORDER" link if location order is available', () => {
        render(<SectionCard isLocationOrderAvailable={true} card={cardMock} />);

        expect(screen.getByRole('link', { name: 'Menu Category' })).toHaveTextContent('ORDER');
    });

    it('should render with the "VIEW ALL" link if location order is not available', () => {
        render(<SectionCard isLocationOrderAvailable={false} card={cardMock} />);

        expect(screen.getByRole('link', { name: 'Menu Category' })).toHaveTextContent('VIEW ALL');
    });

    it('should render with "0 items" in counter', () => {
        render(<SectionCard isLocationOrderAvailable={false} card={cardMock} />);

        expect(screen.getByText('0 items')).toBeInTheDocument();
    });

    it('should render with "9 items" in counter', () => {
        render(<SectionCard isLocationOrderAvailable={false} card={cardMockWithProducts} />);

        expect(screen.getByText('9 items')).toBeInTheDocument();
    });

    it('should render with "9 items" in counter with invisible products', () => {
        render(<SectionCard isLocationOrderAvailable={false} card={cardMockWithInvisibleProducts} />);

        expect(screen.getByText('9 items')).toBeInTheDocument();
    });
});
