export const cardMock = {
    fields: {
        image: {
            fields: {
                description: 'Meals Signatures Gyro Greek',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png',
                },
            },
        },
        categoryName: 'Meals',
        products: [],
        subcategories: [],
        link: {
            fields: {
                name: 'Meals',
                nameInUrl: 'meals',
            },
        },
    },
    sys: {
        contentType: {
            sys: {
                id: 'menuCategory',
            },
        },
    },
} as any;

const product = {
    fields: {
        isVisible: true,
    },
};
const invisibleProduct = {
    fields: {
        isVisible: false,
    },
    sys: {
        contentType: {
            sys: {
                id: 'menuCategory',
            },
        },
    },
};
export const cardMockWithProducts = {
    fields: {
        image: {
            fields: {
                description: 'Meals Signatures Gyro Greek',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png',
                },
            },
        },
        categoryName: 'Meals',
        products: [product, product, product, product],
        subcategories: [
            {
                fields: {
                    products: [product, product],
                },
            },
            {
                fields: {
                    products: [product, product, product],
                },
            },
        ],
        link: {
            fields: {
                name: 'Meals',
                nameInUrl: 'meals',
            },
        },
    },
    sys: {
        contentType: {
            sys: {
                id: 'menuCategory',
            },
        },
    },
} as any;

export const cardMockWithInvisibleProducts = {
    fields: {
        image: {
            fields: {
                description: 'Meals Signatures Gyro Greek',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png',
                },
            },
        },
        categoryName: 'Meals',
        products: [product, product, product, product, invisibleProduct],
        subcategories: [
            {
                fields: {
                    products: [product, product, invisibleProduct],
                },
            },
            {
                fields: {
                    products: [product, product, product],
                },
            },
        ],
        link: {
            fields: {
                name: 'Meals',
                nameInUrl: 'meals',
            },
        },
    },
    sys: {
        contentType: {
            sys: {
                id: 'menuCategory',
            },
        },
    },
} as any;
