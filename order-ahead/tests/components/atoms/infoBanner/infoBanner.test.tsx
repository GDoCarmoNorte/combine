import React from 'react';
import { shallow } from 'enzyme';

import InfoBanner from '../../../../components/atoms/infoBanner';

describe('InfoBanner component', () => {
    it('should render the component without props and with default styles', () => {
        const wrapper = shallow(<InfoBanner />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the component without title', () => {
        const wrapper = shallow(
            <InfoBanner mainText="mainText" description="description" backgroundColor="#ffffff" textColor="#000000" />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the component without description', () => {
        const wrapper = shallow(
            <InfoBanner title="title" mainText="mainText" backgroundColor="#ffffff" textColor="#000000" />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the component without mainText', () => {
        const wrapper = shallow(
            <InfoBanner title="title" description="description" backgroundColor="#ffffff" textColor="#000000" />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the component with props', () => {
        const wrapper = shallow(
            <InfoBanner
                title="title"
                mainText="mainText"
                description="description"
                backgroundColor="#ffffff"
                textColor="#000000"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
