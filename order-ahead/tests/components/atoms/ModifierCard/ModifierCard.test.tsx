import React from 'react';
import { render, screen } from '@testing-library/react';
import ModifierCard from '../../../../components/atoms/ModifierCard';
import { isCaloriesDisplayEnabled } from '../../../../lib/getFeatureFlags';
import userEvent from '@testing-library/user-event';

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
    };
});

const contentfulProductMock: any = {
    fields: {
        image: 'product.jpeg',
    },
};

describe('ModifierCard', () => {
    it('should render component with colories', () => {
        render(
            <ModifierCard
                selectionType="none"
                selectorType="default"
                quantity={1}
                productName="burger"
                modifierItemId=""
                onClick={jest.fn()}
                calories={200}
                contentfulProduct={contentfulProductMock}
            />
        );

        expect(screen.getByText('200 calories')).toBeInTheDocument();
    });

    it('should not render colories for Canada', () => {
        (isCaloriesDisplayEnabled as jest.Mock).mockReturnValueOnce(false);

        render(
            <ModifierCard
                selectionType="none"
                selectorType="default"
                quantity={1}
                productName="burger"
                modifierItemId=""
                onClick={jest.fn()}
                contentfulProduct={contentfulProductMock}
            />
        );

        expect(screen.queryByText('200 calories')).not.toBeInTheDocument();
    });

    it('should render select required modifiers groups message', () => {
        render(
            <ModifierCard
                selectionType="none"
                selectorType="default"
                quantity={1}
                productName="burger"
                modifierItemId=""
                onClick={jest.fn()}
                contentfulProduct={contentfulProductMock}
                requireSelectionModifiersGroups={['Dressing', 'Options']}
            />
        );
        expect(screen.getByText('Select Dressing, Options')).toBeInTheDocument();
    });

    it('should fire click and modify if nested modifiers require selection', () => {
        const onModifyClickMock = jest.fn();
        const onClickMock = jest.fn();

        render(
            <ModifierCard
                selectionType="none"
                selectorType="default"
                quantity={1}
                productName="burger"
                modifierItemId=""
                onClick={onClickMock}
                onModifyClick={onModifyClickMock}
                contentfulProduct={contentfulProductMock}
                requireSelectionModifiersGroups={['Dressing', 'Options']}
            />
        );

        userEvent.click(screen.getByText(/burger/i));

        expect(onClickMock).toHaveBeenCalled();
        expect(onModifyClickMock).toHaveBeenCalled();
    });
});
