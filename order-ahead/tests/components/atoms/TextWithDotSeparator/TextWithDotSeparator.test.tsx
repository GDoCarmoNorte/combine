import React from 'react';
import { shallow } from 'enzyme';
import TextWithDotSeparator from '../../../../components/atoms/TextWithDotSeparator';

describe('TextWithDotSeparator', () => {
    it('should render with dot when left and right texts are defined', () => {
        const wrapper = shallow(<TextWithDotSeparator leftText="left side" rightText="right side" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render only left text', () => {
        const wrapper = shallow(<TextWithDotSeparator leftText="left side" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render only right text', () => {
        const wrapper = shallow(<TextWithDotSeparator rightText="right side" />);

        expect(wrapper).toMatchSnapshot();
    });
});
