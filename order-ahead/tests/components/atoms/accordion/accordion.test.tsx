import React from 'react';
import { render, screen } from '@testing-library/react';
import Accordion from '../../../../components/atoms/accordion';
import userEvent from '@testing-library/user-event';
import isMobileScreen from '../../../../lib/isMobileScreen';
import isSmallScreen from '../../../../lib/isSmallScreen';

jest.mock('../../../../lib/isMobileScreen', () => jest.fn().mockReturnValue(false));
jest.mock('../../../../lib/isSmallScreen', () => jest.fn().mockReturnValue(false));

const expandedHeaderText = 'expanded accordion header';
const collapsedHeaderText = 'collapsed accordion header';
const expandedContentText = 'expanded accordion content';
const collapsedContentText = 'collapsed accordion content';
const renderHeaderFn = (isExpanded, buttonRender, onClickHandler) => {
    return (
        <div onClick={onClickHandler}>
            {isExpanded ? expandedHeaderText : collapsedHeaderText}
            <div>{buttonRender()}</div>
        </div>
    );
};

const renderContentFn = (isExpanded) => {
    return <div>{isExpanded ? expandedContentText : collapsedContentText}</div>;
};

describe('accordion component', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    const props = {
        renderHeader: renderHeaderFn,
        renderContent: renderContentFn,
        containerClassName: 'contentClassName',
        isExpandedByDefault: false,
        title: 'test',
        iconClassName: 'iconClassName',
    };

    it('should renders corretly with passed content', () => {
        render(<Accordion {...props} />);
        expect(screen.getByText(collapsedHeaderText)).toBeInTheDocument();
        expect(screen.getByText(collapsedContentText)).toBeInTheDocument();
    });

    it('should show expanded accordion if requested', () => {
        const testCaseProps = {
            ...props,
            isExpandedByDefault: true,
        };
        render(<Accordion {...testCaseProps} />);
        expect(screen.getByText(expandedHeaderText)).toBeInTheDocument();
        expect(screen.getByText(expandedContentText)).toBeInTheDocument();
    });

    it('should handle accordion click correctly', async () => {
        render(<Accordion {...props} />);
        const accordionHeaderComponent = screen.getByText(collapsedHeaderText);
        userEvent.click(accordionHeaderComponent);
        expect(screen.getByText(expandedHeaderText)).toBeInTheDocument();
        expect(screen.getByText(expandedContentText)).toBeInTheDocument();
    });

    it('should use correct size of accordion icon if mobile screen', () => {
        (isMobileScreen as jest.Mock).mockReturnValue(true);
        render(<Accordion {...props} />);
        expect(screen.getByLabelText(/Expand/i)).toHaveClass('s');
    });

    it('should use correct size of accordion icon if desktop screen', () => {
        (isMobileScreen as jest.Mock).mockReturnValue(false);
        (isSmallScreen as jest.Mock).mockReturnValue(false);
        render(<Accordion {...props} />);
        expect(screen.getByLabelText(/Expand/i)).toHaveClass('m');
    });
});
