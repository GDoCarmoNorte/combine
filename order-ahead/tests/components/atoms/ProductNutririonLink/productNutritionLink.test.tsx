import React from 'react';
import { render, screen } from '@testing-library/react';

import ProductNutritionLink from '../../../../components/atoms/ProductNutritionLink';
import { IExternalLink } from '../../../../@generated/@types/contentful';

const productNutrition = {
    link: {
        sys: {
            id: '1C0L4SzmyWYs5I2t5dXRvJ',
            type: 'Entry',
            createdAt: '2020-10-12T12:58:59.291Z',
            updatedAt: '2021-01-12T22:49:44.117Z',
            contentType: {
                sys: {
                    type: 'Link',
                    linkType: 'ContentType',
                    id: 'externalLink',
                },
            },
            locale: 'en-US',
        },
        fields: {
            name: 'Franchising',
            nameInUrl: 'https://arbysfranchising.com/',
        },
    } as IExternalLink,
};

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({
        productDetailsPagePaths: {},
    }),
}));

describe('ProductNutritionLink', () => {
    it('should render component without title and message', () => {
        render(<ProductNutritionLink productNutrition={productNutrition} />);

        screen.getByText('View full nutrition & allergen information');
        expect(screen.getByRole('link')).toHaveAttribute('href', 'https://arbysfranchising.com/');
    });

    it('should show default message if productNutrition=null', () => {
        render(<ProductNutritionLink productNutrition={null} />);

        screen.getByText('Full nutrition details are not available for this item.');
    });

    it('should show text if exist', () => {
        render(<ProductNutritionLink productNutrition={{ ...productNutrition, text: 'TEXT' }} />);

        screen.getByText('TEXT');
    });
});
