import React from 'react';
import { render, screen } from '@testing-library/react';

import BackToTop from '../../../../components/atoms/BackToTop';

describe('BackToTop', () => {
    it('should render and show text and button', () => {
        render(<BackToTop />);

        screen.getByText(/Back to top/i);
    });
});
