import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';

describe('InspireSimpleLink component', () => {
    it('should match snapshot', () => {
        const { container } = render(<InspireSimpleLink link={'/test'} />);

        expect(container).toMatchSnapshot();
    });

    it('should match snapshot with className', () => {
        const { container } = render(<InspireSimpleLink link={'/test'} className="className" />);

        expect(container).toMatchSnapshot();
    });

    it('should match snapshot with children', () => {
        const { container } = render(<InspireSimpleLink link={'/test'}>children</InspireSimpleLink>);

        expect(container).toMatchSnapshot();
    });

    it('should match snapshot with phone link', () => {
        const { container } = render(<InspireSimpleLink isPhone link={'+10001000'} />);

        expect(container).toMatchSnapshot();
    });

    it('should render link with target set to _blank attribute when newtab prop is passed but link is not external', () => {
        render(
            <InspireSimpleLink newtab link={'test.com'}>
                children
            </InspireSimpleLink>
        );
        expect(screen.getByRole('link')).toHaveAttribute('target', '_blank');
    });

    it('should pass through additional attributes to <a/> tag', async () => {
        render(
            <InspireSimpleLink aria-label="label" link={'/test'}>
                TEST
            </InspireSimpleLink>
        );

        expect(screen.getByRole('link')).toHaveAttribute('aria-label', 'label');
    });

    it('should call onClick after click on link', () => {
        const onClickMock = jest.fn();
        render(<InspireSimpleLink link={'/test'} onClick={onClickMock} />);

        userEvent.click(screen.getByRole('link'));

        expect(onClickMock).toHaveBeenCalledTimes(1);
    });
});
