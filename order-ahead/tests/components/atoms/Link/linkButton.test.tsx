import React from 'react';
import { render, screen } from '@testing-library/react';
import { InspireLinkButton } from '../../../../components/atoms/link';

describe('InspireLinkButton', () => {
    it('should render with primary link type', () => {
        render(<InspireLinkButton linkType="primary">Primary link</InspireLinkButton>);

        const button = screen.getByRole('button', { name: /Primary link/i });

        expect(button).toMatchSnapshot();
    });

    it('should render secondary link type', () => {
        render(<InspireLinkButton linkType="secondary">Secondary link</InspireLinkButton>);

        const button = screen.getByRole('button', { name: /Secondary link/i });

        expect(button).toMatchSnapshot();
    });

    it('should render with className', () => {
        render(
            <InspireLinkButton linkType="secondary" className="class">
                Secondary link
            </InspireLinkButton>
        );

        const button = screen.getByRole('button', { name: /Secondary link/i });

        expect(button.className).toBe('link linkButton linkSecondaryActive class');
    });

    it('should render with gtm id', () => {
        render(
            <InspireLinkButton linkType="secondary" gtmId="link">
                Secondary link
            </InspireLinkButton>
        );

        const button = screen.getByRole('button', { name: /Secondary link/i });

        expect(button.dataset.gtmId).toBe('link');
    });
});
