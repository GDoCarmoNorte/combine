import React from 'react';
import { mount } from 'enzyme';
import { render, screen } from '@testing-library/react';

import productMock from '../../../mocks/contentfulProduct.mock';
import globalPropsMock from '../../../mocks/globalProps.mock';
import phoneLinkMock from '../../../mocks/phoneLink.mock';
import externalLinkMock from '../../../mocks/externalLink.mock';

import { InspireLink } from '../../../../components/atoms/link';

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('InspireLink component', () => {
    it('should match snapshot for product link', () => {
        const wrapper = mount(<InspireLink link={productMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot for product link with currentCategoryUrl', () => {
        const wrapper = mount(<InspireLink link={productMock as any} currentCategoryUrl="limited-time" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with className', () => {
        const wrapper = mount(<InspireLink link={productMock as any} className="className" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with children', () => {
        const wrapper = mount(<InspireLink link={productMock as any}>children</InspireLink>);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with phone link', () => {
        const wrapper = mount(<InspireLink link={phoneLinkMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with external link', () => {
        const wrapper = mount(<InspireLink link={externalLinkMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render link with target set to _blank attribute when newtab prop is passed but link is not external', () => {
        render(
            <InspireLink newtab link={productMock as any}>
                children
            </InspireLink>
        );
        expect(screen.getByRole('link')).toHaveAttribute('target', '_blank');
    });

    it('should pass through additional attributes to <a/> tag', async () => {
        render(
            <InspireLink aria-label="label" link={productMock as any}>
                TEST
            </InspireLink>
        );

        expect(screen.getByRole('link')).toHaveAttribute('aria-label', 'label');
    });

    it('should call onClick after click on link', () => {
        const onClickMock = jest.fn();
        const wrapper = mount(<InspireLink link={externalLinkMock as any} onClick={onClickMock} />);

        wrapper.find('a').simulate('click');

        expect(onClickMock).toHaveBeenCalledTimes(1);
    });
});
