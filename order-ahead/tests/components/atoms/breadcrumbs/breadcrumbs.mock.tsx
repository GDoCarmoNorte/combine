export const breadcrumbsPathsMock = [
    {
        title: 'locations',
        href: '/locations',
    },
    {
        title: 'all locations',
        href: '/locations/all',
    },
    {
        title: 'houston',
        href: `/locations/houston`,
    },
];
