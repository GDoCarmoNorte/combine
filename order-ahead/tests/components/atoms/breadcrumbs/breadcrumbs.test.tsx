import React from 'react';
import { shallow } from 'enzyme';
import { useRouter } from 'next/router';

import Breadcrumbs from '../../../../components/atoms/Breadcrumbs';
import { breadcrumbsPathsMock } from './breadcrumbs.mock';

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

describe('breadcrumbs', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('should render breadcrumbs for 4 levels path', () => {
        (useRouter as jest.Mock).mockReturnValue({
            asPath: 'path/path-name2/path-name3/path-name4/',
            pathname: 'path/[path-name2]/[path-name3]/[path-name4]',
        });
        const wrapper = shallow(<Breadcrumbs />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render breadcrumbs for 3 levels path', () => {
        (useRouter as jest.Mock).mockReturnValue({
            asPath: 'path/path-name2/path-name3/',
            pathname: 'path/[path-name2]/[path-name3]',
        });
        const wrapper = shallow(<Breadcrumbs />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render breadcrumbs for 2 levels path', () => {
        (useRouter as jest.Mock).mockReturnValue({ asPath: 'path/path2/', pathname: 'path/[path2]' });
        const wrapper = shallow(<Breadcrumbs />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should not render breadcrumbs for 1 level path', () => {
        (useRouter as jest.Mock).mockReturnValue({ asPath: 'path/', pathname: 'path' });
        const wrapper = shallow(<Breadcrumbs />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should not render breadcrumbs for empty path', () => {
        (useRouter as jest.Mock).mockReturnValue({ asPath: '/', pathname: '' });
        const wrapper = shallow(<Breadcrumbs />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render', () => {
        const paths = breadcrumbsPathsMock;

        (useRouter as jest.Mock).mockReturnValue({ asPath: '/', pathname: '' });
        const wrapper = shallow(<Breadcrumbs paths={paths} />);

        expect(wrapper).toMatchSnapshot();
    });
});
