import React from 'react';
import { render, screen } from '@testing-library/react';
import { AddNewCard } from '../../../../components/atoms/addNewCard';
import { useNotifications } from '../../../../redux/hooks';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks');
(useNotifications as jest.Mock).mockReturnValue({
    actions: { enqueueError: jest.fn() },
});

describe('AddNewCard', () => {
    const mockAddNewCard = jest.fn();
    const closeOverlay = jest.fn();
    const handlePaymentMethods = jest.fn();
    const handleSetLoading = jest.fn();

    it('should render addNewCard', () => {
        const { container } = render(
            <AddNewCard
                text={''}
                onAddNewCard={mockAddNewCard}
                customerId={'test-123'}
                isOverlayOpen={false}
                closeOverlay={closeOverlay}
                jwtToken={'ekysdfsdfd'}
                handleFetchPaymentMethods={handlePaymentMethods}
                handleSetLoading={handleSetLoading}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should only show addNewButton when button is not clicked', () => {
        render(
            <AddNewCard
                text={''}
                onAddNewCard={mockAddNewCard}
                customerId={'test-123'}
                isOverlayOpen={false}
                closeOverlay={closeOverlay}
                jwtToken={'ekysdfsdfd'}
                handleFetchPaymentMethods={handlePaymentMethods}
                handleSetLoading={handleSetLoading}
            />
        );

        expect(screen.getByTestId('addNewButton')).toBeInTheDocument();
        expect(screen.queryByTestId('AddEditComponent')).not.toBeInTheDocument();
    });

    it('should only show AddEditComponent when button is clicked', () => {
        render(
            <AddNewCard
                text={''}
                onAddNewCard={mockAddNewCard}
                customerId={'test-123'}
                isOverlayOpen={true}
                closeOverlay={closeOverlay}
                jwtToken={'ekysdfsdfd'}
                handleFetchPaymentMethods={handlePaymentMethods}
                handleSetLoading={handleSetLoading}
            />
        );

        expect(screen.queryByTestId('addNewButton')).not.toBeInTheDocument();
        expect(screen.getByTestId('AddEditComponent')).toBeInTheDocument();
    });
});
