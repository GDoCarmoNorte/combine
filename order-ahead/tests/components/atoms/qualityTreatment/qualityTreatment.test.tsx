/* eslint-disable testing-library/no-node-access */
import React from 'react';
import { screen, render, cleanup } from '@testing-library/react';

import QualityTreatment from '../../../../components/atoms/qualityTreatment';

describe('qualityTreatment', () => {
    afterEach(cleanup);

    test('should render the QualityTreatment without subheader and icon', () => {
        const { container } = render(<QualityTreatment header="Test Quality Treatment Header" />);

        expect(container.firstChild.childNodes.length).toEqual(1);
        expect(container.firstChild.lastChild.childNodes.length).toEqual(1);
        expect(screen.getByText('Test Quality Treatment Header')).toHaveClass('m-quality-treatment-header');
    });

    test('should render the QualityTreatment without subheader', () => {
        const { container } = render(<QualityTreatment header="Test Quality Treatment Header" showIcon />);

        expect(container.firstChild.childNodes.length).toEqual(2);
        expect(container.firstChild.lastChild.childNodes.length).toEqual(1);
        expect(screen.getByText('Test Quality Treatment Header')).toHaveClass('m-quality-treatment-header');
    });

    test('should render the QualityTreatment without icon', () => {
        const { container } = render(
            <QualityTreatment header="Test Quality Treatment Header" subheader="Test Quality Treatment SubHeader" />
        );

        expect(container.firstChild.childNodes.length).toEqual(1);
        expect(container.firstChild.lastChild.childNodes.length).toEqual(2);
        expect(screen.getByText('Test Quality Treatment Header')).toHaveClass('m-quality-treatment-header');
        expect(screen.getByText('Test Quality Treatment SubHeader')).toHaveClass('m-quality-treatment-subheader');
    });

    test('should render the QualityTreatment with subheader and icon', () => {
        const { container } = render(
            <QualityTreatment
                header="Test Quality Treatment Header"
                subheader="Test Quality Treatment SubHeader"
                showIcon
            />
        );

        expect(container.firstChild.childNodes.length).toEqual(2);
        expect(container.firstChild.lastChild.childNodes.length).toEqual(2);
        expect(screen.getByText('Test Quality Treatment Header')).toHaveClass('m-quality-treatment-header');
        expect(screen.getByText('Test Quality Treatment SubHeader')).toHaveClass('m-quality-treatment-subheader');
    });
});
