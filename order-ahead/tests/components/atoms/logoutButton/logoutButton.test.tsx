import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import LogoutButton from '../../../../components/atoms/logoutButton';

const logout = jest.fn();
jest.mock('../../../../common/hooks/useLogout', () => ({
    useLogout: () => ({
        logoutAndClearCookies: logout,
    }),
}));

describe('LogoutButton component', () => {
    it('should match snapshot for logoutButton', () => {
        const { container } = render(<LogoutButton />);
        expect(container).toMatchSnapshot();
    });

    it('should find element by LOGOUT text in DOM with given  class', () => {
        render(<LogoutButton className="randomClassName" />);
        expect(screen.getByText(/logout/i)).toHaveClass('randomClassName');
    });

    it('should call logout function on click with correct returnUrl', () => {
        render(<LogoutButton className="randomClassName" />);
        const logoutButton = screen.getByRole('button', { name: /logout/i });
        userEvent.click(logoutButton);
        expect(logout).toHaveBeenCalledTimes(1);
    });

    it('should call logout function onKeyDown event with correct returnUrl', () => {
        render(<LogoutButton className="randomClassName" />);
        const logoutButton = screen.getByRole('button', { name: /logout/i });
        userEvent.type(logoutButton, '{capslock}');
        expect(logout).toHaveBeenCalledTimes(2);
    });
});
