import React from 'react';
import { shallow } from 'enzyme';
import SectionHeader from '../../../../components/atoms/sectionHeader';

describe('SectionHeader', () => {
    it('should render component', () => {
        const wrapper = shallow(<SectionHeader text="Header" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with configured tag', () => {
        const wrapper = shallow(<SectionHeader text="Header" tag="h1" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without bullet', () => {
        const wrapper = shallow(<SectionHeader text="Header" tag="h1" showBullet={false} />);

        expect(wrapper).toMatchSnapshot();
    });
});
