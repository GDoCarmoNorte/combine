import React from 'react';
import { mount, shallow } from 'enzyme';

import SkipLink from '../../../../components/atoms/skipLink';

describe('SkipLink', () => {
    it('should render SkipLink correctly', () => {
        const wrapper = shallow(<SkipLink />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should have "skip to main content" text', () => {
        const wrapper = shallow(<SkipLink />);

        expect(wrapper.text()).toBe('skip to main content');
    });

    it('should call link after click', () => {
        const handleClick = jest.fn();
        const wrapper = mount(<SkipLink />);
        wrapper.props().onClick = handleClick;
        wrapper.props().onClick();

        expect(handleClick).toHaveBeenCalled();
    });
});
