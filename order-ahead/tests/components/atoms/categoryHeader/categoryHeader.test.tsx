/* eslint-disable testing-library/no-node-access */
import React from 'react';
import { screen, render, cleanup } from '@testing-library/react';

import CategoryHeader from '../../../../components/atoms/categoryHeader';

describe('categoryHeader', () => {
    afterEach(cleanup);

    test('should render the CategoryHeader without notationText and bullet', () => {
        const { container } = render(<CategoryHeader text="Test Category Header Text" />);

        expect(container.firstChild.childNodes.length).toEqual(2);
        expect(container.firstChild.lastChild.textContent).toEqual('');
        expect(screen.getByText('Test Category Header Text')).toHaveClass('m-category-header-text');
    });

    test('should render the CategoryHeader without notationText', () => {
        const { container } = render(<CategoryHeader text="Test Category Header Text" showBullet />);

        expect(container.firstChild.childNodes.length).toEqual(3);
        expect(container.firstChild.lastChild.textContent).toEqual('');
        expect(screen.getByText('Test Category Header Text')).toHaveClass('m-category-header-text');
    });

    test('should render the CategoryHeader without bullet', () => {
        const { container } = render(
            <CategoryHeader text="Test Category Header Text" notationText="Test Category Header NotationText" />
        );

        expect(container.firstChild.childNodes.length).toEqual(2);
        expect(container.firstChild.lastChild.textContent).toEqual('Test Category Header NotationText');
        expect(screen.getByText('Test Category Header Text')).toHaveClass('m-category-header-text');
    });

    test('should render the CategoryHeader with notationText and bullet', () => {
        const { container } = render(
            <CategoryHeader
                text="Test Category Header Text"
                notationText="Test Category Header NotationText"
                showBullet
            />
        );

        expect(container.firstChild.childNodes.length).toEqual(3);
        expect(container.firstChild.lastChild.textContent).toEqual('Test Category Header NotationText');
        expect(screen.getByText('Test Category Header Text')).toHaveClass('m-category-header-text');
    });
});
