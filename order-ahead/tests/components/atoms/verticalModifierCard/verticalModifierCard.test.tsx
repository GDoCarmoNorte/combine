import React from 'react';
import { render, screen } from '@testing-library/react';
import { VerticalModifierCard } from '../../../../components/atoms/verticalModifierCard';
import userEvent from '@testing-library/user-event';
import { isCaloriesDisplayEnabled } from '../../../../lib/getFeatureFlags';

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
    };
});

describe('VerticalModifierCard', () => {
    const mockOnClick = jest.fn();
    const contentfulProductMock: any = {
        fields: {
            image: 'product.jpeg',
        },
    };

    it('should render component', () => {
        const { container } = render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should not render calories info for Canada', () => {
        (isCaloriesDisplayEnabled as jest.Mock).mockReturnValueOnce(false).mockReturnValueOnce(false);

        const { container } = render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should message instead of image without contentful product', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={undefined}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        screen.getByText(
            'CONTENT CREATOR WARNING: Please create a product with id = default in Contentful for default image'
        );
    });

    it('should render with single selection type', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                selectionType="single"
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        expect(screen.getByRole('radio', { name: 'Select Name', checked: false })).toBeInTheDocument();
    });

    it('should render with multi selection type', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                selectionType="multi"
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        expect(screen.getByRole('checkbox', { name: 'Select Name', checked: false })).toBeInTheDocument();
    });

    it('should render with quantity selector', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                quantity={3}
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="quantity"
                selected
                contentfulProduct={contentfulProductMock}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        screen.getByRole('button', { name: /Add one Name/i });
        screen.getByRole('button', { name: /Remove one Name/i });
        const input = screen.getByRole('textbox', { name: 'quantity' });
        expect(input).toHaveAttribute('value', '3');
    });

    it('should render with modify button', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        screen.getByRole('button', { name: /Modify/i });
    });

    it('should show modifiers text if selected', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
                selected
            />
        );

        screen.getByText('modifiersText');
    });

    it('should not fire click if disabled', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
                disabled
            />
        );

        userEvent.click(screen.getByText('Name'));
        expect(mockOnClick).not.toHaveBeenCalled();
    });

    it('should not fire click if not disabled', () => {
        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={mockOnClick}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        userEvent.click(screen.getByText('Name'));
        expect(mockOnClick).toHaveBeenCalled();
    });

    it('should fire click if click on modify and not selected', () => {
        const onModifyClickMock = jest.fn();
        const onClickMock = jest.fn();

        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={onClickMock}
                onModifyClick={onModifyClickMock}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
            />
        );

        userEvent.click(screen.getByText('Modify'));
        expect(onModifyClickMock).toHaveBeenCalled();
        expect(onClickMock).toHaveBeenCalled();
    });

    it('should not fire click if click on modify and selected', () => {
        const onModifyClickMock = jest.fn();
        const onClickMock = jest.fn();

        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={onClickMock}
                onModifyClick={onModifyClickMock}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
                canIncrement={false}
                selected
            />
        );

        userEvent.click(screen.getByText('Modify'));
        expect(onModifyClickMock).toHaveBeenCalled();
        expect(onClickMock).not.toHaveBeenCalled();
    });

    it('should render select required modifiers groups message', () => {
        const onModifyClickMock = jest.fn();
        const onClickMock = jest.fn();

        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={onClickMock}
                onModifyClick={onModifyClickMock}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
                canIncrement={false}
                selected
                requireSelectionModifiersGroups={['Dressing', 'Options']}
            />
        );

        expect(screen.getByText('Select Dressing, Options')).toBeInTheDocument();
    });

    it('should fire click and modify if nested modifiers require selection', () => {
        const onModifyClickMock = jest.fn();
        const onClickMock = jest.fn();

        render(
            <VerticalModifierCard
                productName="Name"
                price={1}
                calories={150}
                showModify
                onClick={onClickMock}
                onModifyClick={onModifyClickMock}
                modifierItemId="1234"
                selectorType="default"
                contentfulProduct={contentfulProductMock}
                modifiersText="modifiersText"
                maxQuantityTooltipText="maxQuantityTooltipText"
                requireSelectionModifiersGroups={['Dressing', 'Options']}
            />
        );

        userEvent.click(screen.getByText('Name'));
        expect(mockOnClick).toHaveBeenCalled();
        expect(onModifyClickMock).toHaveBeenCalled();
    });
});
