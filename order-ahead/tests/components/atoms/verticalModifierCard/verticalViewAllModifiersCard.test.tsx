import React from 'react';
import { render } from '@testing-library/react';
import { VerticalViewAllModifiersCard } from '../../../../components/atoms/verticalModifierCard';

describe('VerticalViewAllModifiersCard', () => {
    it('should render component', () => {
        const { container } = render(<VerticalViewAllModifiersCard title="View All Sauces" onClick={jest.fn()} />);

        expect(container).toMatchSnapshot();
    });
});
