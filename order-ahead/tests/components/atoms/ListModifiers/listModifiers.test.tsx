import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import ListModifiers from '../../../../components/atoms/ListModifiers';
import { ModifierGroupType } from '../../../../redux/types';

const addedModifiersMock = [
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640212366',
        name: 'Big Eye Swiss',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640212402',
        name: 'Leaf Lettuce',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640212725',
        name: 'Red Onion',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640212994',
        name: 'Natural Cheddar',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640213227',
        name: 'Pepper Bacon',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640213240',
        name: 'Tomato',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640216159',
        name: 'Diced Jalapeno',
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'ARBYS-WEBOA-640216184',
        name: "Arby's Sauce Packet",
        price: 1,
        calories: 5,
    },
    {
        quantity: 1,
        productId: 'arb-itm-013-011',
        name: 'No Bun',
        price: 0,
        calories: 0,
    },
];

const removedModifiersMock = [
    {
        name: 'Red Ranch',
        defaultQuantity: 1,
        minQuantity: 0,
        maxQuantity: 0,
        productId: 'ARBYS-WEBOA-640212570',
    },
    {
        name: 'Cheddar Sauce',
        defaultQuantity: 1,
        minQuantity: 0,
        maxQuantity: 0,
        productId: 'ARBYS-WEBOA-640212761',
    },
    {
        name: 'Spicy Chili Sauce',
        defaultQuantity: 1,
        minQuantity: 0,
        maxQuantity: 0,
        price: 1,
        productId: 'ARBYS-WEBOA-000000000',
    },
];

describe('ListModifiers', () => {
    it('should render ListModifiers without props', () => {
        const wrapper = shallow(<ListModifiers addedModifiers={[]} removedModifiers={[]} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ListModifiers correct without collapse button', () => {
        const wrapper = shallow(
            <ListModifiers addedModifiers={addedModifiersMock.slice(0, 3)} removedModifiers={removedModifiersMock} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ListModifiers correct with collapse button', () => {
        const wrapper = shallow(
            <ListModifiers addedModifiers={addedModifiersMock} removedModifiers={removedModifiersMock} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should show list items after click button', () => {
        const wrapper = shallow(
            <ListModifiers addedModifiers={addedModifiersMock} removedModifiers={removedModifiersMock} />
        );

        wrapper
            .find('a')
            .at(0)
            .simulate('click', { preventDefault: () => null });
        expect(wrapper).toMatchSnapshot();
    });

    it('should render ListModifiers with wingtype modifier', () => {
        render(
            <ListModifiers
                addedModifiers={[
                    {
                        quantity: 1,
                        productId: 'arb-itm-013-011',
                        name: 'All Flats',
                        price: 1,
                        calories: 0,
                        metadata: { MODIFIER_GROUP_TYPE: ModifierGroupType.WINGTYPE },
                    },
                ]}
                removedModifiers={[]}
            />
        );

        expect(screen.getByText(/All Flats/i)).toBeInTheDocument();
    });

    it('should render ListModifiers with sub modifiers', () => {
        const { container } = render(
            <ListModifiers
                addedModifiers={[
                    {
                        name: 'Tots',
                        quantity: 1,
                        productId: 'IDPModifier-30749',
                        price: 1,
                        calories: 620,
                        metadata: { MODIFIER_GROUP_ID: '16051', MODIFIER_GROUP_TYPE: 'Selections' },
                        modifiers: [
                            { name: 'Chili', quantity: 1, productId: 'IDPModifier-13117', price: 0.75, calories: 110 },
                            { name: 'Bacon', quantity: 1, productId: 'IDPModifier-13118', price: 1.35, calories: 60 },
                        ],
                        removedModifiers: [
                            {
                                name: 'Southwestern Ranch',
                                productId: 'IDPModifier-131133',
                                defaultQuantity: 1,
                                minQuantity: 0,
                                maxQuantity: 1,
                            },
                        ],
                    },
                ]}
                removedModifiers={[]}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render ListModifiers with unavailable modifiers', () => {
        const { container } = render(
            <ListModifiers
                addedModifiers={[
                    {
                        name: 'Tots',
                        quantity: 1,
                        productId: 'IDPModifier-30749',
                        price: 1,
                        calories: 620,
                        metadata: { MODIFIER_GROUP_ID: '16051', MODIFIER_GROUP_TYPE: 'Selections' },
                        modifiers: [],
                    },
                ]}
                removedModifiers={[]}
                unavailableModifiers={['IDPModifier-30749']}
                unavailableSubModifiers={[]}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render ListModifiers with unavailable submodifiers', () => {
        const { container } = render(
            <ListModifiers
                addedModifiers={[
                    {
                        name: 'Tots',
                        quantity: 1,
                        productId: 'IDPModifier-30749',
                        price: 1,
                        calories: 620,
                        metadata: { MODIFIER_GROUP_ID: '16051', MODIFIER_GROUP_TYPE: 'Selections' },
                        modifiers: [
                            { name: 'Chili', quantity: 1, productId: 'IDPModifier-13117', price: 0.75, calories: 110 },
                            { name: 'Bacon', quantity: 1, productId: 'IDPModifier-13118', price: 1.35, calories: 60 },
                        ],
                    },
                ]}
                removedModifiers={[]}
                unavailableModifiers={[]}
                unavailableSubModifiers={['IDPModifier-13117']}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render ListModifiers with unavailable modifier with unavailable submodifier', () => {
        const { container } = render(
            <ListModifiers
                addedModifiers={[
                    {
                        name: 'Tots',
                        quantity: 1,
                        productId: 'IDPModifier-30749',
                        price: 1,
                        calories: 620,
                        metadata: { MODIFIER_GROUP_ID: '16051', MODIFIER_GROUP_TYPE: 'Selections' },
                        modifiers: [
                            { name: 'Chili', quantity: 1, productId: 'IDPModifier-13117', price: 0.75, calories: 110 },
                            { name: 'Bacon', quantity: 1, productId: 'IDPModifier-13118', price: 1.35, calories: 60 },
                        ],
                    },
                ]}
                removedModifiers={[]}
                unavailableModifiers={['IDPModifier-30749']}
                unavailableSubModifiers={['IDPModifier-13117']}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render ListModifiers with unavailable default modifier', () => {
        const { container } = render(
            <ListModifiers
                addedModifiers={[]}
                removedModifiers={[
                    {
                        name: 'Red Ranch',
                        defaultQuantity: 1,
                        minQuantity: 0,
                        maxQuantity: 0,
                        productId: 'ARBYS-WEBOA-640212570',
                    },
                ]}
                defaultModifiers={[
                    {
                        name: 'Tots',
                        productId: 'IDPModifier-30749',
                        defaultQuantity: 1,
                        minQuantity: 1,
                        maxQuantity: 1,
                    },
                ]}
                unavailableModifiers={['IDPModifier-30749']}
                unavailableSubModifiers={[]}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render ListModifiers with unavailable default submodifier', () => {
        const { container } = render(
            <ListModifiers
                addedModifiers={[]}
                removedModifiers={[]}
                defaultSubModifiersData={[
                    {
                        parrentProduct: {
                            name: 'Tots',
                            productId: 'IDPModifier-30749',
                            quantity: 1,
                            price: 1,
                            calories: 620,
                            modifiers: [
                                {
                                    name: 'Chili',
                                    quantity: 1,
                                    productId: 'IDPModifier-13117',
                                    price: 0.75,
                                    calories: 110,
                                },
                                {
                                    name: 'Bacon',
                                    quantity: 1,
                                    productId: 'IDPModifier-13118',
                                    price: 1.35,
                                    calories: 60,
                                },
                            ],
                        },
                        defaultSubModifiers: [
                            {
                                name: 'Chili',
                                productId: 'IDPModifier-13117',
                                defaultQuantity: 1,
                                minQuantity: 1,
                                maxQuantity: 1,
                            },
                        ],
                    },
                ]}
                unavailableModifiers={[]}
                unavailableSubModifiers={['IDPModifier-13117']}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render ListModifiers correct without price', () => {
        const wrapper = shallow(
            <ListModifiers addedModifiers={addedModifiersMock} removedModifiers={removedModifiersMock} hidePrice />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
