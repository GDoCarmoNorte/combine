import React from 'react';
import { screen, render } from '@testing-library/react';
import UserDeals from '../../../../components/atoms/userDeals';
import globalPropsMock from '../../../mocks/globalProps.mock';
import { isAccountDealsPageOn } from '../../../../lib/getFeatureFlags';

const mockDispatch = jest.fn();

jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch,
}));

jest.mock('../../../../redux/store', () => ({
    useAppSelector: () => ({ dealID: null, dealType: null } as any),
    useAppDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/selectors/bag', () => ({
    selectBag: (state) => state.bag,
    generateNextLineItemId: () => 1,
}));

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalPropsMock),
    useBag: () => ({
        actions: {
            addToBag: () => ({}),
        },
    }),
    useOrderLocation: () => ({ currentLocation: { storeId: '99984' } }),
    useRewards: () => ({
        offers: [
            {
                id: 'eaf972b7-f04f-44db-af1b-a67ace13f77a',
                userOfferId: 'eaf972b7-f04f-44db-af1b-a67ace13f77b',
                name: 'Free Cookie',
                type: 'BUY_X_GET_Y_SET_PRICE',
                startDateTime: '2021-06-08T08:04:58.329Z',
                endDateTime: '2021-06-22T08:04:58.329Z',
                terms:
                    "Offer not valid with slider purchases. Offer is not transferrable. Offer cannot be combined with any other coupon or offer. Limit one coupon per person, per order, per visit. No cash value. Offer valid at participating U.S. locations only. Offer and participation in Hawaii and Alaska may vary. TM & © 2019 Arby's IP Holder, LLC. All Rights Reserved. ",
                isRedeemableInStoreOnly: false,
                isRedeemableOnlineOnly: false,
                status: 'OPEN',
                termsLanguageCode: 'en',
                description: 'with Purchase of Any Sandwich',
                applicability: {},
            },
            {
                id: 'eaf972b7-f04f-44db-af1b-a67ace13f771',
                userOfferId: 'eaf972b7-f04f-44db-af1b-a67ace13f771',
                name: 'Free Cookie 2',
                type: 'BUY_X_GET_Y_SET_PRICE',
                startDateTime: '2021-06-08T08:04:58.329Z',
                endDateTime: '2021-06-22T08:04:58.329Z',
                terms: 'term',
                isRedeemableInStoreOnly: true,
                isRedeemableOnlineOnly: false,
                status: 'OPEN',
                termsLanguageCode: 'en',
                description: 'description',
                applicability: {},
            },
        ],
        rewardsCatalogLoading: false,
    }),
}));

jest.mock('../../../../redux/hooks/useRewards', () => {
    return {
        __esModule: true,
        default: jest.fn().mockReturnValue({
            offers: [
                {
                    id: 'eaf972b7-f04f-44db-af1b-a67ace13f77a',
                    userOfferId: 'eaf972b7-f04f-44db-af1b-a67ace13f77b',
                    name: 'Free Cookie',
                    type: 'BUY_X_GET_Y_SET_PRICE',
                    startDateTime: '2021-06-08T08:04:58.329Z',
                    endDateTime: '2021-06-22T08:04:58.329Z',
                    terms:
                        "Offer not valid with slider purchases. Offer is not transferrable. Offer cannot be combined with any other coupon or offer. Limit one coupon per person, per order, per visit. No cash value. Offer valid at participating U.S. locations only. Offer and participation in Hawaii and Alaska may vary. TM & © 2019 Arby's IP Holder, LLC. All Rights Reserved. ",
                    isRedeemableInStoreOnly: false,
                    isRedeemableOnlineOnly: false,
                    status: 'OPEN',
                    termsLanguageCode: 'en',
                    description: 'with Purchase of Any Sandwich',
                    applicability: {},
                },
                {
                    id: 'eaf972b7-f04f-44db-af1b-a67ace13f771',
                    userOfferId: 'eaf972b7-f04f-44db-af1b-a67ace13f771',
                    name: 'Free Cookie 2',
                    type: 'BUY_X_GET_Y_SET_PRICE',
                    startDateTime: '2021-06-08T08:04:58.329Z',
                    endDateTime: '2021-06-22T08:04:58.329Z',
                    terms: 'term',
                    isRedeemableInStoreOnly: true,
                    isRedeemableOnlineOnly: false,
                    status: 'OPEN',
                    termsLanguageCode: 'en',
                    description: 'description',
                    applicability: {},
                },
            ],
            loading: false,
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        }),
        isOfferModel: jest.fn().mockReturnValue(() => true),
    };
});

jest.mock('../../../../lib/getFeatureFlags');

describe('Redeem your deals', () => {
    beforeEach(() => {
        (isAccountDealsPageOn as jest.Mock).mockImplementation(() => true);
    });

    it('should match  snapshot', () => {
        const { container } = render(<UserDeals />);
        expect(container).toMatchSnapshot();
    });

    it('should contain redeem button', () => {
        render(<UserDeals />);
        expect(screen.getAllByRole('link', { name: /Redeem/i }).length).toEqual(1);
    });

    it('should not render if no "accountDealsPage" feature', () => {
        (isAccountDealsPageOn as jest.Mock).mockImplementation(() => false);
        render(<UserDeals />);
        const header = screen.queryByRole('heading', { name: 'redeem your deals' });
        expect(header).toBeNull();
    });
});
