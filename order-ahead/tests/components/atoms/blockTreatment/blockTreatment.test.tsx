import React from 'react';
import { screen, render, cleanup } from '@testing-library/react';

import BlockTreatment, { BlockTreatmentAlt } from '../../../../components/atoms/blockTreatment';

describe('blockTreatment', () => {
    afterEach(cleanup);

    test('should render the BlockTreatment', () => {
        const { container } = render(
            <BlockTreatment title="Test Block Treatment Title" subtitle="Test Block Treatment SubTitle" size="sm" />
        );

        // eslint-disable-next-line testing-library/no-node-access
        expect(container.firstChild).toHaveClass('m-block-treatment-sm');
        expect(screen.getByText('Test Block Treatment Title')).toHaveClass('m-block-treatment-title');
        expect(screen.getByText('Test Block Treatment SubTitle')).toHaveClass('m-block-treatment-subtitle');
    });

    test('should render the BlockTreatmentAlt', () => {
        render(<BlockTreatmentAlt title="Test Block Treatment Title" subtitle="Test Block Treatment SubTitle" />);

        expect(screen.getByText('T')).toHaveClass('m-block-treatment-alt-first-letter');
        expect(screen.getByText('est Block Treatment Title')).toHaveClass('m-block-treatment-alt-title');
        expect(screen.getByText('Test Block Treatment SubTitle')).toHaveClass('m-block-treatment-alt-subtitle');
    });
});
