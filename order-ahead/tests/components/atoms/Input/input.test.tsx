import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import Input from '../../../../components/atoms/Input/input';

describe('input component', () => {
    it('should match snapshot with default props', () => {
        const wrapper = shallow(<Input />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with type search', () => {
        const wrapper = shallow(<Input type="search" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with type search and aria label', () => {
        const wrapper = shallow(
            <Input type="search" searchIconAriaLabel="Search" searchInputAriaLabel="Search Input" />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component with left icon', () => {
        const wrapper = shallow(
            <Input
                type="search"
                searchIconAriaLabel="Search"
                searchInputAriaLabel="Search Input"
                leftIcon={<i>Some icon</i>}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should be disabled when have attribute disabled true', () => {
        render(
            <Input type="search" searchIconAriaLabel="Search" searchInputAriaLabel="Search Input" disabled={true} />
        );
        const searchField = screen.getByLabelText('Search Input', { selector: 'input' });
        expect(searchField).toBeDisabled();
    });
});
