import React from 'react';
import { shallow } from 'enzyme';

import BrandLoader from '../../../../components/atoms/BrandLoader';

describe('BrandLoader', () => {
    it('should render BrandLoader without props', () => {
        const wrapper = shallow(<BrandLoader />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render BrandLoader with className', () => {
        const wrapper = shallow(<BrandLoader className="testBrandLoader" />);

        expect(wrapper).toMatchSnapshot();
    });
});
