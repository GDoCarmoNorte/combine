import React from 'react';
import { shallow } from 'enzyme';

import InspireChip from '../../../../components/atoms/Chip';

describe('chip component', () => {
    it('should match snapshot with default props', () => {
        const wrapper = shallow(<InspireChip label="test label" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with className', () => {
        const wrapper = shallow(<InspireChip label="test label" className="testClass" />);

        expect(wrapper).toMatchSnapshot();
    });
});
