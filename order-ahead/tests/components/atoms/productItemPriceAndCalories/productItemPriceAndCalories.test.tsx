import React from 'react';
import { render, screen } from '@testing-library/react';
import {
    ProductItemCalories,
    ProductItemPrice,
    ProductItemPriceAndCalories,
} from '../../../../components/atoms/productItemPriceAndCalories';
import { isCaloriesDisplayEnabled } from '../../../../lib/getFeatureFlags';

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
    };
});

describe('productItemPriceAndCalories', () => {
    describe('ProductItemCalories', () => {
        it('should render loader', () => {
            render(<ProductItemCalories calories={200} isLoading />);

            screen.getByRole('progressbar');
        });

        it('should render calories', () => {
            render(<ProductItemCalories calories={200} />);

            screen.getByText('200 calories');
        });

        it('should render message when calories is not provided', () => {
            render(<ProductItemCalories calories={null} />);

            screen.getByText('Calorie info unavailable');
        });
    });

    describe('ProductItemPrice', () => {
        it('should render loader', () => {
            render(<ProductItemPrice price={20} isLoading />);

            screen.getByRole('progressbar');
        });

        it('should render price', () => {
            render(<ProductItemPrice price={20} />);

            screen.getByText('$20.00');
        });
    });

    describe('ProductItemPriceAndCalories', () => {
        it('should render loader', () => {
            render(<ProductItemPriceAndCalories calories={200} price={10} isLoading />);

            expect(screen.getAllByRole('progressbar')).toHaveLength(2);
        });

        it('should render price and calories', () => {
            render(<ProductItemPriceAndCalories calories={200} price={10} />);

            screen.getByText(/\$10/);
            screen.getByText(/200 calories/);
        });

        it('should not render price if price is not provided', () => {
            render(<ProductItemPriceAndCalories calories={200} price={null} />);

            expect(screen.queryByText(/\$0.00/)).not.toBeInTheDocument();
            screen.getByText(/200 calories/);
        });

        it('should not render calories for Canada', () => {
            (isCaloriesDisplayEnabled as jest.Mock).mockReturnValueOnce(false).mockReturnValueOnce(false);

            render(<ProductItemPriceAndCalories calories={200} price={10} />);

            screen.getByText(/\$10/);
            expect(screen.queryByText(/200 calories/)).not.toBeInTheDocument();
        });
    });
});
