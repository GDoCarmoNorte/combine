import React from 'react';
import { render, screen } from '@testing-library/react';

import RewardDetailsModal from '../../../../components/molecules/rewardDetailsModal';
import { OfferView } from '../../../../components/molecules/rewardDetailsModal/offerView/offerView';

jest.mock('../../../../redux/hooks/useGlobalProps', () => () => ({}));

describe('RewardsDetailsModal', () => {
    const renderComponent = ({
        open = true,
        modalTitle = 'Offer Details',
        isActiveOffer = false,
        isLoading = false,
        onModalClose = jest.fn(),
    }) => (
        <RewardDetailsModal open={open} modalTitle={modalTitle} isLoading={isLoading} onClose={onModalClose}>
            <OfferView
                title="Loyalty Incentive"
                dateLabel="LIMITED TIME OFFER"
                dateText="Valid between 08/23-12/31/2099"
                imageUrl=""
                terms="Chips & Queso, Chips & Salsa"
                isActive={isActiveOffer}
            />
        </RewardDetailsModal>
    );
    it('should match snapshot', () => {
        const view = render(renderComponent({}));
        expect(view).toMatchSnapshot();
    });

    it('match snapshot during loading', () => {
        const view = render(renderComponent({ isLoading: true }));
        expect(view).toMatchSnapshot();
    });

    it('should render with details', () => {
        render(renderComponent({}));
        screen.getByText(/Offer Details/i);
        screen.getByText(/Loyalty Incentive/i);
        screen.getByText(/LIMITED TIME OFFER/i);
        screen.getByText(/Valid between 08\/23-12\/31\/2099/i);
        screen.getByText(/Chips & Queso, Chips & Salsa/i);
    });

    it('should render with active label', () => {
        render(renderComponent({ isActiveOffer: true }));
        screen.getByText(/Active/i);
    });

    it('should render with loading', () => {
        render(renderComponent({ isLoading: true }));
        screen.getByRole('progressbar');
    });
});
