import React from 'react';

import { render } from '@testing-library/react';

import NotFound from '../../../components/molecules/notFound';
import iconMock from './icon.mock';

describe('NotFound', () => {
    it('should render component', () => {
        const { container } = render(<NotFound />);

        expect(container).toMatchSnapshot();
    });

    it('should render component with heading and body', () => {
        const { container } = render(<NotFound body="test" heading="test" />);

        expect(container).toMatchSnapshot();
    });

    it('should render component with heading, body and icon from contentful', () => {
        const { container } = render(<NotFound body="test" heading="test" icon={iconMock as any} />);

        expect(container).toMatchSnapshot();
    });

    it('should render component with heading, body and icon as jsx element', () => {
        const { container } = render(<NotFound body="test" heading="test" icon={<img alt="custom icon" />} />);

        expect(container).toMatchSnapshot();
    });

    it('should render component with configurable heading tag', () => {
        const { container } = render(<NotFound heading="test" headingTag="h1" />);

        expect(container).toMatchSnapshot();
    });
});
