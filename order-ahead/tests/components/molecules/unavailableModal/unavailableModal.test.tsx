import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { UnavailableModal } from '../../../../components/molecules/unavailableModal/unavailableModal';

describe('unavailable modal', () => {
    const getComponent = ({
        open = true,
        onClose = jest.fn(),
        title = 'modal title',
        description = 'modal description',
        footerText = 'modal footer text',
        renderButtons = jest.fn(),
        renderLoader = jest.fn(),
    }) => (
        <UnavailableModal
            open={open}
            onClose={onClose}
            title={title}
            description={description}
            footerText={footerText}
            renderButtons={renderButtons}
            renderLoader={renderLoader}
        />
    );
    it('should render correctly', () => {
        render(getComponent({}));

        expect(screen.getByText(/modal title/i)).toBeInTheDocument();
        expect(screen.getByText(/modal description/i)).toBeInTheDocument();
        expect(screen.getByText(/modal footer text/i)).toBeInTheDocument();
    });

    it('should call onClose as click on the cross icon', () => {
        const onCloseMock = jest.fn();
        render(getComponent({ onClose: onCloseMock }));
        const crossBtn = screen.getByRole('button', { name: /Close Icon/i });
        userEvent.click(crossBtn);
        expect(onCloseMock).toHaveBeenCalled();
    });
});
