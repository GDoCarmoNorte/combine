import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import NotificationModal from '../../../../components/molecules/notificationModal';

jest.mock('../../../../redux/hooks/useGlobalProps', () => () => ({}));

describe('NotificationModal', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(
            <NotificationModal
                open
                title="Cancel"
                message="your changes will be lost"
                rejectButtonProps={{ text: 'No' }}
                confirmButtonProps={{ text: 'Yes' }}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with children', () => {
        render(
            <NotificationModal
                open
                title="Cancel"
                message="your changes will be lost"
                rejectButtonProps={{ text: 'No' }}
                confirmButtonProps={{ text: 'Yes' }}
            >
                children
            </NotificationModal>
        );

        screen.getByText(/children/i);
    });
});
