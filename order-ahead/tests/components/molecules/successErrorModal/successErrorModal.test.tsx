import React from 'react';
import SuccessErrorModal from '../../../../components/molecules/successErrorModal';
import { render, screen } from '@testing-library/react';
import { TErrorCodeModel } from '../../../../@generated/webExpApi';
import { getTextWithoutHtmlTags } from '../../../utils';
import { Icons } from '../../../../components/molecules/successErrorModal/successErrorModal';

describe('SuccessErrorModal', () => {
    it('should render Success Modal with defaults', () => {
        render(<SuccessErrorModal open={true} isSuccess={true} description={'Success description'} />);
        expect(screen.getByText('Success description')).toBeInTheDocument();
        expect(screen.getByText('SUCCESS!')).toBeInTheDocument();
    });

    it('should render Error modal with defaults', () => {
        render(
            <SuccessErrorModal
                open={true}
                isSuccess={false}
                description={'Error occurred.'}
                errorCode={TErrorCodeModel.Generic}
            />
        );
        expect(
            getTextWithoutHtmlTags(
                'Error occurred. Please contact Customer Service support@inspirebrands.com or contact us.'
            )
        ).toBeInTheDocument();
        expect(screen.getByText('UH OH')).toBeInTheDocument();
    });

    it('should render title passed as parameter for Success modal', () => {
        render(
            <SuccessErrorModal
                open={true}
                isSuccess={true}
                description={'Success description'}
                title={'Success Title'}
            />
        );
        expect(screen.getByText('Success Title')).toBeInTheDocument();
        expect(screen.queryByText('SUCCESS!')).not.toBeInTheDocument();
    });

    it('should render title passed as parameter for Error modal', () => {
        render(
            <SuccessErrorModal open={true} isSuccess={false} description={'Error description'} title={'Error Title'} />
        );
        expect(screen.getByText('Error Title')).toBeInTheDocument();
        expect(screen.queryByText('UH OH')).not.toBeInTheDocument();
    });

    it('should change icon based on parameter', () => {
        render(
            <SuccessErrorModal open={true} isSuccess={false} description={'Error description'} icon={Icons.WARNING} />
        );

        expect(screen.getByRole('img', { name: 'warning icon' })).toBeInTheDocument();
    });

    it('should show error default description', () => {
        render(<SuccessErrorModal open={true} isSuccess={false} description={null} />);
        expect(
            getTextWithoutHtmlTags(
                'Something went wrong. Please contact Customer Service support@inspirebrands.com or try again later.'
            )
        ).toBeInTheDocument();
    });
});
