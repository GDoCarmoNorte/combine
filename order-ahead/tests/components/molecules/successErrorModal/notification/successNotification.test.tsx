import React from 'react';
import { render, screen } from '@testing-library/react';

import SuccessNotification from '../../../../../components/molecules/successErrorModal/notification/SuccessNotification';

describe('SuccessNotification', () => {
    it('should be visible successTitle and successDescription', () => {
        const propsMock = {
            title: 'Account was verified!',
            description: 'You are able to redeem certificates for getting free products',
        };
        render(<SuccessNotification {...propsMock} />);

        expect(screen.getByText(/Account was verified!/i)).toBeInTheDocument();
        expect(screen.getByText(/You are able to redeem certificates for getting free products/i)).toBeInTheDocument();
    });
});
