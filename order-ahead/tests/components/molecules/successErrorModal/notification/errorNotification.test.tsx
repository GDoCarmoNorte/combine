import React from 'react';
import { render, screen } from '@testing-library/react';

import ErrorNotification from '../../../../../components/molecules/successErrorModal/notification/ErrorNotification';

describe('ErrorNotification', () => {
    it('should be visible errorTitle and errorDescription', () => {
        const propsMock = {
            title: 'Account was not verified!',
            description: 'Please try again later',
        };
        render(<ErrorNotification {...propsMock} />);

        expect(screen.getByText(/Account was not verified!/i)).toBeInTheDocument();
        expect(screen.getByText(/Please try again later/i)).toBeInTheDocument();
    });

    it('should be visible default error message', () => {
        render(<ErrorNotification description="Something went wrong!" />);

        expect(screen.getByText(/UH OH/i)).toBeInTheDocument();
        expect(screen.getByText(/Something went wrong!/i)).toBeInTheDocument();
    });
});
