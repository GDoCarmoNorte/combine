import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import NotificationSettings from '../../../components/molecules/notificationSettings/notificationSettings';

const marketingMock = [{ type: 'EMAIL' }];
const onChangeMock = jest.fn();

describe('Notification Settings', () => {
    it('should match  snapshot', () => {
        const { container } = render(<NotificationSettings marketing={marketingMock} onChange={onChangeMock} />);
        expect(container).toMatchSnapshot();
    });

    it('should call onChange with  empty array  when marketing is empty and we clicking checkbox', () => {
        render(<NotificationSettings marketing={[]} onChange={onChangeMock} />);
        const checkbox = screen.getByRole('checkbox');
        userEvent.click(checkbox);
        expect(onChangeMock).toBeCalledWith(marketingMock);
    });

    it('should have selected checkbox when  marketing property  contains object  with type equal  to  Email', () => {
        render(<NotificationSettings marketing={marketingMock} onChange={onChangeMock} />);
        const checkbox = screen.getByRole('checkbox');
        expect(checkbox).toHaveClass('checkboxSelected');
    });

    it('should have not selected checkbox when marketing array is empty', () => {
        render(<NotificationSettings marketing={[]} onChange={onChangeMock} />);
        const checkbox = screen.getByRole('checkbox');
        expect(checkbox).not.toHaveClass('checkboxSelected');
    });
});
