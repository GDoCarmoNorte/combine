export default {
    fields: {
        title: 'icon',
        file: {
            url: 'test_url',
            fileName: 'name',
            contentType: 'image/png',
        },
    },
};
