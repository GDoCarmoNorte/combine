export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '48n1LDhHlnaQyMhIHiYYc4',
        type: 'Entry',
        createdAt: '2020-07-18T12:58:31.711Z',
        updatedAt: '2020-08-05T08:35:17.212Z',
        environment: {
            sys: {
                id: 'feature_DBBP-804',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 3,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'menuCategory',
            },
        },
        locale: 'en-US',
    },
    fields: {
        categoryName: 'Market Fresh',
        nameInUrl: 'market-fresh',
        subcategories: [
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: '7agZmu9b2EjQraZkoFvxY0',
                    type: 'Entry',
                    createdAt: '2020-07-18T13:06:21.647Z',
                    updatedAt: '2020-07-18T13:57:26.479Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-804',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 2,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'menuItemSection',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    name: 'Market Fresh',
                    menuItems: [
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '3VLMbA64Uu8UlKb2xp1Bzy',
                                type: 'Entry',
                                createdAt: '2020-07-18T13:06:17.664Z',
                                updatedAt: '2020-07-18T13:07:26.158Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-804',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 2,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'product',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Roast Turkey & Swiss Sandwich',
                                nameInUrl: 'roast-turkey-swiss',
                                image: {
                                    sys: {
                                        space: {
                                            sys: {
                                                type: 'Link',
                                                linkType: 'Space',
                                                id: 'o19mhvm9a2cm',
                                            },
                                        },
                                        id: '5ei4mkeL1Fk7JzXm0xpfI5',
                                        type: 'Asset',
                                        createdAt: '2020-07-18T13:06:12.636Z',
                                        updatedAt: '2020-07-18T13:06:12.636Z',
                                        environment: {
                                            sys: {
                                                id: 'feature_DBBP-804',
                                                type: 'Link',
                                                linkType: 'Environment',
                                            },
                                        },
                                        revision: 1,
                                        locale: 'en-US',
                                    },
                                    fields: {
                                        title: 'ROAST TURKEY & SWISS SANDWICH',
                                        description: 'ROAST TURKEY & SWISS SANDWICH',
                                        file: {
                                            url:
                                                '//images.ctfassets.net/o19mhvm9a2cm/5ei4mkeL1Fk7JzXm0xpfI5/80c116915c79bc622b64eec2057d835e/RoastTurkeySwiss_18_tile_Desktop_1024x557.jpg',
                                            details: {
                                                size: 163244,
                                                image: {
                                                    width: 1024,
                                                    height: 557,
                                                },
                                            },
                                            fileName: 'RoastTurkeySwiss_18_tile_Desktop_1024x557.jpg',
                                            contentType: 'image/jpeg',
                                        },
                                    },
                                },
                                productId: '674387927',
                            },
                        },
                    ],
                    showSubcategoryHeader: false,
                },
            },
        ],
        image: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '5FkRIsdaImR3yk3D3DQ6yj',
                type: 'Asset',
                createdAt: '2020-08-05T08:35:05.442Z',
                updatedAt: '2020-08-05T08:35:05.442Z',
                environment: {
                    sys: {
                        id: 'feature_DBBP-804',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'Market Fresh',
                description: 'Market Fresh',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/5FkRIsdaImR3yk3D3DQ6yj/f02ca352c2a2adcafc8f3b710fc1d277/image_9.png',
                    details: {
                        size: 38625,
                        image: {
                            width: 187,
                            height: 140,
                        },
                    },
                    fileName: 'image 9.png',
                    contentType: 'image/png',
                },
            },
        },
    },
};
