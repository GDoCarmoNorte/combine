import React from 'react';
import { shallow } from 'enzyme';

import ErrorBanner from '../../../../components/sections/errorBanner/errorBanner';
import entryMock from './errorBanner.mock';

describe('errorBanner', () => {
    it('should render the error banner properly', () => {
        const wrapper = shallow(<ErrorBanner entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render null if no fields were procided', () => {
        const wrapper = shallow(<ErrorBanner entry={{ fields: undefined } as any} />);

        expect(wrapper).toMatchSnapshot();
    });
});
