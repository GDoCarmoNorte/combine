export default {
    fields: {
        errorCodeText: 'error Code Text',
        headerText: 'header Text',
        mainText: 'error has been occured',
        mainImage:
            'https://images.ctfassets.net/o19mhvm9a2cm/7I2Uvnt0k62bDHTwSWw5y/83a459cf4159e4a22eaa7037a3454cc3/Website_LTO_DFT_Sandwich-1-.png?fm=webp&w=650',
        homePageText: 'Go to Home Page',
    },
};
