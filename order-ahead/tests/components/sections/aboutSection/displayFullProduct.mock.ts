// import { ProductDetailDtoSizeEnum } from '../../../../@generated/domainMenu/models/ProductDetailDto';
// import { IDisplayFullProduct } from '../../../../redux/domainMenu';

// TODO MENU2.0 Fix tests
const product /*: IDisplayFullProduct */ = {
    displayName: 'Roast Beef Gyro Meal',
    productId: 'ARBYS-WEBOA-640239870',
    mainProductId: 'ARBYS-WEBOA-640224250',
    mainProductSectionName: 'Sandwich',
    productSections: [
        {
            productSectionDisplayName: 'Sandwich',
            productSectionType: 'main',
            displayProducts: [
                {
                    displayName: 'Roast Beef Gyro',
                    visibleProductSize: '',
                    displayProductDetails: [
                        {
                            productId: 'ARBYS-WEBOA-640224250',
                            calories: 550,
                            attributes: {
                                size: {
                                    displayValue: '',
                                    displayName: 'Small',
                                },
                            },
                            price: null,
                            modifierGroups: [
                                {
                                    modifierGroupId: undefined,
                                    displayName: 'Meat',
                                    minQuantity: 0,
                                    maxQuantity: 20,
                                    position: 1,
                                    modifiers: [],
                                },
                            ],
                            quantity: 1,
                        },
                    ],
                },
            ],
            displayProductsBySubheader: [],
        },
    ],
    whatsIncluded: [],
    price: 9.27,
    calories: 1128.5,
};
export default product;
