export default [
    {
        name: 'Roast Chicken',
        defaultQuantity: 1,
        productId: 'id-1',
    },
    {
        name: 'Tomatoes',
        defaultQuantity: 1,
        productId: 'id-2',
    },
    {
        name: 'Star Cut Bun',
        defaultQuantity: 1,
        productId: 'id-3',
    },
    {
        name: 'Pepper Bacon',
        defaultQuantity: 1,
        productId: 'id-4',
    },
    {
        name: 'Shredded Lettucev',
        defaultQuantity: 1,
        productId: 'id-5',
    },
    {
        name: 'Swiss Cheese',
        defaultQuantity: 1,
        productId: 'id-5',
    },
    {
        name: 'Honey Mustard Sauce',
        defaultQuantity: 1,
        productId: 'id-5',
    },
];
