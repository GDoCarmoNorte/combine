const nutritionData = [
    {
        displayName: 'Sandwich',
        sizeSelections: [
            {
                displayName: 'Sandwich',
                name: 'Small',
                size: 'SM',
                isSelected: true,
                productId: 'ARBYS-WEBOA-640223380',
                enhancedAttributes: {
                    calories: '410',
                    nutritionalFacts: [
                        {
                            name: 'Serving Weight (g)',
                            value: '128',
                        },
                        {
                            name: 'Calories',
                            value: '410',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '200',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '22',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '3',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '0',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '0',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '940',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '49',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '5',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '0',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '5',
                        },
                    ],
                    allergicInformation: 'CONTAINS:\nWHEAT.',
                },
            },
            {
                displayName: 'Sandwich',
                name: 'Medium',
                size: 'MD',
                isSelected: false,
                productId: 'ARBYS-WEBOA-640223381',
                enhancedAttributes: {
                    calories: '550',
                    nutritionalFacts: [
                        {
                            name: 'Serving Weight (g)',
                            value: '170',
                        },
                        {
                            name: 'Calories',
                            value: '550',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '260',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '29',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '4',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '0',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '0',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1250',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '65',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '6',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '0',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '6',
                        },
                    ],
                    allergicInformation: 'CONTAINS:\nWHEAT.',
                },
            },
            {
                displayName: 'Sandwich',
                name: 'Large',
                size: 'LG',
                isSelected: false,
                productId: 'ARBYS-WEBOA-640223382',
                enhancedAttributes: {
                    calories: '650',
                    nutritionalFacts: [
                        {
                            name: 'Serving Weight (g)',
                            value: '201',
                        },
                        {
                            name: 'Calories',
                            value: '650',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '310',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '35',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '5',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '0',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '0',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1480',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '77',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '7',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '0',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '8',
                        },
                    ],
                    allergicInformation: 'CONTAINS:\nWHEAT.',
                },
            },
        ],
    },
    {
        displayName: 'Salad',
        sizeSelections: [
            {
                displayName: 'Salad',
                name: 'Small',
                size: 'SM',
                isSelected: true,
                productId: 'ARBYS-WEBOA-640223380',
                enhancedAttributes: {
                    calories: '33',
                    nutritionalFacts: [
                        {
                            name: 'Serving Weight (g)',
                            value: '142',
                        },
                        {
                            name: 'Calories',
                            value: '33',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '200',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '22',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '3',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '0',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '0',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '940',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '49',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '5',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '0',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '5',
                        },
                    ],
                    allergicInformation: 'CONTAINS:\nWHEAT.',
                },
            },
            {
                displayName: 'Salad',
                name: 'Medium',
                size: 'MD',
                isSelected: false,
                productId: 'ARBYS-WEBOA-640223381',
                enhancedAttributes: {
                    calories: '22',
                    nutritionalFacts: [
                        {
                            name: 'Serving Weight (g)',
                            value: '165',
                        },
                        {
                            name: 'Calories',
                            value: '22',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '260',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '29',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '4',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '0',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '0',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1250',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '65',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '6',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '0',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '6',
                        },
                    ],
                    allergicInformation: 'CONTAINS:\nWHEAT.',
                },
            },
            {
                name: 'Large',
                displayName: 'Salad',
                size: 'LG',
                isSelected: false,
                productId: 'ARBYS-WEBOA-640223382',
                enhancedAttributes: {
                    calories: '650',
                    nutritionalFacts: [
                        {
                            name: 'Serving Weight (g)',
                            value: '201',
                        },
                        {
                            name: 'Calories',
                            value: '650',
                        },
                        {
                            name: 'Calories from Fat',
                            value: '310',
                        },
                        {
                            name: 'Fat - Total (g)',
                            value: '35',
                        },
                        {
                            name: 'Saturated Fat (g)',
                            value: '5',
                        },
                        {
                            name: 'Trans Fat (g)',
                            value: '0',
                        },
                        {
                            name: 'Cholesterol (mg)',
                            value: '0',
                        },
                        {
                            name: 'Sodium (mg)',
                            value: '1480',
                        },
                        {
                            name: 'Total Carbohydrates (g)',
                            value: '77',
                        },
                        {
                            name: 'Dietary Fiber (g)',
                            value: '7',
                        },
                        {
                            name: 'Sugars (g)',
                            value: '0',
                        },
                        {
                            name: 'Proteins (g)',
                            value: '8',
                        },
                    ],
                    allergicInformation: 'CONTAINS:\nWHEAT.',
                },
            },
        ],
    },
];

export default nutritionData;
