export default {
    rootProduct: {
        productId: 'ARBYS-WEBOA-640216318',
        brand: 'ARBYS',
        sellingChannel: 'WEBOA',
        description:
            'Take a bite of Arby’s most savory sandwich: slow-roasted chicken, crispy pepper bacon, Swiss cheese, and honey mustard. Then take another. You’re getting the hang of it.',
        docType: 'PRODUCT_DETAIL',
        referenceId: '640216318',
        plu: '1140',
        displayName: 'Buffalo Chicken Slider',
        productType: 'PRODUCT',
        productKind: 'Sandwich',
        size: 'RG',
        defaultPrice: 0,
        isCombo: false,
        isAvailable: true,
        modifierGroups: [
            {
                modifierGroupId: 'ARBYS-WEBOA-640214237_029',
                name: 'Meat - Buffalo Chicken Slider',
                displayName: 'Meat',
                minQuantity: 0,
                maxQuantity: 20,
                position: 1,
                modifierItems: [
                    {
                        modifierProductId: 'ARBYS-WEBOA-640213150',
                        position: 2,
                        minQuantity: 1,
                        maxQuantity: 1,
                        defaultQuantity: 1,
                    },
                ],
            },
            {
                modifierGroupId: 'ARBYS-WEBOA-640214192_029',
                name: 'Veggies - Buffalo Chicken Slider',
                displayName: 'Veggies',
                minQuantity: 0,
                maxQuantity: 20,
                position: 3,
            },
            {
                modifierGroupId: 'ARBYS-WEBOA-640214236_029',
                name: 'Bread - Buffalo Chicken Slider',
                displayName: 'Bread',
                minQuantity: 0,
                maxQuantity: 20,
                position: 4,
                modifierItems: [
                    {
                        modifierProductId: 'ARBYS-WEBOA-640212412',
                        position: 4,
                        minQuantity: 1,
                        maxQuantity: 1,
                        defaultQuantity: 1,
                    },
                ],
            },
            {
                modifierGroupId: 'ARBYS-WEBOA-640214194_029',
                name: 'Sauces - Buffalo Chicken Slider',
                displayName: 'Sauces',
                minQuantity: 0,
                maxQuantity: 20,
                position: 5,
                modifierItems: [
                    {
                        modifierProductId: 'ARBYS-WEBOA-640212553',
                        position: 1,
                        minQuantity: 0,
                        maxQuantity: 1,
                        defaultQuantity: 1,
                    },
                    {
                        modifierProductId: 'ARBYS-WEBOA-640216167',
                        position: 3,
                        minQuantity: 1,
                        maxQuantity: 1,
                        defaultQuantity: 1,
                    },
                ],
            },
        ],
        relatedProducts: [],
        upgradeProducts: [],
        happyHourProducts: [
            {
                productId: 'ARBYS-WEBOA-640240514',
                relationType: 'HAPPY_HOUR_PRODUCT',
            },
        ],
        enhancedAttributes: {
            calories: '300',
            nutritionalFacts: [
                {
                    name: 'Serving Weight (g)',
                    value: '105',
                },
                {
                    name: 'Calories',
                    value: '300',
                },
                {
                    name: 'Calories from Fat',
                    value: '120',
                },
                {
                    name: 'Fat - Total (g)',
                    value: '14',
                },
                {
                    name: 'Saturated Fat (g)',
                    value: '2',
                },
                {
                    name: 'Trans Fat (g)',
                    value: '0',
                },
                {
                    name: 'Cholesterol (mg)',
                    value: '25',
                },
                {
                    name: 'Sodium (mg)',
                    value: '940',
                },
                {
                    name: 'Total Carbohydrates (g)',
                    value: '31',
                },
                {
                    name: 'Dietary Fiber (g)',
                    value: '1',
                },
                {
                    name: 'Sugars (g)',
                    value: '2',
                },
                {
                    name: 'Proteins (g)',
                    value: '12',
                },
            ],
            allergicInformation:
                'Contains egg, milk, soy, wheat. Menu item is cooked in the same oil as other items that contains fish (where available). ',
            ingredientStatement:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ',
        },
    },
};
