export default {
    fields: {
        taglineHeading: 'THE ART OF THE MEATCRAFT®',
        taglineDescription:
            'CARVED WHOLE MUSCLE CHICKEN BREAST. HAND TRIMMED, SEARED AND SOUS VIDE, LOW AND SLOW FOR 4 HOURS.',
        icon: {
            fields: {
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/4KQ9fsxvtgcr1gm9hbtmgO/68554df298e6e2100407883c4ed7b56c/Icon_Block.svg?fm=webp&w=1500',
                },
            },
        },
    },
};
