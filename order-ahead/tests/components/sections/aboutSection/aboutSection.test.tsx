import React from 'react';
import { shallow } from 'enzyme';

import AboutSection from '../../../../components/sections/aboutSection/aboutSection';
import entryMock from './aboutSection.mock';
import menu from '../../../redux/selectors/__mocks__/menuMock.json';
import {
    standaloneTalyItemMock,
    comboTallyItemMock,
    tallyItemPromoProduct,
    tallyItemPromoProductDifferentChildren,
} from '../../../redux/selectors/__mocks__/tallyItem.mock';

import { selectAboutSectionInfo } from '../../../../redux/selectors/pdp';
import { useAboutSection } from '../../../../redux/hooks/pdp';

jest.mock('../../../../redux/hooks/pdp');

describe('About section', () => {
    it('should render empty about section with no category info', () => {
        (useAboutSection as jest.Mock).mockReturnValue({
            productKind: 'Name',
            description: 'Description',
            whatsIncluded: [],
            nutritionInfo: {},
        });
        const wrapper = shallow(<AboutSection productNutritionLink={null} category={{} as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should not render whats included section with empty whatsIncluded', () => {
        (useAboutSection as jest.Mock).mockReturnValue({
            productKind: 'Name',
            description: 'Description',
            whatsIncluded: [],
            nutritionInfo: {},
        });
        const wrapper = shallow(<AboutSection productNutritionLink={null} category={entryMock as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render about section for combo', () => {
        (useAboutSection as jest.Mock).mockReturnValue(
            selectAboutSectionInfo({
                domainMenu: { payload: menu },
                pdp: { tallyItem: comboTallyItemMock },
            } as any)
        );
        const wrapper = shallow(<AboutSection productNutritionLink={null} category={entryMock as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render about section for single', () => {
        (useAboutSection as jest.Mock).mockReturnValue(
            selectAboutSectionInfo({
                domainMenu: { payload: menu },
                pdp: { tallyItem: standaloneTalyItemMock },
            } as any)
        );
        const wrapper = shallow(<AboutSection productNutritionLink={null} category={entryMock as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render about section for promo with the same products in sections', () => {
        (useAboutSection as jest.Mock).mockReturnValue(
            selectAboutSectionInfo({
                domainMenu: { payload: menu },
                pdp: { tallyItem: tallyItemPromoProduct },
            } as any)
        );
        const wrapper = shallow(<AboutSection productNutritionLink={null} category={entryMock as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render about section for promo with different products in sections', () => {
        (useAboutSection as jest.Mock).mockReturnValue(
            selectAboutSectionInfo({
                domainMenu: { payload: menu },
                pdp: { tallyItem: tallyItemPromoProductDifferentChildren },
            } as any)
        );
        const wrapper = shallow(<AboutSection productNutritionLink={null} category={entryMock as any} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should render about section with product nutrition link', () => {
        (useAboutSection as jest.Mock).mockReturnValue(
            selectAboutSectionInfo({
                domainMenu: { payload: menu },
                pdp: { tallyItem: tallyItemPromoProductDifferentChildren },
            } as any)
        );
        const productNutritionLink = { link: 'https://example.com' };
        const wrapper = shallow(
            <AboutSection productNutritionLink={productNutritionLink as any} category={entryMock as any} />
        );
        expect(wrapper).toMatchSnapshot();
    });
});
