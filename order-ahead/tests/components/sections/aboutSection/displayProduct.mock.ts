//import { ProductDetailDtoSizeEnum } from '../../../../@generated/domainMenu/models/ProductDetailDto';
//import { IDisplayProduct } from '../../../../redux/domainMenu';

// TODO MENU2.0 Fix tests
const product /*: IDisplayProduct */ = {
    displayName: 'Curly Fries',
    visibleProductSize: /*:ProductDetailDtoSizeEnum.SM*/ '',
    displayProductDetails: [
        {
            productId: 'ARBYS-WEBOA-640223380',
            calories: 410,
            attributes: {
                size: {
                    displayValue: /*:ProductDetailDtoSizeEnum.SM*/ '',
                    displayName: 'Small',
                },
            },
            price: null,
            modifierGroups: [
                {
                    modifierGroupId: '',
                    displayName: 'Sauce Packets',
                    minQuantity: 0,
                    maxQuantity: 20,
                    position: 5,
                    modifiers: [],
                },
                {
                    modifierGroupId: '',
                    displayName: 'Other Condiments',
                    minQuantity: 0,
                    maxQuantity: 20,
                    position: 7,
                    modifiers: [],
                },
            ],
            quantity: 1,
        },
        {
            productId: 'ARBYS-WEBOA-640223381',
            calories: 550,
            attributes: {
                size: {
                    displayValue: /*:ProductDetailDtoSizeEnum.MD*/ '',
                    displayName: 'Medium',
                },
            },
            price: null,
            modifierGroups: [
                {
                    modifierGroupId: '',

                    displayName: 'Sauce Packets',
                    minQuantity: 0,
                    maxQuantity: 20,
                    position: 5,
                    modifiers: [],
                },
                {
                    modifierGroupId: '',
                    displayName: 'Other Condiments',
                    minQuantity: 0,
                    maxQuantity: 20,
                    position: 7,
                    modifiers: [],
                },
            ],
            quantity: 0,
        },
        {
            productId: 'ARBYS-WEBOA-640223382',
            calories: 650,
            attributes: {
                size: {
                    displayValue: /*:ProductDetailDtoSizeEnum.LG*/ '',
                    displayName: 'Large',
                },
            },
            price: null,
            modifierGroups: [
                {
                    modifierGroupId: '',

                    displayName: 'Sauce Packets',
                    minQuantity: 0,
                    maxQuantity: 20,
                    position: 5,
                    modifiers: [],
                },
                {
                    modifierGroupId: '',

                    displayName: 'Other Condiments',
                    minQuantity: 0,
                    maxQuantity: 20,
                    position: 7,
                    modifiers: [],
                },
            ],
            quantity: 0,
        },
    ],
};

export default product;
