import React from 'react';
import { useDispatch } from 'react-redux';
import { shallow } from 'enzyme';
import GiftCardBalance from '../../../../components/sections/giftCardBalance';
import entryMock from './giftCardBalance.mock';

import useCardBalance from '../../../../common/hooks/useCardBalance';
import { Formik } from 'formik';
import { GTM_GIFT_CARD_BALANCE_FORM_SUBMIT_SUCCESS } from '../../../../common/services/gtmService/constants';
import { getCardBalance } from '../../../../common/services/cardBalance';

jest.mock('../../../../common/hooks/useCardBalance');
jest.mock('react-redux');
jest.mock('../../../../common/services/cardBalance', () => ({
    getCardBalance: jest.fn(),
}));

describe('giftCardBalance', () => {
    it('should render gift card balance', () => {
        (useCardBalance as jest.Mock).mockReturnValue({});

        const wrapper = shallow(<GiftCardBalance entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render gift card balance with result', () => {
        (useCardBalance as jest.Mock).mockReturnValue({
            balance: 100,
        });

        const wrapper = shallow(<GiftCardBalance entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render gift card balance with error', () => {
        (useCardBalance as jest.Mock).mockReturnValue({
            error: 'error',
        });

        const wrapper = shallow(<GiftCardBalance entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render gift card balance with loading', () => {
        (useCardBalance as jest.Mock).mockReturnValue({
            loading: true,
        });

        const wrapper = shallow(<GiftCardBalance entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should fire event when submit is successful', async () => {
        const getBalanceMock = jest.fn();
        (useCardBalance as jest.Mock).mockReturnValue({
            getBalance: (getBalanceMock as jest.Mock).mockResolvedValue({}),
        });

        const mockValues = {
            cardNumber: '1234',
            pin: '4321',
        };

        const dispatchMock = jest.fn();

        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const wrapper = shallow(<GiftCardBalance entry={entryMock as any} />);
        await wrapper.find(Formik).simulate('submit', mockValues);

        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_GIFT_CARD_BALANCE_FORM_SUBMIT_SUCCESS,
        });
    });

    it('should not fire event when submit is failed', async () => {
        (useCardBalance as jest.Mock).mockImplementation(
            jest.requireActual('../../../../common/hooks/useCardBalance').default
        );

        (getCardBalance as jest.Mock).mockRejectedValue({ message: 'error' });

        const mockValues = {
            cardNumber: '1234',
            pin: '4321',
        };

        const dispatchMock = jest.fn();

        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const wrapper = shallow(<GiftCardBalance entry={entryMock as any} />);
        await wrapper.find(Formik).simulate('submit', mockValues);

        expect(dispatchMock).not.toHaveBeenCalledWith({
            type: GTM_GIFT_CARD_BALANCE_FORM_SUBMIT_SUCCESS,
        });
    });
});
