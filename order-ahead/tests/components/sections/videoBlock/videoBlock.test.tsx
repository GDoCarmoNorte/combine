import React from 'react';
import { shallow } from 'enzyme';

import VideoBlock from '../../../../components/sections/videoBlock';
import { simpleVideoMock, youtubeMock } from './videoBlock.mock';

describe('VideoBlock', () => {
    it('should render simple video', () => {
        const wrapper = shallow(<VideoBlock entry={simpleVideoMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render simple video with className', () => {
        const wrapper = shallow(<VideoBlock entry={simpleVideoMock as any} className="background-video" />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render youtube video', () => {
        const wrapper = shallow(<VideoBlock entry={youtubeMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render youtube video with autoplay', () => {
        const youtubeAutoPlayMock = { ...youtubeMock, fields: { ...youtubeMock.fields, autoPlay: true } };
        const wrapper = shallow(<VideoBlock entry={youtubeAutoPlayMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });
});
