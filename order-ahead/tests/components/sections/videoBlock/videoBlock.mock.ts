export const simpleVideoMock = {
    fields: {
        name: 'bg',
        webM: {
            fields: {
                title: 'webm bg ',
                file: {
                    url:
                        '//videos.ctfassets.net/o19mhvm9a2cm/7fcQSHsKRNGQmNv3Knisru/aba0fac0ee8090fb4bb315938cd09b80/v3-2300x520-Quesadilla-R7__1_.mp4',
                    details: {
                        size: 17824792,
                    },
                    fileName: 'v3-2300x520-Quesadilla-R7 (1).mp4',
                    contentType: 'video/mp4',
                },
            },
        },
        mp4: {
            fields: {
                title: 'test bg video',
                file: {
                    url:
                        '//videos.ctfassets.net/o19mhvm9a2cm/4m5ZVRX7VvOFms53H71tSZ/8fb7be104c57d979c78bc4ee56ac9534/v4-740x1100-Quesadilla_R7-option2-min.mp4',
                    details: {
                        size: 947586,
                    },
                    fileName: 'v4-740x1100-Quesadilla_R7-option2-min.mp4',
                    contentType: 'video/mp4',
                },
            },
        },
        poster: {
            fields: {
                title: 'video bg poster',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/7kNn5qFHxk9WBRShNR2rOr/6e1dfbdbe519b7784ee1b06486da4da1/cq5dam.web.2440.1600.jpg',
                    details: {
                        size: 536788,
                        image: {
                            width: 3904,
                            height: 1600,
                        },
                    },
                    fileName: 'cq5dam.web.2440.1600.jpg',
                    contentType: 'image/jpeg',
                },
            },
        },
        controls: false,
        autoPlay: true,
        muted: true,
        loop: true,
    },
};

export const youtubeMock = {
    fields: {
        name: 'Video Section',
        externalVideo: {
            fields: {
                name: 'youtube test video',
                nameInUrl: 'https://www.youtube.com/watch?v=Wz3klbq8nWo',
            },
        },
        poster: {
            fields: {
                title: 'video bg poster',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/7kNn5qFHxk9WBRShNR2rOr/6e1dfbdbe519b7784ee1b06486da4da1/cq5dam.web.2440.1600.jpg',
                    details: {
                        size: 536788,
                        image: {
                            width: 3904,
                            height: 1600,
                        },
                    },
                    fileName: 'cq5dam.web.2440.1600.jpg',
                    contentType: 'image/jpeg',
                },
            },
        },
        controls: true,
        autoPlay: false,
        muted: false,
        loop: false,
    },
};
