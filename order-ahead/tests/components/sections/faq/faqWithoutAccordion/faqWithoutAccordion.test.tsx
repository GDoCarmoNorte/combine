import React from 'react';
import { shallow } from 'enzyme';

jest.mock('../../../../../redux/hooks/useGlobalProps');

import FaqWithoutAccordion from '../../../../../components/sections/faq/faqWithoutAccordion/faqWithoutAccordion';
import { emptyMock, withItems, withSubItems } from './faqWithoutAccordion.mock';

describe('FaqWithoutAccordion', () => {
    it('should render without props', () => {
        const wrapper = shallow(<FaqWithoutAccordion {...(emptyMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component', () => {
        const wrapper = shallow(<FaqWithoutAccordion {...(withItems as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render header and with subItems', () => {
        const wrapper = shallow(<FaqWithoutAccordion {...(withSubItems as any)} />);

        expect(wrapper).toMatchSnapshot();
    });
});
