import React from 'react';
import { shallow } from 'enzyme';
import { render } from '@testing-library/react';

import Faq from '../../../../components/sections/faq';
import { requiredFields, withAccordion, withoutAccordion, withoutSectionTitle } from './faq.mock';

describe('Faq', () => {
    it('should render header and by default accordion component', () => {
        const wrapper = shallow(<Faq entry={requiredFields as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render header and accordion component', () => {
        const wrapper = shallow(<Faq entry={withAccordion as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render header and without accordion component', () => {
        const wrapper = shallow(<Faq entry={withoutAccordion as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without section title', () => {
        const { container } = render(<Faq entry={withoutSectionTitle as any} />);

        expect(container).toMatchSnapshot();
    });
});
