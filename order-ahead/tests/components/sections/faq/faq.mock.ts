const sys = {
    space: {
        sys: {
            type: 'Link',
            linkType: 'Space',
            id: 'o19mhvm9a2cm',
        },
    },
    id: '7G5RJCAbZ15B3heYqz6rQ1',
    type: 'Entry',
    createdAt: '2020-10-27T10:34:08.433Z',
    updatedAt: '2020-10-29T12:45:14.348Z',
    environment: {
        sys: {
            id: 'DBBP-607',
            type: 'Link',
            linkType: 'Environment',
        },
    },
    revision: 19,
    contentType: {
        sys: {
            type: 'Link',
            linkType: 'ContentType',
            id: 'frequentlyAskedQuestions',
        },
    },
    locale: 'en-US',
};
export const requiredFields = {
    fields: {
        title: 'test',
    },
    sys,
};

export const withAccordion = {
    fields: {
        title: 'test',
        withAccordion: true,
    },
    sys,
};

export const withoutAccordion = {
    fields: {
        title: 'test',
        withAccordion: false,
    },
    sys,
};

export const withoutSectionTitle = {
    fields: {
        withAccordion: true,
    },
    sys,
};
