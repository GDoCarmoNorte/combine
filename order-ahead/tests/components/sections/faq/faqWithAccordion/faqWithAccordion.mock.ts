export const emptyMock = {
    fields: {},
};

export const withItems = {
    fields: {
        items: [
            {
                fields: {
                    title: 'first point',
                },
                sys: {
                    id: 'itemId',
                },
            },
        ],
    },
};

export const withSubItems = {
    fields: {
        items: [
            {
                fields: {
                    title: 'first point',
                    items: [
                        {
                            fields: {
                                question: 'third question ???????',
                                answer:
                                    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur, aspernatur itaque....',
                            },
                            sys: {
                                id: 'testID',
                            },
                        },
                    ],
                },
                sys: {
                    id: 'itemId',
                },
            },
        ],
    },
};
