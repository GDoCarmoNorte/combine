import React from 'react';
import { shallow } from 'enzyme';

jest.mock('../../../../../redux/hooks/useGlobalProps');

import CollapseItem from '../../../../../components/sections/faq/faqWithAccordion/collapseItem';

const mock = {
    title: 'header',
    items: [
        {
            sys: {
                id: 'itemId',
            },
            fields: {
                answer: 'answer',
                question: 'question',
            },
        },
    ],
};
describe('CollapseItem', () => {
    it('should not render CollapseItem', () => {
        const wrapper = shallow(<CollapseItem {...({} as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component', () => {
        const wrapper = shallow(<CollapseItem {...(mock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should change icon prop after click', () => {
        const wrapper = shallow(<CollapseItem {...(mock as any)} />);

        expect(wrapper.find('.icon').at(0).prop('icon')).toEqual('direction-down');
        wrapper.find('.icon').simulate('click');

        expect(wrapper.find('.icon').at(0).prop('icon')).toEqual('direction-up');
    });
});
