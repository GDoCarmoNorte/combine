import React from 'react';
import { shallow } from 'enzyme';

import FaqWithAccordion from '../../../../../components/sections/faq/faqWithAccordion/faqWithAccordion';
import { emptyMock, withItems, withSubItems } from './faqWithAccordion.mock';

describe('FaqWithAccordion', () => {
    it('should render without props', () => {
        const wrapper = shallow(<FaqWithAccordion {...(emptyMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component', () => {
        const wrapper = shallow(<FaqWithAccordion {...(withItems as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render header and with subItems', () => {
        const wrapper = shallow(<FaqWithAccordion {...(withSubItems as any)} />);

        expect(wrapper).toMatchSnapshot();
    });
});
