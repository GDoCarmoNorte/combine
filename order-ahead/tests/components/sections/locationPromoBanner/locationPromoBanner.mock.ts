export const locationPromoBannerMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '4haLFODU8gLmd8mSE0LFhJ',
        type: 'Entry',
        createdAt: '2021-09-21T12:01:56.555Z',
        updatedAt: '2021-09-21T12:01:56.555Z',
        environment: {
            sys: {
                id: 'rio-feature-DBBP-31337-Promo',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'locationPromoBanner',
            },
        },
        locale: 'en-US',
    },
    fields: {
        title: '$3 tall house beer every day',
        description: 'Get here Tall Wild Herd Kölsch by Goose Island.',
        icon: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '017KBYYBt1ZMvXb7SM3y5Y',
                type: 'Asset',
                createdAt: '2021-08-06T08:47:05.232Z',
                updatedAt: '2021-08-06T08:47:05.232Z',
                environment: {
                    sys: {
                        id: 'rio-feature-DBBP-31337-Promo',
                        type: 'Link',
                        linkType: 'Environment+',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'Rewards main icon',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/017KBYYBt1ZMvXb7SM3y5Y/c4e5ad7c4c4187b3ed5b1f61ce66081b/Back.png',
                    details: {
                        size: 3406,
                        image: {
                            width: 140,
                            height: 140,
                        },
                    },
                    fileName: 'Back.png',
                    contentType: 'image/png',
                },
            },
        },
        backgroundImage: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '4bpwZqa8WNxnMdtlVTqfj7',
                type: 'Asset',
                createdAt: '2021-08-11T11:52:32.286Z',
                updatedAt: '2021-08-11T11:52:32.286Z',
                environment: {
                    sys: {
                        id: 'rio-feature-DBBP-31337-Promo',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'image 1 (1)',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/4bpwZqa8WNxnMdtlVTqfj7/66643164756dadfeaeb80a5836c71012/image_1__1_.png',
                    details: {
                        size: 396306,
                        image: {
                            width: 1440,
                            height: 180,
                        },
                    },
                    fileName: 'image 1 (1).png',
                    contentType: 'image/png',
                },
            },
        },
        titleColor: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '15h1wW3x2JfoAqgpS6iwax',
                type: 'Entry',
                createdAt: '2021-07-09T21:24:12.597Z',
                updatedAt: '2021-09-14T13:56:25.948Z',
                environment: {
                    sys: {
                        id: 'rio-feature-DBBP-31337-Promo',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 4,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'bww-white',
                hexColor: 'FFFFFF',
            },
        },
        descriptionColor: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '15h1wW3x2JfoAqgpS6iwax',
                type: 'Entry',
                createdAt: '2021-07-09T21:24:12.597Z',
                updatedAt: '2021-09-14T13:56:25.948Z',
                environment: {
                    sys: {
                        id: 'rio-feature-DBBP-31337-Promo',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 4,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'bww-white',
                hexColor: 'FFFFFF',
            },
        },
    },
};
