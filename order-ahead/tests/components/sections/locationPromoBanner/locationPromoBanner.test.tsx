import React from 'react';
import { render, screen } from '@testing-library/react';
import LocationPromoBanner from '../../../../components/sections/locationPromoBanner';
import { locationPromoBannerMock } from './locationPromoBanner.mock';

describe('LocationNews component', () => {
    it('should render correctly', () => {
        render(<LocationPromoBanner entry={locationPromoBannerMock as any} />);

        //render header
        expect(screen.getByText(/tall house beer every day/i)).toBeInTheDocument();

        //render description
        expect(screen.getByText(/Get here Tall Wild Herd Kölsch by Goose Island/i)).toBeInTheDocument();

        //render icon
        expect(screen.getByAltText(/Rewards main icon/i)).toBeInTheDocument();
    });
    it('should not render title if not provided', () => {
        const testLocationPromoBannerMock = {
            ...locationPromoBannerMock,
            fields: {
                ...locationPromoBannerMock.fields,
                title: undefined,
            },
        };
        render(<LocationPromoBanner entry={testLocationPromoBannerMock as any} />);
        expect(screen.queryByText(/tall house beer every day/i)).not.toBeInTheDocument();
    });
    it('should not render description if not provided', () => {
        const testLocationPromoBannerMock = {
            ...locationPromoBannerMock,
            fields: {
                ...locationPromoBannerMock.fields,
                description: undefined,
            },
        };
        render(<LocationPromoBanner entry={testLocationPromoBannerMock as any} />);
        expect(screen.queryByText(/Get here Tall Wild Herd Kölsch by Goose Island/i)).not.toBeInTheDocument();
    });
    it('should not render icon if not provided', () => {
        const testLocationPromoBannerMock = {
            ...locationPromoBannerMock,
            fields: {
                ...locationPromoBannerMock.fields,
                icon: undefined,
            },
        };
        render(<LocationPromoBanner entry={testLocationPromoBannerMock as any} />);
        expect(screen.queryByAltText(/Rewards main icon/i)).not.toBeInTheDocument();
    });
});
