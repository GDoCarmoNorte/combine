export default {
    fields: {
        leftSideTopText: 'COME AND GET IT',
        leftSideMainText: 'TOP PICKS',
        isSideScroll: false,
        category: {
            fields: {
                categoryName: 'Top Picks',
                link: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'o19mhvm9a2cm',
                            },
                        },
                        id: '6d091b22-8052-45df-9930-0e7bba549f0b',
                        type: 'Entry',
                        createdAt: '2021-04-19T10:26:14.938Z',
                        updatedAt: '2021-04-19T10:26:14.938Z',
                        environment: {
                            sys: {
                                id: 'circular-references',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'menuCategoryLink',
                            },
                        },
                        locale: 'en-US',
                    },
                    fields: {
                        name: 'Top Picks',
                        nameInUrl: 'top-picks',
                    },
                },
                products: [
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '5nwUfTi3RjS0LHVIiWCxAm',
                            type: 'Entry',
                            createdAt: '2020-12-28T16:51:26.866Z',
                            updatedAt: '2021-02-22T13:29:44.664Z',
                            environment: {
                                sys: {
                                    id: 'uat-stg',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 4,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'product',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Crispy Fish Sandwich Meal',
                            nameInUrl: 'crispy-fish-sandwich-meal',
                            image: {
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'o19mhvm9a2cm',
                                        },
                                    },
                                    id: '3dy8sRRX5Uznl61lxLSuw3',
                                    type: 'Asset',
                                    createdAt: '2020-12-16T21:27:29.627Z',
                                    updatedAt: '2020-12-16T21:27:29.627Z',
                                    environment: {
                                        sys: {
                                            id: 'uat-stg',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 1,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'Meal Fish Crispy',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/3dy8sRRX5Uznl61lxLSuw3/3cd98d8fbc8c4414e58a02592de9d2c0/Website_Meal_Fish_Crispy.png',
                                        details: {
                                            size: 2589850,
                                            image: {
                                                width: 4000,
                                                height: 3000,
                                            },
                                        },
                                        fileName: 'Website_Meal_Fish_Crispy.png',
                                        contentType: 'image/png',
                                    },
                                },
                            },
                            productId: 'arb-itm-000-166',
                            metaDescription:
                                'Our Crispy Fish Sandwich pairs perfectly with your choice of side and a drink for the ultimate lunch or dinner combo. Start your order today!',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '4xJd2fZ5f8VUB6voFoMkfw',
                            type: 'Entry',
                            createdAt: '2020-09-23T14:10:19.511Z',
                            updatedAt: '2021-02-22T13:30:02.750Z',
                            environment: {
                                sys: {
                                    id: 'uat-stg',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 4,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'product',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Double Roast Beef Meal',
                            nameInUrl: 'double-roast-beef-meal',
                            image: {
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'o19mhvm9a2cm',
                                        },
                                    },
                                    id: '3hTwgMpSFmcUK82EyoNPw9',
                                    type: 'Asset',
                                    createdAt: '2020-09-25T17:54:12.879Z',
                                    updatedAt: '2020-09-25T17:54:12.879Z',
                                    environment: {
                                        sys: {
                                            id: 'uat-stg',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 1,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'Meals Double Roast Beef',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/3hTwgMpSFmcUK82EyoNPw9/92b031e10f03ae5a8bacf432aa327959/Website_Meals_DRB.png',
                                        details: {
                                            size: 2716975,
                                            image: {
                                                width: 4000,
                                                height: 3000,
                                            },
                                        },
                                        fileName: 'Website_Meals_DRB.png',
                                        contentType: 'image/png',
                                    },
                                },
                            },
                            productId: 'arb-itm-000-009',
                            metaDescription:
                                'Our Double Roast Beef Sandwich pairs perfectly with your choice of side and a drink for the ultimate lunch or dinner combo. Start your order today!',
                        },
                    },
                ],
                image: {
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'o19mhvm9a2cm',
                            },
                        },
                        id: '18lLHEMB2mIrRYtWYUdCtU',
                        type: 'Asset',
                        createdAt: '2020-09-25T18:13:21.427Z',
                        updatedAt: '2020-09-25T18:13:21.427Z',
                        environment: {
                            sys: {
                                id: 'uat-stg',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        locale: 'en-US',
                    },
                    fields: {
                        title: 'Meals Signatures Gyro Roast Beef',
                        file: {
                            url:
                                '//images.ctfassets.net/o19mhvm9a2cm/18lLHEMB2mIrRYtWYUdCtU/696621fac0c2c83c47bac1aaaee96693/Website_Meals_Signatures_Gyro_RB.png',
                            details: {
                                size: 2345415,
                                image: {
                                    width: 4000,
                                    height: 3000,
                                },
                            },
                            fileName: 'Website_Meals_Signatures_Gyro_RB.png',
                            contentType: 'image/png',
                        },
                    },
                },
                metaDescription: "Arby's Top Picks",
            },
        },
    },
};
