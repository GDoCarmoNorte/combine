import React from 'react';
import { render, screen } from '@testing-library/react';
import TopPicks from '../../../../components/sections/topPicks';
import entryMock from './topPicks.mock';
import { getImageUrl } from '../../../../common/helpers/contentfulImage';
import {
    useDiscountPriceAndCalories,
    useDomainMenuSelectors,
    useProducts,
    useTallyPriceAndCalories,
} from '../../../../redux/hooks/domainMenu';
import getBrandInfo, { BRANDS } from '../../../../lib/brandInfo';
import { useDomainMenu, useOrderLocation } from '../../../../redux/hooks';
import { locationSpecificMenuCategoriesEnabled } from '../../../../lib/getFeatureFlags';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks/domainMenu');
jest.mock('../../../../redux/store', () => ({
    useAppSelector: () => jest.fn(),
}));

jest.mock('../../../../lib/brandInfo');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useBag');
jest.mock('../../../../redux/hooks/useDomainMenu');
jest.mock('../../../../common/hooks/useUnavailableCategories', () => ({
    useUnavailableCategories: () => jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../common/hooks/useProductIsAvailable', () => ({
    useProductIsAvailable: () => jest.fn().mockReturnValue({ isAvailable: true }),
}));

jest.mock('../../../../common/helpers/contentfulImage');

jest.mock('../../../../lib/getFeatureFlags');

jest.mock('../../../../common/hooks/useTallyItemHasSodiumWarning', () => ({
    useTallyItemHasSodiumWarning: () => true,
}));

describe('TopPicks', () => {
    beforeEach(() => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: BRANDS.bww.brandId });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isCurrentLocationOAAvailable: true,
        });
        (useProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-166': { availability: { isAvailable: true }, isSaleable: true },
            'arb-itm-000-009': { availability: { isAvailable: true }, isSaleable: true },
        });
        (useDomainMenuSelectors as jest.Mock).mockReturnValue({
            selectProductSize: () => 'size',
        });
        (useDiscountPriceAndCalories as jest.Mock).mockReturnValue({
            price: 5,
            calories: 100,
        });
        (useDomainMenu as jest.Mock).mockReturnValue({
            actions: { getDomainMenu: jest.fn() },
        });
        (locationSpecificMenuCategoriesEnabled as jest.Mock).mockReturnValue(true);
        (useTallyPriceAndCalories as jest.Mock).mockReturnValue({
            calories: 100,
        });
    });

    it('should not render compinent if no fields are provided', () => {
        const { container } = render(<TopPicks entry={{ ...entryMock, fields: undefined } as any} />);

        expect(container).toBeEmptyDOMElement();
    });

    it('should not render compinent if no category is provided', () => {
        const { container } = render(
            <TopPicks entry={{ ...entryMock, fields: { ...entryMock.fields, category: undefined } } as any} />
        );
        expect(container).toBeEmptyDOMElement();
    });

    it('should render component', () => {
        const { container } = render(<TopPicks entry={entryMock as any} />);

        expect(container).toMatchSnapshot();
    });

    it('should render without subtitle', () => {
        render(
            <TopPicks entry={{ ...entryMock, fields: { ...entryMock.fields, leftSideTopText: undefined } } as any} />
        );

        expect(screen.queryByText(/COME AND GET IT/i)).not.toBeInTheDocument();
    });

    it('should render with side scroll', () => {
        render(<TopPicks entry={{ ...entryMock, fields: { ...entryMock.fields, isSideScroll: true } } as any} />);

        expect(screen.getByLabelText(/Next product/i)).toBeInTheDocument();
    });

    it('should render with background image', () => {
        const url = 'image-url';
        (getImageUrl as jest.Mock).mockReturnValueOnce(url);

        render(<TopPicks entry={{ ...entryMock, fields: { ...entryMock.fields, backgroundImage: url } } as any} />);

        expect(getImageUrl).toHaveBeenCalledWith(url);
    });

    it('should render with left side image', () => {
        const url = 'image-url';
        (getImageUrl as jest.Mock).mockReturnValueOnce(url);

        render(<TopPicks entry={{ ...entryMock, fields: { ...entryMock.fields, leftSideImage: url } } as any} />);

        expect(getImageUrl).toHaveBeenCalledWith(url);
    });

    it('should render products if they are available', () => {
        render(<TopPicks entry={entryMock as any} />);

        expect(screen.getByText(/Crispy Fish Sandwich Meal/i)).toBeInTheDocument();
        expect(screen.getByText(/Double Roast Beef Meal/i)).toBeInTheDocument();
    });

    it('should render only 1 available product', () => {
        (useProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-166': {
                availability: { isAvailable: true },
                isSaleable: true,
            },
        });

        render(<TopPicks entry={entryMock as any} />);

        expect(screen.getByText(/Crispy Fish Sandwich Meal/i)).toBeInTheDocument();
        expect(screen.queryByText(/Double Roast Beef Meal/i)).not.toBeInTheDocument();
    });

    it('should render all products if brand is Arbys', () => {
        (useProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-166': { availability: { isAvailable: false }, isSaleable: true },
            'arb-itm-000-009': { availability: { isAvailable: false }, isSaleable: true },
        });
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: BRANDS.arbys.brandId });

        render(<TopPicks entry={entryMock as any} />);

        expect(screen.getByText(/Crispy Fish Sandwich Meal/i)).toBeInTheDocument();
        expect(screen.getByText(/Double Roast Beef Meal/i)).toBeInTheDocument();
    });
});
