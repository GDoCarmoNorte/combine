import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { localTapListProps, mockBeerData, localTapListPropsWithoutHeader } from './localTapList.mock';
import LocalTapList from '../../../../components/sections/localTapList';
import useLocalTapList from '../../../../redux/hooks/useLocalTapList';
import { getBeerMenuByLocationId } from '../../../../common/services/localTapList';

jest.mock('../../../../redux/hooks/useLocalTapList', () => jest.fn());
jest.mock('../../../../common/services/localTapList');

describe('Local Tap List component', () => {
    it('should render correctly contentfull data', () => {
        (useLocalTapList as any).mockReturnValue({ list: {}, available: true });

        render(<LocalTapList {...localTapListProps} />);

        expect(screen.getByText(/Name/i)).toBeInTheDocument();
        expect(screen.getByText(/Description/i)).toBeInTheDocument();
        expect(screen.getByText(/Title/i)).toBeInTheDocument();
        expect(screen.getByAltText(/Title/i)).toBeInTheDocument();
    });

    it('should render correctly contentfull data without header', () => {
        (useLocalTapList as any).mockReturnValue({ list: {}, available: true });

        render(<LocalTapList {...localTapListPropsWithoutHeader} />);

        expect(screen.queryByText(/Name/i)).not.toBeInTheDocument();
        expect(screen.getByText(/Description/i)).toBeInTheDocument();
        expect(screen.getByText(/Title/i)).toBeInTheDocument();
        expect(screen.getByAltText(/Title/i)).toBeInTheDocument();
    });

    it('should render correctly dynamic data', () => {
        (useLocalTapList as any).mockReturnValue({ list: mockBeerData.beersByType, available: true });

        render(<LocalTapList {...localTapListProps} />);

        // tabs
        expect(screen.getByText(/^(DRAFT)$/i)).toBeInTheDocument();
        expect(screen.getByText(/^(BOTTLE)$/i)).toBeInTheDocument();

        // tab data
        expect(screen.getByText(/draft beer/i)).toBeInTheDocument();

        // change tab
        fireEvent(
            screen.getByText(/^(BOTTLE)$/i),
            new MouseEvent('click', {
                bubbles: true,
                cancelable: true,
            })
        );

        expect(screen.getByText(/bottle beer/i)).toBeInTheDocument();
    });

    it('should render empty section if no tap list not available', () => {
        (useLocalTapList as any).mockReturnValue({ list: {}, available: false });

        render(<LocalTapList {...localTapListProps} />);

        expect(screen.queryByText(/Name/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Description/i)).not.toBeInTheDocument();
    });

    it('should request tap list for the passed location and render correctly with both sizes', async () => {
        const mockTapListResponse: any = {
            beersByType: {
                CRAFT: {
                    count: 1,
                    items: [
                        {
                            name: 'Angry Orchard Crisp Apple Cider',
                            category: 'Ciders & Specialty',
                            alcoholPercentage: '5% ABV',
                            caloriesBySize: {
                                REGULAR: 230,
                                TALL: 320,
                            },
                        },
                    ],
                },
            },
        };
        (getBeerMenuByLocationId as jest.Mock).mockResolvedValueOnce(mockTapListResponse);
        (useLocalTapList as any).mockReturnValue({ list: {}, available: false });

        render(<LocalTapList {...localTapListProps} data={{ locationDetails: { id: 1 } }} />);

        await waitFor(() => {
            expect(screen.getByText(/Name/i)).toBeInTheDocument();
        });
        expect(screen.getByText(/Description/i)).toBeInTheDocument();
        expect(screen.getByText(/Title/i)).toBeInTheDocument();
        expect(screen.getByAltText(/Title/i)).toBeInTheDocument();
        expect(screen.getByText('230 cal | 320 cal')).toBeInTheDocument();
    });
    it('should request tap list for the passed location and render correctly with only one size', async () => {
        const mockTapListResponse: any = {
            beersByType: {
                CRAFT: {
                    count: 1,
                    items: [
                        {
                            name: 'Angry Orchard Crisp Apple Cider',
                            category: 'Ciders & Specialty',
                            alcoholPercentage: '5% ABV',
                            caloriesBySize: {
                                REGULAR: 250,
                            },
                        },
                    ],
                },
            },
        };
        (getBeerMenuByLocationId as jest.Mock).mockResolvedValueOnce(mockTapListResponse);
        (useLocalTapList as any).mockReturnValue({ list: {}, available: false });

        render(<LocalTapList {...localTapListProps} data={{ locationDetails: { id: 1 } }} />);

        await waitFor(() => {
            expect(screen.getByText(/250 cal/i)).toBeInTheDocument();
        });
    });
    it('should request tap list for the passed location and render correctly without any size', async () => {
        const mockTapListResponse: any = {
            beersByType: {
                CRAFT: {
                    count: 1,
                    items: [
                        {
                            name: 'Angry Orchard Crisp Apple Cider',
                            category: 'Ciders & Specialty',
                            alcoholPercentage: '5% ABV',
                            caloriesBySize: {},
                        },
                    ],
                },
            },
        };
        (getBeerMenuByLocationId as jest.Mock).mockResolvedValueOnce(mockTapListResponse);
        (useLocalTapList as any).mockReturnValue({ list: {}, available: false });

        render(<LocalTapList {...localTapListProps} data={{ locationDetails: { id: 1 } }} />);

        await waitFor(() => {
            expect(screen.getByText(/Calorie info unavailable/i)).toBeInTheDocument();
        });
    });
    it('should request tap list for the passed location and render correctly with number of oz', async () => {
        const mockTapListResponse: any = {
            beersByType: {
                CRAFT: {
                    count: 1,
                    items: [
                        {
                            name: 'Angry Orchard Crisp Apple Cider',
                            category: 'Ciders & Specialty',
                            alcoholPercentage: '5% ABV',
                            caloriesBySize: {
                                ['18.1 oz']: 320,
                            },
                        },
                    ],
                },
            },
        };
        (getBeerMenuByLocationId as jest.Mock).mockResolvedValueOnce(mockTapListResponse);
        (useLocalTapList as any).mockReturnValue({ list: {}, available: false });

        render(<LocalTapList {...localTapListProps} data={{ locationDetails: { id: 1 } }} />);

        await waitFor(() => {
            expect(screen.getByText(/320 cal/i)).toBeInTheDocument();
        });
        expect(screen.getByText(/Available in 18.1 oz only/i)).toBeInTheDocument();
    });
    it('should render empty section if attempt to get location tap list failed', async () => {
        (getBeerMenuByLocationId as jest.Mock).mockRejectedValueOnce('test error');
        (useLocalTapList as any).mockReturnValue({ list: {}, available: false });

        render(<LocalTapList {...localTapListProps} data={{ locationDetails: { id: 1 } }} />);

        await waitFor(() => {
            expect(screen.queryByText(/Name/i)).not.toBeInTheDocument();
        });
        expect(screen.queryByText(/Description/i)).not.toBeInTheDocument();
    });
});
