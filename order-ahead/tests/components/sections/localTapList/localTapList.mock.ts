export const localTapListProps: any = {
    entry: {
        fields: {
            name: 'Name',
            title: 'Title',
            description: 'Description',
            image: {
                fields: {
                    file: {
                        url: '',
                    },
                },
            },
        },
    },
};

export const localTapListPropsWithoutHeader: any = {
    entry: {
        fields: {
            title: 'Title',
            description: 'Description',
            image: {
                fields: {
                    file: {
                        url: '',
                    },
                },
            },
        },
    },
};

export const mockBeerData: any = {
    beersByType: {
        DRAFT: {
            count: 1,
            items: [
                {
                    name: 'draft beer',
                    style: '',
                    category: 'category',
                    alcoholPercentage: 5,
                    caloriesBySize: {
                        REG: 230,
                        TALL: 300,
                    },
                },
            ],
        },
        BOTTLE: {
            count: 1,
            items: [
                {
                    name: 'bottle beer',
                    style: '',
                    category: 'category',
                    alcoholPercentage: 5,
                    caloriesBySize: {
                        REG: 130,
                        TALL: 200,
                    },
                },
            ],
        },
    },
};
