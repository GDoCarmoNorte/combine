import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import GooglePaySection from '../../../../components/clientOnly/paymentInfoContainer/googlePaySection/googlePaySection';
import { useOrderLocation } from '../../../../redux/hooks';
import { useGooglePayment } from '../../../../common/hooks/useGooglePayment';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { TInitialPaymentTypes } from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../common/hooks/useGooglePayment');
jest.mock('../../../../common/hooks/useFreedomPay');
jest.mock('../../../../lib/getFeatureFlags');
jest.mock('../../../../common/hooks/useConfiguration', () => ({
    createConfiguration: jest.fn(),
    useConfiguration: jest.fn(),
}));

const resetFPIframeMock = jest.fn();

const locationIdMock = '13';
const totalAmountMock = 10;

describe('Google Pay section', () => {
    beforeEach(() => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isGooglePayEnabled: true,
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({ currentLocation: { id: locationIdMock } });
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: null,
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render iframe if device compatible and html provided', async () => {
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<GooglePaySection totalAmount={totalAmountMock} />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Google pay/i)).toBeInTheDocument();
        });
    });
    it('should not render iframe if device not compatible', async () => {
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: false,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<GooglePaySection totalAmount={totalAmountMock} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
        });
    });
    it('should not render iframe if isGooglePayEnabled: false', async () => {
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isGooglePayEnabled: false,
            },
        });
        render(<GooglePaySection totalAmount={totalAmountMock} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
        });
    });
    it('should show loader if iframe is loading', async () => {
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: true,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<GooglePaySection totalAmount={totalAmountMock} />);
        await waitFor(() => {
            expect(screen.getByRole('progressbar')).toBeInTheDocument();
        });
        expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
    });
    it('should not render iframe if iframe not retrieved from FP', async () => {
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: null },
            error: null,
            isLoading: false,
        });
        render(<GooglePaySection totalAmount={totalAmountMock} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
        });
    });
    it('should not render iframe if payment token already retrieved', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: {
                keys: ['test'],
                cardHolderName: 'Test test',
                paymentType: TInitialPaymentTypes.GOOGLE_PAY,
            },
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        const submitSuccessfulMock = () => {
            return Promise.resolve();
        };

        render(<GooglePaySection totalAmount={totalAmountMock} onSubmit={submitSuccessfulMock} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
        });
    });

    it('should render iframe if payment token retrieved is not GooglePay one', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: {
                keys: ['test'],
                cardHolderName: 'Test test',
                paymentType: TInitialPaymentTypes.CREDIT_OR_DEBIT,
            },
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<GooglePaySection totalAmount={totalAmountMock} />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Google pay/i)).toBeInTheDocument();
        });
    });
    it('should call reset FP iframe if submit failed', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: {
                keys: ['test'],
                cardHolderName: 'Test test',
                paymentType: TInitialPaymentTypes.GOOGLE_PAY,
            },
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        const submitRejectedMock = () => {
            return Promise.reject();
        };

        render(<GooglePaySection totalAmount={totalAmountMock} onSubmit={submitRejectedMock} />);
        await waitFor(() => {
            expect(resetFPIframeMock).toHaveBeenCalled();
        });
        expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
    });
    it('should handle FP errors', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: null,
            height: 60,
            errors: ['test error'],
            reset: resetFPIframeMock,
        });
        const onErrorMock = jest.fn();
        render(<GooglePaySection totalAmount={totalAmountMock} onError={onErrorMock} />);
        await waitFor(() => {
            expect(onErrorMock).toHaveBeenCalled();
        });
    });
    it('should handle iframe errors', async () => {
        (useGooglePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: {
                name: 'test error',
                message: 'test error message',
            },
            isLoading: false,
        });
        const onErrorMock = jest.fn();
        render(<GooglePaySection totalAmount={totalAmountMock} onError={onErrorMock} />);
        await waitFor(() => {
            expect(onErrorMock).toHaveBeenCalled();
        });
        expect(screen.queryByLabelText(/Google pay/i)).not.toBeInTheDocument();
    });
});
