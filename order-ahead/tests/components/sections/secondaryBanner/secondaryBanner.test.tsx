/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import React from 'react';
import { render, screen } from '@testing-library/react';

import SecondaryBanner from '../../../../components/sections/secondaryBanner';
import entryMock from './secondaryBanner.mock';

jest.mock('../../../../lib/gtm', () => ({
    getGtmIdByName: () => 'secondaryBanner',
}));

jest.mock('../../../../components/atoms/ContentfulImage', () =>
    jest.fn((props) => <div data-testid="contentful-image-mock">props: {JSON.stringify(props, null, 2)}</div>)
);

jest.mock('../../../../components/atoms/button', () => ({
    InspireButton: jest.fn((props) => (
        <div data-testid="inspire-button-mock">props: {JSON.stringify(props, null, 2)}</div>
    )),
}));

const legalMessageMock: any = {
    fields: {
        message: 'Some message',
    },
};

describe('secondaryBanner', () => {
    it('should render component', () => {
        const { container } = render(<SecondaryBanner entry={entryMock as any} />);

        expect(container).toMatchSnapshot();
    });

    it('should render icon', () => {
        render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            icon: {
                                fields: {
                                    description: 'icon description',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/01oEonVA5KwK6ahfJaNu73/4c046f6a7ab1723b787b6eb4aace95cf/Vector.svg',
                                    },
                                },
                            },
                        },
                    } as any
                }
            />
        );

        expect(screen.getByTestId('contentful-image-mock')).toMatchSnapshot();
    });

    it('should render main text', () => {
        render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            mainText: '2 for $6 Everyday value',
                        },
                    } as any
                }
            />
        );

        screen.getByText('2 for $6 Everyday value');
    });

    it('should render link', () => {
        render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            linkText: 'See Deal',
                            linkUrl: '/drivethrudeals',
                        },
                    } as any
                }
            />
        );

        expect(screen.getByTestId('inspire-button-mock')).toMatchSnapshot();
    });

    it('should render with border', () => {
        const { container } = render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            hasBorder: true,
                        },
                    } as any
                }
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should arrange content in line', () => {
        const { container } = render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            arrangeInline: true,
                        },
                    } as any
                }
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should arrange content in column', () => {
        const { container } = render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            arrangeInline: false,
                        },
                    } as any
                }
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render with configured colors', () => {
        const { container } = render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            backgroundColor: {
                                fields: {
                                    hexColor: 'backgroundColor',
                                },
                            },
                            mainTextColor: {
                                fields: {
                                    hexColor: 'mainTextColor',
                                },
                            },
                            titleColor: {
                                fields: {
                                    hexColor: 'titleColor',
                                },
                            },
                            descriptionColor: {
                                fields: {
                                    hexColor: 'descriptionColor',
                                },
                            },
                        },
                    } as any
                }
            />
        );

        // eslint-disable-next-line testing-library/no-container
        expect(container.querySelector('style')).toMatchSnapshot();
    });

    it('should render with configured background image', () => {
        const { container } = render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            backgroundImage: {
                                fields: {
                                    description: 'image description',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/01oEonVA5KwK6ahfJaNu73/4c046f6a7ab1723b787b6eb4aace95cf/bgimage.jpeg',
                                    },
                                },
                            },
                        },
                    } as any
                }
            />
        );

        expect(container.querySelector('style')).toMatchSnapshot();
    });

    it('should render background image for mobile', () => {
        const { container } = render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            mobileBackgroundImage: {
                                fields: {
                                    description: 'mobile image description',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/01oEonVA5KwK6ahfJaNu73/4c046f6a7ab1723b787b6eb4aace95cf/bgimage.jpeg',
                                    },
                                },
                            },
                        },
                    } as any
                }
            />
        );

        expect(container.querySelector('style')).toMatchSnapshot();
    });

    it('should render legal message', () => {
        render(
            <SecondaryBanner
                entry={
                    {
                        fields: {
                            ...entryMock.fields,
                            legalMessage: legalMessageMock,
                        },
                    } as any
                }
            />
        );

        screen.getByText('Terms Apply.');
        screen.getByText('View Details.');
    });
});
