export default {
    fields: {
        hasBorder: false,
        title: 'Hungry and on a budget? Try out these everyday value items.',
        description: 'Valid on 3 piece tenders, Chicken Cheddar Ranch sandwich, and Buffalo Crispy Chicken sandwich.',
    },
};
