import React from 'react';
import NumberedList from '../../../../components/sections/numberedList/numberedListSection';
import { render } from '@testing-library/react';
import numberedListMock from './numberedList.mock';

describe('NumberedList section', () => {
    it('should render correctly', () => {
        const { container } = render(<NumberedList entry={numberedListMock as any} />);
        expect(container).toMatchSnapshot();
    });
});
