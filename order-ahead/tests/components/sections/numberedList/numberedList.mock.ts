export default {
    fields: {
        title: 'A Numbered List',
        description: "This is a numbered list that we want to use for testing. It's, you know, a list. With numbers. ",
        items: [
            {
                fields: {
                    title: 'Section One',
                    description: "This is the first section of the numbered list. It's okay, I guess.",
                },
            },
            {
                fields: {
                    title: 'Section 2',
                    description:
                        "This is the 2nd item in the numbered list. It's an awful lot like the 1st section. But how different do you expect items in a numbered lists to be?  ",
                },
            },
            {
                fields: {
                    title: '3rd Section',
                    description:
                        "What are you expecting here? This one is like the other two but it's probably important to make sure things wrap the way they're supposed to by adding more text to this one than it really needs.",
                },
            },
        ],
    },
};
