import React from 'react';
import { render, screen } from '@testing-library/react';

import TextBlock from '../../../../components/sections/textBlock';
import textBlockMock, {
    textBlockWithTitleMock,
    textBlockWithSubtitleMock,
    textBlockWithAccordionMock,
} from './textBlock.mock';

jest.mock('../../../../common/hooks/useRichTextOptions', () => jest.fn().mockReturnValue({}));

describe('TextBlock', () => {
    it('should render text block', () => {
        render(<TextBlock entry={textBlockMock} />);

        screen.getByText('Section text content');
    });

    it('should render text block with title', () => {
        render(<TextBlock entry={textBlockWithTitleMock} />);

        screen.getByRole('heading', { name: 'Section title', level: 2 });
    });

    it('should render text block with subtitle', () => {
        render(<TextBlock entry={textBlockWithSubtitleMock} />);

        screen.getByRole('heading', { name: 'Subtitle', level: 3 });
    });

    it('should render text block with accordion', () => {
        render(<TextBlock entry={textBlockWithAccordionMock} />);

        screen.getByText('Accordion title');
        screen.getByText('Accordion caption text');
        screen.getByText('Accordion item content');
    });
});
