import { ITextBlockSection } from '../../../../@generated/@types/contentful';

const textBlock = ({
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '7BbVjBeF7Zy0QDhjUHteEB',
        type: 'Entry',
        createdAt: '2021-01-27T09:29:01.763Z',
        updatedAt: '2021-01-28T15:32:46.465Z',
        environment: {
            sys: {
                id: 'DBBP-1877-text-block',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 11,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'textBlockSection',
            },
        },
        locale: 'en-US',
    },
    fields: {
        text: {
            data: {},
            content: [
                {
                    data: {},
                    content: [
                        {
                            data: {},
                            marks: [],
                            value: 'Section text content',
                            nodeType: 'text',
                        },
                    ],
                    nodeType: 'paragraph',
                },
            ],
            nodeType: 'document',
        },
    },
} as unknown) as ITextBlockSection;

export const textBlockWithTitleMock = ({
    ...textBlock,
    fields: { ...textBlock.fields, title: 'Section title' },
} as unknown) as ITextBlockSection;

export const textBlockWithSubtitleMock = ({
    ...textBlock,
    fields: { ...textBlock.fields, subtitle: 'Subtitle' },
} as unknown) as ITextBlockSection;

export const textBlockWithAccordionMock = ({
    ...textBlock,
    fields: {
        ...textBlock.fields,
        accordionItems: [
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: '2KyvUbMJ7z7OKbaDAmcdnV',
                    type: 'Entry',
                    createdAt: '2021-01-28T11:25:22.458Z',
                    updatedAt: '2021-01-28T15:32:05.675Z',
                    environment: {
                        sys: {
                            id: 'DBBP-1877-text-block',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 4,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'textBlockSectionAccordionItem',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Accordion title',
                    text: 'Accordion caption text',
                    content: {
                        nodeType: 'document',
                        data: {},
                        content: [
                            {
                                nodeType: 'paragraph',
                                content: [
                                    {
                                        nodeType: 'text',
                                        value: 'Accordion item content',
                                        marks: [],
                                        data: {},
                                    },
                                ],
                                data: {},
                            },
                        ],
                    },
                },
            },
        ],
    },
} as unknown) as ITextBlockSection;

export default textBlock;
