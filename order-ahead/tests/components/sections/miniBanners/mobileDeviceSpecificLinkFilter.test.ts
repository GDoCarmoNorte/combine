import { IAppLink } from '../../../../@generated/@types/contentful';
import { getMobileOperatingSystem, MobileOperatingSystem } from '../../../../common/helpers/getMobileOperatingSystem';
import { mobileDeviceSpecificLinkFilter } from '../../../../components/sections/miniBanners/mobileDeviceSpecificLinkFilter';

jest.mock('../../../../common/helpers/getMobileOperatingSystem');

const appStoreLink = {
    fields: {
        title: 'App Store',
        url: 'https://apps.apple.com/us/app/youtube-watch-listen-stream/id544007664',
        image: {},
    },
};

const googlePlayLink = {
    fields: {
        title: 'Google Play',
        url: 'https://play.google.com/store/apps/details?id=com.google.android.youtube&hl=en_US&gl=US',
        image: {},
    },
};

describe('mobileDeviceSpecificLinkFilter', () => {
    it("should return true for appstore link when it's apple device", () => {
        (getMobileOperatingSystem as jest.Mock).mockReturnValue(MobileOperatingSystem.IOS);

        const result = mobileDeviceSpecificLinkFilter(appStoreLink as IAppLink);

        expect(result).toEqual(true);
    });

    it("should return false for not appstore link when it's apple device", () => {
        (getMobileOperatingSystem as jest.Mock).mockReturnValue(MobileOperatingSystem.IOS);

        const result = mobileDeviceSpecificLinkFilter(googlePlayLink as IAppLink);

        expect(result).toEqual(false);
    });

    it("should return true for appstore link when it's android device", () => {
        (getMobileOperatingSystem as jest.Mock).mockReturnValue(MobileOperatingSystem.ANDROID);

        const result = mobileDeviceSpecificLinkFilter(googlePlayLink as IAppLink);

        expect(result).toEqual(true);
    });

    it("should return false for not appstore link when it's android device", () => {
        (getMobileOperatingSystem as jest.Mock).mockReturnValue(MobileOperatingSystem.ANDROID);

        const result = mobileDeviceSpecificLinkFilter(appStoreLink as IAppLink);

        expect(result).toEqual(false);
    });
});
