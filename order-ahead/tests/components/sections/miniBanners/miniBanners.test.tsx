import React from 'react';
import { shallow } from 'enzyme';

import MiniBanners from '../../../../components/sections/miniBanners';
import entryMock from './miniBanners.mock';

jest.mock('../../../../components/sections/withSectionLayout', () => jest.fn((component) => component));

describe('miniBanners', () => {
    it('should render the mini banners section properly', () => {
        const wrapper = shallow(<MiniBanners entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });
});
