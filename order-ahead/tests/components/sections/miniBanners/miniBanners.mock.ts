export default {
    fields: {
        title: 'Get in the game',
        links: [
            {
                sys: { id: 1 },
                fields: {
                    image: {
                        fields: {
                            title: 'Get in the game test 1',
                            description: 'Get in the game test 1',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/4t9MUAEgqxyhP4f3e4AwaS/048d870f934146fd870d1e245596535a/Product_Image_1.png',
                                details: {
                                    size: 281329,
                                    image: {
                                        width: 512,
                                        height: 384,
                                    },
                                },
                                fileName: 'Product Image 1.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    title: 'blazin’ rewards app',
                    secondaryTitle: 'reward your fandom',
                    description:
                        'Download the app for quick access to your orders, easy check-in, rewards push notifications, and faster checkout for future orders.',
                    links: [
                        {
                            fields: {
                                title: 'App Store',
                                url: 'https://apps.apple.com/us/app/youtube-watch-listen-stream/id544007664',
                                image: {
                                    fields: {
                                        title: 'App Store',
                                        file: {
                                            url:
                                                '//images.ctfassets.net/l5fkpck1mwg3/5pdDlXm7E2qtFxBKcGoVsV/4e45a3b4d36d213abf29992502b66853/app-store.png',
                                            details: {
                                                size: 12274,
                                                image: {
                                                    width: 596,
                                                    height: 200,
                                                },
                                            },
                                            fileName: 'app-store.png',
                                            contentType: 'image/png',
                                        },
                                    },
                                },
                            },
                        },
                        {
                            fields: {
                                title: 'Google Play',
                                url:
                                    'https://play.google.com/store/apps/details?id=com.google.android.youtube&hl=en_US&gl=US',
                                image: {
                                    fields: {
                                        title: 'Google Play',
                                        file: {
                                            url:
                                                '//images.ctfassets.net/l5fkpck1mwg3/L070a0hKJq9c4wtEQPL4R/20f2bda5e567cb31b38962339224c156/google-play.png',
                                            details: {
                                                size: 21997,
                                                image: {
                                                    width: 672,
                                                    height: 200,
                                                },
                                            },
                                            fileName: 'google-play.png',
                                            contentType: 'image/png',
                                        },
                                    },
                                },
                            },
                        },
                    ],
                },
            },
            {
                sys: { id: 2 },
                fields: {
                    image: {
                        fields: {
                            title: 'Get in the game test 2',
                            description: 'Get in the game test 2',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/2g8RIux5r83PCk0jaIgI3N/3006ce5bc919e57053b967a5a378162a/Product_Image_1.png',
                                details: {
                                    size: 547644,
                                    image: {
                                        width: 682,
                                        height: 512,
                                    },
                                },
                                fileName: 'Product Image 1.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    title: 'Sports games',
                    secondaryTitle: 'Free-to-play',
                    description: 'Real lines. Real props. Real prizes.',
                    links: [
                        {
                            fields: {
                                name: 'Play Now',
                                nameInUrl: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
                            },
                        },
                    ],
                },
            },
        ],
    },
};
