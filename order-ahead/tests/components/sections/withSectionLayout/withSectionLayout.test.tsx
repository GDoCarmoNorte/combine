import React from 'react';
import { shallow } from 'enzyme';

import withSectionLayout from '../../../../components/sections/withSectionLayout';
import { ISectionComponentProps } from '../../../../components/sections';
import SectionHeader from '../../../../components/atoms/sectionHeader';

const TestComponent = (props: ISectionComponentProps): JSX.Element => <div {...props}>Test Component</div>;

describe('withSectionLayout', () => {
    it('should render without bullet if showBullet undefined', () => {
        const Component = withSectionLayout(TestComponent);
        const wrapper = shallow(<Component entry={{ fields: { name: 'Name' } } as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without bullet', () => {
        const Component = withSectionLayout(TestComponent, { showBullet: false });
        const wrapper = shallow(<Component entry={{ fields: { name: 'Name' } } as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with bullet', () => {
        const Component = withSectionLayout(TestComponent, { showBullet: true });
        const wrapper = shallow(<Component entry={{ fields: { name: 'Name' } } as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without SectionHeader', () => {
        const Component = withSectionLayout(TestComponent, { showBullet: true });
        const wrapper = shallow(<Component entry={{ fields: { name: 'Name', backgroundPosition: 'front' } } as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with configured tag for SectionHeader', () => {
        const Component = withSectionLayout(TestComponent, { headerTag: 'h2' });
        const wrapper = shallow(<Component entry={{ fields: { name: 'Name' } } as any} />);

        expect(wrapper.find(SectionHeader).prop('tag')).toBe('h2');
    });
});
