import { IPromoBlockSection } from '../../../../@generated/@types/contentful';

const promoBlockMock = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '2LOtqEDe7VvpIGFQ6dyt2j',
        type: 'Entry',
        createdAt: '2020-11-24T08:03:01.317Z',
        updatedAt: '2020-11-25T10:07:11.655Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 5,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Promo block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '6ZPYDlqW7gL37WuvjMKz0S',
                    type: 'Entry',
                    createdAt: '2020-11-24T07:53:58.583Z',
                    updatedAt: '2020-11-25T10:01:11.292Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 4,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our new mobile app',
                    description:
                        'Click “download” to receive Arby’s deals, coupons, offers and more! Bring the App to a nearby Arby’s and redeem your next deal.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2rCO17dapCxOPJxDk7jDRA',
                            type: 'Asset',
                            createdAt: '2020-10-09T20:30:48.553Z',
                            updatedAt: '2020-10-09T20:30:48.553Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'LTO Deep Fried Turkey Cranberry Sandwich',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2rCO17dapCxOPJxDk7jDRA/52d5a443152f2c68c6c1e58d9524c164/Website_LTO_DFT_Sandwich_Cranberry.png',
                                details: { size: 3737883, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_LTO_DFT_Sandwich_Cranberry.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '1nmKUqVPUcveqHEbRK9g6J',
                            type: 'Entry',
                            createdAt: '2020-11-24T07:53:22.197Z',
                            updatedAt: '2020-11-24T13:26:53.685Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Download',
                            link: 'mockLink',
                        },
                    },
                },
            },
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '5JvkeBSMmPt4B81urG7irA',
                    type: 'Entry',
                    createdAt: '2020-11-24T08:02:48.276Z',
                    updatedAt: '2020-11-24T13:27:50.284Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 2,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our happy hour',
                    description:
                        'Begins at 2 pm to 5 pm. For just $1, you can get Sliders, Cookies, Small potato cakes, Small curly fries, Small shakes or Small drinks.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '65cmbwDpQ6Jdm4qHpvqMqb',
                            type: 'Asset',
                            createdAt: '2020-10-05T14:53:17.397Z',
                            updatedAt: '2020-10-05T14:53:43.243Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Beverage Minute Maid Zero Sugar',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/65cmbwDpQ6Jdm4qHpvqMqb/e6743df681586a5b9c18ab5c2e07978f/Website_Bev_MM.png',
                                details: { size: 1865607, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Bev_MM.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '4m75M73S6B2oodeDdQMHap',
                            type: 'Entry',
                            createdAt: '2020-11-24T08:02:29.495Z',
                            updatedAt: '2020-11-24T13:25:29.420Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 3,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Start order',
                            link: 'mockLink',
                        },
                    },
                },
            },
        ],
    },
} as unknown) as IPromoBlockSection;

const promoBlockMockWithoutCTA = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '2LOtqEDe7VvpIGFQ6dyt2j',
        type: 'Entry',
        createdAt: '2020-11-24T08:03:01.317Z',
        updatedAt: '2020-11-25T10:07:11.655Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 5,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Promo block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '6ZPYDlqW7gL37WuvjMKz0S',
                    type: 'Entry',
                    createdAt: '2020-11-24T07:53:58.583Z',
                    updatedAt: '2020-11-25T10:01:11.292Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 4,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our new mobile app',
                    description:
                        'Click “download” to receive Arby’s deals, coupons, offers and more! Bring the App to a nearby Arby’s and redeem your next deal.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2rCO17dapCxOPJxDk7jDRA',
                            type: 'Asset',
                            createdAt: '2020-10-09T20:30:48.553Z',
                            updatedAt: '2020-10-09T20:30:48.553Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'LTO Deep Fried Turkey Cranberry Sandwich',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2rCO17dapCxOPJxDk7jDRA/52d5a443152f2c68c6c1e58d9524c164/Website_LTO_DFT_Sandwich_Cranberry.png',
                                details: { size: 3737883, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_LTO_DFT_Sandwich_Cranberry.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                },
            },
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '5JvkeBSMmPt4B81urG7irA',
                    type: 'Entry',
                    createdAt: '2020-11-24T08:02:48.276Z',
                    updatedAt: '2020-11-24T13:27:50.284Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 2,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our happy hour',
                    description:
                        'Begins at 2 pm to 5 pm. For just $1, you can get Sliders, Cookies, Small potato cakes, Small curly fries, Small shakes or Small drinks.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '65cmbwDpQ6Jdm4qHpvqMqb',
                            type: 'Asset',
                            createdAt: '2020-10-05T14:53:17.397Z',
                            updatedAt: '2020-10-05T14:53:43.243Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Beverage Minute Maid Zero Sugar',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/65cmbwDpQ6Jdm4qHpvqMqb/e6743df681586a5b9c18ab5c2e07978f/Website_Bev_MM.png',
                                details: { size: 1865607, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Bev_MM.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                },
            },
        ],
    },
} as unknown) as IPromoBlockSection;

const promoBlockMockWithColoring = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '2LOtqEDe7VvpIGFQ6dyt2j',
        type: 'Entry',
        createdAt: '2020-11-24T08:03:01.317Z',
        updatedAt: '2020-11-25T10:07:11.655Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 5,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Promo block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '6ZPYDlqW7gL37WuvjMKz0S',
                    type: 'Entry',
                    createdAt: '2020-11-24T07:53:58.583Z',
                    updatedAt: '2020-11-25T10:01:11.292Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 4,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our new mobile app',
                    description:
                        'Click “download” to receive Arby’s deals, coupons, offers and more! Bring the App to a nearby Arby’s and redeem your next deal.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2rCO17dapCxOPJxDk7jDRA',
                            type: 'Asset',
                            createdAt: '2020-10-09T20:30:48.553Z',
                            updatedAt: '2020-10-09T20:30:48.553Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'LTO Deep Fried Turkey Cranberry Sandwich',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2rCO17dapCxOPJxDk7jDRA/52d5a443152f2c68c6c1e58d9524c164/Website_LTO_DFT_Sandwich_Cranberry.png',
                                details: { size: 3737883, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_LTO_DFT_Sandwich_Cranberry.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '1nmKUqVPUcveqHEbRK9g6J',
                            type: 'Entry',
                            createdAt: '2020-11-24T07:53:22.197Z',
                            updatedAt: '2020-11-24T13:26:53.685Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Download',
                            link: 'mockLink',
                        },
                    },
                },
            },
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '5JvkeBSMmPt4B81urG7irA',
                    type: 'Entry',
                    createdAt: '2020-11-24T08:02:48.276Z',
                    updatedAt: '2020-11-24T13:27:50.284Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 2,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our happy hour',
                    description:
                        'Begins at 2 pm to 5 pm. For just $1, you can get Sliders, Cookies, Small potato cakes, Small curly fries, Small shakes or Small drinks.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '65cmbwDpQ6Jdm4qHpvqMqb',
                            type: 'Asset',
                            createdAt: '2020-10-05T14:53:17.397Z',
                            updatedAt: '2020-10-05T14:53:43.243Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Beverage Minute Maid Zero Sugar',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/65cmbwDpQ6Jdm4qHpvqMqb/e6743df681586a5b9c18ab5c2e07978f/Website_Bev_MM.png',
                                details: { size: 1865607, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Bev_MM.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '4m75M73S6B2oodeDdQMHap',
                            type: 'Entry',
                            createdAt: '2020-11-24T08:02:29.495Z',
                            updatedAt: '2020-11-24T13:25:29.420Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 3,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Start order',
                            link: 'mockLink',
                        },
                    },
                },
            },
        ],
        backgroundColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3WRLNNeckNZ62pV4d5nly0',
                type: 'Entry',
                createdAt: '2020-08-13T16:02:54.388Z',
                updatedAt: '2020-08-13T16:02:54.388Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'bwwyellow',
                hexColor: 'FFC500',
            },
        },
        titleColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '2xcgnkxTQIPujDHTQSsHWy',
                type: 'Entry',
                createdAt: '2020-08-09T21:39:40.755Z',
                updatedAt: '2020-08-13T15:55:15.442Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'arbysredhover',
                hexColor: 'AC1319',
            },
        },
        descriptionColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3gyI6pWitD9iyEyxeoAUOI',
                type: 'Entry',
                createdAt: '2020-08-09T21:41:18.598Z',
                updatedAt: '2020-08-11T15:06:39.489Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'gray1',
                hexColor: 'FAFBF8',
            },
        },
    },
} as unknown) as IPromoBlockSection;

export const promoBlockMockWithBGImageAndBGColor = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '2LOtqEDe7VvpIGFQ6dyt2j',
        type: 'Entry',
        createdAt: '2020-11-24T08:03:01.317Z',
        updatedAt: '2020-11-25T10:07:11.655Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 5,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Promo block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '6ZPYDlqW7gL37WuvjMKz0S',
                    type: 'Entry',
                    createdAt: '2020-11-24T07:53:58.583Z',
                    updatedAt: '2020-11-25T10:01:11.292Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 4,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our new mobile app',
                    description:
                        'Click “download” to receive Arby’s deals, coupons, offers and more! Bring the App to a nearby Arby’s and redeem your next deal.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2rCO17dapCxOPJxDk7jDRA',
                            type: 'Asset',
                            createdAt: '2020-10-09T20:30:48.553Z',
                            updatedAt: '2020-10-09T20:30:48.553Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'LTO Deep Fried Turkey Cranberry Sandwich',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2rCO17dapCxOPJxDk7jDRA/52d5a443152f2c68c6c1e58d9524c164/Website_LTO_DFT_Sandwich_Cranberry.png',
                                details: { size: 3737883, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_LTO_DFT_Sandwich_Cranberry.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '1nmKUqVPUcveqHEbRK9g6J',
                            type: 'Entry',
                            createdAt: '2020-11-24T07:53:22.197Z',
                            updatedAt: '2020-11-24T13:26:53.685Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Download',
                            link: 'mockLink',
                        },
                    },
                    backgroundImage: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2rCO17dapCxOPJxDk7jDRA',
                            type: 'Asset',
                            createdAt: '2020-10-09T20:30:48.553Z',
                            updatedAt: '2020-10-09T20:30:48.553Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'LTO Deep Fried Turkey Cranberry Sandwich',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2rCO17dapCxOPJxDk7jDRA/52d5a443152f2c68c6c1e58d9524c164/Website_LTO_DFT_Sandwich_Cranberry.png',
                                details: { size: 3737883, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_LTO_DFT_Sandwich_Cranberry.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    backgroundColor: {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '3WRLNNeckNZ62pV4d5nly0',
                            type: 'Entry',
                            createdAt: '2020-08-13T16:02:54.388Z',
                            updatedAt: '2020-08-13T16:02:54.388Z',
                            environment: {
                                sys: {
                                    id: 'DBBP-8575-and-DBBP-8290',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'color',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'bwwyellow',
                            hexColor: 'FFFFFF',
                        },
                    },
                },
            },
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '5JvkeBSMmPt4B81urG7irA',
                    type: 'Entry',
                    createdAt: '2020-11-24T08:02:48.276Z',
                    updatedAt: '2020-11-24T13:27:50.284Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 2,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Our happy hour',
                    description:
                        'Begins at 2 pm to 5 pm. For just $1, you can get Sliders, Cookies, Small potato cakes, Small curly fries, Small shakes or Small drinks.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '65cmbwDpQ6Jdm4qHpvqMqb',
                            type: 'Asset',
                            createdAt: '2020-10-05T14:53:17.397Z',
                            updatedAt: '2020-10-05T14:53:43.243Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 2,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Beverage Minute Maid Zero Sugar',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/65cmbwDpQ6Jdm4qHpvqMqb/e6743df681586a5b9c18ab5c2e07978f/Website_Bev_MM.png',
                                details: { size: 1865607, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Bev_MM.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '4m75M73S6B2oodeDdQMHap',
                            type: 'Entry',
                            createdAt: '2020-11-24T08:02:29.495Z',
                            updatedAt: '2020-11-24T13:25:29.420Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 3,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'promoBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Start order',
                            link: 'mockLink',
                        },
                    },
                },
            },
        ],
        backgroundColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3WRLNNeckNZ62pV4d5nly0',
                type: 'Entry',
                createdAt: '2020-08-13T16:02:54.388Z',
                updatedAt: '2020-08-13T16:02:54.388Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'bwwyellow',
                hexColor: 'FFC500',
            },
        },
    },
} as unknown) as IPromoBlockSection;

export { promoBlockMock as default, promoBlockMockWithoutCTA, promoBlockMockWithColoring };
