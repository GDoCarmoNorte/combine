import React from 'react';
import { render } from '@testing-library/react';

import PromoBlock from '../../../../components/sections/promoBlock';
import promoBlockMock, {
    promoBlockMockWithoutCTA,
    promoBlockMockWithColoring,
    promoBlockMockWithBGImageAndBGColor,
} from './promoBlock.mock';

jest.mock('../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));

describe('PromoBlock', () => {
    it('should render with cta', () => {
        const { container } = render(<PromoBlock entry={promoBlockMock} />);

        expect(container).toMatchSnapshot();
    });

    it('should render without cta', () => {
        const { container } = render(<PromoBlock entry={promoBlockMockWithoutCTA} />);

        expect(container).toMatchSnapshot();
    });

    it('should render with configured coloring', () => {
        const { container } = render(<PromoBlock entry={promoBlockMockWithColoring} />);

        expect(container).toMatchSnapshot();
    });

    it('should render with configured image and background for card', () => {
        const { container } = render(<PromoBlock entry={promoBlockMockWithBGImageAndBGColor} />);

        expect(container).toMatchSnapshot();
    });
});
