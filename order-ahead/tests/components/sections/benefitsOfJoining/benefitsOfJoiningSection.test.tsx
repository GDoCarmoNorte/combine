import React from 'react';
import BenefitsOfJoiningSection from '../../../../components/sections/benefitsOfJoiningSection/benefitsOfJoiningSection';
import { render } from '@testing-library/react';
import BenefitsOfJoiningSectionMock from './benefitsOfJoining.mock';

describe('BenefitsOfJoining section', () => {
    it('should render correctly', () => {
        const { container } = render(<BenefitsOfJoiningSection entry={BenefitsOfJoiningSectionMock as any} />);
        expect(container).toMatchSnapshot();
    });
});
