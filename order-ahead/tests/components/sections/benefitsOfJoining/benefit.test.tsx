import React from 'react';
import { render } from '@testing-library/react';
import BenefitItem from '../../../../components/sections/benefitsOfJoiningSection/benefit';
import BenefitsOfJoiningSectionMock from './benefitsOfJoining.mock';

describe('Benefit Item module', () => {
    it('should render correctly with provided data', () => {
        const { container } = render(<BenefitItem item={BenefitsOfJoiningSectionMock.fields.benefits[0] as any} />);
        expect(container).toMatchSnapshot();
    });
});
