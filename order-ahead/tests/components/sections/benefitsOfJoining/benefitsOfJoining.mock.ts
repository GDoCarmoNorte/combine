export default {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '2xAlyTv2v3UmnrDZrXuXOP',
        type: 'Entry',
        createdAt: '2021-08-08T06:10:24.681Z',
        updatedAt: '2021-08-08T06:10:24.681Z',
        environment: {
            sys: {
                id: 'feature-feature-DBBP-38934',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'benefitsOfJoining',
            },
        },
        locale: 'en-US',
    },
    fields: {
        title: 'Benefits of joining',
        image: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '7DGP5iOIYjnT3Yiy4txCJ7',
                type: 'Asset',
                createdAt: '2021-08-08T06:04:48.880Z',
                updatedAt: '2021-08-08T09:28:51.175Z',
                environment: {
                    sys: {
                        id: 'feature-feature-DBBP-38934',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                locale: 'en-US',
            },
            fields: {
                title: 'Benefits Icon',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/7DGP5iOIYjnT3Yiy4txCJ7/65ed6044df33e7713fe69093028b8fe8/Frame.svg',
                    details: {
                        size: 3744,
                        image: {
                            width: 104,
                            height: 56,
                        },
                    },
                    fileName: 'Frame.svg',
                    contentType: 'image/svg+xml',
                },
            },
        },
        benefits: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '2jjuKgqSLYsY3fKeE4sVxG',
                    type: 'Entry',
                    createdAt: '2021-08-08T06:09:32.135Z',
                    updatedAt: '2021-08-08T06:09:32.135Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-38934',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'benefit',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Win epic Prizes',
                    description: 'Get VIP experiences, tickets, and free wings for your birthday\n',
                    icon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '5QpEsWf5JxVJz03XCMnzUY',
                            type: 'Asset',
                            createdAt: '2021-08-08T06:06:04.756Z',
                            updatedAt: '2021-08-08T06:06:04.756Z',
                            environment: {
                                sys: {
                                    id: 'feature-feature-DBBP-38934',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'star',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/5QpEsWf5JxVJz03XCMnzUY/fd8e255ded0bd48f40d20031498ea941/Vector.svg',
                                details: {
                                    size: 286,
                                    image: {
                                        width: 35,
                                        height: 32,
                                    },
                                },
                                fileName: 'Vector.svg',
                                contentType: 'image/svg+xml',
                            },
                        },
                    },
                },
            },
        ],
        secondaryTitle: 'Earn points',
        secondarySubTitle: 'for loving wings',
        secondaryDescription:
            "Score rewards for doing more of what you're good at: crushing wings and watching sports with the crew. Sign-up, earn points, and get rewarded.\n",
    },
};
