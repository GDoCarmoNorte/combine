import React from 'react';
import { shallow } from 'enzyme';
import { useFormikContext } from 'formik';

import EmailSignup from '../../../../components/sections/emailSignup';
import LocationField from '../../../../components/sections/emailSignup/locationField';
import { useOrderLocation } from '../../../../redux/hooks';

import emailSignupMock from './emailSignup.mock';
import locationsMock from '../../../mocks/expLocations.mock';

import { EmailSignupState } from '../../../../components/sections/emailSignup/types';
import useEmailSignup from '../../../../components/sections/emailSignup/useEmailSignup';

jest.mock('formik');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../components/sections/emailSignup/useEmailSignup');

(useOrderLocation as jest.Mock).mockReturnValue({
    currentLocation: locationsMock.locations[0],
    actions: { setLocation: jest.fn() },
});
(useEmailSignup as jest.Mock).mockReturnValue({
    signupState: EmailSignupState.SIGNUP_FORM_SHOWED,
    onFormSubmit: jest.fn(),
});

describe('SignUpForm component', () => {
    it('should render signup form', () => {
        const wrapper = shallow(<EmailSignup entry={emailSignupMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render success message', () => {
        (useEmailSignup as jest.Mock).mockReturnValueOnce({
            signupState: EmailSignupState.SUCCESSFULLY_SUBMITTED,
            onFormSubmit: jest.fn(),
        });
        const wrapper = shallow(<EmailSignup entry={emailSignupMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render duplicate email message', () => {
        (useEmailSignup as jest.Mock).mockReturnValueOnce({
            signupState: EmailSignupState.DUPLICATE_EMAIL_ERROR,
            onFormSubmit: jest.fn(),
        });
        const wrapper = shallow(<EmailSignup entry={emailSignupMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the LocationField', () => {
        (useFormikContext as jest.Mock).mockReturnValueOnce({
            values: {
                location: undefined,
                zipCode: '',
            },
            errors: {},
            touched: {},
            setFieldValue: jest.fn(),
            setFieldTouched: jest.fn(),
            registerField: jest.fn(),
            unregisterField: jest.fn(),
            validateForm: jest.fn(),
        });
        const wrapper = shallow(<LocationField />);

        expect(wrapper).toMatchSnapshot();
    });
});
