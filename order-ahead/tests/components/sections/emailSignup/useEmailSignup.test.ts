import { renderHook, act } from '@testing-library/react-hooks';
import useEmailSignup from '../../../../components/sections/emailSignup/useEmailSignup';
import { EmailSignupState } from '../../../../components/sections/emailSignup/types';
import { signUp } from '../../../../common/services/customerService';
import { useDispatch } from 'react-redux';
import { GTM_EMAIL_SIGN_UP } from '../../../../common/services/gtmService/constants';
import { useNotifications } from '../../../../redux/hooks';
import locationsMock from '../../../mocks/expLocations.mock';

jest.mock('react-redux');
jest.mock('../../../../common/services/customerService');
jest.mock('../../../../redux/hooks');

describe('useEmailSignup', () => {
    const dispatchMock = jest.fn();
    const enqueueErrorMock = jest.fn();
    const signupFormValuesMock = {
        firstName: 'firstName',
        lastName: 'lastName',
        email: 'email',
        zipCode: 'zipCode',
        location: locationsMock.locations[0],
        dateOfBirth: 'dateOfBirth',
    };

    (useDispatch as jest.Mock).mockReturnValue(dispatchMock);
    (useNotifications as jest.Mock).mockReturnValue({ actions: { enqueueError: enqueueErrorMock } });
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should return onFormSubmit handler and initial signup state', () => {
        const { result } = renderHook(() => useEmailSignup());

        expect(result.current).toEqual({
            signupState: EmailSignupState.SIGNUP_FORM_SHOWED,
            onFormSubmit: expect.any(Function),
        });
    });

    it('should successfully submit signup form and fire gtm event', async () => {
        (signUp as jest.Mock).mockImplementationOnce(() => new Promise((res) => res(true)));
        const { result } = renderHook(() => useEmailSignup());

        await act(async () => {
            await result.current.onFormSubmit(signupFormValuesMock as any);
        });

        expect(result.current.signupState).toBe(EmailSignupState.SUCCESSFULLY_SUBMITTED);
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_EMAIL_SIGN_UP });
    });

    it('should enqueue an error if submit is not successfull', async () => {
        (signUp as jest.Mock).mockImplementationOnce(() => new Promise((_, rej) => rej({ status: 400 })));
        const { result } = renderHook(() => useEmailSignup());

        await act(async () => {
            await result.current.onFormSubmit(signupFormValuesMock as any);
        });

        expect(result.current.signupState).toBe(EmailSignupState.SIGNUP_FORM_SHOWED);
        expect(enqueueErrorMock).toHaveBeenCalledWith({ message: 'Failed sign up to updates' });
        expect(dispatchMock).not.toHaveBeenCalledWith();
    });

    it('should show duplicate email error', async () => {
        (signUp as jest.Mock).mockImplementationOnce(
            () => new Promise((_, rej) => rej({ status: 400, errors: [{ reasonCode: 'CDS_MKTG_PREF_OPTED_IN' }] }))
        );
        const { result } = renderHook(() => useEmailSignup());

        await act(async () => {
            await result.current.onFormSubmit(signupFormValuesMock as any);
        });

        expect(result.current.signupState).toBe(EmailSignupState.DUPLICATE_EMAIL_ERROR);
        expect(dispatchMock).not.toHaveBeenCalled();
        expect(enqueueErrorMock).not.toHaveBeenCalled();
    });
});
