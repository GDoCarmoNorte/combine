import isEmailAlreadyRegistered from '../../../../components/sections/emailSignup/isEmailAlreadyRegistered';

describe('isEmailAlreadyRegistered', () => {
    it('should return false if no errors object is present', () => {
        const result = isEmailAlreadyRegistered({});

        expect(result).toEqual(false);
    });

    it('should return false if reason code is not CDS_MKTG_PREF_OPTED_IN', () => {
        const result = isEmailAlreadyRegistered({
            errors: [
                {
                    code: 'C10003',
                    message: 'Request has been rejected.',
                    source: 'CustomerDomainService',
                    brand: 'ARB',
                    reasonCode: 'CDS_BAD_REQUEST',
                    severity: 1,
                },
            ],
        });

        expect(result).toEqual(false);
    });

    it('should return false if reason code is CDS_MKTG_PREF_OPTED_IN', () => {
        const result = isEmailAlreadyRegistered({
            errors: [
                {
                    code: 'C10001',
                    message: 'Customer already Opted in for Marketing Promotions.',
                    source: 'CustomerDomainService',
                    brand: 'ARB',
                    reasonCode: 'CDS_MKTG_PREF_OPTED_IN',
                    severity: 0,
                },
            ],
        });

        expect(result).toEqual(true);
    });
});
