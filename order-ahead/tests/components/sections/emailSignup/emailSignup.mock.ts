export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '5hvpSDrwYZW5dTY5Gqh0Kf',
        type: 'Entry',
        createdAt: '2020-12-08T07:54:09.377Z',
        updatedAt: '2020-12-11T09:50:15.127Z',
        environment: {
            sys: {
                id: 'DBBP-9655',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 7,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'emailSignup',
            },
        },
        locale: 'en-US',
    },
    fields: {
        header: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3kv71GOmojsW724yAXi995',
                type: 'Entry',
                createdAt: '2020-12-08T07:54:03.412Z',
                updatedAt: '2020-12-08T09:26:46.427Z',
                environment: {
                    sys: {
                        id: 'DBBP-9655',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 8,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'infoBanner',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                title: "Sign up for the arby's email list",
                header: 'Get fries & a soft drink',
                description: 'With any Signatue sandwich purchase',
                headerTextColor: '#ffffff',
            },
        },
        formTitle: 'Email sign up form',
        formFooterText:
            'I understand that my information will be used as described in the TERMS & CONDITIONS and PRIVACY POLICY.',
        formLinkText: 'SIGN UP ',
        legalText:
            'Message and data rates may apply. Text STOP to 27297 to cancel (confirmation text will be sent) or email websupport@arbys.com with your request and phone number. Text HELP for help. "Coca-Cola" is a registered trademark of The Coca-Cola Company.',
        successMessageIcon: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '4dFwfbpVlGAazMUit8P29J',
                type: 'Asset',
                createdAt: '2020-11-11T10:14:47.820Z',
                updatedAt: '2020-11-11T10:14:47.820Z',
                environment: {
                    sys: {
                        id: 'DBBP-9655',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'Ring the Bell',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/4dFwfbpVlGAazMUit8P29J/b9ce75c167f0754457410deb8aaa4088/Noun_Project_1156969.svg',
                    details: {
                        size: 3846,
                        image: {
                            width: 68,
                            height: 68,
                        },
                    },
                    fileName: 'Noun Project 1156969.svg',
                    contentType: 'image/svg+xml',
                },
            },
        },
        successMessageHeaderText: 'Good Move',
        successMessageText: 'Tasty deals are on the way! You will be receiving a confirmation email from us soon.',
    },
};
