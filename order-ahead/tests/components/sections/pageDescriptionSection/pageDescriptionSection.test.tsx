import React from 'react';
import { render, screen } from '@testing-library/react';
import { pageDescriptionSectionMock } from './pageDescriptionSection.mock';
import PageDescriptionSection from '../../../../components/sections/pageDescriptionSection';

describe('Page description component', () => {
    it('should render correctly', () => {
        render(<PageDescriptionSection entry={pageDescriptionSectionMock as any} />);

        //render header
        expect(screen.getByText(/about this location/i)).toBeInTheDocument();

        //render description
        expect(screen.getByText(/Nestled conveniently next to the City Center Plaza/i)).toBeInTheDocument();
    });

    it('should render description from DS if provided', () => {
        const descriptionMock = 'test description';
        render(
            <PageDescriptionSection entry={pageDescriptionSectionMock as any} data={{ description: descriptionMock }} />
        );
        //render description
        expect(screen.getByText(descriptionMock)).toBeInTheDocument();
        expect(screen.queryByText(/Nestled conveniently next to the City Center Plaza/i)).not.toBeInTheDocument();
    });
});
