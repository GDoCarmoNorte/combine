export const pageDescriptionSectionMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '5UkOllUPOAaeIt2uo141YZ',
        type: 'Entry',
        createdAt: '2021-09-20T10:28:35.208Z',
        updatedAt: '2021-09-20T10:55:17.809Z',
        environment: {
            sys: {
                id: 'rio-feature-DBBP-40893-Loc-info',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 2,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'descriptionSection',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'about this location',
        description:
            'Nestled conveniently next to the City Center Plaza, Buffalo Wild Wings on Washington Ave in Houston provides opportunities for sports fans of all types to celebrate their team, their way.  Whether partaking of a Broncos or Steelheads game in person, or watching fights in bar, you take care of the celebrating, and we’ll take care of the grub.\n\nVisit to partake in a fan expereince like no other with our weekly Trivia Nights, world-famous Blazin’ Challenge, and BOGOs every Tuesday and Thursday. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        image: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3+',
                    },
                },
                id: 'yOte9WKSJS6FqtzExu2Dc',
                type: 'Asset',
                createdAt: '2021-08-13T13:13:56.872Z',
                updatedAt: '2021-09-08T08:10:02.155Z',
                environment: {
                    sys: {
                        id: 'rio-feature-DBBP-40893-Loc-info',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                locale: 'en-US',
            },
            fields: {
                title: 'Get in the game test 1',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/yOte9WKSJS6FqtzExu2Dc/c9758a7d371c5cf2fd10b1cd0fffeba6/rewards1.png',
                    details: {
                        size: 180550,
                        image: {
                            width: 397,
                            height: 298,
                        },
                    },
                    fileName: 'rewards1.png',
                    contentType: 'image/png',
                },
            },
        },
        imageLeftAligned: true,
    },
};
