import React from 'react';
import { render } from '@testing-library/react';
import DineTimeWidget from '../../../../components/sections/dineTime/dineTime';
import { locationDetailsMock } from '../../organisms/locationDetailsPageContent/locationDetailsPageLayout.mock';
import { dineTimeMock } from './dineTime.mock';
import { injectScript } from '../../../../lib/injectScriptOnce';
import { isDineTimeEnabled } from '../../../../lib/getFeatureFlags';

const injectScriptMock = jest.fn();

jest.mock('../../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isDineTimeEnabled: jest.fn().mockReturnValue(true),
    };
});

jest.mock('../../../../lib/injectScriptOnce');

describe('DineTime widget', () => {
    beforeEach(() => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (injectScript as jest.Mock) = injectScriptMock;
    });
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should embed correct script if CMS setup is provided', () => {
        render(<DineTimeWidget data={{ locationDetails: locationDetailsMock }} entry={dineTimeMock as any} />);

        expect(injectScriptMock).toHaveBeenCalledTimes(1);
        expect(injectScriptMock).toHaveBeenCalledWith('https://widget.qsr.cloud/dist/embed/embed.js', {
            current: document.createElement('div'),
        });
    });

    it('should not try to embed script if CMS setup is not provided for the store', () => {
        const locationDetailstestSpecificMock = {
            ...locationDetailsMock,
            id: '123',
        };

        render(
            <DineTimeWidget data={{ locationDetails: locationDetailstestSpecificMock }} entry={dineTimeMock as any} />
        );

        expect(injectScriptMock).toHaveBeenCalledTimes(0);
    });
    it('should not try to embed script if location id is not defined', () => {
        const locationDetailstestSpecificMock = {
            ...locationDetailsMock,
            id: undefined,
        };
        render(
            <DineTimeWidget data={{ locationDetails: locationDetailstestSpecificMock }} entry={dineTimeMock as any} />
        );

        expect(injectScriptMock).toHaveBeenCalledTimes(0);
    });

    it('should not try to embed script if dineTime feature is disabled', () => {
        (isDineTimeEnabled as jest.Mock).mockReturnValue(false);
        render(<DineTimeWidget data={{ locationDetails: locationDetailsMock }} entry={dineTimeMock as any} />);

        expect(injectScriptMock).toHaveBeenCalledTimes(0);
    });
});
