export const dineTimeMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '1td2jdlleRuZ09BFAFwj0J',
        type: 'Entry',
        createdAt: '2021-10-23T07:39:05.478Z',
        updatedAt: '2021-10-23T07:39:05.478Z',
        environment: {
            sys: {
                id: 'otdt-feature-DBBP-31335',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'dineTime',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Dine Time',
        locationsMapping: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6yAxq8utBv4i7ZIQZ5xbCt',
                    type: 'Entry',
                    createdAt: '2021-10-23T07:38:57.868Z',
                    updatedAt: '2021-10-23T07:38:57.868Z',
                    environment: {
                        sys: {
                            id: 'otdt-feature-DBBP-31335',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'locationMapping',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    idpLocationId: '12',
                    externalLocationId: '2ce4e537-9adc-4e18-81b8-ffdc27facc25',
                },
            },
        ],
    },
};
