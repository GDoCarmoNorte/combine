import React from 'react';
import { render, screen } from '@testing-library/react';
import LocationNewsSection from '../../../../components/sections/locationNewsSection';
import { locationNewsSectionMock, locationNewsSectioWithoutTilesnMock } from './locationNewsSection.mock';

describe('LocationNews component', () => {
    it('should render correctly', () => {
        render(<LocationNewsSection entry={locationNewsSectionMock as any} />);

        //render header
        expect(screen.getByText(/GO LOCATION/i)).toBeInTheDocument();

        //render description
        expect(screen.getByText(/Some days/i)).toBeInTheDocument();

        //render tiles
        expect(screen.getByAltText(/Quick reorder/i)).toBeInTheDocument();
        expect(screen.getByAltText(/reward-flame/i)).toBeInTheDocument();
        expect(screen.getByAltText(/profile-icon/i)).toBeInTheDocument();
        expect(screen.getByAltText(/Empty Bag Icon/i)).toBeInTheDocument();
    });

    it('should not render tiles if not provided', () => {
        render(<LocationNewsSection entry={locationNewsSectioWithoutTilesnMock as any} />);

        //render header
        expect(screen.getByText(/GO LOCATION/i)).toBeInTheDocument();

        //render description
        expect(screen.getByText(/Some days/i)).toBeInTheDocument();

        //not render tiles
        expect(screen.queryByAltText(/Quick reorder/i)).not.toBeInTheDocument();
        expect(screen.queryByAltText(/reward-flame/i)).not.toBeInTheDocument();
        expect(screen.queryByAltText(/Union/i)).not.toBeInTheDocument();
        expect(screen.queryByAltText(/Empty Bag Icon/i)).not.toBeInTheDocument();
    });
    it('should not render description if not provided', () => {
        const locationMock = {
            ...locationNewsSectioWithoutTilesnMock,
            fields: {
                ...locationNewsSectioWithoutTilesnMock.fields,
                description: undefined,
            },
        };
        render(<LocationNewsSection entry={locationMock as any} />);
        expect(screen.queryByText(/Some days/i)).not.toBeInTheDocument();
    });
});
