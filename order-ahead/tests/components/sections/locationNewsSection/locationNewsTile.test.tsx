import React from 'react';
import { render, screen } from '@testing-library/react';
import LocationNewsTile from '../../../../components/sections/locationNewsSection/locationNewsTile';
import { locationNewsSectionMock } from './locationNewsSection.mock';

describe('LocationNewsTile component', () => {
    const {
        fields: { header, description, icon },
    } = locationNewsSectionMock.fields.tiles[0];

    it('should render correctly', () => {
        render(<LocationNewsTile header={header} description={description} icon={icon as any} />);

        //render header
        expect(screen.getByText(/Ways to order/i)).toBeInTheDocument();

        //render description
        expect(screen.getByText(/Order online/i)).toBeInTheDocument();

        //render icon
        expect(screen.getByAltText(/Quick reorder/i)).toBeInTheDocument();
    });

    it('should not render description if not provided', () => {
        render(<LocationNewsTile header={header} icon={icon as any} />);

        expect(screen.getByText(/Ways to order/i)).toBeInTheDocument();
        expect(screen.queryByText(/Order online/i)).not.toBeInTheDocument();
        expect(screen.getByAltText(/Quick reorder/i)).toBeInTheDocument();
    });
    it('should not render header if not provided', () => {
        render(<LocationNewsTile icon={icon as any} />);

        expect(screen.queryByText(/Ways to order/i)).not.toBeInTheDocument();
    });
    it('should not render icon if not provided', () => {
        render(<LocationNewsTile />);

        expect(screen.queryByAltText(/Quick reorder/i)).not.toBeInTheDocument();
    });
});
