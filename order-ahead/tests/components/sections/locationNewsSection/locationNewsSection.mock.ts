export const locationNewsSectionMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '016Gxb8Lfh7fzOIEX84QYQ',
        type: 'Entry',
        createdAt: '2021-09-13T08:54:25.545Z',
        updatedAt: '2021-09-13T08:54:25.545Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'locationNewsSection',
            },
        },
        locale: 'en-US',
    },
    fields: {
        header: 'go location',
        description:
            'Some days, you just need wings fast on the go – GO brings Buffalo Wild Wings closer to you so you can get your wings fix even on days you can’t fully escape to the sports bar. All your favorite wings and sauces, plus other fan favorites available at GO, so you can keep moving.',
        type: 'GO',
        tiles: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '365bUx3XFwfi8OPBnmNtMb',
                    type: 'Entry',
                    createdAt: '2021-09-13T08:51:11.460Z',
                    updatedAt: '2021-09-13T08:51:11.460Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'locationNewsTile',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    header: 'Ways to order',
                    description:
                        'Order online, in the app, or at the counter. For the best offers customized to you, be sure to sign up for our Blazin Rewards® loyalty program.',
                    icon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '5rCwQV990Ggdba3CLbCkPU',
                            type: 'Asset',
                            createdAt: '2021-08-03T10:31:31.289Z',
                            updatedAt: '2021-08-03T10:31:31.289Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Quick reorder',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/5rCwQV990Ggdba3CLbCkPU/380b616dab7ac559f28e49c2d4bfb1af/Group.svg',
                                details: {
                                    size: 1048,
                                    image: {
                                        width: 44,
                                        height: 60,
                                    },
                                },
                                fileName: 'Group.svg',
                                contentType: 'image/svg+xml',
                            },
                        },
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '3MlX7Nk3i2S5CUPPMqlEV9',
                    type: 'Entry',
                    createdAt: '2021-09-13T08:52:19.841Z',
                    updatedAt: '2021-09-13T08:52:19.841Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'locationNewsTile',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    header: 'great deals',
                    description:
                        'Buffalo Wild Wings GO offers great deals all day every day, no matter what time of day or  day of week.',
                    icon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '3l3EJV5TQXyHlwIZKRAVo8',
                            type: 'Asset',
                            createdAt: '2021-08-05T11:41:48.923Z',
                            updatedAt: '2021-08-05T11:41:48.923Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'profile-icon',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/3l3EJV5TQXyHlwIZKRAVo8/faf76ecd0afc0dbf43d1c0f589fa3e07/profile.svg',
                                details: {
                                    size: 1605,
                                    image: {
                                        width: 54,
                                        height: 60,
                                    },
                                },
                                fileName: 'profile.svg',
                                contentType: 'image/svg+xml',
                            },
                        },
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: 'QXpS67i5WZaNRfu2LdqEK',
                    type: 'Entry',
                    createdAt: '2021-09-13T08:53:29.630Z',
                    updatedAt: '2021-09-13T08:53:29.630Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'locationNewsTile',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    header: 'heated lockers',
                    description:
                        ' At GO, your order will be assigned a heated takeout locker, so your food is hot and fresh when you’re ready for it.',
                    icon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '5rCwQV990Ggdba3CLbCkPU',
                            type: 'Asset',
                            createdAt: '2021-08-03T10:31:31.289Z',
                            updatedAt: '2021-08-03T10:31:31.289Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'reward-flame',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/5rCwQV990Ggdba3CLbCkPU/380b616dab7ac559f28e49c2d4bfb1af/Group.svg',
                                details: {
                                    size: 1048,
                                    image: {
                                        width: 44,
                                        height: 60,
                                    },
                                },
                                fileName: 'Group.svg',
                                contentType: 'image/svg+xml',
                            },
                        },
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6t5PLQgDNt2d6eyZid91sN',
                    type: 'Entry',
                    createdAt: '2021-09-13T08:54:20.225Z',
                    updatedAt: '2021-09-13T08:54:20.225Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'locationNewsTile',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    header: 'Contact free pickup',
                    description:
                        'Order and pay ahead on our app, find your name  and locker number on the digital overhead board, grab, and GO!',
                    icon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '5rCwQV990Ggdba3CLbCkPU',
                            type: 'Asset',
                            createdAt: '2021-08-03T10:31:31.289Z',
                            updatedAt: '2021-08-03T10:31:31.289Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Empty Bag Icon',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/5rCwQV990Ggdba3CLbCkPU/380b616dab7ac559f28e49c2d4bfb1af/Group.svg',
                                details: {
                                    size: 1048,
                                    image: {
                                        width: 44,
                                        height: 60,
                                    },
                                },
                                fileName: 'Group.svg',
                                contentType: 'image/svg+xml',
                            },
                        },
                    },
                },
            },
        ],
    },
};

export const locationNewsSectioWithoutTilesnMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '016Gxb8Lfh7fzOIEX84QYQ',
        type: 'Entry',
        createdAt: '2021-09-13T08:54:25.545Z',
        updatedAt: '2021-09-13T08:54:25.545Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'locationNewsSection',
            },
        },
        locale: 'en-US',
    },
    fields: {
        header: 'go location',
        description:
            'Some days, you just need wings fast on the go – GO brings Buffalo Wild Wings closer to you so you can get your wings fix even on days you can’t fully escape to the sports bar. All your favorite wings and sauces, plus other fan favorites available at GO, so you can keep moving.',
        type: 'GO',
    },
};
