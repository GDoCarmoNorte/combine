import React from 'react';
import { shallow } from 'enzyme';

import SingleImageWithOverlay from '../../../../components/sections/singleImageWithOverlay';
import entryMock from './singleImageWithOverlay.mock';

describe('singleImageWithOverlay', () => {
    it('should render the single image with overlay properly', () => {
        const wrapper = shallow(<SingleImageWithOverlay entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the single image with overlay without info block', () => {
        const updatedNock = {
            fields: {
                ...entryMock.fields,
                textBlock1: undefined,
                textBlock1Value: undefined,
                textBlock2: undefined,
                textBlock2Value: undefined,
            },
        };

        const wrapper = shallow(<SingleImageWithOverlay entry={updatedNock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the single image with overlay with text block 1 only', () => {
        const updatedNock = {
            fields: {
                ...entryMock.fields,
                textBlock2: undefined,
                textBlock2Value: undefined,
            },
        };

        const wrapper = shallow(<SingleImageWithOverlay entry={updatedNock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the single image with overlay with text block 2 only', () => {
        const updatedNock = {
            fields: {
                ...entryMock.fields,
                textBlock1: undefined,
                textBlock1Value: undefined,
            },
        };

        const wrapper = shallow(<SingleImageWithOverlay entry={updatedNock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the single image with overlay without button', () => {
        const updatedNock = {
            fields: {
                ...entryMock.fields,
                buttonCta: undefined,
            },
        };

        const wrapper = shallow(<SingleImageWithOverlay entry={updatedNock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the single image with overlay without phone', () => {
        const updatedNock = {
            fields: {
                ...entryMock.fields,
                phone: undefined,
            },
        };

        const wrapper = shallow(<SingleImageWithOverlay entry={updatedNock as any} />);

        expect(wrapper).toMatchSnapshot();
    });
});
