export default {
    fields: {
        header: 'Contact Us',
        contentHeader: 'We Make it Right',
        contentDescription:
            'Comments, questions or need information? Whatever your reason for contacting us, we are committed to ensuring you receive the service you deserve. Here at Arby’s, if it matters to you, it matters to us. ',
        textBlock1: 'service hours:',
        textBlock1Value: '07:00AM - 11:00PM',
        textBlock2: 'Working days:',
        textBlock2Value: 'Monday - Friday',
        buttonCta: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '6mHrrizPDiAhZs0yxauPuI',
                type: 'Entry',
                createdAt: '2020-10-26T11:20:58.054Z',
                updatedAt: '2020-10-26T11:20:58.055Z',
                environment: {
                    sys: {
                        id: 'DBBP-608',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'externalLink',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'send a message',
                nameInUrl: 'https://commentform.marketforce.com/commentform/Arbys.aspx',
            },
        },
        phone: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '7TtsSv7nVsJH5Z2z4pYW0',
                type: 'Entry',
                createdAt: '2020-10-26T11:22:10.498Z',
                updatedAt: '2020-10-26T11:22:10.498Z',
                environment: {
                    sys: {
                        id: 'DBBP-608',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'phoneNumberLink',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: '1 800 599 2729',
                phoneNumber: '1 800 599 2729',
            },
        },
        image: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '79BWpdA9Aig7GYTT93BWMl',
                type: 'Asset',
                createdAt: '2020-10-26T11:24:27.837Z',
                updatedAt: '2020-10-26T11:24:27.837Z',
                environment: {
                    sys: {
                        id: 'DBBP-608',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'contact us',
                description: 'contact us',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/79BWpdA9Aig7GYTT93BWMl/f1456cbfa46aa73c9514786982702759/No_Image_Available_Desktop_1.png',
                    details: {
                        size: 445067,
                        image: {
                            width: 658,
                            height: 494,
                        },
                    },
                    fileName: 'No Image Available Desktop 1.png',
                    contentType: 'image/png',
                },
            },
        },
        imageLeftAligned: false,
    },
};
