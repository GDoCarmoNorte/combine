import React from 'react';
import { shallow } from 'enzyme';
import MyDealsEmpty from '../../../../components/sections/myDealsEmpty';

import fieldsMock from './myDealsEmpty.mock';

describe('MyDealsEmpty section', () => {
    it('should render MyDealsEmpty section correctly', () => {
        const component = shallow(<MyDealsEmpty fields={fieldsMock as any} />);

        expect(component).toMatchSnapshot();
    });
});
