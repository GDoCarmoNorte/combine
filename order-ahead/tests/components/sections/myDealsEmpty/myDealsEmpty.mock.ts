export default {
    title: 'Check again soon',
    topImage: {
        metadata: {
            tags: [],
        },
        sys: {
            space: {
                sys: {
                    type: 'Link',
                    linkType: 'Space',
                    id: 'o19mhvm9a2cm',
                },
            },
            id: '6iR4jbIhWJx84ZlfSX3ds0',
            type: 'Asset',
            createdAt: '2021-05-26T13:21:19.252Z',
            updatedAt: '2021-05-26T13:21:19.252Z',
            environment: {
                sys: {
                    id: 'DBBP-29641-acc-deals-empty',
                    type: 'Link',
                    linkType: 'Environment',
                },
            },
            revision: 1,
            locale: 'en-US',
        },
        fields: {
            title: 'deals empty image',
            file: {
                url:
                    '//images.ctfassets.net/o19mhvm9a2cm/6iR4jbIhWJx84ZlfSX3ds0/e966c132998f81a8d4a461126a9cd00b/_q_gift_i_3343288.png',
                details: {
                    size: 1611,
                    image: {
                        width: 77,
                        height: 76,
                    },
                },
                fileName: '_q=gift&i=3343288.png',
                contentType: 'image/png',
            },
        },
    },
    mainText:
        'We know you love deals, but you haven’t added any items to your bag yet. Start a new order and start earning deals.',
    ctaText: 'start order',
};
