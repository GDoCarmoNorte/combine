import { shallow } from 'enzyme';
import React from 'react';

import { PageContentWrapper } from '../../../../components/sections/PageContentWrapper';

describe('PageContentWrapper', () => {
    it('should render PageContentWrapper correctly', () => {
        const container = shallow(
            <PageContentWrapper>
                <div>children</div>
            </PageContentWrapper>
        );

        expect(container).toMatchSnapshot();
    });
});
