import React from 'react';
import { shallow } from 'enzyme';

import PaddingSection from '../../../../components/sections/paddingSection';

describe('PaddingSection', () => {
    it('should render simple padding', () => {
        const mockEntry: any = { fields: { height: 50 } };
        const wrapper = shallow(<PaddingSection entry={mockEntry} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render padding with bg color', () => {
        const mockEntry: any = { fields: { height: 50, backgroundColor: 'F00' } };
        const wrapper = shallow(<PaddingSection entry={mockEntry} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render padding with bg image', () => {
        const mockEntry: any = {
            fields: {
                height: 50,
                backgroundImage: { fields: { file: { url: './image-url.jpg' } } },
            },
        };
        const wrapper = shallow(<PaddingSection entry={mockEntry} />);

        expect(wrapper).toMatchSnapshot();
    });

    // make sense when image has transparent layer
    it('should render padding with bg image and bg color', () => {
        const mockEntry: any = {
            fields: {
                height: 50,
                backgroundColor: '000',
                backgroundImage: { fields: { file: { url: './image-url.jpg' } } },
            },
        };
        const wrapper = shallow(<PaddingSection entry={mockEntry} />);

        expect(wrapper).toMatchSnapshot();
    });
});
