export const productBannerMock = {
    topIcon: 'topIconMock',
    topText: 'Limited Time',
    mainText: 'Alternative text',
    mainTextLink: 'mainTextLinkMock',
    bottomText: 'bottom text',
    rightImage: 'rightImageMock',
    rightImageLink: 'rightImageLinkMock',
    productLink: {
        fields: {
            name: "Double Beef 'N Cheddar",
            nameInUrl: 'double-beef-n-cheddar',
            image: 'productLinkImageMock',
            productId: 'ARBYS-WEBOA-640239548',
        },
    },
    backupLinkCTA: 'backupLinkCTAMock',
    backgroundColorRef: {
        fields: {
            hexColor: 'FFF',
        },
    },
    locationModal: {
        fields: {
            icon: 'locationModalIcon',
            header: 'locationModalHeader',
            nonOaStoreDescription: 'nonOaStoreDescription',
            locationNotSelectedDescription: 'locationNotSelectedDescription',
        },
    },
};

export const offerMock = {
    id: 'uuid-35rt43',
    userOfferId: 'test-userOfferId',
    posDiscountId: 'test-posDiscountId',
    name: 'test-name',
    type: 'FIXED_AMOUNT',
    startDateTime: new Date(),
    endDateTime: new Date(),
    terms: 'test-terms',
    isRedeemableInStoreOnly: false,
    isRedeemableOnlineOnly: false,
    termsLanguageCode: 'test-termsLanguageCode',
    applicability: {
        isIncludesAll: false,
    },
};
