import React from 'react';
import { render, screen } from '@testing-library/react';

import MainDealsBanner from '../../../../components/sections/mainProductBanner/mainDealsBanner';

import { offerMock } from './mainProductBanner.mock';
import globalPropsMock from '../../../mocks/globalProps.mock';

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalPropsMock),
}));

describe('mainDealBanner <- mainProductBanner', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should match snapshot', () => {
        const { container } = render(<MainDealsBanner userName="test" offer={offerMock as any} />);

        expect(container).toMatchSnapshot();
    });

    it("should render description, if it's provided", () => {
        const updOfferMock = {
            ...offerMock,
            description: 'test-mainDealBanner-description',
        };

        render(<MainDealsBanner userName="test" offer={updOfferMock as any} />);

        screen.getByText('test-mainDealBanner-description');
    });
});
