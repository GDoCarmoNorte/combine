import React from 'react';
import { shallow } from 'enzyme';

import StaticProductBanner from '../../../../components/sections/mainProductBanner/staticProductBanner';

import { productBannerMock } from './mainProductBanner.mock';
import globalPropsMock from '../../../mocks/globalProps.mock';
import ProductItemControls from '../../../../components/clientOnly/productItemControls';

import { useLocationOrderAheadAvailability } from '../../../../common/hooks/useLocationOrderAheadAvailability';
import { useProductOrderAheadAvailability } from '../../../../common/hooks/useProductOrderAheadAvailability';

const addToBagMock = jest.fn();

jest.mock('@material-ui/core/useMediaQuery', () => jest.fn(() => false).mockImplementationOnce(() => true));

jest.mock('../../../../redux/hooks', () => ({
    useBag: () => ({
        bagEntries: [],
        actions: {
            addDefaultToBag: addToBagMock,
        },
    }),
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalPropsMock),
    useDomainMenu: jest.fn().mockReturnValue({ loading: false }),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProductByContentfulFields: jest
        .fn(() => undefined)
        .mockImplementationOnce(() => ({ name: 'domainName', id: 'ARBYS-WEBOA-640239548' }))
        .mockImplementationOnce(() => ({ name: undefined }))
        .mockImplementationOnce(() => ({})),
    useDiscountPriceAndCalories: jest.fn().mockReturnValue({ price: 5, calories: 100 }),
}));

jest.mock('../../../../common/hooks/useProductOrderAheadAvailability', () => ({
    useProductOrderAheadAvailability: jest.fn().mockReturnValue({ isOrderAheadAvailable: true }),
}));

jest.mock('../../../../common/hooks/useLocationOrderAheadAvailability');

jest.mock('../../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: jest.fn().mockReturnValue({ isSaleable: true }),
}));

jest.mock('../../../../common/hooks/useProductIsAvailable', () => ({
    useProductIsAvailable: jest.fn().mockReturnValue({ isAvailable: true }),
}));

describe('staticProductBanner <- mainProductBanner', () => {
    beforeEach(() => {
        jest.clearAllMocks();

        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({ isLocationOrderAheadAvailable: false });
    });

    it('should render banner properly and call addToBag', () => {
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValueOnce({ isLocationOrderAheadAvailable: true });

        const wrapper = shallow(<StaticProductBanner {...(productBannerMock as any)} />);

        expect(wrapper).toMatchSnapshot();

        wrapper.find(ProductItemControls).simulate('click');

        expect(addToBagMock).toHaveBeenCalledTimes(1);
        expect(addToBagMock).toHaveBeenCalledWith({
            category: 'chicken',
            productId: 'ARBYS-WEBOA-640239548',
            name: 'domainName',
        });
    });

    it('should render without background color, without domain name and without location oa', () => {
        const updatedMock = {
            ...productBannerMock,
            backgroundColorRef: undefined,
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without main text link (product link by default)', () => {
        const updatedMock = {
            ...productBannerMock,
            mainTextLink: undefined,
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without main text (product name by default)', () => {
        const updatedMock = {
            ...productBannerMock,
            mainText: undefined,
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without right image link (product link by default)', () => {
        const updatedMock = {
            ...productBannerMock,
            rightImageLink: undefined,
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without right image (product image by default)', () => {
        const updatedMock = {
            ...productBannerMock,
            rightImage: undefined,
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without contentful product name', () => {
        const updatedMock = {
            ...productBannerMock,
            productLink: {
                fields: {
                    ...productBannerMock.productLink.fields,
                    name: undefined,
                },
            },
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without price if oa is not avaliable', () => {
        (useProductOrderAheadAvailability as jest.Mock).mockReturnValueOnce({ isOrderAheadAvailable: false });

        const wrapper = shallow(<StaticProductBanner {...(productBannerMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render backup cta if locationModal is not provided', () => {
        const updatedMock = {
            ...productBannerMock,
            locationModal: null,
        };

        const wrapper = shallow(<StaticProductBanner {...(updatedMock as any)} />);

        expect(wrapper).toMatchSnapshot();
    });
});
