import React from 'react';
import { render, screen } from '@testing-library/react';
import { entryMock } from './action.mock';
import Action from '../../../../components/sections/action';
import { usePersonalization } from '../../../../redux/hooks';

jest.mock('../../../../redux/hooks/usePersonalization', () => ({
    usePersonalization: jest.fn().mockReturnValue({
        loading: false,
        actions: {
            getPersonalizedSection: jest.fn().mockReturnValue(undefined),
        },
    }),
}));

jest.mock('../../../../components/atoms/BrandLoader', () => () => 'loader');
jest.mock('../../../../components/sections', () => ({ pageSections }) => `${pageSections[0].fields.text}`);

describe('Action section', () => {
    it('should render default section', () => {
        render(<Action entry={entryMock as any} />);

        expect(screen.getByText(/Default Section/i)).toBeInTheDocument();
    });

    it('should render personalized section', () => {
        (usePersonalization as jest.Mock).mockReturnValue({
            loading: false,
            actions: {
                getPersonalizedSection: () => ({
                    fields: {
                        section: {
                            fields: {
                                text: 'Personalized Section',
                            },
                        },
                    },
                }),
            },
        });

        render(<Action entry={entryMock as any} />);

        expect(screen.getByText(/Personalized Section/i)).toBeInTheDocument();
    });

    it('should render loader', () => {
        (usePersonalization as jest.Mock).mockReturnValueOnce({
            loading: true,
            actions: {
                getPersonalizedSection: jest.fn(),
            },
        });

        render(<Action entry={entryMock as any} />);

        expect(screen.getByText(/loader/i)).toBeInTheDocument();
    });
});
