export const entryMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: 'B5pm9196FDrVDljpX2l1z',
        type: 'Entry',
        createdAt: '2021-10-26T14:49:34.314Z',
        updatedAt: '2021-10-29T07:33:02.574Z',
        environment: {
            sys: {
                id: 'feature-feature-DBBP-49874',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 7,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'action',
            },
        },
        locale: 'en-US',
    },
    fields: {
        tableReference: 'HPMainHero',
        defaultSection: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '7yodxUkuKXUle4r1FNwktW',
                type: 'Entry',
                createdAt: '2021-07-13T15:14:58.809Z',
                updatedAt: '2021-10-11T18:19:56.540Z',
                environment: {
                    sys: {
                        id: 'feature-feature-DBBP-49874',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 7,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'mainBanner',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                text: 'Default Section',
                mainText: 'New Truffalo Sauce',
                mainTextLink: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'l5fkpck1mwg3',
                            },
                        },
                        id: '3LNaJ1aSc9KJthmIcDy783',
                        type: 'Entry',
                        createdAt: '2021-07-27T15:28:52.134Z',
                        updatedAt: '2021-09-21T15:25:02.939Z',
                        environment: {
                            sys: {
                                id: 'feature-feature-DBBP-49874',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 9,
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'product',
                            },
                        },
                        locale: 'en-US',
                    },
                    fields: {
                        name: 'Potato Wedges - Regular',
                        nameInUrl: 'potato-wedges',
                        image: {
                            metadata: {
                                tags: [],
                            },
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'l5fkpck1mwg3',
                                    },
                                },
                                id: 'Zj9cGSTJv3naj8p9rVOKN',
                                type: 'Asset',
                                createdAt: '2021-09-21T15:19:58.351Z',
                                updatedAt: '2021-09-21T15:19:58.351Z',
                                environment: {
                                    sys: {
                                        id: 'feature-feature-DBBP-49874',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                locale: 'en-US',
                            },
                            fields: {
                                title: 'Potato Wedges - Regular Side',
                                description: '',
                                file: {
                                    url:
                                        '//images.ctfassets.net/l5fkpck1mwg3/Zj9cGSTJv3naj8p9rVOKN/b75df5e294f710e6214b4cc079d83382/Potato_Wedges_Side.png',
                                    details: {
                                        size: 1229035,
                                        image: {
                                            width: 4000,
                                            height: 3000,
                                        },
                                    },
                                    fileName: 'Potato Wedges_Side.png',
                                    contentType: 'image/png',
                                },
                            },
                        },
                        productId: 'IDPSalesItem-4879',
                        metaDescription:
                            'Enjoy our Potato Wedges Appetizer when you order delivery or pick it up yourself from the nearest Buffalo Wild Wings to you.',
                        isVisible: true,
                    },
                },
                bottomText: {
                    nodeType: 'document',
                    data: {},
                    content: [
                        {
                            nodeType: 'paragraph',
                            content: [
                                {
                                    nodeType: 'text',
                                    value:
                                        '*NO PURCHASE NECESSARY TO PLAY. Open to legal residents of US & DC, 18 or older. Must have valid Blazin’ Rewards account and check in at a participating BWW  to be eligible to win prizing. Game times vary by location. Season Grand Prize will be allocated equally among eligible team players.  See official rules for full details, including number, description and ARV of prizes and game play terms at play.buffalowildwings.com/rules.  Outcome of game play determined by player’s skill. Void where prohibited by law. Sponsor: Buffalo Wild Wings, Inc. Three Glenlake Parkway NE Atlanta, GA 30328. Blazin’ Trivia Wednesday Team Night is not combined with points from participation in other Blazin’ Trivia events ',
                                    marks: [],
                                    data: {},
                                },
                            ],
                            data: {},
                        },
                    ],
                },
                rightImage: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'l5fkpck1mwg3',
                            },
                        },
                        id: '4ACg4dvOf9fDyeBWP7E50u',
                        type: 'Asset',
                        createdAt: '2021-07-13T15:20:47.784Z',
                        updatedAt: '2021-07-13T15:20:47.784Z',
                        environment: {
                            sys: {
                                id: 'feature-feature-DBBP-49874',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        locale: 'en-US',
                    },
                    fields: {
                        title: 'BWW Truffalo Wings',
                        file: {
                            url:
                                '//images.ctfassets.net/l5fkpck1mwg3/4ACg4dvOf9fDyeBWP7E50u/48776be5a57af563ab9847bd099b6959/truffalo_wings_on_platter.png',
                            details: {
                                size: 4400578,
                                image: {
                                    width: 4000,
                                    height: 3000,
                                },
                            },
                            fileName: 'truffalo_wings_on_platter.png',
                            contentType: 'image/png',
                        },
                    },
                },
                backgroundImage: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'l5fkpck1mwg3',
                            },
                        },
                        id: '2veSXeRpTOVxJaVDblyrS5',
                        type: 'Asset',
                        createdAt: '2021-07-13T15:14:11.282Z',
                        updatedAt: '2021-07-13T15:14:11.282Z',
                        environment: {
                            sys: {
                                id: 'feature-feature-DBBP-49874',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        locale: 'en-US',
                    },
                    fields: {
                        title: 'BWW Distressed Background',
                        file: {
                            url:
                                '//images.ctfassets.net/l5fkpck1mwg3/2veSXeRpTOVxJaVDblyrS5/3f7acfc34fb33e03f10df164aab90c21/bww_distressed_bckgd.jpeg',
                            details: {
                                size: 188385,
                                image: {
                                    width: 1280,
                                    height: 394,
                                },
                            },
                            fileName: 'bww_distressed_bckgd.jpeg',
                            contentType: 'image/jpeg',
                        },
                    },
                },
                mainLinkText: 'Order Now',
                mainLinkHref: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'l5fkpck1mwg3',
                            },
                        },
                        id: '7xhVqDWiSDsAydvEbfzyR9',
                        type: 'Entry',
                        createdAt: '2021-07-09T16:03:10.143Z',
                        updatedAt: '2021-07-09T16:03:10.143Z',
                        environment: {
                            sys: {
                                id: 'feature-feature-DBBP-49874',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'pageLink',
                            },
                        },
                        locale: 'en-US',
                    },
                    fields: {
                        name: 'Home',
                    },
                },
                mainLinkType: 'large',
            },
        },
    },
};
