import React from 'react';
import GoingAtTheBar from '../../../../components/sections/goingAtTheBar';
import { render } from '@testing-library/react';
import mock from './locationEventsSection.mock';

jest.mock('../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));

describe('Location Events Section', () => {
    it('should render properly', () => {
        const { container } = render(<GoingAtTheBar entry={mock as any} />);

        expect(container).toMatchSnapshot();
    });
});
