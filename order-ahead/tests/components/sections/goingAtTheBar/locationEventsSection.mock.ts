export default {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '5zu19zjBbxRSOFH2aDLV3z',
        type: 'Entry',
        createdAt: '2021-10-26T11:26:02.217Z',
        updatedAt: '2021-10-26T11:55:57.015Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 3,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'goingAtTheBar',
            },
        },
        locale: 'en-US',
    },
    fields: {
        title: 'At the bar',
        subtitle: 'going on',
        backgroundColor: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '15h1wW3x2JfoAqgpS6iwax',
                type: 'Entry',
                createdAt: '2021-07-09T21:24:12.597Z',
                updatedAt: '2021-09-23T06:55:06.715Z',
                environment: {
                    sys: {
                        id: 'dev',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 5,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'beige',
                hexColor: 'E3E1DB',
            },
        },
        eventCards: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '53kZdQ1ch7vEQkmibRbtmC',
                    type: 'Entry',
                    createdAt: '2021-10-26T10:05:37.543Z',
                    updatedAt: '2021-10-26T10:05:37.543Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'eventCard',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    backgroundColor: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '29iQxGwajctS22OCxwFXDQ',
                            type: 'Entry',
                            createdAt: '2021-07-12T10:31:56.199Z',
                            updatedAt: '2021-09-23T06:48:05.378Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 2,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'color',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'white',
                            hexColor: 'ffffff',
                        },
                    },
                    eventIcon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '6QczfoZeExUmdCPOECtKo2',
                            type: 'Asset',
                            createdAt: '2021-09-28T18:22:00.866Z',
                            updatedAt: '2021-09-28T18:22:00.866Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Icon traditional wings 512w',
                            description: '',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/6QczfoZeExUmdCPOECtKo2/211e91619c9232422d8473d8672c1884/icon-traditional-wings-512w.png',
                                details: {
                                    size: 11326,
                                    image: {
                                        width: 512,
                                        height: 512,
                                    },
                                },
                                fileName: 'icon-traditional-wings-512w.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    eventName: 'Event name',
                    eventDescription: 'Event Description Event Description Event Description',
                    eventLinkText: 'View menu',
                    eventLinkHref: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '6zJe2taXeWsc1zBhWKXIUZ',
                            type: 'Entry',
                            createdAt: '2021-07-30T13:57:04.753Z',
                            updatedAt: '2021-10-18T20:54:18.948Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 5,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'product',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Buffalo Bleu Burger',
                            nameInUrl: 'buffalo-bleu-burger',
                            image: {
                                metadata: {
                                    tags: [],
                                },
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'l5fkpck1mwg3',
                                        },
                                    },
                                    id: '4nNwuDmwM6MIRFhujW3IuB',
                                    type: 'Asset',
                                    createdAt: '2021-10-18T20:53:28.430Z',
                                    updatedAt: '2021-10-18T20:53:59.327Z',
                                    environment: {
                                        sys: {
                                            id: 'dev',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 2,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'No Image Available',
                                    description: '',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/l5fkpck1mwg3/4nNwuDmwM6MIRFhujW3IuB/919fb869e68cdeddff752ca5e796bee1/No_Image_Available_adobespark.png',
                                        details: {
                                            size: 751857,
                                            image: {
                                                width: 4000,
                                                height: 3000,
                                            },
                                        },
                                        fileName: 'No Image Available_adobespark.png',
                                        contentType: 'image/png',
                                    },
                                },
                            },
                            productId: 'IDPSalesItem-4858',
                            metaDescription:
                                'Enjoy our Buffalo Bleu Burger when you order delivery or pick it up yourself from the nearest Buffalo Wild Wings to you.',
                            isVisible: true,
                        },
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '53kZdQ1ch7vEQkmibRbtmC',
                    type: 'Entry',
                    createdAt: '2021-10-26T10:05:37.543Z',
                    updatedAt: '2021-10-26T10:05:37.543Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'eventCard',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    backgroundColor: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '29iQxGwajctS22OCxwFXDQ',
                            type: 'Entry',
                            createdAt: '2021-07-12T10:31:56.199Z',
                            updatedAt: '2021-09-23T06:48:05.378Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 2,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'color',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'white',
                            hexColor: 'ffffff',
                        },
                    },
                    eventIcon: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '6QczfoZeExUmdCPOECtKo2',
                            type: 'Asset',
                            createdAt: '2021-09-28T18:22:00.866Z',
                            updatedAt: '2021-09-28T18:22:00.866Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Icon traditional wings 512w',
                            description: '',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/6QczfoZeExUmdCPOECtKo2/211e91619c9232422d8473d8672c1884/icon-traditional-wings-512w.png',
                                details: {
                                    size: 11326,
                                    image: {
                                        width: 512,
                                        height: 512,
                                    },
                                },
                                fileName: 'icon-traditional-wings-512w.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    eventName: 'Event name',
                    eventDescription: 'Event Description Event Description Event Description',
                    eventLinkText: 'View menu',
                    eventLinkHref: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '6zJe2taXeWsc1zBhWKXIUZ',
                            type: 'Entry',
                            createdAt: '2021-07-30T13:57:04.753Z',
                            updatedAt: '2021-10-18T20:54:18.948Z',
                            environment: {
                                sys: {
                                    id: 'dev',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 5,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'product',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Buffalo Bleu Burger',
                            nameInUrl: 'buffalo-bleu-burger',
                            image: {
                                metadata: {
                                    tags: [],
                                },
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'l5fkpck1mwg3',
                                        },
                                    },
                                    id: '4nNwuDmwM6MIRFhujW3IuB',
                                    type: 'Asset',
                                    createdAt: '2021-10-18T20:53:28.430Z',
                                    updatedAt: '2021-10-18T20:53:59.327Z',
                                    environment: {
                                        sys: {
                                            id: 'dev',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 2,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'No Image Available',
                                    description: '',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/l5fkpck1mwg3/4nNwuDmwM6MIRFhujW3IuB/919fb869e68cdeddff752ca5e796bee1/No_Image_Available_adobespark.png',
                                        details: {
                                            size: 751857,
                                            image: {
                                                width: 4000,
                                                height: 3000,
                                            },
                                        },
                                        fileName: 'No Image Available_adobespark.png',
                                        contentType: 'image/png',
                                    },
                                },
                            },
                            productId: 'IDPSalesItem-4858',
                            metaDescription:
                                'Enjoy our Buffalo Bleu Burger when you order delivery or pick it up yourself from the nearest Buffalo Wild Wings to you.',
                            isVisible: true,
                        },
                    },
                },
            },
        ],
    },
};
