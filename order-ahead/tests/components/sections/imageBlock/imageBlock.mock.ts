import { IImageBlockSection } from '../../../../@generated/@types/contentful';

const imageBlockMock = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '7bUnrvAhYss6FdJjpJxSGj',
        type: 'Entry',
        createdAt: '2020-11-24T12:51:28.284Z',
        updatedAt: '2020-11-25T10:15:08.025Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 7,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Some image block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '74SFe01NbcAc1yYFcQslyw',
                    type: 'Entry',
                    createdAt: '2020-11-24T12:51:17.381Z',
                    updatedAt: '2020-11-25T09:55:33.831Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 3,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    subtitle: 'Eyebrow',
                    title: 'Card Title',
                    description:
                        'Descriptive copy improves SEO. It is suggested to stick with 2 lines or less. Lorem ipsum dolor sit amet consecteteur.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2DdqkId3KkFwFJd4QVg73G',
                            type: 'Asset',
                            createdAt: '2020-10-21T18:58:59.924Z',
                            updatedAt: '2020-10-21T18:58:59.924Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Side Fries Medium',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2DdqkId3KkFwFJd4QVg73G/4d68add33fc81e9683f57a0070b30fe8/Website_Side_Fries_Medium.png',
                                details: { size: 1449059, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Side_Fries_Medium.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '6iiOuscKIZwFJjh8UNAZbY',
                            type: 'Entry',
                            createdAt: '2020-11-24T14:18:06.931Z',
                            updatedAt: '2020-11-24T14:18:06.931Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Primary button',
                            link: 'link',
                        },
                    },
                },
            },
        ],
    },
} as unknown) as IImageBlockSection;

const imageBlockMockWithoutCTA = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '7bUnrvAhYss6FdJjpJxSGj',
        type: 'Entry',
        createdAt: '2020-11-24T12:51:28.284Z',
        updatedAt: '2020-11-25T10:15:08.025Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 7,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Some image block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '74SFe01NbcAc1yYFcQslyw',
                    type: 'Entry',
                    createdAt: '2020-11-24T12:51:17.381Z',
                    updatedAt: '2020-11-25T09:55:33.831Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 3,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    subtitle: 'Eyebrow',
                    title: 'Card Title',
                    description:
                        'Descriptive copy improves SEO. It is suggested to stick with 2 lines or less. Lorem ipsum dolor sit amet consecteteur.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2DdqkId3KkFwFJd4QVg73G',
                            type: 'Asset',
                            createdAt: '2020-10-21T18:58:59.924Z',
                            updatedAt: '2020-10-21T18:58:59.924Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Side Fries Medium',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2DdqkId3KkFwFJd4QVg73G/4d68add33fc81e9683f57a0070b30fe8/Website_Side_Fries_Medium.png',
                                details: { size: 1449059, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Side_Fries_Medium.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                },
            },
        ],
    },
} as unknown) as IImageBlockSection;

const imageBlockMockWithColoring = ({
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '7bUnrvAhYss6FdJjpJxSGj',
        type: 'Entry',
        createdAt: '2020-11-24T12:51:28.284Z',
        updatedAt: '2020-11-25T10:15:08.025Z',
        environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
        revision: 7,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockSection' } },
        locale: 'en-US',
    },
    fields: {
        name: 'Some image block section',
        cards: [
            {
                sys: {
                    space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                    id: '74SFe01NbcAc1yYFcQslyw',
                    type: 'Entry',
                    createdAt: '2020-11-24T12:51:17.381Z',
                    updatedAt: '2020-11-25T09:55:33.831Z',
                    environment: { sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' } },
                    revision: 3,
                    contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockCard' } },
                    locale: 'en-US',
                },
                fields: {
                    subtitle: 'Eyebrow',
                    title: 'Card Title',
                    description:
                        'Descriptive copy improves SEO. It is suggested to stick with 2 lines or less. Lorem ipsum dolor sit amet consecteteur.',
                    image: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '2DdqkId3KkFwFJd4QVg73G',
                            type: 'Asset',
                            createdAt: '2020-10-21T18:58:59.924Z',
                            updatedAt: '2020-10-21T18:58:59.924Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Side Fries Medium',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/2DdqkId3KkFwFJd4QVg73G/4d68add33fc81e9683f57a0070b30fe8/Website_Side_Fries_Medium.png',
                                details: { size: 1449059, image: { width: 4000, height: 3000 } },
                                fileName: 'Website_Side_Fries_Medium.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    cta: {
                        sys: {
                            space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                            id: '6iiOuscKIZwFJjh8UNAZbY',
                            type: 'Entry',
                            createdAt: '2020-11-24T14:18:06.931Z',
                            updatedAt: '2020-11-24T14:18:06.931Z',
                            environment: {
                                sys: { id: 'DBBP-8575-and-DBBP-8290', type: 'Link', linkType: 'Environment' },
                            },
                            revision: 1,
                            contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'imageBlockCardCta' } },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Primary button',
                            link: 'link',
                        },
                    },
                },
            },
        ],
        backgroundColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3WRLNNeckNZ62pV4d5nly0',
                type: 'Entry',
                createdAt: '2020-08-13T16:02:54.388Z',
                updatedAt: '2020-08-13T16:02:54.388Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'bwwyellow',
                hexColor: 'FFC500',
            },
        },
        titleColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '2xcgnkxTQIPujDHTQSsHWy',
                type: 'Entry',
                createdAt: '2020-08-09T21:39:40.755Z',
                updatedAt: '2020-08-13T15:55:15.442Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'arbysredhover',
                hexColor: 'AC1319',
            },
        },
        descriptionColor: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3gyI6pWitD9iyEyxeoAUOI',
                type: 'Entry',
                createdAt: '2020-08-09T21:41:18.598Z',
                updatedAt: '2020-08-11T15:06:39.489Z',
                environment: {
                    sys: {
                        id: 'DBBP-8575-and-DBBP-8290',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'gray1',
                hexColor: 'FAFBF8',
            },
        },
    },
} as unknown) as IImageBlockSection;
export { imageBlockMock as default, imageBlockMockWithoutCTA, imageBlockMockWithColoring };
