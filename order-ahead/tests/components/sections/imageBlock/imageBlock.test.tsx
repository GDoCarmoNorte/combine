import React from 'react';
import { shallow } from 'enzyme';

import ImageBlock from '../../../../components/sections/imageBlock';
import imageBlockMock, { imageBlockMockWithoutCTA, imageBlockMockWithColoring } from './imageBlock.mock';

describe('ImageBlock', () => {
    it('should render with cta', () => {
        const wrapper = shallow(<ImageBlock entry={imageBlockMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render without cta', () => {
        const wrapper = shallow(<ImageBlock entry={imageBlockMockWithoutCTA} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with configured coloring', () => {
        const wrapper = shallow(<ImageBlock entry={imageBlockMockWithColoring} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render with legal message', () => {
        const imageBlockWithLegalMessage: any = {
            ...imageBlockMock,
            fields: {
                ...imageBlockMock.fields,
                cards: [
                    {
                        ...imageBlockMock.fields.cards[0],
                        fields: {
                            ...imageBlockMock.fields.cards[0].fields,
                            legalMessage: {
                                fields: {
                                    message: 'Some message',
                                },
                            },
                        },
                    },
                ],
            },
        };

        const wrapper = shallow(<ImageBlock entry={imageBlockWithLegalMessage} />);
        expect(wrapper.find('.legalMessage')).toMatchSnapshot();
    });
});
