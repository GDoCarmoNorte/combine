import React from 'react';
import { shallow } from 'enzyme';

import SiteMapCategory from '../../../../components/sections/SiteMapCategory';

describe('SiteMapCategory', () => {
    it('should render siteMapCategory', () => {
        const linkBlocks = [
            {
                nameInUrl: '/name-in-url',
                name: 'link block name',
                links: [
                    {
                        nameInUrl: '/link-name-in-url',
                        name: 'link name',
                        isExternal: false,
                    },
                ],
            },
        ];
        const wrapper = shallow(<SiteMapCategory mainLink="mainLink" name="name" linkBlocks={linkBlocks} />);

        expect(wrapper).toMatchSnapshot();
    });
});
