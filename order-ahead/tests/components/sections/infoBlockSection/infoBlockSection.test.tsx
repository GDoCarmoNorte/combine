import React from 'react';
import { shallow } from 'enzyme';

import InfoBlockSection from '../../../../components/sections/infoBlockSection';
import entryMock from './infoBlockSection.mock';

jest.mock('../../../../components/sections/withSectionLayout', () => jest.fn((component) => component));

describe('infoBlockSection', () => {
    it('should render the info block section properly', () => {
        const wrapper = shallow(<InfoBlockSection entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the info block section with single card', () => {
        const [firstCard] = entryMock.fields.cards;
        const updatedMock = {
            fields: {
                name: 'Section',
                cards: [firstCard],
            },
        };

        const wrapper = shallow(<InfoBlockSection entry={updatedMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the info block section with empty card', () => {
        const updatedMock = {
            fields: {
                cards: [{}],
                name: 'Section',
            },
        };

        const wrapper = shallow(<InfoBlockSection entry={updatedMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the info block section with legalMessage', () => {
        const updatedMock = {
            fields: {
                name: 'Section',
                cards: [
                    {
                        ...entryMock.fields.cards[0],
                        fields: {
                            ...entryMock.fields.cards[0].fields,
                            legalMessage: {
                                fields: {
                                    message: 'Some message',
                                },
                            },
                        },
                    },
                ],
            },
        };

        const wrapper = shallow(<InfoBlockSection entry={updatedMock as any} />);

        expect(wrapper.find('.legalMessage')).toMatchSnapshot();
    });
});
