export default {
    fields: {
        name: 'Section',
        cards: [
            {
                fields: {
                    headerText: 'Exclusive Deals',
                    subheaderText: "Sign up for Arby's Email to get the best deals on your favorite items.",
                    buttonText: 'Sign Up',
                    icon: 'iconMock',
                    link: 'linkMock',
                    backgroundColor: {
                        fields: {
                            hexColor: 'FFF',
                        },
                    },
                    headerColor: {
                        fields: {
                            hexColor: '000',
                        },
                    },
                    subheaderColor: {
                        fields: {
                            hexColor: 'EEE',
                        },
                    },
                },
            },
            {
                fields: {
                    headerText: 'Get It Delivered',
                    subheaderText: "Don't want to drive? No problem. The nearest Arby's is your doorstop.",
                    buttonText: 'Get Delivery',
                    icon: 'iconMock',
                    link: 'linkMock',
                },
            },
        ],
    },
};
