export default {
    fields: {
        topIcon: 'topIconMock',
        topText: 'Limited Time',
        mainText: 'chicken cheddar ranch',
        mainTextLink: 'mainTextLinkMock',
        bottomText: {
            fields: {
                text: {
                    nodeType: 'document',
                    data: {},
                    content: [
                        {
                            data: {},
                            content: [
                                {
                                    data: {},
                                    marks: [],
                                    value: 'Rich bottom text content here',
                                    nodeType: 'text',
                                },
                            ],
                        },
                    ],
                },
            },
        },
        rightImage: 'rightImageMock',
        rightImageLink: 'rightImageLinkMock',
        backgroundColor: 'FFF',
        mainLinkText: 'ORDER NOW',
        mainLinkHref: 'mainLinkHrefMock',
        secondaryLinkHref: 'secondaryLinkHrefMock',
    },
};
