import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import MainBanner, { ActionsSection, ImageSection } from '../../../../components/sections/mainBanner';
import entryMock from './mainBanner.mock';
import VideoBlock from '../../../../components/sections/videoBlock';

jest.mock('@material-ui/core/useMediaQuery', () => jest.fn(() => false).mockImplementationOnce(() => true));

jest.mock('../../../../lib/gtm', () => ({
    getGtmIdByName: () => 'mainBanner',
}));

const bgVideoMock = {
    fields: {
        webM: {
            fields: {
                file: {
                    url: '/video.webm',
                },
            },
        },
        mp4: {
            fields: {
                file: {
                    url: '/video.mp4',
                },
            },
        },
        poster: {
            fields: {
                file: {
                    url: '/poster.jpg',
                },
            },
        },
    },
};

const legalMessageMock: any = {
    fields: {
        message: 'Some message',
        termsApplyTitle: 'Terms apply',
        viewDetailsTitle: 'View details',
    },
};

describe('mainBanner', () => {
    it('should render the main banner with right image', () => {
        const wrapper = shallow(<MainBanner entry={entryMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the main banner with left image', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                leftImage: entryMock.fields.rightImage,
                leftImageLink: entryMock.fields.rightImageLink,
                rightImage: undefined,
                rightImageLink: undefined,
            },
        };
        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the background video', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                backgroundVideo: bgVideoMock,
            },
        };
        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find(VideoBlock)).toMatchSnapshot();
    });

    it('should render the main banner with background image', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                backgroundImage: {
                    fields: {
                        title: 'Right Image',
                        description: 'Right Image',
                        file: {
                            url: '//images.ctfassets.net/o19mhvm9a2cm/jk1H/chicken_masked_ltshadow_1.png',
                        },
                    },
                },
            },
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find('style')).toMatchSnapshot();
    });

    it('should render the main banner without background color', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                backgroundColor: undefined,
            },
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find('style')).toMatchSnapshot();
    });

    it('should render the actions section without top text', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                topText: undefined,
            },
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find(ActionsSection).dive()).toMatchSnapshot();
    });

    it('should render the actions section with secondary link', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                secondaryLink: 'secondaryLink',
            },
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find(ActionsSection).dive()).toMatchSnapshot();
    });

    it('should render null if fields does not exists', () => {
        const updatedMock = {
            fields: undefined,
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper).toEqual({});
    });

    it('should render image left section without a link', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                leftImage: 'leftImageMock',
                leftImageLink: null,
                rightImage: null,
            },
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find(ImageSection).dive()).toMatchSnapshot();
    });

    it('should render image right section without a link', () => {
        const updatedMock = {
            fields: {
                ...entryMock.fields,
                rightImageLink: null,
            },
        };

        const wrapper = shallow(<MainBanner entry={updatedMock as any} />);

        expect(wrapper.find(ImageSection).dive()).toMatchSnapshot();
    });

    it('should render the main banner with Rich Text', () => {
        const wrapper = shallow(<MainBanner entry={entryMock as any} />);

        expect(wrapper.find(ActionsSection).dive().find('RichText')).toMatchSnapshot();
    });

    it('should render the main banner with legal message', () => {
        const updatedMock: any = {
            fields: {
                ...entryMock.fields,
                legalMessage: legalMessageMock,
            },
        };

        render(<MainBanner entry={updatedMock} />);

        screen.getByText(legalMessageMock.fields.termsApplyTitle);
        screen.getByText(legalMessageMock.fields.viewDetailsTitle);
    });

    it('should render the main banner with umlaut symbols', () => {
        const mainText = '$3 tall ice-cold wild herd kÖlsch';
        const updatedMock: any = {
            fields: {
                ...entryMock.fields,
                mainText,
            },
        };

        render(<MainBanner entry={updatedMock} />);

        expect(screen.getByText(mainText)).toHaveClass('mainTextUmlaut');
    });
});
