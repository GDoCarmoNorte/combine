import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import ApplePaySection from '../../../../components/clientOnly/paymentInfoContainer/applePaySection/applePaySection';
import { useOrderLocation } from '../../../../redux/hooks';
import { useApplePay } from '../../../../common/hooks/useApplePay';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { TInitialPaymentTypes } from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { getLocationById } from '../../../../common/services/locationService';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../common/hooks/useApplePay');
jest.mock('../../../../common/hooks/useFreedomPay');
jest.mock('../../../../lib/getFeatureFlags');
jest.mock('../../../../common/services/locationService');
jest.mock('../../../../common/hooks/useConfiguration', () => ({
    createConfiguration: jest.fn(),
    useConfiguration: jest.fn(),
}));

const resetFPIframeMock = jest.fn();

const locationIdMock = '13';
const totalAmountMock = 10;

describe('Apple Pay section', () => {
    beforeEach(() => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isApplePayEnabled: true,
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({ currentLocation: { id: locationIdMock } });
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: null,
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        (getLocationById as jest.Mock).mockResolvedValue({
            id: 1,
        });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render iframe if device compatible and html provided', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Apple pay/i)).toBeInTheDocument();
        });
    });
    it('should not render iframe if device not compatible', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: false,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Apple pay/i)).not.toBeInTheDocument();
        });
    });
    it('should not render iframe if isApplePayEnabled: false', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isApplePayEnabled: false,
            },
        });
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Apple pay/i)).not.toBeInTheDocument();
        });
    });
    it('should show loader if iframe is loading', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: true,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} />);
        await waitFor(() => {
            expect(screen.getByRole('progressbar')).toBeInTheDocument();
        });
        expect(screen.queryByLabelText(/Apple pay/i)).not.toBeInTheDocument();
    });
    it('should not render iframe if iframe not retrieved from FP', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: null },
            error: null,
            isLoading: false,
        });
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} />);
        await waitFor(() => {
            expect(screen.queryByLabelText(/Apple pay/i)).not.toBeInTheDocument();
        });
    });
    it('should not render iframe if payment token already retrieved', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: {
                keys: ['test'],
                cardHolderName: 'Test test',
                paymentType: TInitialPaymentTypes.APPLE_PAY,
            },
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        const submitSuccessfulMock = () => {
            return Promise.resolve();
        };

        render(
            <ApplePaySection
                totalAmount={totalAmountMock}
                setCompatibility={jest.fn()}
                onSubmit={submitSuccessfulMock}
            />
        );
        await waitFor(() => {
            expect(screen.queryByLabelText(/Apple pay/i)).not.toBeInTheDocument();
        });
    });

    it('should render iframe if payment token retrieved is not ApplePay one', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: {
                keys: ['test'],
                cardHolderName: 'Test test',
                paymentType: TInitialPaymentTypes.CREDIT_OR_DEBIT,
            },
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Apple pay/i)).toBeInTheDocument();
        });
    });
    it('should call reset FP iframe if submit failed', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: null,
            isLoading: false,
            isCompatible: true,
            initPayment: jest.fn(() => {
                return Promise.resolve();
            }),
        });
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: {
                keys: ['test'],
                cardHolderName: 'Test test',
                paymentType: TInitialPaymentTypes.APPLE_PAY,
            },
            height: 60,
            errors: null,
            reset: resetFPIframeMock,
        });
        const submitRejectedMock = () => {
            return Promise.reject();
        };

        render(
            <ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} onSubmit={submitRejectedMock} />
        );
        await waitFor(() => {
            expect(resetFPIframeMock).toHaveBeenCalled();
        });
    });
    it('should handle FP errors', async () => {
        (useFreedomPay as jest.Mock).mockReturnValue({
            payment: null,
            height: 60,
            errors: ['test error'],
            reset: resetFPIframeMock,
        });
        const onErrorMock = jest.fn();
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} onError={onErrorMock} />);
        await waitFor(() => {
            expect(onErrorMock).toHaveBeenCalled();
        });
    });
    it('should handle iframe errors', async () => {
        (useApplePay as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'test', iframeHtml: document.createElement('div') },
            error: {
                name: 'test error',
                message: 'test error message',
            },
            isLoading: false,
        });
        const onErrorMock = jest.fn();
        render(<ApplePaySection totalAmount={totalAmountMock} setCompatibility={jest.fn()} onError={onErrorMock} />);
        await waitFor(() => {
            expect(onErrorMock).toHaveBeenCalled();
        });
        expect(screen.queryByLabelText(/Apple pay/i)).not.toBeInTheDocument();
    });
});
