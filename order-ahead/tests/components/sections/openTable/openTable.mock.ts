export const openTableMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '5wNud8KdbQm7hiCK1PH29l',
        type: 'Entry',
        createdAt: '2021-10-23T07:40:06.003Z',
        updatedAt: '2021-10-23T07:40:06.003Z',
        environment: {
            sys: {
                id: 'otdt-feature-DBBP-31335',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'openTable',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: 'Open Table',
        locationsMapping: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6uzY36NpckGMqDxO09r39f',
                    type: 'Entry',
                    createdAt: '2021-10-23T07:39:59.582Z',
                    updatedAt: '2021-10-23T07:39:59.582Z',
                    environment: {
                        sys: {
                            id: 'otdt-feature-DBBP-31335',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'locationMapping',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    idpLocationId: '12',
                    externalLocationId: '333793',
                },
            },
        ],
    },
};
