import React from 'react';
import { render } from '@testing-library/react';
import OpenTableWidget from '../../../../components/sections/openTable/openTable';
import { locationDetailsMock } from '../../organisms/locationDetailsPageContent/locationDetailsPageLayout.mock';
import { openTableMock } from './openTable.mock';
import { injectScript } from '../../../../lib/injectScriptOnce';
import { isOpenTableEnabled } from '../../../../lib/getFeatureFlags';
const injectScriptMock = jest.fn();

jest.mock('../../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isOpenTableEnabled: jest.fn().mockReturnValue(true),
    };
});

jest.mock('../../../../lib/injectScriptOnce');

describe('OpenTable widget', () => {
    beforeEach(() => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (injectScript as jest.Mock) = injectScriptMock;
    });
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should embed correct script if CMS setup is provided', () => {
        render(<OpenTableWidget data={{ locationDetails: locationDetailsMock }} entry={openTableMock as any} />);

        expect(injectScriptMock).toHaveBeenCalledTimes(1);
        expect(
            injectScriptMock
        ).toHaveBeenCalledWith(
            '//www.opentable.com/widget/reservation/loader?rid=333793&domain=com&type=standard&theme=wide&lang=en-us&overlay=false&iframe=false',
            { current: document.createElement('div') }
        );
    });

    it('should not try to embed script if CMS setup is not provided for the store', () => {
        const locationDetailstestSpecificMock = {
            ...locationDetailsMock,
            id: '123',
        };

        render(
            <OpenTableWidget data={{ locationDetails: locationDetailstestSpecificMock }} entry={openTableMock as any} />
        );

        expect(injectScriptMock).toHaveBeenCalledTimes(0);
    });
    it('should not try to embed script if location id is not defined', () => {
        const locationDetailstestSpecificMock = {
            ...locationDetailsMock,
            id: undefined,
        };
        render(
            <OpenTableWidget data={{ locationDetails: locationDetailstestSpecificMock }} entry={openTableMock as any} />
        );

        expect(injectScriptMock).toHaveBeenCalledTimes(0);
    });

    it('should not try to embed script if dineTime feature is disabled', () => {
        (isOpenTableEnabled as jest.Mock).mockReturnValue(false);
        render(<OpenTableWidget data={{ locationDetails: locationDetailsMock }} entry={openTableMock as any} />);

        expect(injectScriptMock).toHaveBeenCalledTimes(0);
    });
});
