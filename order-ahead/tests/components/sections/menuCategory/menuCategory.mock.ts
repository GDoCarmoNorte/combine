export const entry = {
    fields: {
        name: 'Our Menu',
        categories: [
            {
                sys: {
                    contentType: {
                        sys: {
                            id: 'menuCategory',
                        },
                    },
                },
                fields: {
                    categoryName: 'Meals',
                    categoryIdList: ['1', '3'],
                    link: {
                        metadata: {
                            tags: [],
                        },
                        fields: {
                            name: 'Meals',
                            nameInUrl: 'meals',
                        },
                    },
                    products: [
                        {
                            metadata: {
                                tags: [],
                            },
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '6V6TbwoLAtU9ixr3ZgpYDl',
                                type: 'Entry',
                                createdAt: '2021-02-22T20:44:14.220Z',
                                updatedAt: '2021-02-22T20:44:14.220Z',
                                environment: {
                                    sys: {
                                        id: 'qa',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'product',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Greek Gyro Meal',
                                nameInUrl: 'greek-gyro-medium-meal',
                                image: {
                                    metadata: {
                                        tags: [],
                                    },
                                    sys: {
                                        space: {
                                            sys: {
                                                type: 'Link',
                                                linkType: 'Space',
                                                id: 'o19mhvm9a2cm',
                                            },
                                        },
                                        id: '3YI5HMcNcwCFza65gsq1qc',
                                        type: 'Asset',
                                        createdAt: '2020-09-25T18:11:27.593Z',
                                        updatedAt: '2020-09-25T18:11:27.593Z',
                                        environment: {
                                            sys: {
                                                id: 'qa',
                                                type: 'Link',
                                                linkType: 'Environment',
                                            },
                                        },
                                        revision: 1,
                                        locale: 'en-US',
                                    },
                                    fields: {
                                        title: 'Meals Signatures Gyro Greek',
                                        file: {
                                            url:
                                                '//images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png',
                                            details: {
                                                size: 2334705,
                                                image: {
                                                    width: 4000,
                                                    height: 3000,
                                                },
                                            },
                                            fileName: 'Website_Meals_Signatures_Gyro_Greek.png',
                                            contentType: 'image/png',
                                        },
                                    },
                                },
                                productId: 'arb-itm-000-058',
                                metaDescription:
                                    'Our Greek Gyro pairs perfectly with your choice of side and a drink for the ultimate lunch or dinner combo. Start your order today!',
                            },
                        },
                    ],
                    image: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '3YI5HMcNcwCFza65gsq1qc',
                            type: 'Asset',
                            createdAt: '2020-09-25T18:11:27.593Z',
                            updatedAt: '2020-09-25T18:11:27.593Z',
                            environment: {
                                sys: {
                                    id: 'qa',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'Meals Signatures Gyro Greek',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png',
                                details: {
                                    size: 2334705,
                                    image: {
                                        width: 4000,
                                        height: 3000,
                                    },
                                },
                                fileName: 'Website_Meals_Signatures_Gyro_Greek.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    taglineHeading: 'Make your meats',
                    taglineDescription: 'a meal',
                    metaDescription:
                        "Choose from favorites like the Classic Beef 'N Cheddar or Chicken Bacon & Swiss sandwiches and pair with your choice of side and a drink for the ultimate meal combo.",
                },
            },
            {
                sys: {
                    contentType: {
                        sys: {
                            id: 'menuCategory',
                        },
                    },
                },
                fields: {
                    categoryName: 'Limited Time',
                    categoryId: '2',
                    link: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '5fcc20a7-9bdb-4e2d-9395-4a848870da3c',
                            type: 'Entry',
                            createdAt: '2021-04-19T10:26:11.566Z',
                            updatedAt: '2021-04-19T10:26:11.566Z',
                            environment: {
                                sys: {
                                    id: 'circular-references',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'menuCategoryLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Limited Time',
                            nameInUrl: 'limited-time',
                        },
                    },
                    products: [
                        {
                            metadata: {
                                tags: [],
                            },
                            fields: {
                                name: 'Crispy Fish Sandwich',
                                nameInUrl: 'crispy-fish-sandwich',
                                image: {
                                    metadata: {
                                        tags: [],
                                    },
                                    sys: {
                                        space: {
                                            sys: {
                                                type: 'Link',
                                                linkType: 'Space',
                                                id: 'o19mhvm9a2cm',
                                            },
                                        },
                                        id: '01uBYmQH3dVeGo52Cjij6r',
                                        type: 'Asset',
                                        createdAt: '2020-12-16T21:27:03.672Z',
                                        updatedAt: '2020-12-16T21:27:03.672Z',
                                        environment: {
                                            sys: {
                                                id: 'qa',
                                                type: 'Link',
                                                linkType: 'Environment',
                                            },
                                        },
                                        revision: 1,
                                        locale: 'en-US',
                                    },
                                    fields: {
                                        title: 'Sandwich Fish Crispy',
                                        file: {
                                            url:
                                                '//images.ctfassets.net/o19mhvm9a2cm/01uBYmQH3dVeGo52Cjij6r/2a1962f36acfa9a564996119b99e11a4/Website_Sandwich_Fish_Crispy.png',
                                            details: {
                                                size: 3074060,
                                                image: {
                                                    width: 4000,
                                                    height: 3000,
                                                },
                                            },
                                            fileName: 'Website_Sandwich_Fish_Crispy.png',
                                            contentType: 'image/png',
                                        },
                                    },
                                },
                                productId: 'arb-itm-000-164',
                                metaDescription:
                                    "Come by and try the Crispy Fish Sandwich, or any of our other limited time options at your local Arby's. Online ordering now available!",
                                isVisible: true,
                            },
                        },
                    ],
                    image: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '7vXW8htMYjWakpvJrz0ROM',
                            type: 'Asset',
                            createdAt: '2021-03-17T19:34:51.695Z',
                            updatedAt: '2021-03-17T19:34:51.695Z',
                            environment: {
                                sys: {
                                    id: 'qa',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'LTO Gyro Spicy ',
                            file: {
                                url:
                                    '//images.ctfassets.net/o19mhvm9a2cm/7vXW8htMYjWakpvJrz0ROM/b5f55aa8a3843fb21d19b3cf47dc957f/Website_LTO_GryoSpicy_R2.png',
                                details: {
                                    size: 2368703,
                                    image: {
                                        width: 4000,
                                        height: 3000,
                                    },
                                },
                                fileName: 'Website_LTO_GryoSpicy_R2.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    taglineHeading: 'Here for a good time',
                    taglineDescription: 'not a long time',
                    metaDescription:
                        "These offers wont't be around forever! Come by and try any of our limited-time options at your local Arby's. Online ordering now available!",
                },
            },
        ],
        backgroundPosition: 'back',
        backgroundColor: {
            fields: {
                name: 'arbysblack',
                hexColor: '2A221C',
            },
        },
        backgroundBottomText: 'We have the ',
        backgroundBottomTextSecondPart: 'meats',
    },
};
