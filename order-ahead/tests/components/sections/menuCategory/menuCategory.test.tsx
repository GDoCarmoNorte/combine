import React from 'react';
import { render, screen } from '@testing-library/react';
import MenuCategorySection from '../../../../components/sections/menuCategory/menuCategory';
import { entry } from './menuCategory.mock';
import { useUnavailableCategories } from '../../../../common/hooks/useUnavailableCategories';

jest.mock('../../../../redux/hooks', () => {
    const { entry } = require('./menuCategory.mock');
    return {
        useDomainMenu: jest.fn().mockReturnValue({
            actions: {
                getAvailableCategories: jest.fn().mockReturnValue(entry.fields.categories),
            },
        }),
    };
});

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainMenuCategories: jest.fn().mockReturnValue({ '1': {}, '2': {} }),
}));

jest.mock('../../../../common/hooks/useUnavailableCategories', () => ({
    useUnavailableCategories: jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../common/hooks/useLocationOrderAheadAvailability', () => ({
    useLocationOrderAheadAvailability: jest.fn().mockReturnValue({ isLocationOrderAheadAvailable: true }),
}));

describe('menu category section', () => {
    it('Should render single and multi id categories', () => {
        render(<MenuCategorySection entry={entry as any} />);

        expect(screen.getByText(/Meals/i)).toBeInTheDocument();
        expect(screen.getByText(/Limited Time/i)).toBeInTheDocument();
    });

    it('Should not render unavailable category', () => {
        (useUnavailableCategories as jest.Mock).mockReturnValue(['1']);

        render(<MenuCategorySection entry={entry as any} />);

        expect(screen.queryByText(/Meals/i)).not.toBeInTheDocument();
        expect(screen.getByText(/Limited Time/i)).toBeInTheDocument();
    });
});
