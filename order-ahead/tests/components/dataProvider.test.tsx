const getContentfulConfigurationExpApiMock = jest.fn();

import React from 'react';
import { render, waitFor } from '@testing-library/react';
import DataProvider from '../../components/dataProvider';
import { useNotifications } from '../../redux/hooks';
import { useLocationUnavailableError } from '../../common/hooks/useLocationUnavailableError';
import { useGtmErrorEvent } from '../../common/hooks/useGtmErrorEvent';
import { useAuth0 } from '@auth0/auth0-react';
import { ORDER_AHEAD_NOT_AVAIABLE_MESSAGE } from '../../common/constants/orderAhead';
import configurationMock from '../mocks/configuration.mock';

jest.mock('../../redux/hooks', () => ({
    __esModule: true,
    useOrderLocation: jest.fn().mockReturnValue({
        pickupAddress: {},
        method: 'PICKUP',
        actions: {
            setPickupLocation: jest.fn(),
            setAvailableTimeSlots: jest.fn(),
            flushDeliveryLocation: jest.fn(),
        },
    }),
    useAccount: jest.fn().mockReturnValue({
        actions: {
            setAccount: jest.fn(),
        },
    }),
    useDomainMenu: jest.fn().mockReturnValue({
        products: [],
        actions: {
            getDomainMenu: jest.fn(),
        },
    }),
    useNotifications: jest.fn().mockReturnValue({
        actions: { enqueueError: jest.fn() },
    }),
    useRewards: jest.fn().mockReturnValue({
        actions: {
            setRewards: jest.fn(),
            setRewardsLoading: jest.fn(),
            setRewardsActivityHistory: jest.fn(),
            setRewardsActivityHistoryLoading: jest.fn(),
        },
    }),
    useLoyalty: jest.fn().mockReturnValue({
        actions: {
            setLoyaltyPointsLoading: jest.fn(),
            setLoyaltyPoints: jest.fn(),
        },
        loyalty: { pointsBalance: 3000 },
    }),
    useSubmitOrder: jest.fn().mockReturnValue({
        lastOrder: {},
    }),
    useSelectedSell: jest.fn().mockReturnValue({
        actions: {
            setCorrelationId: jest.fn(),
        },
    }),
    useOrderHistory: jest.fn().mockReturnValue({
        actions: {
            setOrderHistory: jest.fn(),
            setOrderHistoryLoading: jest.fn(),
        },
    }),
    usePersonalization: jest.fn().mockReturnValue({
        actions: {
            initializePersonalizationDependency: jest.fn(),
        },
    }),
    useTallyOrder: jest.fn().mockReturnValue({
        setUnavailableTallyItems: jest.fn(),
    }),
    useMyTeams: jest.fn().mockReturnValue({
        actions: {
            setMyTeams: jest.fn(),
            setMyTeamsLoading: jest.fn(),
        },
    }),
}));

jest.mock('../../common/hooks/useConfiguration', () => {
    return {
        createConfiguration: jest.fn(),
        useConfiguration: () => ({
            configuration: {
                isOAEnabled: false,
                isDeliveryEnabled: true,
                configurationRefreshFrequency: 10,
            },
            actions: {
                setConfiguration: jest.fn(),
            },
        }),
    };
});

jest.mock('../../common/services/locationService');
jest.mock('@auth0/auth0-react', () => ({
    __esModule: true,
    useAuth0: jest.fn().mockReturnValue({
        isAuthenticated: true,
        user: {},
        logout: jest.fn(),
        getIdTokenClaims: jest.fn().mockResolvedValue({ __raw: 'token' }),
    }),
}));
jest.mock('../../common/services/customerService/account', () => ({
    __esModule: true,
    initAccountService: jest.fn().mockReturnValue({
        getAccount: jest.fn(),
    }),
}));
jest.mock('../../common/services/customerService/deals');
jest.mock('../../redux/hooks/useFeatureFlags', () => {
    return {
        __esModule: true,
        useFeatureFlags: jest.fn().mockReturnValue({
            featureFlags: {
                account: true,
                locationTimeSlotsEnabled: true,
            },
        }),
    };
});
jest.mock('../../common/hooks/useLocationUnavailableError', () => ({
    useLocationUnavailableError: jest.fn().mockReturnValue({
        pushLocationUnavailableError: jest.fn(),
    }),
}));
jest.mock('../../common/hooks/useGtmErrorEvent');
jest.mock('../../common/helpers/accountHelper');
jest.mock('../../@generated/webExpApi/models');

jest.mock('../../common/services/locationService/getLocationById', () => {
    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue({
            id: '1542',
            isDigitallyEnabled: false,
            isClosed: true,
            contactDetails: {
                phone: '777-77-7777',
            },
        }),
    };
});

jest.mock('../../common/services/contentfulConfiguration/contentfulConfiguration', () => ({
    getContentfulConfigurationExpApi: getContentfulConfigurationExpApiMock,
}));

describe('DataProvider component', () => {
    beforeAll(() => {
        (useGtmErrorEvent as jest.Mock).mockReturnValue({ pushGtmErrorEvent: jest.fn() });
    });

    it('should call enqueueError auth0 logout method when user is authenticated but idpCustomerId is missing from jwt', async () => {
        const enqueueErrorMock = jest.fn();
        const logoutMock = jest.fn();
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce(['token', jest.fn()]);

        (useNotifications as jest.Mock).mockReturnValueOnce({
            actions: {
                enqueueError: enqueueErrorMock,
            },
        });
        (useAuth0 as jest.Mock).mockReturnValueOnce({
            isAuthenticated: true,
            user: {},
            logout: logoutMock,
            getIdTokenClaims: jest.fn().mockResolvedValue({ __raw: 'token' }),
        });
        render(<DataProvider />);

        expect(enqueueErrorMock).toBeCalledWith({
            message: 'Please try to sign in again',
            title: 'Something went wrong',
        });
        expect(logoutMock).toBeCalledWith({ returnTo: 'http://www.test.app.url' });

        useStateMock.mockRestore();
    });

    it('should call pushLocationUnavailableError when isDigitlEnabled false', async () => {
        const pushLocationErrorMock = jest.fn();
        (useLocationUnavailableError as jest.Mock).mockReturnValueOnce({
            pushLocationUnavailableError: pushLocationErrorMock,
        });

        render(<DataProvider />);
        await waitFor(() => expect(pushLocationErrorMock).toHaveBeenCalledTimes(1));
        await waitFor(() =>
            expect(pushLocationErrorMock).toHaveBeenCalledWith({
                id: '1542',
                isDigitallyEnabled: false,
                isClosed: true,
                contactDetails: {
                    phone: '777-77-7777',
                },
            })
        );
    });

    it('should not have been called pushLocationUnavailableError when isDigitlEnabled true', async () => {
        const pushLocationErrorMock = jest.fn();
        (useLocationUnavailableError as jest.Mock).mockReturnValueOnce({
            pushLocationUnavailableError: pushLocationErrorMock,
        });

        render(<DataProvider />);
        await waitFor(() => expect(pushLocationErrorMock).not.toHaveBeenCalled());
    });

    it('should display error message when isOrderAhead false', async () => {
        const enqueueErrorMock = jest.fn();
        (useNotifications as jest.Mock).mockReturnValueOnce({
            actions: {
                enqueueError: enqueueErrorMock,
            },
        });

        render(<DataProvider />);
        await waitFor(() =>
            expect(enqueueErrorMock).toBeCalledWith({
                message: ORDER_AHEAD_NOT_AVAIABLE_MESSAGE,
            })
        );
    });

    it('should make configuration endpoint call at start', () => {
        getContentfulConfigurationExpApiMock.mockClear();
        render(<DataProvider />);
        expect(getContentfulConfigurationExpApiMock).toHaveBeenCalledTimes(1);
    });

    it('should make configuration endpoint call on timeout', async () => {
        // Async testing with jest fake timers and promises
        // https://gist.github.com/apieceofbart/e6dea8d884d29cf88cdb54ef14ddbcc4
        const flushPromises = () => new Promise((res) => process.nextTick(res));

        jest.useFakeTimers('legacy');
        jest.spyOn(global, 'setTimeout');

        render(<DataProvider />);

        await flushPromises();

        expect(setTimeout).toHaveBeenLastCalledWith(
            expect.any(Function),
            configurationMock.configurationRefreshFrequency * 1000 * 60
        );

        jest.useRealTimers();
    });
});
