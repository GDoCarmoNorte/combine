export const nonDismissibleBanner = {
    fields: {
        text: 'Our response to covid-19',
        link: {
            sys: {
                space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                id: '01htGrdWbTPs2yVo0DXUOT',
                type: 'Entry',
                createdAt: '2020-07-01T14:40:03.802Z',
                updatedAt: '2020-08-28T12:55:16.250Z',
                environment: { sys: { id: 'master', type: 'Link', linkType: 'Environment' } },
                revision: 8,
                contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'page' } },
                locale: 'en-US',
            },
            fields: { name: 'Covid19', nameInUrl: 'covid-19', section: [] },
        },
        backgroundColor: 'var(--col--primary1)',
        textColor: '#ffffff',
        isDismissible: false,
        showOnCheckout: false,
        bannerId: '1',
    },
};

export const dismissibleBanner = {
    fields: {
        text: 'Our response to covid-19',
        link: {
            sys: {
                space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
                id: '01htGrdWbTPs2yVo0DXUOT',
                type: 'Entry',
                createdAt: '2020-07-01T14:40:03.802Z',
                updatedAt: '2020-08-28T12:55:16.250Z',
                environment: { sys: { id: 'master', type: 'Link', linkType: 'Environment' } },
                revision: 8,
                contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'page' } },
                locale: 'en-US',
            },
            fields: { name: 'Covid19', nameInUrl: 'covid-19', section: [] },
        },
        backgroundColor: 'var(--col--primary1)',
        textColor: '#ffffff',
        isDismissible: true,
        showOnCheckout: false,
        bannerId: '2',
    },
};
