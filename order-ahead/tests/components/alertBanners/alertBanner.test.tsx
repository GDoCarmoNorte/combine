import React from 'react';
import { shallow } from 'enzyme';

import AlertBanner from '../../../components/organisms/alertBanners/alertBanner';
import { nonDismissibleBanner, dismissibleBanner } from './alertBanner.mock';

const dismissedAlertBannersMock = jest.fn();

jest.mock('../../../redux/hooks', () => ({
    useDismissedAlertBanners: () => ({
        bannerEntries: [],
        actions: {
            dismissBanner: dismissedAlertBannersMock,
        },
    }),
}));

describe('Alert Banner', () => {
    it('should render dismissible alert banner', () => {
        const { text, backgroundColor, textColor, isDismissible, bannerId } = dismissibleBanner.fields;
        const link = nonDismissibleBanner.fields.link as any;
        const wrapper = shallow(
            <AlertBanner
                text={text}
                link={link}
                backgroundColor={backgroundColor}
                textColor={textColor}
                isDismissible={isDismissible}
                bannerId={bannerId}
                onCloseClick={jest.fn()}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should call onCLoseLink by click', () => {
        const { text, backgroundColor, textColor, isDismissible, bannerId } = dismissibleBanner.fields;
        const onCloseClickMock = jest.fn();
        const wrapper = shallow(
            <AlertBanner
                text={text}
                backgroundColor={backgroundColor}
                textColor={textColor}
                isDismissible={isDismissible}
                bannerId={bannerId}
                onCloseClick={onCloseClickMock}
            />
        );

        wrapper.find('[aria-label="Close"]').simulate('click');

        expect(onCloseClickMock).toHaveBeenCalledWith(bannerId);
    });

    it('should call onCLoseLink by Enter press', () => {
        const { text, backgroundColor, textColor, isDismissible, bannerId } = dismissibleBanner.fields;
        const onCloseClickMock = jest.fn();
        const wrapper = shallow(
            <AlertBanner
                text={text}
                backgroundColor={backgroundColor}
                textColor={textColor}
                isDismissible={isDismissible}
                bannerId={bannerId}
                onCloseClick={onCloseClickMock}
            />
        );

        wrapper.find('[aria-label="Close"]').simulate('keypress', { key: 'Enter' });

        expect(onCloseClickMock).toHaveBeenCalledWith(bannerId);
    });

    it('should not call onCLoseLink by other key press', () => {
        const { text, backgroundColor, textColor, isDismissible, bannerId } = dismissibleBanner.fields;
        const onCloseClickMock = jest.fn();
        const wrapper = shallow(
            <AlertBanner
                text={text}
                backgroundColor={backgroundColor}
                textColor={textColor}
                isDismissible={isDismissible}
                bannerId={bannerId}
                onCloseClick={onCloseClickMock}
            />
        );

        wrapper.find('[aria-label="Close"]').simulate('keypress', { key: 'Space' });

        expect(onCloseClickMock).not.toHaveBeenCalled();
    });

    it('should render non-dismissible alert banner', () => {
        const { text, backgroundColor, textColor, isDismissible, bannerId } = nonDismissibleBanner.fields;
        const link = nonDismissibleBanner.fields.link as any;
        const wrapper = shallow(
            <AlertBanner
                text={text}
                link={link}
                backgroundColor={backgroundColor}
                textColor={textColor}
                isDismissible={isDismissible}
                bannerId={bannerId}
                onCloseClick={jest.fn()}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
