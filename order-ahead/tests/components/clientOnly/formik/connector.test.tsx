import React from 'react';
import { mount } from 'enzyme';
import { Form, Formik } from 'formik';

import { createConnector } from '../../../../components/clientOnly/formik/connector';
import { actImmediate } from './formikTestUtils';

describe('formik connector', () => {
    it('formik connected should executo callback on form change', async () => {
        const FormikConnector = createConnector<{ test: string }>();
        const changeHandler = jest.fn();
        const noop = jest.fn();

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <input name="test" />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        const input = wrapper.find('input').at(0);

        expect(input).toBeDefined();

        input.simulate('change', { target: { name: 'test', value: 'test' } });

        await actImmediate(wrapper);

        expect(changeHandler).toHaveBeenCalled();

        wrapper.unmount();
    });
});
