import { ReactWrapper } from 'enzyme';
import { act } from 'react-dom/test-utils';

// Helper function - uses act() under the hood from react-dom/test-utils
// Formik specific test issue, see - https://dev.to/dannypule/updating-formik-fields-when-testing-using-jest-and-enzyme-or-react-testing-library-4hb1
export const actImmediate = async (wrapper: ReactWrapper): Promise<void> =>
    // setImmediate not supported in jest 27+
    act(
        () =>
            new Promise<void>((resolve) => {
                wrapper.update();
                resolve();
            })
    );
