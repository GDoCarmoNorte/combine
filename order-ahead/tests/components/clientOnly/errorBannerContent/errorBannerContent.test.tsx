import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';
import ErrorBannerContent from '../../../../components/clientOnly/errorBannerContent/errorBannerContent';
import entryMock from '../../sections/errorBanner/errorBanner.mock';

jest.mock('../../../../redux/hooks/useGlobalProps');

jest.mock('../../../../lib/gtm', () => ({
    getGtmIdByName: () => 'errorBanner',
}));

describe('errorBannerContent', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should render the error banner content properly', () => {
        const wrapper = shallow(<ErrorBannerContent fields={entryMock.fields as any} enableGoBack />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render GO BACK button', () => {
        render(<ErrorBannerContent fields={entryMock.fields as any} enableGoBack />);

        expect(screen.getByText(/GO BACK/i)).toBeInTheDocument();
    });

    it('should not render GO BACK button', () => {
        render(<ErrorBannerContent fields={entryMock.fields as any} />);

        expect(screen.queryByText(/GO BACK/i)).not.toBeInTheDocument();
    });
});
