import React from 'react';
import { render, screen } from '@testing-library/react';

import CheckoutError from '../../../../components/clientOnly/checkoutError/checkoutError';

describe('Checkout Error', () => {
    it('should render component without title and message', () => {
        render(<CheckoutError />);

        screen.getByText('PAYMENT DECLINED:');
        screen.getByText('Your payment was declined and your order was not submitted. Please try again.');
    });

    it('should render component with title and message', () => {
        render(<CheckoutError message="MESSAGE" title="TITLE" />);

        screen.getByText('TITLE:');
        screen.getByText('MESSAGE');
    });

    it('should render component with error object', () => {
        render(<CheckoutError error={new Error('ERROR MESSAGE')} />);

        screen.getByText('PAYMENT DECLINED:');
        screen.getByText('ERROR MESSAGE');
    });
});
