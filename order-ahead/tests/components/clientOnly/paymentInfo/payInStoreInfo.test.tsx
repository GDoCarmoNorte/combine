import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import PayInStoreInfo from '../../../../components/clientOnly/paymentInfoContainer/payInStoreInfo';

jest.mock('../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));

describe('PayInStoreInfo component', () => {
    const mockOnSubmit = jest.fn();

    it('should render properly', () => {
        const { container } = render(<PayInStoreInfo onSubmit={mockOnSubmit} isActive={false} />);
        expect(container).toMatchSnapshot();
    });

    it('should render button with "CONFIRM" text', () => {
        render(<PayInStoreInfo onSubmit={mockOnSubmit} isActive={true} />);
        const confirmButton = screen.getByRole('button', { name: 'Confirm' });
        expect(confirmButton).toBeInTheDocument();
        fireEvent.click(confirmButton);
        expect(mockOnSubmit).toHaveBeenCalled();
    });

    it('should disable button if isActive equals false', () => {
        render(<PayInStoreInfo onSubmit={mockOnSubmit} isActive={false} />);
        const confirmButton = screen.getByRole('button', { name: 'Confirm' });
        expect(confirmButton).toBeDisabled();
    });
});
