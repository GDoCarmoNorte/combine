import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as constants from '../../../../../components/clientOnly/deleteAccount/feedback/constants';
import {
    TITLE_TEXT_MOCK,
    MAIN_TEXT_MOCK,
    FEEDBACK_QUESTION_TEXT_MOCK,
    FEEDBACK_REASONS_MOCK,
    CANCEL_BUTTON_TEXT_MOCK,
    CONTINUE_BUTTON_TEXT_MOCK,
} from './feedback.mock';
import Feedback from '../../../../../components/clientOnly/deleteAccount/feedback';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useDispatch: () => mockDispatch,
}));

jest.mock('../../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
}));

describe('Feedback', () => {
    beforeEach(() => {
        // @ts-ignore
        constants.TITLE_TEXT = TITLE_TEXT_MOCK;
        // @ts-ignore
        constants.MAIN_TEXT = MAIN_TEXT_MOCK;
        // @ts-ignore
        constants.FEEDBACK_QUESTION_TEXT = FEEDBACK_QUESTION_TEXT_MOCK;
        // @ts-ignore
        constants.FEEDBACK_REASONS = FEEDBACK_REASONS_MOCK;
        // @ts-ignore
        constants.CANCEL_BUTTON_TEXT = CANCEL_BUTTON_TEXT_MOCK;
        // @ts-ignore
        constants.CONTINUE_BUTTON_TEXT = CONTINUE_BUTTON_TEXT_MOCK;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render and show text and button', () => {
        render(
            <Feedback
                onSetFeedback={jest.fn()}
                onSetFreeForm={jest.fn()}
                onContinue={jest.fn()}
                onCancel={jest.fn()}
                feedback={null}
            />
        );

        expect(screen.getByText(/titletext/i)).toBeInTheDocument();
        expect(screen.getByText(/maintext/i)).toBeInTheDocument();
        expect(screen.getByText(/fbquestiontext/i)).toBeInTheDocument();
        expect(screen.getByText(/reason1/i)).toBeInTheDocument();
        expect(screen.getByText(/reason2/i)).toBeInTheDocument();
        expect(screen.getByText(/cancelbutton/i)).toBeInTheDocument();
        expect(screen.getByText(/continuebutton/i)).toBeInTheDocument();
    });

    it('should handle checkbox selection', () => {
        const onSetFeedbackMock = jest.fn();

        render(
            <Feedback
                onSetFeedback={onSetFeedbackMock}
                onSetFreeForm={jest.fn()}
                onContinue={jest.fn()}
                onCancel={jest.fn()}
                feedback={null}
            />
        );

        userEvent.click(screen.getByText(/reason1/i));

        expect(onSetFeedbackMock).toHaveBeenCalledWith('reason1');
    });

    it('should handle unselecting radio buttons', () => {
        const onSetFeedbackMock = jest.fn();

        render(
            <Feedback
                onSetFeedback={onSetFeedbackMock}
                onSetFreeForm={jest.fn()}
                onContinue={jest.fn()}
                onCancel={jest.fn()}
                feedback={null}
            />
        );

        userEvent.click(screen.getByText(/reason1/i));
        userEvent.click(screen.getByText(/reason1/i));
        userEvent.click(screen.getByText(/reason2/i));

        expect(onSetFeedbackMock).toHaveBeenCalledTimes(3);
    });

    it('should handle onCancel after click on cancel button', () => {
        const onCancelMock = jest.fn();

        render(
            <Feedback
                onSetFeedback={jest.fn()}
                onSetFreeForm={jest.fn()}
                onContinue={jest.fn()}
                onCancel={onCancelMock}
                feedback={null}
            />
        );

        userEvent.click(screen.getByText(/cancelButton/i));

        expect(onCancelMock).toHaveBeenCalled();
    });

    it('should not handle onContinue if reason not selected', () => {
        const onContinueMock = jest.fn();

        render(
            <Feedback
                onSetFeedback={jest.fn()}
                onSetFreeForm={jest.fn()}
                onContinue={onContinueMock}
                onCancel={jest.fn()}
                feedback={null}
            />
        );

        userEvent.click(screen.getByText(/continueButton/i));
        expect(onContinueMock).not.toHaveBeenCalled();
    });

    it('should handle onContinue', () => {
        const onContinueMock = jest.fn();

        render(
            <Feedback
                onSetFeedback={jest.fn()}
                onSetFreeForm={jest.fn()}
                onContinue={onContinueMock}
                onCancel={jest.fn()}
                feedback={'reason'}
            />
        );

        userEvent.click(screen.getByText(/continueButton/i));
        expect(onContinueMock).toHaveBeenCalled();
    });
});
