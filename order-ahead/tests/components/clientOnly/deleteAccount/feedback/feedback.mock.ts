export const TITLE_TEXT_MOCK = 'titleText';
export const MAIN_TEXT_MOCK = 'mainText';
export const FEEDBACK_QUESTION_TEXT_MOCK = 'fbQuestionText';
export const FEEDBACK_REASONS_MOCK = ['reason1', 'reason2'];
export const CANCEL_BUTTON_TEXT_MOCK = 'cancelButton';
export const CONTINUE_BUTTON_TEXT_MOCK = 'continueButton';
