import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as constants from '../../../../../components/clientOnly/deleteAccount/deletePermissions/constants';
import {
    TITLE_TEXT_MOCK,
    MAIN_TEXT_MOCK,
    CANCEL_BUTTON_TEXT_MOCK,
    SUBMIT_BUTTON_TEXT_MOCK,
} from './deletePermissions.mock';
import DeletePermissions from '../../../../../components/clientOnly/deleteAccount/deletePermissions';

describe('DeletePermissions', () => {
    beforeEach(() => {
        // @ts-ignore
        constants.TITLE_TEXT = TITLE_TEXT_MOCK;
        // @ts-ignore
        constants.MAIN_TEXT = MAIN_TEXT_MOCK;
        // @ts-ignore
        constants.CANCEL_BUTTON_TEXT = CANCEL_BUTTON_TEXT_MOCK;
        // @ts-ignore
        constants.SUBMIT_BUTTON_TEXT = SUBMIT_BUTTON_TEXT_MOCK;
    });
    //onToggleDeletePersonalData, onToggleDeleteConfirmation, onCancel, onSubmit deletePersonalData, deleteConfirmation
    it('should render and show text and button', () => {
        render(
            <DeletePermissions
                onToggleDeletePersonalData={jest.fn()}
                onToggleDeleteConfirmation={jest.fn()}
                onSubmit={jest.fn()}
                onCancel={jest.fn()}
                deletePersonalData={false}
                deleteConfirmation={false}
            />
        );

        expect(screen.getByText(/titletext/i)).toBeInTheDocument();
        expect(screen.getByText(/maintext/i)).toBeInTheDocument();
        expect(screen.getByText(/cancelbutton/i)).toBeInTheDocument();
        expect(screen.getByText(/submitbutton/i)).toBeInTheDocument();
    });

    it('should handle pressing the cancel button', () => {
        const onCancel = jest.fn();

        render(
            <DeletePermissions
                onToggleDeletePersonalData={jest.fn()}
                onToggleDeleteConfirmation={jest.fn()}
                onSubmit={jest.fn()}
                onCancel={onCancel}
                deletePersonalData={false}
                deleteConfirmation={false}
            />
        );

        userEvent.click(screen.getByText(/cancelbutton/i));

        expect(onCancel).toHaveBeenCalled();
    });

    it('should not handle pressing the submit button if delete confirmation is false', () => {
        const onSumbit = jest.fn();

        render(
            <DeletePermissions
                onToggleDeletePersonalData={jest.fn()}
                onToggleDeleteConfirmation={jest.fn()}
                onSubmit={onSumbit}
                onCancel={jest.fn()}
                deletePersonalData={false}
                deleteConfirmation={false}
            />
        );

        userEvent.click(screen.getByText(/submitbutton/i));

        expect(onSumbit).not.toHaveBeenCalled();
    });

    it('should handle pressing the submit button but only if delete confirmation is true', () => {
        const onSumbit = jest.fn();

        render(
            <DeletePermissions
                onToggleDeletePersonalData={jest.fn()}
                onToggleDeleteConfirmation={jest.fn()}
                onSubmit={onSumbit}
                onCancel={jest.fn()}
                deletePersonalData={false}
                deleteConfirmation={true}
            />
        );

        userEvent.click(screen.getByText(/submitbutton/i));

        expect(onSumbit).toHaveBeenCalled();
    });
});
