import { screen, render } from '@testing-library/react';
import React from 'react';
import DeleteConfirmation from '../../../../../components/clientOnly/deleteAccount/deleteConfirmation/deleteConfirmation';
import userEvent from '@testing-library/user-event';

describe('deleteConfirmation', () => {
    const onCancel = jest.fn();
    const onDelete = jest.fn();

    it('should render buttons and text', () => {
        render(<DeleteConfirmation onCancel={onCancel} onDelete={onDelete} />);

        expect(
            screen.getByText(/If you delete your account now, you will lose access to exclusive offers and deals./i)
        ).toBeInTheDocument();
        expect(screen.getByText(/are you sure/i)).toBeInTheDocument();
        expect(screen.getAllByText(/delete account/i)[2]).toBeInTheDocument();
        expect(screen.getByText(/cancel/i)).toBeInTheDocument();
    });

    it('should handle cancel buttons click', () => {
        render(<DeleteConfirmation onCancel={onCancel} onDelete={onDelete} />);

        userEvent.click(screen.getAllByText(/delete account/i)[2]);
        expect(onDelete).toBeCalledTimes(1);

        userEvent.click(screen.getByText(/cancel/i));
        expect(onCancel).toBeCalledTimes(1);
    });
});
