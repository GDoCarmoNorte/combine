import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as constants from '../../../../../components/clientOnly/deleteAccount/deleteFailure/constants';
import * as mock from './deleteFailure.mock';
import DeleteFailure from '../../../../../components/clientOnly/deleteAccount/deleteFailure';

describe('DeleteFailure', () => {
    beforeEach(() => {
        // @ts-ignore
        constants.TITLE_TEXT = mock.TITLE_TEXT_MOCK;
        // @ts-ignore
        constants.PRE_LINK_TEXT = mock.PRE_LINK_TEXT_MOCK;
        // @ts-ignore
        constants.LINK_TEXT = mock.LINK_TEXT_MOCK;
        // @ts-ignore
        constants.LINK = mock.LINK_MOCK;
        // @ts-ignore
        constants.BUTTON_TEXT = mock.BUTTON_TEXT_MOCK;
    });

    it('should render and show text and button', () => {
        render(<DeleteFailure onRetry={jest.fn()} />);

        expect(screen.getByText(/title text/i)).toBeInTheDocument();
        expect(screen.getByText(/pre link text/i)).toBeInTheDocument();
        expect(screen.getByText(/the link text/i)).toBeInTheDocument();

        expect(screen.getByText(/try again/i)).toBeInTheDocument();
    });

    it('should handle onRetry after click on retry button', () => {
        const onRetryMock = jest.fn();

        render(<DeleteFailure onRetry={onRetryMock} />);

        userEvent.click(screen.getByText(/try again/i));

        expect(onRetryMock).toHaveBeenCalled();
    });
});
