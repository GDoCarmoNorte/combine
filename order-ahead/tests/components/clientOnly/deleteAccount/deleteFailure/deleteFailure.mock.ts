export const TITLE_TEXT_MOCK = 'Title Text';
export const PRE_LINK_TEXT_MOCK = 'Pre Link Text';
export const LINK_TEXT_MOCK = 'The Link Text';
export const LINK_MOCK = '../link';
export const BUTTON_TEXT_MOCK = 'Button Text';
