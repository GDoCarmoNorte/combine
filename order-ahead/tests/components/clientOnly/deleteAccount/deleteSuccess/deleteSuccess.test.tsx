import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as constants from '../../../../../components/clientOnly/deleteAccount/deleteSuccess/constants';
import { TITLE_TEXT_MOCK, MAIN_TEXT_MOCK, BUTTON_TEXT_MOCK } from './deleteSuccess.mock';
import DeleteSuccess from '../../../../../components/clientOnly/deleteAccount/deleteSuccess';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useDispatch: () => mockDispatch,
}));

jest.mock('../../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
}));

describe('DeleteSuccess', () => {
    beforeEach(() => {
        // @ts-ignore
        constants.TITLE_TEXT = TITLE_TEXT_MOCK;
        // @ts-ignore
        constants.MAIN_TEXT = MAIN_TEXT_MOCK;
        // @ts-ignore
        constants.BUTTON_TEXT = BUTTON_TEXT_MOCK;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render and show text and button', () => {
        render(<DeleteSuccess onFinish={jest.fn()} />);

        expect(screen.getByText(/title text/i)).toBeInTheDocument();
        expect(screen.getByText(/main text 1/i)).toBeInTheDocument();
        expect(screen.getByText(/main text 2/i)).toBeInTheDocument();

        expect(screen.getByText(/button text/i)).toBeInTheDocument();
    });

    it('should handle onFinish after click on finish button', () => {
        const onFinishMock = jest.fn();

        render(<DeleteSuccess onFinish={onFinishMock} />);

        userEvent.click(screen.getByText(/button text/i));

        expect(onFinishMock).toHaveBeenCalled();
    });
});
