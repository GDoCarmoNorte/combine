import React from 'react';
import { render, screen } from '@testing-library/react';

import ContactlessPayError from '../../../../components/clientOnly/contactlessPayError/contactlessPayError';
import { TContactlessPayErrorTypes } from '../../../../components/clientOnly/contactlessPayError/types';

describe('Checkout Error', () => {
    it('should render component with with disabled messaging', () => {
        render(<ContactlessPayError errorType={TContactlessPayErrorTypes['DISABLED']} />);

        screen.getByText(/uh oh/i);
        screen.getByText(/service not available/i);
        screen.getByText(/please pay using terminal or ask a restaurant staff/i);
    });

    it('should render component with default messaging', () => {
        render(<ContactlessPayError errorType={TContactlessPayErrorTypes['DEFAULT']} />);

        screen.getByText(/uh oh/i);
        screen.getByText(/order error/i);
        screen.getByText(/Sorry, we can't find your order. Retry your link or ask your server for assistance./i);
    });

    it('should render component with default messaging when given an invalid type', () => {
        render(<ContactlessPayError errorType={'invalid' as TContactlessPayErrorTypes} />);

        screen.getByText(/uh oh/i);
        screen.getByText(/order error/i);
        screen.getByText(/Sorry, we can't find your order. Retry your link or ask your server for assistance./i);
    });
});
