import React from 'react';
import { render, screen } from '@testing-library/react';
import { OrderLocationMethod } from '../../../../redux/orderLocation';
import { useDispatch } from 'react-redux';
import { useOrderLocation, useSubmitOrder } from '../../../../redux/hooks';
import OrderInfo from '../../../../components/clientOnly/orderInfo/orderInfo';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useSubmitOrder');
jest.mock('next/router', () => ({
    useRouter: jest.fn().mockReturnValue({
        query: { id: '13' },
    }),
}));

describe('OrderInfo component', () => {
    it('should render properly', () => {
        (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            currentLocation: { timezone: 'America/New_York' },
        });
        (useSubmitOrder as jest.Mock).mockReturnValue({
            error: null,
            isLoading: false,
            lastOrder: { orderId: '13', fulfillment: { time: new Date('2021-02-23T08:48:36.123Z') } },
            submitOrder: jest.fn(),
            resetSubmitOrder: jest.fn(),
        });

        render(<OrderInfo method={OrderLocationMethod.PICKUP} backgroundColor="" />);
        expect(screen.getByText(/your order will be ready for PICKUP/i)).toBeInTheDocument();
        expect(screen.getByText(/Confirmation #13/i)).toBeInTheDocument();
    });

    it('should render without method label text', () => {
        (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            currentLocation: { timezone: 'America/New_York' },
        });
        (useSubmitOrder as jest.Mock).mockReturnValue({
            lastOrder: null,
        });

        render(<OrderInfo method={OrderLocationMethod.PICKUP} backgroundColor="" />);
        expect(screen.getByText(/your order will be ready for/i)).toBeInTheDocument();
    });
});
