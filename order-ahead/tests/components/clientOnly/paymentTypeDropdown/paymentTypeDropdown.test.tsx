import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PaymentTypeSelectBox from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/paymentTypeDropdown';

import { TInitialPaymentTypes } from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

import { useCustomerPaymentMethod } from '../../../../common/hooks/useCustomerPaymentMethod';
import useOrderLocation from '../../../../redux/hooks/useOrderLocation';
import {
    isCreditOrDebitPayEnabled,
    isPlaceholderPaymentOptionEnabled,
    isCardOnFilePayEnabled,
} from '../../../../lib/getFeatureFlags';
import { useTallyOrder } from '../../../../redux/hooks';
import { TAccountPaymentMethodTypeModel } from '../../../../@generated/webExpApi';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useTallyOrder');
jest.mock('../../../../common/hooks/useCustomerPaymentMethod');
jest.mock('../../../../lib/getFeatureFlags');
jest.mock('../../../../common/hooks/useConfiguration', () => ({
    useConfiguration: () => ({
        configuration: {
            isPayAtStoredEnabled: true,
        },
    }),
}));

jest.mock('../../../../redux/store', () => ({
    __esModule: true,
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));

(useTallyOrder as jest.Mock).mockReturnValue({ tallyOrder: { products: [], total: '30' } });

(useOrderLocation as jest.Mock).mockReturnValue({
    currentLocation: { storeId: 'storeId' },
    pickupAddress: { id: 'id' },
});

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = (): void => {};
const creditCard = {
    cardHolderName: 'JOHN DOE',
    type: TAccountPaymentMethodTypeModel.Vi,
    lastFourDigits: '411111xxxxxx1111',
    expirationDate: '05/2022',
    isDefault: true,
    isExpired: false,
    token: '1234567890',
};

describe('PaymentInfoContainer', () => {
    it('should render properly', () => {
        (isCreditOrDebitPayEnabled as jest.Mock).mockImplementation(() => true);
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            loading: false,
            paymentMethods: null,
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
        });

        const selectedMethod = { type: TInitialPaymentTypes['CREDIT_OR_DEBIT'] };
        const { container } = render(<PaymentTypeSelectBox onChange={noop} selectedMethod={selectedMethod} />);
        expect(container).toMatchSnapshot();
    });

    it('should render properly when isCreditOrDebitPayEnabled is off', () => {
        (isCreditOrDebitPayEnabled as jest.Mock).mockImplementation(() => false);
        (isPlaceholderPaymentOptionEnabled as jest.Mock).mockImplementation(() => true);
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            loading: false,
            paymentMethods: null,
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
        });

        const selectedMethod = { type: TInitialPaymentTypes['PLACEHOLDER'] };
        const { container } = render(<PaymentTypeSelectBox onChange={noop} selectedMethod={selectedMethod} />);
        expect(container).toMatchSnapshot();
    });

    it('should render properly for PAY_IN_STORE', () => {
        (isCreditOrDebitPayEnabled as jest.Mock).mockImplementation(() => false);
        (isPlaceholderPaymentOptionEnabled as jest.Mock).mockImplementation(() => false);
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            loading: false,
            paymentMethods: null,
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            currentLocation: {
                additionalFeatures: {
                    isPayAtStoreEnabled: true,
                    payAtStoreMaxAmount: 175,
                },
            },
        });

        const selectedMethod = { type: TInitialPaymentTypes['PAY_IN_STORE'] };
        const { container } = render(<PaymentTypeSelectBox onChange={noop} selectedMethod={selectedMethod} />);
        expect(container).toMatchSnapshot();
    });

    it('should render card name with label when it is provided', async () => {
        const label = 'personal';
        (isCardOnFilePayEnabled as jest.Mock).mockImplementation(() => true);
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            loading: false,
            paymentMethods: { cREDITCARDS: [{ ...creditCard, label }] },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({ isPickUp: true });

        render(<PaymentTypeSelectBox onChange={noop} selectedMethod={{ type: TInitialPaymentTypes['PLACEHOLDER'] }} />);

        userEvent.click(screen.getByRole('button'));

        await waitFor(() => {
            expect(screen.getByText(`${label} ****1111`)).toBeInTheDocument();
        });
    });

    it('should render card name without label when it is not provided', async () => {
        (isCardOnFilePayEnabled as jest.Mock).mockImplementation(() => true);
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            loading: false,
            paymentMethods: { cREDITCARDS: [creditCard] },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({ isPickUp: true });

        render(<PaymentTypeSelectBox onChange={noop} selectedMethod={{ type: TInitialPaymentTypes['PLACEHOLDER'] }} />);

        userEvent.click(screen.getByRole('button'));

        await waitFor(() => {
            expect(screen.getByText('Credit Card ****1111')).toBeInTheDocument();
        });
    });
});
