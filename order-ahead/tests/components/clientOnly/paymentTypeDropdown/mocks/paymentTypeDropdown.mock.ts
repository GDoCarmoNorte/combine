import {
    TInitialPaymentTypes,
    IInitialPaymentTypeSelectionOption,
} from '../../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

export const allInitialPaymentTypesMock: IInitialPaymentTypeSelectionOption[] = [
    {
        type: 'OPTION_ONE' as TInitialPaymentTypes,
        image: 'option one image',
        text: 'option one text',
    },
    {
        type: 'OPTION_TWO' as TInitialPaymentTypes,
        image: 'option two image',
        text: 'åoption two text',
    },
    {
        type: 'OPTION_THREE' as TInitialPaymentTypes,
        image: 'option three image',
        text: 'åoption three text',
    },
];
