import React from 'react';
import { useRouter } from 'next/router';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import CookieBanner from '../../../../components/clientOnly/cookieBanner/cookieBanner';

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({}),
}));

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

describe('CookieBanner', () => {
    it('should render and show text and button', () => {
        (useRouter as jest.Mock).mockReturnValue({ pathname: 'show-banner-page' });

        render(<CookieBanner onClose={jest.fn()} />);

        screen.getByText(/Cookie Policy/i);

        screen.getByText(/CLOSE/i);
    });

    it('should not render banner if not show array includes page', () => {
        (useRouter as jest.Mock).mockReturnValue({ pathname: 'checkout' });

        render(<CookieBanner onClose={jest.fn()} />);

        const child = screen.queryByText(/Cookie Policy/i);

        expect(child).not.toBeInTheDocument();
    });

    it('should handle onClose after click on close button', () => {
        const onCloseMock = jest.fn();
        (useRouter as jest.Mock).mockReturnValue({ pathname: 'show-banner-page' });

        render(<CookieBanner onClose={onCloseMock} />);

        userEvent.click(screen.getByText(/CLOSE/i));

        expect(onCloseMock).toHaveBeenCalled();
    });
});
