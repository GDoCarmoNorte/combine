import React from 'react';
import { mount, shallow } from 'enzyme';

import UnsupportedBrowser from '../../../../components/clientOnly/unsupportedBrowser/unsupportedBrowser';
import { IGlobalContentfulProps } from '../../../../common/services/globalContentfulProps';
import { IPageFields } from '../../../../@generated/@types/contentful';
import { Entry } from 'contentful';
import logger from '../../../../common/services/logger';

jest.mock('../../../../common/services/logger');

let type = undefined;

jest.mock('ua-parser-js', () => {
    return jest.fn().mockImplementation(() => {
        return {
            getBrowser: () => ({ name: 'Chrome', version: '90.0.4430.212' }),
            getDevice: () => ({ type }),
            getUA: () =>
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
            getOS: () => ({ name: 'Windows', version: '10' }),
        };
    });
});

describe('UnsupportedBrowser', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(
            <UnsupportedBrowser
                isMobile={false}
                page={{} as Entry<IPageFields>}
                globalProps={{} as IGlobalContentfulProps}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should call logger.logEvent for unsupported browser', () => {
        mount(
            <UnsupportedBrowser
                isMobile={false}
                page={{} as Entry<IPageFields>}
                globalProps={{} as IGlobalContentfulProps}
            />
        );

        expect(logger.logEvent).toHaveBeenCalled();
        expect(logger.logEvent).toHaveBeenCalledWith('unsupported_browser', {
            appUA:
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
            appUAName: 'Chrome',
            appUAOS: { name: 'Windows', version: '10' },
            appUAType: 'desktop',
            appUAVersion: '90.0.4430.212',
        });
    });

    it('should call logger.logEvent with {type: "mobile"} for unsupported browser', () => {
        type = 'mobile';
        mount(
            <UnsupportedBrowser
                isMobile={false}
                page={{} as Entry<IPageFields>}
                globalProps={{} as IGlobalContentfulProps}
            />
        );

        expect(logger.logEvent).toHaveBeenCalled();
        expect(logger.logEvent).toHaveBeenCalledWith('unsupported_browser', {
            appUA:
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36',
            appUAName: 'Chrome',
            appUAOS: { name: 'Windows', version: '10' },
            appUAType: 'mobile',
            appUAVersion: '90.0.4430.212',
        });
    });
});
