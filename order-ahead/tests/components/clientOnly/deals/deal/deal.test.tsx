import React from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Deal from '../../../../../components/clientOnly/deals/deal/deal';

jest.mock('@material-ui/core/useMediaQuery', () => jest.fn().mockReturnValue(false));
jest.mock('../../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));

const dealPropsMock = {
    title: 'Roast Beef',
    label: 'IN STORE ONLY',
    expires: 'Expires 05/27',
    description:
        'A short description of the deal within one or two sentences, it is better to keep the description small.',
    primaryCtaText: 'REDEEM in store',
    secondaryCtaText: 'REDEEM online',
    image: 'Bacon_Stips.png',
    terms: 'Lorem ipsum dolor sit amet',
    showQr: false,
    onPrimaryCtaClick: jest.fn(),
    onSecondaryCtaClick: jest.fn(),
};

const dealPropsMockQr = {
    ...dealPropsMock,
    showQr: true,
    qrImage:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXIAAAFyCAAAAADNTw7VAAADlElEQVR42u3bQXLDIBAEQP//08kHXC6WHRYn6jk6kQyNLiPw60eG80KAHLkgRy7IkQty5MgFOXJBjlyQIxfkyJELcuSCHLkgR45ckCMX5MgFOXJBjhy5IEcuyJFLi/wVSf1+7674/Nnn0U/MDTly5MiRI0eOHDly5MiR98k3Km4Zq8Nbv0t6bsiRI0eOHDly5MiRI0eOPEneqeqrxTs99foyZOaGHDly5MiRI0eOHDly5MhvkE9sVWeWATly5MiRI0eOHDly5MiR/4fCn1muzusA5MiRI0eOHDly5MiRI0f+zeQT16bZ7s8NOXLkyJEjR44cOXLkyJGv3LqTVaJbn2Xmhhw5cuTIkSNHjhw5cuTId8nTyZTxDuC1mSNHjhw5cuTIkSNHjhw58gWiVbZ69c8cK+4ct87MHDly5MiRI0eOHDly5MiR98nTQ6h/78Y0G1vfyJEjR44cOXLkyJEjR458jnwC69xf79d85MiRI0eOHDly5MiRI0d+ZoATy9Cp+XXo0FIjR44cOXLkyJEjR44cOfIj5BNHjc/xprfSkSNHjhw5cuTIkSNHjhx5MvXyvDrNztHlzouBzoyQI0eOHDly5MiRI0eOHPlpyvTPWydeDHzVT22RI0eOHDly5MiRI0eO/DHkmZJ9rvCfq/5fcNgZOXLkyJEjR44cOXLkyB9IninZ6XLfWcJMkUeOHDly5MiRI0eOHDly5H3ydD3OLEhnaSZeXCBHjhw5cuTIkSNHjhw58mThz0BnXiVksDrzRY4cOXLkyJEjR44cOXLkycKf3tjNbAVnNrKRI0eOHDly5MiRI0eOHPld8vqwOldkvrc+vnMbz8iRI0eOHDly5MiRI0eOfC6Z4Xc2qOv36/wfcuTIkSNHjhw5cuTIkSPfrfmZ8pzmSBf5zoOAHDly5MiRI0eOHDly5Mh3ySfKeJ2t/sBkrhgp/MiRI0eOHDly5MiRI0f+aPJOoc4sdX1BZpcaOXLkyJEjR44cOXLkyJHfJc8sV2eBM3f+Q4UfOXLkyJEjR44cOXLkyB9Nfm6LN13VO2NBjhw5cuTIkSNHjhw5cuRnbt2p1qtsndI+sejIkSNHjhw5cuTIkSNHjnyXPHNIOLOdmz7inHlMkCNHjhw5cuTIkSNHjhz5Lrmkghw5ckGOXJAjF+TIkQty5IIcuSBHLsiRIxfkyAU5ckGOHLkgRy7IkQty5IIcOXJBjlyQI5e3+QVaI1bsABY4CwAAAABJRU5ErkJggg==',
    qrDescription: 'Please show QR code to cashier.',
};

describe('Deal component', () => {
    it('should render deal', () => {
        const { container } = render(<Deal {...(dealPropsMock as any)} />);

        expect(container).toMatchSnapshot();
    });

    it('should render deal without CTAs', () => {
        const { container } = render(
            <Deal {...(dealPropsMock as any)} primaryCtaText={undefined} secondaryCtaText={undefined} />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render QR', () => {
        const { container } = render(<Deal {...(dealPropsMockQr as any)} />);

        expect(container).toMatchSnapshot();
    });

    it('should call cta clicks', () => {
        const onPrimaryCtaClickMock = jest.fn();
        const onSecondaryCtaClickMock = jest.fn();

        render(
            <Deal
                {...(dealPropsMock as any)}
                onPrimaryCtaClick={onPrimaryCtaClickMock}
                onSecondaryCtaClick={onSecondaryCtaClickMock}
            />
        );

        userEvent.click(screen.getByText(dealPropsMock.primaryCtaText));
        expect(onPrimaryCtaClickMock).toHaveBeenCalledTimes(1);

        userEvent.click(screen.getByText(dealPropsMock.secondaryCtaText));
        expect(onSecondaryCtaClickMock).toHaveBeenCalledTimes(1);
    });
});
