import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useRouter } from 'next/router';
import useOrderLocation from '../../../../redux/hooks/useOrderLocation';
import LocationInfo from '../../../../components/clientOnly/locationInfo/locationInfo';
import deliveryLocationMock from '../../../mocks/deliveryAddess.mock';
import DataMock from '../../../mocks/expLocations.mock';
import useSubmitOrder from '../../../../redux/hooks/useSubmitOrder';

jest.mock('../../../../redux/domainMenu', () => ({
    useDomainProduct: () => ({}),
    useDomainProducts: () => [],
}));
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useSubmitOrder');
jest.mock('../../../../components/clientOnly/locationInfo/constants', () => {
    const originalModule = jest.requireActual('../../../../components/clientOnly/locationInfo/constants');

    return {
        __esModule: true,
        ...originalModule,
        SHOW_ORDER_STATUS_BUTTON: true,
    };
});

(useSubmitOrder as jest.Mock).mockReturnValue({});

describe('LocationInfo component', () => {
    const routerPushMock = jest.fn();

    it('should match snapshot', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            pickupAddress: null,
            isPickUp: true,
        });
        const view = render(<LocationInfo />);

        expect(view).toMatchSnapshot();
    });

    it('should renders correctly with location and pickup method', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            pickupAddress: DataMock.locations[0],
            isPickUp: true,
        });
        (useRouter as jest.Mock).mockReturnValue({
            push: routerPushMock,
        });
        render(<LocationInfo />);

        const changeLocationButton = screen.getByText(/Change Location/i);

        expect(screen.getByText(/Pickup Location/i)).toBeInTheDocument();
        expect(changeLocationButton).toBeInTheDocument();
        expect(screen.getByText(/Chardon – Water St/i)).toBeInTheDocument();

        userEvent.click(changeLocationButton);
        expect(routerPushMock).toBeCalled();
    });

    it('should renders correctly with location and delivery method', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            deliveryAddress: deliveryLocationMock,
            isPickUp: false,
        });
        (useRouter as jest.Mock).mockReturnValue({
            push: routerPushMock,
        });

        render(<LocationInfo isPickUp={false} />);

        const changeAddressButton = screen.getByText(/Change Address/i);

        expect(screen.getByText(/Delivery Address/i)).toBeInTheDocument();
        expect(changeAddressButton).toBeInTheDocument();
        expect(screen.getByText(/7947 Golden Valley Rd/i)).toBeInTheDocument();

        userEvent.click(changeAddressButton);
        expect(routerPushMock).toBeCalled();
    });

    it('should renders correctly when viewOnly prop is true', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            pickupAddress: DataMock.locations[0],
            isPickUp: true,
        });
        render(<LocationInfo viewOnly={true} />);

        expect(screen.getByText(/\(440\) 286-7515/i)).toBeInTheDocument();
        expect(screen.getByText(/Get Directions/i)).toBeInTheDocument();
    });

    describe('BWW:', () => {
        it('should render `order status` button for delivery method', () => {
            (useSubmitOrder as jest.Mock).mockReturnValue({
                lastOrder: {
                    fulfillment: {
                        deliveryTrackingUrl: 'https://test.com',
                    },
                },
            });

            render(<LocationInfo viewOnly={true} isPickUp={false} />);

            expect(screen.getByText(/order status/i)).toBeInTheDocument();
        });

        it('should not render `order status` button without tracking url', () => {
            (useSubmitOrder as jest.Mock).mockReturnValue({
                lastOrder: {
                    fulfillment: {
                        deliveryTrackingUrl: null,
                    },
                },
            });
            render(<LocationInfo viewOnly={true} isPickUp={false} />);

            expect(screen.queryByText(/order status/i)).not.toBeInTheDocument();
        });

        it('should not render `order status` button for pickup method', () => {
            render(<LocationInfo viewOnly={true} isPickUp={true} />);

            expect(screen.queryByText(/order status/i)).not.toBeInTheDocument();
        });
    });
});
