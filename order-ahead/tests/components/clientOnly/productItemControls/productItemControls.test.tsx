import React from 'react';
import { shallow } from 'enzyme';

import { InspireLink } from '../../../../components/atoms/link';
import ProductItemControls from '../../../../components/clientOnly/productItemControls/productItemControls';
import { GTM_MODIFY_PRODUCT } from '../../../../common/services/gtmService/constants';

const dispatchMock = jest.fn();
const toggleIsOpen = jest.fn();

jest.mock('react-redux', () => ({
    useDispatch: () => dispatchMock,
}));

jest.mock('../../../../redux/hooks', () => ({
    useDomainMenu: jest.fn(() => ({ loading: false })).mockImplementationOnce(() => ({ loading: true })),
    usePdp: jest.fn(() => ({ actions: { resetPdpState: jest.fn() } })),
}));

const productMock = { fields: 'productMock' };
const defaultProps = { isSaleable: true, isAvailable: true };

describe('ProductItemControls', () => {
    it('should render product item controls with loading state', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={{} as any}
                product={productMock as any}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render add to bag button with link viewtype', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={{} as any}
                product={productMock as any}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render view item link with link viewtype', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable={false}
                productDetails={{} as any}
                product={productMock as any}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render add to bag and modify buttons with link viewtype for combo', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isCombo
                isOrderAheadAvailable
                productDetails={{ modifierGroups: ['modifierGroup'] } as any}
                product={productMock as any}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render add to bag button with button viewtype', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={undefined}
                product={productMock as any}
                viewType="button"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render view item link with button viewtype', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable={false}
                productDetails={{} as any}
                product={productMock as any}
                viewType="button"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render add to bag button with button viewtype for combo', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isCombo
                isOrderAheadAvailable
                productDetails={{ modifierGroups: ['modifierGroup'] } as any}
                product={productMock as any}
                viewType="button"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should dispatch gtm modify event after click on modify link', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                isCombo
                productDetails={{ modifierGroups: ['modifierGroup'] } as any}
                product={productMock as any}
            />
        );

        wrapper.find(InspireLink).simulate('click');

        expect(dispatchMock).toHaveBeenCalledTimes(1);
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MODIFY_PRODUCT });
    });

    it('should close bag after click on modify link for recommended product card', async () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                isCombo
                productDetails={{ modifierGroups: ['modifierGroup'] } as any}
                product={productMock as any}
                onModify={() => toggleIsOpen()}
            />
        );

        wrapper.find(InspireLink).simulate('click');

        expect(await toggleIsOpen).toHaveBeenCalledTimes(1);
    });

    it('should render only view item button when item is not saleable', () => {
        const wrapper = shallow(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable={true}
                productDetails={{} as any}
                product={productMock as any}
                viewType="button"
                isSaleable={false}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
