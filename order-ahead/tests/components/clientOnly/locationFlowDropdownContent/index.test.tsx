import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';

import LocationFlowDropdownContent from '../../../../components/clientOnly/locationFlowDropdown/locationFlowDropdownContent';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useAppSelector } from '../../../../redux/store';
import { useOrderLocation, useConfiguration } from '../../../../redux/hooks';
import { OrderLocationMethod } from '../../../../redux/orderLocation';
import configFeatureFlagsMocks from '../../../mocks/configFeatureFlag.mock';

jest.mock('react-redux');
jest.mock('../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: () => jest.fn(),
}));
jest.mock('../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));
jest.mock('../../../../redux/hooks/useBag', () => jest.fn().mockReturnValue({}));
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));
jest.mock('@material-ui/core/useMediaQuery', () => jest.fn().mockReturnValue(false));

jest.mock('../../../../redux/hooks/useConfiguration');

jest.mock('../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmLocationOrder: jest.fn(),
        pushGtmChangeLocation: jest.fn(),
    }),
}));

describe('LocationFlowDropdownContent component', () => {
    beforeEach(() => {
        (useAppSelector as jest.Mock).mockReturnValue(configFeatureFlagsMocks);
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: configFeatureFlagsMocks,
        });
        (useOrderLocation as jest.Mock).mockReturnValue({ method: OrderLocationMethod.NOT_SELECTED } as any);
    });

    it('should render "START AN ORDER" button for desktop if location does not exist', async () => {
        render(<LocationFlowDropdownContent isLocationSelected={false} />);

        await waitFor(() => {
            expect(screen.getByText(/start an order/i)).toBeInTheDocument();
        });
    });

    it('should render location dropdown "Find an Arby\'s" for mobile if location does not exist', async () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(true);

        render(<LocationFlowDropdownContent />);

        await waitFor(() => {
            expect(screen.getByText(/Select Location/i)).toBeInTheDocument();
        });

        expect(screen.getByText(/Find an Arby’s/i)).toBeInTheDocument();
    });

    it('should render location dropdown if location exists', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: { storeId: '99984' },
        } as any);

        render(<LocationFlowDropdownContent isLocationSelected={true} />);

        await waitFor(() => {
            expect(screen.getByText(/My Arby’s/i)).toBeInTheDocument();
        });
    });

    it('should render tooltip for desktop if location does not exist', async () => {
        render(
            <LocationFlowDropdownContent
                isLocationSelected={false}
                titleTooltip="Select your location to use the deals"
            />
        );

        await waitFor(() => {
            expect(screen.getByText(/start an order/i)).toBeInTheDocument();
        });

        expect(screen.getByText(/Select your location to use the deals/i)).toBeInTheDocument();
    });

    it('should render tooltip for mobile if location does not exist', async () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(true);

        render(<LocationFlowDropdownContent titleTooltip="Select your location to use the deals" />);

        await waitFor(() => {
            expect(screen.getByText(/Select Location/i)).toBeInTheDocument();
        });

        expect(screen.getByText(/Find an Arby’s/i)).toBeInTheDocument();
        expect(screen.getByText(/Select your location to use the deals/i)).toBeInTheDocument();
    });

    it('should not render tooltip if location exists', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: { storeId: '99984' },
        } as any);

        render(<LocationFlowDropdownContent isLocationSelected={true} />);

        await waitFor(() => {
            expect(screen.getByText(/My Arby’s/i)).toBeInTheDocument();
        });

        expect(screen.queryByText(/Select your location to use the deals/i)).not.toBeInTheDocument();
    });
});
