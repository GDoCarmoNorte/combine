import React from 'react';
import { render } from '@testing-library/react';

import Marker from '../../../../components/clientOnly/map/marker';

describe('marker', () => {
    it('should render marker', () => {
        const { container } = render(<Marker place={'locationMock' as any} selected={false} lat={0} lng={0} />);

        expect(container).toMatchSnapshot();
    });

    it('should render selected marker', () => {
        const { container } = render(<Marker place={'locationMock' as any} selected={true} lat={0} lng={0} />);

        expect(container).toMatchSnapshot();
    });

    it('should render marker with order', () => {
        const { container } = render(
            <Marker place={'locationMock' as any} selected={false} listNumber={1} lat={0} lng={0} />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render selected marker with order', () => {
        const { container } = render(
            <Marker place={'locationMock' as any} selected={true} listNumber={1} lat={0} lng={0} />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render marker with delivery pin', () => {
        const { container } = render(
            <Marker place={'locationMock' as any} selected={true} listNumber={1} lat={0} lng={0} isDeliveryMarker />
        );

        expect(container).toMatchSnapshot();
    });
});
