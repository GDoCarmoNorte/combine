import React from 'react';
import { shallow, mount } from 'enzyme';
import locationsMock from '../../../mocks/expLocations.mock';
import { GTM_MAP_CLICK, GTM_MAP_DOUBLE_CLICK } from '../../../../common/services/gtmService/constants';
import InspireMap from '../../../../components/clientOnly/map';
import { isLocationsShowAsOrderedList } from '../../../../lib/getFeatureFlags';

const placeMock = locationsMock.locations[0];

jest.mock('../../../../common/hooks/useWidth', () => jest.fn().mockReturnValue('md'));
jest.mock('google-map-react', () => {
    return {
        __esModule: true,
        // eslint-disable-next-line react/display-name
        default: ({ onClick }) => {
            return (
                <div className="google-map-react-mock" onClick={onClick}>
                    map mock
                </div>
            );
        },
    };
});
jest.mock('../../../../lib/getFeatureFlags', () => ({
    isLocationsShowAsOrderedList: jest.fn().mockReturnValue(false),
}));

const dispatchMock = jest.fn();
jest.mock('react-redux', () => ({
    useDispatch: () => dispatchMock,
}));

describe('InspireMap', () => {
    afterEach(() => {
        dispatchMock.mockReset();
    });

    it('should match snapshot with empty locations list', () => {
        const wrapper = shallow(<InspireMap locations={[]} place={null} onLocationSelect={jest.fn()} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should have element with className mapPlaceHolder with empty locations list', () => {
        const wrapper = shallow(<InspireMap locations={[]} place={null} onLocationSelect={jest.fn()} />);
        expect(wrapper.find('div.mapPlaceHolder').length).toBe(1);
    });
    it('should have element with className mapPlaceHolder with undefined locations', () => {
        const wrapper = shallow(<InspireMap locations={null} place={null} onLocationSelect={jest.fn()} />);
        expect(wrapper.find('div.mapPlaceHolder').length).toBe(1);
    });

    it('should match snapshot with selected place', () => {
        const wrapper = shallow(
            <InspireMap locations={[placeMock] as any} place={placeMock as any} onLocationSelect={jest.fn()} />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot with "isDelivery" option', () => {
        const wrapper = shallow(<InspireMap locations={[placeMock] as any} place={placeMock as any} isDelivery />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should call dispatch via handleDoubleClick on double click', () => {
        const wrapper = mount(
            <InspireMap locations={[placeMock] as any} place={placeMock as any} onLocationSelect={jest.fn()} />
        );
        wrapper.simulate('dblclick');
        expect(dispatchMock).toHaveBeenCalledTimes(1);
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DOUBLE_CLICK });
    });

    it('should call dispatch via handleClick on marker click', () => {
        const wrapper = mount(
            <InspireMap locations={[placeMock] as any} place={placeMock as any} onLocationSelect={jest.fn()} />
        );
        wrapper.find('.google-map-react-mock').simulate('click');
        expect(dispatchMock).toHaveBeenCalledTimes(1);
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_CLICK });
    });

    it('should have element with className mapPlaceHolder when  error is present', () => {
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce([true, jest.fn()])
            .mockReturnValueOnce([[placeMock], jest.fn()])
            .mockReturnValueOnce([null, jest.fn()]);

        const wrapper = shallow(
            <InspireMap locations={[placeMock] as any} place={placeMock as any} onLocationSelect={jest.fn()} />
        );
        expect(wrapper.find('div.mapPlaceHolder').length).toBe(1);
    });

    it('should display markers with order if "showLocationsAsOrderedList" is enabled', () => {
        (isLocationsShowAsOrderedList as jest.Mock).mockReturnValueOnce(true);

        const wrapper = shallow(
            <InspireMap locations={[placeMock] as any} place={placeMock as any} onLocationSelect={jest.fn()} />
        );
        expect(wrapper).toMatchSnapshot();
    });
});
