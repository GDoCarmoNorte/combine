import React from 'react';
import { mount } from 'enzyme';
import { screen, render } from '@testing-library/react';
import PaymentInfo from '../../../components/clientOnly/paymentInfoContainer/paymentInfo';

import { usePayment, UsePaymentHook } from '../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../common/hooks/useFreedomPay';
import logger from '../../../common/services/logger';
import { useOrderLocation, useGlobalProps } from '../../../redux/hooks';
import { TInitialPaymentTypes } from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

jest.mock('../../../common/hooks/usePayment');
jest.mock('../../../common/hooks/useFreedomPay');
jest.mock('../../../redux/hooks');
jest.mock('../../../common/services/logger');

describe('PaymentInfo', () => {
    beforeEach(() => {
        (useOrderLocation as jest.Mock).mockReturnValue({ locationEntry: { storeId: 'storeId' } });
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
        (usePayment as jest.Mock).mockReturnValue({
            payment: { sessionKey: 'sessionKey', iframePayload: 'iframePayload' },
        });
        (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
    });

    it('should match snapshot', () => {
        const wrapper = mount(
            <PaymentInfo
                isActive
                onFormStatusChange={jest.fn()}
                paymentType={TInitialPaymentTypes.CREDIT_OR_DEBIT}
                innnerHtml={{ __html: 'iframePayload' }}
                paymentPayload={{ payment: {} } as UsePaymentHook}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should show loader', () => {
        const wrapper = mount(
            <PaymentInfo
                isActive
                onFormStatusChange={jest.fn()}
                paymentType={TInitialPaymentTypes.CREDIT_OR_DEBIT}
                innnerHtml={{ __html: 'iframePayload' }}
                paymentPayload={{} as UsePaymentHook}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should tick checkbox on label click', () => {
        const wrapper = mount(
            <PaymentInfo
                isActive
                onFormStatusChange={jest.fn()}
                paymentType={TInitialPaymentTypes.CREDIT_OR_DEBIT}
                innnerHtml={{ __html: 'iframePayload' }}
                paymentPayload={{ payment: {} } as UsePaymentHook}
            />
        );
        const input = wrapper.find('input[type="checkbox"]');

        expect(input.props().checked).toBe(false);

        input.simulate('change', { target: { checked: true } });

        expect(wrapper.find('input[type="checkbox"]').props().checked).toBe(true);
    });

    it('should tick checkbox on checkbox click', () => {
        const wrapper = mount(
            <PaymentInfo
                isActive
                onFormStatusChange={jest.fn()}
                paymentType={TInitialPaymentTypes.CREDIT_OR_DEBIT}
                innnerHtml={{ __html: 'iframePayload' }}
                paymentPayload={{ payment: {} } as UsePaymentHook}
            />
        );
        const input = wrapper.find('input[type="checkbox"]');

        expect(input.props().checked).toBe(false);

        wrapper.find({ role: 'checkbox' }).simulate('click');

        expect(wrapper.find('input[type="checkbox"]').props().checked).toBe(true);
    });

    describe('when error', () => {
        beforeAll(() => {
            render(
                <PaymentInfo
                    isActive
                    onFormStatusChange={jest.fn()}
                    paymentType={TInitialPaymentTypes.CREDIT_OR_DEBIT}
                    innnerHtml={{ __html: 'iframePayload' }}
                    paymentPayload={{ error: {} } as UsePaymentHook}
                />
            );
        });

        it('should show error message when error', () => {
            screen.getByText(/Connection error/i);
            screen.getByText(/Temporarily unavailable/i);
            screen.getByText(/The operation could not be completed. Please refresh and try again./i);
        });

        it('should log "payment_iframe_not_displayed" NR event w/ "type" = "CREDIT_OR_DEBIT" and message displayed to user', () => {
            expect(logger.logEvent).toHaveBeenCalledWith('payment_iframe_not_displayed', {
                type: 'CREDIT_OR_DEBIT',
                message: {
                    textError: 'Connection error',
                    header: 'Temporarily unavailable',
                    description: 'The operation could not be completed. Please refresh and try again.',
                },
            });
        });
    });
});
