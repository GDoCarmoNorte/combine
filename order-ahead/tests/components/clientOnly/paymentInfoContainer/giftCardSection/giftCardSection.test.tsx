import React from 'react';
import { screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import GiftCardSection from '../../../../../components/clientOnly/paymentInfoContainer/giftCardSection/giftCardSection';

import { usePayment } from '../../../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../../../common/hooks/useFreedomPay';
import useBwwCardBalance from '../../../../../common/hooks/useBwwCardBalance';
import { useOrderLocation, useGlobalProps } from '../../../../../redux/hooks';

jest.mock('../../../../../redux/hooks');
jest.mock('../../../../../common/hooks/usePayment');
jest.mock('../../../../../common/hooks/useFreedomPay');
jest.mock('../../../../../common/hooks/useBwwCardBalance');
jest.mock('../../../../../common/services/logger');

describe('giftCardSection', () => {
    beforeAll(() => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                storeId: 'storeId',
            },
        });
        (useBwwCardBalance as jest.Mock).mockReturnValue({
            isBalanceLoading: false,
            getBalance: jest.fn(),
            balance: 10,
        });
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
        (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
        (usePayment as jest.Mock).mockReturnValue({});
        (usePayment as jest.Mock).mockReturnValue({
            payment: { iframeHtml: '<div>iframe</div>' },
        });
    });
    const mockFirstGiftCard = {
        paymentToken: {
            keys: ['key'],
            cardIssuer: 'test issuer',
            maskedCardNumber: '4111111111111111',
        },
        balance: 15,
    };

    const mockSecondGiftCard = {
        paymentToken: {
            keys: ['key'],
            cardIssuer: 'test second issuer',
            maskedCardNumber: '5111111111111111',
        },
        balance: 20,
    };

    const getComponent = ({ setGiftCards = jest.fn(), giftCards = [], totalToPay = 0 }) => (
        <GiftCardSection setGiftCards={setGiftCards} giftCards={giftCards} totalToPay={totalToPay} />
    );

    it('should show add gift card text if have no gift cards', () => {
        render(getComponent({}));

        expect(screen.getByText(/Pay with gift card/i)).toBeInTheDocument();
        expect(screen.getByText(/Max of 2/i)).toBeInTheDocument();
    });

    it('should show gift card and message if totalTo pay equals 0', () => {
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([true, jest.fn()]);

        render(getComponent({ giftCards: [mockFirstGiftCard] }));

        expect(screen.getByText(/test issuer/i)).toBeInTheDocument();
        expect(screen.getByText(/\*\*\*\*\*\*\*\*\*\*\*\*1111/i)).toBeInTheDocument();
        expect(screen.getByText(/Balance:/i)).toBeInTheDocument();
        expect(screen.getByText(/15/i)).toBeInTheDocument();
        expect(
            screen.getByText(/Score! Your gift card is covering the bill. No additional payment needed./i)
        ).toBeInTheDocument();
        expect(screen.queryByText(/Add another gift card/i)).not.toBeInTheDocument();

        useStateMock.mockRestore();
    });

    it('should show two gift cards and message and remove second card after click on remove button', () => {
        const setAddGiftCardSelectedMock = jest.fn();
        const setAddAnotherGiftCardSelectedMock = jest.fn();
        const useStateMock = jest
            .spyOn(React, 'useState')
            .mockReturnValueOnce([true, setAddGiftCardSelectedMock])
            .mockReturnValueOnce([true, setAddAnotherGiftCardSelectedMock]);

        render(getComponent({ giftCards: [mockFirstGiftCard, mockSecondGiftCard] }));

        expect(screen.getByText(/test issuer/i)).toBeInTheDocument();
        expect(screen.getByText(/15/i)).toBeInTheDocument();

        expect(screen.getByText(/test second issuer/i)).toBeInTheDocument();
        expect(screen.getByText(/20/i)).toBeInTheDocument();
        expect(
            screen.getByText(/Score! Your gift cards are covering the bill. No additional payment needed./i)
        ).toBeInTheDocument();

        const removeBtnSecondGiftCard = screen.getAllByText(/remove/i)[1];

        userEvent.click(removeBtnSecondGiftCard);

        expect(setAddAnotherGiftCardSelectedMock).toBeCalledWith(false);
        expect(setAddGiftCardSelectedMock).not.toBeCalled();

        useStateMock.mockRestore();
    });

    it('should show gift card and message if totalTo pay more than 0', () => {
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([true, jest.fn()]);

        render(getComponent({ giftCards: [mockFirstGiftCard], totalToPay: 5 }));

        expect(
            screen.getByText(
                /Score! The total order price after applying gift card is 5 \$. Please complete the rest of the payment./i
            )
        ).toBeInTheDocument();
        expect(screen.getByText(/Add another gift card/i)).toBeInTheDocument();

        useStateMock.mockRestore();
    });

    it('should call setAddGiftCardSelectedMock with true if no gift cards and addGiftCardSelected is ture', () => {
        const setAddGiftCardSelectedMock = jest.fn();
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([true, setAddGiftCardSelectedMock]);

        render(getComponent({ giftCards: [mockFirstGiftCard], totalToPay: 5 }));
        const addGiftCardButton = screen.getByLabelText('Pay with gift card');

        userEvent.click(addGiftCardButton);
        expect(setAddGiftCardSelectedMock).toBeCalledWith(true);
        useStateMock.mockRestore();
    });

    it('should call setAddGiftCardSelectedMock with false if there is one gift card', () => {
        const setAddGiftCardSelectedMock = jest.fn();
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([true, setAddGiftCardSelectedMock]);

        render(getComponent({ giftCards: [], totalToPay: 5 }));
        const addGiftCardButton = screen.getByLabelText('Pay with gift card');

        userEvent.click(addGiftCardButton);
        expect(setAddGiftCardSelectedMock).toBeCalledWith(false);
        useStateMock.mockRestore();
    });

    it('should call setAddAnotherGiftCardSelectedMock with true if no gift cards and addGiftCardSelected is ture', () => {
        const setAddAnotherGiftCardSelectedMock = jest.fn();
        const useStateMock = jest
            .spyOn(React, 'useState')
            .mockReturnValueOnce([true, jest.fn()])
            .mockReturnValueOnce([false, setAddAnotherGiftCardSelectedMock]);

        render(getComponent({ giftCards: [mockFirstGiftCard], totalToPay: 5 }));
        const addAnotherGiftCardButton = screen.getByLabelText('Add another gift card');

        userEvent.click(addAnotherGiftCardButton);
        expect(setAddAnotherGiftCardSelectedMock).toBeCalledWith(true);
        useStateMock.mockRestore();
    });

    it('should call setAddAnotherGiftCardSelectedMock with false if there is one gift card', () => {
        const setAddAnotherGiftCardSelectedMock = jest.fn();
        const useStateMock = jest
            .spyOn(React, 'useState')
            .mockReturnValueOnce([true, jest.fn()])
            .mockReturnValueOnce([true, setAddAnotherGiftCardSelectedMock]);

        render(getComponent({ giftCards: [mockFirstGiftCard], totalToPay: 5 }));
        const addAnotherGiftCardButton = screen.getByLabelText('Add another gift card');

        userEvent.click(addAnotherGiftCardButton);
        expect(setAddAnotherGiftCardSelectedMock).toBeCalledWith(false);
        useStateMock.mockRestore();
    });

    it('should call setAddAnotherGiftCardSelectedMock and setAddGiftCardSelectedMock after click on remove button', () => {
        const setGiftCardsMock = jest.fn();
        const setAddGiftCardSelectedMock = jest.fn();
        const setAddAnotherGiftCardSelectedMock = jest.fn();
        const useStateMock = jest
            .spyOn(React, 'useState')
            .mockReturnValueOnce([true, setAddGiftCardSelectedMock])
            .mockReturnValueOnce([false, setAddAnotherGiftCardSelectedMock]);

        render(getComponent({ giftCards: [mockFirstGiftCard], setGiftCards: setGiftCardsMock }));

        const removeBtn = screen.getByText(/remove/i);
        userEvent.click(removeBtn);
        expect(setAddAnotherGiftCardSelectedMock).toBeCalledWith(false);
        expect(setAddGiftCardSelectedMock).toBeCalledWith(false);
        expect(setGiftCardsMock).toBeCalled();

        useStateMock.mockRestore();
    });
});
