import React from 'react';
import { screen, render } from '@testing-library/react';

import GiftCardPaymentInfo from '../../../../../components/clientOnly/paymentInfoContainer/giftCardSection/giftCardPaymentInfo';
import { usePayment } from '../../../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../../../common/hooks/useFreedomPay';
import useBwwCardBalance from '../../../../../common/hooks/useBwwCardBalance';
import logger from '../../../../../common/services/logger';
import { useOrderLocation, useGlobalProps } from '../../../../../redux/hooks';

jest.mock('../../../../../redux/hooks');
jest.mock('../../../../../common/hooks/usePayment');
jest.mock('../../../../../common/hooks/useFreedomPay');
jest.mock('../../../../../common/hooks/useBwwCardBalance');
jest.mock('../../../../../common/services/logger');

describe('GiftCardPaymentInfo', () => {
    const handleApplyGiftCard = jest.fn();

    describe('when payment loading', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                storeId: 'storeId',
            },
        });
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
        (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
        (usePayment as jest.Mock).mockReturnValue({});
        (usePayment as jest.Mock).mockReturnValue({
            isLoading: true,
        });

        it('should show loader', () => {
            const { container } = render(<GiftCardPaymentInfo onApplyGiftCards={handleApplyGiftCard} />);
            expect(container).toMatchSnapshot();
        });
    });

    describe('when balance loading', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                storeId: 'storeId',
            },
        });
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
        (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
        (usePayment as jest.Mock).mockReturnValue({});
        (usePayment as jest.Mock).mockReturnValue({});
        (useBwwCardBalance as jest.Mock).mockReturnValue({ isBalanceLoading: true });

        it('should show loader', () => {
            const { container } = render(<GiftCardPaymentInfo onApplyGiftCards={handleApplyGiftCard} />);
            expect(container).toMatchSnapshot();
        });
    });

    describe('when NO payment error AND no payment either', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                storeId: 'storeId',
            },
        });
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
        (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
        (usePayment as jest.Mock).mockReturnValue({ isLoading: false });
        (useBwwCardBalance as jest.Mock).mockReturnValue({ isBalanceLoading: false });

        it('should show loader', () => {
            const { container } = render(<GiftCardPaymentInfo onApplyGiftCards={handleApplyGiftCard} />);
            expect(container).toMatchSnapshot();
        });
    });

    describe('when success iframe data', () => {
        beforeAll(() => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                currentLocation: {
                    storeId: 'storeId',
                },
            });
            (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
            (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
            (usePayment as jest.Mock).mockReturnValue({});
            (usePayment as jest.Mock).mockReturnValue({
                payment: { iframeHtml: '<div>hello</div>' },
            });
            render(<GiftCardPaymentInfo onApplyGiftCards={handleApplyGiftCard} />);
        });

        it('should show iframe content', () => {
            screen.getByText(/hello/i);
        });
        it('should NOT show error message', () => {
            expect(screen.queryByText(/Connection error/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/Temporarily unavailable/i)).not.toBeInTheDocument();
            expect(
                screen.queryByText(/The operation could not be completed. Please refresh and try again./i)
            ).not.toBeInTheDocument();
        });
    });

    describe('when error', () => {
        beforeAll(() => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                currentLocation: {
                    storeId: 'storeId',
                },
            });
            (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: [] });
            (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
            (usePayment as jest.Mock).mockReturnValue({});
            (usePayment as jest.Mock).mockReturnValue({
                error: {},
            });
            render(<GiftCardPaymentInfo onApplyGiftCards={handleApplyGiftCard} />);
        });

        it('should show error message when error', () => {
            screen.getByText(/Connection error/i);
            screen.getByText(/Temporarily unavailable/i);
            screen.getByText(/The operation could not be completed. Please refresh and try again./i);
        });

        it('should log "payment_iframe_not_displayed" NR event w/ "CREDIT_CARD" type and message displayed to user', () => {
            expect(logger.logEvent).toHaveBeenCalledWith('payment_iframe_not_displayed', {
                type: 'GIFT_CARD',
                message: {
                    textError: 'Connection error',
                    header: 'Temporarily unavailable',
                    description: 'The operation could not be completed. Please refresh and try again.',
                },
            });
        });
    });
});
