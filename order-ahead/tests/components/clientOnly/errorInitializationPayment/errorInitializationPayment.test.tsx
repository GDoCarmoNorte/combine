import React from 'react';
import { shallow } from 'enzyme';

import ErrorInitializationPayment from '../../../../components/clientOnly/errorInitializationPayment';

describe('ErrorInitializationPayment', () => {
    const props = {
        paymentError: {
            description: 'The operation could not be completed. Please refresh and try again.',
            header: 'Temporarily unavailable',
            status: '500',
        },
        onClick: jest.fn(),
    };
    it('should render error content correctly', () => {
        const wrapper = shallow(<ErrorInitializationPayment {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
});
