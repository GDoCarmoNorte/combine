import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import SavedCard from '../../../../components/clientOnly/paymentInfoContainer/savedCard/savedCard';
import { useCustomerPaymentMethod } from '../../../../common/hooks/useCustomerPaymentMethod';

import { usePayment } from '../../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { InspireButton } from '../../../../components/atoms/button';
import { TAccountPaymentMethodTypeModel } from '../../../../@generated/webExpApi';

jest.mock('../../../../components/atoms/button');
jest.mock('../../../../common/hooks/usePayment');
jest.mock('../../../../common/hooks/useFreedomPay');
jest.mock('../../../../common/hooks/useCustomerPaymentMethod');

describe('PaymentInfoContainer', () => {
    const creditCard = {
        type: TAccountPaymentMethodTypeModel.Vi,
        label: 'Visa',
        token: 'token',
    };
    beforeEach(() => {
        (usePayment as jest.Mock).mockReturnValue({ payment: {} });
        (useFreedomPay as jest.Mock).mockReturnValue({});
        (InspireButton as jest.Mock).mockImplementation((props) => (
            <button className="button">props: {JSON.stringify(props, null, 2)}</button>
        ));
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            loading: false,
            paymentMethods: { cREDITCARDS: [{ ...creditCard }] },
        });
    });

    it('should render loader if iframe is loading', () => {
        (usePayment as jest.Mock).mockReturnValue({});
        (useFreedomPay as jest.Mock).mockReturnValue({});

        const { container } = render(<SavedCard token={'token'} onSubmit={jest.fn()} />);

        expect(container).toMatchSnapshot();
    });

    it('should render error in payment has been initialized with an error', () => {
        (usePayment as jest.Mock).mockReturnValue({ error: 'error' });
        (useFreedomPay as jest.Mock).mockReturnValue({});

        const { container } = render(<SavedCard token={'token'} onSubmit={jest.fn()} />);

        expect(container).toMatchSnapshot();
    });

    it('should render content', () => {
        (usePayment as jest.Mock).mockReturnValue({ payment: {} });
        (useFreedomPay as jest.Mock).mockReturnValue({});

        const { container } = render(<SavedCard token={'token'} onSubmit={jest.fn()} isActive />);

        expect(container).toMatchSnapshot();
    });

    it('should enable pay button if terms checkbox is checked and freedom pay is enabled', async () => {
        (usePayment as jest.Mock).mockReturnValue({ payment: {} });
        (useFreedomPay as jest.Mock).mockReturnValue({ isValid: true });

        render(<SavedCard token={'token'} onSubmit={jest.fn()} isActive />);

        expect(screen.getByLabelText('Pay Button Overlay')).toHaveAttribute('aria-disabled', 'true');

        fireEvent.click(screen.getByRole('checkbox'));

        await waitFor(() => {
            expect(screen.getByLabelText('Pay Button Overlay')).toHaveAttribute('aria-disabled', 'false');
        });
    });

    it('should not submit payment if payment token is for card on file type and payment keys are provided', async () => {
        (usePayment as jest.Mock).mockReturnValue({ payment: { sessionKey: 'sessionKey' } });
        (useFreedomPay as jest.Mock).mockReturnValue({
            isValid: true,
            payment: { paymentType: 'CREDIT_OR_DEBIT', keys: ['key1', 'key2'] },
        });

        const onSubmitMock = jest.fn().mockResolvedValue(null);
        render(<SavedCard token={'token'} onSubmit={onSubmitMock} isActive />);

        await waitFor(() => {
            expect(onSubmitMock).not.toHaveBeenCalled();
        });
    });

    it('should submit payment if payment token is for card on file type and payment keys are provided', async () => {
        (usePayment as jest.Mock).mockReturnValue({ payment: { sessionKey: 'sessionKey' } });
        (useFreedomPay as jest.Mock).mockReturnValue({
            isValid: true,
            payment: { paymentType: 'CARD_ON_FILE', keys: ['key1', 'key2'] },
        });

        const onSubmitMock = jest.fn().mockResolvedValue(null);
        render(<SavedCard token={'token'} onSubmit={onSubmitMock} isActive />);

        await waitFor(() => {
            expect(onSubmitMock).toHaveBeenCalledWith({
                paymentType: 'CARD_ON_FILE',
                sessionKey: 'sessionKey',
                paymentKeys: ['key1', 'key2'],
                cardToken: 'token',
                cardIssuer: 'Visa',
            });
        });
    });

    it('should reset freedom pay if there is an error while submit payment', async () => {
        (usePayment as jest.Mock).mockReturnValue({ payment: { sessionKey: 'sessionKey' } });

        const resetFreedomPayMock = jest.fn();
        (useFreedomPay as jest.Mock).mockReturnValue({
            isValid: true,
            payment: { paymentType: 'CARD_ON_FILE', keys: ['key1', 'key2'] },
            reset: resetFreedomPayMock,
        });

        const onSubmitMock = jest.fn().mockRejectedValue(null);
        render(<SavedCard token={'token'} onSubmit={onSubmitMock} isActive />);

        await waitFor(() => {
            expect(resetFreedomPayMock).toHaveBeenCalled();
        });
    });
});
