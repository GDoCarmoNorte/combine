import React from 'react';
import { shallow } from 'enzyme';

import FooterMockData from './index.mock';
import Footer from '../../../components/organisms/footer';

describe('footer component', () => {
    it('should match snapshot', () => {
        const wrapper = shallow(<Footer footer={FooterMockData as any} />);

        expect(wrapper).toMatchSnapshot();
    });
    it('should not match snapshot', () => {
        const mockData = JSON.parse(JSON.stringify(FooterMockData));
        mockData.fields.hidden = true;

        const wrapper = shallow(<Footer footer={mockData as any} />);

        expect(wrapper).toMatchSnapshot();
    });
});
