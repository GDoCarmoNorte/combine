import React from 'react';
import { shallow } from 'enzyme';

import FooterMockData from './index.mock';
import LegalHintBar from '../../../components/organisms/footer/secondaryBlockHintBar';

describe('footer component', () => {
    describe('legalHintBar component', () => {
        it('should match snapshot', () => {
            const wrapper = shallow(<LegalHintBar footer={FooterMockData as any} />);

            expect(wrapper).toMatchSnapshot();
        });
    });
});
