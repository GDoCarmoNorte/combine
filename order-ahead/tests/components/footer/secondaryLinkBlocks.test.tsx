import React from 'react';
import { shallow } from 'enzyme';

import FooterMockData from './index.mock';
import LegalLinkBlocks from '../../../components/organisms/footer/secondaryBlockLinkBlocks';
import globalPropsMock from '../../mocks/globalProps.mock';

jest.mock('../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('footer component', () => {
    describe('legalLinkBlocks component', () => {
        it('should match snapshot', () => {
            jest.spyOn(React, 'useContext').mockReturnValueOnce({});

            const wrapper = shallow(<LegalLinkBlocks footer={FooterMockData as any} />);

            expect(wrapper).toMatchSnapshot();
        });
    });
});
