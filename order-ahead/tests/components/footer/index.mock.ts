import { IFooter } from '../../../@generated/@types/contentful';

export default ({
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '1WxEVIEv7xw7gKem3kGyna',
        type: 'Entry',
        createdAt: '2020-07-23T14:58:38.683Z',
        updatedAt: '2020-07-30T05:31:24.963Z',
        environment: {
            sys: {
                id: 'feature_DBBP-811',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 5,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'footer',
            },
        },
        locale: 'en-US',
    },
    fields: {
        name: "Arby's Footer",
        hidden: false,
        linksBlocks: [
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: '3Vc7w2ct6NT8sP5Al6VCrf',
                    type: 'Entry',
                    createdAt: '2020-07-29T11:37:27.076Z',
                    updatedAt: '2020-07-30T10:44:42.256Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-811',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 3,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'footerLinksBlock',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    title: "We're here when you need us",
                    links: [
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '1AG8caOagL4L1PIkqXSFLq',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:36:55.100Z',
                                updatedAt: '2020-07-29T11:36:55.100Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'FAQ',
                                nameInUrl: 'faq',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '5wcbm9E0z35nz0KDImVNy8',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:37:05.236Z',
                                updatedAt: '2020-07-29T11:37:05.236Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Contact',
                                nameInUrl: 'contact',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '1svQloP8ntsiWoNbXIqOSv',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:37:16.714Z',
                                updatedAt: '2020-07-29T11:37:16.714Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Press',
                                nameInUrl: 'press',
                            },
                        },
                    ],
                },
            },
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: 'Xx0nxbwM5Q6J03NVCKTt7',
                    type: 'Entry',
                    createdAt: '2020-07-29T11:39:26.061Z',
                    updatedAt: '2020-07-29T11:39:26.061Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-811',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'footerLinksBlock',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    title: 'We take our food seriously',
                    links: [
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '5awjiXJpqQchB8yXNSHP6T',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:38:14.476Z',
                                updatedAt: '2020-07-29T11:38:14.476Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Nutrition',
                                nameInUrl: 'nutrition',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '3xS4sXY3FAU53hMu9TuYnX',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:38:51.399Z',
                                updatedAt: '2020-07-29T11:38:51.399Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Food Quality',
                                nameInUrl: 'food-quality',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '42br5TLFCWcc5ZkbsqFqsI',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:39:13.162Z',
                                updatedAt: '2020-07-29T11:39:13.162Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Food Safety',
                                nameInUrl: 'food-safety',
                            },
                        },
                    ],
                },
            },
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: '31hGY36i3ATiiBc7Qar63L',
                    type: 'Entry',
                    createdAt: '2020-07-29T11:42:22.894Z',
                    updatedAt: '2020-07-29T11:42:22.894Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-811',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'footerLinksBlock',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Who we are and what we do',
                    links: [
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '1LiqHbbGIi9CWwviOuwLAy',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:40:03.825Z',
                                updatedAt: '2020-07-29T11:40:03.825Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: "About Arby's",
                                nameInUrl: 'about',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '3KDdClIXLWKQnP6iJOggXS',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:40:14.916Z',
                                updatedAt: '2020-07-29T11:40:14.916Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Franchise',
                                nameInUrl: 'franchise',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '7A27GLB0HITI0zoW1osw8L',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:40:38.474Z',
                                updatedAt: '2020-07-29T11:40:38.474Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Foundation',
                                nameInUrl: 'foundation',
                            },
                        },
                        {
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'o19mhvm9a2cm',
                                    },
                                },
                                id: '3f5PSASnnX8nmNu86lPfHA',
                                type: 'Entry',
                                createdAt: '2020-07-29T11:40:49.416Z',
                                updatedAt: '2020-07-29T11:40:49.416Z',
                                environment: {
                                    sys: {
                                        id: 'feature_DBBP-811',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                contentType: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'ContentType',
                                        id: 'externalLink',
                                    },
                                },
                                locale: 'en-US',
                            },
                            fields: {
                                name: 'Careers',
                                nameInUrl: 'careers',
                            },
                        },
                    ],
                },
            },
        ],
        socialMediaLinksBlock: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '4g2URlYBpfHuUFYy0igeIC',
                type: 'Entry',
                createdAt: '2020-07-29T11:53:47.419Z',
                updatedAt: '2020-07-29T11:53:47.419Z',
                environment: {
                    sys: {
                        id: 'feature_DBBP-811',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'footerSocialMediaLinks',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                title: "Let's Connect",
                socialMediaLinks: [
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '5TjEu3TVoix3XazasoXHNW',
                            type: 'Entry',
                            createdAt: '2020-07-29T11:47:55.133Z',
                            updatedAt: '2020-07-30T10:56:10.775Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 2,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'socialMediaLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Facebook',
                            url: 'https://facebook.com',
                            icon: {
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'o19mhvm9a2cm',
                                        },
                                    },
                                    id: '55wllAUpTQWsy8SdeR5gxD',
                                    type: 'Asset',
                                    createdAt: '2020-07-29T11:47:50.448Z',
                                    updatedAt: '2020-07-29T11:47:50.448Z',
                                    environment: {
                                        sys: {
                                            id: 'feature_DBBP-811',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 1,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'Facebook Icon',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/79XbXs7E5XxPLcsdqxOUOJ/98267caf8cc8449d31a8fbf7eb60725e/Facebook.svg',
                                        details: {
                                            size: 371,
                                            image: {
                                                width: 15,
                                                height: 25,
                                            },
                                        },
                                        fileName: 'facebook.svg',
                                        contentType: 'image/svg+xml',
                                    },
                                },
                            },
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '3jFUwBYecel0bZnmUQisRq',
                            type: 'Entry',
                            createdAt: '2020-07-29T11:51:15.198Z',
                            updatedAt: '2020-07-29T11:51:15.198Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'socialMediaLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Twitter',
                            url: 'twitter.com',
                            icon: {
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'o19mhvm9a2cm',
                                        },
                                    },
                                    id: '4RAS85HHzq49owpVBMjmQG',
                                    type: 'Asset',
                                    createdAt: '2020-07-29T11:49:53.726Z',
                                    updatedAt: '2020-07-29T11:52:24.071Z',
                                    environment: {
                                        sys: {
                                            id: 'feature_DBBP-811',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 2,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'Twitter Icon',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/4xvpUeeNz7uI0kgWvOuwxI/cf787ddd90889f700e98a75958d77650/Twitter.svg',
                                        details: {
                                            size: 1115,
                                            image: {
                                                width: 30,
                                                height: 23,
                                            },
                                        },
                                        fileName: 'twitter.svg',
                                        contentType: 'image/svg+xml',
                                    },
                                },
                            },
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '5wRGl6VmzEd1Bp86q6phPD',
                            type: 'Entry',
                            createdAt: '2020-07-29T11:53:35.960Z',
                            updatedAt: '2020-07-29T11:53:35.960Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'socialMediaLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Instagram',
                            url: 'instagram.com',
                            icon: {
                                sys: {
                                    space: {
                                        sys: {
                                            type: 'Link',
                                            linkType: 'Space',
                                            id: 'o19mhvm9a2cm',
                                        },
                                    },
                                    id: '2LwDuZzMdQ34XcZvUqyOp2',
                                    type: 'Asset',
                                    createdAt: '2020-07-29T11:53:25.714Z',
                                    updatedAt: '2020-07-29T11:53:25.714Z',
                                    environment: {
                                        sys: {
                                            id: 'feature_DBBP-811',
                                            type: 'Link',
                                            linkType: 'Environment',
                                        },
                                    },
                                    revision: 1,
                                    locale: 'en-US',
                                },
                                fields: {
                                    title: 'Instagram Icon',
                                    file: {
                                        url:
                                            '//images.ctfassets.net/o19mhvm9a2cm/6LFURS5lCCsbyoQz5VevFK/27ed60cc3a6caa783f89c4a8ea70d975/icon_insta.png',
                                        details: {
                                            size: 3089,
                                            image: {
                                                width: 27,
                                                height: 27,
                                            },
                                        },
                                        fileName: 'instagram (3).svg',
                                        contentType: 'image/svg+xml',
                                    },
                                },
                            },
                        },
                    },
                ],
            },
        },
        backgroundImage: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '6NmH9oSPWRWrol4hcqepUz',
                type: 'Asset',
                createdAt: '2020-07-29T11:54:27.489Z',
                updatedAt: '2020-07-29T11:54:27.489Z',
                environment: {
                    sys: {
                        id: 'feature_DBBP-811',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'Background Image',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/6NmH9oSPWRWrol4hcqepUz/33b5aae6dc781a6b855000f3d12afc31/Vector__1_.svg',
                    details: {
                        size: 6007,
                        image: {
                            width: 194,
                            height: 208,
                        },
                    },
                    fileName: 'Vector (1).svg',
                    contentType: 'image/svg+xml',
                },
            },
        },
        footerIcons: [
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: '4MUlnEI5LHfoNLAhuZnG1D',
                    type: 'Asset',
                    createdAt: '2020-07-29T11:55:06.006Z',
                    updatedAt: '2020-07-29T11:55:06.006Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-811',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'Chicken Icon',
                    file: {
                        url:
                            '//images.ctfassets.net/o19mhvm9a2cm/4MUlnEI5LHfoNLAhuZnG1D/2a84206501051c10ba78d68249c3b3d0/Chicken__1_.svg',
                        details: {
                            size: 5436,
                            image: {
                                width: 62,
                                height: 74,
                            },
                        },
                        fileName: 'Chicken (1).svg',
                        contentType: 'image/svg+xml',
                    },
                },
            },
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: 'qQpltataHFIOHspuP43zV',
                    type: 'Asset',
                    createdAt: '2020-07-29T11:55:39.709Z',
                    updatedAt: '2020-07-29T11:55:39.709Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-811',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'Cow Icon',
                    file: {
                        url:
                            '//images.ctfassets.net/o19mhvm9a2cm/qQpltataHFIOHspuP43zV/7268e2cc4e819ac6480e304c7efaf978/Vector__2_.svg',
                        details: {
                            size: 4049,
                            image: {
                                width: 63,
                                height: 51,
                            },
                        },
                        fileName: 'Vector (2).svg',
                        contentType: 'image/svg+xml',
                    },
                },
            },
            {
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'o19mhvm9a2cm',
                        },
                    },
                    id: '6vF4ZcGfNyIJvSTNpSS06d',
                    type: 'Asset',
                    createdAt: '2020-07-29T11:56:07.034Z',
                    updatedAt: '2020-07-29T11:56:07.034Z',
                    environment: {
                        sys: {
                            id: 'feature_DBBP-811',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'Pig Icon',
                    file: {
                        url:
                            '//images.ctfassets.net/o19mhvm9a2cm/6vF4ZcGfNyIJvSTNpSS06d/9f3c03335d6a15424f4a8414a88df092/Pig__1_.svg',
                        details: {
                            size: 2682,
                            image: {
                                width: 48,
                                height: 24,
                            },
                        },
                        fileName: 'Pig (1).svg',
                        contentType: 'image/svg+xml',
                    },
                },
            },
        ],
        secondaryBlock: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '5nawMO5DsSoHLM35CV5QzS',
                type: 'Entry',
                createdAt: '2020-07-30T05:30:36.169Z',
                updatedAt: '2020-07-30T05:30:36.169Z',
                environment: {
                    sys: {
                        id: 'feature_DBBP-811',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'footerSecondaryBlock',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                label: "TM & © 2020 Arby's IP Holder, LLC. All Rights Reserved",
                links: [
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '5VTSzD4N24lcg52RcNBrFd',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:28:43.071Z',
                            updatedAt: '2020-07-30T05:28:43.071Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Privacy Policy',
                            nameInUrl: 'privacy',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '1SDrmd6lnODETGSrL8hEC6',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:28:57.017Z',
                            updatedAt: '2020-07-30T05:28:57.017Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Terms of Use',
                            nameInUrl: 'terms',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '2iC5FAK9yGe7bsOxsQ0Fra',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:29:15.089Z',
                            updatedAt: '2020-07-30T05:29:15.089Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Accessibility',
                            nameInUrl: 'accessibility',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: 'ORYqVuoz1M3rWAY9D91IU',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:29:29.382Z',
                            updatedAt: '2020-07-30T05:29:29.382Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'About Our Ads',
                            nameInUrl: 'about-ads',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '3YoqMSqtAqeWAes9cfYOVf',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:29:50.828Z',
                            updatedAt: '2020-07-30T05:29:50.828Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Do Not Sell My Info',
                            nameInUrl: 'about-ads',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '1VN0llDy73C96KVjiH8zQy',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:30:04.847Z',
                            updatedAt: '2020-07-30T05:30:04.847Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: 'Sitemap',
                            nameInUrl: 'sitemap',
                        },
                    },
                    {
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'o19mhvm9a2cm',
                                },
                            },
                            id: '60RO42jurkXAkrXNtVNcmg',
                            type: 'Entry',
                            createdAt: '2020-07-30T05:30:31.676Z',
                            updatedAt: '2020-07-30T05:30:31.676Z',
                            environment: {
                                sys: {
                                    id: 'feature_DBBP-811',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            contentType: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'ContentType',
                                    id: 'externalLink',
                                },
                            },
                            locale: 'en-US',
                        },
                        fields: {
                            name: "Arby's Canada",
                            nameInUrl: 'https://arbyscanada.com',
                        },
                    },
                ],
            },
        },
    },
} as unknown) as IFooter;
