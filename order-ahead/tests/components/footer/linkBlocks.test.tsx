import React from 'react';
import { shallow } from 'enzyme';

import FooterMockData from './index.mock';
import LinkBlocks from '../../../components/organisms/footer/linkBlocks';
import globalPropsMock from '../../mocks/globalProps.mock';

jest.mock('../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('footer component', () => {
    describe('linkBlocks component', () => {
        it('should match snapshot', () => {
            jest.spyOn(React, 'useContext').mockReturnValueOnce({});

            const wrapper = shallow(<LinkBlocks footer={FooterMockData as any} />);

            expect(wrapper).toMatchSnapshot();
        });
    });
});
