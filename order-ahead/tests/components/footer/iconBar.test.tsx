import React from 'react';
import { shallow } from 'enzyme';

import FooterMockData from './index.mock';
import IconBar from '../../../components/organisms/footer/iconBar';

describe('footer component', () => {
    describe('iconBar component', () => {
        it('should match snapshot', () => {
            const wrapper = shallow(<IconBar footer={FooterMockData as any} />);

            expect(wrapper).toMatchSnapshot();
        });
    });
});
