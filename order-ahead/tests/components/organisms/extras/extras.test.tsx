import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Extras from '../../../../components/organisms/extras/extras';
import extrasMock from './extras.mock';

jest.mock('../../../../redux/hooks/pdp', () => ({
    useExtraProducts: () => extrasMock,
    useChangedModifiersForChildItem: () => ({
        addedModifiers: [],
        removedDefaultModifiers: [],
    }),
    useBwwDisplayModifierGroupsForExtraItem: () => [],
}));

jest.mock('../../../../redux/hooks/usePdp', () =>
    jest.fn(() => ({
        useExtraChild: jest.fn().mockReturnValue({ setSelectedExtraChild: jest.fn(), unselectExtraChild: jest.fn() }),
    }))
);

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => ({
        modifierItemsById: { default: { fields: { image: 'mockImage' } } },
    }))
);

// useGlobalProps

describe('extras component', () => {
    it('should expand nutrition and render dropdowns for combo product', () => {
        render(<Extras />);

        screen.getByText(/SIDES/i);
        screen.getByText(/DRINKS/i);
        screen.getByText(/DESSERTS/i);
    });

    it('should render products after click on group', () => {
        render(<Extras />);

        expect(screen.queryByText(/Product Name 1/i)).not.toBeInTheDocument();

        userEvent.click(screen.queryByText(/SIDES/i));

        expect(screen.getByText(/Product Name 1/i)).toBeInTheDocument();
    });

    it('should render products after type enter on group', () => {
        render(<Extras />);

        expect(screen.queryByText(/Product Name 1/i)).not.toBeInTheDocument();

        userEvent.type(screen.queryByText(/SIDES/i), '{ enter }');

        expect(screen.getByText(/Product Name 1/i)).toBeInTheDocument();
    });

    it('should render all expanded groups after clicks', () => {
        render(<Extras />);

        expect(screen.queryByText(/Product Name 1/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Product Name 2/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Product Name 3/i)).not.toBeInTheDocument();

        userEvent.click(screen.queryByText(/SIDES/i));
        userEvent.click(screen.queryByText(/DRINKS/i));
        userEvent.click(screen.queryByText(/DESSERTS/i));

        expect(screen.getByText(/Product Name 1/i)).toBeInTheDocument();
        expect(screen.getByText(/Product Name 2/i)).toBeInTheDocument();
        expect(screen.getByText(/Product Name 3/i)).toBeInTheDocument();
    });
});
