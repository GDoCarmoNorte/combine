import { IExtraProductGroup } from '../../../../redux/types';

const extraProductGroups: IExtraProductGroup[] = [
    {
        displayName: 'SIDES',
        products: [
            {
                name: 'Product Name 1',
                id: '1',
                quantity: 0,
                resultCalories: 300,
                resultPrice: 100,
            },
        ],
        sequence: 1,
    },
    {
        displayName: 'DRINKS',
        products: [
            {
                name: 'Product Name 2',
                id: '2',
                quantity: 0,
                resultCalories: 300,
                resultPrice: 100,
            },
        ],
        sequence: 2,
    },
    {
        displayName: 'DESSERTS',
        products: [
            {
                name: 'Product Name 3',
                id: '3',
                quantity: 0,
                resultCalories: 300,
                resultPrice: 100,
            },
        ],
        sequence: 3,
    },
];

export default extraProductGroups;
