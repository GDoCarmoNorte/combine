import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import NestedModifiersModal from '../../../../components/organisms/nestedModifiersModal/nestedModifiersModal';
import { useGlobalProps, usePdp } from '../../../../redux/hooks';
import modifierWithNestedModifiers from './mocks/modifierWithNestedModifiers.mock';
import modifierGroupsWithNestedModifiers from './mocks/modifierGroupsWithNestedModifiers.mock';
import { useDomainProduct } from '../../../../redux/hooks/domainMenu';
import { NO_SAUCE_CATEGORY_ID } from '../../../../common/constants/product';
import { useBwwDisplayModifierGroup } from '../../../../redux/hooks/pdp';

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(),
    usePdp: jest.fn().mockReturnValue({
        pdpTallyItem: {},
        useIntensity: jest.fn(),
        actions: {
            onModifierChange: jest.fn(),
        },
    }),
}));

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/hooks/pdp', () => ({
    useProductTallyModifierGroup: jest.fn().mockReturnValue({ totalCount: 1 }),
    useBwwDisplayModifierGroup: jest.fn().mockReturnValue({}),
    useBwwDisplayModifierGroupsForModifier: jest.fn(),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({
        itemModifierGroups: [{}, {}],
    }),
    useDefaultModifiers: () => [],
    useSelectedModifiers: () => [],
    useNoSauceDomainProduct: () => jest.fn(),
}));

describe('NestedModifiersModal', () => {
    it('should render component', () => {
        const onModifierChangeMock = jest.fn();

        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });

        (useDomainProduct as jest.Mock).mockReturnValue({
            itemModifierGroups: [{}, {}],
            categoryIds: [NO_SAUCE_CATEGORY_ID],
        });

        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue({
            modifiers: modifierGroupsWithNestedModifiers[0].modifiers,
            displayName: 'displayName',
            maxQuantity: 2,
            minQuantity: 0,
        });

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: {},
            useIntensity: jest.fn(),
            actions: {
                onModifierChange: onModifierChangeMock,
            },
        });

        render(
            <NestedModifiersModal
                isOpen={true}
                onClose={jest.fn()}
                modifier={modifierWithNestedModifiers}
                modifierGroups={[
                    {
                        ...modifierGroupsWithNestedModifiers[0],
                        maxQuantity: 2,
                    },
                ]}
                modifierGroupId="IDPModifierGroup-16159"
            />
        );

        expect(screen.getAllByRole('checkbox')).toHaveLength(2);
        screen.getByText('Tomatoes');
        screen.getByText('Shaved Parmesan Cheese');
        userEvent.click(screen.getByText('Tomatoes'));
        expect(onModifierChangeMock).toHaveBeenCalled();
    });

    it('should not render component', () => {
        const onModifierChangeMock = jest.fn();

        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });

        (useDomainProduct as jest.Mock).mockReturnValue({
            itemModifierGroups: [{}, {}],
            categoryIds: [NO_SAUCE_CATEGORY_ID],
        });

        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue({
            modifiers: modifierGroupsWithNestedModifiers[0].modifiers,
            displayName: 'displayName',
            maxQuantity: 2,
            minQuantity: 0,
        });

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: {},
            useIntensity: jest.fn(),
            actions: {
                onModifierChange: onModifierChangeMock,
            },
        });

        render(
            <NestedModifiersModal
                onClose={jest.fn()}
                modifier={modifierWithNestedModifiers}
                modifierGroups={[
                    {
                        ...modifierGroupsWithNestedModifiers[0],
                        maxQuantity: 2,
                    },
                ]}
                modifierGroupId="IDPModifierGroup-16159"
            />
        );

        const child = screen.queryByText('Tomatoes');

        expect(child).not.toBeInTheDocument();
    });

    it('should render component with one group selection type=single', () => {
        const onModifierChangeMock = jest.fn();

        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: {},
            useIntensity: jest.fn(),
            actions: {
                onModifierChange: onModifierChangeMock,
            },
        });

        (useDomainProduct as jest.Mock).mockReturnValue({
            itemModifierGroups: [{}, {}],
        });

        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(undefined);

        const firstModifier = modifierGroupsWithNestedModifiers[0].modifiers[0];

        const singleSelectedModifierGroupsWithNestedModifiers = [
            {
                ...modifierGroupsWithNestedModifiers[0],
                maxQuantity: 1,
                modifiers: [
                    {
                        ...firstModifier,
                        displayProductDetails: {
                            ...firstModifier.displayProductDetails,
                            quantity: 0,
                            defaultQuantity: 0,
                        },
                    },
                    modifierGroupsWithNestedModifiers[0].modifiers[1],
                ],
            },
        ];

        render(
            <NestedModifiersModal
                isOpen={true}
                onClose={jest.fn()}
                modifier={modifierWithNestedModifiers}
                modifierGroups={singleSelectedModifierGroupsWithNestedModifiers}
                modifierGroupId="IDPModifierGroup-16159"
            />
        );
        screen.getByText('Tomatoes');
        screen.getByText('Shaved Parmesan Cheese');
        expect(screen.getByDisplayValue('IDPModifier-5763')).toHaveAttribute('type', 'radio');
        expect(screen.getAllByDisplayValue('1')).toHaveLength(1);
    });

    it('should fire increment/decrement events by click', () => {
        const onModifierChangeMock = jest.fn();

        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: {},
            useIntensity: jest.fn(),
            actions: {
                onModifierChange: onModifierChangeMock,
            },
        });

        (useDomainProduct as jest.Mock).mockReturnValue({
            itemModifierGroups: [{}, {}],
        });

        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(modifierGroupsWithNestedModifiers[0]);

        render(
            <NestedModifiersModal
                isOpen={true}
                onClose={jest.fn()}
                modifier={modifierWithNestedModifiers}
                modifierGroups={modifierGroupsWithNestedModifiers}
                modifierGroupId="IDPModifierGroup-16159"
            />
        );

        expect(screen.getAllByDisplayValue('1')).toHaveLength(1);

        userEvent.click(screen.getByLabelText('Remove one', { exact: false }));

        userEvent.click(screen.getByLabelText('Add one', { exact: false }));

        expect(onModifierChangeMock).toHaveBeenCalledTimes(2);
    });

    it('should fire increment/decrement events by keypress enter', () => {
        const onModifierChangeMock = jest.fn();

        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: {},
            useIntensity: jest.fn(),
            actions: {
                onModifierChange: onModifierChangeMock,
            },
        });

        (useDomainProduct as jest.Mock).mockReturnValue({
            itemModifierGroups: [{}, {}],
        });

        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(modifierGroupsWithNestedModifiers[0]);

        render(
            <NestedModifiersModal
                isOpen={true}
                onClose={jest.fn()}
                modifier={modifierWithNestedModifiers}
                modifierGroups={modifierGroupsWithNestedModifiers}
                modifierGroupId="IDPModifierGroup-16159"
            />
        );

        expect(screen.getAllByDisplayValue('1')).toHaveLength(1);

        userEvent.type(screen.getByLabelText('Add one', { exact: false }), '{enter}');

        userEvent.type(screen.getByLabelText('Remove one', { exact: false }), '{enter}');

        expect(onModifierChangeMock).toHaveBeenCalledTimes(4); // type Enter fires keydown on enter and click
    });

    it('should disable save&close button if there required modifiers not selected', () => {
        const onModifierChangeMock = jest.fn();

        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });

        (useDomainProduct as jest.Mock).mockReturnValue({
            itemModifierGroups: [{}, {}],
            categoryIds: [NO_SAUCE_CATEGORY_ID],
        });

        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue({
            modifiers: modifierGroupsWithNestedModifiers[0].modifiers,
            displayName: 'displayName',
            maxQuantity: 2,
            minQuantity: 0,
        });

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: {},
            useIntensity: jest.fn(),
            actions: {
                onModifierChange: onModifierChangeMock,
            },
        });

        render(
            <NestedModifiersModal
                isOpen={true}
                onClose={jest.fn()}
                modifier={modifierWithNestedModifiers}
                modifierGroups={[
                    {
                        ...modifierGroupsWithNestedModifiers[0],
                        maxQuantity: 2,
                    },
                ]}
                modifierGroupId="IDPModifierGroup-16159"
                requireSelectionModifiersGroups={['Dressing']}
            />
        );

        expect(screen.getAllByText(/Save & Close/i)[1]).toBeDisabled();
    });
});
