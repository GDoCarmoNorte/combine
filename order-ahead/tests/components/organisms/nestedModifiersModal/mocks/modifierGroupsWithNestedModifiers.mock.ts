const modifierGroupsWithNestedModifiers = [
    {
        displayName: 'Options',
        minQuantity: 0,
        maxQuantity: 7,
        modifierGroupId: 'IDPModifierGroup-9514',
        sequence: null,
        modifiers: [
            {
                displayName: 'Tomatoes',
                displayProductDetails: {
                    calories: null,
                    defaultQuantity: 1,
                    displayName: 'Tomatoes',
                    minQuantity: 0,
                    maxQuantity: 1,
                    price: 0,
                    productId: 'IDPModifier-5763',
                    quantity: 1,
                },
            },
            {
                displayName: 'Shaved Parmesan Cheese',
                displayProductDetails: {
                    calories: null,
                    defaultQuantity: 1,
                    displayName: 'Shaved Parmesan Cheese',
                    minQuantity: 0,
                    maxQuantity: 7,
                    price: 0,
                    productId: 'IDPModifier-17925',
                    quantity: 1,
                },
            },
        ],
    },
];

export default modifierGroupsWithNestedModifiers;
