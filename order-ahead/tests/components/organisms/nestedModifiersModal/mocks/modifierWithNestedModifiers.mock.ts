const modifierWithNestedModifiers = {
    displayName: 'Garden Side Salad',
    displayProductDetails: {
        calories: null,
        defaultQuantity: 0,
        displayName: 'Garden Side Salad',
        minQuantity: 0,
        maxQuantity: 1,
        price: 1,
        productId: 'IDPModifier-31001',
        quantity: 1,
    },
};

export default modifierWithNestedModifiers;
