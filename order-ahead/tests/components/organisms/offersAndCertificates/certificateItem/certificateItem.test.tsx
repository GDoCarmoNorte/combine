import React from 'react';
import { render, screen, within } from '../../../../utils';
import userEvent from '@testing-library/user-event';
import { offersAndCertificates } from '../offersAndCertificates.mock';
import CertificateItem from './../../../../../components/organisms/offersAndCertificates/certificateItem/certificateItem';
import { format } from '../../../../../common/helpers/dateTime';
import { useRewardsService } from '../../../../../common/hooks/useRewardsService';

jest.mock('../../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({
        id: 'ARBYS-WEBOA-640239548',
    }),
}));
jest.mock('../../../../../common/hooks/useRewardsService');

describe('certificate item component', () => {
    it('should have refund points and view details texts and expiration text with value for certificate', () => {
        const certificate = offersAndCertificates.certificates[0];
        render(<CertificateItem certificate={certificate as any} />);
        screen.getByText(/refund points/i);
        screen.getByText(/view details/i);
        const date = format(new Date(certificate.expirationDateTime), 'MM/dd/yyyy');
        const regExp = new RegExp(date, 'i');
        screen.getByText(regExp);
    });
    it('should have no refund points in case if status = Active and pointsBalance = 0', () => {
        const certificate = offersAndCertificates.certificates[1];
        render(<CertificateItem certificate={certificate as any} />);
        expect(screen.queryByText(/refund points/i)).not.toBeInTheDocument();
    });

    it('should display details modal when view details button is clicked', () => {
        const certificate = offersAndCertificates.certificates[1];
        render(<CertificateItem certificate={certificate as any} />);

        userEvent.click(screen.getByRole('button', { name: /view details/i }));

        const modal = screen.getByRole('presentation');

        expect(within(modal).getByRole(/heading/i, { name: /Test certificate/i })).toBeInTheDocument();
        expect(within(modal).getByText(/Expires 08\/18\/2021./i)).toBeInTheDocument();
        expect(within(modal).getByText(/ACTIVE REWARD/i)).toBeInTheDocument();

        expect(within(modal).getByRole('link', { name: /order online/i })).toBeInTheDocument();
        expect(within(modal).getByRole('link', { name: /order online/i })).toHaveAttribute('href', '/menu');
    });

    it('should display "refund pending" label for certificates with pending cancellation status', () => {
        const certificate = offersAndCertificates.certificates[2];
        render(<CertificateItem certificate={certificate as any} />);

        expect(screen.getByRole('button', { name: /refund pending/i })).toBeInTheDocument();
    });

    it('should display info label for certificates with pending cancellation status when clicking on cta', () => {
        const certificate = offersAndCertificates.certificates[2];
        render(<CertificateItem certificate={certificate as any} />);

        const cta = screen.getByRole('button', { name: /refund pending/i });

        userEvent.click(cta);

        expect(
            screen.getByText(/Your point total will be updated to reflect points refund within 24 hours/i)
        ).toBeInTheDocument();
        expect(screen.getByText(/Your Reward is currently PENDING/i)).toBeInTheDocument();
    });

    it('should display modal with cancellation pending label, back to rewards button for certificates with pending cancellation status when clicking view details', () => {
        const certificate = offersAndCertificates.certificates[2];
        render(<CertificateItem certificate={certificate as any} />);

        userEvent.click(screen.getByRole('button', { name: /view details/i }));

        const modal = screen.getByRole('presentation');

        expect(within(modal).getByRole(/heading/i, { name: /Test certificate/i })).toBeInTheDocument();
        expect(within(modal).getByText(/Expires 08\/18\/2021./i)).toBeInTheDocument();
        expect(within(modal).getByText(/cancellation pending/i)).toBeInTheDocument();

        expect(within(modal).getByRole('button', { name: /back to rewards/i })).toBeInTheDocument();
    });

    it('should display modal with refund points button for certificates with active status and points price > 0 when clicking view details', () => {
        const certificate = offersAndCertificates.certificates[0];
        render(<CertificateItem certificate={certificate as any} />);

        userEvent.click(screen.getByRole('button', { name: /view details/i }));

        const modal = screen.getByRole('presentation');

        expect(within(modal).getByText(/active/i)).toBeInTheDocument();

        expect(within(modal).getByRole('button', { name: /refund points/i })).toBeInTheDocument();
    });

    it('should call cancelCertificate service and display loader when user clicks refund points for active certificate from modal', () => {
        const cancelCertificateMock = jest.fn().mockResolvedValue(null);
        (useRewardsService as jest.Mock).mockReturnValue({
            cancelCertificate: cancelCertificateMock,
        });
        const certificate = offersAndCertificates.certificates[0];
        render(<CertificateItem certificate={certificate as any} />);

        userEvent.click(screen.getByRole('button', { name: /view details/i }));

        const modal = screen.getByRole('presentation');

        expect(within(modal).getByText(/active/i)).toBeInTheDocument();

        expect(within(modal).getByRole('button', { name: /refund points/i })).toBeInTheDocument();

        userEvent.click(within(modal).getByRole('button', { name: /refund points/i }));

        expect(cancelCertificateMock).toBeCalledTimes(1);

        expect(screen.getByRole('progressbar')).toBeInTheDocument();
    });

    it('should call cancelCertificate service when user clicks refund points for active certificate from list', () => {
        const cancelCertificateMock = jest.fn().mockResolvedValue(null);
        (useRewardsService as jest.Mock).mockReturnValue({
            cancelCertificate: cancelCertificateMock,
        });
        const certificate = offersAndCertificates.certificates[0];
        render(<CertificateItem certificate={certificate as any} />);

        expect(screen.getByRole('button', { name: /refund points/i })).toBeInTheDocument();

        userEvent.click(screen.getByRole('button', { name: /refund points/i }));

        expect(cancelCertificateMock).toBeCalledTimes(1);
    });
});
