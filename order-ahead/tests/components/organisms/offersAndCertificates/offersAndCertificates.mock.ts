import {
    ICertificateModel,
    IInactiveOfferModel,
    IOfferModel,
    TCertificateStatusModel,
    TOfferTypeModel,
    TRewardOfferStatusModel,
} from '../../../../@generated/webExpApi';

export const offersAndCertificates: {
    offers: IOfferModel[];
    certificates: ICertificateModel[];
} = {
    offers: [
        {
            id: '52907745-7672-470e-a803-a2f8feb52944',
            startDateTime: new Date('2019-08-23T05:00:00Z'),
            endDateTime: new Date('2099-12-31T11:59:59Z'),
            code: 'BNS558',
            name: '400 extra points',
            terms: `Get 400 points when you visit .  Offer Valid on eligible dine-in purchase of $10 or more (excluding purchases of gift cards, delivery orders, taxes, surcharges, rewards, gratuity, other promotions, and alcohol where prohibited by law) at participating locations. Offer may be redeemed 1 times during offer valid period. Guest must be a Blazin' Rewards member and activate the offer to be eligible to redeemBonus Points. To earn Bonus Points, provide the valid phone number associated with your enrollment to your server, bartender or cashier. Remember, points expire after 6 months of not earning points through an eligible purchase (see Terms and Conditions for details).`,
            description:
                'Offer valid 9/2/2019 - 12/31/2099. Loyalty Incentive Test - 100% discount (free) on one of these 4 sharable items ordered: Chips & Queso, Chips & Salsa, Mozzarella Sticks, or Fried Pickles, depending on which one is ordered. Two markets are participating in the market test – Charlotte & Portland.',
            imageUrl:
                '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BugLogo_Loyalty.jpg',
            userOfferId: '1bf1c890-f9b2-3619-e053',
            type: TOfferTypeModel.FixedAmount,
            isRedeemableInStoreOnly: false,
            isRedeemableOnlineOnly: true,
            termsLanguageCode: '',
            applicability: null,
        },
    ],
    certificates: [
        {
            number: 'uuid-35rt44',
            priceInPoints: 125,
            expirationDateTime: new Date('2021-08-18T15:49:09.794Z'),
            title: 'Test certificate',
            imageUrl:
                '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BugLogo_Loyalty.jpg',
            applicability: {
                isIncludesAll: true,
            },
            type: 'certificate',
            category: 'certificate',
            status: TCertificateStatusModel.Active,
            code: '123465',
        },
        {
            number: 'uuid-35rt48',
            priceInPoints: 0,
            expirationDateTime: new Date('2021-08-18T15:49:09.794Z'),
            title: 'Test certificate',
            imageUrl:
                '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BugLogo_Loyalty.jpg',
            applicability: {
                isIncludesAll: false,
                eligibleIds: [
                    {
                        menuId: 'IDPSalesItem-4858',
                        quantity: 1,
                    },
                ],
            },
            type: 'certificate',
            category: 'certificate',
            status: TCertificateStatusModel.Active,
            code: '123456',
        },
        {
            number: 'uuid-35rt49',
            priceInPoints: 350,
            expirationDateTime: new Date('2021-08-18T15:49:09.794Z'),
            title: 'Test certificate',
            imageUrl:
                '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BugLogo_Loyalty.jpg',
            applicability: {
                isIncludesAll: false,
                eligibleIds: [
                    {
                        menuId: 'IDPSalesItem-4858',
                        quantity: 1,
                    },
                ],
            },
            type: 'certificate',
            category: 'certificate',
            status: TCertificateStatusModel.PendingCancellation,
            code: '123456',
        },
    ],
};

export const inactiveOffers: IInactiveOfferModel[] = [
    {
        id: '52907745-7672-470e-a803-a2f8feb52944',
        startDateTime: '2019-08-23T05:00:00Z',
        endDateTime: '2099-12-31T11:59:59Z',
        code: 'BNS558',
        name: '400 extra points',
        terms: `Get 400 points when you visit .  Offer Valid on eligible dine-in purchase of $10 or more (excluding purchases of gift cards, delivery orders, taxes, surcharges, rewards, gratuity, other promotions, and alcohol where prohibited by law) at participating locations. Offer may be redeemed 1 times during offer valid period. Guest must be a Blazin' Rewards member and activate the offer to be eligible to redeemBonus Points. To earn Bonus Points, provide the valid phone number associated with your enrollment to your server, bartender or cashier. Remember, points expire after 6 months of not earning points through an eligible purchase (see Terms and Conditions for details).`,
        description:
            'Offer valid 9/2/2019 - 12/31/2099. Loyalty Incentive Test - 100% discount (free) on one of these 4 sharable items ordered: Chips & Queso, Chips & Salsa, Mozzarella Sticks, or Fried Pickles, depending on which one is ordered. Two markets are participating in the market test – Charlotte & Portland.',
        imageUrl:
            '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BugLogo_Loyalty.jpg',
        status: TRewardOfferStatusModel.Inactive,
        termsLanguageCode: 'en',
    },
];
