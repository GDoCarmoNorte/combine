import React from 'react';
import OffersAndCertificates from '../../../../components/organisms/offersAndCertificates';
import { render, screen } from '@testing-library/react';
import { useRewardsService } from '../../../../common/hooks/useRewardsService';
import { useGtmErrorEvent } from '../../../../common/hooks/useGtmErrorEvent';
import { useRewards } from '../../../../redux/hooks';
import { offersAndCertificates, inactiveOffers } from './offersAndCertificates.mock';

jest.mock('../../../../redux/hooks/useNotifications.ts', () => {
    return {
        __esModule: true,
        default: jest.fn().mockImplementation(() => {
            return {
                actions: {
                    enqueueError: jest.fn(),
                },
            };
        }),
    };
});

jest.mock('../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../redux/hooks/useRewards');
jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({
        id: 'ARBYS-WEBOA-640239548',
    }),
}));

jest.mock('../../../../common/hooks/useRewardsService');
jest.mock('../../../../common/hooks/useGtmErrorEvent');

describe('offers and certificates component', () => {
    beforeAll(() => {
        (useRewardsService as jest.Mock).mockReturnValue({ activateOffer: jest.fn() });
        (useGtmErrorEvent as jest.Mock).mockReturnValue({ pushGtmErrorEvent: jest.fn() });
        (useRewards as jest.Mock).mockReturnValue({
            __esModule: true,
            offers: offersAndCertificates.offers,
            inactiveOffers: inactiveOffers,
            certificates: offersAndCertificates.certificates,
            certificatesSortedByStatus: offersAndCertificates.certificates,
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
            loading: false,
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        });
    });

    it('should render correctly', () => {
        const { container } = render(<OffersAndCertificates />);

        expect(container).toMatchSnapshot();
    });

    it('should return null', () => {
        (useRewards as jest.Mock).mockReturnValueOnce({
            offers: [],
            certificates: [],
            inactiveOffers: [],
            certificatesSortedByStatus: [],
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
            loading: false,
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        });
        const { container } = render(<OffersAndCertificates />);
        expect(container).toMatchSnapshot();
    });

    it('should render correctly with loader', () => {
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([true, jest.fn()]);
        render(<OffersAndCertificates />);
        screen.getByRole('progressbar');

        useStateMock.mockRestore();
    });
});
