import React from 'react';
import { render, screen, waitFor } from '../../../../utils';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';
import { inactiveOffers } from '../offersAndCertificates.mock';
import OfferItem from '../../../../../components/organisms/offersAndCertificates/offerItem/offerItem';
import { useRewardsService } from '../../../../../common/hooks/useRewardsService';

jest.mock('../../../../../redux/hooks/useGlobalProps', () => () => ({}));
jest.mock('../../../../../redux/hooks/useNotifications.ts', () => {
    return {
        __esModule: true,
        default: jest.fn().mockImplementation(() => {
            return {
                actions: {
                    enqueueError: jest.fn(),
                },
            };
        }),
    };
});

jest.mock('../../../../../common/hooks/useRewardsService');

const mockState = {
    rewards: {
        certificates: [],
        offers: [
            {
                id: 1,
            },
        ],
        rewardsCatalogLoading: false,
        loading: false,
    },
};

describe('offer item component', () => {
    it('should have activate offer and view details texts for offer', () => {
        (useRewardsService as jest.Mock).mockReturnValue({
            activateOffer: jest.fn().mockImplementation(() => Promise.resolve()),
        });
        const setIsLoading = jest.fn();
        const offer = inactiveOffers[0];
        render(<OfferItem offer={offer as any} onLoadingToggle={setIsLoading} />, {
            state: mockState as any,
            renderOptions: {},
        });
        screen.getByText(/Activate Offer/i);
        screen.getByText(/view details/i);
        screen.getByText(/Limited Time Offer/i);
        screen.getByText(/valid 8\/23-12\/31\/99/i);
    });

    it('should change active state, open notification and close this in 5 sec.', async () => {
        (useRewardsService as jest.Mock).mockReturnValue({
            activateOffer: jest.fn().mockImplementation(() => Promise.resolve()),
        });
        jest.useFakeTimers();
        const offer = inactiveOffers[0];
        render(<OfferItem offer={offer as any} onLoadingToggle={jest.fn()} />, {
            state: mockState as any,
            renderOptions: {},
        });
        const activateBtn = screen.getByText(/Activate Offer/i);
        userEvent.click(activateBtn);
        act(() => {
            jest.advanceTimersByTime(5000);
        });
        await waitFor(() => {
            expect(screen.queryByText(/Your Offer is currently ACTIVE/i)).not.toBeInTheDocument();
        });
    });

    it('should open modal with details activate offer and change states', async () => {
        (useRewardsService as jest.Mock).mockReturnValueOnce({
            activateOffer: jest.fn().mockImplementation(() => Promise.resolve()),
        });

        const offer = inactiveOffers[0];
        render(<OfferItem offer={offer as any} onLoadingToggle={jest.fn()} />, {
            state: mockState as any,
            renderOptions: {},
        });
        const detailsBtn = screen.getByText(/View Details/i);
        userEvent.click(detailsBtn);
        expect(screen.getByRole('presentation')).toBeInTheDocument();
        const activateBtn = screen.getByRole('button', { name: 'activate button' });
        userEvent.click(activateBtn);
        const closeIcon = screen.getByRole('button', { name: 'Close Icon' });
        userEvent.click(closeIcon);
        expect(screen.queryByRole('presentation')).not.toBeInTheDocument();
    });

    it('should not to be changed after unsuccessful request', async () => {
        (useRewardsService as jest.Mock).mockReturnValue({
            activateOffer: jest.fn().mockImplementationOnce(() => Promise.reject()),
        });
        const offer = inactiveOffers[0];
        render(<OfferItem offer={offer as any} onLoadingToggle={jest.fn()} />, {
            state: mockState as any,
            renderOptions: {},
        });
        const activateBtn = screen.getByText(/Activate Offer/i);
        userEvent.click(activateBtn);

        await waitFor(() => {
            expect(screen.getByText(/Activate Offer/i)).toBeInTheDocument();
        });
    });
});
