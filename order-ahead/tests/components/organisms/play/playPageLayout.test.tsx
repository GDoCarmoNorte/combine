import React from 'react';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import PlayPageLayout from '../../../../components/organisms/play/playPageLayout';
import { useAccount, useOrderHistory } from '../../../../redux/hooks';
import { isPlayExperienceEnabled } from '../../../../lib/getFeatureFlags';
import { useAuth0 } from '@auth0/auth0-react';
import { useCheckin } from '../../../../common/hooks/useCheckin';
import { getCoordinates } from '../../../../common/helpers/getCoordinates';
import { setCookieValue } from '../../../../common/helpers/cookieHelper';
import { getLocationById } from '../../../../common/services/locationService';

jest.mock('react-redux');
jest.mock('../../../../common/services/locationService');
jest.mock('../../../../lib/getFeatureFlags');
jest.mock('../../../../redux/hooks/useOrderHistory');
jest.mock('../../../../redux/hooks/useAccount');
jest.mock('../../../../common/hooks/useCheckin');
jest.mock('../../../../common/helpers/getCoordinates');
jest.mock('../../../../common/helpers/cookieHelper');
jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,
        // @ts-ignore
        ...jest.requireActual('@auth0/auth0-react'),
        useAuth0: jest.fn(),
        withAuthenticationRequired: jest.fn().mockImplementation((component) => component),
    };
});
jest.mock('../../../../common/hooks/useConfiguration', () => ({
    createConfiguration: jest.fn(),
}));

const userAccountMock = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'john.doe@gmail34.com',
    references: [
        {
            type: 'LMS',
            id: '100500',
        },
    ],
    phones: [{ number: '1231231234' }],
    preferences: {
        postalCode: '12345',
    },
};
const hashedCookieMock = 'test hashed string';
const latMock = 1;
const longMock = 1;
const enqueeErrorMock = jest.fn();
const loginWithRedirectMock = jest.fn().mockResolvedValue({});
const checkinMock = jest.fn().mockResolvedValue({});
const setCookieMock = jest.fn();
const sha256Mock = jest.fn().mockReturnValue(hashedCookieMock);
const playPageUrlMock = 'https://play.com';
const valueEnv = process.env.NEXT_PUBLIC_PLAY_PAGE_URL;
const integrationKey = process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY;
const locatioMock = {
    id: 1,
    contactDetails: {
        address: {
            stateProvinceCode: 'MN',
            postalCode: '12345',
        },
    },
};

jest.mock('../../../../redux/hooks/useNotifications', () =>
    jest.fn().mockReturnValue({
        actions: {
            enqueueError: () => enqueeErrorMock(),
        },
    })
);

jest.mock('js-sha256', () => {
    return { sha256: (value) => sha256Mock(value) };
});

describe('Play page layout', () => {
    beforeEach(() => {
        (getLocationById as jest.Mock).mockReturnValue(locatioMock);
        (getCoordinates as jest.Mock).mockResolvedValue({ coords: { latitude: latMock, longitude: longMock } });
        (setCookieValue as jest.Mock).mockImplementation(() => setCookieMock());
        (isPlayExperienceEnabled as jest.Mock).mockReturnValue(true);
        (useAuth0 as jest.Mock).mockReturnValue({ loginWithRedirect: loginWithRedirectMock, isAuthenticated: false });
        (useOrderHistory as jest.Mock).mockReturnValue({
            orderHistory: [
                {
                    fulfillment: {
                        location: {
                            id: '123',
                        },
                    },
                },
            ],
            isLoading: false,
        });
        (useAccount as jest.Mock).mockReturnValue({
            account: userAccountMock,
            accountInfo: {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@gmail34.com',
                references: [],
                phone: '1231231234',
                preferences: {},
            },
            actions: {
                setAccount: jest.fn(),
            },
        });
        (useCheckin as jest.Mock).mockReturnValue({ checkin: checkinMock, payload: { storeId: 7, message: '' } });
        process.env.NEXT_PUBLIC_PLAY_PAGE_URL = playPageUrlMock;
        process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY = '';
    });
    afterEach(() => {
        process.env.NEXT_PUBLIC_PLAY_PAGE_URL = valueEnv;
        process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY = integrationKey;
        jest.clearAllMocks();
    });

    it('should render iframe with guest data in cookie if isAuthenticated: false', async () => {
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(checkinMock).toHaveBeenCalledTimes(0);
        });
        expect(setCookieMock).toHaveBeenCalledTimes(2);
        expect(getCoordinates).toHaveBeenCalledTimes(1);
        expect(sha256Mock).toHaveBeenCalledTimes(1);

        const iframe = screen.getByLabelText(/Play/i);
        expect(iframe).toBeInTheDocument();
    });
    it('should attempt to checkin loggedIn user', async () => {
        (useAuth0 as jest.Mock).mockReturnValue({ loginWithRedirect: loginWithRedirectMock, isAuthenticated: true });
        render(<PlayPageLayout />);
        const expectedCookie = {
            profileId: userAccountMock.references[0].id,
            email: userAccountMock.email,
            firstName: userAccountMock.firstName,
            lastName: userAccountMock.lastName,
            userZipcode: userAccountMock.preferences.postalCode,
            phoneNumber: userAccountMock.phones[0].number,
            eCommerceDate: '',
            eCommerceStoreId: '123',
            latitude: latMock,
            longitude: longMock,
            appIdentifier: '',
            GTMContainerId: '',
            deviceType: 'web',
        };
        await waitFor(() => {
            expect(checkinMock).toHaveBeenCalledTimes(1);
        });
        expect(setCookieMock).toHaveBeenCalledTimes(2);
        expect(getCoordinates).toHaveBeenCalledTimes(1);
        expect(sha256Mock).toHaveBeenCalledTimes(1);
        expect(sha256Mock).toHaveBeenCalledWith(JSON.stringify(expectedCookie));

        const iframe = screen.getByLabelText(/Play/i);
        expect(iframe).toBeInTheDocument();
    });
    it('should handle correctly cases when some user data unavailable', async () => {
        (useOrderHistory as jest.Mock).mockReturnValue({ orderHistory: [], isLoading: false });
        (useAccount as jest.Mock).mockReturnValue({
            account: {},
            accountInfo: {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@gmail34.com',
                references: [],
                phone: '1231231234',
                preferences: {},
            },
            actions: {
                setAccount: jest.fn(),
            },
        });
        const expectedCookie = {
            profileId: '',
            email: '',
            firstName: '',
            lastName: '',
            userZipcode: '',
            phoneNumber: '',
            eCommerceDate: '',
            eCommerceStoreId: '',
            latitude: latMock,
            longitude: longMock,
            appIdentifier: '',
            GTMContainerId: '',
            deviceType: 'web',
        };
        (useAuth0 as jest.Mock).mockReturnValue({ loginWithRedirect: loginWithRedirectMock, isAuthenticated: true });
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(checkinMock).toHaveBeenCalledTimes(1);
        });
        expect(setCookieMock).toHaveBeenCalledTimes(2);
        expect(getCoordinates).toHaveBeenCalledTimes(1);
        expect(sha256Mock).toHaveBeenCalledTimes(1);
        expect(sha256Mock).toHaveBeenCalledWith(JSON.stringify(expectedCookie));

        const iframe = screen.getByLabelText(/Play/i);
        expect(iframe).toBeInTheDocument();
    });
    it('should enquee error if geolocation data is not rerieved', async () => {
        (getCoordinates as jest.Mock).mockRejectedValue('error occured');
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(enqueeErrorMock).toHaveBeenCalledTimes(1);
        });
        const iframe = screen.getByLabelText(/Play/i);
        expect(iframe).toBeInTheDocument();
    });
    it('should handle incoming userLogin message event', async () => {
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Play/i)).toBeInTheDocument();
        });

        const messageEvent = new MessageEvent('message', {
            data: { command: 'userLogin' },
            origin: playPageUrlMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(loginWithRedirectMock).toHaveBeenCalledTimes(1);
        });
    });
    it('should not try to login user if already authenticated', async () => {
        (useAuth0 as jest.Mock).mockReturnValue({ loginWithRedirect: loginWithRedirectMock, isAuthenticated: true });
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Play/i)).toBeInTheDocument();
        });

        const messageEvent = new MessageEvent('message', {
            data: { command: 'userLogin' },
            origin: playPageUrlMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(loginWithRedirectMock).toHaveBeenCalledTimes(0);
        });
    });
    it('should handle incoming userCheckin message event', async () => {
        (useAuth0 as jest.Mock).mockReturnValue({ loginWithRedirect: loginWithRedirectMock, isAuthenticated: true });
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Play/i)).toBeInTheDocument();
        });

        const messageEvent = new MessageEvent('message', {
            data: { command: 'userCheckin' },
            origin: playPageUrlMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(checkinMock).toHaveBeenCalledTimes(2);
        });
    });
    it('should not try to checkin unauthenticated user', async () => {
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Play/i)).toBeInTheDocument();
        });

        const messageEvent = new MessageEvent('message', {
            data: { command: 'userCheckin' },
            origin: playPageUrlMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(checkinMock).toHaveBeenCalledTimes(0);
        });
    });
    it('should not handle message from unexpected origin', async () => {
        render(<PlayPageLayout />);
        await waitFor(() => {
            expect(screen.getByLabelText(/Play/i)).toBeInTheDocument();
        });

        const messageEvent = new MessageEvent('message', {
            data: { command: 'userLogin' },
            origin: playPageUrlMock + 'test',
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(loginWithRedirectMock).toHaveBeenCalledTimes(0);
        });
    });
});
