import React from 'react';
import { render, waitFor, screen } from '../../../../utils';
import userEvent from '@testing-library/user-event';
import ClaimNumberInput from '../../../../../components/organisms/claimPoints/claimNumber/claimNumberInput';
import globalPropsMock from '../../../../mocks/globalProps.mock';

jest.mock('../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('claim Number', () => {
    const submitBtn = jest.fn();
    const resetAndValidateForm = expect.any(Function);

    it('should have  enter claim number title', () => {
        render(<ClaimNumberInput isLoading={false} onSubmit={submitBtn} />);
        screen.getByText(/enter claim number/i);
    });

    it('should have claim number input with label', () => {
        render(<ClaimNumberInput isLoading={false} onSubmit={submitBtn} />);
        screen.getByLabelText(/claim number/i);
    });

    it('should call form submit with specific payload', async () => {
        render(<ClaimNumberInput isLoading={false} onSubmit={submitBtn} />);
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        const submitButton = screen.getByRole('button', { name: /submit/i });
        userEvent.type(claimNumberInput, '1234-210528-11-12');

        await waitFor(() => {
            expect(claimNumberInput).toHaveValue('1234-210528-11-12');
        });

        expect(submitButton).not.toBeDisabled();

        userEvent.click(submitButton);

        await waitFor(() => {
            expect(submitBtn).toHaveBeenCalledWith(
                {
                    claimNumber: '1234-210528-11-12',
                },
                resetAndValidateForm
            );
        });
    });

    it('submit button should be disabled  in case if claim number has not correct format', async () => {
        render(<ClaimNumberInput isLoading={false} onSubmit={submitBtn} />);
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        const submitButton = screen.getByRole('button', { name: /submit/i });
        userEvent.type(claimNumberInput, '123-210528-11-12'); //locationId is not correct;

        await waitFor(() => {
            expect(submitButton).toBeDisabled();
        });
    });

    it('submit button should be disabled  if loading true', async () => {
        render(<ClaimNumberInput isLoading={true} onSubmit={submitBtn} />);
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        const submitButton = screen.getByRole('button', { name: /submit/i });
        userEvent.type(claimNumberInput, '9012-211019-10007-15852');

        await waitFor(() => {
            expect(submitButton).toBeDisabled();
        });
    });
});
