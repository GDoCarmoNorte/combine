import ClaimPoints from '../../../../components/organisms/claimPoints/claimPoints';
import { render, screen } from '../../../utils';
import React from 'react';
import globalPropsMock from '../../../mocks/globalProps.mock';
import userEvent from '@testing-library/user-event';

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);
jest.mock('../../../../common/hooks/useRewardsActivityHistory', () => ({
    useRewardsActivityHistory: jest.fn().mockReturnValue({
        getRewardsActivityHistory: jest.fn(),
    }),
}));

describe('Claim Points', () => {
    it('should render correctly', () => {
        const { container } = render(<ClaimPoints />);
        expect(container).toMatchSnapshot();
    });

    it('should have earn more points text', () => {
        render(<ClaimPoints />);
        const header = screen.getByText(/earn more points/i);
        expect(header).toBeInTheDocument();
    });

    it('should have enter receipt dropdown and on click open/close enter Receipt section', () => {
        render(<ClaimPoints />);
        const enterReceiptText = screen.getByText(/Enter Receipt/i);
        userEvent.click(enterReceiptText);
        expect(screen.getAllByText(/Enter Receipt/i).length).toEqual(2);
        userEvent.click(enterReceiptText);
        expect(screen.getAllByText(/Enter Receipt/i).length).toEqual(1);
    });
});
