import React from 'react';
import EnterReceipt from '../../../../../components/organisms/claimPoints/enterReceipt/enterReceipt';
import { render, screen, waitFor } from '../../../../utils';
import globalPropsMock from '../../../../mocks/globalProps.mock';

import userEvent from '@testing-library/user-event';

jest.mock('../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('Enter Receipt', () => {
    const submitBtn = jest.fn();

    it('should have enter receipt title', () => {
        render(<EnterReceipt isLoading={false} onSubmit={submitBtn} />);
        screen.getByText(/enter receipt/i);
    });

    it('should have form inputs with  specific labels', () => {
        render(<EnterReceipt isLoading={false} onSubmit={submitBtn} />);
        screen.getByLabelText(/store number/i);
        screen.getByLabelText(/date/i);
        screen.getByLabelText(/check number/i);
        screen.getByLabelText(/sub total/i);
    });

    it('should have submit button', () => {
        render(<EnterReceipt isLoading={false} onSubmit={submitBtn} />);
        screen.getByRole('button', { name: /submit/i });
    });

    it('submit button should be disabled  in case if one of the filed is not correct', async () => {
        render(<EnterReceipt isLoading={false} onSubmit={submitBtn} />);
        const storeNumberInput = screen.getByLabelText(/store number/i);
        const dateInput = screen.getByLabelText(/date/i);
        const checkNumberInput = screen.getByLabelText(/check number/i);
        const subTotalInput = screen.getByLabelText(/sub total/i);
        const submitButton = screen.getByRole('button', { name: /submit/i });
        userEvent.type(storeNumberInput, '134'); //storeNumber is not correct
        userEvent.type(dateInput, '08/22/2021');
        userEvent.type(checkNumberInput, '123');
        userEvent.type(subTotalInput, '10');
        await waitFor(() => {
            expect(submitButton).toBeDisabled();
        });
    });

    it('submit button should be disabled  if loading true', async () => {
        render(<EnterReceipt isLoading={false} onSubmit={submitBtn} />);
        const storeNumberInput = screen.getByLabelText(/store number/i);
        const dateInput = screen.getByLabelText(/date/i);
        const checkNumberInput = screen.getByLabelText(/check number/i);
        const subTotalInput = screen.getByLabelText(/sub total/i);
        const submitButton = screen.getByRole('button', { name: /submit/i });
        userEvent.type(storeNumberInput, '9012');
        userEvent.type(dateInput, '08/22/2021');
        userEvent.type(checkNumberInput, '123');
        userEvent.type(subTotalInput, '10');
        await waitFor(() => {
            expect(submitButton).toBeDisabled();
        });
    });
});
