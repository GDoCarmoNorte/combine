import { render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import { LocationHeader } from '../../../../components/organisms/locationHeader';
import { locationDataMock } from './locationHeader.mock';
import userEvent from '@testing-library/user-event';
import { useRouter } from 'next/router';
import cityLocationTileMockData from '../cityLocationTile/cityLocationTile.mock';
import { getLocationById } from '../../../../common/services/locationService';
import { useDomainMenu, useOrderLocation } from '../../../../redux/hooks';
import { useLocationUnavailableError } from '../../../../common/hooks/useLocationUnavailableError';

jest.mock('../../../../common/services/locationService');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../redux/hooks/useDomainMenu');

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('../../../../common/hooks/useLocationUnavailableError', () => ({
    useLocationUnavailableError: jest.fn().mockReturnValue({
        pushLocationUnavailableError: jest.fn(),
    }),
}));

jest.mock('../../../../common/hooks/useBreadcrumbs', () => ({
    useBreadcrumbs: jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../redux/hooks/useTallyOrder', () => ({
    __esModule: true,
    default: jest.fn().mockReturnValue({
        setUnavailableTallyItems: jest.fn(),
    }),
}));

jest.mock('../../../../redux/store', () => ({
    useAppDispatch: jest.fn(),
}));

describe('LocationHeader component', () => {
    const setLocationMock = jest.fn();
    const getMenuMock = jest.fn();
    const routerPushMock = jest.fn();

    beforeEach(() => {
        (getLocationById as jest.Mock).mockReturnValue({ id: 1, isDigitallyEnabled: true });
        (useOrderLocation as jest.Mock).mockReturnValue({ actions: { setPickupLocation: setLocationMock } });
        (useDomainMenu as jest.Mock).mockReturnValue({ actions: { getDomainMenu: getMenuMock } });
        (useRouter as jest.Mock).mockImplementation(() => ({
            push: routerPushMock,
        }));
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should render store name', () => {
        render(<LocationHeader locationDetails={locationDataMock} />);

        screen.getByText(locationDataMock.displayName);
    });

    it('should render store address', () => {
        const result = 'store address';

        jest.spyOn(
            require('../../../../lib/locations/getLocationAddressString'),
            'getLocationAddressDetailsString'
        ).mockReturnValue(result);

        render(<LocationHeader locationDetails={locationDataMock} />);

        screen.getByText(result);
    });

    it('should render store phone', () => {
        const result = '(345) 2342-34-355';

        jest.spyOn(require('../../../../lib/formatPhoneNumber'), 'formatPhoneNumber').mockReturnValue(result);

        render(<LocationHeader locationDetails={locationDataMock} />);

        screen.getByText(result);
    });

    it('should render store directions link', () => {
        const result = 'directions';

        render(<LocationHeader locationDetails={locationDataMock} />);

        screen.getByText(result);
    });

    it('should render store id', () => {
        const result = `Store ID: ${locationDataMock.id}`;

        render(<LocationHeader locationDetails={locationDataMock} />);

        screen.getByText(result);
    });

    it('should render "VIEW MENU" button if store is closed', () => {
        render(<LocationHeader locationDetails={{ ...cityLocationTileMockData, isClosed: true }} />);

        expect(screen.getByRole('button', { name: /View menu/i })).toBeInTheDocument();
        expect(screen.queryByRole('button', { name: /Order/i })).not.toBeInTheDocument();
    });

    it('should render "VIEW MENU" button if online ordering is not available', () => {
        render(<LocationHeader locationDetails={{ ...cityLocationTileMockData, isOnlineOrderAvailable: false }} />);

        expect(screen.getByRole('button', { name: /View menu/i })).toBeInTheDocument();
        expect(screen.queryByRole('button', { name: /Order/i })).not.toBeInTheDocument();
    });

    it('should render "ORDER" link if online ordering is available and store is not closed', () => {
        render(<LocationHeader locationDetails={cityLocationTileMockData} />);

        expect(screen.getByRole('link', { name: /Order/i })).toBeInTheDocument();
    });

    it('should redirect to "/menu" if "VIEW MENU" button is clicked', async () => {
        render(
            <LocationHeader locationDetails={{ ...locationDataMock, isClosed: true, isOnlineOrderAvailable: false }} />
        );

        const btn = screen.getByRole('button', { name: /View menu/i });
        userEvent.click(btn);

        await waitFor(() => {
            expect(getMenuMock).toHaveBeenCalled();
        });

        expect(setLocationMock).toHaveBeenCalled();
        expect(routerPushMock).toHaveBeenCalledWith('/menu');
    });

    it('should call pushLocationUnavailableError when isDigitlEnabled false', async () => {
        const pushLocationErrorMock = jest.fn();
        (useLocationUnavailableError as jest.Mock).mockReturnValueOnce({
            pushLocationUnavailableError: pushLocationErrorMock,
        });

        const locationMockData = {
            id: 1,
            storeId: 1,
            isDigitallyEnabled: false,
            isClosed: true,
            contactDetails: {
                phone: '777-77-7777',
            },
        };
        (getLocationById as jest.Mock).mockReturnValueOnce(locationMockData);

        render(
            <LocationHeader locationDetails={{ ...locationDataMock, isClosed: true, isOnlineOrderAvailable: false }} />
        );

        const btn = screen.getByRole('button', { name: /View menu/i });
        userEvent.click(btn);

        await waitFor(() => expect(pushLocationErrorMock).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(pushLocationErrorMock).toHaveBeenCalledWith(locationMockData));
    });
});
