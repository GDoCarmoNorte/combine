import React from 'react';
import { getTextWithoutHtmlTags, render, screen } from '../../../../utils';
import userEvent from '@testing-library/user-event';
import CatalogItem from './../../../../../components/organisms/rewardsCatalog/catalogItem/catalogItem';
import { usePurchaseOffer } from '../../../../../common/hooks/usePurchaseOffer';
import { useRewards } from '../../../../../redux/hooks';
import { useRouter } from 'next/router';
import { TErrorCodeModel } from '../../../../../@generated/webExpApi';
import { fireEvent } from '@testing-library/react';

jest.mock('../../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../../redux/hooks/useLoyalty', () =>
    jest.fn().mockReturnValue({
        actions: { setLoyaltyPoints: jest.fn() },
    })
);

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('../../../../../redux/hooks/useRewards', () =>
    jest.fn().mockReturnValue({
        getCertificateById: jest.fn(),
        actions: { setRewards: jest.fn() },
    })
);

jest.mock('../../../../../common/hooks/usePurchaseOffer');

const GtmId = 'CTA-RewardsDeals-Redeem_Certificate-Chips_and_Salsa';

describe('catalog item component', () => {
    beforeEach(() => {
        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn().mockImplementation(() => Promise.resolve()),
            lastPurchaseData: null,
            cleanLastPurchaseData: jest.fn(),
            error: null,
        });
    });
    const renderComponent = ({
        title = 'Chips and Salsa',
        imageUrl = '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BWW_ChipsSalsa_RT.jpg',
        label = `500 pts`,
        points = 500,
        code = '1',
        pointsBalance = 1000,
        hasEnoughPoints = true,
        activeCTAText = 'redeem',
        setLodaing = jest.fn(),
    }) => (
        <CatalogItem
            title={title}
            code={code}
            imageUrl={imageUrl}
            label={label}
            points={points}
            setLoading={setLodaing}
            pointsBalance={pointsBalance}
            hasEnoughPoints={hasEnoughPoints}
            activeCTAText={activeCTAText}
        ></CatalogItem>
    );

    it('should has catalog reward and view details texts for reward', () => {
        render(renderComponent({}));
        screen.getByText(/redeem/i);
        screen.getByText(/view details/i);
        expect(screen.getByRole('button', { name: /redeem/i })).toHaveAttribute('data-gtm-id', GtmId);
    });

    it('should open modal with details for available reward', () => {
        render(renderComponent({}));
        expect(screen.getByText(/500 pts/i)).toBeInTheDocument();
        const detailsBtn = screen.getByText(/View Details/i);
        userEvent.click(detailsBtn);
        expect(screen.getByRole(/presentation/i)).toBeInTheDocument();
        expect(screen.getByText(/You’re eligible to redeem this reward with 500 points/i)).toBeInTheDocument();
        expect(screen.getByRole(/button/i, { name: /redeem certificate/i })).toHaveAttribute('data-gtm-id', GtmId);
        const closeIcon = screen.getByRole(/button/i, { name: /Close Icon/i });
        userEvent.click(closeIcon);
        expect(screen.queryByRole(/presentation/i)).not.toBeInTheDocument();
    });

    it('should open modal with details for unavailable reward', () => {
        render(
            renderComponent({ label: 'Redeem with 500 pts', points: 1000, pointsBalance: 500, hasEnoughPoints: false })
        );

        expect(screen.getByText(/Redeem with 500 pts/i)).toBeInTheDocument();
        const detailsBtn = screen.getByText(/View Details/i);
        userEvent.click(detailsBtn);
        expect(screen.getByRole(/presentation/i)).toBeInTheDocument();
        expect(screen.getByRole(/button/i, { name: /back to rewards/i })).toBeInTheDocument();
        const closeIcon = screen.getByRole(/button/i, { name: /Close Icon/i });
        userEvent.click(closeIcon);
        expect(screen.queryByRole(/presentation/i)).not.toBeInTheDocument();
    });

    it('purchaseOffer should be called', () => {
        const purchaseOffer = jest.fn().mockImplementation(() => Promise.resolve());

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer,
            lastPurchaseData: {},
            cleanLastPurchaseData: jest.fn(),
            success: false,
            error: null,
        });

        render(
            renderComponent({ label: 'Redeem with 500 pts', points: 500, pointsBalance: 1000, hasEnoughPoints: true })
        );

        const redeemBtn = screen.getByRole(/button/i, { name: /redeem/i });
        userEvent.click(redeemBtn);
        expect(purchaseOffer).toHaveBeenCalled();
    });

    it('should render modal with redeemed reward and cleanLastPurchaseData should be called after click on order online button', () => {
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([true, jest.fn()]);
        const routerStub = {
            push: jest.fn(),
        };
        (useRouter as jest.Mock).mockReturnValue(routerStub);

        const cleanLastPurchaseData = jest.fn();
        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn(),
            lastPurchaseData: {},
            cleanLastPurchaseData,
        });

        (useRewards as jest.Mock).mockReturnValueOnce({
            getCertificateById: jest.fn().mockReturnValueOnce({
                expirationDateTime: '2015-06-01T00:00:00Z',
            }),
            actions: { setRewards: jest.fn() },
        });

        render(
            renderComponent({ label: 'Redeem with 500 pts', points: 500, pointsBalance: 1000, hasEnoughPoints: true })
        );

        expect(screen.getByRole(/button/i, { name: /order online/i })).toBeInTheDocument();
        expect(screen.getByText(/Expires by 06\/01\/2015/i)).toBeInTheDocument();
        expect(screen.getByText(/Reward has been redeem. Find it in your account/i)).toBeInTheDocument();

        const orderOnlineBtn = screen.getByRole(/button/i, { name: /order online/i });
        userEvent.click(orderOnlineBtn);

        expect(cleanLastPurchaseData).toHaveBeenCalled();
        expect(routerStub.push).toHaveBeenCalled();
        useStateMock.mockRestore();
    });

    it('should render error modal on redeeming certificate and email not verified', () => {
        const purchaseOffer = jest.fn().mockImplementation(() => Promise.resolve());
        const cleanErrorCode = jest.fn();

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer,
            lastPurchaseData: {},
            cleanLastPurchaseData: jest.fn(),
            success: false,
            errorCode: TErrorCodeModel.EmailNotVerified,
            cleanErrorCode,
            error: 'Email not verified',
        });

        render(
            renderComponent({ label: 'Redeem with 500 pts', points: 500, pointsBalance: 1000, hasEnoughPoints: true })
        );

        const errorTitle = screen.getByText(/EMAIL UNVERIFIED/i);
        expect(errorTitle).toBeInTheDocument();
        expect(
            screen.getByText(
                /We need to verify your email address before you can redeem a reward. Check your email now./i
            )
        ).toBeInTheDocument();
        const closeButton = screen.getByText(/GO BACK/i);
        expect(closeButton).toBeInTheDocument();
        fireEvent.click(closeButton);
        expect(errorTitle).not.toBeInTheDocument();
        expect(cleanErrorCode).toHaveBeenCalled();
    });

    it('should show generic error modal when unhandled error is encountered', () => {
        const purchaseOffer = jest.fn().mockImplementation(() => Promise.reject());

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer,
            success: false,
            errorCode: null,
            error: 'Error',
        });

        render(
            renderComponent({ label: 'Redeem with 500 pts', points: 500, pointsBalance: 1000, hasEnoughPoints: true })
        );

        expect(
            getTextWithoutHtmlTags(
                'Something went wrong. Please contact Customer Service support@inspirebrands.com or try again later.'
            )
        ).toBeInTheDocument();
    });
});
