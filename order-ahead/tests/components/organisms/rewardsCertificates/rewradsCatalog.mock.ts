export const rewardsByCatalog = {
    cODE1: {
        totalCount: 20,
        description: 'Shareables',
        certificates: [
            {
                id: 'e958588f-0aa2-4dfc-8882-bf696de3a2a0',
                type: 'ON_DEMAND',
                code: '39',
                status: 'ACTIVE',
                title: 'Chips and Salsa',
                points: 450,
                imageUrl:
                    '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BWW_ChipsSalsa_RT.jpg',
                startDateTime: '2015-06-01T00:00:00Z',
                endDateTime: '2051-01-01T05:59:59Z',
            },
        ],
    },
};
