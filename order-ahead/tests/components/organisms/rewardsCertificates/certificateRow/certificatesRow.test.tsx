import React from 'react';
import CertificatesRow from '../../../../../components/organisms/rewardsCatalog/certificatesRow/certificatesRow';
import { render, screen } from '../../../../utils';
import userEvent from '@testing-library/user-event';
import { rewardsByCatalog } from '../rewradsCatalog.mock';
import { useLoyalty } from '../../../../../redux/hooks';
import { usePurchaseOffer } from '../../../../../common/hooks/usePurchaseOffer';

const {
    cODE1: { totalCount, description, certificates },
} = rewardsByCatalog;

jest.mock('../../../../../redux/hooks/useLoyalty');
jest.mock('../../../../../common/hooks/usePurchaseOffer');

describe('Certificates Row', () => {
    beforeEach(() => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn().mockImplementation(() => Promise.resolve()),
            lastPurchaseData: null,
            cleanLastPurchaseData: jest.fn(),
            error: null,
        });
    });
    const subtitle = `${totalCount} certificates`;
    const descriptionRegexp = new RegExp(description, 'i');
    const countRegexp = new RegExp(subtitle, 'i');
    it('should render row with certificates count and section name', () => {
        render(<CertificatesRow title={description} items={certificates as any} subTitle={subtitle} />);
        screen.getByText(descriptionRegexp);
        screen.getByText(countRegexp);
    });

    it('should expand and show items in  expanded area', () => {
        render(<CertificatesRow title={description} items={certificates as any} subTitle={subtitle} />);
        const collapseRow = screen.getByLabelText(/Collapse Row/i);
        userEvent.click(collapseRow);
        const certificate = screen.getByText(/Chips and Salsa/i);
        expect(certificate).toBeInTheDocument();
    });

    it('certificate should have lock redeem with x amount of pts when hasEnoughPoints = false', () => {
        render(<CertificatesRow title={description} items={certificates as any} subTitle={subtitle} />);
        const collapseRow = screen.getByLabelText(/Collapse Row/i);
        userEvent.click(collapseRow);
        const text = `Redeem with ${certificates[0].points} pts`;
        const regexp = new RegExp(text, 'i');
        const noEnoughPointsText = screen.getByText(regexp);
        expect(noEnoughPointsText).toBeInTheDocument();
    });

    it('should show only needed points in case if hasEnoughPoints = true', () => {
        (useLoyalty as jest.Mock).mockReturnValueOnce({
            loyalty: {
                pointsBalance: 2000,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });

        render(<CertificatesRow title={description} items={certificates as any} subTitle={subtitle} />);
        const collapseRow = screen.getByLabelText(/Collapse Row/i);
        userEvent.click(collapseRow);
        const text = `${certificates[0].points} pts`;
        const regexp = new RegExp(text, 'i');
        const noEnoughPointsText = screen.getByText(regexp);
        expect(noEnoughPointsText).toBeInTheDocument();
    });

    it('should show loading', () => {
        const useStateMock = jest
            .spyOn(React, 'useState')
            .mockReturnValueOnce([true, jest.fn()])
            .mockReturnValueOnce([true, jest.fn()]);

        render(<CertificatesRow title={description} items={certificates as any} subTitle={subtitle} />);
        expect(screen.getByRole(/progressbar/i)).toBeInTheDocument();
        useStateMock.mockRestore();
    });
});
