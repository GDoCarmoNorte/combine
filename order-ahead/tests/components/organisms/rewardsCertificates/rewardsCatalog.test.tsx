import React from 'react';
import RewardsCatalog from '../../../../components/organisms/rewardsCatalog';
import { render, screen } from '../../../utils';
import { useLoyalty } from '../../../../redux/hooks';
import { usePurchaseOffer } from '../../../../common/hooks/usePurchaseOffer';

jest.mock('../../../../redux/hooks/useLoyalty');
jest.mock('../../../../common/hooks/usePurchaseOffer');

describe('Rewards Certificates', () => {
    beforeEach(() => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn().mockImplementation(() => Promise.resolve()),
            lastPurchaseData: null,
            cleanLastPurchaseData: jest.fn(),
            error: null,
        });
    });

    it('should render correctly', () => {
        const { container } = render(<RewardsCatalog />);
        expect(container).toMatchSnapshot();
    });

    it('should have loader', () => {
        render(<RewardsCatalog />, {
            state: {
                rewards: {
                    rewardsCatalogLoading: true,
                    rewardsCatalog: {},
                    totalCount: 0,
                    offers: [],
                    certificates: [],
                    lastPurchasedCertificate: null,
                    loading: false,
                    rewardsRecommendations: [],
                    rewardsActivityHistory: [],
                    rewardsActivityHistoryLoading: false,
                },
            },
            renderOptions: {},
        });
        expect(screen.getByRole('progressbar')).toBeInTheDocument();
    });
});
