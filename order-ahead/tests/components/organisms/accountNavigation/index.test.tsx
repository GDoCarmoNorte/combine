import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';

import AccountNavigation from '../../../../components/organisms/accountNavigation';
import MockData from '../../../__mocks__/contentful/userAccountMenu.mock.json';
import Counter from '../../../../components/atoms/counter';
import { getAccountNavLinks } from '../../../../common/helpers/accountHelper';
import { isAccountDealsPageOn } from '../../../../lib/getFeatureFlags';
import configurationMock from '../../../mocks/configuration.mock';

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../lib/getFeatureFlags');

describe('account navigation component', () => {
    (isAccountDealsPageOn as jest.Mock).mockReturnValueOnce(true);
    const accountNavLinks = getAccountNavLinks(MockData as any, 0, configurationMock);
    it('should match snapshot', () => {
        const wrapper = shallow(<AccountNavigation links={accountNavLinks as any} currentPath={'/account'} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('if there are active offers the counter should be shown', () => {
        const mockDealsLink = [{ name: 'Deals', link: '/account/deals', count: 1 }];
        const wrapper = shallow(<AccountNavigation links={mockDealsLink as any} currentPath={'/account'} />);
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find(Counter).prop('value')).toEqual(1);
    });

    it('should display active if user navigate to personal deal page', () => {
        render(<AccountNavigation links={accountNavLinks as any} currentPath={'/account/deals/deal'} />);
        expect(screen.getByText('DEALS').getAttribute('class')).toMatch(/linkTextSelected/gi);
    });

    it('should scroll nav element into view', () => {
        const containerRef = {
            getBoundingClientRect: () => ({
                width: 280,
            }),
            scrollLeft: 0,
        };

        const targetRef = {
            getBoundingClientRect: () => ({
                right: 500,
            }),
        };
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce([containerRef, jest.fn()])
            .mockReturnValueOnce([targetRef, jest.fn()]);

        render(<AccountNavigation links={accountNavLinks as any} currentPath="/account/deals" />);
        expect(containerRef.scrollLeft).toBe(240);
        jest.clearAllMocks();
    });
});
