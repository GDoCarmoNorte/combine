import React from 'react';
import { mount } from 'enzyme';
import { Form, Formik } from 'formik';
import { render, screen } from '@testing-library/react';

import { createConnector } from '../../../../components/clientOnly/formik/connector';
import { actImmediate } from '../../clientOnly/formik/formikTestUtils';
import FormikPasswordInput from '../../../../components/organisms/formikPasswordInput';

describe('FormikPasswordInput', () => {
    it('FormikPasswordInput validation success', async () => {
        const FormikConnector = createConnector<{ test: string }>();

        const noop = jest.fn();
        const changeHandler = jest.fn();
        const validate = jest.fn().mockReturnValueOnce(undefined);

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikPasswordInput name="test" label="test" type="text" validate={validate} />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        const input = wrapper.find('input').at(0);

        expect(input).toBeDefined();

        input.simulate('change', { target: { name: 'test', value: 'test' } });

        await actImmediate(wrapper);

        expect(validate).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenLastCalledWith({ test: 'test' }, true, {});

        wrapper.unmount();
    });

    it('FormikPasswordInput validation failure', async () => {
        const FormikConnector = createConnector<{ test: string }>();

        const noop = jest.fn();
        const changeHandler = jest.fn();
        const validate = jest.fn().mockReturnValue('test error');

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikPasswordInput name="test" label="test" type="text" validate={validate} />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        const input = wrapper.find('input').at(0);

        expect(input).toBeDefined();

        input.simulate('change', { target: { name: 'test', value: 'test' } });

        await actImmediate(wrapper);

        expect(validate).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenLastCalledWith({ test: 'test' }, false, { test: 'test error' });

        wrapper.unmount();
    });

    it('should display eyeball button by default', () => {
        const FormikConnector = createConnector<{ test: string }>();

        const noop = jest.fn();
        const changeHandler = jest.fn();
        const validate = jest.fn().mockReturnValueOnce(undefined);

        render(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikPasswordInput name="test" label="test" type="text" validate={validate} />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        screen.getByLabelText('Show Password');
    });

    it('should not display eyeball button when showHideButton prop is false', () => {
        const FormikConnector = createConnector<{ test: string }>();

        const noop = jest.fn();
        const changeHandler = jest.fn();
        const validate = jest.fn().mockReturnValueOnce(undefined);

        render(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikPasswordInput
                            name="test"
                            label="test"
                            type="text"
                            validate={validate}
                            showHideButton={false}
                        />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        expect(screen.queryByLabelText(/Show Password/i)).not.toBeInTheDocument();
    });
});
