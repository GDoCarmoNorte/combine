import React from 'react';
import { mount, shallow } from 'enzyme';
import { useRouter } from 'next/router';
import { ItemModel } from '../../../../@generated/webExpApi';
import productMock from '../../../mocks/contentfulProduct.mock';
import useTallyOrder from '../../../../redux/hooks/useTallyOrder';
import domainProductMock from '../../../mocks/domainProduct.mock';
import { useTallyItemIsPromoList } from '../../../../redux/hooks/useTally';
import { useDomainProducts, useTallyItemsWithPricesAndCalories } from '../../../../redux/hooks/domainMenu';

import {
    useBag,
    useDomainMenu,
    useGlobalProps,
    useOrderLocation,
    usePageLoader,
    useNotifications,
    useRewards,
    useSelectedSell,
    useConfiguration,
} from '../../../../redux/hooks';
import { getCheckoutEntries, hasCheckoutEntries, sortRenderItems } from '../../../../components/organisms/bag/bagModel';
import Bag from '../../../../components/organisms/bag';
import resolveOpeningHours from '../../../../lib/locations/resolveOpeningHours';
import MockEmptyShoppingBagData from '../../../__mocks__/contentful/emptyShoppingBag.mock.json';
import MockProductsByIdData from '../../../__mocks__/contentful/productsById.mock.json';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { isRewardsOn, isShoppingBagLocationSelectComponentEnabled } from '../../../../lib/getFeatureFlags';
import { ITEMS_NOT_AVAILABLE_FOR_LOCATION, ORDER_NOT_AVAILABLE_FOR_LOCATION } from '../../../../common/constants/bag';
import configFeatureFlagsMocks from '../../../mocks/configFeatureFlag.mock';
import useBagAvailableItemsCounter from '../../../../common/hooks/useBagAvailableItemsCounter';
import { useAppDispatch } from '../../../../redux/store';
import { GTM_ERROR_EVENT } from '../../../../common/services/gtmService/constants';
import { GtmErrorCategory } from '../../../../common/services/gtmService/types';
import { useUnavailableItems } from '../../../../common/hooks/useUnavailableItems';
import { useBagRenderItems } from '../../../../common/hooks/useBagRenderItems';
import { usePrevious } from '../../../../common/hooks/usePreviousState';

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('../../../../lib/getFeatureFlags');
jest.mock('../../../../common/hooks/useBagRenderItems');
jest.mock('../../../../common/hooks/usePreviousState');

jest.mock('../../../../redux/store', () => ({
    useAppSelector: () => ({ dealID: null, dealType: null } as any),
    useAppDispatch: () => jest.fn(),
}));

jest.mock('react-redux');
jest.mock('../../../../redux/store', () => ({
    __esModule: true,
    useAppDispatch: jest.fn(),
    useAppSelector: jest.fn(),
    dispatch: jest.fn(),
}));
jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/useBag');
jest.mock('../../../../redux/hooks/useTally');
jest.mock('../../../../redux/hooks/useAccount', () =>
    jest.fn().mockReturnValue({
        __esModule: true,
        account: { idpCustomerId: 'idpCustomerId' },
    })
);
jest.mock('../../../../redux/hooks/useConfiguration');
jest.mock('../../../../redux/hooks/domainMenu');
jest.mock('../../../../lib/locations/resolveOpeningHours');
jest.mock('../../../../redux/hooks/useTallyOrder');
jest.mock('../../../../components/organisms/bag/bagModel');
jest.mock('../../../../lib/locations');
jest.mock('../../../../redux/hooks/useRewards', () => jest.fn().mockImplementation(() => ({ offers: [] })));

jest.mock('../../../../redux/hooks/useSelectedSell');
jest.mock('../../../../common/hooks/useBagAvailableItemsCounter');
jest.mock('../../../../common/hooks/useUnavailableItems');

const dispatch = jest.fn();

describe('bag component testing', () => {
    const toggleIsOpen = jest.fn();
    const removeAllFromBag = jest.fn();
    const submitTallyOrder = jest.fn();
    const getDomainMenu = jest.fn();
    const updateBagItems = jest.fn();
    const initPickupTimeValues = jest.fn();
    const resetPickupTime = jest.fn();
    const resetDeliveryTime = jest.fn();
    const removeDealFromBag = jest.fn();
    const fetchRecommendedProducts = jest.fn();
    const enqueueErrorMock = jest.fn();
    const removeFromBag = jest.fn();
    const bagMock = {
        bagEntries: [
            {
                category: 'drinks',
                lineItemId: 2,
                name: 'Bottled Water',
                price: 1.79,
                productId: 'arb-itm-003-125',
                quantity: 1,
            },
        ],
        entriesMarkedAsRemoved: [],
        isOpen: true,
        deal: {
            dealID: null,
            dealType: null,
        },
        actions: {
            toggleIsOpen,
            removeAllFromBag,
            updateBagItems,
            initPickupTimeValues,
            resetPickupTime,
            resetDeliveryTime,
            removeDealFromBag,
            removeFromBag,
        },
        bagEntriesCount: 1,
    };

    beforeAll(async () => {
        (useAppDispatch as jest.Mock).mockReturnValue(dispatch);
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: configFeatureFlagsMocks,
        });
        (sortRenderItems as jest.Mock).mockReturnValue([
            {
                contentfulProduct: productMock,
                discountBanner: undefined,
                entry: {
                    category: 'top-picks',
                    childItems: undefined,
                    lineItemId: 1,
                    name: 'Beef n Cheddar',
                    price: 6.59,
                    productId: 'arb-itm-000-021',
                    quantity: 1,
                },
                entryPath: {
                    as: '/menu/top-picks/classic-beef-n-cheddar-meal',
                    href: '/menu/[menuCategoryUrl]/[productDetailsPageUrl]',
                },
                isAvailable: false,
                arkedAsRemoved: false,
            },
        ]);

        (useGlobalProps as jest.Mock).mockReturnValue({
            emptyShoppingBag: MockEmptyShoppingBagData,
        });

        (useTallyOrder as jest.Mock).mockReturnValue({
            isLoading: false,
            submitTallyOrder,
        });

        (useDomainProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-021': domainProductMock as ItemModel,
        });

        (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(1);

        (useTallyItemIsPromoList as jest.Mock).mockReturnValue({
            'arb-itm-000-250': true,
        });

        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                id: '99984',
                contactDetails: { phone: '12345' },
                additionalFeatures: {
                    maxOrderAmount: 10,
                },
            },
            isCurrentLocationOAAvailable: true,
        });

        (getCheckoutEntries as jest.Mock).mockReturnValue([
            {
                name: 'Beef n Cheddar',
                price: 6.59,
                productId: 'arb-itm-000-021',
                quantity: 1,
                lineItemId: 1,
                childeItems: [
                    {
                        price: 2.81,
                        quantity: 1,
                    },
                ],
            },
        ]);
        (useDomainMenu as jest.Mock).mockReturnValue({
            loading: false,
            actions: {
                getDomainMenu,
            },
        });

        (useTallyItemsWithPricesAndCalories as jest.Mock).mockReturnValue([
            {
                category: 'drinks',
                lineItemId: 2,
                name: 'Bottled Water',
                price: 1.79,
                productId: 'arb-itm-003-125',
                quantity: 1,
                productData: {
                    calories: 1070,
                    price: 7.59,
                },
            },
        ]);

        (usePageLoader as jest.Mock).mockReturnValue({
            show: false,
            actions: {
                showLoader: jest.fn(),
                hideLoader: jest.fn(),
            },
        });
        (useNotifications as jest.Mock).mockReturnValue({
            actions: { enqueueError: enqueueErrorMock },
        });
        (useBagRenderItems as jest.Mock).mockReturnValue([
            {
                contentfulProduct: productMock,
                discountBanner: undefined,
                entry: {
                    category: 'top-picks',
                    childItems: undefined,
                    lineItemId: 1,
                    name: 'Beef n Cheddar',
                    price: 6.59,
                    productId: 'arb-itm-000-021',
                    quantity: 1,
                },
                entryPath: {
                    as: '/menu/top-picks/classic-beef-n-cheddar-meal',
                    href: '/menu/[menuCategoryUrl]/[productDetailsPageUrl]',
                },
                isAvailable: false,
                arkedAsRemoved: false,
            },
        ]);

        (useSelectedSell as jest.Mock).mockReturnValue({
            actions: {
                fetchRecommendedProducts,
            },
            recommendedProductIds: [],
            correlationId: 'test',
        });

        (useRouter as jest.Mock).mockReturnValue({ asPath: '/', pathname: '' });

        (isShoppingBagLocationSelectComponentEnabled as jest.Mock).mockReturnValue(false);

        (useUnavailableItems as jest.Mock).mockReturnValue({ unavailableItems: [] });
    });
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should match snapshot when rendered with empty bag', () => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                ...configFeatureFlagsMocks,
                isOAEnabled: true,
            },
        });
        (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(0);
        (useBagRenderItems as jest.Mock).mockReturnValueOnce([]);
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [],
            entriesMarkedAsRemoved: [],
            isOpen: true,
            actions: {
                toggleIsOpen,
            },
            bagEntriesCount: 0,
        });
        const wrapper = shallow(<Bag />);

        expect(wrapper).toMatchSnapshot();

        wrapper.unmount();
    });

    it('should match snapshot when rendered with bag items', () => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                ...configFeatureFlagsMocks,
                isOAEnabled: true,
            },
        });
        (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(1);
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    category: 'drinks',
                    lineItemId: 2,
                    name: 'Bottled Water',
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                },
            ],
            entriesMarkedAsRemoved: [],
            isOpen: true,
            actions: {
                toggleIsOpen: jest.fn().mockReturnValue({
                    isOpen: false,
                }),
            },
            bagEntriesCount: 1,
        });

        const wrapper = shallow(<Bag />);

        expect(wrapper).toMatchSnapshot();

        wrapper.unmount();
    });
    it('should remove cached offer cart items', () => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                ...configFeatureFlagsMocks,
                isOAEnabled: true,
            },
        });
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    category: 'drinks',
                    lineItemId: 2,
                    name: 'Bottled Water',
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                },
            ],
            entriesMarkedAsRemoved: [],
            isOpen: true,
            actions: {
                toggleIsOpen: jest.fn().mockReturnValue({
                    isOpen: false,
                }),
            },
            bagEntriesCount: 1,
        });
        (useGlobalProps as jest.Mock).mockReturnValue({
            productsById: MockProductsByIdData,
            emptyShoppingBag: MockEmptyShoppingBagData,
        });
        const wrapper = shallow(<Bag />);
        expect(wrapper).toMatchSnapshot();
        wrapper.unmount();
    });

    it('should match snapshot when location select component is enabled', () => {
        (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(1);
        (isShoppingBagLocationSelectComponentEnabled as jest.Mock).mockReturnValueOnce(true);

        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    category: 'drinks',
                    lineItemId: 2,
                    name: 'Bottled Water',
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                },
            ],
            entriesMarkedAsRemoved: [],
            isOpen: true,
            actions: {
                toggleIsOpen: jest.fn().mockReturnValue({
                    isOpen: false,
                }),
            },
            bagEntriesCount: 1,
        });

        const wrapper = shallow(<Bag />);

        expect(wrapper).toMatchSnapshot();
        wrapper.unmount();
    });

    it('should close the bag', () => {
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [],
            entriesMarkedAsRemoved: [],
            isOpen: true,
            actions: {
                toggleIsOpen,
            },
            bagEntriesCount: 0,
        });

        const wrapper = shallow(<Bag />);
        const closeButton = wrapper.find('.closeButton');
        closeButton.simulate('click');
        expect(toggleIsOpen).toHaveBeenCalledTimes(1);
        wrapper.unmount();
    });

    it('should not toggleOpen on init', () => {
        (useBag as jest.Mock).mockReturnValue(bagMock);
        const wrapper = mount(<Bag />);
        expect(toggleIsOpen).toHaveBeenCalledTimes(0);
        wrapper.unmount();
    });

    it('should remove all unavailable items', async () => {
        (useBag as jest.Mock).mockReturnValue(bagMock);
        (resolveOpeningHours as jest.Mock).mockReturnValue({});

        render(<Bag />);
        const unavailableRemove = await screen.findByRole('button', { name: /remove all/i });
        userEvent.click(unavailableRemove);
        expect(updateBagItems).toHaveBeenCalledTimes(1);
    });

    it(`should render "${ORDER_NOT_AVAILABLE_FOR_LOCATION}" if order is not available for location`, async () => {
        (sortRenderItems as jest.Mock).mockReturnValue([]);

        (useBag as jest.Mock).mockReturnValue(bagMock);

        (useTallyOrder as jest.Mock).mockReturnValue({
            error: {},
        });

        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                id: '99984',
                contactDetails: { phone: '12345' },
                additionalFeatures: {
                    maxOrderAmount: 10,
                },
            },
            isCurrentLocationOAAvailable: false,
        });

        render(<Bag />);

        await waitFor(() => {
            expect(screen.getByText(ORDER_NOT_AVAILABLE_FOR_LOCATION)).toBeInTheDocument();
        });
    });

    it(`should render "${ITEMS_NOT_AVAILABLE_FOR_LOCATION}" if items is not available for location`, async () => {
        (sortRenderItems as jest.Mock).mockReturnValue([]);

        (useBag as jest.Mock).mockReturnValue(bagMock);

        (useTallyOrder as jest.Mock).mockReturnValue({
            error: {},
        });

        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                id: '99984',
                contactDetails: { phone: '12345' },
                additionalFeatures: {
                    maxOrderAmount: 10,
                },
            },
            isCurrentLocationOAAvailable: true,
        });
        (hasCheckoutEntries as jest.Mock).mockReturnValue(false);

        render(<Bag />);

        await waitFor(() => {
            expect(screen.getByText(ITEMS_NOT_AVAILABLE_FOR_LOCATION)).toBeInTheDocument();
        });
    });

    it('should show an unavailable notification if there are unavailable items', async () => {
        (useBag as jest.Mock).mockReturnValue(bagMock);
        (resolveOpeningHours as jest.Mock).mockReturnValue({});

        render(<Bag />);

        expect(screen.getByText(/You have 1 unavailable item in your bag./i)).toBeInTheDocument();
    });

    it('should not render loyalty hint if there are no active rewards for a customer and rewards feature is enabled', () => {
        render(<Bag />);

        expect(screen.queryByText(/Have reward certificates?/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Reward certificates applied at checkout/i)).not.toBeInTheDocument();
    });

    it('should display loyalty hints when there are active rewards for a customer and rewards feature is enabled', () => {
        (isRewardsOn as jest.Mock).mockReturnValue(true);
        (useRewards as jest.Mock).mockReturnValue({ totalCount: 5, offers: [], certificates: [] });
        render(<Bag />);

        expect(screen.getByText(/Have reward certificates?/i)).toBeInTheDocument();
        expect(screen.getByText(/Reward certificates applied at checkout/i)).toBeInTheDocument();
    });

    it('should not display loyalty hints when and rewards feature is disabled', () => {
        (isRewardsOn as jest.Mock).mockReturnValue(false);
        render(<Bag />);

        expect(screen.queryByText(/Have reward certificates?/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Reward certificates applied at checkout/i)).not.toBeInTheDocument();
    });

    it('should fire GA event when in bag subtotal over maxOrderAmount', async () => {
        (useTallyItemsWithPricesAndCalories as jest.Mock).mockReturnValue([
            {
                category: 'drinks',
                lineItemId: 2,
                name: 'Bottled Water',
                price: 1.79,
                productId: 'arb-itm-003-125',
                quantity: 1,
                productData: {
                    calories: 1070,
                    price: 137.59,
                    totalPrice: 137.59,
                },
            },
        ]);

        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                id: '99984',
                contactDetails: { phone: '12345' },
                additionalFeatures: {
                    maxOrderAmount: 25,
                },
            },
            isCurrentLocationOAAvailable: false,
        });
        (isRewardsOn as jest.Mock).mockReturnValue(false);
        render(<Bag />);

        expect(dispatch).toHaveBeenCalledWith({
            type: GTM_ERROR_EVENT,
            payload: {
                ErrorCategory: GtmErrorCategory.BAG_ERROR_MAX_TOTAL,
                ErrorDescription:
                    'Online orders over 25 cannot be placed. Please call the restaurant to help fulfill your order: 12345',
            },
        });
    });

    it('should be called resetPickupTime, resetDeliveryTime if pickup or delivery location was changed', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {
                id: '1',
                contactDetails: { phone: '12345' },
                additionalFeatures: {
                    maxOrderAmount: 10,
                },
            },
            isCurrentLocationOAAvailable: true,
            pickupAddress: {
                id: '1',
                contactDetails: { phone: '12345' },
                additionalFeatures: {
                    maxOrderAmount: 10,
                },
            },
            deliveryAddress: {
                locationDetails: {
                    id: '4',
                    contactDetails: { phone: '12345' },
                    additionalFeatures: {
                        maxOrderAmount: 10,
                    },
                },
            },
        });
        (usePrevious as jest.Mock).mockReturnValueOnce({ id: 2 }).mockReturnValueOnce({ id: 3 });
        render(<Bag />);
        expect(resetPickupTime).toBeCalled();
        expect(resetDeliveryTime).toBeCalled();
    });
});
