import { getCheckoutEntries, hasCheckoutEntries, sortRenderItems } from '../../../../components/organisms/bag/bagModel';
import domainProductMock from '../../../mocks/domainProduct.mock';
import renderItems from './mocks/renderItems.mock';

jest.mock('../../../../lib/domainProduct', () => ({
    __esModule: true,
    getProductPath: jest.fn().mockReturnValue(''),
    getRelatedProduct: jest.fn(),
}));
jest.mock('../../../../common/helpers/discountHelper');

describe('Bag render model', () => {
    it('getCheckoutEntries should return correct result', () => {
        let checkoutEntries = getCheckoutEntries(renderItems, []);

        expect(checkoutEntries).toBeDefined();
        expect(checkoutEntries.length).toEqual(1);
        expect(checkoutEntries[0].productId).toEqual(domainProductMock.id);

        checkoutEntries = getCheckoutEntries([renderItems[1]], []);

        expect(checkoutEntries).toBeDefined();
        expect(checkoutEntries.length).toBe(0);

        checkoutEntries = getCheckoutEntries([], []);

        expect(checkoutEntries).toBeDefined();
        expect(checkoutEntries.length).toBe(0);
    });

    it('hasCheckoutEntries should return correct result', () => {
        expect(hasCheckoutEntries(renderItems, [])).toBe(true);

        expect(hasCheckoutEntries([renderItems[1]], [])).toBe(false);
        expect(hasCheckoutEntries([], [])).toBe(false);
    });

    it('sortRenderItems should return correct result', () => {
        // eslint-disable-next-line testing-library/render-result-naming-convention
        let sortedRenderItems = sortRenderItems(renderItems, {
            [domainProductMock.id]: domainProductMock,
            'unavailable product': { name: 'x - last sorted display name' },
        });

        expect(sortedRenderItems).toBeDefined();
        expect(sortedRenderItems.length).toEqual(4);
        expect(sortedRenderItems[0].entry.productId).toEqual(domainProductMock.id);
        expect(sortedRenderItems[1].entry.productId).toEqual('unavailable product');

        sortedRenderItems = sortRenderItems(renderItems, {
            [domainProductMock.id]: domainProductMock,
            'unavailable product': { name: 'a - first sorted display name' },
        });

        expect(sortedRenderItems).toBeDefined();
        expect(sortedRenderItems.length).toEqual(4);
        expect(sortedRenderItems[0].entry.productId).toEqual('unavailable product');
        expect(sortedRenderItems[2].entry.productId).toEqual(domainProductMock.id);
    });
});
