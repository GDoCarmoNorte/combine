import React from 'react';
import { shallow } from 'enzyme';
import { Entry } from 'contentful';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import BagContent from '../../../../components/organisms/bag/bagContent';
import domainProductMock from '../../../mocks/domainProduct.mock';
import { IBagRenderItem } from '../../../../components/organisms/bag/bagModel';

import { IProductFields } from '../../../../@generated/@types/contentful';

import { useAuth0 } from '@auth0/auth0-react';
import { mocked } from 'ts-jest/utils';
import { useAccount } from '../../../../redux/hooks';
import { recommendedProductsFull } from '../../../mocks/recommendedProducts.mock';
import isLocationOrderAheadAvailable from '../../../../lib/locations/isLocationOrderAheadAvailable';
import { useAppDispatch } from '../../../../redux/store';
import { GTM_ERROR_EVENT } from '../../../../common/services/gtmService/constants';
import { GtmErrorCategory } from '../../../../common/services/gtmService/types';

jest.mock('@auth0/auth0-react');
const mockedUseAuth0 = mocked(useAuth0, true);
const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch,
    createSelectorHook: jest.fn(),
}));

jest.mock('../../../../redux/store', () => ({
    useAppDispatch: jest.fn(),
    useAppSelector: jest.fn().mockReturnValue({ dealID: 1 }),
}));

jest.mock('../../../../redux/hooks/useConfiguration');

jest.mock('../../../../redux/hooks/useRewards', () => {
    return {
        __esModule: true,
        default: jest.fn().mockReturnValue({
            offers: [
                {
                    id: 1,
                },
            ],
            loading: false,
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        }),
        isOfferModel: jest.fn().mockReturnValue(() => true),
    };
});

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn().mockReturnValue({
        dealItemsById: {
            default: {
                fields: {
                    image: 'image.png',
                },
            },
        },
    })
);

jest.mock('../../../../redux/hooks/domainMenu', () => {
    const domainProductMock = require('../../../mocks/domainProduct.mock');

    return {
        __esModule: true,
        useDomainProducts: jest.fn().mockReturnValue({
            [domainProductMock.id]: domainProductMock,
        }),
    };
});

jest.mock('../../../../components/organisms/bag/item', () => ({
    __esModule: true,
    // eslint-disable-next-line react/display-name
    default: ({ entry }) => <div>{entry.productId}</div>,
}));

jest.mock('../../../../redux/hooks/useAccount', () => {
    const accountMock = import('../../../mocks/account.mock');
    return { __esModule: true, default: jest.fn().mockReturnValue({ account: accountMock }) };
});

jest.mock('../../../../redux/hooks/useBag', () => ({
    __esModule: true,
    isItemWithUnavailableModifires: jest.requireActual('../../../../redux/hooks/useBag').isItemWithUnavailableModifires,
    default: jest.fn().mockReturnValue({
        actions: {
            toggleIsOpen: jest.fn(),
            removeDealFromBag: jest.fn(),
            unavailableModifierRenderItems: () => [],
        },
    }),
}));

jest.mock('../../../../redux/hooks/useSelectedSell', () =>
    jest
        .fn(() => undefined)
        .mockImplementation(() => {
            return {
                ...recommendedProductsFull,
                recommendedProductIds: [],
            };
        })
);

jest.mock('../../../../redux/hooks/useOrderLocation', () => {
    const orderLocationMock = import('../../../mocks/orderLocation.mock');
    return {
        __esModule: true,
        default: jest.fn().mockReturnValue({
            ...orderLocationMock,
            isCurrentLocationOAAvailable: true,
            actions: {
                setPickupLocation: jest.fn(),
                setDeliveryLocation: jest.fn(),
                flushSelectedLocation: jest.fn(),
                setAvailableTimeSlots: jest.fn(),
            },
        }),
    };
});

jest.mock('../../../../lib/getFeatureFlags', () => ({
    // @ts-ignore
    ...jest.requireActual('../../../../lib/getFeatureFlags'),
    isAccountDealsPageOn: () => true,
}));

jest.mock('../../../../lib/locations/isLocationOrderAheadAvailable');

describe('BagContent component', () => {
    (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
    it('should match snapshot with available bag item', () => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: true,
            isLoading: false,
        } as any);
        const renderItemMock: IBagRenderItem = {
            entry: {
                lineItemId: 0,
                price: domainProductMock.price.currentPrice,
                productId: domainProductMock.id,
                quantity: 1,
            },
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
            isAvailable: true,
            markedAsRemoved: false,
        };

        const wrapper = shallow(
            <BagContent
                isMenuLoading={false}
                renderItems={[renderItemMock]}
                onRemoveUnavailable={jest.fn()}
                totalDiscount={domainProductMock.price.currentPrice.toString()}
                totalPrice={domainProductMock.price.currentPrice.toString()}
                subTotal={domainProductMock.price.currentPrice.toString()}
                unavailableTallyItems={[]}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should trigger onRemoveUnavailable with unavailable bag item', () => {
        const dispatch = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatch);
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: true,
            isLoading: false,
        } as any);

        const removeUnavailableCb = jest.fn();

        const renderItemMock: IBagRenderItem = {
            entry: {
                lineItemId: 0,
                price: domainProductMock.price.currentPrice,
                productId: domainProductMock.id,
                quantity: 1,
            },
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
            isAvailable: false,
            markedAsRemoved: false,
        };

        render(
            <BagContent
                isMenuLoading={false}
                renderItems={[renderItemMock]}
                onRemoveUnavailable={removeUnavailableCb}
                totalDiscount={domainProductMock.price.currentPrice.toString()}
                totalPrice={domainProductMock.price.currentPrice.toString()}
                subTotal={domainProductMock.price.currentPrice.toString()}
                unavailableTallyItems={[]}
            />
        );

        const removeAll = screen.getByRole('button', { name: /remove all/i });

        userEvent.click(removeAll);

        expect(removeUnavailableCb).toHaveBeenCalled();
    });

    it('should contain redeem you deals block  in case if user is authenticated', () => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: true,
            isLoading: false,
        } as any);

        const renderItemMock: IBagRenderItem = {
            entry: {
                lineItemId: 0,
                price: domainProductMock.price.currentPrice,
                productId: domainProductMock.id,
                quantity: 1,
            },
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
            isAvailable: true,
            markedAsRemoved: false,
        };

        render(
            <BagContent
                isMenuLoading={false}
                renderItems={[renderItemMock]}
                onRemoveUnavailable={jest.fn()}
                totalDiscount={domainProductMock.price.currentPrice.toString()}
                totalPrice={domainProductMock.price.currentPrice.toString()}
                subTotal={domainProductMock.price.currentPrice.toString()}
                unavailableTallyItems={[]}
            />
        );
        screen.getByText(/redeem your deals/i);
    });

    it('should not display redeem your deals block if user is not authenticated', () => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: false,
            isLoading: false,
        } as any);
        (useAccount as jest.Mock).mockReturnValue({
            account: null,
        });

        const renderItemMock: IBagRenderItem = {
            entry: {
                lineItemId: 0,
                price: domainProductMock.price.currentPrice,
                productId: domainProductMock.id,
                quantity: 1,
            },
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
            isAvailable: true,
            markedAsRemoved: false,
        };

        render(
            <BagContent
                isMenuLoading={false}
                renderItems={[renderItemMock]}
                onRemoveUnavailable={jest.fn()}
                totalDiscount={domainProductMock.price.currentPrice.toString()}
                totalPrice={domainProductMock.price.currentPrice.toString()}
                subTotal={domainProductMock.price.currentPrice.toString()}
                unavailableTallyItems={[]}
            />
        );
        expect(screen.queryByText(/redeem your deals/i)).not.toBeInTheDocument();
    });

    it('should render items with unavailable modifiers.', () => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: false,
            isLoading: false,
        } as any);
        (useAccount as jest.Mock).mockReturnValue({
            account: null,
        });

        const renderItemMock: IBagRenderItem = {
            entry: {
                lineItemId: 1,
                price: domainProductMock.price.currentPrice,
                productId: 'item with unavailable modifiers',
                quantity: 1,
                modifierGroups: [
                    { productId: 'test-id', modifiers: [{ productId: 'unavailableId', price: 1, quantity: 1 }] },
                ],
            },
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
            isAvailable: true,
            markedAsRemoved: false,
        };

        render(
            <BagContent
                isMenuLoading={false}
                renderItems={[renderItemMock]}
                onRemoveUnavailable={jest.fn()}
                totalDiscount={domainProductMock.price.currentPrice.toString()}
                totalPrice={domainProductMock.price.currentPrice.toString()}
                subTotal={domainProductMock.price.currentPrice.toString()}
                unavailableTallyItems={[
                    {
                        lineItemId: 1,
                        productId: 'item with unavailable modifiers',
                        isDefaultModifiersUnavailable: false,
                        isWholeProductUnavailable: false,
                        modifierIds: ['unavailableId'],
                        subModifierIds: [],
                    },
                ]}
            />
        );

        expect(screen.getByRole('heading', { name: /Unavailable/ })).toBeInTheDocument();
    });

    it('should fire GA event when in bag if have unavailable Items', async () => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: false,
            isLoading: false,
        } as any);
        (useAccount as jest.Mock).mockReturnValue({
            account: null,
        });

        const dispatch = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatch);

        const renderItemMock: IBagRenderItem = {
            entry: {
                lineItemId: 1,
                price: domainProductMock.price.currentPrice,
                productId: 'item with unavailable modifiers',
                quantity: 1,
                modifierGroups: [],
            },
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
            isAvailable: false,
            markedAsRemoved: false,
        };

        render(
            <BagContent
                isMenuLoading={false}
                renderItems={[renderItemMock]}
                onRemoveUnavailable={jest.fn()}
                totalDiscount={domainProductMock.price.currentPrice.toString()}
                totalPrice={domainProductMock.price.currentPrice.toString()}
                subTotal={domainProductMock.price.currentPrice.toString()}
                unavailableTallyItems={[
                    {
                        lineItemId: 1,
                        productId: 'test-id',
                        modifierIds: [],
                        subModifierIds: [],
                        isWholeProductUnavailable: true,
                        isDefaultModifiersUnavailable: false,
                    },
                ]}
            />
        );
        expect(dispatch).toHaveBeenCalledWith({
            type: GTM_ERROR_EVENT,
            payload: {
                ErrorCategory: GtmErrorCategory.BAG_ERROR,
                ErrorDescription: 'These items are currently unavailable at this location',
            },
        });
        expect(dispatch).toHaveBeenCalledTimes(1);
    });
});
