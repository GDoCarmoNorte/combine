import React from 'react';
import { mount, shallow } from 'enzyme';
import { Entry } from 'contentful';
import { useDispatch, useSelector } from 'react-redux';
import { render, screen } from '@testing-library/react';
import * as nextRouter from 'next/router';
import { act } from 'react-dom/test-utils';

import { IProductFields } from '../../../../@generated/@types/contentful';

import { useAppDispatch } from '../../../../redux/store';
import useBag from '../../../../redux/hooks/useBag';
import { useGlobalProps, useConfiguration } from '../../../../redux/hooks';
import domainProductMock from '../../../mocks/domainProduct.mock';
import resolveOpeningHours from '../../../../lib/locations/resolveOpeningHours';
import isLocationOrderAheadAvailable from '../../../../lib/locations/isLocationOrderAheadAvailable';
import BagButtons from '../../../../components/organisms/bag/bagButtons';

import locationsMock from '../../../mocks/expLocations.mock';
import configFeatureFlagsMocks from '../../../mocks/configFeatureFlag.mock';

jest.mock('react-redux');
jest.mock('next/router');
jest.mock('../../../../redux/store');
jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/useBag');
jest.mock('../../../../lib/locations/resolveOpeningHours');
jest.mock('../../../../lib/locations/isLocationOrderAheadAvailable');
jest.mock('../../../../redux/hooks/useConfiguration');

jest.mock('../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
    useAppDispatch: jest.fn(),
}));

const locationMock = locationsMock.locations[0] as any;

describe('BagButtons:', () => {
    const toggleBag = jest.fn();
    const onCheckoutClick = jest.fn();
    const toggleIsOpen = jest.fn().mockReturnValue({
        isOpen: false,
    });

    const mockRouter = () => {
        jest.spyOn(nextRouter, 'useRouter').mockImplementation(
            () =>
                ({
                    push: jest.fn(() => true),
                } as any)
        );
    };

    const mockBag = () => {
        (useBag as jest.Mock).mockReturnValue({
            isOpen: true,
            actions: {
                toggleIsOpen,
            },
        });
    };

    const renderItem = [
        {
            entry: {
                childItems: undefined,
                lineItemId: 2,
                modifierGroups: undefined,
                price: 1.79,
                productId: 'arb-itm-003-125',
                quantity: 1,
            },
            isAvailable: true,
            markedAsRemoved: false,
            contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
        },
    ];
    const props = {
        toggleBag,
        onCheckoutClick,
        isLoading: false,
        location: {},
        renderItems: [...renderItem],
        noCheckoutEntries: false,
        isCheckoutDisabledByTally: false,
    };

    beforeAll(() => {
        (useGlobalProps as jest.Mock).mockReturnValue({});
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: true,
        });
        (useBag as jest.Mock).mockReturnValue({
            isOpen: true,
            actions: {
                toggleIsOpen,
            },
        });
        (useConfiguration as jest.Mock).mockReturnValue(configFeatureFlagsMocks);
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should match snapshot when the bag is empty', () => {
        const wrapper = shallow(<BagButtons {...props} location={locationMock} />);
        expect(wrapper).toMatchSnapshot();
    });

    it('should redirect to /menu when the button "START AN ORDER" is clicked and location is valid', () => {
        const pushMock = jest.fn(() => true);
        const spyRouter = jest.spyOn(nextRouter, 'useRouter').mockImplementation(
            () =>
                ({
                    push: pushMock,
                } as any)
        );

        const wrapper = mount(
            <BagButtons
                {...props}
                renderItems={[
                    {
                        entry: {
                            childItems: undefined,
                            lineItemId: 2,
                            modifierGroups: undefined,
                            price: 1.79,
                            productId: 'arb-itm-003-125',
                            quantity: 1,
                        },
                        isAvailable: false,
                        markedAsRemoved: true,
                        contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
                    },
                ]}
                noCheckoutEntries={true}
                location={{ ...locationMock, id: '1' } as any}
            />
        );
        const buttonStartOrder = wrapper.find('button.callToActionButton');
        buttonStartOrder.simulate('click');

        expect(pushMock).toHaveBeenCalledTimes(1);
        expect(pushMock).toHaveBeenCalledWith('/menu');

        spyRouter.mockRestore();
    });

    it('should redirect to /locations when the button "SELECT A LOCATION" is clicked and location is invalid', () => {
        const pushMock = jest.fn(() => true);
        const spyRouter = jest.spyOn(nextRouter, 'useRouter').mockImplementation(
            () =>
                ({
                    push: pushMock,
                } as any)
        );

        const wrapper = mount(
            <BagButtons
                {...props}
                renderItems={[
                    {
                        entry: {
                            childItems: undefined,
                            lineItemId: 2,
                            modifierGroups: undefined,
                            price: 1.79,
                            productId: 'arb-itm-003-125',
                            quantity: 1,
                        },
                        isAvailable: false,
                        markedAsRemoved: true,
                        contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
                    },
                ]}
                location={undefined}
            />
        );

        const buttonStartOrder = wrapper.find('button.callToActionButton');
        buttonStartOrder.simulate('click');

        expect(pushMock).toHaveBeenCalledTimes(1);
        expect(pushMock).toHaveBeenCalledWith('/locations');

        spyRouter.mockRestore();
    });

    it('should close bag when the button "ADD MORE ITEMS" is clicked', async () => {
        mockRouter();
        mockBag();
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
        (useSelector as jest.Mock).mockReturnValue([2, 3]);
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: true,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        const wrapper = mount(<BagButtons {...props} location={locationMock} />);

        wrapper.find('button.secondary').simulate('click');
        expect(await toggleBag).toHaveBeenCalledTimes(1);
    });

    it('should NOT close bag when the button "ADD MORE ITEMS" is clicked', async () => {
        mockRouter();
        mockBag();
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        const wrapper = mount(<BagButtons {...props} location={locationMock} />);
        wrapper.find('button.secondary').simulate('click');
        expect(await toggleBag).toHaveBeenCalledTimes(0);
    });

    it('should show a "TooltipErrorMessage" when message is passed as prop', async () => {
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(false);
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: false,
        });
        const wrapper = mount(
            <BagButtons {...props} tooltipText="TooltipErrorMessage" location={{ ...locationMock, id: '1' } as any} />
        );

        const BagToolTipText = wrapper.find('InspireTooltip Portal');
        expect(BagToolTipText.text()).toEqual('TooltipErrorMessage');
    });

    it('should call onTooltipDismiss when handler is passed as prop', async () => {
        jest.useFakeTimers();
        const onTooltipDismissMock = jest.fn();
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(false);
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: false,
        });

        mount(
            <BagButtons
                {...props}
                tooltipText="TooltipErrorMessage"
                onTooltipDismiss={onTooltipDismissMock}
                tooltipDismiss={true}
                location={{ ...locationMock, id: '1' } as any}
            />
        );

        act(() => {
            jest.runAllTimers();
        });

        expect(onTooltipDismissMock).toBeCalledTimes(1);

        jest.useRealTimers();
    });

    it('should disable buttons when isOverLimitForOrder is true', async () => {
        render(<BagButtons {...props} isOverLimitForOrder location={{ ...locationMock, id: '1' } as any} />);

        expect(screen.getByText(/add more items/i)).toBeDisabled();
        expect(screen.getByText(/checkout now/i)).toBeDisabled();
    });

    it('should render "CHANGE LOCATION" button if all items is unavailable', async () => {
        render(
            <BagButtons
                {...props}
                renderItems={[
                    {
                        entry: {
                            childItems: undefined,
                            lineItemId: 2,
                            modifierGroups: undefined,
                            price: 1.79,
                            productId: 'arb-itm-003-125',
                            quantity: 1,
                        },
                        isAvailable: false,
                        markedAsRemoved: false,
                        contentfulProduct: (domainProductMock as any) as Entry<IProductFields>,
                    },
                ]}
                noCheckoutEntries={true}
                location={{ ...locationMock, id: '1' } as any}
            />
        );

        expect(screen.getByText(/CHANGE LOCATION/i)).toBeInTheDocument();
    });

    it('should disable checkout now button when items is unavailable by tally', async () => {
        render(<BagButtons {...props} location={locationMock} isCheckoutDisabledByTally />);

        expect(screen.getByText(/add more items/i)).toBeEnabled();
        expect(screen.getByText(/checkout now/i)).toBeDisabled();
    });
});
