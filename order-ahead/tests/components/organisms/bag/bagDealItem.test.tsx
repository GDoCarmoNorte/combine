import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import BagDealItem from '../../../../components/organisms/bag/bagDealItem';
import { useRewards, useBag } from '../../../../redux/hooks';

const removeDealFromBag = jest.fn();

jest.mock('../../../../redux/hooks/useBag');
jest.mock('../../../../redux/hooks/useRewards');
jest.mock('../../../../common/hooks/useDeal', () =>
    jest.fn().mockReturnValue({
        getDealImage: jest.fn().mockReturnValue({}),
    })
);

describe('deal Item', () => {
    const dealId = 'a7492d9a-46ff-41da-b909-eec691356d5f';
    beforeEach(() => {
        (useBag as jest.Mock).mockReturnValue({
            actions: {
                removeDealFromBag,
            },
        });
    });

    it('should match snapshot with available bag deal item', () => {
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        });
        const { container } = render(<BagDealItem dealId={dealId} />);

        expect(container).toMatchSnapshot();
    });

    it('should match snapshot without offer', () => {
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(null),
        });
        const { container } = render(<BagDealItem dealId={dealId} />);

        expect(container).toMatchSnapshot();
    });

    it('should call removeDealFromBag', () => {
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        });
        render(<BagDealItem dealId={dealId} />);
        const getButtonRemove = () => screen.getByText(/remove/i);

        expect(getButtonRemove()).toBeInTheDocument();

        userEvent.click(getButtonRemove());

        expect(removeDealFromBag).toBeCalled();
    });
});
