import React from 'react';
import { mount, shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';
import { fireEvent, within } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { useRouter } from 'next/router';
import { useBag, usePdp } from '../../../../redux/hooks';
import { useAppDispatch } from '../../../../redux/store';
import domainProductMock from '../../../mocks/domainProduct.mock';
import { useTallyItemIsPromo } from '../../../../redux/hooks/useTally';
import { getBagItemSizeLabel, removeUnavailableModifiers } from '../../../../common/helpers/bagHelper';
import { useTallyModifiers } from '../../../../common/hooks/useTallyModifiers';

import tallyItemMock from '../../../mocks/tallyItem.mock';
import productMock from '../../../mocks/contentfulProduct.mock';
import BagItem from '../../../../components/organisms/bag/item';
import { useDomainMenuSelectors, useDomainProduct, useProductItemGroup } from '../../../../redux/hooks/domainMenu';
import { ItemGroupEnum } from '../../../../redux/types';
import ListModifiers from '../../../../components/atoms/ListModifiers';
import { useUnavailableItems } from '../../../../common/hooks/useUnavailableItems';

jest.mock('../../../../common/hooks/useProductHasOtherSizes', () => ({
    useProductHasOtherSizes: jest.fn().mockReturnValue(true),
}));

jest.mock('react-redux');
jest.mock('next/router');
jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/useTally');
jest.mock('../../../../common/helpers/bagHelper');
jest.mock('../../../../common/hooks/useTallyModifiers');
jest.mock('../../../../components/organisms/bag/bagModel');
jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useProductItemGroup: jest.fn().mockReturnValue({}),
    useDomainProduct: jest.fn(() => domainProductMock),
    useTallyPriceAndCalories: jest.fn().mockReturnValue({
        price: 8,
        colories: 1000,
    }),
    useIsProductEditable: jest.fn().mockReturnValue(true),
    useDomainMenuSelectors: jest.fn().mockReturnValue({
        selectProductSize: jest.fn().mockReturnValue('Small'),
        selectSelectedModifiers: jest.fn().mockReturnValue([]),
        selectDefaultModifiers: jest.fn().mockReturnValue([]),
    }),
    useDomainProducts: jest.fn().mockReturnValue({
        'arb-itm-000-075': { id: 'arb-itm-000-075' },
    }),
    useComboMainDefaultProduct: jest.fn().mockReturnValue(() => {
        domainProductMock;
    }),
    useSelectedModifiers: jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../redux/store', () => ({
    useAppDispatch: jest.fn(),
    useAppSelector: () => ({ dealID: null, dealType: null } as any),
}));

jest.mock('../../../../redux/hooks/useRewards', () => jest.fn().mockImplementation(() => ({ offers: [] })));
jest.mock('../../../../common/hooks/useUnavailableItems');

describe('bag content snapshot testing', () => {
    const markAsRemoved = jest.fn();
    const editBagLineItem = jest.fn();
    const toggleIsOpen = jest.fn();
    const editTallyItemCount = jest.fn();
    const putTallyItem = jest.fn();
    const updateBagItemCount = jest.fn();
    const removeFromBag = jest.fn();
    const props = {
        contentfulProduct: productMock as any,
        discountBanner: undefined,
        entryPath: {
            as: '/menu/top-picks/crispy-chicken-bacon-swiss-sandwich-meal',
            href: '/menu/[menuCategoryUrl]/[productDetailsPageUrl]',
        },
    };
    const childItems = [
        {
            lineItemId: 1,
            productId: 'arb-itm-000-075',
            price: 4.81,
            quantity: 1,
        },
    ];

    beforeAll(() => {
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    productId: 'arb-itm-003-125',
                    lineItemId: 1,
                },
            ],
            entriesMarkedAsRemoved: [],
            isOpen: true,
            actions: {
                toggleIsOpen,
                markAsRemoved,
                editBagLineItem,
                updateBagItemCount,
                removeFromBag,
            },
        });

        (usePdp as jest.Mock).mockReturnValue({
            useHasUnsavedModifications: jest.fn().mockReturnValue(true),
            actions: {
                editTallyItemCount,
                putTallyItem,
            },
            entriesMarkedAsRemoved: [],
            isOpen: true,
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            addedModifiers: [],
            removedDefaultModifiers: [],
            modifiersIsChanged: false,
            defaultSubModifiersData: [],
        });
        (useTallyItemIsPromo as jest.Mock).mockReturnValue(false);
        (getBagItemSizeLabel as jest.Mock).mockReturnValue(false);
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: true,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        (useRouter as jest.Mock).mockReturnValue({
            asPath: '/menu/',
            route: '/menu',
            push: jest.fn(),
        });
        (useUnavailableItems as jest.Mock).mockReturnValue([]);
        (removeUnavailableModifiers as jest.Mock).mockReturnValue({});
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should match snapshot with available bag item', () => {
        const wrapper = shallow(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot when the bag is empty', () => {
        const wrapper = shallow(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={true}
            />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should add item back to bag', () => {
        const wrapper = mount(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={true}
            />
        );

        const removedItemAction = wrapper.find('div.removedItemAction');
        removedItemAction.simulate('click');

        expect(markAsRemoved).toHaveBeenCalledTimes(1);
    });

    it('should add item back to bag using keypress', () => {
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={true}
            />
        );

        const removedItemAction = screen.getByText('ADD THIS ITEM BACK TO YOUR BAG');
        fireEvent.keyDown(removedItemAction, { key: 'Enter', code: 13 });
        expect(markAsRemoved).toHaveBeenCalledTimes(1);
    });

    it('should add the total count for BOGO', () => {
        (useProductItemGroup as jest.Mock).mockReturnValueOnce({ name: ItemGroupEnum.BOGO });
        (useDomainMenuSelectors as jest.Mock).mockReturnValueOnce({
            selectProductSize: jest.fn().mockReturnValueOnce('6'),
        });

        (useDomainProduct as jest.Mock).mockReturnValueOnce({
            ...domainProductMock,
            name: 'BOGO Boneless Wings',
        });

        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-000-030',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        screen.getByText('BOGO Boneless Wings (12 Total)');
    });

    it('should display product name', () => {
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: childItems,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        expect(screen.getByText(domainProductMock.name)).toBeInTheDocument();
    });

    it('should remove the item if there are unsaved changes', async () => {
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('REMOVE');
        userEvent.click(link);
        expect(await markAsRemoved).toHaveBeenCalledTimes(1);
    });

    it('should remove the item if there are unsaved changes and close bag', async () => {
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('REMOVE');
        userEvent.click(link);
        expect(await toggleIsOpen).toHaveBeenCalledTimes(1);
    });

    it('should remove the item if there are no unsaved changes', () => {
        (usePdp as jest.Mock).mockReturnValue({
            useHasUnsavedModifications: jest.fn().mockReturnValue(false),
            actions: {
                editTallyItemCount,
                putTallyItem,
            },
            entriesMarkedAsRemoved: [],
            isOpen: true,
        });
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('REMOVE');
        userEvent.click(link);
        expect(markAsRemoved).toHaveBeenCalledTimes(1);
    });

    it('should remove item from the bag using keypress', async () => {
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('REMOVE');
        fireEvent.keyDown(link, { key: 'Enter', code: 13 });
        expect(await markAsRemoved).toHaveBeenCalledTimes(1);
    });

    it('should call putTallyItem and toggleIsOpen when click on modify with payload', async () => {
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: true,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('MODIFY');
        userEvent.click(link);
        expect(await toggleIsOpen).toHaveBeenCalledTimes(1);
        expect(await putTallyItem).toHaveBeenCalledTimes(1);
    });

    it('should call the increment and decrement functions', () => {
        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 2,
                }}
                markedAsRemoved={false}
            />
        );
        const reduceQuantityControl = screen.getByLabelText('reduce quantity');
        const increaseQuantityControl = screen.getByLabelText('increase quantity');
        const quantityElement = screen.getByLabelText('quantity');

        within(quantityElement).getByText('2');

        userEvent.click(increaseQuantityControl);
        expect(updateBagItemCount).toHaveBeenCalledTimes(1);
        expect(editTallyItemCount).toHaveBeenCalledTimes(1);

        userEvent.click(reduceQuantityControl);
        expect(updateBagItemCount).toHaveBeenCalledTimes(2);
        expect(editTallyItemCount).toHaveBeenCalledTimes(2);

        fireEvent.keyDown(increaseQuantityControl, { key: 'Enter', code: 13 });
        expect(updateBagItemCount).toHaveBeenCalledTimes(3);
        expect(editTallyItemCount).toHaveBeenCalledTimes(3);

        fireEvent.keyDown(reduceQuantityControl, { key: 'Enter', code: 13 });
        expect(updateBagItemCount).toHaveBeenCalledTimes(4);
        expect(editTallyItemCount).toHaveBeenCalledTimes(4);
    });

    it('should not call the increment functions', () => {
        const wrapper = mount(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 51,
                }}
                markedAsRemoved={false}
            />
        );
        const quantityControl = wrapper.find('.quantityControl');

        quantityControl.at(2).simulate('click');
        expect(updateBagItemCount).toHaveBeenCalledTimes(0);
    });

    it('should not call the decrement functions', () => {
        const wrapper = mount(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 1,
                }}
                markedAsRemoved={false}
            />
        );
        const quantityControl = wrapper.find('.quantityControl');
        quantityControl.at(0).simulate('click');
        expect(updateBagItemCount).toHaveBeenCalledTimes(0);
    });

    it('should close the bag when clicking on the modify', async () => {
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 2,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('MODIFY');
        userEvent.click(link);
        expect(await toggleIsOpen).toHaveBeenCalledTimes(1);
    });

    it('should close the bag when clicking on the modify using "Enter"', async () => {
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        render(
            <BagItem
                {...props}
                entry={{
                    lineItemId: 1,
                    childItems: undefined,
                    price: 1.79,
                    productId: 'arb-itm-003-125',
                    quantity: 2,
                }}
                markedAsRemoved={false}
            />
        );

        const link = screen.getByText('MODIFY');
        link.focus();
        fireEvent.keyDown(link, { key: 'Enter', code: 13 });
        expect(await toggleIsOpen).toHaveBeenCalledTimes(1);
    });

    it('should render unavailable modifiers list', () => {
        render(
            <ListModifiers
                addedModifiers={[{ name: 'unavailable modifier', quantity: 1, productId: 'unavailableId', price: 1 }]}
                removedModifiers={[]}
                defaultModifiers={[]}
                unavailableModifiers={['unavailableId']}
                unavailableSubModifiers={[]}
            />
        );
        const listItem = screen.getByRole('listitem');
        expect(within(listItem).getByText('unavailable modifier')).toBeInTheDocument();
        expect(within(listItem).getByText('Not Available')).toBeInTheDocument();
    });

    it('should remove unavailable modifiers from the Bag Item', () => {
        const currentTallyItem = {
            ...tallyItemMock,
            modifierGroups: tallyItemMock.modifierGroups.slice(-1),
        };
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        (removeUnavailableModifiers as jest.Mock).mockReturnValue({
            ...currentTallyItem,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [],
                },
            ],
        });

        render(
            <BagItem
                {...props}
                entry={currentTallyItem}
                markedAsRemoved={false}
                unavailableModifiers={['arb-itm-013-001']}
                unavailableSubModifiers={[]}
            />
        );

        const link = screen.getByText('REMOVE');
        userEvent.click(link);
        expect(editBagLineItem).toHaveBeenCalledTimes(1);
        expect(editBagLineItem).toHaveBeenCalledWith({
            bagEntry: {
                ...currentTallyItem,
                modifierGroups: [
                    {
                        productId: 'arb-prg-001-008',
                        modifiers: [],
                    },
                ],
            },
        });
    });

    it('should remove item from the Bag if default modifiers is unavailable', () => {
        const currentTallyItem = {
            ...tallyItemMock,
            modifierGroups: tallyItemMock.modifierGroups.slice(-1),
            lineItemId: 1,
        };
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        (removeUnavailableModifiers as jest.Mock).mockReturnValue({
            ...currentTallyItem,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [],
                },
            ],
        });

        render(
            <BagItem
                {...props}
                entry={currentTallyItem}
                markedAsRemoved={false}
                unavailableModifiers={['arb-itm-013-001']}
                unavailableSubModifiers={[]}
                isDefaultModifiersUnavailable={true}
            />
        );

        const link = screen.getByText('REMOVE');
        userEvent.click(link);
        expect(markAsRemoved).toHaveBeenCalledTimes(0);
        expect(editBagLineItem).toHaveBeenCalledTimes(0);
        expect(toggleIsOpen).toHaveBeenCalledTimes(0);
        expect(removeFromBag).toHaveBeenCalledTimes(1);
        expect(removeFromBag).toHaveBeenCalledWith({
            lineItemId: 1,
        });
    });

    it('should remove item from the Bag if bag item is unavailable', () => {
        const currentTallyItem = {
            ...tallyItemMock,
            modifierGroups: tallyItemMock.modifierGroups.slice(-1),
            lineItemId: 2,
        };
        const dispatchMock = jest.fn().mockReturnValue(
            Promise.resolve({
                payload: false,
            })
        );
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        (removeUnavailableModifiers as jest.Mock).mockReturnValue({
            ...currentTallyItem,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [],
                },
            ],
        });

        render(
            <BagItem
                {...props}
                entry={currentTallyItem}
                markedAsRemoved={false}
                unavailableModifiers={['arb-itm-013-001']}
                unavailableSubModifiers={[]}
                isUnavailableItem={true}
            />
        );

        const link = screen.getByText('REMOVE');
        userEvent.click(link);
        expect(markAsRemoved).toHaveBeenCalledTimes(0);
        expect(editBagLineItem).toHaveBeenCalledTimes(0);
        expect(toggleIsOpen).toHaveBeenCalledTimes(0);
        expect(removeFromBag).toHaveBeenCalledTimes(1);
        expect(removeFromBag).toHaveBeenCalledWith({
            lineItemId: 2,
        });
    });
});
