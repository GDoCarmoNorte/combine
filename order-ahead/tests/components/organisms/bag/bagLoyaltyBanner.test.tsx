import React from 'react';
import { render, screen } from '../../../utils';
import BagLoyaltyBanner from '../../../../components/organisms/bag/bagLoyaltyBanner';

describe('bagLoyaltyBanner', () => {
    it('should return null when there are no active rewards for a customer', () => {
        render(<BagLoyaltyBanner />, {
            state: {
                rewards: {
                    totalCount: 0,
                    offers: [],
                    certificates: [],
                    lastPurchasedCertificate: null,
                    loading: false,
                    rewardsCatalog: {},
                    rewardsCatalogLoading: false,
                    rewardsRecommendations: [],
                    rewardsActivityHistory: [],
                    rewardsActivityHistoryLoading: false,
                },
            },
        });

        expect(screen.queryByText(/Have reward certificates?/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Reward certificates applied at checkout/i)).not.toBeInTheDocument();
    });

    it('should display loyalty hints when there are active rewards for a customer', () => {
        render(<BagLoyaltyBanner />);

        expect(screen.getByText(/Have reward certificates?/i)).toBeInTheDocument();
        expect(screen.getByText(/Reward certificates applied at checkout/i)).toBeInTheDocument();
    });
});
