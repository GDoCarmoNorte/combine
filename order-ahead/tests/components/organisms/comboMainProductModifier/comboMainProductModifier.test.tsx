import React from 'react';
import { shallow } from 'enzyme';

import ComboMainProductModifier from '../../../../components/organisms/comboMainProductModifier';
import { ModifierCardContainer } from '../../../../components/organisms/modifierCardContainer/modifierCardContainer';
import displayProduct from '../../../mocks/displayProduct.mock';
import { useMainComboDisplayProductByProductId } from '../../../../redux/hooks/pdp';

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: () => ({ productsById: 'productsById' }),
}));

jest.mock('../../../../redux/hooks/pdp');

const onSizeSelectFnMock = jest.fn();
const section = {
    productSectionType: 'main',
    productSectionDisplayName: 'Sandwich',
    productGroupId: 'arb-prg-000-019',
};

describe('ComboMainProductModifier', () => {
    it('should render correctly', () => {
        (useMainComboDisplayProductByProductId as jest.Mock).mockReturnValue({
            displayProductDetails: { selections: [1, 2] },
        });

        const wrapper = shallow(
            <ComboMainProductModifier section={section as any} id="arb-itm-000-021" onSizeSelect={onSizeSelectFnMock} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should not render if sizes < 2', () => {
        (useMainComboDisplayProductByProductId as jest.Mock).mockReturnValue({
            displayProductDetails: { selections: [1] },
        });

        const wrapper = shallow(
            <ComboMainProductModifier section={section as any} id="arb-itm-000-021" onSizeSelect={onSizeSelectFnMock} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should call onSizeSelect with proper param', () => {
        (useMainComboDisplayProductByProductId as jest.Mock).mockReturnValue({
            displayProductDetails: { selections: [1, 2] },
        });

        const wrapper = shallow(
            <ComboMainProductModifier section={section as any} id="arb-itm-000-021" onSizeSelect={onSizeSelectFnMock} />
        );

        const params = {
            id: 'id',
            quantity: 1,
            displayProduct: displayProduct,
        };

        wrapper.find(ModifierCardContainer).props().onChange(params);
        expect(onSizeSelectFnMock).toHaveBeenCalledTimes(1);
        expect(onSizeSelectFnMock).toHaveBeenCalledWith('id');
    });
});
