import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import DeliveryLocationAutocompleteInput from '../../../../../components/organisms/locationsPageContent/deliveryLocationAutocomplete/delivetyLocationAutocompleteInput';
import userEvent from '@testing-library/user-event';

describe('DeliveryLocationAutocompleteInput component', () => {
    it('should render DeliveryLocationAutocompleteInput properly', async () => {
        const { container } = render(
            <DeliveryLocationAutocompleteInput
                params={{ InputProps: jest.fn } as any}
                disabled={false}
                onClick={jest.fn()}
                searchAddressLength={0}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render input and icon button properly', async () => {
        render(
            <DeliveryLocationAutocompleteInput
                params={{ InputProps: jest.fn } as any}
                disabled={false}
                onClick={jest.fn()}
                searchAddressLength={0}
            />
        );

        await waitFor(() => {
            expect(screen.getByLabelText(/Delivery Address Input/i)).toBeInTheDocument();
        });
        expect(screen.getByLabelText(/Enter delivery address/i)).toBeInTheDocument();
    });

    it('should disabled input if initializing is true', async () => {
        render(
            <DeliveryLocationAutocompleteInput
                params={{ InputProps: jest.fn } as any}
                disabled
                onClick={jest.fn()}
                searchAddressLength={0}
            />
        );

        await waitFor(() => {
            expect(screen.getByLabelText(/Delivery Address Input/i)).toBeDisabled();
        });
    });

    it('should render ClearIcon if searchAddressLength has length', async () => {
        render(
            <DeliveryLocationAutocompleteInput
                params={{ InputProps: jest.fn } as any}
                disabled={false}
                onClick={jest.fn()}
                searchAddressLength={5}
            />
        );

        await waitFor(() => {
            expect(screen.getByLabelText(/Clear delivery address/i)).toBeInTheDocument();
        });
    });

    it('should call onClick after clicking on button', async () => {
        const onClick = jest.fn();
        render(
            <DeliveryLocationAutocompleteInput
                params={{ InputProps: jest.fn } as any}
                disabled={false}
                onClick={onClick}
                searchAddressLength={5}
            />
        );

        const clearIcon = screen.getByLabelText(/Clear delivery address/i);

        userEvent.click(clearIcon);

        expect(onClick).toBeCalledTimes(1);
    });
});
