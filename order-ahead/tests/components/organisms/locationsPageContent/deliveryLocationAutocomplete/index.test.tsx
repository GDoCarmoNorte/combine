import React from 'react';
import { useRouter } from 'next/router';
import { render, screen, waitFor } from '@testing-library/react';

import DeliveryLocationAutocomplete from '../../../../../components/organisms/locationsPageContent/deliveryLocationAutocomplete';

jest.mock('next/router');

describe('DeliveryLocationAutocomplete component', () => {
    (useRouter as jest.Mock).mockReturnValue({
        query: {
            t: 'pk',
            q: '',
        },
        push: jest.fn,
    });

    it('should render DeliveryLocationAutocomplete properly', async () => {
        const { container } = render(<DeliveryLocationAutocomplete onLocationSelect={jest.fn} />);

        expect(container).toMatchSnapshot();
    });

    it('should render combobox with input and icon button', async () => {
        render(<DeliveryLocationAutocomplete onLocationSelect={jest.fn} />);

        await waitFor(() => {
            expect(screen.getByRole('combobox')).toBeInTheDocument();
        });

        expect(screen.getByLabelText(/Delivery Address Input/i)).toBeInTheDocument();
        expect(screen.getByLabelText(/Enter delivery address/i)).toBeInTheDocument();
    });
});
