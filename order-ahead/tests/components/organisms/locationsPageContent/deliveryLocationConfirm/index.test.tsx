import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';

import DeliveryLocationConfirm from '../../../../../components/organisms/locationsPageContent/deliveryLocationConfirm';

import globalPropsMock from '../../../../mocks/globalProps.mock';

jest.mock('../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

jest.mock('../../../../../redux/hooks/useNotifications');

const locationMock = {
    line1: 'Street',
    city: 'City',
    state: 'NY',
    zipCode: '100001',
};

describe('DeliveryLocationConfirm component', () => {
    it('should render DeliveryLocationConfirm properly', async () => {
        const onSubmit = jest.fn().mockResolvedValue('true');
        render(<DeliveryLocationConfirm location={locationMock} onFormSubmit={onSubmit} />);

        expect(screen.getByText(/address line 1/i)).toBeInTheDocument();
        expect(screen.getByText(/address line 2/i)).toBeInTheDocument();
        expect(screen.getByText(/business name/i)).toBeInTheDocument();
        expect(screen.getByText(/city/i)).toBeInTheDocument();
        expect(screen.getByText(/zip Code/i)).toBeInTheDocument();
        const [title, button] = screen.getAllByText(/confirm address/i);
        expect(title).toHaveClass('t-header-h3 title');
        expect(button).toHaveClass('button primary fullWidth submitButton');
    });

    it('submit form DeliveryLocationConfirm', async () => {
        const onFormSubmitMock = jest.fn();
        render(<DeliveryLocationConfirm location={locationMock} onFormSubmit={onFormSubmitMock} />);

        const [_, formSubmitButton] = screen.getAllByText(/confirm address/i);
        fireEvent.click(formSubmitButton);
        await waitFor(() => expect(onFormSubmitMock).toHaveBeenCalledTimes(1));
    });

    it('should disable submit button during submission', async () => {
        let resolveSubmission;
        const onFormSubmitMock = () =>
            new Promise<void>((res) => {
                resolveSubmission = res;
            });

        render(<DeliveryLocationConfirm location={locationMock} onFormSubmit={onFormSubmitMock} />);

        const [_, formSubmitButton] = screen.getAllByText(/confirm address/i);
        fireEvent.click(formSubmitButton);

        await waitFor(() => {
            expect(screen.getByRole('button', { name: /confirm address/i })).toHaveAttribute('disabled');
        });

        resolveSubmission();

        await waitFor(() => {
            expect(screen.getByRole('button', { name: /confirm address/i })).not.toHaveAttribute('disabled');
        });
    });
});
