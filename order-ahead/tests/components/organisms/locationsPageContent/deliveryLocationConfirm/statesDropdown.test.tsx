import React from 'react';
import { render } from '@testing-library/react';
import { useFormikContext } from 'formik';

jest.mock('formik');

import StatesDropdown from '../../../../../components/organisms/locationsPageContent/deliveryLocationConfirm/statesDropdown';

describe('StatesDropdown component', () => {
    it('should render StatesDropdown with selected state', async () => {
        (useFormikContext as jest.Mock).mockReturnValue({
            values: {
                state: 'NY',
            },
            errors: {},
            touched: {},
            setFieldValue: jest.fn(),
            setFieldTouched: jest.fn(),
            registerField: jest.fn(),
            unregisterField: jest.fn(),
            validateForm: jest.fn(),
        });
        const { container } = render(<StatesDropdown />);
        expect(container).toMatchSnapshot();
    });

    it('should render StatesDropdown without state', async () => {
        (useFormikContext as jest.Mock).mockReturnValue({
            values: {
                state: undefined,
            },
            errors: {},
            touched: {},
            setFieldValue: jest.fn(),
            setFieldTouched: jest.fn(),
            registerField: jest.fn(),
            unregisterField: jest.fn(),
            validateForm: jest.fn(),
        });
        const { container } = render(<StatesDropdown />);
        expect(container).toMatchSnapshot();
    });
});
