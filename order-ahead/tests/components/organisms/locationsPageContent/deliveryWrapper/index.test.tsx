import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';

jest.doMock('../../../../../components/organisms/locationsPageContent/deliveryLocationSearch', () => {
    const DeliveryLocationSearch = ({ onDeliveryLocationSelect, outOfTheBoundsComponent }) => {
        const { switchToPickupTab } = outOfTheBoundsComponent.props;

        return (
            <div>
                <button onClick={() => onDeliveryLocationSelect('mockedDropOff')}>Delivery Location Select Mock</button>
                <button onClick={switchToPickupTab}>Switch To Pickup Tab Trigger Mock</button>
            </div>
        );
    };
    return DeliveryLocationSearch;
});

jest.doMock('../../../../../components/organisms/locationsPageContent/deliveryLocationConfirm', () => {
    const DeliveryLocationConfirm = ({ onFormSubmit }) => (
        <button onClick={() => onFormSubmit('mockedFormData')}>Delivery Location Confirm Mock</button>
    );
    return DeliveryLocationConfirm;
});

import DeliveryWrapper from '../../../../../components/organisms/locationsPageContent/deliveryWrapper';

import { useDeliveryAddress } from '../../../../../common/hooks/useDeliveryAddress';

jest.mock('../../../../../common/hooks/useDeliveryAddress');
jest.mock('next/router', () => ({
    useRouter: () => ({
        push: jest.fn(),
    }),
}));

describe('DeliveryWrapper component', () => {
    beforeAll(() => {
        (useDeliveryAddress as jest.Mock).mockImplementation(() => ({
            setDeliveryAddress: jest.fn(),
        }));
    });

    it('should render DeliveryWrapper properly', async () => {
        const { container } = render(<DeliveryWrapper setActiveTab={jest.fn} selectLocation={jest.fn} />);
        expect(container).toMatchSnapshot();
    });

    it('DeliveryWrapper full flow test', async () => {
        const setDeliveryLocationMock = jest.fn();
        (useDeliveryAddress as jest.Mock).mockImplementation(() => ({
            setDeliveryAddress: setDeliveryLocationMock,
        }));

        const selectLocationMock = jest.fn();
        render(<DeliveryWrapper setActiveTab={jest.fn} selectLocation={selectLocationMock} />);

        const locationSelectButton = screen.getByText(/delivery location select mock/i);
        expect(locationSelectButton).toBeInTheDocument();
        fireEvent.click(locationSelectButton);

        expect(selectLocationMock).toHaveBeenCalledWith('mockedDropOff');

        const locationConfirmButton = screen.getByText(/delivery location confirm mock/i);
        expect(locationConfirmButton).toBeInTheDocument();
        fireEvent.click(locationConfirmButton);

        await waitFor(() => {
            expect(setDeliveryLocationMock).toHaveBeenCalledTimes(1);
        });
    });

    it('DeliveryWrapper trigger switchToPickupTab', async () => {
        const setActiveTab = jest.fn();
        render(<DeliveryWrapper setActiveTab={setActiveTab} selectLocation={jest.fn} />);

        const switchTabButton = screen.getByText(/switch to pickup tab trigger mock/i);
        fireEvent.click(switchTabButton);

        expect(setActiveTab).toHaveBeenCalledTimes(1);
    });
});
