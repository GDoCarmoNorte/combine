import { render, screen } from '@testing-library/react';
import React from 'react';
import NotFoundScreen from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/notFoundScreen';

jest.mock('../../../../../../components/atoms/allLocationsLink', () =>
    jest.fn((props) => <a data-testid="all-locations-link-mock">props: {JSON.stringify(props, null, 2)}</a>)
);

describe('NotFoundScreen', () => {
    const defaultProps = {
        header: 'header mock',
        body: 'body mock',
        icon: <img alt="icon mock" />,
    };
    it('should render component', () => {
        const { container } = render(<NotFoundScreen {...defaultProps} />);

        expect(container).toMatchSnapshot();
    });

    it('should render component with all locations link', () => {
        render(<NotFoundScreen {...defaultProps} showAllLocationsLink />);

        expect(screen.getByTestId('all-locations-link-mock')).toMatchSnapshot();
    });
});
