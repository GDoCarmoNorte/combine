import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useDispatch } from 'react-redux';
import LocationsListItem from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItem';
import { TServiceTypeText } from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItem/types';

import * as constants from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItem/constants';
import locationsPageMock from '../../../../../mocks/expLocations.mock';
import getLocationDetailsPageUrl from '../../../../../../common/helpers/getLocationDetailsPageUrl';

import { useRouter } from 'next/router';
import { TLocationStatusModel } from '../../../../../../@generated/webExpApi';
import { GTM_MAKE_MY_STORE, GTM_START_PICKUP_ORDER } from '../../../../../../common/services/gtmService/constants';

import configFeatureFlagsMock from '../../../../../mocks/configFeatureFlag.mock';

jest.mock('../../../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

jest.mock('react-redux');
jest.mock('../../../../../../common/helpers/getLocationDetailsPageUrl');
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({}),
    useConfiguration: () => configFeatureFlagsMock,
}));

const locationsMock = locationsPageMock.locations.slice(0, 2);

describe('LocationsListItem', () => {
    // @ts-ignore
    // eslint-disable-next-line
    constants.UNAVAILABLE_MESSAGE = 'Restaurant hours currently unavailable';
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should render not selected LocationsListItem properly', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const { container } = render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render selected LocationsListItem properly', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const { container } = render(
            <LocationsListItem
                location={locationsMock[1] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation={false}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render current location properly', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const { container } = render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render services properly', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation
            />
        );

        const servicesList = screen.getAllByTestId('serviceInfo');
        let counter = 0;
        expect(servicesList.length).toEqual(locationsMock[0].services.length);
        locationsMock[0].services.forEach((service) => {
            expect(servicesList[counter]).toHaveTextContent(TServiceTypeText[service.type]);
            counter++;
        });
    });

    it('should not render services if should be hidden', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation
                hideStoreFeatures={true}
            />
        );

        const servicesList = screen.queryAllByTestId('serviceInfo');
        expect(servicesList.length).toEqual(0);
    });

    it('should render with link to location details page', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
        (getLocationDetailsPageUrl as jest.Mock).mockReturnValueOnce('https://locations.arbys.com/locationId/99980');

        const { container } = render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should trigger onLocationSelect after click', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const handleSelectLocation = jest.fn();
        render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={handleSelectLocation}
                selected={false}
                isCurrentLocation={false}
            />
        );

        userEvent.click(screen.getByRole('button', { name: 'Select Chardon – Water St location' }));

        expect(handleSelectLocation).toHaveBeenCalled();
    });

    it('should trigger onLocationSelect after keypress', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const handleSelectLocation = jest.fn();
        render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={handleSelectLocation}
                selected={false}
                isCurrentLocation={false}
            />
        );

        userEvent.type(screen.getByRole('button', { name: 'Select Chardon – Water St location' }), '{enter}');

        expect(handleSelectLocation).toHaveBeenCalled();
    });

    it('should render "ORDER" button with "MY STORE" chip if it is OA location and selected', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <LocationsListItem
                location={locationsMock[1] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation
            />
        );

        screen.getByRole('button', { name: /Order/i });
        screen.getByText(/My store/i);
    });

    it('should render "ORDER" button with "MAKE MY STORE" link if it is OA location and non-selected', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <LocationsListItem
                location={locationsMock[1] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        screen.getByRole('button', { name: /Order/i });
        screen.getByRole('button', { name: /Make my store/i });
    });

    it('should render "VIEW MENU" button with "ONLINE ORDERING COMING SOON" link if it is non-OA location', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <LocationsListItem
                location={{ ...locationsMock[0], isDigitallyEnabled: false } as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        screen.getByRole('button', { name: /View menu/i });
        screen.getByRole('link', { name: /ONLINE ORDERING COMING SOON/i });
    });

    it('should render "VIEW MENU" button with "MY STORE" link if it is non-OA location', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <LocationsListItem
                location={{ ...locationsMock[0], isDigitallyEnabled: false } as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation
            />
        );

        screen.getByRole('button', { name: /View menu/i });
        screen.getByText(/My store/i);
    });

    it('should render "VIEW MENU" button with "MAKE MY STORE" link if it is non-OA location and with status CLOSE', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const locationMockStatusClose = {
            ...locationsMock[1],
            status: TLocationStatusModel.Closed,
        };

        render(
            <LocationsListItem
                location={locationMockStatusClose as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        screen.getByRole('button', { name: /View menu/i });
        screen.getByRole('button', { name: /Make my store/i });
        screen.getByText('Closed');
    });

    it('should not render secondary CTA if requested', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        const locationMockStatusClose = {
            ...locationsMock[1],
            status: TLocationStatusModel.Closed,
        };

        render(
            <LocationsListItem
                location={locationMockStatusClose as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
                hideSelectStoreLink={true}
            />
        );

        expect(screen.queryByRole('button', { name: /Make my store/i })).not.toBeInTheDocument();
    });

    it('should move to "/menu" if "VIEW MENU" button is clicked', async () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
        const routerPushMock = jest.fn();

        (useRouter as jest.Mock).mockImplementation(
            () =>
                ({
                    push: routerPushMock,
                } as any)
        );

        render(
            <LocationsListItem
                location={locationsMock[0] as any}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        const btn = screen.getByRole('button', { name: /View menu/i });
        userEvent.click(btn);

        expect(routerPushMock).toHaveBeenCalledWith('/menu');
    });

    it('should call onLocationSet, dispatch gtm event and redirect to /menu if "ORDER" button is clicked', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        const dispatchMock = jest.fn();
        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);
        const routerPushMock = jest.fn();

        (useRouter as jest.Mock).mockImplementation(
            () =>
                ({
                    push: routerPushMock,
                } as any)
        );

        const onLocationSetMock = jest.fn();
        const locationMock = locationsMock[1] as any;

        render(
            <LocationsListItem
                location={locationMock}
                onLocationSet={onLocationSetMock}
                onLocationSelect={jest.fn()}
                selected
                isCurrentLocation
            />
        );

        const btn = screen.getByRole('button', { name: /Order/i });
        userEvent.click(btn);

        expect(onLocationSetMock).toHaveBeenCalledWith(locationMock);
        expect(routerPushMock).toHaveBeenCalledWith('/menu');
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_START_PICKUP_ORDER });
        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_MAKE_MY_STORE,
            payload: {
                id: '99982',
                name: '5398, Unit',
            },
        });
    });

    it('should call onLocationSet and dispatch gtm event if "MAKE MY STORE" button is clicked', () => {
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        const dispatchMock = jest.fn();
        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const onLocationSetMock = jest.fn();
        const locationMock = locationsMock[1] as any;
        render(
            <LocationsListItem
                location={locationMock}
                onLocationSet={onLocationSetMock}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );
        const makeMyStoreBtn = screen.getByRole('button', { name: /Make my store/i });
        userEvent.click(makeMyStoreBtn);

        expect(onLocationSetMock).toHaveBeenCalledWith(locationMock);
        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_MAKE_MY_STORE,
            payload: {
                id: '99982',
                name: '5398, Unit',
            },
        });
    });
    it('Should display message if the SHOW_UNAVAILABLE IS TRUE', () => {
        // @ts-ignore
        // eslint-disable-next-line
        constants.SHOW_UNAVAILABLE = true;

        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        const dispatchMock = jest.fn();
        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const onLocationSetMock = jest.fn();
        const locationMock = locationsMock[1] as any;
        const modLocMock = {
            ...locationMock,
            hours: null,
        };
        render(
            <LocationsListItem
                location={modLocMock}
                onLocationSet={onLocationSetMock}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );

        expect(screen.getByText(/Restaurant hours currently unavailable/i)).toBeInTheDocument();
    });

    it('Should not display message if the SHOW_UNAVAILABLE IS FALSE', () => {
        // @ts-ignore
        // eslint-disable-next-line
        constants.SHOW_UNAVAILABLE = false;
        jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: { parentNode: document.createElement('div') } });
        jest.spyOn(React, 'useEffect').mockImplementationOnce((f) => f());
        const dispatchMock = jest.fn();
        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const onLocationSetMock = jest.fn();
        const locationMock = locationsMock[1] as any;

        render(
            <LocationsListItem
                location={locationMock}
                onLocationSet={onLocationSetMock}
                onLocationSelect={jest.fn()}
                selected={false}
                isCurrentLocation={false}
            />
        );
        expect(screen.queryByText(/Restaurant hours currently unavailable/i)).toBeNull();
    });
});
