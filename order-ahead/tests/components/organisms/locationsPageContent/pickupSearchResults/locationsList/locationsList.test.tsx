import React from 'react';
import { shallow } from 'enzyme';
import LocationsList from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList';
import locationsPageMock from '../../../../../mocks/expLocations.mock';
import ViewMoreButton from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/viewMoreButton';
import AllLocationsLink from '../../../../../../components/atoms/allLocationsLink';
import LocationsListItem from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItem';

const locationsMock = locationsPageMock.locations.slice(0, 2) as any;

describe('LocationsList', () => {
    it('should render component', () => {
        const wrapper = shallow(
            <LocationsList
                locationsList={locationsMock}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                onViewMoreButtonClick={jest.fn()}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component with view more button', () => {
        const wrapper = shallow(
            <LocationsList
                locationsList={locationsMock}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                onViewMoreButtonClick={jest.fn()}
                showViewMoreButton
            />
        );

        expect(wrapper.find(ViewMoreButton)).toMatchSnapshot();
    });

    it('should render with view all locations link', () => {
        const wrapper = shallow(
            <LocationsList
                locationsList={locationsMock}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                onViewMoreButtonClick={jest.fn()}
                showAllLocationsLink
            />
        );

        expect(wrapper.find(AllLocationsLink)).toMatchSnapshot();
    });

    it('should render with selected location', () => {
        const wrapper = shallow(
            <LocationsList
                selectedLocation={locationsMock[0]}
                locationsList={locationsMock}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                onViewMoreButtonClick={jest.fn()}
                showAllLocationsLink
            />
        );

        expect(wrapper.find(LocationsListItem).first().prop('selected')).toBe(true);
    });

    it('should render with current location', () => {
        const wrapper = shallow(
            <LocationsList
                currentLocation={locationsMock[0]}
                locationsList={locationsMock}
                onLocationSet={jest.fn()}
                onLocationSelect={jest.fn()}
                onViewMoreButtonClick={jest.fn()}
                showAllLocationsLink
            />
        );

        expect(wrapper.find(LocationsListItem).first().prop('isCurrentLocation')).toBe(true);
    });
});
