import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ViewMoreButton from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/locationsList/viewMoreButton';

describe('ViewMoreButton', () => {
    it('should render button', () => {
        render(<ViewMoreButton onClick={jest.fn} />);

        expect(screen.getByRole('button', { name: 'View more locations' })).toMatchSnapshot();
    });

    it('should render button with loading state', () => {
        render(<ViewMoreButton onClick={jest.fn} isLoading />);

        expect(screen.getByRole('button', { name: 'View more locations' })).toMatchSnapshot();
    });

    it('should call button after click', () => {
        const onClick = jest.fn();

        render(<ViewMoreButton onClick={onClick} />);
        const button = screen.getByRole('button', { name: 'View more locations' });
        userEvent.click(button);

        expect(onClick).toBeCalledTimes(1);
    });
});
