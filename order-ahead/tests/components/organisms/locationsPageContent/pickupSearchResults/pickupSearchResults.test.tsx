import React from 'react';
import { shallow } from 'enzyme';
import { LoadingStatusEnum } from '../../../../../common/types';
import PickupSearchResults from '../../../../../components/organisms/locationsPageContent/pickupSearchResults';

describe('PickupSearchResults', () => {
    const defaultProps = {
        locationsList: null,
        onLocationSet: jest.fn(),
        onLocationSelect: jest.fn(),
        onViewMoreButtonClick: jest.fn(),
        selectedLocation: null,
        currentLocation: null,
        showAllLocationsLink: true,
        moreLocationsLoading: true,
        locationFailedMessage: {
            fields: { header: 'location failed header', body: 'location failed body', icon: 'location failed icon' },
        } as any,
        locationNotFoundMessage: {
            fields: {
                header: 'location not found header',
                body: 'location not found body',
                icon: 'location not found icon',
            },
        } as any,
        isOAEnabled: true,
    };
    it('should render component if locations search status is Idle', () => {
        const wrapper = shallow(
            <PickupSearchResults {...defaultProps} locationsSearchStatus={LoadingStatusEnum.Idle} showIdleScreen />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component if locations search status is Idle and showIdleScreen prop is falsy', () => {
        const wrapper = shallow(
            <PickupSearchResults {...defaultProps} locationsSearchStatus={LoadingStatusEnum.Idle} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component if locations search status is loading', () => {
        const wrapper = shallow(
            <PickupSearchResults {...defaultProps} locationsSearchStatus={LoadingStatusEnum.Loading} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component if locations search status is error', () => {
        const wrapper = shallow(
            <PickupSearchResults {...defaultProps} locationsSearchStatus={LoadingStatusEnum.Error} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component if locations search status is success and no locations list', () => {
        const wrapper = shallow(
            <PickupSearchResults
                {...defaultProps}
                locationsSearchStatus={LoadingStatusEnum.Success}
                locationsList={null}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component if locations search status is success and empty locations list', () => {
        const wrapper = shallow(
            <PickupSearchResults
                {...defaultProps}
                locationsSearchStatus={LoadingStatusEnum.Success}
                locationsList={[]}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render component if locations search status is success and locations list is not empty', () => {
        const wrapper = shallow(
            <PickupSearchResults
                {...defaultProps}
                locationsSearchStatus={LoadingStatusEnum.Success}
                locationsList={['location mock' as any]}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render NotOrderAheadScreen component if order ahead false', () => {
        const wrapper = shallow(
            <PickupSearchResults
                {...defaultProps}
                locationsSearchStatus={LoadingStatusEnum.Success}
                locationsList={['location mock' as any]}
                isOAEnabled={false}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });
});
