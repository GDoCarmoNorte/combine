import { render, screen } from '@testing-library/react';
import React from 'react';
import IdleScreen from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/idleScreen';

jest.mock('../../../../../../components/atoms/allLocationsLink', () =>
    jest.fn((props) => <a data-testid="all-locations-link-mock">props: {JSON.stringify(props, null, 2)}</a>)
);

describe('IdleScreen', () => {
    it('should render component', () => {
        const { container } = render(<IdleScreen />);

        expect(container).toMatchSnapshot();
    });

    it('should render component with all locations link', () => {
        render(<IdleScreen showAllLocationsLink />);

        expect(screen.getByTestId('all-locations-link-mock')).toMatchSnapshot();
    });
});
