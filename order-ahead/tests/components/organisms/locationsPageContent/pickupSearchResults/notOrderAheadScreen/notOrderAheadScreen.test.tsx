import { render } from '@testing-library/react';
import React from 'react';
import NotOrderAheadScreen from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/notOrderAheadScreen';

describe('NotFoundScreen', () => {
    it('should render component', () => {
        const { container } = render(<NotOrderAheadScreen />);

        expect(container).toMatchSnapshot();
    });
});
