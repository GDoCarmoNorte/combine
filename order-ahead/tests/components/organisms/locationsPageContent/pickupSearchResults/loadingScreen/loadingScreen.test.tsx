import { render } from '@testing-library/react';
import React from 'react';
import LoadingScreen from '../../../../../../components/organisms/locationsPageContent/pickupSearchResults/loadingScreen';

describe('LoadingScreen', () => {
    it('should render component', () => {
        const { container } = render(<LoadingScreen />);

        expect(container).toMatchSnapshot();
    });
});
