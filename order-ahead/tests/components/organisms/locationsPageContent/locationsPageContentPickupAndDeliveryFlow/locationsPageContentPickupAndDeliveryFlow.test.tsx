import React from 'react';
import { useRouter } from 'next/router';
import { render } from '@testing-library/react';

import { LocationsPageContentPickupAndDeliveryFlow } from '../../../../../components/organisms/locationsPageContent';

import useOrderLocation from '../../../../../redux/hooks/useOrderLocation';
import usePickupFlow from '../../../../../common/hooks/usePickupFlow';
import { useDeliveryAddress } from '../../../../../common/hooks/useDeliveryAddress';
import { LoadingStatusEnum } from '../../../../../common/types';

jest.mock('next/router');
jest.mock('../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../common/hooks/usePickupFlow');
jest.mock('../../../../../common/hooks/useDeliveryAddress');
jest.mock('../../../../../components/clientOnly/map', () =>
    jest.fn((props) => <div className="map">props: {JSON.stringify(props, null, 2)}</div>)
);
jest.mock('../../../../../components/organisms/locationsPageContent/pickupSearchResults', () =>
    jest.fn((props) => <div className="pickupSearchResults">props: {JSON.stringify(props, null, 2)}</div>)
);
jest.mock('../../../../../components/atoms/button', () => ({
    InspireButton: jest.fn((props) => <button className="button">props: {JSON.stringify(props, null, 2)}</button>),
}));

describe('LocationsPageContentPickupFlow', () => {
    (usePickupFlow as jest.Mock).mockReturnValue({
        selectedLocation: {},
        currentLocation: {},
        locationsList: [],
        isAbleToFetchMoreLocations: true,
        locationsSearchStatus: LoadingStatusEnum.Idle,
        moreLocationsFetchStatus: LoadingStatusEnum.Idle,
        locationQuery: 'query',
        onMapLocationSelect: jest.fn(),
        onViewMoreLocationsClick: jest.fn(),
        onSearchClick: jest.fn(),
        onSearchInputChange: jest.fn(),
        onSearchInputKeyPress: jest.fn(),
        onLocationSelect: jest.fn(),
        onLocationSet: jest.fn(),
    });
    (useOrderLocation as jest.Mock).mockReturnValue({
        actions: { setDeliveryLocation: jest.fn() },
    });
    (useDeliveryAddress as jest.Mock).mockReturnValue(() => ({
        setDeliveryAddress: jest.fn(),
    }));
    (useRouter as jest.Mock).mockReturnValue({
        query: {
            t: 'pk',
            q: '',
        },
        push: jest.fn,
    });

    it('should render component', () => {
        const { container } = render(
            <LocationsPageContentPickupAndDeliveryFlow
                sections={{
                    locationNotFound: 'locationNotFound mock' as any,
                    locationFailed: 'locationFailed mock' as any,
                }}
            />
        );

        expect(container).toMatchSnapshot();
    });
});
