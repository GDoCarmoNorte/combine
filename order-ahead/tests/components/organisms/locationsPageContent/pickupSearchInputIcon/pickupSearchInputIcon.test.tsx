import React from 'react';
import { render } from '@testing-library/react';
import PickupSearchInputIcon from '../../../../../components/organisms/locationsPageContent/pickupSearchInputIcon';

describe('PickupSearchInputIcon', () => {
    it('should render component', () => {
        const { container } = render(<PickupSearchInputIcon />);

        expect(container).toMatchSnapshot();
    });
});
