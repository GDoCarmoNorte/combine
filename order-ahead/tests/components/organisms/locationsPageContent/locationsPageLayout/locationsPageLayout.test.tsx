import { render } from '@testing-library/react';
import React from 'react';
import LocationsPageLayout from '../../../../../components/organisms/locationsPageContent/locationsPageLayout';

describe('LocationsPageLayout', () => {
    it('should render component', () => {
        const { container } = render(
            <LocationsPageLayout
                renderMap={() => <div className="map-content" />}
                renderSearchPanel={() => <div className="search-panel" />}
            />
        );

        expect(container).toMatchSnapshot();
    });
});
