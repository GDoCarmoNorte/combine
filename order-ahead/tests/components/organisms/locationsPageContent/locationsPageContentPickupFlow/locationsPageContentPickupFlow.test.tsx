import React from 'react';
import { LocationsPageContentPickupFlow } from '../../../../../components/organisms/locationsPageContent';
import { render } from '@testing-library/react';
import usePickupFlow from '../../../../../common/hooks/usePickupFlow';
import { LoadingStatusEnum } from '../../../../../common/types';

jest.mock('../../../../../common/hooks/usePickupFlow');
jest.mock('../../../../../components/clientOnly/map', () =>
    jest.fn((props) => <div className="map">props: {JSON.stringify(props, null, 2)}</div>)
);
jest.mock('../../../../../components/organisms/locationsPageContent/pickupSearchResults', () =>
    jest.fn((props) => <div className="pickupSearchResults">props: {JSON.stringify(props, null, 2)}</div>)
);
jest.mock('../../../../../components/atoms/allLocationsLink', () =>
    jest.fn((props) => <a className="allLocationsLink">props: {JSON.stringify(props, null, 2)}</a>)
);

describe('LocationsPageContentPickupFlow', () => {
    (usePickupFlow as jest.Mock).mockReturnValue({
        selectedLocation: {},
        currentLocation: {},
        locationsList: [],
        isAbleToFetchMoreLocations: true,
        locationsSearchStatus: LoadingStatusEnum.Idle,
        moreLocationsFetchStatus: LoadingStatusEnum.Idle,
        locationQuery: 'query',
        onMapLocationSelect: jest.fn(),
        onViewMoreLocationsClick: jest.fn(),
        onSearchClick: jest.fn(),
        onSearchInputChange: jest.fn(),
        onSearchInputKeyPress: jest.fn(),
        onLocationSelect: jest.fn(),
        onLocationSet: jest.fn(),
    });

    it('should render component', () => {
        const { container } = render(
            <LocationsPageContentPickupFlow
                sections={{
                    locationNotFound: 'locationNotFound mock' as any,
                    locationFailed: 'locationFailed mock' as any,
                }}
            />
        );

        expect(container).toMatchSnapshot();
    });
});
