export const dropOffMock = {
    address: {
        line1: '7947 Golden Valley Rd',
        postalCode: '55427-4402',
        stateProvinceCode: 'MN',
        countryCode: 'US',
        city: 'Minneapolis',
    },
};
export const pickUpMock = {
    id: 5,
    name: '15.1 Lab 9009',
    details: {
        latitude: 44.98513,
        longitude: -93.3816777,
    },
    distance: {
        amount: 0,
        unit: 'mi',
    },
    maximumDeliveryRadius: {
        amount: 50,
        unit: 'mi',
    },
    maximumDeliveryFee: 7,
    address: {
        line1: '7914 Olson Memorial Hwy.',
        postalCode: '55427',
        stateProvinceCode: 'MN',
        countryCode: 'US',
        city: 'Golden Valley',
    },
    phone: '360-758-2190',
    isDeliveryEnabled: true,
    deliveryServices: ['favor', 'notipmock'],
    deliveryServicesPreferred: ['notipmock'],
    utcOffset: 'UTC-05:00',
    hoursByDay: {},
};

export const getDeliveryLocationByPlaceIdMock = {
    isInDeliveryZone: true,
    dropOff: dropOffMock,
    pickUp: pickUpMock,
};

export const getDeliveryLocationByPlaceIdMockOutOfBounds = {
    isInDeliveryZone: false,
};
