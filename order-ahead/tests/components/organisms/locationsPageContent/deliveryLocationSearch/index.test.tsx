import React from 'react';
import { render, screen, fireEvent, waitFor, waitForElementToBeRemoved } from '@testing-library/react';

import {
    dropOffMock,
    pickUpMock,
    getDeliveryLocationByPlaceIdMock,
    getDeliveryLocationByPlaceIdMockOutOfBounds,
} from './deliveryLocationSearch.mock';
import * as deliveryLocationServices from '../../../../../common/services/deliveryLocation';
jest.doMock('../../../../../components/organisms/locationsPageContent/deliveryLocationAutocomplete', () => {
    const DeliveryLocationAutocomplete = ({ onLocationSelect }) => (
        <button onClick={onLocationSelect}>Autocomplete Mock</button>
    );
    return DeliveryLocationAutocomplete;
});

import DeliveryLocationSearch from '../../../../../components/organisms/locationsPageContent/deliveryLocationSearch';

describe('DeliveryLocationSearch component', () => {
    it('should render DeliveryLocationSearch properly', async () => {
        const { container } = render(
            <DeliveryLocationSearch
                onDeliveryLocationSelect={jest.fn}
                outOfTheBoundsComponent={<p>Mock out of the bounds</p>}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render DeliveryLocationSearch out of bounds scenario', async () => {
        jest.spyOn(deliveryLocationServices, 'getDeliveryLocationByPlaceId').mockResolvedValue(
            getDeliveryLocationByPlaceIdMockOutOfBounds as any
        );

        render(
            <DeliveryLocationSearch
                onDeliveryLocationSelect={jest.fn}
                outOfTheBoundsComponent={<p>Mock out of the bounds</p>}
            />
        );

        const autocompleteMockButton = screen.getByText(/autocomplete mock/i);
        expect(autocompleteMockButton).toBeInTheDocument();

        fireEvent.click(autocompleteMockButton);

        const loadingBar = screen.getByRole('progressbar');
        expect(loadingBar).toBeInTheDocument();
        await waitForElementToBeRemoved(loadingBar);
        await screen.findByText(/mock out of the bounds/i);
    });

    it('should render DeliveryLocationSearch confirm address scenario', async () => {
        jest.spyOn(deliveryLocationServices, 'getDeliveryLocationByPlaceId').mockResolvedValue(
            getDeliveryLocationByPlaceIdMock as any
        );

        const onDeliveryLocationSelectMock = jest.fn();
        render(
            <DeliveryLocationSearch
                onDeliveryLocationSelect={onDeliveryLocationSelectMock}
                outOfTheBoundsComponent={<p>Mock out of the bounds</p>}
            />
        );

        fireEvent.click(screen.getByText(/autocomplete mock/i));

        const loadingBar = screen.getByRole('progressbar');
        await waitForElementToBeRemoved(loadingBar);
        await waitFor(() => {
            expect(onDeliveryLocationSelectMock).toHaveBeenCalledTimes(1);
        });
        expect(onDeliveryLocationSelectMock).toHaveBeenCalledWith(dropOffMock, pickUpMock);
    });
});
