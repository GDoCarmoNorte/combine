import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import DeliveryOutOfBounds from '../../../../../components/organisms/locationsPageContent/deliveryLocationSearch/deliveryOutOfBounds';

import globalPropsMock from '../../../../mocks/globalProps.mock';

jest.mock('../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

describe('DeliveryOutOfBounds component', () => {
    it('should render DeliveryOutOfBounds properly', async () => {
        render(<DeliveryOutOfBounds switchToPickupTab={jest.fn} />);
        const outOfBoundsText =
            "It looks like your address isn't in our delivery area. Try entering a new address above.";

        expect(screen.getByText(/out of bounds/i)).toBeInTheDocument();
        expect(screen.getByText(outOfBoundsText)).toBeInTheDocument();
        expect(screen.getByText(/pickup instead/i)).toBeInTheDocument();
    });

    it('should call "switchToPickupTab" prop function on click', async () => {
        const switchToPickupTabMock = jest.fn();
        render(<DeliveryOutOfBounds switchToPickupTab={switchToPickupTabMock} />);

        fireEvent.click(screen.getByText(/pickup instead/i));
        expect(switchToPickupTabMock).toHaveBeenCalledTimes(1);
    });
});
