import { useLocationOrderAheadAvailability } from '../../../../common/hooks/useLocationOrderAheadAvailability';
import { CheckLocation } from '../../../../components/organisms/navigation/navigationUtils';

jest.mock('../../../../common/hooks/useLocationOrderAheadAvailability', () => ({
    useLocationOrderAheadAvailability: jest.fn(),
}));

describe('Navigation utils', () => {
    it('CheckLocation function', () => {
        const locationOrderAheadAvailability: any = { isLocationOrderAheadAvailable: false, location: {} };
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue(locationOrderAheadAvailability);

        const location = CheckLocation();
        expect(location).toBe(false);
    });
});
