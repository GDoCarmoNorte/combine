import React from 'react';
import { fireEvent, render, screen } from '../../../../utils';
import { useAuth0 } from '@auth0/auth0-react';
import SignIn from '../../../../../components/organisms/navigation/signIn';
import { mocked } from 'ts-jest/utils';
import useAccount from '../../../../../redux/hooks/useAccount';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import useRewards from '../../../../../redux/hooks/useRewards';
import { TOfferStatusModel } from '../../../../../@generated/webExpApi';
import { useRouter } from 'next/router';
import MockUserAccountMenuData from '../../../../__mocks__/contentful/userAccountMenu.mock.json';
import { isAccountDealsPageOn, isRewardsOn } from '../../../../../lib/getFeatureFlags';
import { useLoyalty } from '../../../../../redux/hooks';
import configurationMock from '../../../../mock-data/provider/configuration.mock.json';

jest.mock('@auth0/auth0-react');
jest.mock('../../../../../redux/hooks/useAccount', () =>
    jest.fn().mockReturnValue({
        accountInfo: {
            email: 'johndoe@me.com',
            firstName: 'johndoe',
        },
        account: {
            email: 'johndoe@me.com',
            firstName: 'johndoe',
        },
    })
);
jest.mock('@material-ui/core/useMediaQuery', () => jest.fn().mockReturnValue(true));

jest.mock('../../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => ({ alertBanners: { total: 0 } })),
    useDismissedAlertBanners: jest.fn(() => undefined).mockImplementation(() => ({ bannerEntries: [] })),
    useSelectedSell: () => ({ correlationId: '1' }),
    useLoyalty: jest.fn().mockReturnValue({
        actions: {
            setLoyaltyPointsLoading: jest.fn(),
            setLoyaltyPoints: jest.fn(),
        },
        loyalty: { pointsBalance: 0 },
    }),
}));

jest.mock(
    '../../../../../components/organisms/navigation/navigationDropdown/navigationDropdown',
    () => ({ children }) => children
);

jest.mock('../../../../../redux/hooks/useRewards', () =>
    jest.fn().mockReturnValue({
        offers: [],
        rewardsActivityHistory: [],
    })
);
jest.mock('../../../../../redux/hooks/useLoyalty');
jest.mock('../../../../../lib/getFeatureFlags');

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('../../../../../redux/hooks/useNotifications', () =>
    jest.fn().mockReturnValue({
        actions: {
            enqueueError: jest.fn(),
        },
    })
);
jest.mock('../../../../../common/hooks/useConfiguration', () => ({
    createConfiguration: jest.fn(),
    useConfiguration: () => configurationMock,
}));

const mockedUseAuth0 = mocked(useAuth0, true);

describe('SignIn component', () => {
    beforeEach(() => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: true,
            user: {},
            logout: jest.fn(),
            loginWithRedirect: jest.fn(),
            getAccessTokenWithPopup: jest.fn(),
            getAccessTokenSilently: jest.fn(),
            handleRedirectCallback: jest.fn(),
            getIdTokenClaims: jest.fn(),
            loginWithPopup: jest.fn(),
            isLoading: false,
        } as any);

        (useRouter as jest.Mock).mockReturnValue({
            pathname: '/',
        });

        (isAccountDealsPageOn as jest.Mock).mockReturnValueOnce(true);
        (isRewardsOn as jest.Mock).mockReturnValue(true);
    });

    it('should match desc snapshot', () => {
        const { container } = render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(container).toMatchSnapshot();
    });

    it('should match mobile snapshot', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(false);
        const { container } = render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(container).toMatchSnapshot();
    });

    it('should match not authorized snapshot', () => {
        mockedUseAuth0.mockReturnValueOnce({
            isAuthenticated: false,
            user: null,
            logout: jest.fn(),
            loginWithRedirect: jest.fn(),
            getAccessTokenWithPopup: jest.fn(),
            getAccessTokenSilently: jest.fn(),
            handleRedirectCallback: jest.fn(),
            getIdTokenClaims: jest.fn(),
            loginWithPopup: jest.fn(),
            isLoading: false,
        } as any);

        (useAccount as jest.Mock).mockReturnValueOnce({
            account: null,
            accountInfo: null,
        });

        const { container } = render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(container).toMatchSnapshot();
    });

    it('should match loading snapshot', () => {
        mockedUseAuth0.mockReturnValueOnce({
            isAuthenticated: true,
            user: {},
            logout: jest.fn(),
            loginWithRedirect: jest.fn(),
            getAccessTokenWithPopup: jest.fn(),
            getAccessTokenSilently: jest.fn(),
            handleRedirectCallback: jest.fn(),
            getIdTokenClaims: jest.fn(),
            loginWithPopup: jest.fn(),
            isLoading: true,
        } as any);

        const { container } = render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(container).toMatchSnapshot();
    });

    it('should display "Hi johndoe" in 2 places for mobile', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(false);
        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        const text = /Hi johndoe/i;

        expect(screen.queryAllByText(text)).toHaveLength(2);
    });
    it('username should be gotten from redux if it is in state', () => {
        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        const text = /Hi johndoe/i;
        expect(screen.getByText(text)).toHaveClass('signInText');
    });

    it('if there are active offers the counter should be shown', () => {
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        const text = 'deals counter';
        expect(screen.getByLabelText(text)).toBeInTheDocument();
    });

    it('if totalCount >= 1 should show counter when isRewardsOn', () => {
        (useRewards as jest.Mock).mockReturnValue({
            offers: [],
            totalCount: 1,
            rewardsActivityHistory: [],
        });
        (isAccountDealsPageOn as jest.Mock).mockReturnValueOnce(false);

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        const text = 'rewards counter';
        expect(screen.getByLabelText(text)).toBeInTheDocument();
    });

    it('Should show points and rewards total if presented', () => {
        const pointsBalance = 800;
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance,
            },
        });
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
            totalCount: 12,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.getByText(/800 pts/i)).toBeInTheDocument();
        expect(screen.getByText(/12 rewards/i)).toBeInTheDocument();
    });
    it('Should show only points and hide rewards total if not presented', () => {
        const pointsBalance = 800;
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance,
            },
        });
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
            totalCount: 0,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.getByText(`${pointsBalance.toLocaleString()} pts`)).toBeInTheDocument();
        expect(screen.queryByText(/0 rewards/i)).not.toBeInTheDocument();
    });
    it('Should not show points if Rewards feature flag is turned off', () => {
        const pointsBalance = 800;
        (isRewardsOn as jest.Mock).mockReturnValue(false);
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance,
            },
        });
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
            totalCount: 10,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.queryByText(`${pointsBalance.toLocaleString()} pts`)).not.toBeInTheDocument();
        expect(screen.queryByText(/10 rewards/i)).not.toBeInTheDocument();
    });
    it('Should not show points if there is error while points retrieving', () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
            },
            error: 'test error',
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.queryByText(/0 pts/i)).not.toBeInTheDocument();
    });
    it('Should not show 0 points if no points returned', () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: undefined,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.queryByText(/0 pts/i)).not.toBeInTheDocument();
    });

    it('should show email verification modal with success title', () => {
        const redirect = jest.fn();
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                isEmailVerification: 'true',
            },
            push: redirect,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);

        const title = screen.getByText(/Account was verified!/i);
        const redirectButton = screen.getByText(/Go to rewards roster/i);

        expect(title).toBeInTheDocument();

        fireEvent.click(redirectButton);

        expect(redirect).toHaveBeenCalledTimes(1);
        expect(title).not.toBeInTheDocument();
    });

    it('should show email verification modal with unsuccess title', () => {
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                isEmailVerification: '',
            },
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);

        const closeButton = screen.getByText(/Close/i);
        const title = screen.getByText(/Account was not verified/i);

        expect(title).toBeInTheDocument();

        fireEvent.click(closeButton);

        expect(title).not.toBeInTheDocument();
    });

    it('should show rewards info points in mobile view', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(false);

        const pointsBalance = 800;
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance,
            },
        });
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
            totalCount: 0,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.getByText(/800 pts/i)).toBeInTheDocument();
        expect(screen.queryByText(/0 rewards/i)).not.toBeInTheDocument();
    });

    it('should show rewards info rewards in mobile view', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(false);

        const pointsBalance = 0;
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance,
            },
        });
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
            totalCount: 12,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.queryByText(/0 pts/i)).not.toBeInTheDocument();
        expect(screen.getByText(/12 rewards/i)).toBeInTheDocument();
    });

    it('should show rewards info rewards and points in mobile view', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(false);

        const pointsBalance = 800;
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance,
            },
        });
        (useRewards as jest.Mock).mockReturnValue({
            offers: [
                {
                    status: TOfferStatusModel.Open,
                },
            ],
            rewardsActivityHistory: [],
            totalCount: 12,
        });

        render(<SignIn userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.getByText(/800 pts/i)).toBeInTheDocument();
        expect(screen.getByText(/12 rewards/i)).toBeInTheDocument();
    });
});
