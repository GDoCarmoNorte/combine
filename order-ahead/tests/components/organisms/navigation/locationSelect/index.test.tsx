import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import LocationSelect from '../../../../../components/organisms/navigation/locationSelect';

import {
    isLocationClosed,
    isLocationOrderAheadAvailable,
    resolveOpeningHours,
    getBrandDefaultStatusText,
    getDeliveryAddressText,
} from '../../../../../lib/locations';
import useOrderLocation from '../../../../../redux/hooks/useOrderLocation';
import useBag from '../../../../../redux/hooks/useBag';
import { useConfiguration } from '../../../../../redux/hooks';
import LocationsMock from '../../../../mocks/expLocations.mock';
import DeliveryAddressMock from '../../../../mocks/deliveryAddess.mock';
import globalPropsMock from '../../../../mocks/globalProps.mock';
import { OrderLocationMethod } from '../../../../../redux/orderLocation';
import { useAppSelector } from '../../../../../redux/store';
import configFeatureFlagsMocks from '../../../../mocks/configFeatureFlag.mock';

const locationMock = LocationsMock.locations[0];

jest.mock('react-redux');
jest.mock('../../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: () => jest.fn(),
}));
jest.mock('../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../redux/hooks/useBag', () => jest.fn().mockReturnValue({}));
jest.mock('../../../../../lib/locations');
jest.mock('../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

jest.mock('../../../../../redux/hooks/useConfiguration');

jest.mock('../../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

jest.mock('../../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmLocationOrder: jest.fn(),
        pushGtmChangeLocation: jest.fn(),
    }),
}));

describe('navigation locationSelect component', () => {
    beforeAll(() => {
        (getBrandDefaultStatusText as jest.Mock).mockReturnValue('default location status text');
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: configFeatureFlagsMocks,
        });
        (useAppSelector as jest.Mock).mockReturnValue(configFeatureFlagsMocks);
    });

    it('should render correctly if no location selected', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.NOT_SELECTED,
        });

        render(<LocationSelect />);
        expect(screen.getByText(/select location/i)).toBeInTheDocument();
        expect(screen.getByText(/find an arby’s/i)).toBeInTheDocument();
    });

    it('should render correctly if delivery location selected', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.DELIVERY,
            deliveryAddress: DeliveryAddressMock,
        });
        (getDeliveryAddressText as jest.Mock).mockReturnValue('7947 Golden Valley Rd, 55427-4402');

        render(<LocationSelect />);
        expect(screen.getByText(/deliver to/i)).toBeInTheDocument();
        expect(screen.getByText(/7947 Golden Valley Rd, 55427-4402/i)).toBeInTheDocument();
    });

    it('should render correctly if closed pickup location selected (isLocationOrderAheadAvailable = false)', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(false);

        render(<LocationSelect />);
        expect(screen.getByText(/default location status text/i)).toBeInTheDocument();
        expect(screen.getByText(locationMock.displayName)).toBeInTheDocument();
    });

    it('should render correctly if closed pickup location selected (isLocationClosed = true)', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
        (isLocationClosed as jest.Mock).mockReturnValue(true);

        render(<LocationSelect />);
        expect(screen.getByText(/default location status text/i)).toBeInTheDocument();
        expect(screen.getByText(locationMock.displayName)).toBeInTheDocument();
    });

    it('should render correctly if open pickup location selected', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (useBag as jest.Mock).mockReturnValue({
            bagEntriesCount: 0,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
        (isLocationClosed as jest.Mock).mockReturnValue(false);
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: false,
            isOpeningSoon: false,
            isClosingSoon: false,
        });

        render(<LocationSelect />);
        expect(screen.getByText(/pickup from/i)).toBeInTheDocument();
    });

    it('should render correctly if pickup location will be closed soon', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (useBag as jest.Mock).mockReturnValue({
            bagEntriesCount: 0,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
        (isLocationClosed as jest.Mock).mockReturnValue(false);
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: true,
            isOpeningSoon: false,
            isClosingSoon: true,
        });

        render(<LocationSelect />);
        expect(screen.getByText(/store closing soon/i)).toBeInTheDocument();
    });

    it('should render correctly if pickup location will be open soon', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (useBag as jest.Mock).mockReturnValue({
            bagEntriesCount: 0,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);
        (isLocationClosed as jest.Mock).mockReturnValue(false);
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            isOpen: false,
            isOpeningSoon: true,
            isClosingSoon: false,
        });

        render(<LocationSelect />);
        expect(screen.getByText(/store opening soon/i)).toBeInTheDocument();
    });

    it('should open and close dropdown correctly on button click', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(false);

        render(<LocationSelect />);
        const locationBlockButton = screen.getByRole('button');
        const chevron = screen.getByRole('status');

        userEvent.click(locationBlockButton);
        expect(chevron).toHaveClass('direction-up');

        userEvent.click(locationBlockButton);
        expect(chevron).toHaveClass('direction-down');
    });

    it('should disabled dropdown when order ahead false', () => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isOAEnabled: false,
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: locationMock,
        });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(false);

        render(<LocationSelect />);
        const locationBlockButton = screen.getByRole('button');

        userEvent.click(locationBlockButton);
        expect(screen.queryByRole('tablist')).not.toBeInTheDocument();
    });
});
