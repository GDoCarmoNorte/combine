import React from 'react';
import { render, screen } from '@testing-library/react';
import LocationTabs from '../../../../../../../components/organisms/navigation/locationSelect/locationDropdown/locationTabs';
import deliveryAddressMock from '../../../../../../mocks/deliveryAddess.mock';
import useOrderLocation from '../../../../../../../redux/hooks/useOrderLocation';
import useDomainMenu from '../../../../../../../redux/hooks/useDomainMenu';
import { OrderLocationMethod } from '../../../../../../../redux/orderLocation';
import LocationsMock from '../../../../../../mocks/expLocations.mock';
import globalPropsMock from '../../../../../../mocks/globalProps.mock';
import { useRouter } from 'next/router';
import { useAppSelector } from '../../../../../../../redux/store';
import { useBag, useConfiguration } from '../../../../../../../redux/hooks';
import useBagAvailableItemsCounter from '../../../../../../../common/hooks/useBagAvailableItemsCounter';
import configFeatureFlags from '../../../../../../mocks/configFeatureFlag.mock';
import * as constants from '../../../../../../../components/organisms/navigation/locationSelect/locationDropdown/locationTabs/locationMethodTab/constants';
import { useBagAnalytics } from '../../../../../../../common/hooks/useBagAnalytics';
const locationMockOANotAvailable = LocationsMock.locations[0];
const locationMockOAAvailable = LocationsMock.locations[1];
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../../../redux/hooks/useDomainMenu');
jest.mock('../../../../../../../common/hooks/useBagAvailableItemsCounter');
jest.mock('../../../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);
jest.mock('../../../../../../../redux/hooks/useBag');
jest.mock('../../../../../../../redux/hooks/useConfiguration');
jest.mock('../../../../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

jest.mock('../../../../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmLocationOrder: jest.fn(),
        pushGtmChangeLocation: jest.fn(),
    }),
}));

jest.mock('../../../../../../../common/helpers/getStoreStatus', () => ({
    getStoreStatus: jest.fn().mockReturnValue({
        statusText: '',
    }),
}));

describe('locationTabs', () => {
    const getDomainMenuMock = jest.fn();
    (useDomainMenu as jest.Mock).mockReturnValue({ actions: { getDomainMenu: getDomainMenuMock } });

    const props = {
        closeDropdown: jest.fn(),
    };

    const routerStub = {
        push: jest.fn(),
    };

    beforeEach(() => {
        (useBag as jest.Mock).mockReturnValue({
            actions: {
                toggleIsOpen: jest.fn(),
            },
        });
        (useAppSelector as jest.Mock).mockReturnValue(configFeatureFlags);
        (useConfiguration as jest.Mock).mockReturnValue(configFeatureFlags);
        (useBagAnalytics as jest.Mock).mockReturnValue({
            pushGtmLocationOrder: jest.fn(),
            pushGtmChangeLocation: jest.fn(),
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('handleStartOrderClick', () => {
        it('delivery tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.DELIVERY,
                deliveryAddress: deliveryAddressMock,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            screen.getByText(/start delivery order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('pickup tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            screen.getByText(/start pickup order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('pickup tab with unavailable location', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOANotAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);
            screen.getByText(/start order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('should call pushGtmLocationOrder method when click on CTA "start order"', () => {
            const pushGtmMock = jest.fn();
            (useBagAnalytics as jest.Mock).mockReturnValue({
                pushGtmLocationOrder: pushGtmMock,
                pushGtmChangeLocation: jest.fn(),
            });

            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOANotAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);
            screen.getByText(/start order/i).click();

            expect(pushGtmMock).toHaveBeenCalledWith(OrderLocationMethod.PICKUP, '5398');
            expect(pushGtmMock).toBeCalledTimes(1);
        });
    });

    describe('handleChangeLocationClick', () => {
        it('delivery tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.DELIVERY,
                deliveryAddress: deliveryAddressMock,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            screen.getByText(/Change Address/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('should call pushGtmChangeLocation when user click CTA change Address', () => {
            const pushGtmMock = jest.fn();
            (useBagAnalytics as jest.Mock).mockReturnValue({
                pushGtmChangeLocation: pushGtmMock,
            });

            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.DELIVERY,
                deliveryAddress: deliveryAddressMock,
                actions: {
                    setMethod: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            screen.getByText(/Change Address/i).click();

            expect(pushGtmMock).toBeCalledTimes(1);
        });

        it('pickup tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            screen.getByText(/Change Address/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('pickup tab with incorrect zip', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);
            screen.getByText(/Change Address/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });
    });

    describe('handleStartOrderClick when shopping bag is not empty', () => {
        it('delivery tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.DELIVERY,
                deliveryAddress: deliveryAddressMock,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useBag as jest.Mock).mockReturnValue({
                bagEntriesCount: 1,
                actions: {
                    toggleIsOpen: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);
            (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(1);

            render(<LocationTabs {...props} />);

            screen.getByText(/continue delivery order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).not.toBeCalled();
        });

        it('pickup tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useBag as jest.Mock).mockReturnValue({
                bagEntriesCount: 2,
                actions: {
                    toggleIsOpen: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);
            (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(1);

            render(<LocationTabs {...props} />);

            screen.getByText(/continue pickup order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).not.toBeCalled();
        });
    });

    describe('handleStartOrderClick when shopping bag with unavailable items', () => {
        it('delivery tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.DELIVERY,
                deliveryAddress: deliveryAddressMock,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useBag as jest.Mock).mockReturnValue({
                bagEntriesCount: 10,
                actions: {
                    toggleIsOpen: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);
            (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(0);

            render(<LocationTabs {...props} />);

            screen.getByText(/continue delivery order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('pickup tab', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useBag as jest.Mock).mockReturnValue({
                bagEntriesCount: 3,
                actions: {
                    toggleIsOpen: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);
            (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(0);

            render(<LocationTabs {...props} />);

            screen.getByText(/continue pickup order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });
    });

    describe('Should not display message if the SHOW_UNAVAILABLE IS TRUE/FALSE', () => {
        // @ts-ignore
        // eslint-disable-next-line
        constants.UNAVAILABLE_MESSAGE = 'Restaurant hours currently unavailable';
        it('SHOW_UNAVAILABLE is TRUE', () => {
            // @ts-ignore
            // eslint-disable-next-line
            constants.SHOW_UNAVAILABLE = true;

            const moddedLocationMockOAAvailable = { ...locationMockOAAvailable, hours: null };
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: moddedLocationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            expect(screen.getByText(/Restaurant hours currently unavailable/i)).toBeInTheDocument();
        });
        it('SHOW_UNAVAILABLE is FALSE', () => {
            // @ts-ignore
            // eslint-disable-next-line
            constants.SHOW_UNAVAILABLE = false;
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
                actions: {
                    setMethod: jest.fn(),
                    setPreviousLocation: jest.fn(),
                },
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationTabs {...props} />);

            expect(screen.queryByText(/Restaurant hours currently unavailable/i)).toBeNull();
        });
    });
});
