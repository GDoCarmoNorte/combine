import React from 'react';
import { render, screen } from '@testing-library/react';
import { useAppSelector } from '../../../../../../redux/store';
import LocationDropdown from '../../../../../../components/organisms/navigation/locationSelect/locationDropdown/locationDropdown';
import useOrderLocation from '../../../../../../redux/hooks/useOrderLocation';
import { OrderLocationMethod } from '../../../../../../redux/orderLocation';
import LocationsMock from '../../../../../mocks/expLocations.mock';
import globalPropsMock from '../../../../../mocks/globalProps.mock';
import configFeatureFlagsMocks from '../../../../../mocks/configFeatureFlag.mock';
import { useRouter } from 'next/router';
import NavigationDropdown from '../../../../../../components/organisms/navigation/navigationDropdown/navigationDropdown';
import { useDispatch } from 'react-redux';
import { useBag, useConfiguration } from '../../../../../../redux/hooks';

const locationMockOAAvailable = LocationsMock.locations[1];
const locationMockOANotAvailable = LocationsMock.locations[0];

jest.mock('react-redux');
jest.mock('../../../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: () => jest.fn(),
}));
jest.mock('../../../../../../redux/hooks/useConfiguration');
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../../redux/hooks/useBag');
jest.mock('../../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);
jest.mock('../../../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

jest.mock('../../../../../../components/organisms/navigation/navigationDropdown/navigationDropdown', () => ({
    __esModule: true,
    default: jest.fn(),
}));

jest.mock('../../../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmLocationOrder: jest.fn(),
        pushGtmChangeLocation: jest.fn(),
    }),
}));

describe('locationDropdown', () => {
    const props = {
        navRef: {},
        controlRef: { current: { getBoundingClientRect: jest.fn().mockReturnValue({ left: 0 }) } },
        isDropdownOpen: true,
        closeDropdown: jest.fn(),
    } as any;

    const routerStub = {
        push: jest.fn(),
    };

    beforeEach(() => {
        (useAppSelector as jest.Mock).mockReturnValue(configFeatureFlagsMocks);
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                ...configFeatureFlagsMocks.configuration,
                isDeliveryEnabled: false,
            },
        });
        (NavigationDropdown as jest.Mock).mockImplementation(
            jest.requireActual(
                '../../../../../../components/organisms/navigation/navigationDropdown/navigationDropdown'
            ).default
        );
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('handleStartOrderClick', () => {
        beforeEach(() => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [],
                entriesMarkedAsRemoved: [],
                isOpen: true,
                actions: {
                    toggleIsOpen: jest.fn(),
                },
                bagEntriesCount: 1,
            });
        });
        it('should work correctly', () => {
            (useAppSelector as jest.Mock).mockReturnValue(configFeatureFlagsMocks.configuration);

            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
            });
            (useBag as jest.Mock).mockReturnValue({
                bagEntriesCount: 0,
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationDropdown {...props} />);

            screen.getByText(/start pickup order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('should work correctly for non oa location', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOANotAvailable,
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationDropdown {...props} />);

            screen.getByText(/start order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('should work correctly when shopping bag is not empty', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
            });
            (useBag as jest.Mock).mockReturnValue({
                bagEntriesCount: 1,
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);

            render(<LocationDropdown {...props} />);

            screen.getByText(/continue pickup order/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });
    });

    describe('handleChangeLocationClick', () => {
        beforeEach(() => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [],
                entriesMarkedAsRemoved: [],
                isOpen: true,
                actions: {
                    toggleIsOpen: jest.fn(),
                },
                bagEntriesCount: 1,
            });
        });

        it('should work correctly', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOAAvailable,
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);
            (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
            render(<LocationDropdown {...props} />);

            screen.getByText(/Change Address/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });

        it('should work with incorrect zip', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: locationMockOANotAvailable,
            });
            (useRouter as jest.Mock).mockReturnValue(routerStub);
            (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
            render(<LocationDropdown {...props} />);
            screen.getByText(/Change Address/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });
    });

    it('should close dropdown', () => {
        const MockModal = ({ onClose }) => <div onClick={onClose}>close</div>;
        (NavigationDropdown as jest.Mock).mockImplementation(MockModal);

        render(<LocationDropdown {...props} />);

        screen.getByText('close').click();

        expect(props.closeDropdown).toBeCalledTimes(1);
    });
});
