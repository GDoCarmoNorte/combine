import React from 'react';
import { render, screen } from '@testing-library/react';
import globalPropsMock from '../../../../../mocks/globalProps.mock';
import { useRouter } from 'next/router';
import PickupLocationFind from '../../../../../../components/organisms/navigation/locationSelect/pickupLocationFind';
import Input from '../../../../../../components/atoms/Input/input';
import { OrderLocationMethod } from '../../../../../../redux/orderLocation';

const MockInput = ({ onClick, handleKeyPress, onChange }) => (
    <input onKeyPress={handleKeyPress} onClick={onClick} onChange={onChange} data-testid="input" />
);

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);
jest.mock('../../../../../../components/atoms/Input/input', () => ({
    __esModule: true,
    default: jest.fn(),
}));

describe('pickupLocationFind', () => {
    const props = {
        closeDropdown: jest.fn(),
    } as any;

    const routerStub = {
        push: jest.fn(),
    };

    beforeEach(() => {
        (Input as any).mockImplementation(MockInput);
        (useRouter as jest.Mock).mockReturnValue(routerStub);
        (global.navigator.geolocation.getCurrentPosition as jest.Mock).mockImplementation((callback) =>
            callback({ coords: { latitude: 0, longtitude: 0 } })
        );
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('handleGeolocationClick', () => {
        it('should work correctly', () => {
            render(<PickupLocationFind method={OrderLocationMethod.PICKUP} {...props} />);

            screen.getByText(/use my location/i).click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });
    });

    describe('input events', () => {
        it('handleClick', () => {
            render(<PickupLocationFind {...props} />);

            screen.getByTestId('input').click();

            expect(props.closeDropdown).toBeCalledTimes(1);
            expect(routerStub.push).toBeCalledTimes(1);
        });
    });
});
