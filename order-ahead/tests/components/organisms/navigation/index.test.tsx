import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { shallow } from 'enzyme';
import { mocked } from 'ts-jest/utils';
import { useRouter } from 'next/router';
import { useAuth0 } from '@auth0/auth0-react';

import Navigation from '../../../../components/organisms/navigation';

import MockData from './index.mock.json';
import MockUserAccountMenuData from '../../../__mocks__/contentful/userAccountMenu.mock.json';
import NavigationLink from '../../../../components/organisms/navigation/navigationLink';
import useDismissedAlertBanners from '../../../../redux/hooks/useDismissedAlertBanners';
import { OrderLocationMethod } from '../../../../redux/orderLocation';
import useBagAvailableItemsCounter from '../../../../common/hooks/useBagAvailableItemsCounter';

jest.mock('@auth0/auth0-react');

const mockedUseAuth0 = mocked(useAuth0, true);

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch,
    createSelectorHook: jest.fn(),
}));

jest.mock('../../../../redux/hooks/useDismissedAlertBanners');
jest.mock('../../../../common/hooks/useBagAvailableItemsCounter');

jest.mock('../../../../redux/domainMenu', () => ({
    useDomainProduct: () => ({}),
    useDomainProducts: () => [],
}));

jest.mock('../../../../redux/hooks', () => ({
    useBag: () => ({
        actions: {
            updateTooltip: () => ({}),
        },
    }),
    useGlobalProps: () => ({ alertBanners: { total: 0 } }),
    useOrderLocation: () => ({ method: OrderLocationMethod.NOT_SELECTED }),
    useDismissedAlertBanners: () => ({ bannerEntries: [] }),
    useSelectedSell: () => ({ correlationId: '1' }),
    useLoyalty: jest.fn().mockReturnValue({
        loyalty: { pointsBalance: 0 },
    }),
    useConfiguration: () => ({
        configuration: {
            isOAEnabled: true,
        },
    }),
}));

let accountFlag = true;

jest.mock('../../../../redux/hooks/useFeatureFlags', () => ({
    useFeatureFlags: () => ({
        featureFlags: {
            account: accountFlag,
        },
    }),
}));

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('../../../../redux/hooks/useAccount', () =>
    jest.fn().mockReturnValue({
        accountInfo: {},
    })
);

jest.mock('../../../../redux/hooks/useRewards', () =>
    jest.fn().mockReturnValue({
        offers: [],
        rewardsActivityHistory: [],
    })
);

jest.mock('../../../../common/hooks/useConfiguration', () => {
    const configurationMock = require('../../../mocks/configuration.mock');
    return {
        useConfiguration: jest.fn().mockReturnValue({
            configuration: configurationMock,
        }),
    };
});

jest.mock('../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

jest.mock('../../../../redux/hooks/useOrderLocation', () =>
    jest.fn().mockReturnValue({
        pickupAddress: {},
        deliveryAddress: {},
        currentLocation: {
            location: {},
        },
        method: OrderLocationMethod.NOT_SELECTED,
        actions: {
            setPickupLocation: jest.fn(),
            setAvailableTimeSlots: jest.fn(),
        },
    })
);

jest.mock('../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmLocationOrder: jest.fn(),
        pushGtmChangeLocation: jest.fn(),
    }),
}));

describe('navigation component', () => {
    beforeEach(() => {
        mockedUseAuth0.mockReturnValue({
            isAuthenticated: false,
            isLoading: false,
        } as any);
        (useRouter as jest.Mock).mockReturnValue({
            asPath: 'http://',
            pathname: 'http://localhost:3000',
        });
        (useDismissedAlertBanners as jest.Mock).mockReturnValue({
            bannerEntries: [],
        });
        (useBagAvailableItemsCounter as jest.Mock).mockReturnValue(1);
    });

    it('should match snapshot', () => {
        const wrapper = shallow(
            <Navigation navigation={MockData as any} userAccountMenu={MockUserAccountMenuData as any} />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should contain SignIn component when Account flag = true', () => {
        render(<Navigation navigation={MockData as any} userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.getByText('Sign In')).toBeInTheDocument();
    });

    it('should  not contain SignIn component when Account flag = false', () => {
        accountFlag = false;

        render(<Navigation navigation={MockData as any} userAccountMenu={MockUserAccountMenuData as any} />);
        expect(screen.queryByText('Sign In')).not.toBeInTheDocument();
    });

    it('should show Start An Order button when no location is selected', async () => {
        render(<Navigation navigation={MockData as any} userAccountMenu={MockUserAccountMenuData as any} />);
        await waitFor(() => {
            expect(screen.getAllByText(/Start an Order/i).length).toEqual(2);
        });
    });
});

describe('navigation link component', () => {
    it('should match snapshot with relative link', () => {
        (useRouter as jest.Mock).mockReturnValue({
            asPath: 'path/path-name2/path-name3/path-name4/',
            pathname: 'path/[path-name2]/[path-name3]/[path-name4]',
        });
        const link = getLinkMock();
        const { container } = render(<NavigationLink key={link.sys.id} link={link as any} />);

        expect(container).toMatchSnapshot();
    });
    it('should match snapshot with non relative link', () => {
        (useRouter as jest.Mock).mockReturnValue({
            asPath: 'path/path-name2/path-name3/path-name4/',
            pathname: 'path/[path-name2]/[path-name3]/[path-name4]',
        });
        const link = getLinkMock(false);
        const wrapper = shallow(<NavigationLink key={link.sys.id} link={link as any} />);

        expect(wrapper).toMatchSnapshot();
    });
});

const getLinkMock = (relativeLink = true) => {
    return {
        sys: {
            id: '1SSMLPOwxPLSWjXPhXtinC',
            type: 'Entry',
            createdAt: '2020-07-01T14:45:19.278Z',
            updatedAt: '2021-03-31T12:30:47.163Z',
            contentType: {
                sys: {
                    type: 'Link',
                    linkType: 'ContentType',
                    id: 'page',
                },
            },
            locale: 'en-US',
        },
        fields: {
            name: 'LOCATIONS',
            nameInUrl: relativeLink ? 'locations' : 'https://www.arbys.com/locations',
            metaDescription:
                "Find an Arby's location near you, and start an order for pickup. Online ordering available at participating locations. ",
            metaTitle: "Find Your Local Arby's Restaurant",
        },
    };
};
