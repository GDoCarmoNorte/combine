import React from 'react';
import { render } from '@testing-library/react';

import NavigationDropdown from '../../../../../components/organisms/navigation/navigationDropdown/navigationDropdown';

jest.mock('@material-ui/core/useMediaQuery', () => jest.fn(() => false).mockImplementationOnce(() => true));

const currentMock = {
    current: {
        clientWidth: 100,
        clientHeight: 100,
        getBoundingClientRect: () => {
            return { left: 800, top: 100 };
        },
    },
};

jest.spyOn(React, 'useRef').mockReturnValue(currentMock);

describe('navigationDropdown component', () => {
    const onClose = jest.fn();
    const props = {
        onClose,
        open: true,
        className: '',
        controlRef: currentMock,
        navRef: currentMock,
    };

    it('should match snapshot', () => {
        const { container } = render(<NavigationDropdown {...(props as any)} />);

        expect({ container }).toMatchSnapshot();
    });

    it('should match snapshot with children', () => {
        const { container } = render(<NavigationDropdown {...(props as any)}>children</NavigationDropdown>);

        expect({ container }).toMatchSnapshot();
    });
});
