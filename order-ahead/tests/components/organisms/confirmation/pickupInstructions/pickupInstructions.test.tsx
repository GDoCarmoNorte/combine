import React from 'react';
import { shallow } from 'enzyme';

import { IPickupInstructionsItem } from '../../../../../@generated/@types/contentful';
import PickupInstructions from '../../../../../components/organisms/confirmation/pickupInstructions/pickupInstructions';
import pickupMock from './pickupInstructions.mock';

describe('PickupInstructions', () => {
    it('should render PickupInstructions with props', () => {
        const wrapper = shallow(<PickupInstructions pickupInstructions={pickupMock.fields.instractions} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render PickupInstructions without props', () => {
        const wrapper = shallow(<PickupInstructions pickupInstructions={undefined as IPickupInstructionsItem[]} />);

        expect(wrapper).toMatchSnapshot();
    });
});
