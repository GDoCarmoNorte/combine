import { IPickupInstructions } from '../../../../../@generated/@types/contentful';

export default ({
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '20zOcr60XTjUi50rfnCQlc',
        type: 'Entry',
        createdAt: '2021-10-26T09:51:11.813Z',
        updatedAt: '2021-10-26T09:51:11.813Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'pickupInstructions',
            },
        },
        locale: 'en-US',
    },
    fields: {
        instractions: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '5B2dn1gNCbxXtsmMxuY2xf',
                    type: 'Entry',
                    createdAt: '2021-10-26T09:50:40.304Z',
                    updatedAt: '2021-10-26T09:50:40.304Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'pickupInstructionsItem',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    instruction: 'Wear a mask when picking up order',
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6lAU6xq6M5E0wHLDsYuWbv',
                    type: 'Entry',
                    createdAt: '2021-10-26T09:51:07.412Z',
                    updatedAt: '2021-10-26T09:51:07.412Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'pickupInstructionsItem',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    instruction: 'tell the cashier your name',
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: 'P0QHOzqnnF4DJBGqglQMD',
                    type: 'Entry',
                    createdAt: '2021-10-26T09:50:53.977Z',
                    updatedAt: '2021-10-26T09:50:53.977Z',
                    environment: {
                        sys: {
                            id: 'dev',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'pickupInstructionsItem',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    instruction: 'Proceed to the online order lane',
                },
            },
        ],
    },
} as unknown) as IPickupInstructions;
