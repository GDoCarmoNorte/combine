import React from 'react';
import { render, screen } from '@testing-library/react';
import CaloriesLegalSection from '../../../../components/organisms/caloriesLegalSection/caloriesLegelSection';
import { caloriesLegal } from './caloriesLegal.mock';
describe('caloriesLegalComponentTest', () => {
    it('should load and show colariesLegalSection', () => {
        render(
            <div>
                <CaloriesLegalSection prop={caloriesLegal}></CaloriesLegalSection>
            </div>
        );
        expect(
            screen.getByText(/2,000 calories a day is used for general nutrition advice, but calorie needs vary./i)
        ).toBeInTheDocument();
    });
});
