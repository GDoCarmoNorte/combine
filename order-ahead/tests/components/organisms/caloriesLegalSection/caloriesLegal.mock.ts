export const caloriesLegal = {
    metadata: { tags: [] },
    sys: {
        space: { sys: { type: 'Link', linkType: 'Space', id: 'o19mhvm9a2cm' } },
        id: '2IpKfjWIgf7XfQyl5qsZAn',
        type: 'Entry',
        createdAt: '2022-01-26T18:53:54.623Z',
        updatedAt: '2022-01-26T18:53:54.623Z',
        environment: { sys: { id: 'feature-feature-DBBP-62381', type: 'Link', linkType: 'Environment' } },
        revision: 1,
        contentType: { sys: { type: 'Link', linkType: 'ContentType', id: 'caloriesLegal' } },
        locale: 'en-US',
    },
    fields: {
        message: {
            nodeType: 'document',
            data: {},
            content: [
                {
                    nodeType: 'paragraph',
                    content: [
                        {
                            nodeType: 'text',
                            value:
                                '2,000 calories a day is used for general nutrition advice, but calorie needs vary.  Additional nutrition information available upon request.',
                            marks: [{ type: 'italic' }],
                            data: {},
                        },
                    ],
                    data: {},
                },
                {
                    nodeType: 'paragraph',
                    content: [
                        {
                            nodeType: 'text',
                            value:
                                'Before placing your order, please inform your server or restaurant if a person in your party has a food allergy.',
                            marks: [{ type: 'italic' }],
                            data: {},
                        },
                    ],
                    data: {},
                },
                {
                    nodeType: 'paragraph',
                    content: [
                        {
                            nodeType: 'text',
                            value: 'Some food products prepared with beef shortening. For more information, view the ',
                            marks: [{ type: 'italic' }],
                            data: {},
                        },
                        {
                            nodeType: 'hyperlink',
                            content: [
                                {
                                    nodeType: 'text',
                                    value: 'Nutrition, Allergen & Preparation Guide',
                                    marks: [{ type: 'italic' }],
                                    data: {},
                                },
                            ],
                            data: {
                                uri: 'https://cds.arbys.com/pdfs/nutrition/Arbys_Nutritional__Allergen_Info_OCT.pdf',
                            },
                        },
                        { nodeType: 'text', value: '.', marks: [{ type: 'italic' }], data: {} },
                    ],
                    data: {},
                },
            ],
        },
    },
};
