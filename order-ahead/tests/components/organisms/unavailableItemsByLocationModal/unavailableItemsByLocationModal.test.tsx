import React from 'react';
import { render, screen } from '../../../utils';
import userEvent from '@testing-library/user-event';
import { UnavailableItemsByLocationModal } from '../../../../components/organisms/unavailableItemsByLocationModal/unavailableItemsByLocationModal';
import { useBagRenderItems } from '../../../../common/hooks/useBagRenderItems';
import productMock from '../../../mocks/contentfulProduct.mock';
import useOrderLocation from '../../../../redux/hooks/useOrderLocation';
import { useMediaQuery } from '@material-ui/core';
import { OrderLocationMethod } from '../../../../redux/orderLocation';
import useDomainMenu from '../../../../redux/hooks/useDomainMenu';

jest.mock('../../../../redux/hooks/useDomainMenu');
jest.mock('../../../../common/hooks/useBagRenderItems');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('@material-ui/core/useMediaQuery');

describe('unavailable items by location modal', () => {
    const getDomainMenuMock = jest.fn();
    const setPreviousLocationMock = jest.fn();
    const setPickupLocationMock = jest.fn();
    const setDeliveryLocationMock = jest.fn();

    beforeEach(() => {
        (useDomainMenu as jest.Mock).mockReturnValue({
            loading: false,
            actions: { getDomainMenu: getDomainMenuMock },
        });

        (useBagRenderItems as jest.Mock).mockReturnValue([
            {
                contentfulProduct: productMock,
                discountBanner: undefined,
                entry: {
                    category: 'top-picks',
                    childItems: undefined,
                    lineItemId: 1,
                    name: 'Beef n Cheddar',
                    price: 6.59,
                    productId: 'arb-itm-000-021',
                    quantity: 1,
                },
                entryPath: {
                    as: '/menu/top-picks/classic-beef-n-cheddar-meal',
                    href: '/menu/[menuCategoryUrl]/[productDetailsPageUrl]',
                },
                isAvailable: false,
                markedAsRemoved: false,
            },
        ]);

        (useOrderLocation as jest.Mock).mockReturnValue({
            isCurrentLocationOAAvailable: true,
            previousLocation: {},
            actions: {
                setPreviousLocation: setPreviousLocationMock,
                setPickupLocation: setPickupLocationMock,
                setDeliveryLocation: setDeliveryLocationMock,
            },
        });

        (useMediaQuery as jest.Mock).mockReturnValue(true);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    const getComponent = () => <UnavailableItemsByLocationModal />;
    it('should render correctly in desktop view', () => {
        render(getComponent());

        expect(screen.getByRole(/presentation/i)).toBeInTheDocument();
        expect(screen.getByText(/Items unavailable in your location/i)).toBeInTheDocument();
        expect(screen.getByText(/Some items aren’t available at this new location./i)).toBeInTheDocument();
        expect(screen.getByText(/Would you like to proceed to new location?/i)).toBeInTheDocument();
        expect(screen.getByText(/Yes, proceed to new location/i)).toBeInTheDocument();
        expect(screen.getByText(/No, keep current location/i)).toBeInTheDocument();
    });

    it('should render correctly in mobile view', () => {
        (useMediaQuery as jest.Mock).mockReturnValue(false);
        render(getComponent());

        expect(screen.getByRole(/presentation/i)).toBeInTheDocument();
        expect(screen.getByText(/Yes, select new/i)).toBeInTheDocument();
        expect(screen.getByText(/No, keep current/i)).toBeInTheDocument();
    });

    it('should not render modal window', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isCurrentLocationOAAvailable: false,
            previousLocation: null,
            actions: {
                setPreviousLocation: setPreviousLocationMock,
                setPickupLocation: setPickupLocationMock,
                setDeliveryLocation: setDeliveryLocationMock,
            },
        });
        render(getComponent());

        expect(screen.queryByRole(/presentation/i)).not.toBeInTheDocument();
    });

    it('should call setPreviousLocation with null as close the modal', () => {
        render(getComponent());
        const crossBtn = screen.getByRole('button', { name: /Close Icon/i });
        userEvent.click(crossBtn);
        expect(setPreviousLocationMock).toHaveBeenCalledWith(null);
    });

    it('should call setPreviousLocation with null as click on the proceed to new location button', () => {
        render(getComponent());
        const button = screen.getByRole('button', { name: /Yes, proceed to new location/i });
        userEvent.click(button);
        expect(setPreviousLocationMock).toHaveBeenCalledWith(null);
    });

    it('should call onKeepCurrentLocation for not a delivery previous location', () => {
        render(getComponent());
        const button = screen.getByRole('button', { name: /No, keep current location/i });
        userEvent.click(button);
        expect(setPickupLocationMock).toHaveBeenCalledWith({});
        expect(getDomainMenuMock).toHaveBeenCalledWith({});
        expect(setPreviousLocationMock).toHaveBeenCalledWith(null);
    });

    it('should call onKeepCurrentLocation for not a pickup previous location', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isCurrentLocationOAAvailable: true,
            previousLocation: { deliveryLocation: {}, locationDetails: {} },
            actions: {
                setPreviousLocation: setPreviousLocationMock,
                setPickupLocation: setPickupLocationMock,
                setDeliveryLocation: setDeliveryLocationMock,
            },
        });
        render(getComponent());
        const button = screen.getByRole('button', { name: /No, keep current location/i });
        userEvent.click(button);
        expect(setDeliveryLocationMock).toHaveBeenCalledWith({ deliveryLocation: {}, locationDetails: {} });
        expect(getDomainMenuMock).toHaveBeenCalledWith({}, OrderLocationMethod.DELIVERY);
        expect(setPreviousLocationMock).toHaveBeenCalledWith(null);
    });
});
