import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import AddEditFormWithFreedomPay from '../../../../components/organisms/addEditCard/components/addEditFormWithFreedomPay';
import { FreedomPayDto, useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { mount } from 'enzyme';
import { IPaymentInitResponseModel } from '../../../../@generated/webExpApi';

jest.mock('../../../../common/hooks/useFreedomPay');

describe('AddEditFormWithFreedomPay', () => {
    const mockAddCard = jest.fn();
    const mockCloseOverlay = jest.fn();
    const wallet = { sessionKey: 'sessionKey', iframeHtml: '' } as IPaymentInitResponseModel;

    beforeEach(() => {
        const paymentToken = {
            cardIssuer: 'Visa',
            maskedCardNumber: '411111XXXX111111',
            keys: ['212312312'],
        } as FreedomPayDto;
        (useFreedomPay as jest.Mock).mockReturnValue({
            height: 300,
            errors: [],
            isValid: false,
            payment: paymentToken,
        });
    });

    const renderComponent = () => {
        render(
            <AddEditFormWithFreedomPay
                innerHtml={null}
                wallet={wallet}
                addCard={mockAddCard}
                closeOverlay={mockCloseOverlay}
                isLoading={false}
            />
        );
    };

    it('should render component', () => {
        renderComponent();
        expect(screen.getByTestId('addCardContainer')).toBeInTheDocument();
    });

    it('should close form on close icon click', () => {
        renderComponent();
        const closeIcon = screen.getByLabelText(/Close/i);
        fireEvent.click(closeIcon);
        expect(mockCloseOverlay).toHaveBeenCalled();
    });

    it('should add card once the paymentToken is received', () => {
        const wallet = { sessionKey: 'sessionKey', iframeHtml: '' } as IPaymentInitResponseModel;
        mount(
            <AddEditFormWithFreedomPay
                innerHtml={{ __html: 'iframePayload' }}
                wallet={wallet}
                addCard={mockAddCard}
                closeOverlay={mockCloseOverlay}
                isLoading={false}
            />
        );
        expect(mockAddCard).toHaveBeenCalled();
    });
});
