import React from 'react';
import { render, screen } from '@testing-library/react';
import PaymentProcessing from '../../../../../../components/organisms/checkout/paymentProcessing/paymentProcessing';

describe('Payment Processing screen', () => {
    it('should show header, description and loader', () => {
        render(<PaymentProcessing />);

        expect(screen.getByRole('progressbar')).toBeInTheDocument();
        expect(screen.getByText(/payment processing.../i)).toBeInTheDocument();
        expect(screen.getByText(/Please do not close this tab or refresh your browser./i)).toBeInTheDocument();
    });
    it('should hide header if requested', () => {
        render(<PaymentProcessing hideHeader={true} />);

        expect(screen.queryByText(/payment processing.../i)).not.toBeInTheDocument();
        expect(screen.getByRole('progressbar')).toBeInTheDocument();
        expect(screen.getByText(/Please do not close this tab or refresh your browser./i)).toBeInTheDocument();
    });
});
