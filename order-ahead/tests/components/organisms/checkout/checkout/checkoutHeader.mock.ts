export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: 'fNwu48rE9Gg2iwZ6vEG7P',
        type: 'Entry',
        createdAt: '2021-06-02T19:08:52.551Z',
        updatedAt: '2021-06-02T19:08:52.551Z',
        environment: {
            sys: {
                id: 'feature-DBBP-29422',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'checkoutHeader',
            },
        },
        locale: 'en-US',
    },
    fields: {
        backgroundColor: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '7FY6Ptl8PSAmBoIvyCa6Bn',
                type: 'Entry',
                createdAt: '2020-08-09T21:38:50.910Z',
                updatedAt: '2020-08-09T21:38:50.910Z',
                environment: {
                    sys: {
                        id: 'feature-DBBP-29422',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'color',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'arbysred',
                hexColor: 'D71920',
            },
        },
        backgroundImage: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '7wo8ktaSCPHphEE7iR7Y1x',
                type: 'Asset',
                createdAt: '2021-06-02T19:08:47.698Z',
                updatedAt: '2021-06-02T19:08:47.698Z',
                environment: {
                    sys: {
                        id: 'feature-DBBP-29422',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: "Arby's Checkout Background Image",
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/7wo8ktaSCPHphEE7iR7Y1x/e7b87e6ec82f51f987aa2ec64e5e456e/Arby_Checkout_Header.jpeg',
                    details: {
                        size: 194507,
                        image: {
                            width: 1440,
                            height: 530,
                        },
                    },
                    fileName: 'Arby_Checkout_Header.jpeg',
                    contentType: 'image/jpeg',
                },
            },
        },
    },
};
