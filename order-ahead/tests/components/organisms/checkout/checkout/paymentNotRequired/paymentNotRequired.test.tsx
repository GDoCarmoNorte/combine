import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import PaymentNotRequired from '../../../../../../components/organisms/checkout/paymentNotRequired/paymentNotRequired';
import { TInitialPaymentTypes } from '../../../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

jest.mock('../../../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));

describe('PaymentNotRequired component', () => {
    const mockOnSubmit = jest.fn();
    const mockSetPaymentInfo = jest.fn();

    it('should render for pickup', () => {
        const { container } = render(
            <PaymentNotRequired
                onSubmit={mockOnSubmit}
                isActive={true}
                setPaymentInfo={mockSetPaymentInfo}
                isPickUp={true}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render for delivery', () => {
        const { container } = render(
            <PaymentNotRequired
                onSubmit={mockOnSubmit}
                isActive={true}
                setPaymentInfo={mockSetPaymentInfo}
                isPickUp={false}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should not submit order if button is not active', () => {
        render(
            <PaymentNotRequired
                onSubmit={mockOnSubmit}
                isActive={false}
                setPaymentInfo={mockSetPaymentInfo}
                isPickUp={true}
            />
        );
        const button = screen.getByRole('button', { name: 'SUBMIT' });
        fireEvent.click(button);
        expect(mockOnSubmit).not.toBeCalled();
    });

    it('should set payment info to not required on component mount', () => {
        render(
            <PaymentNotRequired
                onSubmit={mockOnSubmit}
                isActive={false}
                setPaymentInfo={mockSetPaymentInfo}
                isPickUp={true}
            />
        );
        expect(mockSetPaymentInfo).toHaveBeenCalledWith({
            type: TInitialPaymentTypes.NO_PAYMENT,
        });
    });
});
