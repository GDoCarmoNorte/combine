import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TipsBlock from '../../../../../../components/organisms/checkout/tipsBlock/tipsBlock';
import { useTips } from '../../../../../../common/hooks/useTips';
import { statusApplyingTips } from '../../../../../../components/organisms/contactlessPay/contactlessPay';
import { useAppDispatch } from '../../../../../../redux/store';
import { GTM_CHECKOUT_TIP_SELECTION_EVENT } from '../../../../../../common/services/gtmService/constants';

jest.mock('../../../../../../common/hooks/useTips');
jest.mock('../../../../../../redux/hooks', () => ({
    useOrderLocation: () => ({ isPickUp: true }),
}));

jest.mock('../../../../../../redux/store');

const dispatch = jest.fn();

const tipsMock = {
    tipsItems: [
        { amount: 1, percentage: 0 },
        { amount: 2, percentage: 0 },
        { amount: 3, percentage: 0 },
    ],
    titleText: 'Add tip:',
    isError: false,
    errorMessage: '',
    defaultTipsId: 1,
};

const tipsMockWithError = { ...tipsMock, isError: true, errorMessage: 'error message' };

describe('Custom Tips Button', () => {
    (useTips as jest.Mock).mockReturnValue(tipsMock);
    (useAppDispatch as jest.Mock).mockReturnValue(dispatch);

    const getComponent = ({
        subtotal = 0,
        setTipsAmount = jest.fn(),
        tipsAmount = 2,
        isContactless = false,
        setTipsPercentage = jest.fn(),
        showWarningModal = false,
        setShowWarningModal = jest.fn(),
        applyTips = statusApplyingTips.initial,
        setCurrentTipsAmount = jest.fn(),
    }) => (
        <TipsBlock
            subtotal={subtotal}
            setTipsAmount={setTipsAmount}
            tipsAmount={tipsAmount}
            isContactless={isContactless}
            setTipsPercentage={setTipsPercentage}
            showWarningModal={showWarningModal}
            setShowWarningModal={setShowWarningModal}
            applyTips={applyTips}
            setCurrentTipsAmount={setCurrentTipsAmount}
        />
    );

    it('should renders correctly', () => {
        render(getComponent({}));

        const customButton = screen.getByText(/custom/i);
        const minTipsButton = screen.getByRole('button', { name: /\$1/i });
        const title = screen.getByText(/Add tip:/i);
        expect(customButton).toBeInTheDocument();
        expect(minTipsButton).toBeInTheDocument();
        expect(title).toBeInTheDocument();
    });

    it('should renders correctly with percentages', () => {
        (useTips as jest.Mock).mockReturnValue({
            tipsItems: [
                { amount: 1, percentage: 10 },
                { amount: 2, percentage: 15 },
                { amount: 3, percentage: 20 },
            ],
            titleText: 'Add tip:',
            isError: false,
            errorMessage: '',
            defaultTipsId: 1,
        });
        render(getComponent({}));

        const minTipsButton = screen.getByRole('button', { name: /10%/i });
        const averageTipsButton = screen.getByRole('button', { name: /15%/i });
        const maxTipsButton = screen.getByRole('button', { name: /20%/i });

        expect(minTipsButton).toBeInTheDocument();
        expect(averageTipsButton).toBeInTheDocument();
        expect(maxTipsButton).toBeInTheDocument();
    });

    it('should change custom tips and set a custom value without errors', () => {
        render(getComponent({}));

        const customButton = screen.getByRole('button', { name: /custom/i });
        userEvent.click(customButton);

        const input = screen.getByPlaceholderText(/\$/i);
        expect(input).toBeInTheDocument();

        fireEvent.change(input, { target: { value: '$23.0' } });
        expect(input).toHaveValue('$23.0');

        userEvent.type(input, '{enter}');
        expect(screen.getByText(/custom/i)).toBeInTheDocument();
        expect(screen.getByText(/\$23/i)).toBeInTheDocument();
    });

    it('should activate custom tips button and do not set a custom value', () => {
        render(getComponent({}));

        const customButton = screen.getByRole('button', { name: /custom/i });
        const minTipsButton = screen.getByRole('button', { name: /\$1/i });
        userEvent.click(customButton);

        const input = screen.getByPlaceholderText(/\$/i);
        userEvent.click(minTipsButton);
        expect(screen.getByText(/custom/i)).toBeInTheDocument();
        expect(input).not.toBeInTheDocument();
    });

    it('should activate custom tips button when there is an error', () => {
        (useTips as jest.Mock).mockReturnValue(tipsMockWithError);
        render(getComponent({}));

        const customButton = screen.getByText(/custom/i);
        const error = screen.getByText(/error message/i);
        userEvent.click(customButton);

        const input = screen.getByPlaceholderText(/\$/i);
        fireEvent.change(input, { target: { value: '$23.0' } });
        userEvent.type(input, '{enter}');
        expect(screen.getByText(/custom/i)).toBeInTheDocument();
        expect(input).not.toBeInTheDocument();
        expect(error).toBeInTheDocument();
    });

    it('should call setTipsAmount function with defaultTipsId', () => {
        (useTips as jest.Mock).mockReturnValue(tipsMock);
        const mockSetTipsAmount = jest.fn();
        render(getComponent({ tipsAmount: 10, setTipsAmount: mockSetTipsAmount }));

        const customButton = screen.getByRole('button', { name: /custom/i });
        userEvent.click(customButton);

        const input = screen.getByPlaceholderText(/\$/i);
        userEvent.type(input, '{enter}');

        expect(input).not.toBeInTheDocument();
        expect(mockSetTipsAmount).toBeCalledTimes(2);
        expect(mockSetTipsAmount).toBeCalledWith(2);
        expect(dispatch).toHaveBeenCalledWith({
            type: GTM_CHECKOUT_TIP_SELECTION_EVENT,
            payload: {
                tipAmount: 2,
            },
        });
    });

    it('should render tipsBlock the same if Contactlesspay is true', () => {
        render(getComponent({ isContactless: true }));

        const customButton = screen.getByText(/custom/i);
        const minTipsButton = screen.getByRole('button', { name: /\$1/i });
        const title = screen.getByText(/Add tip:/i);
        expect(customButton).toBeInTheDocument();
        expect(minTipsButton).toBeInTheDocument();
        expect(title).toBeInTheDocument();
    });

    it('should render input and should not render error message if an error exists and Contactlesspay is true', () => {
        (useTips as jest.Mock).mockReturnValue(tipsMockWithError);

        render(getComponent({ isContactless: true }));

        const customButton = screen.getByText(/custom/i);
        const error = screen.queryByText(/error message/i);
        userEvent.click(customButton);

        const input = screen.getByPlaceholderText(/\$/i);
        fireEvent.change(input, { target: { value: '$23.0' } });

        expect(screen.getByText(/custom/i)).toBeInTheDocument();
        expect(input).toBeInTheDocument();
        expect(error).not.toBeInTheDocument();
    });
});
