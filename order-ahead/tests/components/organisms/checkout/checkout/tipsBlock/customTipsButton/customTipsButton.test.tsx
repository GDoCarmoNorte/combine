import React from 'react';
import { render, screen } from '@testing-library/react';
import CustomTipsButton from '../../../../../../../components/organisms/checkout/tipsBlock/customTipsButton/customTipsButton';
import { CustomTipsStatus } from '../../../../../../../components/organisms/checkout/tipsBlock/tipsBlock';
import userEvent from '@testing-library/user-event';

describe('Custom Tips Button', () => {
    const getComponent = ({
        onKeyUp = jest.fn(),
        onChange = jest.fn(),
        onClick = jest.fn(),
        onBlur = jest.fn(),
        status = CustomTipsStatus.inactive,
        value = '',
        isPickUp = true,
    }) => (
        <CustomTipsButton
            onKeyUp={onKeyUp}
            onChange={onChange}
            onClick={onClick}
            onBlur={onBlur}
            status={status}
            value={value}
            isPickUp={isPickUp}
        />
    );
    it('should render with inactive state', () => {
        const handleClickMock = jest.fn();
        render(getComponent({ onClick: handleClickMock }));

        const button = screen.getByRole('button', { name: /custom/i });
        userEvent.click(button);
        expect(handleClickMock).toHaveBeenCalled();
    });

    it('should render with active state', () => {
        const handleKeyUpMock = jest.fn();
        render(getComponent({ status: CustomTipsStatus.active, onKeyUp: handleKeyUpMock }));

        const placeholder = screen.getByPlaceholderText(/\$/i);
        expect(placeholder).toBeInTheDocument();

        userEvent.type(placeholder, '');
        expect(handleKeyUpMock).not.toBeCalled();

        userEvent.type(placeholder, '{enter}');
        expect(handleKeyUpMock).toHaveBeenCalled();
    });

    it('should render with success state', () => {
        render(getComponent({ status: CustomTipsStatus.success, value: '10' }));

        const title = screen.getByText(/custom/i);
        expect(title).toBeInTheDocument();
        const amount = screen.getByText(/\$10/i);
        expect(amount).toBeInTheDocument();
    });
});
