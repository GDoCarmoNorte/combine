import React from 'react';
import { shallow } from 'enzyme';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import Checkout from '../../../../../components/organisms/checkout/checkout';
import { IGlobalContentfulProps } from '../../../../../common/services/globalContentfulProps';
import { IPaymentRequest } from '../../../../../components/clientOnly/paymentInfoContainer/paymentInfo';
import getLocationById from '../../../../../common/services/locationService/getLocationById';
import { getLocationMenu } from '../../../../../common/services/domainMenu';
import useAccount from '../../../../../redux/hooks/useAccount';
import useBag from '../../../../../redux/hooks/useBag';
import useSubmitOrder from '../../../../../redux/hooks/useSubmitOrder';
import useTallyOrder from '../../../../../redux/hooks/useTallyOrder';
import useDomainMenu from '../../../../../redux/hooks/useDomainMenu';
import useOrderLocation from '../../../../../redux/hooks/useOrderLocation';
import useSelectedSell from '../../../../../redux/hooks/useSelectedSell';
import checkoutHeaderMock from './checkoutHeader.mock';
import { useAppSelector } from '../../../../../redux/store';
import { useNotValidBagProductsByTime } from '../../../../../common/hooks/useNotValidBagProductsByTime';
import { actions as submitOrderActions } from '../../../../../redux/submitOrder';
import {
    OrderResponseModel,
    BrandIdEnumModel,
    TallyFulfillmentTypeModel,
    OrderSubmitRequestStatusModel,
    TallyProductModel,
} from '../../../../../@generated/webExpApi';
import logger from '../../../../../common/services/logger';
import { useConfiguration } from '../../../../../common/hooks/useConfiguration';
import { useSignUp } from '../../../../../common/hooks/useSignUp';

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

jest.mock('../../../../../common/hooks/useConfiguration', () => ({
    useConfiguration: jest.fn().mockReturnValue({
        configuration: {},
    }),
}));

jest.mock('../../../../../lib/getFeatureFlags');
jest.mock('../../../../../common/services/locationService/getLocationById');
jest.mock('../../../../../common/services/domainMenu');
jest.mock('../../../../../lib/domainProduct', () => ({
    __esModule: true,
    getProductItemsFromMenu: jest.fn(),
}));
jest.mock('../../../../../redux/hooks/useAccount');
jest.mock('../../../../../redux/hooks/useBag');
jest.mock('../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../redux/hooks/useSubmitOrder');
jest.mock('../../../../../redux/hooks/useTallyOrder');
jest.mock('../../../../../redux/hooks/useDomainMenu');
jest.mock('../../../../../redux/hooks/useSelectedSell');
jest.mock('../../../../../common/hooks/useSignUp');
jest.mock('../../../../../common/hooks/useNotValidBagProductsByTime');
jest.mock('../../../../../common/services/logger');
jest.mock('react-redux');
jest.mock('../../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

describe('Checkout component', () => {
    const globalProps = {
        footer: {},
        alertBanners: { items: [] },
        navigation: {},
        modifierItemsById: {},
    } as IGlobalContentfulProps;

    const props = {
        tallyLoading: false,
        handleOrderTimeChange: jest.fn(),
        onBagChange: jest.fn(),
        sections: {
            pickupInstructions: ['test', 'test', 'test'],
        },
        checkoutLegal: {},
    };
    const bagElements: TallyProductModel[] = [
        { productId: '1', price: 10.1, quantity: 5, lineItemId: 1 },
        { productId: '2', price: 11.11, quantity: 6, lineItemId: 2 },
        { productId: '3', price: 12.12, quantity: 1, lineItemId: 3 },
    ];
    const tallyOrderObject = {
        subTotalAfterDiscounts: -3.0,
        total: 33.33,
        products: [
            {
                description: 'Product name one',
                price: 10.1,
            },
            {
                description: 'Product name two',
                price: 11.11,
            },
            {
                description: 'Product name three',
                price: 12.12,
            },
        ],
        fulfillment: {
            type: 'PickUp',
        },
    };

    const checkoutLegalMock = {
        deliveryFeeLegalMessage: 'deliveryFeeLegalMessage',
        serviceFeeLegalMessage: 'serviceFeeLegalMessage',
        takeoutServiceFeeLegalMessage: 'takeoutServiceFeeLegalMessage',
    };

    const dispatchSpy = jest.fn();
    const removeAllFromBagSpy = jest.fn();
    const pushSpy = jest.fn();
    const replaceSpy = jest.fn(async () => '/');
    const setCorrelationIdSpy = jest.fn();
    const mockSignUp = jest.fn();

    beforeEach(() => {
        (useNotValidBagProductsByTime as jest.Mock).mockReturnValue([]);
        (getLocationById as jest.Mock).mockReturnValue({ storeId: 'storeId' });
        (getLocationMenu as jest.Mock).mockReturnValue({ products: [] });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            currentLocation: { isTippingEnabled: false },
            isCurrentLocationOAAvailable: true,
        });
        (useAccount as jest.Mock).mockReturnValue({ account: {} });
        (useSelectedSell as jest.Mock).mockReturnValue({
            actions: {
                setCorrelationId: setCorrelationIdSpy,
            },
        });
        (useAppSelector as jest.Mock).mockReturnValue([
            { product: { id: '1' }, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: { id: '2' }, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: { id: '3' }, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
        ]);
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: bagElements,
            actions: {
                removeAllFromBag: removeAllFromBagSpy,
                toggleIsOpen: jest.fn(),
                removeFromBag: jest.fn(),
                clearLastRemovedLineItemId: jest.fn(),
                clearLastEdit: jest.fn(),
            },
            entriesMarkedAsRemoved: [],
            orderTimeType: 'asap',
            orderTime: '2020-12-17T22:15:00.000Z',
        });
        (useSubmitOrder as jest.Mock).mockReturnValue({
            error: null,
            isLoading: false,
            lastOrder: null,
            submitOrder: jest.fn(),
            resetSubmitOrder: jest.fn(),
            unavailableItems: null,
            isShowAlertModal: false,
            hideAlertModal: jest.fn(),
        });
        (useTallyOrder as jest.Mock).mockReturnValue({
            tallyOrder: tallyOrderObject,
            isLoading: false,
            error: null,
        });
        (useDomainMenu as jest.Mock).mockReturnValue({
            actions: { setDomainMenu: () => jest.fn() },
        });
        (useSelector as jest.Mock).mockReturnValue({
            unavailableItems: null,
        });
        (useDispatch as jest.Mock).mockImplementation(() => dispatchSpy);
        (useRouter as jest.Mock).mockImplementation(
            () =>
                ({
                    replace: replaceSpy,
                    push: pushSpy,
                } as any)
        );
        (useSignUp as jest.Mock).mockReturnValue({
            signUpMarketingPreferences: mockSignUp,
        });
    });
    afterEach(() => jest.clearAllMocks());

    it('should match snapshot', () => {
        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );
        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot: ReviewOrder withTips=true', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            isCurrentLocationOAAvailable: true,
            currentLocation: {
                isTippingEnabled: true,
            },
        });
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isTippingEnabled: true,
            },
        });

        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render the checkout header with a background color', async () => {
        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );
        expect(wrapper.find('style')).toMatchSnapshot();
    });

    it('should redirect to home when location is not order ahead available', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            currentLocation: { isTippingEnabled: false },
            isCurrentLocationOAAvailable: false,
        });
        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );

        const customerInfo = wrapper.find('[data-testid="customer-info"]') as any;
        customerInfo.props().onChange(
            {
                firstName: 'string',
                lastName: 'string',
                phone: 'string',
                email: 'string',
            },
            true
        );

        const paymentRequest = {} as IPaymentRequest;
        const paymentInfo = wrapper.find('[data-testid="payment-info"]') as any;
        await paymentInfo.props().onSubmit(paymentRequest);

        // TODO: rework dynamic components testing
        expect(replaceSpy).toHaveBeenCalledWith('/');
    });

    it('should redirect to home when some products is not available', async () => {
        (useAppSelector as jest.Mock).mockReturnValue([
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: {}, availability: { isAvailable: false }, markedAsRemoved: false, isSaleable: true },
            { product: {}, availability: { isAvailable: false }, markedAsRemoved: false, isSaleable: true },
        ]);
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [{ productId: 1 }, { productId: 2 }, { productId: 3 }],
            actions: { toggleIsOpen: jest.fn() },
            entriesMarkedAsRemoved: [],
            orderTimeType: 'asap',
        });

        const replaceMock = jest.fn(async () => '/');
        (useRouter as jest.Mock).mockImplementation(
            () =>
                ({
                    replace: replaceMock,
                } as any)
        );

        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );
        const customerInfo = wrapper.find('[data-testid="customer-info"]') as any;
        customerInfo.props().onChange(
            {
                firstName: 'string',
                lastName: 'string',
                phone: 'string',
                email: 'string',
            },
            true
        );
        const paymentRequest = {} as IPaymentRequest;
        const paymentInfo = wrapper.find('[data-testid="payment-info"]') as any;
        await paymentInfo.props().onSubmit(paymentRequest);

        // TODO: rework dynamic components testing
        expect(replaceMock).toHaveBeenCalledWith('/');
    });

    it('should call submit when order is valid', async () => {
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [{ productId: '1' }, { productId: '2' }],
            actions: { toggleIsOpen: jest.fn() },
            entriesMarkedAsRemoved: [],
            orderTimeType: 'asap',
        });

        const submitOrderMock = jest.fn(async () => ({ type: 'type' }));

        (useSubmitOrder as jest.Mock).mockReturnValue({
            submitOrder: submitOrderMock,
            resetSubmitOrder: jest.fn(),
        });

        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );
        const customerInfo = wrapper.find('[data-testid="customer-info"]') as any;
        customerInfo.props().onChange(
            {
                firstName: 'string',
                lastName: 'string',
                phone: 'string',
                email: 'string',
            },
            true
        );
        const paymentRequest = {} as IPaymentRequest;
        const paymentInfo = wrapper.find('[data-testid="payment-info"]') as any;
        await paymentInfo.props().onSubmit(paymentRequest);

        expect(submitOrderMock).toHaveBeenCalled();
    });

    it('should not show paymentInfo when rewards certificate totals 0$ for pickup', async () => {
        (useAppSelector as jest.Mock).mockReturnValue([
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
        ]);
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [{ productId: '1' }, { productId: '2' }],
            actions: { toggleIsOpen: jest.fn() },
            entriesMarkedAsRemoved: [],
            orderTimeType: 'asap',
        });
        (useTallyOrder as jest.Mock).mockReturnValue({
            tallyOrder: {
                discountsAmount: 3.0,
                discounts: [
                    {
                        amount: 3.0,
                        omsOfferCode: '12312312',
                    },
                ],
                subTotalAfterDiscounts: -3.0,
                total: 0,
                products: [
                    {
                        description: 'Chicken Wrap',
                        price: 3.0,
                    },
                ],
                fulfillment: {
                    type: 'PickUp',
                },
            },
            isLoading: false,
            error: null,
        });

        const submitOrderMock = jest.fn(async () => ({
            type: 'type',
        }));

        (useSubmitOrder as jest.Mock).mockReturnValue({
            submitOrder: submitOrderMock,
            resetSubmitOrder: jest.fn(),
        });

        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );
        const customerInfo = wrapper.find('[data-testid="customer-info"]') as any;
        customerInfo.props().onChange(
            {
                firstName: 'string',
                lastName: 'string',
                phone: 'string',
                email: 'string',
            },
            true
        );
        const paymentRequest = {} as IPaymentRequest;
        const paymentNotRequired = wrapper.find('[data-testid="payment-not-required"]') as any;
        expect(paymentNotRequired).toBeTruthy();
        const paymentInfo = wrapper.find('[data-testid="payment-info"]') as any;
        expect(paymentInfo).toEqual({});
        await paymentNotRequired.props().onSubmit(paymentRequest);
        expect(submitOrderMock).toHaveBeenCalled();
    });

    it('should not show paymentInfo when rewards certificate totals 0$ for delivery', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            currentLocation: { isTippingEnabled: false },
            isCurrentLocationOAAvailable: true,
        });
        (useAppSelector as jest.Mock).mockReturnValue([
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
            { product: {}, availability: { isAvailable: true }, markedAsRemoved: false, isSaleable: true },
        ]);
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [{ productId: '1' }, { productId: '2' }],
            actions: { toggleIsOpen: jest.fn() },
            entriesMarkedAsRemoved: [],
            orderTimeType: 'asap',
        });
        (useTallyOrder as jest.Mock).mockReturnValue({
            tallyOrder: {
                discountsAmount: 3.0,
                discounts: [
                    {
                        amount: 3.0,
                        omsOfferCode: '12312312',
                    },
                ],
                subTotalAfterDiscounts: -3.0,
                total: 0,
                products: [
                    {
                        description: 'Chicken Wrap',
                        price: 3.0,
                    },
                ],
                fulfillment: {
                    type: 'Delivery',
                },
            },
            isLoading: false,
            error: null,
        });

        const submitOrderMock = jest.fn(async () => ({
            type: 'type',
        }));

        (useSubmitOrder as jest.Mock).mockReturnValue({
            submitOrder: submitOrderMock,
            resetSubmitOrder: jest.fn(),
        });

        const wrapper = shallow(
            <Checkout
                {...globalProps}
                {...props}
                checkoutHeader={checkoutHeaderMock as any}
                checkoutLegal={checkoutLegalMock as any}
            />
        );
        const customerInfo = wrapper.find('[data-testid="customer-info"]') as any;
        customerInfo.props().onChange(
            {
                firstName: 'string',
                lastName: 'string',
                phone: 'string',
                email: 'string',
            },
            true
        );
        const paymentRequest = {} as IPaymentRequest;
        const paymentNotRequired = wrapper.find('[data-testid="payment-not-required"]') as any;
        expect(paymentNotRequired).toBeTruthy();
        const paymentInfo = wrapper.find('[data-testid="payment-info"]') as any;
        expect(paymentInfo).toEqual({});
        await paymentNotRequired.props().onSubmit(paymentRequest);
        expect(submitOrderMock).toHaveBeenCalled();
    });

    describe('when order placed successfully', () => {
        beforeEach(async () => {
            const successResponse: OrderResponseModel = {
                brandId: BrandIdEnumModel.Arb,
                fulfillment: {
                    type: TallyFulfillmentTypeModel.PickUp,
                },
                requestStatus: OrderSubmitRequestStatusModel.Success,
                idempotentId: '223344',
                orderId: '999000',
            };
            const submitOrderMockFulfilled = jest.fn(async () => ({
                type: submitOrderActions.fulfilled.type,
                payload: {
                    response: successResponse,
                },
            }));

            (useSubmitOrder as jest.Mock).mockReturnValue({
                submitOrder: submitOrderMockFulfilled,
                resetSubmitOrder: jest.fn(),
            });

            (useSignUp as jest.Mock).mockReturnValue({
                signUpMarketingPreferences: mockSignUp,
            });

            (useOrderLocation as jest.Mock).mockReturnValue({
                currentLocation: {
                    contactDetails: { address: { postalCode: '12345' } },
                    id: 1,
                    displayName: 'displayName',
                },
                isCurrentLocationOAAvailable: true,
            });

            const wrapper = shallow(
                <Checkout
                    {...globalProps}
                    {...props}
                    checkoutHeader={checkoutHeaderMock as any}
                    checkoutLegal={checkoutLegalMock as any}
                />
            );
            const customerInfo = wrapper.find('[data-testid="customer-info"]') as any;
            customerInfo.props().onChange(
                {
                    firstName: 'firstName',
                    lastName: 'lastName',
                    phone: '1231231234',
                    email: 'test@test.com',
                    isSignUpChecked: true,
                },
                true
            );
            const paymentRequest = {} as IPaymentRequest;
            const paymentInfo = wrapper.find('[data-testid="payment-info"]') as any;
            await paymentInfo.props().onSubmit(paymentRequest);
        });

        it('should redirect to confirmation page w/ "id=999000" query parameter', async () => {
            expect(pushSpy).toHaveBeenCalledWith('/confirmation?id=999000');
        });
        it('should dispatch analytics events w/ custom data', async () => {
            expect(dispatchSpy).toHaveBeenNthCalledWith(1, {
                type: 'GTM_CHECKOUT_CUSTOMER_INFO',
                payload: {
                    tallyOrder: tallyOrderObject,
                    bagEntries: bagElements,
                    step: 2,
                    action: 'Checkout',
                },
            });
            expect(dispatchSpy).toHaveBeenNthCalledWith(2, {
                type: 'GTM_CHECKOUT_PAYMENT',
                payload: {
                    tallyOrder: tallyOrderObject,
                    bagEntries: bagElements,
                    step: 4,
                    action: 'Payment',
                },
            });
        });
        it('should log order data event to NR', async () => {
            expect(logger.logEvent).toHaveBeenCalledWith('order_placed', {
                isGuestUser: true,
                type: 'PickUp',
                amount: 33.33,
            });
        });
        it('should clear stuff', async () => {
            expect(removeAllFromBagSpy).toHaveBeenCalledWith({ lineItemIdList: [1, 2] });
            expect(setCorrelationIdSpy).toHaveBeenCalled();
        });

        it('should call signup if customer mark to do so', () => {
            const expectedParams = {
                firstName: 'firstName',
                lastName: 'lastName',
                email: 'test@test.com',
                storeId: 1,
                storeName: 'displayName',
                postalCode: '12345',
            };
            expect(mockSignUp).toHaveBeenCalledWith(expectedParams);
        });
    });
});
