import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';
import { IProductItemById } from '../../../../../../../common/services/globalContentfulProps';
import {
    useDomainProduct,
    useDomainMenuSelectors,
    useTallyPriceAndCalories,
    useProductItemGroup,
} from '../../../../../../../redux/hooks/domainMenu';

import ReviewOrderItem from '../../../../../../../components/organisms/checkout/reviewOrder/reviewOrderItem';
import { mealProductMock } from '../../../../../../mocks/TallyProduct.mock';

import { productsByIdMock } from '../../../../../../mocks/orderHistoryItem.mocks';
import { useTallyModifiers } from '../../../../../../../common/hooks/useTallyModifiers';
import { TallyProductModel } from '../../../../../../../@generated/webExpApi';

jest.mock('../../../../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn(),
    useDomainMenuSelectors: jest.fn(),
    useTallyPriceAndCalories: jest.fn().mockReturnValue({
        totalPrice: 1,
    }),
    useProductItemGroup: jest.fn(),
}));

jest.mock('../../../../../../../common/hooks/useTallyModifiers', () => ({
    useTallyModifiers: jest.fn(),
}));

describe('ReviewOrderItem', () => {
    it('should render ReviewOrderItem when no size selected', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test', price: { currentPrice: 5 } });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => null,
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: '' });

        const wrapper = shallow(
            <ReviewOrderItem
                tallyItem={{ price: 1, quantity: 1 } as TallyProductModel}
                productsById={{} as IProductItemById}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderItem when domainProduct price is undefined', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => null,
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: '' });

        const wrapper = shallow(
            <ReviewOrderItem
                tallyItem={{ price: 1, quantity: 1 } as TallyProductModel}
                productsById={{} as IProductItemById}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderItem when domainProduct name is undefined', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: undefined, price: { currentPrice: 5 } });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: '' });

        const wrapper = shallow(
            <ReviewOrderItem
                tallyItem={{ price: 1, quantity: 1, description: 'Roast Beef' } as TallyProductModel}
                productsById={{} as IProductItemById}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderItem when "Medium" size selected', () => {
        (useTallyPriceAndCalories as jest.Mock).mockReturnValue({ totalPrice: 10.57 });
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: '' });

        const wrapper = shallow(
            <ReviewOrderItem tallyItem={mealProductMock as TallyProductModel} productsById={{} as IProductItemById} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderItem whшер Discount', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: '' });
        const dataMock = {
            ...mealProductMock,
            discounts: [
                {
                    amount: 1.5,
                    quantity: 3,
                },
            ],
        };
        render(<ReviewOrderItem tallyItem={dataMock as TallyProductModel} productsById={{} as IProductItemById} />);

        expect(screen.getByText(/Offers\/Discount/i)).toBeInTheDocument();
        expect(screen.getByText(/4.50/i)).toBeInTheDocument();
    });

    it('should show product image when showImage is true', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => null,
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: '' });

        render(
            <ReviewOrderItem
                tallyItem={{ productId: 'IDPSalesItem-5289' } as TallyProductModel}
                productsById={(productsByIdMock as unknown) as IProductItemById}
                showImage={true}
            />
        );

        const picture = screen.getByAltText('Traditional Wings');
        expect(picture).toBeInTheDocument();
    });

    it('should add total for BOGO items', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'BOGO 10 Traditional Wings' });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => '10',
            };
        });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: [],
            addedModifiers: [],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({ name: 'BOGO' });

        render(
            <ReviewOrderItem
                tallyItem={{ productId: 'IDPSalesItem-5289' } as TallyProductModel}
                productsById={(productsByIdMock as unknown) as IProductItemById}
                showImage={true}
            />
        );

        expect(screen.getByText('BOGO 10 Traditional Wings (20 Total)')).toBeInTheDocument();
    });
});
