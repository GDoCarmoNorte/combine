import React from 'react';
import { shallow } from 'enzyme';

import ReviewOrder from '../../../../../../../components/organisms/checkout/reviewOrder/reviewOrder';
import mock from '../../../../../../mocks/TallyResponse.mock';
import { TallyResponseModel } from '../../../../../../../@generated/webExpApi';
import { render, screen } from '../../../../../../utils';

jest.mock('../../../../../../../redux/hooks/domainMenu', () => ({
    // @ts-ignore
    ...jest.requireActual('../../../../../../../redux/hooks/domainMenu'),
    useTallyItemsWithPricesAndCalories: jest.fn().mockReturnValue([]),
    useDomainProduct: jest.fn().mockReturnValue({
        id: '1',
    }),
    useProductItemGroup: jest.fn().mockReturnValue({ name: '' }),
}));

jest.mock('../../../../../../../lib/getFeatureFlags', () => ({
    // @ts-ignore
    ...jest.requireActual('../../../../../../../lib/getFeatureFlags'),
}));

jest.mock('../../../../../../../common/hooks/useConfiguration', () => ({
    createConfiguration: jest.fn(),
    useConfiguration: () => ({
        configuration: {
            isTippingEnabled: true,
        },
    }),
}));

jest.mock('../../../../../../../redux/hooks', () => ({
    useOrderLocation: () => ({ pickupAddress: { storeId: '99984' }, isPickUp: true }),
}));

jest.mock('../../../../../../../common/hooks/useTips', () => ({
    useTips: jest.fn(() => ({
        tipsItems: [
            { amount: 1, percentage: 0 },
            { amount: 2, percentage: 0 },
            { amount: 3, percentage: 0 },
        ],
        titleText: 'Add tip:',
        isError: false,
        errorMessage: '',
        defaultTipsId: 0,
    })),
}));

describe('ReviewOrder', () => {
    it('should render ReviewOrder without order', () => {
        const wrapper = shallow(<ReviewOrder withTips={false} productsById={{}} tipsAmount={0} checkoutLegal={{}} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrder with order', () => {
        const wrapper = shallow(
            <ReviewOrder
                withTips={false}
                productsById={{}}
                order={mock as TallyResponseModel}
                tipsAmount={0}
                checkoutLegal={{}}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrder with tips block', () => {
        const wrapper = shallow(
            <ReviewOrder
                tipsAmount={0}
                setTipsAmount={jest.fn()}
                withTips={true}
                productsById={{}}
                order={mock as TallyResponseModel}
                checkoutLegal={{}}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrder with service & delivery fees', () => {
        const orderWithFeesMock = {
            ...mock,
            fees: [
                { type: 'serviceFee', name: 'SERVICE FEE', amount: 10.5 },
                { type: 'deliveryFee', name: 'DELIVERY FEE', amount: 11.5 },
            ],
        };

        render(
            <ReviewOrder
                setTipsAmount={jest.fn()}
                tipsAmount={5}
                withTips={true}
                productsById={{}}
                order={orderWithFeesMock as TallyResponseModel}
                checkoutLegal={{}}
            />
        );

        expect(screen.getByText(/service fee/i)).toBeInTheDocument();
        expect(screen.getByText(/10.50/i)).toBeInTheDocument();
        expect(screen.getByText(/delivery fee/i)).toBeInTheDocument();
        expect(screen.getByText(/11.50/i)).toBeInTheDocument();
    });

    it('should render ReviewOrder with free delivery when delivery discount is provided', () => {
        const orderWithFeesMock = {
            ...mock,
            fees: [{ type: 'DELIVERY', name: 'THRESHOLDBASED DELIVERY FEE', amount: 2 }],
            discounts: {
                discountDetails: [
                    {
                        name: 'Free delivery',
                        amount: 2,
                    },
                ],
            },
        };

        render(
            <ReviewOrder
                setTipsAmount={jest.fn()}
                tipsAmount={0}
                withTips={false}
                productsById={{}}
                order={orderWithFeesMock as TallyResponseModel}
                checkoutLegal={{}}
            />
        );

        expect(screen.getByText(/delivery fee/i)).toBeInTheDocument();
        expect(screen.getByText(/0.00/i)).toBeInTheDocument();
    });

    it('should render ReviewOrder with discount', () => {
        const orderWithFeesMock = {
            ...mock,
            discounts: {
                discountType: 'PROVIDED',
                discountDetails: [
                    {
                        appliedItems: [
                            {
                                lineItemId: 1,
                                amount: 1.5,
                                menuItemId: 'arb-000',
                                quantity: 3,
                            },
                        ],
                    },
                ],
            },
        };

        render(
            <ReviewOrder
                withTips={false}
                productsById={{}}
                order={orderWithFeesMock as TallyResponseModel}
                tipsAmount={0}
                checkoutLegal={{}}
            />
        );

        expect(screen.getByText(/Discounts/i)).toBeInTheDocument();
        expect(screen.getByText(/4.50/i)).toBeInTheDocument();
    });

    it('should render ReviewOrder with tips block and tips field if tipping is enabled and tips amount not a 0', () => {
        render(
            <ReviewOrder
                tipsAmount={5}
                setTipsAmount={jest.fn()}
                withTips={true}
                productsById={{}}
                order={mock as TallyResponseModel}
                checkoutLegal={{}}
            />
        );

        expect(screen.getByText(/\$5.00/i)).toBeInTheDocument();
    });

    it('should render ReviewOrder without tips block and tips field if tipping is not enabled', () => {
        render(
            <ReviewOrder
                tipsAmount={0}
                setTipsAmount={jest.fn()}
                withTips={false}
                productsById={{}}
                order={mock as TallyResponseModel}
                checkoutLegal={{}}
            />
        );

        expect(screen.queryByText(/\$0.00/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Tip/i)).not.toBeInTheDocument();
    });

    it('should render ReviewOrder with tips block and tips field if tipping is enabled and tips amount is 0', () => {
        render(
            <ReviewOrder
                tipsAmount={0}
                setTipsAmount={jest.fn()}
                withTips={true}
                productsById={{}}
                order={mock as TallyResponseModel}
                checkoutLegal={{}}
            />
        );

        expect(screen.getByText(/\$0.00/i)).toBeInTheDocument();
    });

    it('should render ReviewOrder with disclaimer text for each fee and with asterisks when legal messages are provided', () => {
        const orderWithFeesMock = {
            ...mock,
            fees: [
                { type: 'DELIVERY', name: 'DELIVERY FEE', amount: 2 },
                { type: 'SERVICE', name: 'SERVICE FEE', amount: 2 },
                { type: 'TAKEOUT', name: 'TAKEOUT FEE', amount: 2 },
            ],
        };

        const checkoutLegalMock = {
            deliveryFeeLegalMessage: 'deliveryFeeLegalMessage',
            serviceFeeLegalMessage: 'serviceFeeLegalMessage',
            takeoutServiceFeeLegalMessage: 'takeoutServiceFeeLegalMessage',
        };

        render(
            <ReviewOrder
                setTipsAmount={jest.fn()}
                tipsAmount={0}
                withTips={false}
                productsById={{}}
                order={orderWithFeesMock as TallyResponseModel}
                checkoutLegal={checkoutLegalMock}
            />
        );

        expect(screen.getByText(/delivery fee \*/i)).toBeInTheDocument();
        expect(screen.getByText(/service fee \*\*/i)).toBeInTheDocument();
        expect(screen.getByText(/takeout fee \*\*\*/i)).toBeInTheDocument();
        expect(screen.getByText(/\* deliveryFeeLegalMessage/i)).toBeInTheDocument();
        expect(screen.getByText(/\*\* serviceFeeLegalMessage/i)).toBeInTheDocument();
        expect(screen.getByText(/\*\*\* takeoutServiceFeeLegalMessage/i)).toBeInTheDocument();
    });

    it('should render ReviewOrder with disclaimer text for provied legal messages only', () => {
        const orderWithFeesMock = {
            ...mock,
            fees: [
                { type: 'DELIVERY', name: 'DELIVERY FEE', amount: 2 },
                { type: 'SERVICE', name: 'SERVICE FEE', amount: 2 },
                { type: 'TAKEOUT', name: 'TAKEOUT FEE', amount: 2 },
            ],
        };

        const checkoutLegalMock = {
            serviceFeeLegalMessage: 'serviceFeeLegalMessage',
        };

        render(
            <ReviewOrder
                setTipsAmount={jest.fn()}
                tipsAmount={0}
                withTips={false}
                productsById={{}}
                order={orderWithFeesMock as TallyResponseModel}
                checkoutLegal={checkoutLegalMock}
            />
        );

        expect(screen.getByText(/service fee \*/i)).toBeInTheDocument();
        expect(screen.getByText(/\* serviceFeeLegalMessage/i)).toBeInTheDocument();
    });
});
