import React from 'react';
import { shallow } from 'enzyme';

import {
    ReviewOrderComboItem,
    ReviewOrderComboItemList,
} from '../../../../../../../components/organisms/checkout/reviewOrder/reviewOrderComboItem';
import { useDomainMenuSelectors, useDomainProduct } from '../../../../../../../redux/hooks/domainMenu';
import { useTallyModifiers } from '../../../../../../../common/hooks/useTallyModifiers';
import { singleProductMock } from '../../../../../../mocks/TallyProduct.mock';
import { TallyProductModel } from '../../../../../../../@generated/webExpApi';

jest.mock('../../../../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn(),
    useDomainMenuSelectors: jest.fn(),
}));

jest.mock('../../../../../../../common/hooks/useTallyModifiers', () => ({
    useTallyModifiers: jest.fn(),
}));

describe('ReviewOrderComboItem', () => {
    it('should render ReviewOrderComboItem', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useTallyModifiers as jest.Mock).mockReturnValue({});
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });

        const wrapper = shallow(<ReviewOrderComboItem comboItem={singleProductMock as TallyProductModel} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderComboItem with size label', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useTallyModifiers as jest.Mock).mockReturnValue({});
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });

        const wrapper = shallow(<ReviewOrderComboItem comboItem={singleProductMock as TallyProductModel} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderComboItem with tally removed modifiers', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            removedDefaultModifiers: ['removedModifier'],
        });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });

        const wrapper = shallow(<ReviewOrderComboItem comboItem={singleProductMock as TallyProductModel} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ReviewOrderComboItem with tally added modifiers', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'test' });
        (useTallyModifiers as jest.Mock).mockReturnValue({
            addedModifiers: ['addedModifier'],
        });
        (useDomainMenuSelectors as jest.Mock).mockImplementation(() => {
            return {
                selectProductSize: () => 'Medium',
            };
        });

        const wrapper = shallow(<ReviewOrderComboItem comboItem={singleProductMock as TallyProductModel} />);

        expect(wrapper).toMatchSnapshot();
    });
});

describe('ReviewOrderComboItemList', () => {
    it('should render ReviewOrderComboItemList', () => {
        const wrapper = shallow(<ReviewOrderComboItemList comboItems={[singleProductMock as TallyProductModel]} />);

        expect(wrapper).toMatchSnapshot();
    });
});
