import React from 'react';
import { render, screen } from '@testing-library/react';
import CustomerInfo from '../../../../../components/organisms/checkout/customerInfo';

describe('customer info', () => {
    const accountInfoMock = {
        firstName: 'testName',
        lastName: 'testLastName',
        phone: '(762) 436-5093',
        email: 'test@mail.com',
    };
    it('should renders correctly', () => {
        render(<CustomerInfo accountInfo={accountInfoMock} />);

        expect(screen.getByDisplayValue(/testName/i)).toBeInTheDocument();
        expect(screen.getByDisplayValue(/testLastName/i)).toBeInTheDocument();
        expect(screen.getByDisplayValue(/7624365093/i)).toBeInTheDocument();
        expect(screen.getByDisplayValue(/test@mail.com/i)).toBeInTheDocument();
    });
});
