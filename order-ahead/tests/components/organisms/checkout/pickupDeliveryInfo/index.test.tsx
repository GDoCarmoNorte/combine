import React from 'react';
import { mount, shallow } from 'enzyme';
import { screen, render } from '@testing-library/react';

import PickupDeliveryInfo from '../../../../../components/organisms/checkout/pickupDeliveryInfo';

import useBag from '../../../../../redux/hooks/useBag';
import useOrderLocation from '../../../../../redux/hooks/useOrderLocation';

import { resolveOpeningHours } from '../../../../../lib/locations';
import { add } from '../../../../../common/helpers/dateTime';

jest.mock('../../../../../redux/hooks/useBag');
jest.mock('../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../lib/locations');

const dateReferenceStart = new Date('2020-12-17T06:00:00.000Z');
const dateReferenceEnd = new Date('2020-12-17T19:00:00.000Z');
const openedTimeRanges = [
    {
        start: dateReferenceStart,
        end: dateReferenceEnd,
    },
    {
        start: add(dateReferenceStart, { days: 1 }),
        end: add(dateReferenceEnd, { days: 1 }),
    },
    {
        start: add(dateReferenceStart, { days: 2 }),
        end: add(dateReferenceEnd, { days: 2 }),
    },
    {
        start: add(dateReferenceStart, { days: 3 }),
        end: add(dateReferenceEnd, { days: 3 }),
    },
    {
        start: add(dateReferenceStart, { days: 4 }),
        end: add(dateReferenceEnd, { days: 4 }),
    },
    {
        start: add(dateReferenceStart, { days: 5 }),
        end: add(dateReferenceEnd, { days: 5 }),
    },
    {
        start: add(dateReferenceStart, { days: 6 }),
        end: add(dateReferenceEnd, { days: 6 }),
    },
    {
        start: add(dateReferenceStart, { days: 7 }),
        end: add(dateReferenceEnd, { days: 7 }),
    },
];

const availableTimeSlotsMock = {
    pickup: {
        byDay: {
            '2021-10-19': [
                {
                    display: '11:30 AM',
                    utc: new Date('2021-10-19T16:30:00.000Z'),
                },
            ],
        },
    },
    delivery: {
        byDay: {
            '2021-10-19': [
                {
                    display: '11:30 AM',
                    utc: '2021-10-19T16:30:00.000Z',
                },
            ],
        },
    },
};

const RealDate = Date.now;

describe('pickupInfo component', () => {
    beforeAll(() => {
        global.Date.now = jest.fn(() => new Date('2020-12-17T13:00:00.000Z').getTime());
    });

    afterAll(() => {
        global.Date.now = RealDate;
    });

    beforeEach(() => {
        (useBag as jest.Mock).mockReturnValue({
            orderTime: '2020-12-17T13:30:00.000Z',
            pickupTimeValues: [
                {
                    day: '2020-12-17T06:00:00.000Z',
                    timeRange: ['2020-12-17T13:15:00.000Z', '2020-12-17T13:30:00.000Z', '2020-12-17T13:45:00.000Z'],
                },
            ],
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            currentLocation: {},
        });
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            openedTimeRanges,
            storeTimezone: 'America/New_York',
        });
    });

    it('should match snapshot', () => {
        const wrapper = shallow(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot when info is loading', () => {
        const wrapper = shallow(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={true}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot when info is not loading', async () => {
        const wrapper = mount(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should show "ASAP" option with corresponding hint, if store is open and have preparation time field', async () => {
        (useBag as jest.Mock).mockReturnValue({
            orderTime: 'asap',
            orderTimeType: 'asap',
            pickupTimeValues: [
                {
                    day: '2020-12-17T06:00:00.000Z',
                    timeRange: ['2020-12-17T13:15:00.000Z', '2020-12-17T13:30:00.000Z', '2020-12-17T13:45:00.000Z'],
                },
            ],
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            currentLocation: { additionalFeatures: { prepTime: 5 } },
        });
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            openedTimeRanges,
            storeTimezone: 'America/New_York',
            isOpen: true,
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        expect(screen.getByText(/asap/i)).toBeInTheDocument();
        expect(screen.getByText(/(Ready in ~5 minutes)/i)).toBeInTheDocument();
    });

    it('should show optional fields for delivery instructions', async () => {
        (useBag as jest.Mock).mockReturnValue({
            orderTime: 'asap',
            orderTimeType: 'asap',
            pickupTimeValues: [
                {
                    day: '2020-12-17T06:00:00.000Z',
                    timeRange: ['2020-12-17T13:15:00.000Z', '2020-12-17T13:30:00.000Z', '2020-12-17T13:45:00.000Z'],
                },
            ],
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            currentLocation: {},
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        expect(screen.getByText(/Delivery Instructions/i)).toBeInTheDocument();
        expect(screen.getByText(/Delivery Place/i)).toBeInTheDocument();
        expect(screen.getByPlaceholderText(/APT\/Suite #/i)).toBeInTheDocument();
        expect(screen.getByPlaceholderText(/Enter instructions/i)).toBeInTheDocument();
    });

    it('should show optional fields for delivery instructions with initial value of addressline2', async () => {
        (useBag as jest.Mock).mockReturnValue({
            orderTime: 'asap',
            orderTimeType: 'asap',
            pickupTimeValues: [
                {
                    day: '2020-12-17T06:00:00.000Z',
                    timeRange: ['2020-12-17T13:15:00.000Z', '2020-12-17T13:30:00.000Z', '2020-12-17T13:45:00.000Z'],
                },
            ],
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            currentLocation: {},
            pickupLocationTimeSlots: availableTimeSlotsMock,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            deliveryAddress: { deliveryLocation: { addressLine2: 'Good house' } },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        expect(screen.getByDisplayValue(/Good house/i)).toBeInTheDocument();
    });
});
