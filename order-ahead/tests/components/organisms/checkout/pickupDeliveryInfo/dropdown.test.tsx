import React from 'react';
import { screen, render } from '@testing-library/react';

import { DropdownTarget } from '../../../../../components/organisms/checkout/pickupDeliveryInfo/dropdown';

describe('pickupDeliveryInfo dropdown', () => {
    describe('DropdownTarget', () => {
        it('should render correctly', async () => {
            render(<DropdownTarget text="DropdownTestText" />);

            expect(screen.getByText(/DropdownTestText/i)).toBeInTheDocument();
        });

        it('should render with hint', async () => {
            render(<DropdownTarget text="DropdownTestTextWithHint" hint="DropdownTestHint" />);

            expect(screen.getByText(/DropdownTestTextWithHint/i)).toBeInTheDocument();
            expect(screen.getByText(/(DropdownTestHint)/i)).toHaveClass('labelHint');
        });
    });
});
