import React from 'react';
import { shallow } from 'enzyme';
import RecommendedProducts from '../../../../components/organisms/recommendedProducts';
import { screen, render } from '@testing-library/react';

import * as recommendedProductsMocks from '../../../mocks/recommendedProducts.mock';

import * as constants from '../../../../components/organisms/recommendedProducts/constants';
import { useProductIsAvailable } from '../../../../common/hooks/useProductIsAvailable';
import globalContentfulPropsMock from '../../../mock-data/contentful/globalContentfulPropsMock.json';

let mockRecommendedProducts = {};

jest.mock('../../../../common/hooks/useProductIsAvailable');

jest.mock('../../../../lib/domainProduct', () => ({
    __esModule: true,
    getRelatedProduct: jest.fn(),
}));

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProductByContentfulFields: () => ({ id: '640213024' }),
    useDiscountPriceAndCalories: () => ({ price: 5, calories: 100 }),
    useProductIsCombo: () => true,
    useProductCategory: () => ({
        id: 'arb-cat-000-002',
        name: 'Limited Time',
        sequence: 1,
        parentCategoryId: 'arb-cat-000-000',
    }),
    useDomainMenuSelectors: jest.fn().mockImplementation(() => ({
        selectProductSize: jest.fn(),
    })),
    useProductItemGroup: jest.fn(),
    useDomainProduct: jest.fn().mockReturnValue({
        id: '640239548',
    }),
    useDefaultTallyItem: () => ({
        name: 'Test',
        quantity: 1,
        productId: 'IDPItem-1234',
        price: 1,
        modifierGroups: [],
        childExtras: [],
    }),
    useTallyPriceAndCalories: () => ({
        calories: 500,
    }),
}));

jest.mock('../../../../redux/hooks', () => ({
    useBag: () => ({
        bagEntries: [],
        actions: {},
    }),
    useDomainMenu: jest.fn().mockResolvedValue(false),
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalContentfulPropsMock),
    usePdp: jest.fn(),
    useSelectedSell: jest.fn().mockImplementation(() => mockRecommendedProducts),
    useOrderLocation: jest.fn().mockReturnValue({
        actions: {
            setPickupLocation: jest.fn(),
            setDeliveryLocation: jest.fn(),
            flushSelectedLocation: jest.fn(),
        },
    }),
}));

jest.mock('../../../../redux/store', () => ({
    __esModule: true,
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
    dispatch: jest.fn(),
}));

jest.mock('../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn().mockReturnValue({}),
}));

jest.mock('../../../../common/hooks/useTallyItemHasSodiumWarning', () => ({
    useTallyItemHasSodiumWarning: () => true,
}));

describe('RecommendedProducts component', () => {
    (useProductIsAvailable as jest.Mock).mockReturnValue({ isAvailable: true });
    beforeEach(() => {
        //replace constant with mock
        Object.defineProperty(constants, 'TITLE_TEXT', { value: recommendedProductsMocks.recommendedProductsTitle });
    });

    it('should render some recommended products', () => {
        mockRecommendedProducts = recommendedProductsMocks.recommendedProductsFull;

        const wrapper = shallow(<RecommendedProducts />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should render 3 recommended products', () => {
        mockRecommendedProducts = recommendedProductsMocks.recommendedProductsFull;

        render(<RecommendedProducts />);

        expect(screen.queryByText(/mockrecommendedproductstitle/i)).not.toBe(null);
        expect(screen.queryAllByTitle(/shake/i).length).toBe(3);
    });

    it('should render with one recommended product', () => {
        mockRecommendedProducts = recommendedProductsMocks.recommendedProductsSingle;

        render(<RecommendedProducts />);

        expect(screen.queryByText(/mockrecommendedproductstitle/i)).not.toBe(null);
        expect(screen.queryAllByTitle(/shake/i).length).toBe(1);
    });

    it('should not render with no recommended products', () => {
        mockRecommendedProducts = recommendedProductsMocks.recommendedProductsEmpty;

        render(<RecommendedProducts />);

        expect(screen.queryByText(/mockrecommendedproductstitle/i)).toBe(null);
        expect(screen.queryAllByTitle(/shake/i).length).toBe(0);
    });

    it('should not render invalid recommended products', () => {
        mockRecommendedProducts = recommendedProductsMocks.recommendedProductsWithOneInvalid;

        render(<RecommendedProducts />);

        expect(screen.queryByText(/mockrecommendedproductstitle/i)).not.toBe(null);
        expect(screen.queryAllByTitle(/shake/i).length).toBe(2);
    });
});
