import { INutritionInfo } from '../../../../redux/types';

const nutritionInfo: INutritionInfo[] = [
    {
        displayName: "King's Hawaiian Fish Deluxe",
        sizeSelections: [
            {
                name: 'None',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 300,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Calories',
                        weight: {
                            value: 695,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Calories from Fat',
                        weight: {
                            value: 304,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 34,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 10,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 1,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 103,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 997,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 74,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 3,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Sugars (g)',
                        weight: {
                            value: 19,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Proteins (g)',
                        weight: {
                            value: 25,
                            unit: 'g',
                        },
                    },
                ],
                allergicInformation: 'Contains egg, milk, soy, wheat, Fish (Pollock)',
            },
        ],
    },
    {
        displayName: 'Curly Fries',
        sizeSelections: [
            {
                name: 'Small',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 128,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Calories',
                        weight: {
                            value: 410,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Calories from Fat',
                        weight: {
                            value: 200,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 22,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 3,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 0,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 940,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 49,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 5,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Sugars (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Proteins (g)',
                        weight: {
                            value: 5,
                            unit: 'g',
                        },
                    },
                ],
                allergicInformation:
                    'Contains wheat. Menu item is cooked in the same oil as other items that contains egg, milk, soy, fish (where available). ',
            },
            {
                name: 'Medium',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 170,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Calories',
                        weight: {
                            value: 550,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Calories from Fat',
                        weight: {
                            value: 260,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 29,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 4,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 0,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 1250,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 65,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 6,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Sugars (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Proteins (g)',
                        weight: {
                            value: 6,
                            unit: 'g',
                        },
                    },
                ],
                allergicInformation:
                    'Contains wheat. Menu item is cooked in the same oil as other items that contains egg, milk, soy, fish (where available). ',
            },
            {
                name: 'Large',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                        weight: {
                            value: 201,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Calories',
                        weight: {
                            value: 650,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Calories from Fat',
                        weight: {
                            value: 310,
                            unit: 'cal',
                        },
                    },
                    {
                        label: 'Fat - Total (g)',
                        weight: {
                            value: 35,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Saturated Fat (g)',
                        weight: {
                            value: 5,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Trans Fat (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Cholesterol (mg)',
                        weight: {
                            value: 0,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Sodium (mg)',
                        weight: {
                            value: 1480,
                            unit: 'mg',
                        },
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                        weight: {
                            value: 77,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Dietary Fiber (g)',
                        weight: {
                            value: 7,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Sugars (g)',
                        weight: {
                            value: 0,
                            unit: 'g',
                        },
                    },
                    {
                        label: 'Proteins (g)',
                        weight: {
                            value: 8,
                            unit: 'g',
                        },
                    },
                ],
                allergicInformation:
                    'Contains wheat. Menu item is cooked in the same oil as other items that contains egg, milk, soy, fish (where available). ',
            },
        ],
    },
    {
        displayName: 'Coca-Cola',
        sizeSelections: [
            {
                name: 'Small',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                    },
                    {
                        label: 'Calories',
                    },
                    {
                        label: 'Calories from Fat',
                    },
                    {
                        label: 'Fat - Total (g)',
                    },
                    {
                        label: 'Saturated Fat (g)',
                    },
                    {
                        label: 'Trans Fat (g)',
                    },
                    {
                        label: 'Cholesterol (mg)',
                    },
                    {
                        label: 'Sodium (mg)',
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                    },
                    {
                        label: 'Dietary Fiber (g)',
                    },
                    {
                        label: 'Sugars (g)',
                    },
                    {
                        label: 'Proteins (g)',
                    },
                ],
            },
            {
                name: 'Medium',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                    },
                    {
                        label: 'Calories',
                    },
                    {
                        label: 'Calories from Fat',
                    },
                    {
                        label: 'Fat - Total (g)',
                    },
                    {
                        label: 'Saturated Fat (g)',
                    },
                    {
                        label: 'Trans Fat (g)',
                    },
                    {
                        label: 'Cholesterol (mg)',
                    },
                    {
                        label: 'Sodium (mg)',
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                    },
                    {
                        label: 'Dietary Fiber (g)',
                    },
                    {
                        label: 'Sugars (g)',
                    },
                    {
                        label: 'Proteins (g)',
                    },
                ],
            },
            {
                name: 'Large',
                nutritionalFacts: [
                    {
                        label: 'Serving Weight (g)',
                    },
                    {
                        label: 'Calories',
                    },
                    {
                        label: 'Calories from Fat',
                    },
                    {
                        label: 'Fat - Total (g)',
                    },
                    {
                        label: 'Saturated Fat (g)',
                    },
                    {
                        label: 'Trans Fat (g)',
                    },
                    {
                        label: 'Cholesterol (mg)',
                    },
                    {
                        label: 'Sodium (mg)',
                    },
                    {
                        label: 'Total Carbohydrates (g)',
                    },
                    {
                        label: 'Dietary Fiber (g)',
                    },
                    {
                        label: 'Sugars (g)',
                    },
                    {
                        label: 'Proteins (g)',
                    },
                ],
            },
        ],
    },
];

export default nutritionInfo;
