import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Nutrition from '../../../../components/organisms/nutrition/nutrition';
import nutritionData from './nutrition.mock';

describe('nutrition component', () => {
    const getNutritionSection = () => screen.getByText(/Nutrition Information/i);

    it('should render collapsed nutrition component', () => {
        render(<Nutrition nutritionLink={null} nutritionInfo={nutritionData} />);

        getNutritionSection();

        screen.getByText('View nutrition information');
        expect(screen.queryByText('Hide nutrition information')).not.toBeInTheDocument();
    });

    it('should expand nutrition and render dropdowns for combo product', () => {
        render(<Nutrition nutritionLink={null} nutritionInfo={nutritionData} />);

        userEvent.click(getNutritionSection());

        screen.getByText('Hide nutrition information');
        screen.getByText("King's Hawaiian Fish Deluxe");
        screen.getByText('Curly Fries');
        screen.getByText('Coca-Cola');
    });

    it('should call "onHide" function when clicking "Hide nutrition information"', () => {
        const onHideMock = jest.fn();
        render(<Nutrition nutritionLink={null} nutritionInfo={nutritionData} onHide={onHideMock} />);

        userEvent.click(screen.getByText('View nutrition information'));
        userEvent.click(screen.getByText('Hide nutrition information'));

        expect(onHideMock).toHaveBeenCalled();
    });

    it('should render nutrition information inside expanded dropdown after click', () => {
        const { container } = render(<Nutrition nutritionLink={null} nutritionInfo={nutritionData} />);

        userEvent.click(getNutritionSection());
        userEvent.click(screen.getByText("King's Hawaiian Fish Deluxe"));
        expect(container).toMatchSnapshot();
    });

    it('should render nutrition information inside expanded dropdown after keypress', () => {
        const { container } = render(<Nutrition nutritionLink={null} nutritionInfo={nutritionData} />);

        userEvent.type(getNutritionSection(), '{ enter }');
        userEvent.click(screen.getByText("King's Hawaiian Fish Deluxe"));
        expect(container).toMatchSnapshot();
    });

    it('should render nutrition content for single product after click', () => {
        const { container } = render(<Nutrition nutritionLink={null} nutritionInfo={[nutritionData[0]]} />);

        userEvent.click(getNutritionSection());

        expect(container).toMatchSnapshot();
    });

    it('should render nutrition content for single product after keypress', () => {
        const { container } = render(<Nutrition nutritionLink={null} nutritionInfo={[nutritionData[0]]} />);

        userEvent.type(getNutritionSection(), '{ enter }');

        expect(container).toMatchSnapshot();
    });

    it('should render nutrition content for a product with single size without tabs after click', () => {
        render(
            <Nutrition
                nutritionLink={null}
                nutritionInfo={[{ ...nutritionData[0], sizeSelections: [nutritionData[0].sizeSelections[0]] }]}
            />
        );

        userEvent.click(getNutritionSection());

        expect(screen.queryByRole('tab', { name: 'Small' })).not.toBeInTheDocument();
    });

    it('should render nutrition content for a product with single size without tabs after keypress', () => {
        render(
            <Nutrition
                nutritionLink={null}
                nutritionInfo={[{ ...nutritionData[0], sizeSelections: [nutritionData[0].sizeSelections[0]] }]}
            />
        );

        userEvent.type(getNutritionSection(), '{ enter }');

        expect(screen.queryByRole('tab', { name: 'Small' })).not.toBeInTheDocument();
    });

    it('should render nutrition when no data is provided after click', () => {
        const { container } = render(
            <Nutrition nutritionLink={null} nutritionInfo={[{ ...nutritionData[0], sizeSelections: null }]} />
        );

        userEvent.click(getNutritionSection());

        expect(container).toMatchSnapshot();
    });

    it('should render nutrition when no data is provided after keypress', () => {
        const { container } = render(
            <Nutrition nutritionLink={null} nutritionInfo={[{ ...nutritionData[0], sizeSelections: null }]} />
        );

        userEvent.type(getNutritionSection(), '{ enter }');

        expect(container).toMatchSnapshot();
    });

    it('should render nutrition component with nutrition link', () => {
        const nutritionLink = { link: 'https://example.com' };
        const { container } = render(<Nutrition nutritionLink={nutritionLink as any} nutritionInfo={nutritionData} />);

        expect(container).toMatchSnapshot();
    });
});
