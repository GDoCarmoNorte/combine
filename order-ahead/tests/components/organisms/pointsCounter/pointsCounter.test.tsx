import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import PointsCounter from '../../../../components/organisms/pointsCounter/pointsCounter';
import globalPropsMock from '../../../mocks/globalProps.mock';
import isMobileScreen from '../../../../lib/isMobileScreen';
import { useLoyalty } from '../../../../redux/hooks';
import userEvent from '@testing-library/user-event';
import { useRouter } from 'next/router';
import { format } from '../../../../common/helpers/dateTime';

const dispatchMock = jest.fn();

jest.mock('../../../../common/hooks/useLoyaltyService');

jest.mock('../../../../lib/isMobileScreen', () => jest.fn().mockReturnValue(false));

jest.mock('react-redux', () => ({
    useDispatch: () => dispatchMock,
    useSelector: () => jest.fn(),
    createSelectorHook: jest.fn(),
}));

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

jest.mock('../../../../redux/hooks/useLoyalty');

jest.mock('../../../../redux/hooks/useNotifications.ts', () => {
    return {
        __esModule: true,
        default: jest.fn().mockImplementation(() => {
            return {
                actions: {
                    enqueueError: jest.fn(),
                },
            };
        }),
    };
});

jest.mock('../../../../common/hooks/useCheckin', () => {
    return {
        useCheckin: jest.fn().mockReturnValue({ checkin: jest.fn(), isShowTooltip: false }),
    };
});

jest.mock('next/router');

describe('Points Counter component', () => {
    beforeAll(() => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });
    });

    it('should render correctly', async () => {
        render(<PointsCounter />);
        expect(await screen.findByLabelText('Points Counter')).toMatchSnapshot();
    });

    it('should render correctly for loading state', () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: true,
        });
        const { container } = render(<PointsCounter />);
        expect(container).toMatchSnapshot();
    });

    it('should have button with text check-in for mobile screens', async () => {
        (isMobileScreen as jest.Mock).mockReturnValue(true);
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });
        render(<PointsCounter />);
        const checkInButton = screen.findByRole('button', { name: /check-in/i });
        expect(await checkInButton).toBeInTheDocument();
    });

    it('should have fined rewards button when pointsBalance is greater than 0', async () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 300,
                pointsExpiring: 25,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });
        render(<PointsCounter />);
        const findRewardsButton = screen.findByRole('button', { name: /find rewards/i });
        expect(await findRewardsButton).toBeInTheDocument();
    });

    it('should have button order  now when pointsBalance is equal  to 0', async () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });
        render(<PointsCounter />);
        const orderNowButton = screen.findByRole('button', { name: /order now/i });
        expect(await orderNowButton).toBeInTheDocument();
    });

    it('should navigate to  /rewards page when fined rewards button is clicked', async () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 30,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
        });
        const routerPush = jest.fn();
        (useRouter as jest.Mock).mockReturnValue({
            push: routerPush,
        });
        render(<PointsCounter />);
        const getFindRewardsButton = () => screen.getByRole('button', { name: /find rewards/i });
        await waitFor(() => {
            expect(getFindRewardsButton()).toBeInTheDocument();
        });

        userEvent.click(getFindRewardsButton());

        await waitFor(() => {
            expect(routerPush).toBeCalledWith('/rewards');
        });
    });

    it('should have expiration date in format MM/dd/yyyy', async () => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 30,
                pointsExpiring: 0,
                pointsExpiringDate: '2021-12-17T09:24:00.000Z',
            },
            isLoading: false,
        });
        render(<PointsCounter />);
        const formattedDate = format(new Date('2021-12-17T09:24:00.000Z'), 'MM/dd/yyyy');
        const regexp = new RegExp(formattedDate, 'i');
        expect(await screen.findByText(regexp)).toBeInTheDocument();
    });
});
