import React from 'react';
import { render, screen } from '@testing-library/react';
import { CheckinResult } from '../../../../components/organisms/pointsCounter/checkinResult';
import { LOCATION_SERVICES_DISABLED_ERROR_CODE } from '../../../../common/helpers/getCoordinates';
import { TErrorCodeModel } from '../../../../@generated/webExpApi';

jest.mock('../../../../components/atoms/BrandLoader', () => () => 'loader');
jest.mock('../../../../components/organisms/pointsCounter/constants', () => ({
    CHECKIN_ERROR_TEXT: 'CHECKIN_ERROR_TEXT',
    CHECKIN_LOCATION_SERVICES_DISABLED_TEXT: 'CHECKIN_LOCATION_SERVICES_DISABLED_TEXT',
    CHECKIN_NO_LOCATIONS_AROUND_TEXT: 'CHECKIN_NO_LOCATIONS_AROUND_TEXT',
    CHECKIN_SUCCESS_TEXT: 'CHECKIN_SUCCESS_TEXT',
}));

describe('CheckinResult', () => {
    it('should render loader', () => {
        render(<CheckinResult loading={true} error={null} payload={null} />);

        expect(screen.getByText(/loader/i)).toBeInTheDocument();
    });

    it('should render success message', () => {
        render(<CheckinResult loading={false} error={null} payload={{} as any} />);

        expect(screen.getByText(/CHECKIN_SUCCESS_TEXT/i)).toBeInTheDocument();
    });

    it('should render default error message', () => {
        render(<CheckinResult loading={false} error={{} as any} payload={null} />);

        expect(screen.getByText(/CHECKIN_ERROR_TEXT/i)).toBeInTheDocument();
    });

    it('should render location services disabled error message', () => {
        render(
            <CheckinResult
                loading={false}
                error={{ code: LOCATION_SERVICES_DISABLED_ERROR_CODE } as any}
                payload={null}
            />
        );

        expect(screen.getByText(/CHECKIN_LOCATION_SERVICES_DISABLED_TEXT/i)).toBeInTheDocument();
    });

    it('should render no locations around error message', () => {
        render(
            <CheckinResult loading={false} error={{ code: TErrorCodeModel.NoLocationsAround } as any} payload={null} />
        );

        expect(screen.getByText(/CHECKIN_NO_LOCATIONS_AROUND_TEXT/i)).toBeInTheDocument();
    });
});
