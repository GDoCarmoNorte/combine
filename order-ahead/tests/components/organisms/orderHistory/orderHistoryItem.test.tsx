import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { OrderHistoryItem } from '../../../../components/organisms/orderHistory/orderHistoryItem/orderHistoryItem';
import { productsByIdMock, orderHistoryItemMock, domainProductsMock } from '../../../mocks/orderHistoryItem.mocks';
import { IOrderModel } from '../../../../@generated/webExpApi';
import { IProductItemById } from '../../../../common/services/globalContentfulProps';
import {
    useDomainProducts,
    useDomainMenuSelectors,
    useTallyPriceAndCalories,
    useSelectedModifiers,
    useDefaultModifiers,
    useDomainProduct,
} from '../../../../redux/hooks/domainMenu';
import { useGlobalProps, useOrderLocation } from '../../../../redux/hooks';
import useAddToBagFromOrderHistory from '../../../../common/hooks/useAddToBagFromOrderHistory';
import LocationsMock from '../../../mocks/expLocations.mock';

jest.mock('react-redux');
jest.mock('../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));
jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/domainMenu');
jest.mock('../../../../common/hooks/useAddToBagFromOrderHistory');

describe('Order History Item', () => {
    (useTallyPriceAndCalories as jest.Mock).mockReturnValue({ totalPrice: 9.72 });

    const addFromOrderHistoryMock = jest.fn();
    (useDomainProducts as jest.Mock).mockReturnValue(domainProductsMock);
    (useDomainMenuSelectors as jest.Mock).mockReturnValue({
        selectProductSize: () => 'size',
        selectDefaultModifiers: () => [],
    });
    (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: 'paths' });
    (useSelectedModifiers as jest.Mock).mockReturnValue([]);
    (useDefaultModifiers as jest.Mock).mockReturnValue([]);
    (useAddToBagFromOrderHistory as jest.Mock).mockReturnValue({
        addFromOrderHistory: addFromOrderHistoryMock,
        modifierGroups: [
            [
                {
                    modifiers: [
                        {
                            price: 0,
                            productId: 'IDPModifier-16411',
                            quantity: 1,
                        },
                    ],
                },
            ],
        ],
    });

    it('"ADD TO BAG" button happy flow', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: LocationsMock.locations[1],
            isCurrentLocationOAAvailable: true,
            isPickUp: true,
        });
        (useDomainProduct as jest.Mock).mockReturnValue({ id: 'IDPSalesItem-5289' });

        render(
            <OrderHistoryItem
                orderItem={(orderHistoryItemMock as unknown) as IOrderModel}
                productsById={(productsByIdMock as unknown) as IProductItemById}
            />
        );
        const addButton = screen.getByRole('button', { name: 'add to bag' });
        expect(addButton).not.toBeDisabled();
        fireEvent.click(addButton);
        expect(addFromOrderHistoryMock).toHaveBeenCalled();
    });

    it('"SELECT A LOCATION" button should be displayed if no location is selected', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isCurrentLocationOAAvailable: true,
            isPickUp: true,
        });
        (useDomainProduct as jest.Mock).mockReturnValue({ id: 'IDPSalesItem-5289' });

        render(
            <OrderHistoryItem
                orderItem={(orderHistoryItemMock as unknown) as IOrderModel}
                productsById={(productsByIdMock as unknown) as IProductItemById}
            />
        );
        const addButton = screen.getByRole('link', { name: /select a location/i });
        expect(addButton).not.toBeDisabled();
        expect(addButton).toHaveAttribute('href', '/locations');
    });
});
