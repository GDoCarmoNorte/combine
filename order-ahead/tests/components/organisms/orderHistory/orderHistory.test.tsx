import React from 'react';
import { render, screen } from '@testing-library/react';
import { OrderHistory } from '../../../../components/organisms/orderHistory/orderHistory';
import { productsByIdMock, orderHistoryItemMock, domainProductsMock } from '../../../mocks/orderHistoryItem.mocks';
import { IProductItemById } from '../../../../common/services/globalContentfulProps';
import { useBag, useGlobalProps, useOrderHistory, useOrderLocation } from '../../../../redux/hooks';
import {
    useDomainMenuSelectors,
    useDomainProducts,
    useTallyPriceAndCalories,
    useSelectedModifiers,
    useDefaultModifiers,
    useProductItemGroup,
    useDomainProduct,
} from '../../../../redux/hooks/domainMenu';
import useAddToBagFromOrderHistory from '../../../../common/hooks/useAddToBagFromOrderHistory';
import LocationsMock from '../../../mocks/expLocations.mock';

jest.mock('react-redux');
jest.mock('../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));
jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/domainMenu');
jest.mock('../../../../redux/hooks/useOrderHistory');
jest.mock('../../../../common/hooks/useAddToBagFromOrderHistory');

describe('Order History', () => {
    it('Should renders a loading', () => {
        (useOrderHistory as jest.Mock).mockReturnValue({ orderHistory: [], isLoading: true });

        render(<OrderHistory productsById={(productsByIdMock as unknown) as IProductItemById} />);
        expect(screen.getByRole(/progressbar/i)).toBeInTheDocument();
    });

    it('Should renders OrderHistoryEmpty', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: 'paths' });
        (useOrderHistory as jest.Mock).mockReturnValue({ orderHistory: [], isLoading: false });
        render(<OrderHistory productsById={(productsByIdMock as unknown) as IProductItemById} />);
        expect(screen.getByText(/no recent orders/i)).toBeInTheDocument();
    });

    it('Should render OrdersHistoryItems', () => {
        (useTallyPriceAndCalories as jest.Mock).mockReturnValue({ totalPrice: 9.72 });
        (useDomainProducts as jest.Mock).mockReturnValue(domainProductsMock);
        (useSelectedModifiers as jest.Mock).mockReturnValue([]);
        (useDefaultModifiers as jest.Mock).mockReturnValue([]);
        (useDomainMenuSelectors as jest.Mock).mockReturnValue({
            selectProductSize: () => 'size',
            selectDefaultModifiers: () => [],
        });
        (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: 'paths' });
        const addToBag = jest.fn();
        (useBag as jest.Mock).mockReturnValue({
            actions: { addDefaultToBag: addToBag },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: LocationsMock.locations[1],
            isPickUp: true,
        });
        (useOrderHistory as jest.Mock).mockReturnValue({ orderHistory: [orderHistoryItemMock], isLoading: false });
        (useAddToBagFromOrderHistory as jest.Mock).mockReturnValue({
            modifierGroups: [
                [
                    {
                        modifiers: [
                            {
                                price: 0,
                                productId: 'IDPModifier-16411',
                                quantity: 1,
                            },
                        ],
                    },
                ],
            ],
        });
        (useProductItemGroup as jest.Mock).mockReturnValue({});
        (useDomainProduct as jest.Mock).mockReturnValue({ id: 'IDPSalesItem-5289' });
        render(<OrderHistory productsById={(productsByIdMock as unknown) as IProductItemById} />);
        expect(screen.getByRole('button', { name: 'add to bag' })).toBeInTheDocument();
    });
});
