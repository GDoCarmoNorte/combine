import React from 'react';
import { shallow, mount } from 'enzyme';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import AccountInfo from '../../../../components/organisms/accountInfo';
import { rootReducer } from '../../../../redux/store';
import globalPropsMock from '../../../mocks/globalProps.mock';
import { render, screen } from '@testing-library/react';

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

const accountMock = {
    firstName: 'test name',
    lastName: 'test last name',
    phone: '1231231234',
    email: 'test@gmail.com',
    birthDate: '12/12',
    pending: false,
    tooltipIsOpen: false,
};

describe('AccountInfo component', () => {
    it('should render AccountInfo form', () => {
        const onSubmit = jest.fn();
        const onChangePasswordClick = jest.fn();
        const wrapper = shallow(
            <AccountInfo {...accountMock} onSubmit={onSubmit} onChangePasswordClick={onChangePasswordClick} />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should call onChangePasswordClick after click on change password link', () => {
        const onSubmit = jest.fn();
        const onChangePasswordClick = jest.fn();
        const store = createStore(rootReducer);

        const wrapper = mount(
            <Provider store={store}>
                <AccountInfo {...accountMock} onSubmit={onSubmit} onChangePasswordClick={onChangePasswordClick} />
            </Provider>
        );

        const changePasswordLink = wrapper.find('[aria-label="Change Password"]');

        changePasswordLink.simulate('click');

        expect(onChangePasswordClick).toHaveBeenCalledTimes(1);
    });

    it('date of birth must be editable if not received from api', () => {
        const onSubmit = jest.fn();
        const onChangePasswordClick = jest.fn();
        render(
            <AccountInfo
                {...accountMock}
                onSubmit={onSubmit}
                onChangePasswordClick={onChangePasswordClick}
                birthDate=""
            />
        );

        expect(screen.getByPlaceholderText(/MM\/DD/i)).not.toBeDisabled();
    });
});
