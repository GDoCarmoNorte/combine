import React from 'react';
import { render, screen } from '@testing-library/react';
import BirthDayInput from '../../../../../components/organisms/accountInfo/components/birthDayInput';
import { Formik } from 'formik';
const onSubmit = jest.fn();

describe('Account info birthday input component', () => {
    it('should be  editable in case if received disabled  false or not received', () => {
        render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <BirthDayInput />
            </Formik>
        );
        expect(screen.getByPlaceholderText(/MM\/DD/i)).not.toBeDisabled();
    });
    it('should be none editable in case if received disabled true', () => {
        render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <BirthDayInput disabled />
            </Formik>
        );
        expect(screen.getByPlaceholderText(/MM\/DD/i)).toBeDisabled();
    });
});
