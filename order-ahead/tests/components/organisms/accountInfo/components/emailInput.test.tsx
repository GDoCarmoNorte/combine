import React from 'react';
import { render, screen } from '@testing-library/react';
import EmailInput from '../../../../../components/organisms/accountInfo/components/emailInput';
import { Formik } from 'formik';
const onSubmit = jest.fn();

describe('Account info email input component', () => {
    it('should render correctly', () => {
        const { container } = render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <EmailInput />
            </Formik>
        );
        expect(container).toMatchSnapshot();
    });
    it('should be none editable filed', () => {
        render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <EmailInput />
            </Formik>
        );
        expect(screen.getByPlaceholderText(/Enter Email/i)).toBeDisabled();
    });
});
