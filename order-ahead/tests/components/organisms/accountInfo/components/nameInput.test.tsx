import React from 'react';
import { render, screen } from '@testing-library/react';
import NameInput from '../../../../../components/organisms/accountInfo/components/nameInput';
import { Formik } from 'formik';
const onSubmit = jest.fn();
const firstNameMockProps = {
    maxLength: 256,
    name: 'firstName',
    label: 'First Name',
    type: 'text',
    placeHolder: 'Enter First Name',
};
const lastNameMockProps = {
    maxLength: 256,
    name: 'lastName',
    label: 'Last Name',
    type: 'text',
    placeHolder: 'Enter Last Name',
};

describe('Account info birthday input component', () => {
    it('should be  editable in case if received disabled  false or not received', () => {
        const { container } = render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <NameInput {...firstNameMockProps} />
            </Formik>
        );
        expect(container).toMatchSnapshot();
    });
    it('should be editable input', () => {
        render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <Formik initialValues={{}} onSubmit={onSubmit}>
                    <NameInput {...firstNameMockProps} />
                </Formik>
            </Formik>
        );
        expect(screen.getByPlaceholderText(/Enter First Name/i)).not.toBeDisabled();
    });
    // eslint-disable-next-line jest/expect-expect
    it('should be editable for last name filed', () => {
        render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <Formik initialValues={{}} onSubmit={onSubmit}>
                    <NameInput {...lastNameMockProps} />
                </Formik>
            </Formik>
        );
    });
});
