import React from 'react';
import { render } from '@testing-library/react';
import PhoneInput from '../../../../../components/organisms/accountInfo/components/phoneInput';
import { Formik } from 'formik';
const onSubmit = jest.fn();

describe('Account info phone input component', () => {
    it('should render correctly', () => {
        const { container } = render(
            <Formik initialValues={{}} onSubmit={onSubmit}>
                <PhoneInput />
            </Formik>
        );
        expect(container).toMatchSnapshot();
    });
});
