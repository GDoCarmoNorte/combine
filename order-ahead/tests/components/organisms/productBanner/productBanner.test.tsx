import React from 'react';
import { screen, render, cleanup, fireEvent } from '@testing-library/react';

import productMock from './mocks/productBanner.mock';
import ProductBanner from '../../../../components/organisms/productBanner';
import { useProductIsSaleable } from '../../../../common/hooks/useProductIsSaleable';
import {
    GTM_NOT_SALEABLE_DESCRIPTION,
    GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
    NOT_SALEABLE_BUTTON_TEXT,
} from '../../../../common/constants/product';

import { useDispatch } from 'react-redux';
import { GTM_ERROR_EVENT } from '../../../../common/services/gtmService/constants';
import { GtmErrorEvent } from '../../../../common/services/gtmService/types';
import { useLocalization } from '../../../../common/hooks/useLocalization';

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useIsProductEditable: () => true,
    useDefaultModifiers: () => [],
    useSelectedModifiers: () => [],
    useSelectedSideAndDrinks: () => [],
    useDomainMenuSelectors: () => ({
        selectProductById: null,
        selectProductSize: '',
        selectProductSizes: () => [],
    }),
    useProductItemGroup: jest.fn().mockReturnValue({}),
}));

jest.mock('../../../../common/hooks/useLocalization', () => ({
    useLocalization: () => ({
        locationLinkText: '',
        descriptionText: '',
        isProductAvailable: true,
    }),
}));

jest.mock('../../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: jest.fn().mockReturnValue({
        isSaleable: true,
    }),
}));

jest.mock('../../../../common/hooks/useSodiumWarning', () => ({
    useSodiumWarning: () => null,
}));

jest.mock('../../../../common/hooks/useTallyItemHasSodiumWarning', () => ({
    useTallyItemHasSodiumWarning: () => false,
}));

jest.mock('../../../../common/hooks/useSodiumLegalWarning', () => ({
    useSodiumLegalWarning: () => null,
}));

jest.mock('../../../../redux/hooks/usePdp', () => () => ({
    pdpTallyItem: {
        productId: 'arb-itm-000-057',
    },
}));

jest.mock('../../../../redux/hooks/useGlobalProps', () => () => ({}));
jest.mock('react-redux');
jest.mock('../../../../common/hooks/useLocalization', () => ({
    useLocalization: jest.fn().mockReturnValue({
        locationLinkText: '',
        descriptionText: '',
        isLocationOrderAheadAvailable: true,
        isProductAvailable: true,
    }),
}));
const productInfo = {
    price: 100,
    calories: 100,
};

describe('productBanner', () => {
    const mockDispatch = jest.fn();

    afterEach(cleanup);

    test('should render the product banner correctly', async () => {
        const onAddToBag = jest.fn();
        const onModifyProduct = jest.fn();
        render(
            <ProductBanner
                product={productMock as any}
                onModifyProduct={onModifyProduct as any}
                onAddToBag={onAddToBag}
                addToBagBtnText="Add to bag"
                productInfo={productInfo}
                displayName="Greek Gyro"
                mainProductSectionName="Sandwich"
                productType="MEAL"
            />
        );

        expect(screen.getByLabelText(/Product Main Section/)).toMatchSnapshot();
    });

    test('should render the product banner with "Make it a meal" button', async () => {
        const onAddToBag = jest.fn();
        const onModifyProduct = jest.fn();

        render(
            <ProductBanner
                product={productMock as any}
                onModifyProduct={onModifyProduct as any}
                onAddToBag={onAddToBag}
                displayName="Greek Gyro"
                mainProductSectionName="Sandwich"
                productType="MEAL"
                addToBagBtnText="Add to bag"
                productInfo={productInfo}
                showMakeItAMealButton
            />
        );

        await screen.findByText(/Make it a meal/i);
    });

    test('should call click events properly', async () => {
        const onAddToBag = jest.fn();
        const onModifyProduct = jest.fn();
        render(
            <ProductBanner
                product={productMock as any}
                onModifyProduct={onModifyProduct as any}
                displayName="Greek Gyro"
                mainProductSectionName="Sandwich"
                productType="MEAL"
                onAddToBag={onAddToBag}
                addToBagBtnText="Add to bag"
                productInfo={productInfo}
            />
        );

        fireEvent.click(screen.getByRole('button', { name: /Add to bag/i }));
        expect(onAddToBag).toHaveBeenCalledTimes(1);

        fireEvent.click(screen.getAllByText(/Modify Sandwich/)[0]);
        expect(onModifyProduct).toHaveBeenCalledTimes(1);
    });

    test('should render not saleable message and dispatch gtm error', () => {
        (useDispatch as jest.Mock).mockReturnValue(mockDispatch);
        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({
            isSaleable: false,
        });

        render(
            <ProductBanner
                product={productMock as any}
                onModifyProduct={jest.fn()}
                displayName="Greek Gyro"
                mainProductSectionName="Sandwich"
                productType="MEAL"
                onAddToBag={jest.fn()}
                addToBagBtnText="Add to bag"
                productInfo={productInfo}
            />
        );

        const payload = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: GTM_NOT_SALEABLE_DESCRIPTION,
        } as GtmErrorEvent;

        expect(mockDispatch).toBeCalledWith({ type: GTM_ERROR_EVENT, payload });
        expect(screen.getByText(NOT_SALEABLE_BUTTON_TEXT)).toBeInTheDocument();
    });

    test('should dispatch gtm error event when location link is available', () => {
        const LOCATION_LINK_MESSAGE = 'LOCATION_LINK_MESSAGE';
        const DESCRIPTION_TEXT = 'DESCRIPTION_TEXT';
        (useLocalization as jest.Mock<any, any>).mockReturnValueOnce({
            locationLinkText: LOCATION_LINK_MESSAGE,
            descriptionText: DESCRIPTION_TEXT,
            isLocationOrderAheadAvailable: false,
        });
        (useDispatch as jest.Mock).mockReturnValue(mockDispatch);

        render(
            <ProductBanner
                product={productMock as any}
                onModifyProduct={jest.fn()}
                displayName="Greek Gyro"
                mainProductSectionName="Sandwich"
                productType="MEAL"
                onAddToBag={jest.fn()}
                addToBagBtnText="Add to bag"
                productInfo={productInfo}
            />
        );

        const payload = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: DESCRIPTION_TEXT,
        } as GtmErrorEvent;

        expect(mockDispatch).toBeCalledWith({ type: GTM_ERROR_EVENT, payload });
    });
});
