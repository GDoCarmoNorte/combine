import React from 'react';
import { render, screen } from '@testing-library/react';

import { useBag, useGlobalProps } from '../../../../redux/hooks';
import { useDomainProduct } from '../../../../redux/hooks/domainMenu';
import UnavailableItem from '../../../../components/organisms/unavailableItemsModal/unavailableItem';
import restoreAllMocks = jest.restoreAllMocks;
import userEvent from '@testing-library/user-event';

jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/domainMenu');

const onRemoveMock = jest.fn();
const onModifiersRemoveMock = jest.fn();
const onModifyClickMock = jest.fn();
const onSetLastEdit = jest.fn();

describe('UnavailableItem component', () => {
    describe('when whole product is unavailable', () => {
        beforeEach(() => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [
                    {
                        productId: 'IDPSalesItem-6348',
                        lineItemId: 5,
                        quantity: 1,
                    },
                ],
                actions: { setLastEdit: onSetLastEdit },
            });
            (useGlobalProps as jest.Mock).mockReturnValue({
                productsById: {
                    ['IDPSalesItem-6348']: {
                        fields: {
                            name: 'New York-Syle Content',
                            image: 'Image',
                        },
                    },
                },
            });
            (useDomainProduct as jest.Mock).mockReturnValue({ name: 'New York-Syle Domain Name' });
        });

        afterEach(() => {
            restoreAllMocks();
        });

        it('should display correct data', () => {
            render(
                <UnavailableItem
                    lineItemId={5}
                    unavailableProductId="IDPSalesItem-6348"
                    unavailableModifierIds={[]}
                    unavailableSubModifierIds={[]}
                    isWholeProductUnavailable={true}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.getByText(/REMOVE/i)).toBeInTheDocument();
            expect(screen.getByText(/Not Available/i)).toBeInTheDocument();
            expect(screen.getByText(/New York-Syle Domain Name/i)).toBeInTheDocument();
        });

        it('should display name from contentful', () => {
            (useDomainProduct as jest.Mock).mockReturnValue({ name: '' });

            render(
                <UnavailableItem
                    lineItemId={5}
                    unavailableProductId="IDPSalesItem-6348"
                    unavailableModifierIds={[]}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.getByText(/New York-Syle Content/i)).toBeInTheDocument();
        });

        it('should display empty name', () => {
            (useGlobalProps as jest.Mock).mockReturnValue({
                productsById: {},
            });

            (useDomainProduct as jest.Mock).mockReturnValue({});

            render(
                <UnavailableItem
                    lineItemId={5}
                    unavailableProductId="IDPSalesItem-6348"
                    unavailableModifierIds={[]}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.queryByText(/New York-Syle Cheesecake Slice/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/New York-Syle Content/i)).not.toBeInTheDocument();
        });

        it('should display name with quantity', () => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [
                    {
                        productId: 'IDPSalesItem-6348',
                        lineItemId: 5,
                        quantity: 3,
                    },
                ],
                actions: { setLastEdit: onSetLastEdit },
            });

            render(
                <UnavailableItem
                    lineItemId={5}
                    unavailableProductId="IDPSalesItem-6348"
                    unavailableModifierIds={[]}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.getByText(/3X New York-Syle Domain Name/i)).toBeInTheDocument();
        });

        it('should display content with unavailable item in bag', () => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [],
                actions: { setLastEdit: onSetLastEdit },
            });

            render(
                <UnavailableItem
                    lineItemId={5}
                    unavailableProductId="IDPSalesItem-6348"
                    unavailableModifierIds={[]}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.getByText(/REMOVE/i)).toBeInTheDocument();
        });

        it('should call onRemove with correct arg', () => {
            render(
                <UnavailableItem
                    lineItemId={5}
                    unavailableProductId="IDPSalesItem-6348"
                    isWholeProductUnavailable={true}
                    unavailableModifierIds={[]}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            userEvent.click(screen.getByText('REMOVE'));
            expect(onRemoveMock).toHaveBeenCalled();
            expect(onRemoveMock).toBeCalledWith(5);
        });
    });

    describe('when modifiers are unavailable', () => {
        beforeEach(() => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [
                    {
                        productId: 'IDPSalesItem-4879',
                        quantity: 1,
                        lineItemId: 1,
                        modifierGroups: [
                            {
                                modifiers: [
                                    {
                                        productId: 'IDPModifier-13117',
                                        quantity: 1,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                actions: { setLastEdit: onSetLastEdit },
            });
            (useGlobalProps as jest.Mock).mockReturnValue({
                productsById: {
                    ['IDPSalesItem-4879']: {
                        fields: {
                            name: 'Potato Wedges - Regular',
                            image: 'Image',
                        },
                    },
                },
                modifierItemsById: {
                    ['IDPModifier-13117']: {
                        fields: {
                            name: 'Chili',
                            image: 'Image',
                        },
                    },
                },
            });
            (useDomainProduct as jest.Mock)
                .mockReturnValueOnce({ name: 'Potato Wedges Domain Name' })
                .mockReturnValueOnce({ name: 'New York-Syle Domain Name' });
        });

        afterEach(() => {
            restoreAllMocks();
        });

        it('should display correct data', () => {
            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.getByText(/MODIFY/i)).toBeInTheDocument();
            expect(screen.getByText(/REMOVE/i)).toBeInTheDocument();
            expect(screen.getByText(/Not Available/i)).toBeInTheDocument();
            expect(screen.getByText(/New York-Syle Domain Name/i)).toBeInTheDocument();
            expect(screen.getByLabelText(/modify unavailable modifier/i)).toBeInTheDocument();
            expect(screen.getByLabelText(/remove unavailable modifier/i)).toBeInTheDocument();
        });

        it('should display correct data for touched state', () => {
            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                    isTouched
                />
            );

            expect(screen.getByText(/REMOVED UNAVAILABLE ITEMS/i)).toBeInTheDocument();
            expect(screen.getByText(/Not Available/i)).toBeInTheDocument();
            expect(screen.getByText(/New York-Syle Domain Name/i)).toBeInTheDocument();
            expect(screen.getByLabelText(/remove unavailable modifier/i)).toBeInTheDocument();
        });

        it('should display correct inputs for unavailable default modifier', () => {
            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                    isDefaultModifiersUnavailable
                />
            );

            expect(screen.getByText(/MODIFY/i)).toBeInTheDocument();
            expect(screen.getByText(/REMOVE/i)).toBeInTheDocument();
            expect(screen.getByText(/Not Available/i)).toBeInTheDocument();
            expect(screen.getByText(/Potato Wedges Domain Name/i)).toBeInTheDocument();
            expect(screen.getByLabelText(/modify bag item/i)).toBeInTheDocument();
            expect(screen.getByLabelText(/remove bag item/i)).toBeInTheDocument();
        });

        it('should display correct inputs for unavailable default modifier for touched state', () => {
            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                    isDefaultModifiersUnavailable
                    isTouched
                />
            );

            expect(screen.getByText(/REMOVED/i)).toBeInTheDocument();
            expect(screen.getByText(/Not Available/i)).toBeInTheDocument();
            expect(screen.getByText(/Potato Wedges Domain Name/i)).toBeInTheDocument();
            expect(screen.getByLabelText(/remove bag item/i)).toBeInTheDocument();
        });

        it('should display empty name', () => {
            (useGlobalProps as jest.Mock).mockReturnValue({
                productsById: {},
                modifierItemsById: {},
            });

            (useDomainProduct as jest.Mock).mockReset().mockReturnValueOnce({}).mockReturnValueOnce({});

            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.queryByText(/New York-Syle Domain Name/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/Potato Wedges Domain Name/i)).not.toBeInTheDocument();
        });

        it('should display name with quantity', () => {
            (useBag as jest.Mock).mockReturnValue({
                bagEntries: [
                    {
                        productId: 'IDPSalesItem-4879',
                        quantity: 3,
                        lineItemId: 1,
                        modifierGroups: [
                            {
                                modifiers: [
                                    {
                                        productId: 'IDPModifier-13117',
                                        quantity: 2,
                                    },
                                ],
                            },
                        ],
                    },
                ],
                actions: { setLastEdit: onSetLastEdit },
            });

            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            expect(screen.getByText(/New York-Syle Domain Name x2/i)).toBeInTheDocument();
        });

        it('should call onModifiersRemove with correct arg', () => {
            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            userEvent.click(screen.getByText('REMOVE'));
            expect(onModifiersRemoveMock).toHaveBeenCalled();
            expect(onModifiersRemoveMock).toBeCalledWith(
                {
                    lineItemId: 1,
                    modifierGroups: [{ modifiers: [{ productId: 'IDPModifier-13117', quantity: 1 }] }],
                    productId: 'IDPSalesItem-4879',
                    quantity: 1,
                },
                ['IDPModifier-13117'],
                []
            );
        });

        it('should call onModifyClick with correct arg', () => {
            render(
                <UnavailableItem
                    lineItemId={1}
                    isWholeProductUnavailable={false}
                    unavailableProductId="IDPSalesItem-4879"
                    unavailableModifierIds={['IDPModifier-13117']}
                    unavailableSubModifierIds={[]}
                    onRemove={onRemoveMock}
                    onModifiersRemove={onModifiersRemoveMock}
                    onModifyClick={onModifyClickMock}
                />
            );

            userEvent.click(screen.getByText('MODIFY'));
            expect(onModifyClickMock).toHaveBeenCalled();
            expect(onModifyClickMock).toBeCalledWith(1);
        });
    });
});
