import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { useBag, useGlobalProps } from '../../../../redux/hooks';
import { useDomainProduct } from '../../../../redux/hooks/domainMenu';
import UnavailableItemsModal from '../../../../components/organisms/unavailableItemsModal/';
import restoreAllMocks = jest.restoreAllMocks;
import { GTM_ERROR_EVENT } from '../../../../common/services/gtmService/constants';
import { useDispatch } from 'react-redux';
import { GtmErrorCategory } from '../../../../common/services/gtmService/types';

jest.mock('react-redux');
jest.mock('../../../../redux/hooks');
jest.mock('../../../../redux/hooks/domainMenu');

const onRemoveMock = jest.fn();
const onModifiersRemoveMock = jest.fn();
const onModifyClickMock = jest.fn();
const onCloseMock = jest.fn();
const onSetLastEdit = jest.fn();

describe('UnavailableItem component', () => {
    const dispatchMock = jest.fn();
    beforeEach(() => {
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    productId: 'IDPSalesItem-6348',
                    lineItemId: 5,
                    quantity: 1,
                    modifierGroups: [
                        {
                            modifiers: [
                                {
                                    productId: 'IDPModifier-13117',
                                    quantity: 1,
                                },
                                {
                                    productId: 'IDPModifier-5847',
                                    quantity: 1,
                                    modifierGroups: [
                                        {
                                            modifiers: [
                                                {
                                                    productId: 'IDPModifier-13118',
                                                    quantity: 1,
                                                },
                                            ],
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
            actions: { setLastEdit: onSetLastEdit },
        });
        (useGlobalProps as jest.Mock).mockReturnValue({
            productsById: {
                ['IDPSalesItem-6348']: {
                    fields: {
                        name: 'New York-Syle Content',
                        image: 'Image',
                    },
                },
            },
            modifierItemsById: {
                ['IDPModifier-13117']: {
                    fields: {
                        name: 'Chili',
                        image: 'Image',
                    },
                },
                ['IDPModifier-13118']: {
                    fields: {
                        name: 'Becon',
                        image: 'Image',
                    },
                },
            },
        });
        (useDomainProduct as jest.Mock).mockReturnValue({ name: 'New York-Syle Cheesecake Domain Name' });
        (useDispatch as jest.Mock).mockReturnValue(dispatchMock);
    });

    afterEach(() => {
        restoreAllMocks();
        dispatchMock.mockReset();
    });

    it('should display correct data', () => {
        (useDomainProduct as jest.Mock)
            .mockReset()
            .mockReturnValueOnce({ name: 'New York-Syle Cheesecake Domain Name' })
            .mockReturnValueOnce({ name: 'Chili Domain Name' })
            .mockReturnValueOnce({ name: 'Becon Domain Name' });

        render(
            <UnavailableItemsModal
                open
                unavailableItems={[
                    {
                        productId: 'IDPSalesItem-6348',
                        lineItemId: 5,
                        isWholeProductUnavailable: false,
                        isDefaultModifiersUnavailable: false,
                        modifierIds: ['IDPModifier-13117'],
                        subModifierIds: ['IDPModifier-13118'],
                        isTouched: false,
                    },

                    {
                        productId: 'IDPSalesItem-6345',
                        lineItemId: 1,
                        isWholeProductUnavailable: true,
                        isDefaultModifiersUnavailable: false,
                        modifierIds: [],
                        subModifierIds: [],
                        isTouched: false,
                    },
                ]}
                onRemove={onRemoveMock}
                onModifiersRemove={onModifiersRemoveMock}
                onModifyClick={onModifyClickMock}
                onClose={onCloseMock}
                isNotValidTime={false}
            />
        );

        expect(screen.getByText(/change in your bag/i)).toBeInTheDocument();
        expect(screen.getByText(/A modification or side in your bag is no longer available./i)).toBeInTheDocument();
        expect(
            screen.getByText(/One or more of the items above require modification before you can proceed./i)
        ).toBeInTheDocument();
        expect(screen.getAllByText(/Chili Domain Name/i).length).toEqual(1);
        expect(screen.getAllByText(/Becon Domain Name/i).length).toEqual(1);
        expect(screen.getAllByText(/Not Available/i).length).toEqual(3);
    });

    it('should display correct data for unavailable default modifiers', () => {
        render(
            <UnavailableItemsModal
                open
                unavailableItems={[
                    {
                        productId: 'IDPSalesItem-6348',
                        lineItemId: 5,
                        isWholeProductUnavailable: true,
                        modifierIds: ['IDPModifier-13117'],
                        subModifierIds: [],
                        isDefaultModifiersUnavailable: false,
                        isTouched: false,
                    },
                    {
                        productId: 'IDPSalesItem-6345',
                        lineItemId: 1,
                        isWholeProductUnavailable: true,
                        modifierIds: [],
                        subModifierIds: [],
                        isDefaultModifiersUnavailable: false,
                        isTouched: false,
                    },
                ]}
                onRemove={onRemoveMock}
                onModifiersRemove={onModifiersRemoveMock}
                onModifyClick={onModifyClickMock}
                onClose={onCloseMock}
                isNotValidTime={false}
            />
        );

        expect(screen.getByText(/change in your bag/i)).toBeInTheDocument();
        expect(screen.getByText(/A modification or side in your bag is no longer available./i)).toBeInTheDocument();
        expect(
            screen.getByText(/One or more of the items above require modification before you can proceed./i)
        ).toBeInTheDocument();
        expect(screen.getAllByText(/Not Available/i).length).toEqual(2);
        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_ERROR_EVENT,
            payload: {
                ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_ITEMS,
                ErrorDescription: 'A modification or side in your bag is no longer available.',
            },
        });
        expect(dispatchMock).toHaveBeenCalledTimes(1);
    });

    it('should display null for no unavailable items', () => {
        render(
            <UnavailableItemsModal
                open
                unavailableItems={null}
                onRemove={onRemoveMock}
                onModifiersRemove={onModifiersRemoveMock}
                onModifyClick={onModifyClickMock}
                onClose={onCloseMock}
                isNotValidTime={false}
            />
        );

        expect(screen.queryByText(/change in your bag/i)).not.toBeInTheDocument();
    });

    it('should display correct data for not valid items by time', () => {
        render(
            <UnavailableItemsModal
                open
                unavailableItems={[
                    {
                        productId: 'IDPSalesItem-6348',
                        lineItemId: 5,
                        isWholeProductUnavailable: true,
                        isDefaultModifiersUnavailable: false,
                        modifierIds: [],
                        subModifierIds: [],
                        isTouched: false,
                    },
                    {
                        productId: 'IDPSalesItem-6345',
                        lineItemId: 1,
                        isWholeProductUnavailable: true,
                        isDefaultModifiersUnavailable: false,
                        modifierIds: [],
                        subModifierIds: [],
                        isTouched: false,
                    },
                ]}
                onRemove={onRemoveMock}
                onClose={onCloseMock}
                onModifiersRemove={onModifiersRemoveMock}
                onModifyClick={onModifyClickMock}
                isNotValidTime={true}
            />
        );

        expect(screen.getByText(/change in your bag/i)).toBeInTheDocument();
        expect(
            screen.getByText(
                /These products are only available during a specific period of time. Update your chosen time to order these products or remove unavailable products./i
            )
        ).toBeInTheDocument();
        expect(
            screen.getByText(/One or more of the items above require modification before you can proceed./i)
        ).toBeInTheDocument();
        expect(screen.getAllByText(/Not Available/i).length).toEqual(2);
        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_ERROR_EVENT,
            payload: {
                ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_ITEMS,
                ErrorDescription:
                    'These products are only available during a specific period of time. Update your chosen time to order these products or remove unavailable products.',
            },
        });
        expect(dispatchMock).toHaveBeenCalledTimes(1);
    });

    it('should call onClose', () => {
        render(
            <UnavailableItemsModal
                open
                unavailableItems={[
                    {
                        productId: 'IDPSalesItem-6348',
                        lineItemId: 5,
                        isWholeProductUnavailable: false,
                        isDefaultModifiersUnavailable: false,
                        modifierIds: ['IDPModifier-13117'],
                        subModifierIds: [],
                        isTouched: false,
                    },
                    {
                        productId: 'IDPSalesItem-6345',
                        lineItemId: 1,
                        isWholeProductUnavailable: true,
                        isDefaultModifiersUnavailable: false,
                        modifierIds: [],
                        subModifierIds: [],
                        isTouched: false,
                    },
                ]}
                onRemove={onRemoveMock}
                onModifiersRemove={onModifiersRemoveMock}
                onModifyClick={onModifyClickMock}
                onClose={onCloseMock}
                isNotValidTime={false}
            />
        );

        const closeIcon = screen.getByLabelText(/Close Icon/i);
        userEvent.click(closeIcon);

        expect(onCloseMock).toBeCalled();
    });
});
