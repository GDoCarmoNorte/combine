import React from 'react';
import { render, screen } from '@testing-library/react';
import {
    ILocationServicesProps,
    LocationServices,
} from '../../../../../components/organisms/locationInfo/locationServices';
import { IServiceTypeModel } from '../../../../../@generated/webExpApi';
import { locationDetailsMock } from '../../../../mocks/locationDetails.mock';
import { LocationServicesModel } from '../../../../../common/constants/locations';

describe('locationServices component', () => {
    it('should render location services', () => {
        const props = {
            services: locationDetailsMock.services,
        };

        render(<LocationServices {...(props as ILocationServicesProps)} />);

        screen.getByText(LocationServicesModel[IServiceTypeModel.Wifi]);
    });

    it('should not render location services if there are no services', () => {
        const props = {
            services: null,
        };

        render(<LocationServices {...(props as ILocationServicesProps)} />);

        expect(screen.queryByText(LocationServicesModel[IServiceTypeModel.Wifi])).not.toBeInTheDocument();
    });
});
