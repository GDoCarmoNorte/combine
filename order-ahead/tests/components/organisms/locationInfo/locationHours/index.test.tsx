import React from 'react';
import { render, screen } from '@testing-library/react';
import { ILocationHoursProps, LocationHours } from '../../../../../components/organisms/locationInfo/locationHours';
import { locationDetailsMock } from '../../../../mocks/locationDetails.mock';
import restoreAllMocks = jest.restoreAllMocks;
import { THourDayOfWeekModel } from '../../../../../@generated/webExpApi';

describe('LocationHours component', () => {
    beforeEach(() => {
        restoreAllMocks();
    });

    it('should render store opening hours', () => {
        const props = {
            hoursByDay: locationDetailsMock.hoursByDay,
            timezone: locationDetailsMock.timezone,
        };

        jest.spyOn(require('../../../../../common/helpers/convertHours'), 'convertHours').mockImplementation(
            (value) => value
        );

        render(<LocationHours {...(props as ILocationHoursProps)} />);

        screen.getByText(THourDayOfWeekModel.Monday);
        screen.getByText(THourDayOfWeekModel.Tuesday);
        screen.getByText(THourDayOfWeekModel.Wednesday);
        screen.getByText(THourDayOfWeekModel.Thursday);
        screen.getByText(THourDayOfWeekModel.Saturday);
        screen.getByText(THourDayOfWeekModel.Sunday);
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Mon.start} - ${locationDetailsMock.hoursByDay.Mon.end}`
            )
        ).not.toHaveLength(0);
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Tue.start} - ${locationDetailsMock.hoursByDay.Tue.end}`
            )
        ).not.toHaveLength(0);
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Wed.start} - ${locationDetailsMock.hoursByDay.Wed.end}`
            )
        ).not.toHaveLength(0);
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Thu.start} - ${locationDetailsMock.hoursByDay.Thu.end}`
            )
        ).not.toHaveLength(0);
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Sat.start} - ${locationDetailsMock.hoursByDay.Sat.end}`
            )
        ).not.toHaveLength(0);
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Sun.start} - ${locationDetailsMock.hoursByDay.Sun.end}`
            )
        ).not.toHaveLength(0);
    });

    it('should not render store opening hours if there is no hoursByDay', () => {
        const props = {
            hoursByDay: null,
            timezone: locationDetailsMock.timezone,
        };

        jest.spyOn(require('../../../../../common/helpers/getStoreStatus'), 'getStoreStatus').mockReturnValue({
            statusText: '',
            isClosingSoon: true,
        });

        render(<LocationHours {...(props as ILocationHoursProps)} />);

        expect(screen.queryByText(THourDayOfWeekModel.Monday)).not.toBeInTheDocument();
        expect(
            screen.queryAllByText(
                `${locationDetailsMock.hoursByDay.Mon.start} - ${locationDetailsMock.hoursByDay.Mon.end}`
            )
        ).toHaveLength(0);
    });

    it('should define store status', () => {
        const props = {
            hoursByDay: locationDetailsMock.hoursByDay,
            timezone: locationDetailsMock.timezone,
        };
        const expectedStatus = 'Closed Now - Opens at 11:00 PM';

        jest.spyOn(require('../../../../../common/helpers/getStoreStatus'), 'getStoreStatus').mockReturnValue({
            statusText: expectedStatus,
            isClosingSoon: true,
        });

        render(<LocationHours {...(props as ILocationHoursProps)} />);

        screen.getByText(expectedStatus);
    });
});
