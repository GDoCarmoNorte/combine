import React from 'react';
import { shallow } from 'enzyme';
import productMock from '../productBanner/mocks/productBanner.mock';

import FloatingBanner from '../../../../components/organisms/floatingBanner';
import { InspireButton } from '../../../../components/atoms/button';
import { useProductIsSaleable } from '../../../../common/hooks/useProductIsSaleable';
import { render, screen } from '@testing-library/react';
import { useConfiguration } from '../../../../redux/hooks';

jest.mock('../../../../common/hooks/useLocalization', () => ({
    useLocalization: () => ({
        locationLinkText: '',
        descriptionText: '',
        isProductAvailable: true,
    }),
    useDomainProduct: () => null,
}));

jest.mock('../../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: jest.fn().mockReturnValue({
        isSaleable: true,
    }),
}));

jest.mock('../../../../common/hooks/useProductIsAvailable', () => ({
    useProductIsAvailable: jest.fn().mockReturnValue({ isAvailable: true }),
}));

describe('floatingBanner component', () => {
    it('should not show banner initially', () => {
        const wrapper = shallow(
            <FloatingBanner
                offset={400}
                product={productMock as any}
                productInfo={{
                    price: 100,
                    calories: 100,
                }}
                onAddToBag={jest.fn()}
                addToBagBtnText="Add to bag"
                domainProduct={{}}
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should disable Add To Bag on banner', () => {
        const wrapper = shallow(
            <FloatingBanner
                offset={400}
                product={productMock as any}
                productInfo={{
                    price: 100,
                    calories: 100,
                }}
                onAddToBag={jest.fn()}
                addToBagBtnText="Add to bag"
                domainProduct={{}}
                showInitially
                addToBagBtnDisabled
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot', () => {
        const wrapper = shallow(
            <FloatingBanner
                offset={400}
                product={productMock as any}
                productInfo={{
                    price: 100,
                    calories: 100,
                }}
                modifications={['Curly Fries (S)', 'Coca-Cola® (S)']}
                onAddToBag={jest.fn()}
                addToBagBtnText="Add to bag"
                domainProduct={{}}
                showInitially
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should call onAddToBag in the product banner on click button', () => {
        const onAddToBag = jest.fn();

        const wrapper = shallow(
            <FloatingBanner
                offset={0}
                showInitially
                product={productMock as any}
                productInfo={{
                    price: 100,
                    calories: 100,
                }}
                modifications={['Curly Fries (S)', 'Coca-Cola® (S)']}
                onAddToBag={onAddToBag}
                domainProduct={{}}
                addToBagBtnText="Add to bag"
            />
        );
        wrapper.find(InspireButton).simulate('click');

        expect(onAddToBag).toHaveBeenCalledTimes(1);
    });

    it('should render not saleable button', () => {
        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({ isSaleable: false });

        const wrapper = shallow(
            <FloatingBanner
                offset={0}
                showInitially
                product={productMock as any}
                productInfo={{
                    price: 100,
                    calories: 100,
                }}
                modifications={['Curly Fries (S)', 'Coca-Cola® (S)']}
                onAddToBag={jest.fn()}
                domainProduct={{}}
                addToBagBtnText="Add to bag"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render disabled not saleable button', () => {
        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({ isSaleable: false });
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isOAEnabled: false,
            },
        });

        render(
            <FloatingBanner
                offset={0}
                showInitially
                product={productMock as any}
                productInfo={{
                    price: 100,
                    calories: 100,
                }}
                modifications={['Curly Fries (S)', 'Coca-Cola® (S)']}
                onAddToBag={jest.fn()}
                domainProduct={{}}
                addToBagBtnText="Add to bag"
            />
        );
        const link = screen.getByRole('link', { name: /View menu/i });
        expect(link).toHaveClass('disabled');
    });
});
