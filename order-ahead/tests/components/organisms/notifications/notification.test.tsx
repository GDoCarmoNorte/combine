import React from 'react';
import { shallow } from 'enzyme';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Notification from '../../../../components/organisms/notifications/notification';
import { NotificationType } from '../../../../redux/notifications';

const errorNotificationMock = {
    id: 0,
    title: 'Error Title',
    message: 'Error Message',
    type: NotificationType.ERROR,
};

const successNotificationMock = {
    id: 1,
    title: 'Success Title',
    message: 'Success Message',
    type: NotificationType.SUCCESS,
};

const closeIcon = () => screen.getByLabelText('close');

describe('Notification', () => {
    describe('Error Notification', () => {
        it('should render Notification with props', () => {
            const wrapper = shallow(<Notification {...errorNotificationMock} onCloseClick={jest.fn} />);
            expect(wrapper).toMatchSnapshot();
        });

        it('should handle close icon click', () => {
            const onCloseClickMock = jest.fn();
            render(<Notification {...errorNotificationMock} onCloseClick={onCloseClickMock} />);

            userEvent.click(closeIcon());

            expect(onCloseClickMock).toHaveBeenCalledTimes(1);
        });
    });

    describe('Success Notification', () => {
        it('should render Notification with props', () => {
            const wrapper = shallow(<Notification {...successNotificationMock} onCloseClick={jest.fn} />);
            expect(wrapper).toMatchSnapshot();
        });

        it('should handle close icon click', () => {
            const onCloseClickMock = jest.fn();
            render(<Notification {...successNotificationMock} onCloseClick={onCloseClickMock} />);

            userEvent.click(closeIcon());

            expect(onCloseClickMock).toHaveBeenCalledTimes(1);
        });
    });
});
