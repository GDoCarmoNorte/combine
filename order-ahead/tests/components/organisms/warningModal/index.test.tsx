import React from 'react';
import { render, screen } from '@testing-library/react';

import WarningModal from '../../../../components/organisms/warningModal';
import userEvent from '@testing-library/user-event';

describe('Warning Modal', () => {
    it('should render Modal properly if open props is true', () => {
        render(
            <WarningModal
                description={{
                    title: 'title',
                    message: 'message',
                }}
                open={true}
                primaryButton={{
                    text: 'button text',
                    onClick: jest.fn(),
                }}
            />
        );

        expect(screen.getByText(/title/i)).toBeInTheDocument();
        expect(screen.getByText(/message/i)).toBeInTheDocument();
        expect(screen.getByText(/button text/i)).toBeInTheDocument();
    });

    it('should call onClick if user clicks on button', () => {
        const jestFn = jest.fn();
        render(
            <WarningModal
                description={{
                    title: 'title',
                    message: 'message',
                }}
                open={true}
                primaryButton={{
                    text: 'button text',
                    onClick: jestFn,
                }}
            />
        );

        const btn = screen.getByText(/button text/i);

        userEvent.click(btn);

        expect(jestFn).toBeCalled();
    });
});
