import { LocationPickupAndDeliveryResponseModel } from '../../../../@generated/webExpApi';

export const availableTimeSlotsMock: LocationPickupAndDeliveryResponseModel = {
    pickup: {
        byDay: {
            '2021-10-19': [
                {
                    display: '11:30 AM',
                    utc: new Date('2021-10-19T16:30:00.000Z'),
                },
                {
                    display: '11:45 AM',
                    utc: new Date('2021-10-19T16:45:00.000Z'),
                },
                {
                    display: '12:00 PM',
                    utc: new Date('2021-10-19T17:00:00.000Z'),
                },
            ],
            '2021-10-20': [
                {
                    display: '11:00 AM',
                    utc: new Date('2021-10-20T16:30:00.000Z'),
                },
                {
                    display: '11:15 AM',
                    utc: new Date('2021-10-20T16:45:00.000Z'),
                },
                {
                    display: '11:30 AM',
                    utc: new Date('2021-10-20T17:00:00.000Z'),
                },
            ],
        },
    },
    delivery: {
        byDay: {
            '2021-10-19': [
                {
                    display: '11:30 AM',
                    utc: new Date('2021-10-19T16:30:00.000Z'),
                },
                {
                    display: '11:45 AM',
                    utc: new Date('2021-10-19T16:45:00.000Z'),
                },
                {
                    display: '12:00 PM',
                    utc: new Date('2021-10-19T17:00:00.000Z'),
                },
            ],
        },
    },
};

export const emptyTimeSlotsMock: LocationPickupAndDeliveryResponseModel = {
    pickup: {},
    delivery: {
        byDay: {},
    },
};
