import React from 'react';
import { render, screen, within } from '@testing-library/react';
import restoreAllMocks = jest.restoreAllMocks;
import TimeSlots from '../../../../components/organisms/timeSlots';
import useBag from '../../../../redux/hooks/useBag';

import { resolveOpeningHours } from '../../../../lib/locations';
import useOrderLocation from '../../../../redux/hooks/useOrderLocation';
import { OrderLocationMethod } from '../../../../redux/orderLocation';
import { availableTimeSlotsMock, emptyTimeSlotsMock } from './timeSlots.mock';
import userEvent from '@testing-library/user-event';
import { transformHoursByDay } from '../../../../common/helpers/getStoreStatus';
import { getHoursWithTimezone } from '../../../../lib/locations/resolveOpeningHours';
import { useBagAnalytics } from '../../../../common/hooks/useBagAnalytics';

const setPickupTimeMock = jest.fn();
const setAvailableTimeSlotsMock = jest.fn();

jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useBag');
jest.mock('../../../../lib/locations');
jest.mock('../../../../lib/locations/resolveOpeningHours');
jest.mock('../../../../common/helpers/getStoreStatus');
jest.mock('../../../../common/hooks/useLocationAvailableTimes');

jest.mock('../../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmChangeOrderDate: jest.fn(),
    }),
}));

const CurrentDate = Date.now;

describe('TimeSlots component', () => {
    beforeAll(() => {
        global.Date.now = jest.fn(() => new Date('2021-10-19T08:30:00.000Z').getTime());
    });

    afterAll(() => {
        global.Date.now = CurrentDate;
    });

    beforeEach(() => {
        (useBag as jest.Mock).mockReturnValue({
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            actions: {
                setPickupTime: setPickupTimeMock,
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            currentLocation: {},
        });
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            storeTimezone: 'America/New_York',
            isOpen: true,
        });
        (getHoursWithTimezone as jest.Mock).mockReturnValue({
            storeTimezone: 'America/New_York',
            isOpen: true,
        });
        (transformHoursByDay as jest.Mock).mockReturnValue({});
    });

    afterEach(() => {
        restoreAllMocks();
    });

    describe('pickup time slots', () => {
        beforeEach(() => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupLocationTimeSlots: availableTimeSlotsMock,
                deliveryLocationTimeSlots: availableTimeSlotsMock,
                actions: {
                    setPickupLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                    setDeliveryLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                },
                pickupAddress: {},
                deliveryAddress: {
                    pickUpLocation: {
                        hoursByDay: {
                            Sun: {
                                start: '11:00',
                                end: '23:30',
                            },
                        },
                    },
                },
            } as any);
        });

        test('should render days dropdown and open available days', () => {
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            expect(screen.getByText(/Pickup Date/i)).toBeInTheDocument();
            const pickupDayDropdownButtonComponent = screen.queryAllByRole('button')[0];
            expect(pickupDayDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupDayDropdownButtonComponent);

            const dayDropdownItems = screen.queryAllByRole('menuitem');
            expect(dayDropdownItems.length).toEqual(2);
            expect(within(dayDropdownItems[0]).getByText(/Today/i)).toBeInTheDocument();
            expect(within(dayDropdownItems[1]).getByText('10/20/2021')).toBeInTheDocument();
        });
        test('should render times dropdown and open available time ranges for open store', () => {
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            expect(screen.getAllByText(/Pickup Time/i)[0]).toBeInTheDocument();
            const pickupTimeDropdownButtonComponent = screen.queryAllByRole('button')[1];
            expect(pickupTimeDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupTimeDropdownButtonComponent);

            const timeDropdownItems = screen.queryAllByRole('menuitem');
            expect(timeDropdownItems.length).toEqual(3);
            expect(within(timeDropdownItems[0]).getByText(/ASAP/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[1]).getByText(/11:45 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[2]).getByText(/12:00 PM/i)).toBeInTheDocument();
        });

        test('should render times dropdown and open available delivery time ranges with future type', () => {
            (useBag as jest.Mock).mockReturnValue({
                pickupTime: null,
                pickupTimeType: 'future',
                deliveryTime: null,
                deliveryTimeType: 'asap',
                actions: {
                    setPickupTime: setPickupTimeMock,
                },
            });

            (resolveOpeningHours as jest.Mock).mockReturnValue({
                storeTimezone: 'America/New_York',
                isOpen: false,
            });
            (getHoursWithTimezone as jest.Mock).mockReturnValue({
                storeTimezone: 'America/New_York',
                isOpen: false,
            });

            render(<TimeSlots method={OrderLocationMethod.DELIVERY} />);

            expect(screen.getAllByText(/Delivery Time/i)[0]).toBeInTheDocument();
            const pickupTimeDropdownButtonComponent = screen.queryAllByRole('button')[1];
            expect(pickupTimeDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupTimeDropdownButtonComponent);

            const timeDropdownItems = screen.queryAllByRole('menuitem');
            expect(timeDropdownItems.length).toEqual(3);
            expect(within(timeDropdownItems[0]).getByText(/11:30 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[1]).getByText(/11:45 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[2]).getByText(/12:00 PM/i)).toBeInTheDocument();
        });

        test('should render times dropdown and open available delivery time ranges with future type for opened stores', () => {
            (useBag as jest.Mock).mockReturnValue({
                pickupTime: null,
                pickupTimeType: 'future',
                deliveryTime: null,
                deliveryTimeType: 'asap',
                actions: {
                    setPickupTime: setPickupTimeMock,
                },
            });

            (resolveOpeningHours as jest.Mock).mockReturnValue({
                storeTimezone: 'America/New_York',
                isOpen: true,
            });
            (getHoursWithTimezone as jest.Mock).mockReturnValue({
                storeTimezone: 'America/New_York',
                isOpen: true,
            });

            render(<TimeSlots method={OrderLocationMethod.DELIVERY} />);

            expect(screen.getAllByText(/Delivery Time/i)[0]).toBeInTheDocument();
            const pickupTimeDropdownButtonComponent = screen.queryAllByRole('button')[1];
            expect(pickupTimeDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupTimeDropdownButtonComponent);

            const timeDropdownItems = screen.queryAllByRole('menuitem');
            expect(timeDropdownItems.length).toEqual(4);
            expect(within(timeDropdownItems[0]).getByText(/ASAP/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[1]).getByText(/11:30 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[2]).getByText(/11:45 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[3]).getByText(/12:00 PM/i)).toBeInTheDocument();
        });

        test('should update time slots on day selection', () => {
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            const pickupDayDropdownButtonComponent = screen.queryAllByRole('button')[0];
            userEvent.click(pickupDayDropdownButtonComponent);
            const dayDropdownItems = screen.queryAllByRole('menuitem');
            const dayDropdownItemComponent = within(dayDropdownItems[1]).getByText('10/20/2021');
            userEvent.click(dayDropdownItemComponent);

            const pickupTimeDropdownButtonComponent = screen.queryAllByRole('button')[1];
            expect(pickupTimeDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupTimeDropdownButtonComponent);

            const timeDropdownItems = screen.queryAllByRole('menuitem');
            expect(timeDropdownItems.length).toEqual(3);
            expect(within(timeDropdownItems[0]).getByText(/11:00 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[1]).getByText(/11:15 AM/i)).toBeInTheDocument();
            expect(within(timeDropdownItems[2]).getByText(/11:30 AM/i)).toBeInTheDocument();
        });
        test('should set selected pickup time on time selection', () => {
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            const pickupTimeDropdownButtonComponent = screen.queryAllByRole('button')[1];
            expect(pickupTimeDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupTimeDropdownButtonComponent);

            const timeDropdownItems = screen.queryAllByRole('menuitem');
            const timeDropdownItemComponent = within(timeDropdownItems[2]).getByText(/12:00 PM/i);
            userEvent.click(timeDropdownItemComponent);

            expect(setPickupTimeMock).toHaveBeenCalled();
        });
        test('should preselect previously set Pickup date', () => {
            (useBag as jest.Mock).mockReturnValue({
                pickupTime: '2021-10-20T06:45:00.000Z',
                pickupTimeType: 'future',
                deliveryTime: null,
                deliveryTimeType: 'asap',
                actions: {
                    setPickupTime: setPickupTimeMock,
                },
            });
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            expect(screen.getByText('10/20/2021')).toBeInTheDocument();
        });

        test('should call pushGtmChangeOrderDate when select day', () => {
            const pushGtmMock = jest.fn();
            (useBagAnalytics as jest.Mock).mockReturnValue({
                pushGtmChangeOrderDate: pushGtmMock,
            });

            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            const pickupDayDropdownButtonComponent = screen.queryAllByRole('button')[0];
            userEvent.click(pickupDayDropdownButtonComponent);
            const dayDropdownItems = screen.queryAllByRole('menuitem');
            const timeDropdownItemComponent = within(dayDropdownItems[1]).getByText('10/20/2021');
            userEvent.click(timeDropdownItemComponent);
            expect(pushGtmMock).toHaveBeenCalledWith(OrderLocationMethod.PICKUP, '2021-10-20T16:30:00.000Z');

            expect(pushGtmMock).toHaveBeenCalledTimes(1);
        });

        test('should call pushGtmChangeOrderDate when select a time', () => {
            const pushGtmMock = jest.fn();
            (useBagAnalytics as jest.Mock).mockReturnValue({
                pushGtmChangeOrderDate: pushGtmMock,
            });

            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            const pickupTimeDropdownButtonComponent = screen.queryAllByRole('button')[1];
            userEvent.click(pickupTimeDropdownButtonComponent);

            const timeDropdownItems = screen.queryAllByRole('menuitem');
            const timeDropdownItemComponent = within(timeDropdownItems[2]).getByText(/12:00 PM/i);

            userEvent.click(timeDropdownItemComponent);
            expect(pushGtmMock).toHaveBeenCalledWith(OrderLocationMethod.PICKUP, '2021-10-19T17:00:00.000Z');

            expect(pushGtmMock).toHaveBeenCalledTimes(1);
        });
    });

    describe('delivery time slots', () => {
        beforeEach(() => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.DELIVERY,
                pickupLocationTimeSlots: availableTimeSlotsMock,
                deliveryLocationTimeSlots: availableTimeSlotsMock,
                actions: {
                    setPickupLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                    setDeliveryLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                },
                pickupAddress: {},
                deliveryAddress: {
                    pickUpLocation: {
                        hoursByDay: {
                            Sun: {
                                start: '11:00',
                                end: '23:30',
                            },
                        },
                    },
                },
            } as any);
        });

        test('should render days dropdown and open available days', () => {
            render(<TimeSlots method={OrderLocationMethod.DELIVERY} />);

            expect(screen.getByText(/Delivery Date/i)).toBeInTheDocument();
            const pickupDayDropdownButtonComponent = screen.queryAllByRole('button')[0];
            expect(pickupDayDropdownButtonComponent).toBeInTheDocument();
            userEvent.click(pickupDayDropdownButtonComponent);

            const dayDropdownItems = screen.queryAllByRole('menuitem');
            expect(dayDropdownItems.length).toEqual(1);
            expect(within(dayDropdownItems[0]).getByText(/Today/i)).toBeInTheDocument();
        });
    });

    test('should render unavailable dropdown and switch to pickup message', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.DELIVERY,
            pickupLocationTimeSlots: emptyTimeSlotsMock,
            deliveryLocationTimeSlots: emptyTimeSlotsMock,
            actions: {
                setPickupLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                setDeliveryLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
            },
            pickupAddress: {},
            deliveryAddress: {
                pickUpLocation: {},
            },
        } as any);
        render(<TimeSlots method={OrderLocationMethod.DELIVERY} />);
        expect(screen.getAllByDisplayValue('Unavailable').length).toEqual(2);
        expect(screen.getByTestId('selectPickupMessage')).toBeInTheDocument();
    });

    describe('no time slots', () => {
        test('should not render content', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupLocationTimeSlots: null,
                deliveryLocationTimeSlots: null,
                actions: {
                    setPickupLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                    setDeliveryLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                },
                pickupAddress: {},
                deliveryAddress: {},
            } as any);
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            expect(screen.queryByText(/Pickup Date/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/Pickup Time/i)).not.toBeInTheDocument();
        });

        test('should not render content if time slots response is incorrect', () => {
            (useOrderLocation as jest.Mock).mockReturnValue({
                method: OrderLocationMethod.PICKUP,
                pickupLocationTimeSlots: emptyTimeSlotsMock,
                deliveryLocationTimeSlots: emptyTimeSlotsMock,
                actions: {
                    setPickupLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                    setDeliveryLocationAvailableTimeSlots: setAvailableTimeSlotsMock,
                },
                pickupAddress: {},
                deliveryAddress: {
                    pickUpLocation: {},
                },
            } as any);
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            expect(screen.queryByText(/Pickup Date/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/Pickup Time/i)).not.toBeInTheDocument();
        });

        test('should call setPickupTime if pickUp time is earlier than current time', () => {
            (useBag as jest.Mock).mockReturnValue({
                pickupTime: '2021-10-18T06:45:00.000Z',
                pickupTimeType: 'future',
                deliveryTime: null,
                deliveryTimeType: 'asap',
                actions: {
                    setPickupTime: setPickupTimeMock,
                },
            });
            render(<TimeSlots method={OrderLocationMethod.PICKUP} />);

            expect(setPickupTimeMock).toBeCalledWith({
                time: null,
                asap: true,
                method: OrderLocationMethod.PICKUP,
            });
        });
    });
});
