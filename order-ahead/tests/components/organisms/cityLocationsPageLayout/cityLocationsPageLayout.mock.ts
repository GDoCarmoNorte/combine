import { IStateLocationsResponseModel } from '../../../../@generated/webExpApi/models/IStateLocationsResponseModel';
import { IServiceTypeModel } from '../../../../@generated/webExpApi';

export const cityLocationsPageLayoutMockData: IStateLocationsResponseModel = {
    countryCode: 'US',
    countryName: 'United States',
    stateOrProvince: {
        code: 'de',
        name: 'Delaware',
        count: 6,
        cities: [
            {
                name: 'Bear',
                displayName: 'bear',
                count: 1,
                locations: [
                    {
                        id: '438',
                        displayName: 'Bear store',
                        url: 'us/de/bear/1887-pulaski-hwy/sports-bar-438',
                        contactDetails: {
                            address: {
                                line: '1887 Pulaski Hwy',
                                postalCode: '19701-1731',
                                cityName: 'Bear',
                                stateProvinceCode: 'DE',
                            },
                            phone: '302-832-3900',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=1887%20Pulaski%20Hwy%2C%20Bear%2C%20DE%2C%2019701-1731',
                        services: [IServiceTypeModel.BlazingRewards],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 39.610548,
                            longitude: -75.707431,
                        },
                        timezone: 'America/New_York',
                        isOnlineOrderAvailable: true,
                        isClosed: false,
                    },
                ],
            },
            {
                name: 'Dover',
                displayName: 'dover',
                count: 1,
                locations: [
                    {
                        id: '490',
                        displayName: 'Dover, DE',
                        url: 'us/de/dover/680-south-bay-road/sports-bar-490',
                        contactDetails: {
                            address: {
                                line: '680 South Bay Road',
                                postalCode: '19901-4604',
                                cityName: 'Dover',
                                stateProvinceCode: 'DE',
                            },
                            phone: '302-346-9464',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=680%20South%20Bay%20Road%2C%20Dover%2C%20DE%2C%2019901-4604',
                        services: [IServiceTypeModel.BlazingRewards],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 39.15135,
                            longitude: -75.505836,
                        },
                        timezone: 'America/New_York',
                        isOnlineOrderAvailable: true,
                        isClosed: false,
                    },
                ],
            },
            {
                name: 'Middletown',
                displayName: 'middletown',
                count: 1,
                locations: [
                    {
                        id: '507',
                        displayName: 'Middletown, DE',
                        url: 'us/de/middletown/540-west-main-street/sports-bar-507',
                        contactDetails: {
                            address: {
                                line: '540 West Main Street',
                                postalCode: '19709-1057',
                                cityName: 'Middletown',
                                stateProvinceCode: 'DE',
                            },
                            phone: '302-285-0000',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=540%20West%20Main%20Street%2C%20Middletown%2C%20DE%2C%2019709-1057',
                        services: [IServiceTypeModel.BlazingRewards],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 39.447196,
                            longitude: -75.728159,
                        },
                        timezone: 'America/New_York',
                        isOnlineOrderAvailable: true,
                        isClosed: false,
                    },
                ],
            },
            {
                name: 'Newark',
                displayName: 'newark',
                count: 1,
                locations: [
                    {
                        id: '649',
                        displayName: 'Newark, DE-Elkton Road',
                        url: 'us/de/newark/100-elkton-road/sports-bar-649',
                        contactDetails: {
                            address: {
                                line: '100 Elkton Road',
                                postalCode: '19711-7963',
                                cityName: 'Newark',
                                stateProvinceCode: 'DE',
                            },
                            phone: '302-731-3145',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=100%20Elkton%20Road%2C%20Newark%2C%20DE%2C%2019711-7963',
                        services: [IServiceTypeModel.BlazingRewards],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 39.680513,
                            longitude: -75.759294,
                        },
                        timezone: 'America/New_York',
                        isOnlineOrderAvailable: true,
                        isClosed: false,
                    },
                ],
            },
            {
                name: 'Rehoboth Beach',
                displayName: 'rehoboth-beach',
                count: 1,
                locations: [
                    {
                        id: '1047',
                        displayName: 'Rehoboth Beach, DE',
                        url: 'us/de/rehoboth-beach/19930-lighthouse-plaza-blvd-/sports-bar-1047',
                        contactDetails: {
                            address: {
                                line: '19930 Lighthouse Plaza Blvd.',
                                postalCode: '19971',
                                cityName: 'Rehoboth Beach',
                                stateProvinceCode: 'DE',
                            },
                            phone: '302-727-5946',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=19930%20Lighthouse%20Plaza%20Blvd.%2C%20Rehoboth%20Beach%2C%20DE%2C%2019971',
                        services: [IServiceTypeModel.BlazingRewards],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '23:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '23:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '23:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '23:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 38.721414,
                            longitude: -75.119401,
                        },
                        timezone: 'America/New_York',
                        isOnlineOrderAvailable: true,
                        isClosed: false,
                    },
                ],
            },
            {
                name: 'Wilmington',
                displayName: 'wilmington',
                count: 1,
                locations: [
                    {
                        id: '1118',
                        displayName: 'Wilmington, DE',
                        url: 'us/de/wilmington/2062-limestone-road/sports-bar-1118',
                        contactDetails: {
                            address: {
                                line: '2062 Limestone Road',
                                postalCode: '19808-5506',
                                cityName: 'Wilmington',
                                stateProvinceCode: 'DE',
                            },
                            phone: '302-999-9211',
                        },
                        addressMapLink:
                            'https://www.google.com/maps?hl=en&saddr=current+location&daddr=2062%20Limestone%20Road%2C%20Wilmington%2C%20DE%2C%2019808-5506',
                        services: [],
                        paymentMethods: [],
                        hoursByDay: {
                            Mon: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Tue: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Wed: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Thu: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                            Fri: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sat: {
                                start: '11:00',
                                end: '01:00',
                                isOpen24Hs: false,
                            },
                            Sun: {
                                start: '11:00',
                                end: '00:00',
                                isOpen24Hs: false,
                            },
                        },
                        details: {
                            latitude: 39.722042,
                            longitude: -75.657295,
                        },
                        timezone: 'America/New_York',
                        isOnlineOrderAvailable: true,
                        isClosed: false,
                    },
                ],
            },
        ],
    },
};

export const sectionsMock = [
    {
        metadata: {
            tags: [],
        },
        sys: {
            space: {
                sys: {
                    type: 'Link',
                    linkType: 'Space',
                    id: 'l5fkpck1mwg3',
                },
            },
            id: '7J9sdwm8gU8R19pviNTcGH',
            type: 'Entry',
            createdAt: '2021-09-07T07:15:04.810Z',
            updatedAt: '2021-09-07T07:15:04.810Z',
            environment: {
                sys: {
                    id: 'rio-feature-DBBP-39869-Location-banner',
                    type: 'Link',
                    linkType: 'Environment',
                },
            },
            revision: 1,
            contentType: {
                sys: {
                    type: 'Link',
                    linkType: 'ContentType',
                    id: 'notificationBanner',
                },
            },
            locale: 'en-US',
        },
        fields: {
            text:
                'Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm.',
            icon: {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6pOhYWBq7R3yEO6sfiYRz1',
                    type: 'Asset',
                    createdAt: '2021-07-19T19:31:23.341Z',
                    updatedAt: '2021-08-03T14:00:19.020Z',
                    environment: {
                        sys: {
                            id: 'rio-feature-DBBP-39869-Location-banner',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 4,
                    locale: 'en-US',
                },
                fields: {
                    title: 'Alert icon',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/6pOhYWBq7R3yEO6sfiYRz1/691a897ce1bfb0c3246ff01130fe46b5/bww-logo-mobile.svg',
                        details: {
                            size: 3629,
                            image: {
                                width: 61,
                                height: 61,
                            },
                        },
                        fileName: 'bww-logo-mobile.svg',
                        contentType: 'image/svg+xml',
                    },
                },
            },
        },
    },
];
