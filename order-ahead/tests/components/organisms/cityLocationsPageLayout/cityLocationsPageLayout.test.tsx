import React from 'react';
import { useDispatch } from 'react-redux';
import { render, screen } from '@testing-library/react';
import CityLocationsPageLayout from '../../../../components/organisms/locations/cityLocationsPageLayout';
import { cityLocationsPageLayoutMockData, sectionsMock } from './cityLocationsPageLayout.mock';
import { getLocationById } from '../../../../common/services/locationService';
import { useDomainMenu, useOrderLocation } from '../../../../redux/hooks';
import { useRouter } from 'next/router';

const {
    stateOrProvince: { cities, code, name },
    countryCode,
} = cityLocationsPageLayoutMockData;

const breadcrumbsData = { countryCode, stateOrProvinceCode: code, stateOrProvince: name };
const mockMetaTitle = 'Buffalo wild wings Locations in Alabaster, Texas';

jest.mock('../../../../common/hooks/useBreadcrumbs', () => ({
    useBreadcrumbs: jest.fn().mockReturnValue([]),
}));

jest.mock('react-redux');
jest.mock('../../../../common/services/locationService');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../redux/hooks/useDomainMenu');
jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../redux/hooks/useTallyOrder', () => ({
    __esModule: true,
    default: jest.fn().mockReturnValue({
        setUnavailableTallyItems: jest.fn(),
    }),
}));

describe('CityLocationsPageLayout component', () => {
    const setLocationMock = jest.fn();
    const getMenuMock = jest.fn();
    const routerPushMock = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
        (getLocationById as jest.Mock).mockReturnValue({ id: 1 });
        (useOrderLocation as jest.Mock).mockReturnValue({
            actions: { setPickupLocation: setLocationMock },
            locationEntry: null,
        });
        (useDomainMenu as jest.Mock).mockReturnValue({ actions: { getDomainMenu: getMenuMock } });
        (useRouter as jest.Mock).mockImplementation(() => ({
            push: routerPushMock,
        }));
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should correct render', () => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <CityLocationsPageLayout
                cityLocations={cities[0]}
                sections={sectionsMock as any}
                breadcrumbsData={breadcrumbsData}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(screen.getByText(/1 bear location/i)).toBeInTheDocument();
        expect(screen.getByText(/Bear store/i)).toBeInTheDocument();
        expect(screen.getByText(/1887 Pulaski Hwy/i)).toBeInTheDocument();
        expect(screen.getByText(mockMetaTitle)).toBeInTheDocument();
    });

    it('should render correct header if location count more than one', () => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());

        render(
            <CityLocationsPageLayout
                cityLocations={{ ...cities[0], count: 2 }}
                sections={sectionsMock as any}
                breadcrumbsData={breadcrumbsData}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(screen.getByText(/2 bear locations/i)).toBeInTheDocument();
    });

    it('should show notification banner if provided', () => {
        render(
            <CityLocationsPageLayout
                cityLocations={cities[0]}
                sections={sectionsMock as any}
                breadcrumbsData={breadcrumbsData}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(
            screen.getByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).toBeInTheDocument();
    });
    it('should not show notification banner if no sections provided', () => {
        render(
            <CityLocationsPageLayout
                cityLocations={cities[0]}
                breadcrumbsData={breadcrumbsData}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(
            screen.queryByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).not.toBeInTheDocument();
    });
});
