import React from 'react';
import { render, screen } from '@testing-library/react';

import MyTeams from '../../../../components/organisms/myTeams/myTeams';
import useSurvey from '../../../../common/hooks/useSurvey';
import { initialState } from '../../../../redux/myTeams';

jest.mock('../../../../common/hooks/useSurvey');

describe('MyTeams component', () => {
    beforeEach(() => {
        (useSurvey as jest.Mock).mockReturnValue({
            myTeams: {
                ...initialState,
                hasResponded: true,
            },
            actions: {
                updateSurveys: jest.fn(),
            },
        });
    });

    it('should render component with text', () => {
        render(<MyTeams />);

        expect(screen.getByText(/Tell us your teams for 50 points/i)).toBeInTheDocument();
        expect(screen.getByText(/Add teams, get points. It’s that easy./i)).toBeInTheDocument();
    });

    it('should render loader if loading is true', () => {
        (useSurvey as jest.Mock).mockReturnValue({
            myTeams: {
                ...initialState,
                loading: true,
                hasResponded: true,
            },
            isLoading: true,
            actions: {
                updateSurveys: jest.fn(),
            },
        });
        render(<MyTeams />);

        expect(screen.getByRole(/progressbar/i)).toBeInTheDocument();
    });
});
