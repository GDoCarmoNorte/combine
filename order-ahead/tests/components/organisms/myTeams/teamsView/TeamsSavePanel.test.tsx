import React from 'react';
import { render, screen } from '@testing-library/react';

import TeamsSavePanel from '../../../../../components/organisms/myTeams/teamsView/TeamsSavePanel';

describe('MyTeams component', () => {
    beforeAll(() => {
        const container = document.createElement('div');
        container.setAttribute('id', 'my-teams-portal');
        document.body.appendChild(container);
    });
    it('should render component with text', () => {
        render(
            <TeamsSavePanel
                onSaveTeams={jest.fn()}
                selectedTeams={[
                    {
                        questionId: 'id',
                        id: 'answerid',
                        text: 'Arizona Cardinals',
                    },
                ]}
                categoryText="NFL"
                loading={false}
            />
        );

        expect(screen.getByText(/NFL/i)).toBeInTheDocument();
        expect(screen.getByText(/Arizona Cardinals/i)).toBeInTheDocument();
        expect(screen.getByText(/save teams/i)).toBeInTheDocument();
    });

    it('Should disabled button when loading is true', () => {
        render(
            <TeamsSavePanel
                onSaveTeams={jest.fn()}
                selectedTeams={[
                    {
                        questionId: 'id',
                        id: 'answerid',
                        text: 'Arizona Cardinals',
                    },
                ]}
                categoryText="NFL"
                loading={true}
            />
        );

        expect(screen.queryByText(/save teams/i)).toBeDisabled();
    });
});
