import React from 'react';
import { render, screen } from '@testing-library/react';

import TeamsView from '../../../../../components/organisms/myTeams/teamsView';
import useSurvey from '../../../../../common/hooks/useSurvey';
import { initialState } from '../../../../../redux/myTeams';
import userEvent from '@testing-library/user-event';

jest.mock('../../../../../common/hooks/useSurvey');
jest.mock('next/router', () => ({
    useRouter: jest.fn(() => ({
        push: jest.fn(),
    })),
}));

const updateSurveys = jest.fn().mockImplementation(() => Promise.resolve());
const survey = {
    myTeams: {
        ...initialState,
        questions: [
            {
                id: 'id',
                text: 'NFL:',
                answers: [
                    {
                        id: 'answerid',
                        text: 'Arizona Cardinals',
                    },
                    {
                        id: 'answerid2',
                        text: 'Baltimore Ravens',
                    },
                ],
            },
        ],
        replies: [],
        hasResponded: true,
    },
    isLoading: false,
    actions: {
        updateSurveys,
    },
};
const surveyWithReplies = {
    myTeams: {
        ...initialState,
        questions: [
            {
                id: 'id',
                text: 'NFL:',
                answers: [
                    {
                        id: 'answerid',
                        text: 'Arizona Cardinals',
                    },
                    {
                        id: 'answerid2',
                        text: 'Baltimore Ravens',
                    },
                ],
            },
        ],
        replies: [
            {
                questionId: 'id',
                id: 'answerid',
                text: 'Arizona Cardinals',
            },
            {
                questionId: 'id3',
                id: 'answerid2',
                text: 'Arizona',
            },
        ],
        hasResponded: true,
    },
    isLoading: false,
    actions: {
        updateSurveys,
    },
};

describe('MyTeams component', () => {
    beforeAll(() => {
        const container = document.createElement('div');
        container.setAttribute('id', 'my-teams-portal');
        document.body.appendChild(container);
    });
    beforeEach(() => {
        (useSurvey as jest.Mock).mockReturnValue(survey);
    });

    it('should render component title and list with text', () => {
        render(<TeamsView teamId="id" />);

        expect(screen.getByRole('heading', { name: /NFL/i })).toBeInTheDocument();

        expect(screen.getByText(/back to add teams/i)).toBeInTheDocument();
        expect(screen.getByRole('button', { name: /Arizona Cardinals/i })).toBeInTheDocument();
        expect(screen.getByText(/Baltimore Ravens/i)).toBeInTheDocument();
    });

    it('should render component with save button when there is a selected teams', () => {
        (useSurvey as jest.Mock).mockReturnValue(surveyWithReplies);
        render(<TeamsView teamId="id" />);

        expect(screen.getByRole('button', { name: /save teams/i })).toBeInTheDocument();
    });

    it('should disappeared special modal when user select team', () => {
        (useSurvey as jest.Mock).mockReturnValue(surveyWithReplies);
        render(<TeamsView teamId="id" />);

        const saveButton = screen.getByText(/save teams/i);
        const selectTeamButton = screen.getByRole('button', { name: /Arizona Cardinals/i });
        expect(selectTeamButton).toBeInTheDocument();

        userEvent.click(selectTeamButton);
        expect(saveButton).not.toBeInTheDocument();
    });

    it('should save selected teams', () => {
        (useSurvey as jest.Mock).mockReturnValue(surveyWithReplies);
        const saveButton = () => screen.queryByText(/save teams/i);

        render(<TeamsView teamId="id" />);

        const selectTeamButton = screen.getByRole('button', { name: /Baltimore Ravens/i });
        userEvent.click(selectTeamButton);
        userEvent.click(saveButton());

        expect(saveButton()).toBeDisabled();
        expect(updateSurveys).toHaveBeenCalledWith({
            surveyId: '',
            surveyRespond: [
                { questionId: 'id', answerId: 'answerid' },
                { questionId: 'id', answerId: 'answerid2' },
                { questionId: 'id3', answerId: 'answerid2' },
            ],
        });
    });

    it('should return null', () => {
        render(<TeamsView teamId="" />);
        expect(screen.queryByRole('heading', { name: /NFL/i })).not.toBeInTheDocument();
    });
});
