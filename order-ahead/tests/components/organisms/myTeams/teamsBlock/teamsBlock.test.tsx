import React from 'react';

import AddTeamsBlock from '../../../../../components/organisms/myTeams/teamsBlock';
import { render, screen } from '../../../../utils';
import { ISurveyModel } from '../../../../../@generated/webExpApi';

describe('AddTeamsBlock component', () => {
    it('should render nothing if myTeamsSurvey.questions is empty array', () => {
        render(<AddTeamsBlock myTeamsSurvey={{ questions: [] } as ISurveyModel} />);

        expect(screen.queryByText(/add teams/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/select team/i)).not.toBeInTheDocument();
    });

    it('should render my teams block if myTeamsSurvey.questions is filled in', () => {
        render(<AddTeamsBlock myTeamsSurvey={{ questions: [{ id: '1', text: 'question1:' }] } as ISurveyModel} />);

        expect(screen.getByText(/add teams/i)).toBeInTheDocument();
        expect(screen.getByText(/select team/i)).toBeInTheDocument();
        expect(screen.getByText(/question1/i)).toBeInTheDocument();
    });

    it('should render my teams block with correct links if myTeamsSurvey.questions is filled in', () => {
        render(<AddTeamsBlock myTeamsSurvey={{ questions: [{ id: '1', text: 'question1' }] } as ISurveyModel} />);

        expect(screen.getByRole('link', { name: /select team/i })).toHaveAttribute('href', '/account/my-teams?team=1');
    });
});
