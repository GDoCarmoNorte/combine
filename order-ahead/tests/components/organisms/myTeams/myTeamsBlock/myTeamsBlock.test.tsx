import React from 'react';
import { render, screen } from '../../../../utils';
import userEvent from '@testing-library/user-event';

import MyTeamsBlock from '../../../../../components/organisms/myTeams/myTeamsBlock';
import { ISurveyModel } from '../../../../../@generated/webExpApi';

describe('MyTeamsBlock component', () => {
    it('should render nothing if myTeamsSurvey.replies is empty array', () => {
        render(<MyTeamsBlock myTeamsSurvey={{ replies: [] } as ISurveyModel} updateTeamsSurvey={jest.fn} />);

        expect(screen.queryByText(/my teams/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/remove/i)).not.toBeInTheDocument();
    });

    it('should render my teams block if myTeamsSurvey.replies is filled in', () => {
        render(
            <MyTeamsBlock
                myTeamsSurvey={{ replies: [{ id: '1', questionText: 'question1:' }] } as ISurveyModel}
                updateTeamsSurvey={jest.fn}
            />
        );

        expect(screen.getByText(/my teams/i)).toBeInTheDocument();
        expect(screen.getByText(/remove/i)).toBeInTheDocument();
        expect(screen.getByText(/question1/i)).toBeInTheDocument();
    });

    it('should render view all button if myTeamsSurvey.replies length are more than 6', () => {
        render(
            <MyTeamsBlock
                myTeamsSurvey={
                    {
                        replies: [
                            { id: '1', questionText: 'question1:' },
                            { id: '2', questionText: 'question2:' },
                            { id: '3', questionText: 'question3:' },
                            { id: '4', questionText: 'question4:' },
                            { id: '5', questionText: 'question5:' },
                            { id: '6', questionText: 'question6:' },
                            { id: '7', questionText: 'question7:' },
                        ],
                    } as ISurveyModel
                }
                updateTeamsSurvey={jest.fn}
            />
        );

        expect(screen.getByText(/view all/i)).toBeInTheDocument();
    });

    it('should not render view all button if myTeamsSurvey.replies length are equals 6', () => {
        render(
            <MyTeamsBlock
                myTeamsSurvey={
                    {
                        replies: [
                            { id: '1', questionText: 'question1:' },
                            { id: '2', questionText: 'question2:' },
                            { id: '3', questionText: 'question3:' },
                            { id: '4', questionText: 'question4:' },
                            { id: '5', questionText: 'question5:' },
                            { id: '6', questionText: 'question6:' },
                        ],
                    } as ISurveyModel
                }
                updateTeamsSurvey={jest.fn}
            />
        );

        expect(screen.queryByText(/view all/i)).not.toBeInTheDocument();
    });

    it('should not render view all button if myTeamsSurvey.replies length are less than 6', () => {
        render(
            <MyTeamsBlock
                myTeamsSurvey={
                    {
                        replies: [
                            { id: '1', questionText: 'question1:' },
                            { id: '2', questionText: 'question2:' },
                            { id: '3', questionText: 'question3:' },
                            { id: '4', questionText: 'question4:' },
                            { id: '5', questionText: 'question5:' },
                        ],
                    } as ISurveyModel
                }
                updateTeamsSurvey={jest.fn}
            />
        );

        expect(screen.queryByText(/view all/i)).not.toBeInTheDocument();
    });

    it('should render banner if if there are no added teams', () => {
        render(<MyTeamsBlock myTeamsSurvey={{ replies: [] } as ISurveyModel} updateTeamsSurvey={jest.fn} />);

        expect(screen.getByText('Add teams, get points. It’s that easy.')).toBeInTheDocument();
    });

    it('should call setViewAll after click on viewAll btn', () => {
        const setViewAllMock = jest.fn();
        const useStateMock = jest.spyOn(React, 'useState').mockReturnValueOnce([false, setViewAllMock]);
        render(
            <MyTeamsBlock
                myTeamsSurvey={
                    {
                        replies: [
                            { id: '1', questionText: 'question1:' },
                            { id: '2', questionText: 'question2:' },
                            { id: '3', questionText: 'question3:' },
                            { id: '4', questionText: 'question4:' },
                            { id: '5', questionText: 'question5:' },
                            { id: '6', questionText: 'question6:' },
                            { id: '7', questionText: 'question7:' },
                        ],
                    } as ISurveyModel
                }
                updateTeamsSurvey={jest.fn()}
            />
        );

        const viewAllBtn = screen.getByText(/view all/i);
        userEvent.click(viewAllBtn);
        expect(setViewAllMock).toBeCalledWith(true);

        useStateMock.mockRestore();
    });

    it('should call update teams survey after click on remove btn', () => {
        const updateTeamsSurveyMock = jest.fn();
        render(
            <MyTeamsBlock
                myTeamsSurvey={
                    {
                        id: 'surveyId',
                        replies: [
                            { id: '1', questionText: 'question1:', questionId: '1' },
                            { id: '2', questionText: 'question2:', questionId: '2' },
                        ],
                    } as ISurveyModel
                }
                updateTeamsSurvey={updateTeamsSurveyMock}
            />
        );

        const removeBtn = screen.getAllByText(/remove/i)[0];
        userEvent.click(removeBtn);
        expect(updateTeamsSurveyMock).toBeCalledWith({
            surveyId: 'surveyId',
            surveyRespond: [{ questionId: '2', answerId: '2' }],
            typeSurvey: 'delete',
        });
    });
});
