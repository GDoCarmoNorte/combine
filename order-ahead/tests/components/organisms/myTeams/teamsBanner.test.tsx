import React from 'react';
import { render, screen } from '@testing-library/react';

import TeamsBanner from '../../../../components/organisms/myTeams/teamsBanner';

describe('TeamsBanner', () => {
    it('should render component with text', () => {
        render(<TeamsBanner />);

        expect(screen.getByText(/Tell us your teams for 50 points/i)).toBeInTheDocument();
        expect(screen.getByText(/Add teams, get points. It’s that easy./i)).toBeInTheDocument();
    });
});
