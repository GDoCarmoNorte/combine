export const tapListMenuCategory = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '6N94hTKyLE6MJqdV4ac0Ud',
        type: 'Entry',
        createdAt: '2021-10-04T07:25:29.147Z',
        updatedAt: '2021-10-04T08:20:17.870Z',
        environment: {
            sys: {
                id: 'feature-feature-DBBP-41917',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 2,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'tapListMenuCategory',
            },
        },
        locale: 'en-US',
    },
    fields: {
        categoryName: 'Local Tap List',
        image: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '5svnw4v26IC4FgquOBaT1p',
                type: 'Asset',
                createdAt: '2021-09-28T18:11:56.418Z',
                updatedAt: '2021-09-28T18:11:56.418Z',
                environment: {
                    sys: {
                        id: 'feature-feature-DBBP-41917',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'reward icon 512w',
                description: '',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/5svnw4v26IC4FgquOBaT1p/856296b420cb1cb38b83bdc9e181b427/icon-reward-512w.png',
                    details: {
                        size: 7125,
                        image: {
                            width: 512,
                            height: 512,
                        },
                    },
                    fileName: 'icon-reward-512w.png',
                    contentType: 'image/png',
                },
            },
        },
        sections: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '45fscAHryaI0eV6yDAl0gG',
                    type: 'Entry',
                    createdAt: '2021-10-04T08:20:14.385Z',
                    updatedAt: '2021-10-04T08:20:14.385Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-41917',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'localTapList',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    title: 'Local Tap List',
                    image: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '5ItEcxXpfZsGAiiAREKZNy',
                            type: 'Asset',
                            createdAt: '2021-10-04T08:20:06.154Z',
                            updatedAt: '2021-10-04T08:20:06.154Z',
                            environment: {
                                sys: {
                                    id: 'feature-feature-DBBP-41917',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'tap list',
                            description: 'asdasd',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/5ItEcxXpfZsGAiiAREKZNy/a90fca214d0505be5bad88755fb9f700/beer2_1.png',
                                details: {
                                    size: 272512,
                                    image: {
                                        width: 499,
                                        height: 363,
                                    },
                                },
                                fileName: 'beer2 1.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    description: 'cvxcvx sfsfd',
                },
            },
        ],
        metaDescription: 'Description',
        link: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '40CMWUr6c2DNfBnweFUkaI',
                type: 'Entry',
                createdAt: '2021-10-04T07:23:14.377Z',
                updatedAt: '2021-10-04T07:23:14.377Z',
                environment: {
                    sys: {
                        id: 'feature-feature-DBBP-41917',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'menuCategoryLink',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'Local Tap List',
                nameInUrl: 'local-tap-list',
            },
        },
    },
};
