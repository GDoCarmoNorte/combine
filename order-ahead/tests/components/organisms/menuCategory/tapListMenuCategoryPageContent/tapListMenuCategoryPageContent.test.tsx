import React from 'react';
import { render, screen } from '@testing-library/react';
import TapListMenuCategoryPageContent from '../../../../../components/organisms/menuCategory/tapListMenuCategoryPageContent';
import { tapListMenuCategory } from './tapListMenuCategoryPageContent.mock';

jest.mock('../../../../../redux/hooks/useLocalTapList', () => jest.fn().mockReturnValue({ list: {}, available: true }));
jest.mock('../../../../../components/atoms/Breadcrumbs', () => () => 'Breadcrumbs');
jest.mock('../../../../../components/atoms/categorySelect', () => () => 'Category Select');
jest.mock('../../../../../redux/hooks', () => ({
    useDomainMenu: () => ({
        actions: {
            getAvailableCategories: jest.fn().mockReturnValue([]),
        },
    }),
}));

describe('tapListMenuCategoryPageContent', () => {
    it('Should render tap list page content', () => {
        render(
            <TapListMenuCategoryPageContent
                tapListMenuCategory={tapListMenuCategory as any}
                menuCategories={[tapListMenuCategory as any]}
            />
        );

        expect(screen.getByText(/Breadcrumbs/i)).toBeInTheDocument();
        expect(screen.getByText(/Category Select/i)).toBeInTheDocument();
        expect(screen.getByText(tapListMenuCategory.fields.sections[0].fields.title)).toBeInTheDocument();
    });

    it('should show loader if sections are loading', () => {
        render(
            <TapListMenuCategoryPageContent
                tapListMenuCategory={tapListMenuCategory as any}
                menuCategories={[tapListMenuCategory as any]}
                areSectionsloading
            />
        );

        screen.getByRole('progressbar');
        expect(screen.queryByText(tapListMenuCategory.fields.sections[0].fields.title)).not.toBeInTheDocument();
    });
});
