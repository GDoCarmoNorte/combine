import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import CityLocationTile from '../../../../components/organisms/locations/cityLocationTile';
import * as constants from '../../../../components/organisms/locations/cityLocationTile/constants';
import cityLocationTileMockData from './cityLocationTile.mock';
import { getLocationById } from '../../../../common/services/locationService';
import { useDomainMenu, useOrderLocation, useTallyOrder } from '../../../../redux/hooks';
import { useRouter } from 'next/router';
import userEvent from '@testing-library/user-event';
import { useLocationUnavailableError } from '../../../../common/hooks/useLocationUnavailableError';

jest.mock('react-redux');
jest.mock('../../../../common/services/locationService');
jest.mock('../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../redux/hooks/useGlobalProps');
jest.mock('../../../../redux/hooks/useDomainMenu');
jest.mock('../../../../redux/hooks/useTallyOrder');

jest.mock('../../../../common/hooks/useLocationUnavailableError', () => ({
    useLocationUnavailableError: jest.fn().mockReturnValue({
        pushLocationUnavailableError: jest.fn(),
    }),
}));

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../redux/hooks/useBag', () => ({
    __esModule: true,
    default: jest.fn().mockReturnValue({
        bagEntries: [],
    }),
}));

describe('CityLocationTile component', () => {
    const setLocationMock = jest.fn();
    const getMenuMock = jest.fn();
    const routerPushMock = jest.fn();
    // @ts-ignore
    // eslint-disable-next-line
    constants.UNAVAILABLE_MESSAGE = 'Restaurant hours currently unavailable';
    beforeEach(() => {
        (getLocationById as jest.Mock).mockReturnValue({ id: 1 });
        (useOrderLocation as jest.Mock).mockReturnValue({
            actions: { setPickupLocation: setLocationMock },
            locationEntry: null,
        });
        (useDomainMenu as jest.Mock).mockReturnValue({ actions: { getDomainMenu: getMenuMock } });
        (useTallyOrder as jest.Mock).mockReturnValue({ setUnavailableTallyItems: jest.fn() });
        (useRouter as jest.Mock).mockImplementation(() => ({
            push: routerPushMock,
        }));
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should correct render', () => {
        render(<CityLocationTile location={cityLocationTileMockData} />);

        expect(screen.getByText(/Gahanna store/i)).toBeInTheDocument();
        expect(screen.getByText(/Store ID/i)).toBeInTheDocument();
        expect(screen.getByText(/43230-6771/i)).toBeInTheDocument();
        expect(screen.getByText(/OH/i)).toBeInTheDocument();
        expect(screen.getByText(/1380 Cherry Bottom Rd. Village Square at Cherry Bottom/i)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: /Order/i })).toBeInTheDocument();
    });

    it('should render "VIEW MENU" button if store is closed', () => {
        render(<CityLocationTile location={{ ...cityLocationTileMockData, isClosed: true }} />);

        expect(screen.getByRole('button', { name: /View menu/i })).toBeInTheDocument();
        expect(screen.queryByRole('link', { name: /Order/i })).not.toBeInTheDocument();
    });

    it('should render "VIEW MENU" button if online ordering is not available', () => {
        render(<CityLocationTile location={{ ...cityLocationTileMockData, isOnlineOrderAvailable: false }} />);

        expect(screen.getByRole('button', { name: /View menu/i })).toBeInTheDocument();
        expect(screen.queryByRole('link', { name: /Order/i })).not.toBeInTheDocument();
    });
    it('should not render location status if location hours are not provided', () => {
        const cityLocationWithoutHoursTileMockData = {
            ...cityLocationTileMockData,
            hoursByDay: {},
        };
        render(
            <CityLocationTile location={{ ...cityLocationWithoutHoursTileMockData, isOnlineOrderAvailable: false }} />
        );

        expect(screen.queryByText(/Closed/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Open/i)).not.toBeInTheDocument();
    });
    it('should render phone number link if provided', () => {
        render(<CityLocationTile location={{ ...cityLocationTileMockData, isOnlineOrderAvailable: false }} />);

        expect(screen.getByRole('link', { name: '(614) 478-7972' })).toBeInTheDocument();
    });
    it('should not render phone number link if pnone is not provided', () => {
        const cityLocationWithoutPhoneTileMockData = {
            ...cityLocationTileMockData,
            contactDetails: {
                ...cityLocationTileMockData.contactDetails,
                phone: undefined,
            },
        };
        render(
            <CityLocationTile location={{ ...cityLocationWithoutPhoneTileMockData, isOnlineOrderAvailable: false }} />
        );

        expect(screen.queryByRole('link', { name: '(614) 478-7972' })).not.toBeInTheDocument();
    });

    it('Should display message if the SHOW_UNAVAILABLE IS TRUE', () => {
        // @ts-ignore
        // eslint-disable-next-line
        constants.SHOW_UNAVAILABLE = true;
        const cityLocationWithoutHoursTileMockData = {
            ...cityLocationTileMockData,
            hoursByDay: null,
        };
        render(
            <CityLocationTile location={{ ...cityLocationWithoutHoursTileMockData, isOnlineOrderAvailable: false }} />
        );

        expect(screen.getByText(/Restaurant hours currently unavailable/i)).toBeInTheDocument();
    });

    it('Should not display message if the SHOW_UNAVAILABLE IS FALSE', () => {
        // @ts-ignore
        // eslint-disable-next-line
        constants.SHOW_UNAVAILABLE = false;
        const cityLocationWithoutHoursTileMockData = {
            ...cityLocationTileMockData,
            hoursByDay: null,
        };
        render(
            <CityLocationTile location={{ ...cityLocationWithoutHoursTileMockData, isOnlineOrderAvailable: false }} />
        );

        expect(screen.queryByText(/Restaurant hours currently unavailable/i)).toBeNull();
    });

    it('should call pushLocationUnavailableError when isDigitlEnabled false', async () => {
        const pushLocationUnavailableErrorMock = jest.fn();
        const locationMockData = {
            id: 1,
            isDigitallyEnabled: false,
            contactDetails: {
                phone: '777-77-7777',
            },
        };
        (getLocationById as jest.Mock).mockReturnValue(locationMockData);
        (useLocationUnavailableError as jest.Mock).mockReturnValueOnce({
            pushLocationUnavailableError: pushLocationUnavailableErrorMock,
        });

        render(<CityLocationTile location={{ ...cityLocationTileMockData, isOnlineOrderAvailable: false }} />);
        const btn = screen.getByRole('button', { name: /View menu/i });
        userEvent.click(btn);

        await waitFor(() => expect(pushLocationUnavailableErrorMock).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(pushLocationUnavailableErrorMock).toHaveBeenCalledWith(locationMockData));
    });
});
