import { IServiceTypeModel, ILocationServiceHoursModel } from '../../../../@generated/webExpApi';

interface ILocationContactDetails {
    address: {
        line?: string;
        postalCode?: string;
        cityName?: string;
        stateProvinceCode?: string;
    };
    phone?: string;
}

export interface ICityLocationTileProps {
    id?: string;
    displayName: string;
    url?: string;
    contactDetails: ILocationContactDetails;
    addressMapLink?: string;
    services?: Array<IServiceTypeModel>;
    paymentMethods?: Array<any>;
    hoursByDay?: { [key: string]: ILocationServiceHoursModel };
    details: {
        latitude: number;
        longitude: number;
    };
    timezone: string;
    isOnlineOrderAvailable: boolean;
    isClosed: boolean;
}

const cityLocationTileMockData: ICityLocationTileProps = {
    id: '12',
    displayName: 'Gahanna store',
    url: 'us/oh/gahanna/1380-cherry-bottom-rd--village-square-at-cherry-bottom/sports-bar-12',
    contactDetails: {
        address: {
            line: '1380 Cherry Bottom Rd. Village Square at Cherry Bottom',
            postalCode: '43230-6771',
            cityName: 'Gahanna',
            stateProvinceCode: 'OH',
        },
        phone: '614-478-7972',
    },
    addressMapLink:
        'https://www.google.com/maps?hl=en&saddr=current+location&daddr=1380%20Cherry%20Bottom%20Rd.%20Village%20Square%20at%20Cherry%20Bottom%2C%20Gahanna%2C%20OH%2C%2043230-6771',
    services: [IServiceTypeModel.BlazingRewards, IServiceTypeModel.Delivery],
    paymentMethods: [],
    hoursByDay: {
        Mon: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Tue: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Wed: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Thu: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Fri: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Sat: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Sun: {
            start: '11:00',
            end: '23:00',
            isOpen24Hs: false,
        },
    },
    details: {
        latitude: 40.055344,
        longitude: -82.884732,
    },
    timezone: 'America/New_York',
    isOnlineOrderAvailable: true,
    isClosed: false,
};

export default cityLocationTileMockData;
