import React from 'react';
import { render, screen } from '@testing-library/react';
import { allLocationsMock } from './allLocationsPageContent.mock';
import CityLocationsContainer from '../../../../components/organisms/locations/countryLocationsContainer/stateLocationsContainer/cityLocationsContainer';

const countryMock = allLocationsMock.locationsByCountry[0];
const countryMockMock = countryMock.countryCode;
const stateMock = countryMock.statesOrProvinces[0];
const stateCodeMock = stateMock.code;
const cityMock = stateMock.cities[0];

describe('CityLocationsContainer component', () => {
    it('should contain link to city page', () => {
        render(
            <CityLocationsContainer
                cityLocationDetails={cityMock}
                stateCode={stateCodeMock}
                countryCode={countryMockMock}
            />
        );

        expect(screen.getByRole('link')).toHaveAttribute(
            'href',
            `/locations/${countryMockMock}/${stateCodeMock}/${cityMock.displayName}`.toLowerCase()
        );
        expect(screen.getByRole('link')).toHaveTextContent(cityMock.name);
    });
});
