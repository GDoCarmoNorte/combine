import React from 'react';
import { render, screen } from '@testing-library/react';
import { allLocationsMock } from './allLocationsPageContent.mock';
import CountryLocationsContainer from '../../../../components/organisms/locations/countryLocationsContainer';

const countryMock = allLocationsMock.locationsByCountry[0];

describe('CountryLocationsContainer component', () => {
    it('should contain name of the state', () => {
        render(<CountryLocationsContainer locationDetails={countryMock} />);

        expect(screen.getByText(countryMock.countryName)).toHaveClass('title');
    });
    it('should contain number of country locations', () => {
        render(<CountryLocationsContainer locationDetails={countryMock} />);

        expect(screen.getByText(`${countryMock.count.toLocaleString()} locations`)).toHaveClass('details');
    });
    it('should contain number of country locations with correct title', () => {
        const countryWithOneLocationMock = {
            ...countryMock,
            count: 1,
        };
        render(<CountryLocationsContainer locationDetails={countryWithOneLocationMock} />);

        expect(screen.getByText(`${countryWithOneLocationMock.count.toLocaleString()} location`)).toHaveClass(
            'details'
        );
    });
});
