import React from 'react';
import { render, screen } from '@testing-library/react';
import { allLocationsMock } from './allLocationsPageContent.mock';
import userEvent from '@testing-library/user-event';
import StateLocationsContainer from '../../../../components/organisms/locations/countryLocationsContainer/stateLocationsContainer';
import isMobileScreen from '../../../../lib/isMobileScreen';
import isSmallScreen from '../../../../lib/isSmallScreen';

const countryMock = allLocationsMock.locationsByCountry[0];
const countryMockMock = countryMock.countryCode;
const stateMock = countryMock.statesOrProvinces[0];

jest.mock('../../../../lib/isMobileScreen', () => jest.fn().mockReturnValue(false));
jest.mock('../../../../lib/isSmallScreen', () => jest.fn().mockReturnValue(false));

describe('StateLocationsContainer component', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should contain name of the state as a link', () => {
        render(<StateLocationsContainer stateLocationDetails={stateMock} countryCode={countryMockMock} />);

        const linkComponent = screen.getByLabelText(`${stateMock.name} location`);
        expect(linkComponent).toHaveAttribute('href', `/locations/${countryMockMock}/${stateMock.code}`.toLowerCase());
        expect(linkComponent).toHaveTextContent(stateMock.name);
    });
    it('should contain number of state locations', () => {
        render(<StateLocationsContainer stateLocationDetails={stateMock} countryCode={countryMockMock} />);

        expect(screen.getByText(`${stateMock.count.toLocaleString()} locations`)).toHaveClass('details');
    });
    it('should expand accordion if state selected', () => {
        render(
            <StateLocationsContainer
                stateLocationDetails={stateMock}
                countryCode={countryMockMock}
                selectedStateOrProvinceCode={stateMock.code}
            />
        );

        expect(screen.getByLabelText(/Collapse/i)).toBeInTheDocument();
    });
    it('should contain number of state locations with correct title', () => {
        const stateWithOneLocationMock = {
            ...stateMock,
            count: 1,
        };
        render(
            <StateLocationsContainer stateLocationDetails={stateWithOneLocationMock} countryCode={countryMockMock} />
        );

        expect(screen.getByText(`${stateWithOneLocationMock.count.toLocaleString()} location`)).toHaveClass('details');
    });
    it('should have expanded accordion if clicked', async () => {
        render(<StateLocationsContainer stateLocationDetails={stateMock} countryCode={countryMockMock} />);
        const accordionComponent = screen.getByLabelText(/Expand/i);

        userEvent.click(accordionComponent);
        expect(screen.getByLabelText(/Collapse/i)).toBeInTheDocument();
    });
    it('should use correct size of accordion icon if mobile screen', () => {
        (isMobileScreen as jest.Mock).mockReturnValue(true);
        render(<StateLocationsContainer stateLocationDetails={stateMock} countryCode={countryMockMock} />);
        expect(screen.getByLabelText(/Expand/i)).toHaveClass('s');
    });

    it('should use correct size of accordion icon if desktop screen', () => {
        (isMobileScreen as jest.Mock).mockReturnValue(false);
        (isSmallScreen as jest.Mock).mockReturnValue(false);
        render(<StateLocationsContainer stateLocationDetails={stateMock} countryCode={countryMockMock} />);
        expect(screen.getByLabelText(/Expand/i)).toHaveClass('m');
    });

    it('should expand/collapse accordion on click', async () => {
        render(<StateLocationsContainer stateLocationDetails={stateMock} countryCode={countryMockMock} />);
        const icon = screen.getByLabelText(/Expand/i);
        userEvent.click(icon);

        expect(icon).toHaveClass('direction-up');

        userEvent.click(icon);

        expect(icon).toHaveClass('direction-down');
    });
});
