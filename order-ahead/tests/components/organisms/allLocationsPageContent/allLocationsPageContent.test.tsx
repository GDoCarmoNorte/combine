import React from 'react';
import { render, screen } from '@testing-library/react';
import AllLocationsPageLayout from '../../../../components/organisms/locations/allLocationsPageLayout';
import { allLocationsMock, sectionsMock } from './allLocationsPageContent.mock';

jest.mock('../../../../common/hooks/useBreadcrumbs', () => ({
    useBreadcrumbs: jest.fn().mockReturnValue([]),
}));

const mockMetaTitle = "BWW's Locations List | Buffalo wild wings";

describe('allLocationsPageLayout component', () => {
    it('should render correct', () => {
        render(
            <AllLocationsPageLayout
                allLocationDetails={allLocationsMock}
                sections={sectionsMock as any}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(screen.getByText(`${allLocationsMock.totalCount.toLocaleString()} locations`)).toBeInTheDocument();
        expect(screen.getByText(mockMetaTitle)).toBeInTheDocument();
    });

    it('should show notification banner if provided', () => {
        render(
            <AllLocationsPageLayout
                allLocationDetails={allLocationsMock}
                sections={sectionsMock as any}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(
            screen.getByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).toBeInTheDocument();
    });
    it('should not show notification banner if no sections provided provided', () => {
        render(<AllLocationsPageLayout allLocationDetails={allLocationsMock} pageMetaTitle={mockMetaTitle} />);

        expect(
            screen.queryByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).not.toBeInTheDocument();
    });
});
