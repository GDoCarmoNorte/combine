import React from 'react';
import { render } from '@testing-library/react';

import TermsAndConditionsLinks from '../../../../components/organisms/termsAndConditionsLinks';

describe('Terms And Conditions Links', () => {
    it('should render terms and conditions links', () => {
        const { asFragment } = render(<TermsAndConditionsLinks />);
        expect(asFragment()).toMatchSnapshot();
    });
});
