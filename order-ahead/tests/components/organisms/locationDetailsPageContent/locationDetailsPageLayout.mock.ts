import { ILocationByStateOrProvinceDetailsModel, IServiceTypeModel } from '../../../../@generated/webExpApi';

export const locationDetailsMock: ILocationByStateOrProvinceDetailsModel = {
    id: '12',
    displayName: 'Gahanna store',
    url: 'us/oh/gahanna/1380-cherry-bottom-rd--village-square-at-cherry-bottom/sports-bar-12',
    contactDetails: {
        address: {
            line: '1380 Cherry Bottom Rd. Village Square at Cherry Bottom',
            postalCode: '43230-6771',
            cityName: 'Gahanna',
            stateProvinceCode: 'OH',
        },
        phone: '614-478-7972',
    },
    addressMapLink:
        'https://www.google.com/maps?hl=en&saddr=current+location&daddr=1380%20Cherry%20Bottom%20Rd.%20Village%20Square%20at%20Cherry%20Bottom%2C%20Gahanna%2C%20OH%2C%2043230-6771',
    services: [IServiceTypeModel.BlazingRewards, IServiceTypeModel.Delivery],
    paymentMethods: [],
    hoursByDay: {
        Mon: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Tue: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Wed: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Thu: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Fri: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Sat: {
            start: '11:00',
            end: '00:00',
            isOpen24Hs: false,
        },
        Sun: {
            start: '11:00',
            end: '23:00',
            isOpen24Hs: false,
        },
    },
    details: {
        latitude: 40.055344,
        longitude: -82.884732,
    },
    timezone: 'America/New_York',
    isOnlineOrderAvailable: true,
    isClosed: false,
};

export const sectionsMock = [
    {
        sys: {
            space: {
                sys: {
                    type: 'Link',
                    linkType: 'Space',
                    id: 'l5fkpck1mwg3',
                },
            },
            id: '5eYmXApAlJjZawSphi6hkB',
            type: 'Entry',
            createdAt: '2021-09-07T17:50:49.432Z',
            updatedAt: '2021-09-09T06:53:58.485Z',
            environment: {
                sys: {
                    id: 'rio-pages',
                    type: 'Link',
                    linkType: 'Environment',
                },
            },
            revision: 3,
            contentType: {
                sys: {
                    type: 'Link',
                    linkType: 'ContentType',
                    id: 'secondaryBanner',
                },
            },
            locale: 'en-US',
        },
        fields: {
            hasBorder: true,
            icon: {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6pOhYWBq7R3yEO6sfiYRz1',
                    type: 'Asset',
                    createdAt: '2021-07-19T19:31:23.341Z',
                    updatedAt: '2021-08-03T14:00:19.020Z',
                    environment: {
                        sys: {
                            id: 'rio-pages',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 4,
                    locale: 'en-US',
                },
                fields: {
                    title: 'BWW Logo Mobile',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/6pOhYWBq7R3yEO6sfiYRz1/691a897ce1bfb0c3246ff01130fe46b5/bww-logo-mobile.svg',
                        details: {
                            size: 3629,
                            image: {
                                width: 61,
                                height: 61,
                            },
                        },
                        fileName: 'bww-logo-mobile.svg',
                        contentType: 'image/svg+xml',
                    },
                },
            },
            mainText: 'LOOKING FOR A DREAM JOB?',
            title: 'Game-time energy. Lifetime experience.',
            description:
                'At Buffalo Wild Wings, we forste a winning culture and  organization, where our team members enjoy the energy of game and gain experience for a lifetime.',
            linkText: 'JOIN OUR TEAM',
            mainTextColor: {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '2cOe4bqfSSqJzZ9SWXO6J',
                    type: 'Entry',
                    createdAt: '2021-08-05T11:31:37.124Z',
                    updatedAt: '2021-08-05T11:31:37.124Z',
                    environment: {
                        sys: {
                            id: 'rio-pages',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'color',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    name: 'primary',
                    hexColor: '382E2C',
                },
            },
        },
    },
    {
        metadata: {
            tags: [],
        },
        sys: {
            space: {
                sys: {
                    type: 'Link',
                    linkType: 'Space',
                    id: 'l5fkpck1mwg3',
                },
            },
            id: '3z5m7ULkItGrhwlVNKnX40',
            type: 'Entry',
            createdAt: '2021-09-14T16:52:31.652Z',
            updatedAt: '2021-09-14T16:52:31.652Z',
            environment: {
                sys: {
                    id: 'rio-feature-DBBP-31333-Nearby-loc',
                    type: 'Link',
                    linkType: 'Environment',
                },
            },
            revision: 1,
            contentType: {
                sys: {
                    type: 'Link',
                    linkType: 'ContentType',
                    id: 'nearbyLocationsSection',
                },
            },
            locale: 'en-US',
        },
        fields: {
            header: 'locations nearby',
            locationsLimit: '3',
        },
    },
];
