import React from 'react';
import { render, screen } from '@testing-library/react';
import NearbyLocationsContainer from '../../../../components/sections/nearbyLocationsSection';
import { locationDetailsMock, sectionsMock } from './locationDetailsPageLayout.mock';
import usePickupLocationsSearch from '../../../../common/hooks/usePickupLocationsSearch';
import { useRouter } from 'next/router';
import { useAppDispatch } from '../../../../redux/store';
import { LoadingStatusEnum } from '../../../../common/types';
import locationsPage1 from '../../../mocks/expLocations.mock';
import configFeatureFlags from '../../../mocks/configFeatureFlag.mock';

const nearbyLocationMock = locationsPage1.locations[0];
const nearbyLocationSectionMock = sectionsMock[1];

const dispatchMock = jest.fn();
const searchLocationsByQueryMock = jest.fn();
const searchLocationsByCoordinates = jest.fn();
const resetSearchResultMock = jest.fn();
const fetchMoreLocationsMock = jest.fn();
const routerPush = jest.fn();
const searchLocationsByUserGeolocationMock = jest.fn();

jest.mock('react-redux');
jest.mock('../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
    useAppDispatch: jest.fn(),
}));
jest.mock('../../../../common/hooks/usePickupLocationsSearch');
jest.mock('next/router');
jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({}),
    useOrderLocation: jest.fn().mockReturnValue({ actions: { setLocation: jest.fn() }, currentLocation: null }),
    useDomainMenu: jest.fn().mockReturnValue({ actions: { getDomainMenu: jest.fn() } }),
    useConfiguration: () => ({
        configuration: configFeatureFlags,
    }),
}));

describe('NearbyLocationsContainer component', () => {
    beforeEach(() => {
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                q: '',
            },
            push: routerPush,
        });
        (usePickupLocationsSearch as jest.Mock).mockReturnValue({
            searchLocationsByQuery: searchLocationsByQueryMock,
            searchLocationsByCoordinates: searchLocationsByCoordinates,
            searchLocationsByUserGeolocation: searchLocationsByUserGeolocationMock,
            fetchMoreLocations: fetchMoreLocationsMock,
            resetSearchResult: resetSearchResultMock,
            locationsSearchStatus: LoadingStatusEnum.Idle,
            locationsSearchResult: {
                locations: [
                    { id: locationDetailsMock.id, displayName: locationDetailsMock.displayName },
                    nearbyLocationMock,
                ],
            },
            moreLocationsFetchStatus: LoadingStatusEnum.Idle,
            isAbleToFetchMoreLocations: false,
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should show title', () => {
        render(
            <NearbyLocationsContainer
                data={{ locationDetails: locationDetailsMock }}
                entry={nearbyLocationSectionMock as any}
            />
        );
        expect(screen.getByText(/locations nearby/i)).toBeInTheDocument();
        expect(searchLocationsByCoordinates).toBeCalledTimes(1);
        expect(searchLocationsByCoordinates).toBeCalledWith({
            lat: locationDetailsMock.details?.latitude,
            lng: locationDetailsMock.details?.longitude,
        });
    });
    it('should render tiles', () => {
        render(
            <NearbyLocationsContainer
                data={{ locationDetails: locationDetailsMock }}
                entry={nearbyLocationSectionMock as any}
            />
        );
        expect(screen.getByText(nearbyLocationMock.displayName)).toBeInTheDocument();
        expect(screen.queryByText(locationDetailsMock.displayName)).not.toBeInTheDocument();
        expect(searchLocationsByCoordinates).toBeCalledTimes(1);
        expect(searchLocationsByCoordinates).toBeCalledWith({
            lat: locationDetailsMock.details?.latitude,
            lng: locationDetailsMock.details?.longitude,
        });
    });
    it('should not show nearby locations if no location details provided', () => {
        render(<NearbyLocationsContainer entry={nearbyLocationSectionMock as any} />);
        expect(screen.queryByText(nearbyLocationMock.displayName)).not.toBeInTheDocument();
    });
});
