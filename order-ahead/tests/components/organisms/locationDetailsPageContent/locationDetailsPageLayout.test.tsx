import React from 'react';
import { render, screen } from '@testing-library/react';
import LocationDetailsPageLayout from '../../../../components/organisms/locations/locationDetailsPageLayout';
import { locationDetailsMock } from './locationDetailsPageLayout.mock';
import mockLocationDetailsPage from '../../../mock-data/contentful/contentful-getEntry-page-locationDetails.json';
import { LoadingStatusEnum } from '../../../../common/types';
import usePickupLocationsSearch from '../../../../common/hooks/usePickupLocationsSearch';
import { getBeerMenuByLocationId } from '../../../../common/services/localTapList';
import useLocalTapList from '../../../../redux/hooks/useLocalTapList';
import { tapListResponseMock } from '../../../mocks/expLocations.mock';
import { usePersonalization } from '../../../../redux/hooks';

const searchLocationsByQueryMock = jest.fn();
const searchLocationsByCoordinates = jest.fn();
const resetSearchResultMock = jest.fn();
const fetchMoreLocationsMock = jest.fn();
const searchLocationsByUserGeolocationMock = jest.fn();

jest.mock('../../../../common/hooks/usePickupLocationsSearch');
jest.mock('../../../../common/services/localTapList');
jest.mock('../../../../redux/hooks/useLocalTapList');

jest.mock('../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
    useAppDispatch: jest.fn(),
}));

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({}),
    useDomainMenu: jest.fn().mockReturnValue({ actions: { getDomainMenu: jest.fn() } }),
    useOrderLocation: jest.fn().mockReturnValue({
        currentLocation: null,
        actions: {
            setPickupLocation: jest.fn(),
            setAvailableTimeSlots: jest.fn(),
        },
    }),
    useTallyOrder: jest.fn().mockReturnValue({ setUnavailableTallyItems: jest.fn() }),
    useNotifications: jest.fn().mockReturnValue({
        actions: { enqueueError: jest.fn() },
    }),
    usePersonalization: jest.fn(),
}));

jest.mock('next/router', () => ({
    useRouter: jest.fn().mockReturnValue({
        asPath: '',
        pathname: '',
    }),
}));

const sectionsMock = mockLocationDetailsPage.includes.Entry;

describe('LocationDetailsPageLayout component', () => {
    beforeEach(() => {
        (usePickupLocationsSearch as jest.Mock).mockReturnValue({
            searchLocationsByQuery: searchLocationsByQueryMock,
            searchLocationsByCoordinates: searchLocationsByCoordinates,
            searchLocationsByUserGeolocation: searchLocationsByUserGeolocationMock,
            fetchMoreLocations: fetchMoreLocationsMock,
            resetSearchResult: resetSearchResultMock,
            locationsSearchStatus: LoadingStatusEnum.Idle,
            locationsSearchResult: {
                locations: [],
            },
            moreLocationsFetchStatus: LoadingStatusEnum.Idle,
            isAbleToFetchMoreLocations: false,
        });
        (getBeerMenuByLocationId as jest.Mock).mockResolvedValue(tapListResponseMock);
        (useLocalTapList as any).mockReturnValue({
            list: {},
            available: false,
            loading: false,
            error: false,
            actions: {
                getLocalTapList: jest.fn(),
                setLocalTapList: jest.fn(),
            },
        });

        (usePersonalization as jest.Mock).mockReturnValue({
            loading: false,
            actions: { initializePersonalizationDependency: jest.fn(), getPersonalizedSection: jest.fn() },
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should show location header', () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} sections={sectionsMock as any} />);
        expect(screen.getByText(/Gahanna store/i)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: /ORDER/i })).toBeInTheDocument();
    });
    it('should show location info', () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} sections={sectionsMock as any} />);
        expect(screen.getByText(/Store hours/i)).toBeInTheDocument();
        expect(screen.getByText(/Services/i)).toBeInTheDocument();
    });
    it('should show secondary banner if passed', () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} sections={sectionsMock as any} />);
        expect(screen.getByText(/Game-time energy. Lifetime experience./i)).toBeInTheDocument();
    });
    it('should have notification banner if provided', async () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} sections={sectionsMock as any} />);
        expect(
            screen.getByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).toBeInTheDocument();
    });
    it('should have location news section if provided', async () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} sections={sectionsMock as any} />);
        const goLocationNewsSection = screen.getByText(/Facilities/i);
        expect(goLocationNewsSection).toBeInTheDocument();
    });
    it('should have location news section collapsed if more than one section', async () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} sections={sectionsMock as any} />);
        const expandedLocationNewsSections = screen.queryAllByLabelText(/Collapse/i);
        expect(expandedLocationNewsSections.length).toEqual(0);
    });
    it('should have location news section expanded if only one section', async () => {
        const locationNewsSectionMock = sectionsMock.filter(
            (s) => s.sys.contentType.sys.id === 'locationNewsSection'
        )[0];
        render(
            <LocationDetailsPageLayout
                locationDetails={locationDetailsMock}
                sections={[locationNewsSectionMock] as any}
            />
        );
        const expandedLocationNewsSections = screen.queryAllByLabelText(/Collapse/i);
        expect(expandedLocationNewsSections.length).toEqual(1);
    });
    it('should not have notification banner if section is not specified in CMS', () => {
        const sectionsWithoutNotificationBannerMock = sectionsMock.filter(
            (s) => s.sys.contentType.sys.id !== 'notificationBanner'
        );
        render(
            <LocationDetailsPageLayout
                locationDetails={locationDetailsMock}
                sections={sectionsWithoutNotificationBannerMock as any}
            />
        );
        expect(
            screen.queryByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).not.toBeInTheDocument();
    });
    it('should not have nearby locations if section is not specified in CMS', () => {
        const sectionsWithoutNearbyLocationsMock = sectionsMock.filter(
            (s) => s.sys.contentType.sys.id !== 'nearbyLocationsSection'
        );
        render(
            <LocationDetailsPageLayout
                locationDetails={locationDetailsMock}
                sections={sectionsWithoutNearbyLocationsMock as any}
            />
        );
        expect(screen.queryByText(/locations nearby/i)).not.toBeInTheDocument();
        expect(searchLocationsByCoordinates).toBeCalledTimes(0);
    });
    it('should not show any sections if not specified in CMS', () => {
        render(<LocationDetailsPageLayout locationDetails={locationDetailsMock} />);
        expect(screen.queryByText(/Facilities/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/locations nearby/i)).not.toBeInTheDocument();
        expect(
            screen.queryByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).not.toBeInTheDocument();
    });
});
