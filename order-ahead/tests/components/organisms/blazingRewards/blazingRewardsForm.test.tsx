import React from 'react';
import { render, screen, fireEvent, waitFor, act } from '@testing-library/react';
import { BlazingRewardsForm } from '../../../../components/organisms/blazingRewards/blazingRewardsForm';
import { useAccount } from '../../../../redux/hooks';
import useLoyaltyService from '../../../../common/hooks/useLoyaltyService';

jest.mock('../../../../redux/hooks/useAccount', () => jest.fn());
jest.mock('../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));
jest.mock('../../../../common/hooks/useLoyaltyService', () => jest.fn());
jest.mock('@material-ui/core/useMediaQuery', () => jest.fn().mockReturnValue(true));

describe('Blazing Rewards Form', () => {
    it('should not render with isOpen flag equal false', () => {
        render(<BlazingRewardsForm isOpen={false} onClose={jest.fn()} />);

        expect(screen.queryByText(/blazin’ rewards®/i)).not.toBeInTheDocument();
    });

    it('should render form', () => {
        render(<BlazingRewardsForm isOpen={true} onClose={jest.fn()} />);

        expect(screen.getByText(/blazin’ rewards®/i)).toBeInTheDocument();
        expect(screen.getByText(/first name/i)).toBeInTheDocument();
        expect(screen.getByText(/last name/i)).toBeInTheDocument();
        expect(screen.getByText(/phone/i)).toBeInTheDocument();
        expect(screen.getByText(/message/i)).toBeInTheDocument();
        expect(screen.getByText(/contact reason/i)).toBeInTheDocument();
        expect(screen.getByText(/submit/i)).toBeInTheDocument();
    });

    it('should prefill form', () => {
        (useAccount as jest.Mock).mockReturnValueOnce({
            account: {
                firstName: 'FIRSTNAME',
                lastName: 'LASTNAME',
                phones: [
                    {
                        number: '111-111-1111',
                    },
                ],
                email: 'aaaa@aaa.aa',
            },
        });

        render(<BlazingRewardsForm isOpen={true} onClose={jest.fn()} />);

        expect(screen.getByDisplayValue(/FIRSTNAME/i)).toBeInTheDocument();
        expect(screen.getByDisplayValue(/LASTNAME/i)).toBeInTheDocument();
        expect(screen.getByDisplayValue('(111) 111-1111')).toBeInTheDocument();
        expect(screen.getByDisplayValue(/aaaa@aaa.aa/i)).toBeInTheDocument();
    });

    it('should validate incomplete fields', async () => {
        render(<BlazingRewardsForm isOpen={true} onClose={jest.fn()} />);

        fireEvent.click(screen.getByText(/Submit/i));

        await waitFor(() => {
            expect(screen.getByText(/First name is incomplete/i)).toBeInTheDocument();
        });

        expect(screen.getByText(/Last name is incomplete/i)).toBeInTheDocument();
        expect(screen.getByText(/Email is incomplete/i)).toBeInTheDocument();
        expect(screen.getByText(/Phone number is incomplete/i)).toBeInTheDocument();
        expect(screen.getByText(/Contact Reason is incomplete/i)).toBeInTheDocument();
        expect(screen.getByText(/Message is incomplete/i)).toBeInTheDocument();
    });

    it('should validate fields', async () => {
        (useAccount as jest.Mock).mockReturnValueOnce({
            account: {
                firstName: 'FIRSTNAME11',
                lastName: 'LASTNAME11',
                phones: [
                    {
                        number: '111-111',
                    },
                ],
                email: 'aaaa',
            },
        });

        render(<BlazingRewardsForm isOpen={true} onClose={jest.fn()} />);

        fireEvent.click(screen.getByText(/Submit/i));

        await waitFor(() => {
            expect(screen.getAllByText(/Alpha characters only/i).length).toEqual(2);
        });
        expect(screen.getByText(/Incorrect email/i)).toBeInTheDocument();
        expect(screen.getByText(/Incorrect phone/i)).toBeInTheDocument();
    });

    it('Should submit form', async () => {
        (useAccount as jest.Mock).mockReturnValueOnce({
            account: {
                firstName: 'FIRSTNAME',
                lastName: 'LASTNAME',
                phones: [
                    {
                        number: '111-111-1111',
                    },
                ],
                email: 'aaaa@aaa.aa',
            },
        });

        const contactUsStub = jest.fn();

        (useLoyaltyService as jest.Mock).mockReturnValueOnce({
            contactUs: contactUsStub,
        });

        render(<BlazingRewardsForm isOpen={true} onClose={jest.fn()} />);

        fireEvent.click(screen.getByText(/Select a reason/i));
        fireEvent.click(screen.getByText(/Enrollment Help/i));

        fireEvent.change(screen.getByPlaceholderText(/Enter message/i), { target: { value: 'messsage' } });
        await screen.findByText(/messsage/i);
        fireEvent.click(screen.getByText(/Submit/i));

        await waitFor(() => {
            expect(contactUsStub).toBeCalled();
        });

        expect(screen.getByText(/Close/i)).toBeVisible();

        fireEvent.click(screen.getByText(/Close/i));

        expect(screen.queryByText(/Close/i)).not.toBeVisible();
    });

    it('Should show error banner', async () => {
        jest.useFakeTimers();

        (useAccount as jest.Mock).mockReturnValueOnce({
            account: {
                firstName: 'FIRSTNAME',
                lastName: 'LASTNAME',
                phones: [
                    {
                        number: '111-111-1111',
                    },
                ],
                email: 'aaaa@aaa.aa',
            },
        });

        const contactUsStub = jest.fn().mockImplementation(() => {
            throw new Error();
        });

        (useLoyaltyService as jest.Mock).mockReturnValueOnce({
            contactUs: contactUsStub,
        });

        render(<BlazingRewardsForm isOpen={true} onClose={jest.fn()} />);

        fireEvent.click(screen.getByText(/Select a reason/i));
        fireEvent.click(screen.getByText(/Enrollment Help/i));

        fireEvent.change(screen.getByPlaceholderText(/Enter message/i), { target: { value: 'messsage' } });

        await screen.findByText(/messsage/i);

        fireEvent.click(screen.getByText(/Submit/i));

        await waitFor(() => {
            expect(screen.getByText(/TECHNICAL ERROR/i)).toBeVisible();
        });

        await act(async () => {
            jest.runAllTimers();
        });

        await waitFor(() => expect(screen.queryByText(/TECHNICAL ERROR/i)).not.toBeVisible());
    });
});
