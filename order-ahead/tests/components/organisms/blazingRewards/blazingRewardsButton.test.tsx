import React from 'react';
import { render, screen } from '@testing-library/react';
import { useAccount } from '../../../../redux/hooks';
import { BlazingRewardsButton } from '../../../../components/organisms/blazingRewards/blazingRewardsButton';

jest.mock('../../../../redux/hooks/useAccount', () => jest.fn());
jest.mock('../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));

describe('Blazing Rewards Button', () => {
    it('Should render button for authenticated user', () => {
        (useAccount as jest.Mock).mockReturnValueOnce({
            account: {
                firstName: 'FIRSTNAME',
                lastName: 'LASTNAME',
                phones: [
                    {
                        number: '111-111-1111',
                    },
                ],
                email: 'aaaa@aaa.aa',
            },
        });

        render(<BlazingRewardsButton />);

        expect(screen.getByText(/blazin’ rewards/i)).toBeInTheDocument();
    });

    it('Should not render button for not authenticated user', () => {
        render(<BlazingRewardsButton />);

        expect(screen.queryByText(/blazin’ rewards/i)).not.toBeInTheDocument();
    });
});
