import React from 'react';
import RecentOrdersHeader from '../../../../components/sections/recentOrders/recentOrdersHeader';
import { useGlobalProps } from '../../../../redux/hooks';
import { render, screen } from '../../../utils';

jest.mock('../../../../redux/hooks');

describe('recent order header', () => {
    (useGlobalProps as jest.Mock).mockReturnValue({ productDetailsPagePaths: 'paths' });

    it('should renders correctly if there are main and secondary texts', () => {
        render(<RecentOrdersHeader mainText="Test main text" secondaryText="Test secondary text" image={'' as any} />);

        expect(screen.getByText(/Test main text/i)).toBeInTheDocument();
        expect(screen.getByText(/Test secondary text/i)).toBeInTheDocument();
    });

    it('should renders correctly if there are not main and secondary texts', () => {
        render(<RecentOrdersHeader image={'' as any} />);

        expect(screen.getByText(/RELIVE THE GLORY/i)).toBeInTheDocument();
        expect(screen.getByText(/RECENT ORDERS/i)).toBeInTheDocument();
    });
});
