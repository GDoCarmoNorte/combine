import React from 'react';
import { render, screen } from '../../../utils';
import RecentOrders from '../../../../components/sections/recentOrders';
import { orderHistoryItemMock } from '../../../mocks/orderHistoryItem.mocks';
import { IRecentOrdersSection } from '../../../../@generated/@types/contentful';

const orderHistoryStateMock = {
    orderHistory: [orderHistoryItemMock, orderHistoryItemMock, orderHistoryItemMock, orderHistoryItemMock],
    loading: false,
};

const recentOrdersSectionMock = {
    fields: {
        name: 'Recent Orders Section',
        mainText: 'Relive the Glory',
        secondaryText: 'Recent Orders',
        image: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '2r1HxBacFYuXHiGzeB2mjx',
                type: 'Asset',
                createdAt: '2021-12-13T11:23:04.400Z',
                updatedAt: '2021-12-13T13:57:35.149Z',
                environment: {
                    sys: {
                        id: 'feature-feature-DBBP-31429',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                locale: 'en-US',
            },
            fields: {
                title: 'Icon Reorder',
                description: '',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/2r1HxBacFYuXHiGzeB2mjx/f2ec3887a27e2eb0d220dd3bf5993b42/Navigational.svg',
                    details: {
                        size: 1408,
                        image: {
                            width: 60,
                            height: 60,
                        },
                    },
                    fileName: 'Navigational.svg',
                    contentType: 'image/svg+xml',
                },
            },
        },
    },
};

jest.mock('../../../../redux/hooks/useGlobalProps');

describe('RecentOrders component', () => {
    it('should not render anything if order history is empty', () => {
        render(<RecentOrders entry={(recentOrdersSectionMock as unknown) as IRecentOrdersSection} />);

        expect(screen.queryByText(/recent orders/i)).not.toBeInTheDocument();
    });

    it('should display recent orders block and have max 3 items', () => {
        render(<RecentOrders entry={(recentOrdersSectionMock as unknown) as IRecentOrdersSection} />, {
            state: { orderHistory: orderHistoryStateMock },
        });

        expect(screen.getByText(/recent orders/i)).toBeInTheDocument();
        expect(screen.getAllByRole('button', { name: /add to bag/i })).toHaveLength(3);
    });

    it('should display view all link', () => {
        render(<RecentOrders entry={(recentOrdersSectionMock as unknown) as IRecentOrdersSection} />, {
            state: { orderHistory: orderHistoryStateMock },
        });

        const link = screen.getByRole('link', { name: /view all/i });
        expect(link).toBeInTheDocument();
        expect(link).toHaveAttribute('href', '/account/orders');
    });

    it('should render view details link if location is not selected', () => {
        render(<RecentOrders entry={(recentOrdersSectionMock as unknown) as IRecentOrdersSection} />, {
            state: {
                orderHistory: orderHistoryStateMock,
                orderLocation: { method: null, deliveryAddress: null, pickupAddress: null },
            },
        });

        const links = screen.getAllByRole('link', { name: /view details/i });

        expect(screen.queryByRole('button', { name: /add to bag/i })).not.toBeInTheDocument();
        expect(links).toHaveLength(3);

        links.forEach((link) => expect(link).toHaveAttribute('href', '/account/orders'));
    });
});
