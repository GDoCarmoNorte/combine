import React from 'react';
import { mount } from 'enzyme';
import { Form, Formik } from 'formik';

import { createConnector } from '../../../../components/clientOnly/formik/connector';
import { actImmediate } from '../../clientOnly/formik/formikTestUtils';
import FormikInput from '../../../../components/organisms/formikInput';

describe('FormikInput', () => {
    it('FormikInput validation success', async () => {
        const FormikConnector = createConnector<{ test: string }>();

        const noop = jest.fn();
        const changeHandler = jest.fn();
        const validate = jest.fn().mockReturnValueOnce(undefined);

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikInput name="test" label="test" type="text" validate={validate} />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        const input = wrapper.find('input').at(0);

        expect(input).toBeDefined();

        input.simulate('change', { target: { name: 'test', value: 'test' } });

        await actImmediate(wrapper);

        expect(validate).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenLastCalledWith({ test: 'test' }, true, {});

        wrapper.unmount();
    });

    it('FormikInput validation failure', async () => {
        const FormikConnector = createConnector<{ test: string }>();

        const noop = jest.fn();
        const changeHandler = jest.fn();
        const validate = jest.fn().mockReturnValue('test error');

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikInput name="test" label="test" type="text" validate={validate} />
                        <FormikConnector onChange={changeHandler} />
                    </Form>
                )}
            </Formik>
        );

        const input = wrapper.find('input').at(0);

        expect(input).toBeDefined();

        input.simulate('change', { target: { name: 'test', value: 'test' } });

        await actImmediate(wrapper);

        expect(validate).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenCalled();
        expect(changeHandler).toHaveBeenLastCalledWith({ test: 'test' }, false, { test: 'test error' });

        wrapper.unmount();
    });

    it('formik input with optional label', () => {
        const noop = jest.fn();

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikInput name="test" label="test" type="text" optional />
                    </Form>
                )}
            </Formik>
        );
        const label = wrapper.find('.inputLabelOptional');
        expect(label.text()).toEqual(' (Optional)');
    });

    it('formik input with required label', () => {
        const noop = jest.fn();

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikInput name="test" label="test" type="text" required />
                    </Form>
                )}
            </Formik>
        );
        const label = wrapper.find('.inputLabelStar');
        expect(label.text()).toEqual(' *');
    });
});

describe('tooltip icon', () => {
    it('tooltip icon should not be visible', () => {
        const noop = jest.fn();

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikInput name="test" label="test" type="text" />
                    </Form>
                )}
            </Formik>
        );
        const icon = wrapper.find('.brand-icon');
        expect(icon).toHaveLength(0);
    });

    it('tooltip icon should be visible and after click tooltip should be open', () => {
        const noop = jest.fn();

        const wrapper = mount(
            <Formik onSubmit={noop} initialValues={{ test: '' }}>
                {() => (
                    <Form>
                        <FormikInput
                            name="test"
                            label="test"
                            type="text"
                            tooltipTitle="Tooltip title"
                            tooltipClassName="tooltip"
                            withTooltip
                        />
                    </Form>
                )}
            </Formik>
        );
        const icon = wrapper.find('.brand-icon');
        expect(icon).toHaveLength(1);

        icon.simulate('click');
        const tooltip = wrapper.find({ role: 'tooltip' });
        expect(tooltip.text()).toEqual('Tooltip title');
    });
});
