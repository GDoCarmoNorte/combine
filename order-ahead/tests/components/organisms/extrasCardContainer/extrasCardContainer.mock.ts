export const productMock = {
    name: 'Product Name 1',
    id: '1',
    quantity: 1,
    resultCalories: 300,
    resultPrice: 100,
    sizeSelections: [
        { product: { id: '1' }, sizeGroup: { name: 'sg1' }, isGroupedBySize: true },
        { product: { id: '1' }, sizeGroup: { name: 'sg2' }, isGroupedBySize: true },
    ],
};
