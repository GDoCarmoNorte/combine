import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { ExtrasCardContainer } from '../../../../components/organisms/extrasCardContainer/extrasCardContainer';
import { productMock } from './extrasCardContainer.mock';
import { usePdp } from '../../../../redux/hooks';

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/store', () => ({
    __esModule: true,
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
    dispatch: jest.fn(),
}));

jest.mock('../../../../redux/hooks/pdp', () => ({
    useChangedModifiersForChildItem: () => ({
        addedModifiers: [],
        removedDefaultModifiers: [],
    }),
    useBwwDisplayModifierGroupsForExtraItem: () => [{ name: 'modifier' }],
}));

jest.mock('../../../../redux/hooks/usePdp', () =>
    jest.fn(() => ({
        useExtraChild: jest.fn().mockReturnValue({ setSelectedExtraChild: jest.fn(), unselectExtraChild: jest.fn() }),
    }))
);

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => ({
        modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        productsById: {
            '1': {},
        },
    }))
);

describe('extras component', () => {
    it('should show modify when item is selected and has modifiers', () => {
        render(<ExtrasCardContainer item={productMock as any} sectionIndex={0} />);

        screen.getByText(/modify/i);
    });

    it('should select card', () => {
        const setSelectedMock = jest.fn();
        const unselectMock = jest.fn();

        (usePdp as jest.Mock).mockImplementation(() => ({
            useExtraChild: jest
                .fn()
                .mockReturnValue({ setSelectedExtraChild: setSelectedMock, unselectExtraChild: unselectMock }),
        }));

        render(<ExtrasCardContainer item={{ ...productMock, quantity: 0 } as any} sectionIndex={0} />);
        userEvent.click(screen.getByLabelText('Select Product Name 1'));
        expect(setSelectedMock).toHaveBeenCalled();
    });

    it('should unselect card', () => {
        const setSelectedMock = jest.fn();
        const unselectMock = jest.fn();

        (usePdp as jest.Mock).mockImplementation(() => ({
            useExtraChild: jest
                .fn()
                .mockReturnValue({ setSelectedExtraChild: setSelectedMock, unselectExtraChild: unselectMock }),
        }));

        render(<ExtrasCardContainer item={productMock as any} sectionIndex={0} />);

        userEvent.click(screen.getByLabelText('Select Product Name 1'));

        expect(unselectMock).toHaveBeenCalled();
    });
});
