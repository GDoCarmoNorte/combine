import React from 'react';
import { render, screen } from '@testing-library/react';
import { allLocationsMock, sectionsMock } from '../stateLocationsTile/stateLocations.mock';
import StateLocationsPageLayout from '../../../../components/organisms/locations/stateLocationsPageLayout';

const countryMock = allLocationsMock.locationsByCountry[0];
const countryMockMock = countryMock.countryCode;
const stateMock = countryMock.statesOrProvinces[0];

jest.mock('../../../../common/hooks/useBreadcrumbs', () => ({
    useBreadcrumbs: jest.fn().mockReturnValue([]),
}));

const mockMetaTitle = 'Buffalo wild wings Locations in Texas';

describe('StateLocationsTile component', () => {
    it('should correct render', () => {
        render(
            <StateLocationsPageLayout
                stateLocationDetails={stateMock}
                countryCode={countryMockMock}
                sections={sectionsMock as any}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(screen.getByText(`${stateMock.count.toLocaleString()} ${stateMock.name} locations`)).toBeInTheDocument();
        expect(screen.getByText(/Minneapolis/i)).toBeInTheDocument();
        expect(screen.getByText(/Test/i)).toBeInTheDocument();
        expect(screen.getByText(mockMetaTitle)).toBeInTheDocument();
    });

    it('should show notification banner if provided', () => {
        render(
            <StateLocationsPageLayout
                stateLocationDetails={stateMock}
                countryCode={countryMockMock}
                sections={sectionsMock as any}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(
            screen.getByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).toBeInTheDocument();
    });

    it('should not show notification banner if not provided', () => {
        render(
            <StateLocationsPageLayout
                stateLocationDetails={stateMock}
                countryCode={countryMockMock}
                pageMetaTitle={mockMetaTitle}
            />
        );

        expect(
            screen.queryByText(
                /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
            )
        ).not.toBeInTheDocument();
    });
});
