import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import PaymentMethods from '../../../../components/organisms/paymentMethods/paymentMethods';
import { useCustomerPaymentMethod } from '../../../../common/hooks/useCustomerPaymentMethod';
import { mockCreditCard, mockCreditCards, mockExpiredCreditCard } from './paymentMethods.mock';

jest.mock('react-redux');
jest.mock('../../../../common/hooks/useCustomerPaymentMethod');

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({
        productDetailsPagePaths: {},
    }),
}));

describe('Payment methods component', () => {
    const mockCustomerId = 'abc-123';
    const mockJwt = 'eblyhfksl';

    beforeAll(() => {
        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            __esModule: true,
            paymentMethods: mockCreditCard,
            loading: false,
        });
    });

    it('should render correctly', () => {
        const { container } = render(<PaymentMethods customerId={mockCustomerId} jwtToken={mockJwt} />);

        expect(container).toMatchSnapshot();
    });

    it('should be render 1 credit card and 1 blank card', async () => {
        render(<PaymentMethods customerId={mockCustomerId} jwtToken={mockJwt} />);

        expect(screen.getAllByText('Remove').length).toEqual(1);
        expect(screen.getAllByText('Add new credit card').length).toEqual(1);
    });

    it('should be render only 2 credit card', async () => {
        (useCustomerPaymentMethod as jest.Mock).mockReturnValueOnce({
            __esModule: true,
            paymentMethods: mockCreditCards,
            loading: false,
        });

        render(<PaymentMethods customerId={mockCustomerId} jwtToken={mockJwt} />);

        expect(screen.getAllByText('Remove').length).toEqual(2);
        expect(screen.queryByText('Add new credit card')).toBeNull();
    });

    it('should be display text "Expired:" when card isExpired', () => {
        (useCustomerPaymentMethod as jest.Mock).mockReturnValueOnce({
            __esModule: true,
            paymentMethods: mockExpiredCreditCard,
            loading: false,
        });
        render(<PaymentMethods customerId={mockCustomerId} jwtToken={mockJwt} />);

        expect(screen.getByText(/Expired:/i)).toBeInTheDocument();
    });

    it('should be open and close payment modal', async () => {
        render(<PaymentMethods customerId={mockCustomerId} jwtToken={mockJwt} />);
        const removeCreditCardButton = screen.getAllByRole('button', { name: /Remove/i });
        fireEvent.click(removeCreditCardButton[0]);
        const modalContainer = screen.getByRole('presentation');
        expect(modalContainer).toBeInTheDocument();
        const closeModalButton = screen.getByRole('button', { name: /No, leave it/i });
        userEvent.click(closeModalButton);
        await expect(modalContainer).not.toBeInTheDocument();
    });

    it('should be shown loader when loading true', async () => {
        (useCustomerPaymentMethod as jest.Mock).mockReturnValueOnce({
            __esModule: true,
            paymentMethods: mockExpiredCreditCard,
            loading: true,
        });
        render(<PaymentMethods customerId={mockCustomerId} jwtToken={mockJwt} />);
        expect(screen.getByRole('progressbar')).toBeInTheDocument();
    });
});
