import {
    TAccountPaymentMethodTypeModel,
    IGetCustomerAccountPaymentMethodsResponseModel,
} from '../../../../@generated/webExpApi';

export const mockCreditCard: IGetCustomerAccountPaymentMethodsResponseModel = {
    cREDITCARDS: [
        {
            label: 'personal',
            cardHolderName: 'JOHN DOE',
            type: TAccountPaymentMethodTypeModel.Vi,
            lastFourDigits: '411111xxxxxx1111',
            expirationDate: '05/2022',
            isDefault: true,
            isExpired: false,
            token: '1234567890',
        },
    ],
};

export const mockCreditCards: IGetCustomerAccountPaymentMethodsResponseModel = {
    cREDITCARDS: [
        {
            label: 'personal',
            cardHolderName: 'JOHN DOE',
            type: TAccountPaymentMethodTypeModel.Vi,
            lastFourDigits: '411111xxxxxx1111',
            expirationDate: '05/2022',
            isDefault: true,
            isExpired: false,
            token: '1234567890',
        },
        {
            label: 'bussiness',
            cardHolderName: 'JOHN DOE',
            type: TAccountPaymentMethodTypeModel.Vi,
            lastFourDigits: '411111xxxxxx1111',
            expirationDate: '05/2022',
            isDefault: false,
            isExpired: false,
            token: '1234567891',
        },
    ],
};

export const mockExpiredCreditCard: IGetCustomerAccountPaymentMethodsResponseModel = {
    cREDITCARDS: [
        {
            label: 'personal',
            cardHolderName: 'JOHN DOE',
            type: TAccountPaymentMethodTypeModel.Vi,
            lastFourDigits: '411111xxxxxx1111',
            expirationDate: '05/2022',
            isDefault: true,
            isExpired: true,
            token: '1234567890',
        },
    ],
};
