import React from 'react';
import { render, screen } from '@testing-library/react';
import NotificationBanner from '../../../../components/organisms/notificationBanner/notificationBanner';
import { notificationBannerMock } from './notificationBanner.mock';

describe('NotificationBanner component', () => {
    it('should render correctly', () => {
        render(<NotificationBanner entry={notificationBannerMock as any} />);

        //render header
        expect(screen.getByText(notificationBannerMock.fields.text)).toBeInTheDocument();
        expect(screen.getByRole(/banner/i)).toHaveStyle(`background-color: (--col--gray1)`);
    });

    it('should use passed backgrounfColor if provided', () => {
        const notificationBannerWithBackgroundMock = {
            ...notificationBannerMock,
            fields: {
                ...notificationBannerMock.fields,
                backgroundColor: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'o19mhvm9a2cm',
                            },
                        },
                        id: '7FY6Ptl8PSAmBoIvyCa6Bn',
                        type: 'Entry',
                        createdAt: '2020-08-09T21:38:50.910Z',
                        updatedAt: '2020-08-09T21:38:50.910Z',
                        environment: {
                            sys: {
                                id: 'feature-DBBP-29422',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'color',
                            },
                        },
                        locale: 'en-US',
                    },
                    fields: {
                        name: 'arbysred',
                        hexColor: 'D71920',
                    },
                },
            },
        };
        render(<NotificationBanner entry={notificationBannerWithBackgroundMock as any} />);
        expect(screen.getByRole(/banner/i)).toHaveStyle(`background-color: #D71920;`);
    });
});
