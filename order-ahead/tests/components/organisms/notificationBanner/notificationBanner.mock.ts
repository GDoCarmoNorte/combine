export const notificationBannerMock = {
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '7J9sdwm8gU8R19pviNTcGH',
        type: 'Entry',
        createdAt: '2021-09-07T07:15:04.810Z',
        updatedAt: '2021-09-07T07:15:04.810Z',
        environment: {
            sys: {
                id: 'rio-feature-DBBP-39869-Location-banner',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'notificationBanner',
            },
        },
        locale: 'en-US',
    },
    fields: {
        text:
            'Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm.',
        icon: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '6pOhYWBq7R3yEO6sfiYRz1',
                type: 'Asset',
                createdAt: '2021-07-19T19:31:23.341Z',
                updatedAt: '2021-08-03T14:00:19.020Z',
                environment: {
                    sys: {
                        id: 'rio-feature-DBBP-39869-Location-banner',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 4,
                locale: 'en-US',
            },
            fields: {
                title: 'Alert icon',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/6pOhYWBq7R3yEO6sfiYRz1/691a897ce1bfb0c3246ff01130fe46b5/bww-logo-mobile.svg',
                    details: {
                        size: 3629,
                        image: {
                            width: 61,
                            height: 61,
                        },
                    },
                    fileName: 'bww-logo-mobile.svg',
                    contentType: 'image/svg+xml',
                },
            },
        },
    },
};
