import React from 'react';
import { render, screen } from '../../../utils';
import userEvent from '@testing-library/user-event';
import { RewardsActivityHistoryItem } from '../../../../components/organisms/rewardsActivityHistory/rewardsActivityHistoryItem/rewardsActivityHistoryItem';

describe('RewardsActivityHistoryItem', () => {
    const renderComponent = ({
        type = 'Redeemed certificate',
        date = '4/21/21',
        itemDescription = '',
        points = 100,
        isFullView = false,
        transactionNumber = undefined,
        offers = [],
        certificates = [],
        products = undefined,
        total = undefined,
        totalEligible = undefined,
    }) => (
        <RewardsActivityHistoryItem
            type={type}
            date={date}
            itemDescription={itemDescription}
            points={points}
            isFullView={isFullView}
            transactionNumber={transactionNumber}
            offers={offers}
            certificates={certificates}
            products={products}
            total={total}
            totalEligible={totalEligible}
        ></RewardsActivityHistoryItem>
    );
    it('Shoul renders correctly by default', () => {
        render(renderComponent({}));

        expect(screen.getByText(/100 points/i)).toBeInTheDocument();
        expect(screen.getByText(/4\/21\/21/i)).toBeInTheDocument();
        expect(screen.getByText(/Redeemed certificate/i)).toBeInTheDocument();
    });

    it('Shoul renders correctly with full view', () => {
        render(
            renderComponent({
                type: 'Eat & Earn',
                isFullView: true,
                offers: [{ code: 'ABC', name: 'LUNCH BONUS OFFER: 300 POINTS' }],
                certificates: [{ number: '123', title: 'Certificate' }],
                products: [
                    {
                        lineItemId: 1,
                        description: 'Cheese Curd Bacon Burger',
                        quantity: 1,
                        price: 10.99,
                        modifiers: [
                            {
                                lineItemId: 2,
                                description: 'Chili',
                                quantity: 1,
                                price: 1,
                                modifiers: [],
                            },
                        ],
                    },
                ],
                transactionNumber: '1111122222',
                total: 10.99,
                totalEligible: 10.99,
            })
        );
        const link = screen.getByRole(/button/i, { name: /Eat & Earn/i });
        expect(link).toBeInTheDocument();

        userEvent.click(link);
        expect(screen.getByText(/LUNCH BONUS OFFER: 300 POINTS/i)).toBeInTheDocument();
        expect(screen.getByText(/TRANSACTION NUMBER: 1111122222/i)).toBeInTheDocument();
        expect(screen.getByText(/Cheese Curd Bacon Burger/i)).toBeInTheDocument();
        expect(screen.getByText(/Chili/i)).toBeInTheDocument();
        expect(screen.getByText(/REWARD APPLIED/i)).toBeInTheDocument();
        expect(screen.getByText(/Certificate/i)).toBeInTheDocument();
    });
});
