import React from 'react';
import { render, screen } from '../../../utils';
import userEvent from '@testing-library/user-event';
import { RewardsActivityHistory } from '../../../../components/organisms/rewardsActivityHistory/rewardsActivityHistory';
import { TActivityTypeModel } from '../../../../@generated/webExpApi/models';

jest.mock('../../../../redux/hooks/useGlobalProps');

describe('RewardsActivityHistory', () => {
    it('Should renders correctly with view all link within account/rewards', () => {
        render(<RewardsActivityHistory />);

        expect(screen.getByText(/view all/i)).toBeInTheDocument();
    });

    it('Should renders correctly without view all link within account/rewards', () => {
        render(<RewardsActivityHistory />, {
            state: {
                rewards: {
                    rewardsCatalogLoading: true,
                    rewardsCatalog: {},
                    totalCount: 0,
                    offers: [],
                    certificates: [],
                    lastPurchasedCertificate: null,
                    loading: false,
                    rewardsRecommendations: [],
                    rewardsActivityHistory: [
                        {
                            description: 'Chips and Salsa',
                            storeId: '',
                            type: TActivityTypeModel.CertificateRedemption,
                            dateTime: new Date('2021-09-22T09:20:23Z'),
                            points: -450,
                            pointsBalanceAfter: 0,
                            offersApplied: [],
                            certificatesApplied: [],
                        },
                        {
                            description: 'Fried Pickles',
                            storeId: '',
                            type: TActivityTypeModel.CertificateRedemption,
                            dateTime: new Date('2021-09-16T09:45:13Z'),
                            points: -600,
                            pointsBalanceAfter: 0,
                            offersApplied: [],
                            certificatesApplied: [],
                        },
                    ],
                    rewardsActivityHistoryLoading: false,
                },
            },
            renderOptions: {},
        });

        expect(screen.queryByText(/view all/i)).not.toBeInTheDocument();
    });

    it('Should return null within account/rewards', () => {
        const { container } = render(<RewardsActivityHistory />, {
            state: {
                rewards: {
                    rewardsCatalogLoading: true,
                    rewardsCatalog: {},
                    totalCount: 0,
                    offers: [],
                    certificates: [],
                    lastPurchasedCertificate: null,
                    loading: false,
                    rewardsRecommendations: [],
                    rewardsActivityHistory: [],
                    rewardsActivityHistoryLoading: false,
                },
            },
            renderOptions: {},
        });

        expect(container).toMatchSnapshot();
    });

    it('Should show a loader', () => {
        render(<RewardsActivityHistory />, {
            state: {
                rewards: {
                    rewardsCatalogLoading: true,
                    rewardsCatalog: {},
                    totalCount: 0,
                    offers: [],
                    certificates: [],
                    lastPurchasedCertificate: null,
                    loading: false,
                    rewardsRecommendations: [],
                    rewardsActivityHistory: [],
                    rewardsActivityHistoryLoading: true,
                },
            },
            renderOptions: {},
        });

        expect(screen.getByRole(/progressbar/i)).toBeInTheDocument();
    });

    it('Should renders correctly with a more link within the points activity page and when all items will be shown, a more link should not be', () => {
        render(<RewardsActivityHistory isActivityPage={true} />);
        const moreLink = screen.getByText(/more/i);
        expect(moreLink).toBeInTheDocument();

        userEvent.click(moreLink);
        expect(screen.queryByText(/more/i)).not.toBeInTheDocument();
    });
});
