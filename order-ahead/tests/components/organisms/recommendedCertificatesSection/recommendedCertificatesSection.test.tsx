import React from 'react';
import { render } from '../../../utils';
import RecommendedCertificatesSection from '../../../../components/organisms/recommendedCertificatesSection';
import UseRewards from '../../../../redux/hooks/useRewards';
import { usePurchaseOffer } from '../../../../common/hooks/usePurchaseOffer';

jest.mock('../../../../redux/hooks/useRewards');
jest.mock('../../../../common/hooks/usePurchaseOffer');

const rewardsRecommendationsMock = [
    {
        id: 'c3589464-590f-4e4a-954e-e2624d3bfa74',
        type: 'ON_DEMAND',
        code: '59',
        status: 'ACTIVE',
        title: 'Boneless - 30 Wings',
        points: 3000,
        imageUrl:
            '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BWW_BonelessWings_Sauce.jpg',
        startDateTime: '2020-02-17T06:00:00Z',
        endDateTime: '2099-02-18T05:59:59Z',
    },
    {
        id: '36a4d0d3-35b6-4315-bd36-f355e465a6c2',
        type: 'ON_DEMAND',
        code: '62',
        status: 'ACTIVE',
        title: 'Traditional - 15 Wings',
        points: 2900,
        imageUrl:
            '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BWW_TraditionalWings_Sauce.jpg',
        startDateTime: '2015-06-01T00:00:00Z',
        endDateTime: '2051-01-01T05:59:59Z',
    },
    {
        id: 'fdcd476d-76ba-4de0-8c7d-a5d5573d5fee',
        type: 'ON_DEMAND',
        code: '142',
        status: 'ACTIVE',
        title: '20 Boneless Wings + Fries',
        points: 2450,
        imageUrl:
            '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/Boneless_fries_bundle.jpg',
        startDateTime: '2020-03-20T05:00:00Z',
        endDateTime: '2099-03-21T04:59:59Z',
    },
    {
        id: '73cf1e54-74fa-4369-b9c1-6bc41f074509',
        type: 'ON_DEMAND',
        code: '145',
        status: 'ACTIVE',
        title: '10 Traditional Wings + Fries',
        points: 2300,
        imageUrl:
            '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/Traditional_fries _ bundles.jpg',
        startDateTime: '2020-03-23T05:00:00Z',
        endDateTime: '2099-03-24T04:59:59Z',
    },
    {
        id: 'ea5c47a7-1ae3-461c-a54f-736ddbc40aac',
        type: 'ON_DEMAND',
        code: '58',
        status: 'ACTIVE',
        title: 'Boneless - 20 Wings',
        points: 2150,
        imageUrl:
            '//s3.amazonaws.com/u1bfww-al-images.epsilonagilityloyalty.com/epsilon/fusion/epsilon.fusion.admin.web/rewardimages/BWW_BonelessWings_Sauce.jpg',
        startDateTime: '2015-06-01T00:00:00Z',
        endDateTime: '2051-01-01T05:59:59Z',
    },
];

describe('recommended certificates section', () => {
    it('should render correctly Recommended Certificates Section', () => {
        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn().mockImplementation(() => Promise.resolve()),
            lastPurchaseData: null,
            cleanLastPurchaseData: jest.fn(),
            success: false,
        });
        (UseRewards as jest.Mock).mockReturnValue({
            rewardsRecommendations: rewardsRecommendationsMock,
        });
        const { container } = render(<RecommendedCertificatesSection />);
        expect(container).toMatchSnapshot();
    });
});
