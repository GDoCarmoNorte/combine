import React from 'react';
import { render, screen } from '@testing-library/react';
import PaymentHeader from '../../../../../components/organisms/header/paymentHeader';
import { useRouter } from 'next/router';
import { getImageUrl } from '../../../../../common/helpers/contentfulImage';
import { useMediaQuery } from '@material-ui/core';

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));
jest.mock('../../../../../common/helpers/contentfulImage', () => ({ getImageUrl: jest.fn() }));
jest.mock('@material-ui/core/useMediaQuery');

describe('Payment Header', () => {
    (getImageUrl as jest.Mock).mockReturnValueOnce('image-url');
    (useRouter as jest.Mock).mockImplementation(() => ({
        back: jest.fn(),
    }));

    it('should render back button for desktop', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(false);
        render(<PaymentHeader navigation={{ items: [] }} isPaymentProcessingShouldBeShown={false} />);

        expect(screen.getByText(/Back/i)).toBeInTheDocument();
    });

    it('should not render back button for mobile if payment is in proccessing', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(true);
        render(<PaymentHeader navigation={{ items: [] }} isPaymentProcessingShouldBeShown={true} />);

        expect(screen.queryByText(/Back/i)).not.toBeInTheDocument();
    });
});
