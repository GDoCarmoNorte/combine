import React from 'react';
import { render, screen } from '../../../utils';
import RewardsRoster from './../../../../components/organisms/rewardsRoster/rewardsRoster';
import rewardsRosterPageMock from '../../../mock-data/contentful/contentful-getEntries-page-rewards-roster.json';
import globalPropsMock from '../../../mocks/globalProps.mock';
import { useLoyalty, useRewards } from '../../../../redux/hooks';
import { usePurchaseOffer } from '../../../../common/hooks/usePurchaseOffer';
import rewardsMock from '../../../mock-data/provider/rewards.mock.json';

jest.mock('../../../../redux/hooks/useGlobalProps', () =>
    jest.fn(() => undefined).mockImplementation(() => globalPropsMock)
);

jest.mock('../../../../redux/hooks/useLoyalty');
jest.mock('../../../../redux/hooks/useRewards');
jest.mock('../../../../common/hooks/usePurchaseOffer');

describe('rewards roster', () => {
    const faq = rewardsRosterPageMock.items[0].fields?.section[0];
    const emptyFaq = { fields: { title: '' } };

    beforeEach(() => {
        (useLoyalty as jest.Mock).mockReturnValue({
            loyalty: {
                pointsBalance: 0,
                pointsExpiring: 0,
                pointsExpiringDate: null,
            },
            isLoading: false,
            error: null,
            actions: { setLoyaltyPoints: jest.fn(), setLoyaltyPointsLoading: jest.fn() },
        });

        (usePurchaseOffer as jest.Mock).mockReturnValue({
            loading: false,
            purchaseOffer: jest.fn().mockImplementation(() => Promise.resolve()),
            lastPurchaseData: null,
            cleanLastPurchaseData: jest.fn(),
            error: null,
        });

        (useRewards as jest.Mock).mockReturnValue({
            isCatalogLoading: false,
            rewardsCatalog: rewardsMock.rewardsCatalog,
            rewardsRecommendations: [],
        });
    });

    it('should renders correctly with FAQ', () => {
        render(<RewardsRoster faq={faq as any} points={0} isLoading={false} isError={false} />);
        screen.getByText(/how rewards work/i);
        screen.getByText(/use points, get rewarded/i);
        screen.getByText(/earn points, score rewards/i);
    });

    describe('header', () => {
        beforeEach(() => {
            (useRewards as jest.Mock).mockReturnValue({
                isCatalogLoading: false,
                rewardsCatalog: null,
                rewardsRecommendations: [],
            });
        });

        it('should render correctly with points', () => {
            const { container } = render(
                <RewardsRoster faq={emptyFaq as any} points={1500} isLoading={false} isError={false} />
            );
            expect(container).toMatchSnapshot();
        });

        it('should render correctly with loading state', () => {
            const { container } = render(
                <RewardsRoster faq={emptyFaq as any} points={0} isLoading={true} isError={false} />
            );
            expect(container).toMatchSnapshot();
        });

        it('should render correctly with error state', () => {
            const { container } = render(
                <RewardsRoster faq={emptyFaq as any} points={0} isLoading={false} isError={true} />
            );
            expect(container).toMatchSnapshot();
        });
    });
});
