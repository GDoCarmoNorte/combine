import React from 'react';
import { render, screen } from '@testing-library/react';

import ModalFooterContent from '../../../../components/organisms/modifiersSelection/modalFooterContent';

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn().mockReturnValue({}),
}));

describe('ModalFooterContent', () => {
    it('should render component', () => {
        render(<ModalFooterContent onSaveAndCloseClick={jest.fn()} />);

        expect(screen.getByRole('button', { name: 'Save & Close' })).toBeInTheDocument();
    });

    it('should render component with selections', () => {
        render(<ModalFooterContent onSaveAndCloseClick={jest.fn()} selections="Celery" />);

        expect(screen.getByText(/Selections:/)).toBeInTheDocument();
        expect(screen.getByText(/Celery/)).toBeInTheDocument();
    });
});
