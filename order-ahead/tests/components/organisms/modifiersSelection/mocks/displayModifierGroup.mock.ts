import { IDisplayModifierGroup } from '../../../../../redux/types';

const modifierGroup: IDisplayModifierGroup = {
    sequence: 1,
    displayName: 'Cheese',
    minQuantity: 1,
    maxQuantity: 1,
    modifierGroupId: 'ModifierGroup-9124',
    modifiers: [
        {
            displayName: 'Swiss',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 0,
                displayName: 'Swiss',
                minQuantity: 0,
                maxQuantity: 1,
                price: 0,
                productId: 'Modifier-3751',
                quantity: 0,
            },
        },
        {
            displayName: 'Bleu Cheese Crumbles',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 1,
                displayName: 'Bleu Cheese Crumbles',
                minQuantity: 0,
                maxQuantity: 1,
                price: 0,
                productId: 'Modifier-3697',
                quantity: 1,
            },
        },
        {
            displayName: 'Cheddar',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 0,
                displayName: 'Cheddar',
                minQuantity: 0,
                maxQuantity: 1,
                price: 0,
                productId: 'Modifier-3752',
                quantity: 0,
            },
        },
        {
            displayName: 'Camembert Cheese',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 0,
                displayName: 'Camembert Cheese',
                minQuantity: 0,
                maxQuantity: 1,
                price: 0,
                productId: 'Modifier-5062',
                quantity: 0,
            },
        },
        {
            displayName: 'American',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 0,
                displayName: 'American',
                minQuantity: 0,
                maxQuantity: 1,
                price: 0,
                productId: 'Modifier-3698',
                quantity: 0,
            },
        },
        {
            displayName: 'Pepper Jack',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 0,
                displayName: 'Pepper Jack',
                minQuantity: 0,
                maxQuantity: 1,
                price: 0,
                productId: 'Modifier-3753',
                quantity: 0,
            },
        },
    ],
};

export default modifierGroup;
