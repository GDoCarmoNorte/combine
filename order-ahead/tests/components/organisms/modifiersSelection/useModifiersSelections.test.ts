import useModifiersSelection, {
    getInitialModifiersToDisplayIds,
    getNewModifiersToDisplayIds,
} from '../../../../components/organisms/modifiersSelection/useModifiersSelection';
import { act, renderHook } from '@testing-library/react-hooks';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import modifierGroup from './mocks/displayModifierGroup.mock';
import modifierGroupWithTwoRows from './mocks/displayModifierGroupWithTwoRows';
import { IDisplayModifierGroup } from '../../../../redux/types';
import { useProductTallyModifierGroup } from '../../../../redux/hooks/pdp';

jest.mock('@material-ui/core/useMediaQuery');
jest.mock('../../../../redux/hooks/pdp');

describe('useModifiersSelection', () => {
    beforeEach(() => {
        (useMediaQuery as jest.Mock).mockReturnValue(false);
        (useProductTallyModifierGroup as jest.Mock).mockReturnValue({ totalCount: 0 });
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should return correct modifier selections', () => {
        const { result } = renderHook(() => useModifiersSelection(modifierGroup, 'productId'));

        const expected = {
            limitText: '(Select one)',
            modifiersToDisplay: [
                {
                    displayName: 'Swiss',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Swiss',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3751',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Bleu Cheese Crumbles',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 1,
                        displayName: 'Bleu Cheese Crumbles',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3697',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'Cheddar',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Cheddar',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3752',
                        quantity: 0,
                    },
                },
            ],
            selectionType: 'single',
            shouldShowViewAllBtn: true,
            selectedModifiersText: 'Bleu Cheese Crumbles',
            isModalOpened: false,
            isMinimumAmountReached: true,
            onViewAll: expect.any(Function),
            onCloseModal: expect.any(Function),
        };

        expect(result.current).toEqual(expected);
    });

    it('should return correct modifier selections for mobile breakpoint', () => {
        (useMediaQuery as jest.Mock).mockReturnValue(true);

        const { result } = renderHook(() => useModifiersSelection(modifierGroup, 'productId'));

        const expected = {
            limitText: '(Select one)',
            modifiersToDisplay: [
                {
                    displayName: 'Swiss',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Swiss',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-3751',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Bleu Cheese Crumbles',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 1,
                        displayName: 'Bleu Cheese Crumbles',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-3697',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'Cheddar',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Cheddar',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-3752',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Camembert Cheese',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Camembert Cheese',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-5062',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'American',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'American',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-3698',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Pepper Jack',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Pepper Jack',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-3753',
                        quantity: 0,
                    },
                },
            ],
            selectionType: 'single',
            shouldShowViewAllBtn: false,
            selectedModifiersText: 'Bleu Cheese Crumbles',
            isModalOpened: false,
            isMinimumAmountReached: true,
            onViewAll: expect.any(Function),
            onCloseModal: expect.any(Function),
        };

        expect(result.current).toEqual(expected);
    });

    it('should return correct modifier selections for desktop breakpoint for two rows', () => {
        const { result } = renderHook(() => useModifiersSelection(modifierGroupWithTwoRows, 'productId'));

        const expected = {
            limitText: '(Up to 8)',
            modifiersToDisplay: [
                {
                    displayName: 'Bleu Cheese Crumbles 2',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 1,
                        displayName: 'Bleu Cheese Crumbles 2',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-36972',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'Bleu Cheese Crumbles',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 1,
                        displayName: 'Bleu Cheese Crumbles',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3697',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'Cheddar',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Cheddar',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3752',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Camembert Cheese',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Camembert Cheese',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-5062',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'American',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'American',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3698',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Pepper Jack',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Pepper Jack',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3753',
                        quantity: 0,
                    },
                },
                {
                    displayName: 'Swiss 2',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Swiss 2',
                        minQuantity: 0,
                        maxQuantity: 1,
                        price: 0,
                        productId: 'Modifier-37512',
                        quantity: 0,
                    },
                },
            ],
            selectionType: 'multi',
            shouldShowViewAllBtn: true,
            selectedModifiersText: 'Bleu Cheese Crumbles, Bleu Cheese Crumbles 2',
            isModalOpened: false,
            isMinimumAmountReached: true,
            onViewAll: expect.any(Function),
            onCloseModal: expect.any(Function),
        };

        expect(result.current).toEqual(expected);
    });

    it('should display modal on onViewAll click', () => {
        const { result } = renderHook(() => useModifiersSelection(modifierGroup, 'productId'));

        act(() => {
            result.current.onViewAll();
        });

        expect(result.current.isModalOpened).toBe(true);
    });

    it('should not return selectedModifiersText if no selected modifiers', () => {
        const { result } = renderHook(() =>
            useModifiersSelection(
                {
                    sequence: 1,
                    displayName: 'Cheese',
                    minQuantity: 1,
                    maxQuantity: 1,
                    modifierGroupId: 'ModifierGroup-9124',
                    modifiers: [
                        {
                            displayName: 'Swiss',
                            displayProductDetails: {
                                calories: null,
                                defaultQuantity: 0,
                                displayName: 'Swiss',
                                minQuantity: 0,
                                maxQuantity: 1,
                                price: 0,
                                productId: 'Modifier-3751',
                                quantity: 0,
                            },
                        },
                    ],
                },
                'productId'
            )
        );

        act(() => {
            result.current.onViewAll();
        });

        expect(result.current.isMinimumAmountReached).toEqual(false);

        expect(result.current.selectedModifiersText).toBe(null);
    });

    it('should change modifiers to display if a modifier was selected in the modal', () => {
        const { result, rerender } = renderHook((newGroup: IDisplayModifierGroup) => {
            return useModifiersSelection(newGroup || modifierGroup, 'productId');
        });

        let newGroup;

        act(() => {
            newGroup = {
                ...modifierGroup,
                modifiers: modifierGroup.modifiers.map((it) =>
                    it.displayName === 'Camembert Cheese'
                        ? { ...it, displayProductDetails: { ...it.displayProductDetails, quantity: 1 } }
                        : it
                ),
            };
        });

        rerender(newGroup);

        act(() => {
            result.current.onCloseModal();
        });

        expect(result.current.isModalOpened).toBe(false);
        expect(result.current.modifiersToDisplay).toEqual([
            {
                displayName: 'Camembert Cheese',
                displayProductDetails: {
                    calories: null,
                    defaultQuantity: 0,
                    displayName: 'Camembert Cheese',
                    maxQuantity: 1,
                    minQuantity: 0,
                    price: 0,
                    productId: 'Modifier-5062',
                    quantity: 1,
                },
            },
            {
                displayName: 'Bleu Cheese Crumbles',
                displayProductDetails: {
                    calories: null,
                    defaultQuantity: 1,
                    displayName: 'Bleu Cheese Crumbles',
                    maxQuantity: 1,
                    minQuantity: 0,
                    price: 0,
                    productId: 'Modifier-3697',
                    quantity: 1,
                },
            },
            {
                displayName: 'Cheddar',
                displayProductDetails: {
                    calories: null,
                    defaultQuantity: 0,
                    displayName: 'Cheddar',
                    maxQuantity: 1,
                    minQuantity: 0,
                    price: 0,
                    productId: 'Modifier-3752',
                    quantity: 0,
                },
            },
        ]);
    });
});

describe('getInitialModifiersToDisplayIds', () => {
    it('should display all modifiers if length to display is the same as all modifiers length', () => {
        const modifiers = modifierGroup.modifiers;

        const result = getInitialModifiersToDisplayIds(modifiers, 6);

        expect(result).toEqual([
            'Modifier-3751',
            'Modifier-3697',
            'Modifier-3752',
            'Modifier-5062',
            'Modifier-3698',
            'Modifier-3753',
        ]);
    });

    it('should display all modifiers if length to display is more than all modifiers length', () => {
        const modifiers = modifierGroup.modifiers;

        const result = getInitialModifiersToDisplayIds(modifiers, 8);

        expect(result).toEqual([
            'Modifier-3751',
            'Modifier-3697',
            'Modifier-3752',
            'Modifier-5062',
            'Modifier-3698',
            'Modifier-3753',
        ]);
    });

    it('should display all selected modifiers if all modifiers length is more than length to display', () => {
        const result = getInitialModifiersToDisplayIds(
            [
                {
                    displayName: 'Cheddar',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Cheddar',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3752',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'Camembert Cheese',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Camembert Cheese',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-5062',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'American',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'American',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3698',
                        quantity: 1,
                    },
                },
                {
                    displayName: 'Pepper Jack',
                    displayProductDetails: {
                        calories: null,
                        defaultQuantity: 0,
                        displayName: 'Pepper Jack',
                        maxQuantity: 1,
                        minQuantity: 0,
                        price: 0,
                        productId: 'Modifier-3753',
                        quantity: 1,
                    },
                },
            ],
            3
        );

        expect(result).toEqual(['Modifier-3752', 'Modifier-5062', 'Modifier-3698']);
    });

    it('should display selected and not selected modifiers if all modifiers length is more than length to display', () => {
        const modifiers = modifierGroup.modifiers;

        const result = getInitialModifiersToDisplayIds(modifiers, 5);

        expect(result).toEqual(['Modifier-3751', 'Modifier-3697', 'Modifier-3752', 'Modifier-5062', 'Modifier-3698']);
    });
});

describe('getNewModifiersToDisplayIds', () => {
    it('should remain the same ids if no new modifiers were selected', () => {
        const modifiers = modifierGroup.modifiers;

        const result = getNewModifiersToDisplayIds(modifiers, ['Modifier-3697', 'Modifier-3752', 'Modifier-3752']);

        expect(result).toEqual(['Modifier-3697', 'Modifier-3752', 'Modifier-3752']);
    });

    it('should swap selected modifier with unselected', () => {
        const modifiers = modifierGroup.modifiers;

        const result = getNewModifiersToDisplayIds(modifiers, ['Modifier-3751', 'Modifier-3752', 'Modifier-3752']);

        expect(result).toEqual(['Modifier-3697', 'Modifier-3752', 'Modifier-3752']);
    });
});
