import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { createNextState } from '@reduxjs/toolkit';

import AllSaucesModifiersModal from '../../../../components/organisms/modifiersSelection/allSaucesModifiersModal';
import { useGlobalProps } from '../../../../redux/hooks';
import sauceModifierGroup from './mocks/sauceModifierGroup.mock';
import { ModifierCardContainer } from '../../../../components/organisms/modifierCardContainer/modifierCardContainer';
import { ProductIntensityEnum } from '../../../../redux/types';

jest.mock('../../../../components/organisms/modifierCardContainer/modifierCardContainer', () => ({
    // eslint-disable-next-line react/display-name
    ModifierCardContainer: ({ displayProduct }) => <div>{displayProduct?.displayName}</div>,
}));

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(),
    usePdp: jest.fn().mockReturnValue({
        pdpTallyItem: {},
        useIntensity: jest.fn(),
        actions: {
            onModifierChange: jest.fn(),
        },
    }),
}));

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/hooks/pdp', () => ({
    useProductTallyModifierGroup: jest.fn().mockReturnValue({ totalCount: 1 }),
    useBwwDisplayModifierGroup: jest.fn().mockReturnValue({}),
    useBwwDisplayModifierGroupsForModifier: jest.fn(),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({
        itemModifierGroups: [{}, {}],
    }),
    useDefaultModifiers: () => [],
    useSelectedModifiers: () => [],
    useNoSauceDomainProduct: () => jest.fn(),
}));

ModifierCardContainer;

jest.mock('../../../../common/hooks/useModifierCardClick', () => jest.fn());

describe('AllSaucesModifiersModal', () => {
    beforeEach(() => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
    });

    it('should render component', () => {
        render(
            <AllSaucesModifiersModal
                limitText="(Select one)"
                isOpen={true}
                onClose={jest.fn()}
                selectionType="single"
                modifierGroup={sauceModifierGroup}
            />
        );

        screen.getByLabelText('Recommended category');
        screen.getByLabelText(`${ProductIntensityEnum.MILD} category`);
        screen.getByLabelText(`${ProductIntensityEnum.MEDIUM} category`);
        screen.getByLabelText(`${ProductIntensityEnum.HOT} category`);
        screen.getByLabelText(`${ProductIntensityEnum.WILD} category`);
        screen.getByLabelText('uncategorised');
    });

    it('should not render "Recommended" category if no recommended sauces', () => {
        const saucesWithoutRecommended = createNextState(sauceModifierGroup, (newGroup) => {
            newGroup.modifiers.forEach((it) => {
                // eslint-disable-next-line no-param-reassign
                it.displayProductDetails.isRecommended = false;
            });
        });

        render(
            <AllSaucesModifiersModal
                limitText="(Select one)"
                isOpen={true}
                onClose={jest.fn()}
                selectionType="single"
                modifierGroup={saucesWithoutRecommended}
            />
        );

        expect(screen.queryByLabelText('Recommended category')).not.toBeInTheDocument();
    });

    it('should show only filtered categories', () => {
        render(
            <AllSaucesModifiersModal
                limitText="(Select one)"
                isOpen={true}
                onClose={jest.fn()}
                selectionType="single"
                modifierGroup={sauceModifierGroup}
            />
        );

        userEvent.click(screen.getAllByText(ProductIntensityEnum.HOT)[0]);

        screen.getByLabelText(`${ProductIntensityEnum.HOT} category`);
        expect(screen.queryByLabelText('Recommended category')).not.toBeInTheDocument();
        expect(screen.queryByLabelText(`${ProductIntensityEnum.MILD} category`)).not.toBeInTheDocument();
        expect(screen.queryByLabelText(`${ProductIntensityEnum.MEDIUM} category`)).not.toBeInTheDocument();
        expect(screen.queryByLabelText(`${ProductIntensityEnum.WILD} category`)).not.toBeInTheDocument();
        expect(screen.queryByLabelText('uncategorised')).not.toBeInTheDocument();

        userEvent.click(screen.getAllByText(ProductIntensityEnum.HOT)[0]);
        expect(screen.getByLabelText(`${ProductIntensityEnum.MILD} category`)).toBeInTheDocument();
        expect(screen.getByLabelText(`${ProductIntensityEnum.MEDIUM} category`)).toBeInTheDocument();
        expect(screen.getByLabelText(`${ProductIntensityEnum.WILD} category`)).toBeInTheDocument();
        expect(screen.getByLabelText('uncategorised')).toBeInTheDocument();
    });
});
