import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import { SauceModifiersSelection } from '../../../../components/organisms/modifiersSelection';
import { useGlobalProps, usePdp } from '../../../../redux/hooks';
import sauceModifierGrop from './mocks/sauceModifierGroup.mock';
import { useBwwSaucesDisplayModifierGroup } from '../../../../redux/hooks/pdp';

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(),
    usePdp: jest.fn().mockReturnValue({
        pdpTallyItem: {},
        useIntensity: jest.fn(),
        actions: { onSauceOnSideChange: jest.fn(), onModifierChange: jest.fn() },
    }),
}));

jest.mock('../../../../redux/hooks/pdp', () => ({
    useProductTallyModifierGroup: jest.fn().mockReturnValue({ totalCount: 1 }),
    useBwwDisplayModifierGroupsForModifier: jest.fn(),
    useBwwDisplayModifierGroup: jest.fn(),
    useBwwSaucesDisplayModifierGroup: jest.fn(),
}));

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({
        itemModifierGroups: [{}, {}],
    }),
    useDefaultModifiers: () => [],
    useSelectedModifiers: () => [],
    useNoSauceDomainProduct: () => jest.fn(),
}));

describe('SauceModifiersSelection', () => {
    it('should render components', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwSaucesDisplayModifierGroup as jest.Mock).mockReturnValue(sauceModifierGrop);

        const { container } = render(
            <SauceModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render component with on side checkbox', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwSaucesDisplayModifierGroup as jest.Mock).mockReturnValue({
            ...sauceModifierGrop,
            hasOnSideOption: true,
        });

        render(
            <SauceModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />
        );

        expect(screen.getByRole('checkbox', { name: 'Sauce on the side', checked: false })).toBeInTheDocument();
    });

    it('should render component with checked on side checkbox', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwSaucesDisplayModifierGroup as jest.Mock).mockReturnValue({
            ...sauceModifierGrop,
            hasOnSideOption: true,
            isOnSideChecked: true,
        });

        render(
            <SauceModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />
        );

        expect(screen.getByRole('checkbox', { name: 'Sauce on the side', checked: true })).toBeInTheDocument();
    });

    it('should fire onSauceOnSideChange action on "on side" checkbox click', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwSaucesDisplayModifierGroup as jest.Mock).mockReturnValue({
            ...sauceModifierGrop,
            hasOnSideOption: true,
        });
        const onSauceOnSideChangeMock = jest.fn();
        (usePdp as jest.Mock).mockReturnValue({
            useIntensity: jest.fn(),
            actions: { onSauceOnSideChange: onSauceOnSideChangeMock },
        });

        render(
            <SauceModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />
        );
        const checkbox = screen.getByRole('checkbox', { name: 'Sauce on the side', checked: false });

        fireEvent.click(checkbox);

        expect(onSauceOnSideChangeMock).toHaveBeenCalledWith({ productGroupId: 'productGroupId' });
    });
});
