import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ModifiersSelection from '../../../../components/organisms/modifiersSelection';
import useModifiersSelection from '../../../../components/organisms/modifiersSelection/useModifiersSelection';
import { useGlobalProps } from '../../../../redux/hooks';
import displayModifierGroup from './mocks/displayModifierGroup.mock';
import { useBwwDisplayModifierGroup, useBwwDisplayModifierGroupsForModifier } from '../../../../redux/hooks/pdp';

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

jest.mock('../../../../redux/hooks', () => ({
    useGlobalProps: jest.fn(),
    usePdp: jest.fn().mockReturnValue({
        pdpTallyItem: {},
        useIntensity: jest.fn(),
        actions: {
            onModifierChange: jest.fn(),
        },
    }),
}));

jest.mock('../../../../redux/hooks/pdp', () => ({
    useProductTallyModifierGroup: jest.fn().mockReturnValue({ totalCount: 1 }),
    useBwwDisplayModifierGroup: jest.fn(),
    useBwwDisplayModifierGroupsForModifier: jest.fn(),
}));

jest.mock('react-redux', () => ({
    useDispatch: () => jest.fn(),
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({
        itemModifierGroups: [{}, {}],
    }),
    useDefaultModifiers: () => [],
    useSelectedModifiers: () => [],
    useNoSauceDomainProduct: () => jest.fn(),
}));

jest.mock('../../../../components/organisms/modifiersSelection/useModifiersSelection', () =>
    jest
        .fn()
        .mockImplementation(
            jest.requireActual('../../../../components/organisms/modifiersSelection/useModifiersSelection').default
        )
);

describe('ModifiersSelection', () => {
    it('should render component with modifiers', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(displayModifierGroup);

        const { container } = render(
            <ModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render component with modifiers one of with quantity=0', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(displayModifierGroup);
        (useBwwDisplayModifierGroupsForModifier as jest.Mock).mockReturnValue([displayModifierGroup]);

        render(<ModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />);

        expect(screen.getAllByText('Modify')).toHaveLength(3);
        userEvent.click(screen.getAllByText('Modify')[0]);

        screen.getByText('Save & Close', { selector: 'button span' });
        userEvent.click(screen.getByText('Save & Close', { selector: 'button span' }));
    });

    it('should render AllModifiersModal', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(displayModifierGroup);
        (useBwwDisplayModifierGroupsForModifier as jest.Mock).mockReturnValue([displayModifierGroup]);

        render(<ModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />);

        expect(screen.getAllByText('Modify')).toHaveLength(3);

        expect(screen.getAllByText('View All', { exact: false })).toHaveLength(2);
        userEvent.click(screen.getAllByText('View All', { exact: false })[0]);

        expect(screen.getAllByText('Modify')).toHaveLength(10);

        userEvent.click(screen.getAllByText('Modify')[5]);

        screen.getAllByText('Save & Close', { exact: false });
        userEvent.click(screen.getAllByText('Save & Close', { exact: false })[3]);

        screen.getAllByText('Save & Close', { exact: false });
        userEvent.click(screen.getAllByText('Save & Close', { exact: false })[0]);

        expect(screen.getAllByText('Modify')).toHaveLength(3);

        const child = screen.queryByText('Save & Close');

        expect(child).not.toBeInTheDocument();
    });

    it('should not render section if modifiers are empty', () => {
        (useGlobalProps as jest.Mock).mockReturnValue({
            modifierItemsById: { default: { fields: { image: 'mockImage' } } },
        });
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue({ ...displayModifierGroup, modifiers: [] });
        (useBwwDisplayModifierGroupsForModifier as jest.Mock).mockReturnValue([]);
        (useModifiersSelection as jest.Mock).mockReturnValue({ modifiersToDisplay: [] });

        render(<ModifiersSelection title="sauce selection" productGroupId="productGroupId" productId="productId" />);

        expect(screen.queryByText(/sauce selection/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/view all/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/modify/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/add to bag/i)).not.toBeInTheDocument();
    });
});
