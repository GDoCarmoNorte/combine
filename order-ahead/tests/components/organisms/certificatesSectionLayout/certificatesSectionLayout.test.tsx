import React from 'react';
import { render } from '../../../utils';
import CertificatesSectionLayout from '../../../../components/organisms/withCertificatesSectionLayout';

const TestComponent = (): JSX.Element => <div>Test Component</div>;

const mockProps = {
    bgImageUrl: '/brands/bww/recommended-just-for-you.png',
    headingFirstLineText: 'recommended',
    headingSecondLineText: 'just',
    headingThirdLineText: 'for you',
};

describe('withCertificatesSectionLayout', () => {
    it('should render correctly the component returned by withCertificatesSectionLayout', () => {
        const Component = (
            <CertificatesSectionLayout {...mockProps}>
                <TestComponent />
            </CertificatesSectionLayout>
        );
        const { container } = render(Component);
        expect(container).toMatchSnapshot();
    });
});
