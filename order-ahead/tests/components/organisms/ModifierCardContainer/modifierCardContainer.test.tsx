import React from 'react';
import { shallow, mount } from 'enzyme';
import { ModifierCardContainer } from '../../../../components/organisms/modifierCardContainer/modifierCardContainer';
import displayProduct from '../../../mocks/displayProduct.mock';
import TextWithDotSeparator from '../../../../components/atoms/TextWithDotSeparator';
import { IDisplayProduct } from '../../../../redux/types';
import { usePdp } from '../../../../redux/hooks';
import SauceIntensity from '../../../../components/atoms/SauceIntensity/SauceIntensity';

const modifierItemId = 'arb-itm-000-021';

const modifierItemsById: any = {
    [modifierItemId]: {
        sys: {},
        fields: { name: 'Beef n Cheddar', productId: 'arb-itm-000-021' },
    },
};

jest.mock('../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

jest.mock('../../../../common/hooks/useProductOrderAheadAvailability', () => ({
    useProductOrderAheadAvailability: () => false,
}));

jest.mock('../../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn().mockReturnValue({}),
}));

jest.mock('../../../../redux/hooks/pdp/', () => ({
    useDisplayModifierGroupsByProductId: jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../redux/hooks/usePdp');

describe('ModifierCardContainer', () => {
    beforeAll(() => {
        (usePdp as jest.Mock).mockReturnValue({
            useIntensity: jest.fn().mockReturnValue(null),
        });
    });

    it('should render modifierCard correctly', () => {
        const wrapper = shallow(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProduct}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should not render SauceIntensity on modifierCard', () => {
        const wrapper = shallow(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProduct}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(SauceIntensity)).toHaveLength(0);
    });

    it('should render ModifierCardContainer correctly with size type', () => {
        const wrapper = shallow(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="size"
                displayProduct={{
                    ...displayProduct,
                    displayProductDetails: { ...displayProduct.displayProductDetails, selections: [1, 2] as any },
                }}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should render ModifierCardContainer with size type when no selected', () => {
        const wrapper = shallow(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="size"
                displayProduct={{
                    ...displayProduct,
                    displayProductDetails: {
                        ...displayProduct.displayProductDetails,
                        quantity: 0,
                        selections: [1, 2] as any,
                    },
                }}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should not render unavailable modifierCard with hideOrderAheadUnavailable', () => {
        const wrapper = shallow(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProduct}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                hideOrderAheadUnavailable={true}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should display 0 calories', () => {
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={
                    {
                        ...displayProduct,
                        displayProductDetails: { ...displayProduct.displayProductDetails, calories: 0 },
                    } as IDisplayProduct
                }
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should render a special symbol [ • ] when price and calories exist', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 3.29,
                calories: 550,
            },
        };
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should render calories without special symbol [ • ] when price is 0', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 0,
                calories: 500,
            },
        };
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should not render price for preselected modifier, quantity === defaultQuantity', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 1,
                defaultQuantity: 1,
            },
        };
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should not render price for preselected modifier, quantity < defaultQuantity', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 0,
                defaultQuantity: 1,
            },
        };
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should render price for modifier, defaultQuantity = 0', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 0,
                defaultQuantity: 0,
            },
        };
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should render price for modifier, quantity > defaultQuantity', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 2,
                defaultQuantity: 1,
            },
        };
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper.find(TextWithDotSeparator)).toMatchSnapshot();
    });

    it('should call onChange after ModifierCardContainer with selectorType="default" after cardTop click', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 0,
                defaultQuantity: 1,
            },
        };
        const onChange = jest.fn();
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                onChange={onChange}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        const cardTop = wrapper.find('.cardTop');
        cardTop.simulate('click');

        expect(onChange).toBeCalledTimes(1);
    });

    it('should call onChange after ModifierCardContainer with selectorType="default" after cardTop keypress', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 0,
                defaultQuantity: 1,
            },
        };
        const onChange = jest.fn();
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="default"
                displayProduct={displayProductMock}
                onChange={onChange}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        const cardTop = wrapper.find('.cardTop');
        cardTop.simulate('keypress', { key: 'Enter' });

        expect(onChange).toBeCalledTimes(1);
    });

    it('should render ModifierCardContainer with selectorType="quantity" correctly', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 2,
                defaultQuantity: 1,
                maxQuantity: 5,
            },
        };

        const wrapper = shallow(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="quantity"
                displayProduct={displayProductMock}
                canIncrement={true}
                canDecrement={false}
                onChange={jest.fn()}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        expect(wrapper).toMatchSnapshot();
    });

    it('should call onChange after ModifierCardContainer with selectorType="default" after inputMinus click', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 1,
                defaultQuantity: 1,
            },
        };
        const onChange = jest.fn();
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="quantity"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={true}
                onChange={onChange}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        const inputMinus = wrapper.find('.input').at(0);
        inputMinus.simulate('click');

        expect(onChange).toBeCalledTimes(1);
    });

    it('should call onChange after ModifierCardContainer with selectorType="default" after inputMinus keypress', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 1,
                defaultQuantity: 1,
            },
        };
        const onChange = jest.fn();
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="quantity"
                displayProduct={displayProductMock}
                canIncrement={false}
                canDecrement={true}
                onChange={onChange}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        const inputMinus = wrapper.find('.input').at(0);
        inputMinus.simulate('keypress', { key: 'Enter' });

        expect(onChange).toBeCalledTimes(1);
    });

    it('should call onChange after ModifierCardContainer with selectorType="default" after inputPlus click', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 1,
                defaultQuantity: 1,
                maxQuantity: 2,
            },
        };
        const onChange = jest.fn();
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="quantity"
                displayProduct={displayProductMock}
                canIncrement={true}
                canDecrement={false}
                onChange={onChange}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        const inputPlus = wrapper.find('.input').at(2);

        inputPlus.simulate('click');

        expect(onChange).toBeCalledTimes(1);
    });

    it('should call onChange after ModifierCardContainer with selectorType="default" after inputPlus keypress', () => {
        const displayProductMock = {
            ...displayProduct,
            displayProductDetails: {
                ...displayProduct.displayProductDetails,
                price: 100,
                calories: 500,
                quantity: 1,
                defaultQuantity: 1,
                maxQuantity: 2,
            },
        };
        const onChange = jest.fn();
        const wrapper = mount(
            <ModifierCardContainer
                modifierItemsById={modifierItemsById}
                selectorType="quantity"
                displayProduct={displayProductMock}
                canIncrement={true}
                canDecrement={false}
                onChange={onChange}
                sectionIndex={0}
                selectionType="multi"
            />
        );

        const inputPlus = wrapper.find('.input').at(2);
        inputPlus.simulate('keypress', { key: 'Enter' });

        expect(onChange).toBeCalledTimes(1);
    });
});
