export const modifierGroupWithModifiers = {
    displayName: 'Special Request On Traditional',
    minQuantity: 0,
    maxQuantity: 2,
    modifierGroupId: 'IDPModifierGroup-14071',
    modifiers: [
        {
            displayName: 'All Flats',
            displayProductDetails: {
                calories: null,
                defaultQuantity: 0,
                displayName: 'All Flats',
                minQuantity: 0,
                maxQuantity: 2,
                price: 1,
                productId: 'IDPModifier-18191',
                quantity: 0,
            },
        },
        {
            displayName: 'All Drums',
            displayProductDetails: {
                defaultQuantity: 0,
                displayName: 'All Drums',
                minQuantity: 0,
                maxQuantity: 2,
                price: 1,
                productId: 'IDPModifier-18192',
                quantity: 0,
            },
        },
    ],
};

export const modifierGroupWithoutModifiers = {
    displayName: 'Special Request On Traditional',
    minQuantity: 0,
    maxQuantity: 2,
    modifierGroupId: 'IDPModifierGroup-14071',
    modifiers: [],
};
