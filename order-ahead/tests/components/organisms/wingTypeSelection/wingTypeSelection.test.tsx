import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import WingTypeSelection from '../../../../components/organisms/wingTypeSelection';
import { useBwwDisplayModifierGroup } from '../../../../redux/hooks/pdp';
import { modifierGroupWithModifiers, modifierGroupWithoutModifiers } from './wingTypeSelectrion.mock';
import { usePdp } from '../../../../redux/hooks';

jest.mock('../../../../redux/hooks/pdp', () => ({
    useBwwDisplayModifierGroup: jest.fn(),
}));

jest.mock('../../../../redux/hooks', () => ({
    usePdp: jest.fn().mockReturnValue({
        actions: { onModifierChange: jest.fn() },
    }),
}));

describe('wingTypeSelection', () => {
    it('Should render correctly', () => {
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(modifierGroupWithModifiers);

        render(<WingTypeSelection productGroupId={''} />);

        expect(screen.getAllByText(/TYPE/i)[0]).toBeInTheDocument(); // title
        expect(screen.getAllByText(/Mixed/i)[0]).toBeInTheDocument(); // subTitle
        expect(screen.getAllByText(/Mixed/i)[1]).toBeInTheDocument();
        expect(screen.getByText(/All Flats/i)).toBeInTheDocument();
        expect(screen.getByText(/All Drums/i)).toBeInTheDocument();
    });

    it('Should render empty component if modifiers not present', () => {
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(modifierGroupWithoutModifiers);

        render(<WingTypeSelection productGroupId={''} />);

        expect(screen.queryByText(/Type/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/Mixed/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/All Flats/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/All Drums/i)).not.toBeInTheDocument();
    });

    it('Should handle modifiers click', () => {
        (useBwwDisplayModifierGroup as jest.Mock).mockReturnValue(modifierGroupWithModifiers);

        const onModifierChangeStub = jest.fn();
        (usePdp as jest.Mock).mockReturnValueOnce({
            actions: {
                onModifierChange: onModifierChangeStub,
            },
        });

        render(<WingTypeSelection productGroupId={''} />);

        fireEvent.click(screen.getByText(/All Flats/i));

        expect(onModifierChangeStub.mock.calls[0][0].modifierId).toEqual(
            modifierGroupWithModifiers.modifiers[0].displayProductDetails.productId
        );

        fireEvent.click(screen.getAllByText(/Mixed/i)[1]);

        expect(onModifierChangeStub.mock.calls[1][0].modifierId).toEqual('');
    });
});
