import { render, screen } from '@testing-library/react';
import React from 'react';
import LoyaltyPointsBanner from '../../../../components/organisms/loyaltyPointsBanner';
import { isRewardsOn } from '../../../../lib/getFeatureFlags';
import { useAccount, useLoyalty, useRewards } from '../../../../redux/hooks';

jest.mock('../../../../redux/hooks', () => ({
    useAccount: jest.fn().mockReturnValue({ account: { firstName: 'Frank' } }),
    useLoyalty: jest.fn().mockReturnValue({ loyalty: { pointsBalance: 0 } }),
    useRewards: jest.fn().mockReturnValue({ totalCount: 0 }),
}));

jest.mock('../../../../lib/getFeatureFlags', () => ({
    isRewardsOn: jest.fn().mockReturnValue(true),
}));

describe('LoyaltyPointsBanner', () => {
    it('should render banner with 0 rewards and 0 points', () => {
        render(<LoyaltyPointsBanner />);

        screen.getByText('Hi Frank! Start earning points to get rewards.');
    });

    it('should render banner with single reward', () => {
        (useRewards as jest.Mock).mockReturnValueOnce({ totalCount: 1 });
        render(<LoyaltyPointsBanner />);

        screen.getByText(/Hi Frank! You’ve got/);
        screen.getByText(/1 reward/);
    });

    it('should render banner with multiple rewards', () => {
        (useRewards as jest.Mock).mockReturnValueOnce({ totalCount: 5 });
        render(<LoyaltyPointsBanner />);

        screen.getByText(/Hi Frank! You’ve got/);
        screen.getByText(/5 rewards/);
    });

    it('should render banner with 50 points', () => {
        (useLoyalty as jest.Mock).mockReturnValueOnce({ loyalty: { pointsBalance: 50 } });
        render(<LoyaltyPointsBanner />);

        screen.getByText(/Hi Frank! You’ve got/);
        screen.getByText(/50 points/);
    });

    it('should render banner with 1 reward and 50 points', () => {
        (useRewards as jest.Mock).mockReturnValueOnce({ totalCount: 1 });
        (useLoyalty as jest.Mock).mockReturnValueOnce({ loyalty: { pointsBalance: 50 } });
        render(<LoyaltyPointsBanner />);

        screen.getByText(/Hi Frank! You’ve got/);
        screen.getByText(/1 reward/);
        screen.getByText(/50 pts/);
    });

    it('should render banner with 2 rewards and 50 points', () => {
        (useRewards as jest.Mock).mockReturnValueOnce({ totalCount: 2 });
        (useLoyalty as jest.Mock).mockReturnValueOnce({ loyalty: { pointsBalance: 50 } });
        render(<LoyaltyPointsBanner />);

        screen.getByText(/Hi Frank! You’ve got/);
        screen.getByText(/2 rewards/);
        screen.getByText(/50 pts/);
    });

    it('should not render banner for not auth user', () => {
        (useAccount as jest.Mock).mockReturnValueOnce({ account: null });
        const { container } = render(<LoyaltyPointsBanner />);
        expect(container).toBeEmptyDOMElement();
    });

    it('should not render banner when isRewardsOn = false', () => {
        (isRewardsOn as jest.Mock).mockReturnValueOnce(false);
        const { container } = render(<LoyaltyPointsBanner />);
        expect(container).toBeEmptyDOMElement();
    });
});
