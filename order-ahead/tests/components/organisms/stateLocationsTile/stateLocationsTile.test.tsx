import React from 'react';
import { render, screen } from '@testing-library/react';
import { allLocationsMock } from './stateLocations.mock';
import StateLocationsTile from '../../../../components/organisms/locations/stateLocationsTile';

const countryMock = allLocationsMock.locationsByCountry[0];
const countryMockMock = countryMock.countryCode;
const stateMock = countryMock.statesOrProvinces[0];

describe('StateLocationsTile component', () => {
    it('should contain number of state locations', () => {
        render(<StateLocationsTile stateLocationDetails={stateMock} countryCode={countryMockMock} />);

        expect(screen.getByText(`${stateMock.count.toLocaleString()} ${stateMock.name} locations`)).toBeInTheDocument();
    });

    it('should contain number of state locations with correct title', () => {
        const stateWithOneLocationMock = {
            ...stateMock,
            count: 1,
        };
        render(<StateLocationsTile stateLocationDetails={stateWithOneLocationMock} countryCode={countryMockMock} />);

        expect(
            screen.getByText(`${stateWithOneLocationMock.count.toLocaleString()} ${stateMock.name} location`)
        ).toBeInTheDocument();
    });

    it('should display cities', () => {
        render(<StateLocationsTile stateLocationDetails={stateMock} countryCode={countryMockMock} />);

        expect(screen.getByText(/Minneapolis/i)).toBeInTheDocument();
        expect(screen.getByText(/Test/i)).toBeInTheDocument();
    });
});
