import React from 'react';
import { render, screen } from '@testing-library/react';

import logger from '../../../../common/services/logger';
import ErrorBoundary from '../../../../components/organisms/errorBoundary';

jest.mock('../../../../common/services/logger');

describe('ErrorBoundary', () => {
    const realConsoleError = console.error;

    beforeAll(() => {
        // hide console errors during the tests
        console.error = jest.fn();
    });

    beforeEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        console.error = realConsoleError;
    });

    describe('when error', () => {
        const error = new Error('Some error');

        beforeEach(() => {
            const ComponentWithError = () => {
                throw new Error('Some error');
            };
            render(
                <ErrorBoundary>
                    <ComponentWithError />
                </ErrorBoundary>
            );
        });

        it('should log error and event', () => {
            expect(logger.logError).toHaveBeenCalledWith(error, { componentStack: expect.any(String) });
            expect(logger.logEvent).toHaveBeenCalledWith('error_boundary', {
                error,
                componentStack: expect.any(String),
            });
        });
        it('should render fallback', () => {
            expect(screen.getByRole('heading', { name: /Something went wrong./i })).toBeInTheDocument();
            expect(screen.getByText(/We are working on a solution to fix this problem./i)).toBeInTheDocument();
            expect(screen.queryByText(/Some error/i)).not.toBeInTheDocument();
        });
    });
    describe('when NO error', () => {
        beforeEach(() =>
            render(
                <ErrorBoundary>
                    <p>Content</p>
                </ErrorBoundary>
            )
        );
        it('should render children and no error', () => {
            screen.getByText('Content');

            expect(screen.queryByRole('heading')).not.toBeInTheDocument();
            expect(screen.queryByText(/We are working on a solution to fix this problem./i)).not.toBeInTheDocument();
            expect(screen.queryByText(/Some error/i)).not.toBeInTheDocument();
        });

        it('should NOT log error or event', () => {
            expect(logger.logError).not.toHaveBeenCalled();
            expect(logger.logEvent).not.toHaveBeenCalled();
        });
    });
});
