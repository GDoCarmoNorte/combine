export default {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '6ZHrL3tvzUeJO3aWJpGHIo',
        type: 'Entry',
        createdAt: '2020-07-23T16:03:30.035Z',
        updatedAt: '2020-07-31T18:54:46.115Z',
        environment: {
            sys: {
                id: 'feature_DBBP-804',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 5,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'infoBlockCard',
            },
        },
        locale: 'en-US',
    },
    fields: {
        headerText: 'Meat care expertise',
        subheaderText: 'Check that second text is empty/link on the gift-cards',
        buttonText: 'Food quality',
        icon: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3rTDA5yeKmbIypJUB7gwkt',
                type: 'Asset',
                createdAt: '2020-07-31T18:48:10.568Z',
                updatedAt: '2020-07-31T18:48:23.568Z',
                environment: {
                    sys: {
                        id: 'feature_DBBP-804',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                locale: 'en-US',
            },
            fields: {
                title: 'In the know - GEAR',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/3rTDA5yeKmbIypJUB7gwkt/de196f3bb20561020dc8bb6e0feb32d4/Vector.svg',
                    details: {
                        size: 4039,
                        image: {
                            width: 60,
                            height: 60,
                        },
                    },
                    fileName: 'Vector.svg',
                    contentType: 'image/svg+xml',
                },
            },
        },
        link: {
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '3Zipjy68UWHlOgZxG4IrcT',
                type: 'Entry',
                createdAt: '2020-07-01T14:43:36.770Z',
                updatedAt: '2020-07-19T14:37:44.383Z',
                environment: {
                    sys: {
                        id: 'feature_DBBP-804',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 3,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'page',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'DRIVE THRU DEALS',
                nameInUrl: 'drivethrudeals',
            },
        },
    },
};
