import React from 'react';
import { render } from '@testing-library/react';
import NewRelicSnippet from '../../../components/NewRelicSnippet';

describe('NewRelicSnippet', () => {
    describe('when no env variables', () => {
        it('should return null when where is no needed env variables', () => {
            const { container } = render(<NewRelicSnippet />);
            expect(container).toBeEmptyDOMElement();
        });
    });
    describe('when env variables defined', () => {
        let renderResult;
        let scriptElement: HTMLScriptElement;
        let jsonObjectStr: string;

        beforeAll(() => {
            process.env.NEXT_PUBLIC_NEWRELIC_ACCOUNT_ID = '2879290';
            process.env.NEXT_PUBLIC_NEWRELIC_TRUST_KEY = '2879290';
            process.env.NEXT_PUBLIC_NEWRELIC_AGENT_ID = '911136446';
            process.env.NEXT_PUBLIC_NEWRELIC_LICENSE_KEY = '8113c7c00e8b8a83f7d';
            process.env.NEXT_PUBLIC_NEWRELIC_APPLICATION_ID = '911136446';
            process.env.NEXT_PUBLIC_NEWRELIC_DISTRIBUTED_TRACING_ALLOWED_ORIGINS =
                "['https://api-1.com', 'https://api-2.com']";

            renderResult = render(<NewRelicSnippet />);

            // eslint-disable-next-line testing-library/no-node-access
            scriptElement = renderResult.container.querySelector('script');
        });
        afterAll(() => {
            delete process.env.NEXT_PUBLIC_NEWRELIC_ACCOUNT_ID;
            delete process.env.NEXT_PUBLIC_NEWRELIC_TRUST_KEY;
            delete process.env.NEXT_PUBLIC_NEWRELIC_AGENT_ID;
            delete process.env.NEXT_PUBLIC_NEWRELIC_LICENSE_KEY;
            delete process.env.NEXT_PUBLIC_NEWRELIC_APPLICATION_ID;
            delete process.env.NEXT_PUBLIC_NEWRELIC_DISTRIBUTED_TRACING_ALLOWED_ORIGINS;
        });

        // eslint-disable-next-line jest/expect-expect
        it('should be valid JS code', () => {
            Function(scriptElement.textContent);
        });

        it('should match NR snippet', () => {
            expect(scriptElement).toMatchSnapshot();
        });

        it('should contain "accountID", "trustKey", "agentID", "licenseKey" & "applicationID" ENV variables w/ proper values', () => {
            const scriptString = scriptElement.textContent;
            const searchTerm = ';NREUM.loader_config=';
            const start = scriptString.indexOf(searchTerm) + searchTerm.length + 1;
            jsonObjectStr = scriptString.substring(start, scriptString.indexOf('}', start));
            const expected =
                'accountID:"2879290",trustKey:"2879290",agentID:"911136446",licenseKey:"8113c7c00e8b8a83f7d",applicationID:"911136446"';

            expect(jsonObjectStr).toEqual(expected);
        });

        it('should contain "allowed_origins" ENV variable w/ proper value', () => {
            const scriptString = scriptElement.textContent;
            const searchTerm = 'allowed_origins:';
            const start = scriptString.indexOf(searchTerm) + searchTerm.length;
            jsonObjectStr = scriptString.substring(start, scriptString.indexOf('}', start));
            const expected = "['https://api-1.com', 'https://api-2.com']";

            expect(jsonObjectStr).toEqual(expected);
        });
    });
});
