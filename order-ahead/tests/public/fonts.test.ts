import fs from 'fs';
import path from 'path';
import { toJSON } from 'cssjson';

const requiredFontVars = [
    '--ff--header',
    '--ff--header-small',
    '--ff--subheader',
    '--ff--subheader-small',
    '--ff--pharagraph',
    '--ff--pharagraph-strong',
    '--ff--button',
    '--ff--link',
];

let brands = [];
describe('validating brands fonts.css structure', () => {
    beforeAll((done) => {
        const publicBrandsPath = path.resolve(process.cwd(), 'public/brands');
        fs.readdir(publicBrandsPath, (err, files) => {
            if (err) throw Error(`Error while scanning ${publicBrandsPath} directory`);
            brands = [...files];
            done();
        });
    });

    it('testing fonts.css for all brands', async () => {
        for (let brand of brands) {
            const fontsStylePath = path.resolve(process.cwd(), `public/brands/${brand}/fonts.css`);
            await new Promise((resolve) => {
                fs.readFile(fontsStylePath, 'utf-8', (err, data) => {
                    if (!err) {
                        const fontsJson = toJSON(data);
                        const fontVars = fontsJson.children[':root'].attributes;
                        // @ts-ignore
                        expect(fontVars).toContainElements(requiredFontVars);
                    }
                    resolve(undefined);
                });
            });
        }
    });
});
