import fs from 'fs';
import path from 'path';
import { toJSON } from 'cssjson';

const requiredRootCssClasses = [
    // mobile media
    '@media screen and (max-width: 959px)',
    // unordered lists
    '.list-unordered',
    '.list-unordered-checklist',
    '.list-unordered-links',
    // ordered lists
    '.list-ordered',
    '.list-ordered-large',
];

let brands = [];
describe('validating brands lists.css structure', () => {
    beforeAll(async (done) => {
        const publicBrandsPath = path.resolve(process.cwd(), 'public/brands');
        fs.readdir(publicBrandsPath, (err, files) => {
            if (err) throw Error(`Error while scanning ${publicBrandsPath} directory`);
            brands = [...files];
            done();
        });
    });

    it('testing lists.css for all brands', async () => {
        for (let brand of brands) {
            const listsStylePath = path.resolve(process.cwd(), `public/brands/${brand}/lists.css`);
            await new Promise((resolve) => {
                fs.readFile(listsStylePath, 'utf-8', (err, data) => {
                    if (!err) {
                        const listsJson = toJSON(data);
                        // @ts-ignore
                        expect(listsJson.children).toContainElements(requiredRootCssClasses);
                    }
                    resolve(undefined);
                });
            });
        }
    });
});
