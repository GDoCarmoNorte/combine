import fs from 'fs';
import path from 'path';
import { toJSON } from 'cssjson';

const requiredRootCssClasses = [
    // mobile media
    '@media screen and (max-width: 959px)',
    // headers
    '.t-header-hero',
    '.t-header-h1',
    '.t-header-h2',
    '.t-header-h3',
    '.t-header-card-title',
    // subheaders
    // '.t-subheader-hero', ---------- those to are connected with ".t-subheader-hero.t-subheader-small"
    '.t-subheader',
    // '.t-subheader-small',
    '.t-subheader-smaller',
    '.t-subheader-universal',
    '.t-subheader-universal-small',
    '.t-subheader-universal-smaller',
    // paragraphs
    '.t-paragraph',
    '.t-paragraph-strong',
    '.t-paragraph-small',
    '.t-paragraph-small-strong',
    '.t-paragraph-hint',
    '.t-paragraph-hint-strong',
    // highlighted
    '.t-highlighted',
];

let brands = [];
describe('validating brands typography.css structure', () => {
    beforeAll(async (done) => {
        const publicBrandsPath = path.resolve(process.cwd(), 'public/brands');
        fs.readdir(publicBrandsPath, (err, files) => {
            if (err) throw Error(`Error while scanning ${publicBrandsPath} directory`);
            brands = [...files];
            done();
        });
    });

    it('testing typography.css for all brands', async () => {
        for (let brand of brands) {
            const typographyStylePath = path.resolve(process.cwd(), `public/brands/${brand}/typography.css`);
            await new Promise((resolve) => {
                fs.readFile(typographyStylePath, 'utf-8', (err, data) => {
                    if (!err) {
                        const typographyJson = toJSON(data);
                        // @ts-ignore
                        expect(typographyJson.children).toContainElements(requiredRootCssClasses);
                    }
                    resolve(undefined);
                });
            });
        }
    });
});
