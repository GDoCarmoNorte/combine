import { selectAuth0 } from '../../../../redux/selectors/auth0';

describe('selectAuth0', () => {
    it('should return auth0 info', () => {
        const mockState = {
            auth0: 'test',
        } as any;

        const result = selectAuth0(mockState);
        expect(result).toEqual('test');
    });
});
