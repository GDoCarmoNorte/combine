import * as domainMenuSelectors from '../../../../redux/selectors/domainMenu';
import { selectBwwNewModiferGroups } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem, wingsTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwNewModifiersGroups', () => {
    it('should select modifier groups', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: burgerTallyItem },
        } as any;

        const result = selectBwwNewModiferGroups(
            mockState,
            'IDPModifierGroup-12305',
            'IDPModifier-16411',
            1,
            burgerTallyItem.modifierGroups
        );

        expect(result).toMatchSnapshot();
    });

    it('should select modifier groups (no sauce case)', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: burgerTallyItem },
        } as any;

        jest.spyOn(domainMenuSelectors, 'selectNoSauceDomainProduct').mockReturnValue({
            id: 'IDPModifier-16411',
        } as any);

        const result = selectBwwNewModiferGroups(
            mockState,
            'IDPModifierGroup-12305',
            'IDPModifier-16411',
            1,
            burgerTallyItem.modifierGroups
        );

        expect(result).toMatchSnapshot();
    });

    it('should select modifier groups (wingtype case)', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: wingsTallyItem },
        } as any;

        let result = selectBwwNewModiferGroups(
            mockState,
            'IDPModifierGroup-16160',
            'IDPModifier-6258',
            1,
            wingsTallyItem.modifierGroups
        );

        expect(result[6].modifiers.length).toEqual(1);

        result = selectBwwNewModiferGroups(mockState, 'IDPModifierGroup-16160', '', 1, wingsTallyItem.modifierGroups);

        expect(result[6].modifiers.length).toEqual(0);
    });
});
