import { selectAboutSectionInfo } from '../../../../redux/selectors/pdp';
import { ModifierGroupType } from '../../../../redux/types';
import menu from '../__mocks__/menuMock.json';
import { standaloneTalyItemMock, comboTallyItemMock } from '../__mocks__/tallyItem.mock';

describe('selectAboutSectionInfo', () => {
    it('should return about section info for standalone product', () => {
        const result = selectAboutSectionInfo({
            domainMenu: { payload: menu },
            pdp: { tallyItem: standaloneTalyItemMock },
        } as any);

        expect(result).toMatchSnapshot();
    });

    it('should return about section info for combo product', () => {
        const result = selectAboutSectionInfo({
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemMock },
        } as any);

        expect(result).toMatchSnapshot();
    });

    it('should return about section info for product with ModifierGroupType Selections', () => {
        const modifierGroupMock = {
            productId: 'arb-prg-001-001',
            metadata: {
                MODIFIER_GROUP_ID: '640214237',
                MODIFIER_GROUP_TYPE: ModifierGroupType.SELECTIONS,
            },
            modifiers: [
                {
                    productId: 'arb-itm-006-001',
                    price: 2,
                    quantity: 1,
                },
            ],
        };

        const tallyItemMock = {
            ...standaloneTalyItemMock,
            modifierGroups: [modifierGroupMock],
        };

        const resultWithoutModifier = selectAboutSectionInfo({
            domainMenu: { payload: menu },
            pdp: { tallyItem: standaloneTalyItemMock },
        } as any);

        expect(resultWithoutModifier.nutritionInfo.length).toEqual(1);

        const resultWithModifier = selectAboutSectionInfo({
            domainMenu: { payload: menu },
            pdp: { tallyItem: tallyItemMock },
        } as any);

        expect(resultWithModifier.nutritionInfo.length).toEqual(2);
    });

    it('should return null if pdp tally item is not provided', () => {
        const result = selectAboutSectionInfo({
            domainMenu: { payload: menu },
            pdp: { tallyItem: null },
        } as any);

        expect(result).toBe(null);
    });
});
