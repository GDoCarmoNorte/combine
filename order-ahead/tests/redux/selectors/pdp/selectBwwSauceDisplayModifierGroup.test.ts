import { createNextState } from '@reduxjs/toolkit';
import { selectBwwSauceDisplayModifierGroup } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { wingsTallyItem, burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwSauceDisplayModifierGroup', () => {
    it('should return display modifier group with regular sauces', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: wingsTallyItem },
        } as any;

        const result = selectBwwSauceDisplayModifierGroup(mockState, 'IDPModifierGroup-13393');

        expect(result).toMatchSnapshot();
    });

    it('should return display modifier group with sauces on the side', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: {
                tallyItem: createNextState(wingsTallyItem, (nextState) => {
                    nextState.modifierGroups[3].isOnSideChecked = true;
                }),
            },
        } as any;

        const result = selectBwwSauceDisplayModifierGroup(mockState, 'IDPModifierGroup-13393');

        expect(result).toMatchSnapshot();
    });

    it('should return display modifier group without "on side" option', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: {
                tallyItem: burgerTallyItem,
            },
        } as any;

        const result = selectBwwSauceDisplayModifierGroup(mockState, 'IDPModifierGroup-14295');

        expect(result).toMatchSnapshot();
    });
});
