import { selectDisplayModifierGroupsByProductIdAndProductGroupId } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/menuMock.json';
import {
    standaloneTalyItemMock,
    comboTallyItemMock,
    singleTallyItemWithModsWithSelectionsMock,
    comboTallyItemWithModsWithSelectionsMock,
} from '../__mocks__/tallyItem.mock';

describe('selectDisplayModifierGroupsByProductIdAndProductGroupId', () => {
    it('should return display modifier group for standalone product', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: standaloneTalyItemMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(
            mockState,
            'arb-itm-000-001',
            'arb-prg-000-019',
            0
        );

        expect(result).toMatchSnapshot();
    });

    it('should return display modifier group for main product in combo', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(
            mockState,
            'arb-itm-000-005',
            'arb-prg-000-019',
            0
        );

        expect(result).toMatchSnapshot();
    });

    it('should return display modifier group for side group in combo', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(
            mockState,
            'arb-itm-000-005',
            'arb-prg-000-008',
            1
        );

        expect(result).toMatchSnapshot();
    });

    it('should return empty array if product is not provided', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(mockState, null, 'arb-prg-000-008', 1);

        expect(result).toEqual([]);
    });

    it('should return empty array if product group is not provided', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(mockState, 'arb-itm-000-005', null, 0);

        expect(result).toEqual([]);
    });

    it('should return modifier group with modifier with selections for single product', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: singleTallyItemWithModsWithSelectionsMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(
            mockState,
            'arb-itm-000-169',
            'arb-prg-000-019',
            0
        );

        expect(result).toMatchSnapshot();
    });

    it('should return modifier group with modifier with selections for meal', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemWithModsWithSelectionsMock },
        } as any;

        const result = selectDisplayModifierGroupsByProductIdAndProductGroupId(
            mockState,
            'arb-itm-000-171',
            'arb-prg-000-019',
            0
        );

        expect(result).toMatchSnapshot();
    });
});
