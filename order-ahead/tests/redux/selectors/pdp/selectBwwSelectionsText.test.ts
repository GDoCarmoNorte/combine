import { selectBwwSelectionsText } from '../../../../redux/selectors/pdp';
import { burgerTallyItem, wingsTallyItem } from '../__mocks__/bwwTallyItem.mock';
import menu from '../__mocks__/bwwMenuMock.json';
import { createNextState } from '@reduxjs/toolkit';
import { ItemGroupEnum } from '../../../../redux/types';

describe('selectBwwSelectionsText', () => {
    it('should have correct selections message for product with size', () => {
        const state: any = {
            domainMenu: { payload: menu },
        };

        expect(selectBwwSelectionsText(state, wingsTallyItem, '')).toEqual('10 Boneless Wings + Fries, Celery, Ranch');
    });

    it('should have correct selections message for product without size', () => {
        const state: any = {
            domainMenu: { payload: menu },
        };

        expect(selectBwwSelectionsText(state, burgerTallyItem, '')).toEqual('Buffalo Bleu Burger');
    });

    it('should have correct selections message for modifiers with different quantity', () => {
        const state: any = {
            domainMenu: { payload: menu },
        };

        expect(
            selectBwwSelectionsText(
                state,
                createNextState(wingsTallyItem, (nextState) => {
                    nextState.modifierGroups[4].modifiers[0].quantity = 5;
                    nextState.modifierGroups[4].modifiers[1].quantity = 2;
                }),
                ''
            )
        ).toEqual('10 Boneless Wings + Fries, 5 Celery, 2 Ranch');
    });

    it('should calculate total and append it at the end for BOGO', () => {
        const state: any = {
            domainMenu: { payload: menu },
        };

        expect(selectBwwSelectionsText(state, wingsTallyItem, ItemGroupEnum.BOGO)).toEqual(
            'Boneless Wings + Fries (20 Total), Celery, Ranch'
        );
    });

    it('should calculate total and append it at the end for BOGO 50', () => {
        const state: any = {
            domainMenu: { payload: menu },
        };

        expect(selectBwwSelectionsText(state, wingsTallyItem, ItemGroupEnum.BOGO_50_OFF)).toEqual(
            'Boneless Wings + Fries (20 Total), Celery, Ranch'
        );
    });
});
