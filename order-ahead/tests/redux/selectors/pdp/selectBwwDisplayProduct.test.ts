import { selectBwwDisplayProduct } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectDisplayProduct', () => {
    it('should return display product for standalone product', () => {
        const result = selectBwwDisplayProduct({
            domainMenu: { payload: menu },
            pdp: { tallyItem: burgerTallyItem },
        } as any);

        expect(result).toEqual({
            productType: 'SINGLE',
            displayName: 'BUFFALO BLEU BURGER',
            productId: 'IDPSalesItem-4858',
            mainProductId: 'IDPSalesItem-4858',
            mainProductSectionName: 'BURGER',
            productSections: [
                {
                    productSectionType: 'main',
                    productSectionDisplayName: 'Options',
                    productGroupId: 'IDPModifierGroup-12305',
                },
                {
                    productSectionType: 'sauce',
                    productSectionDisplayName: 'Signature Sauce',
                    productGroupId: 'IDPModifierGroup-14295',
                },
                {
                    productSectionType: 'side',
                    productSectionDisplayName: 'Sides (Additional Charges May Apply)',
                    productGroupId: 'IDPModifierGroup-16051',
                },
                {
                    productSectionType: 'main',
                    productSectionDisplayName: 'Cheese',
                    productGroupId: 'IDPModifierGroup-9124',
                },
            ],
            price: 12.29,
            totalPrice: 12.29,
            calories: 1270,
        });
    });
});
