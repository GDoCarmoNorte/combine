import { selectIsPDPProductReadyToAddToBag } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { wingsTallyItem, burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectIsPDPProductReadyToAddToBag', () => {
    it('should return correct value for wingsTallyItem', () => {
        const mockState = { domainMenu: { payload: menu }, pdp: { tallyItem: wingsTallyItem } } as any;
        expect(selectIsPDPProductReadyToAddToBag(mockState)).toEqual(false);
    });

    it('should return correct value for burgerTallyItem', () => {
        const mockState = { domainMenu: { payload: menu }, pdp: { tallyItem: burgerTallyItem } } as any;
        expect(selectIsPDPProductReadyToAddToBag(mockState)).toEqual(true);
    });
});
