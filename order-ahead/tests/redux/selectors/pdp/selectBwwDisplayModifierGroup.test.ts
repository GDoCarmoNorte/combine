import { selectBwwDisplayModifierGroup } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwDisplayModifierGroup', () => {
    it('should return display modifier group', () => {
        const mockState = {
            domainMenu: { payload: menu },
        } as any;

        const result = selectBwwDisplayModifierGroup(mockState, burgerTallyItem, 'IDPModifierGroup-12305');

        expect(result).toMatchSnapshot();
    });
});
