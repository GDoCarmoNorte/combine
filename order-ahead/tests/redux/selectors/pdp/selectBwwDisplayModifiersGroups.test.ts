import { selectBwwDisplayModifiersGroups } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwDisplayModifiersGroups', () => {
    it('should return display modifier groups', () => {
        const mockState = {
            domainMenu: { payload: menu },
        } as any;

        const result = selectBwwDisplayModifiersGroups(
            mockState,
            burgerTallyItem,
            'IDPModifierGroup-14322',
            'IDPModifier-24065'
        );

        expect(result).toMatchSnapshot();
    });
});
