import { selectExtrasDisplayProducts } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectExtrasDisplayProducts', () => {
    const mockState = { domainMenu: { payload: menu }, pdp: { tallyItem: burgerTallyItem } };
    it('should return extras', () => {
        const result = selectExtrasDisplayProducts(mockState as any);

        expect(result).toMatchSnapshot();
    });
});
