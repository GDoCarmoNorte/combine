import { selectSelectedExtras } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectSelectedExtras', () => {
    it('should return null if no child extras', () => {
        const tallyItem = { ...burgerTallyItem, childExtras: null };
        const mockState = { domainMenu: { payload: menu }, pdp: { tallyItem } } as any;

        const result = selectSelectedExtras(mockState);

        expect(result).toBeNull();
    });

    it('should return null if no valid child items in array', () => {
        const tallyItem = { ...burgerTallyItem, childExtras: [null, null, null] };
        const mockState = { domainMenu: { payload: menu }, pdp: { tallyItem } } as any;

        const result = selectSelectedExtras(mockState);

        expect(result).toBeNull();
    });

    it('should returh selected extras', () => {
        const tallyItem = {
            ...burgerTallyItem,
            childExtras: [
                {
                    productId: 'IDPSalesItem-4879',
                    price: 3.79,
                    quantity: 1,
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup-15533',
                            modifiers: [
                                {
                                    productId: 'IDPModifier-14678',
                                    quantity: 1,
                                    price: 0.65,
                                },
                            ],
                        },
                    ],
                    name: 'Regular Potato Wedges',
                },
                null,
                null,
            ],
        };
        const mockState = { domainMenu: { payload: menu }, pdp: { tallyItem } } as any;

        const result = selectSelectedExtras(mockState);

        expect(result).toEqual([
            {
                name: 'Regular Potato Wedges',
                price: 4.44,
                calories: 370,
            },
        ]);
    });
});
