import { selectBwwOnSideSauces } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { wingsTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwOnSideSauces', () => {
    it('should select sauces with "on side" options and "no sauce" option', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: wingsTallyItem },
        } as any;

        const result = selectBwwOnSideSauces(mockState, 'IDPModifierGroup-13393');

        expect(result).toMatchSnapshot();
    });
});
