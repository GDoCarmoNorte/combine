import { selectDisplayProduct } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/menuMock.json';
import { standaloneTalyItemMock, comboTallyItemMock, promoTallyItemMock } from '../__mocks__/tallyItem.mock';

describe('selectDisplayProduct', () => {
    it('should return display product for standalone product', () => {
        const result = selectDisplayProduct({
            domainMenu: { payload: menu },
            pdp: { tallyItem: standaloneTalyItemMock },
        } as any);

        expect(result).toEqual({
            calories: 360,
            displayName: 'Classic Roast Beef',
            price: 3.49,
            totalPrice: 3.49,
            mainProductId: 'arb-itm-000-001',
            mainProductSectionName: 'Sandwich',
            productId: 'arb-itm-000-001',
            productSections: [
                {
                    productGroupId: 'arb-prg-000-019',
                    productSectionDisplayName: 'Sandwich',
                    productSectionType: 'main',
                },
            ],
            productType: 'SINGLE',
        });
    });

    it('should return display product for combo', () => {
        const result = selectDisplayProduct({
            domainMenu: { payload: menu },
            pdp: { tallyItem: comboTallyItemMock },
        } as any);

        expect(result).toEqual({
            calories: 930,
            displayName: 'Classic Roast Beef Meal',
            price: 5.99,
            totalPrice: 5.99,
            mainProductId: 'arb-itm-000-005',
            mainProductSectionName: 'Sandwich',
            productId: 'arb-itm-000-005',
            productSections: [
                {
                    productGroupId: 'arb-prg-000-019',
                    productSectionDisplayName: 'Sandwich',
                    productSectionType: 'main',
                },
                {
                    productGroupId: 'arb-prg-000-008',
                    productSectionDisplayName: 'Side',
                    productSectionType: 'side',
                },
                {
                    productGroupId: 'arb-prg-000-010',
                    productSectionDisplayName: 'Drink',
                    productSectionType: 'drink',
                },
            ],
            productType: 'MEAL',
        });
    });

    it('should return display product for promo', () => {
        const result = selectDisplayProduct({
            domainMenu: { payload: menu },
            pdp: { tallyItem: promoTallyItemMock },
        } as any);

        expect(result).toEqual({
            productType: 'PROMO',
            displayName: '2 for $6 Everyday Value',
            productId: 'arb-itm-000-186',
            mainProductId: 'arb-itm-000-186',
            mainProductSectionName: 'Promo',
            productSections: [
                {
                    productSectionType: 'promo',
                    productSectionDisplayName: 'Sandwich 1',
                    productGroupId: 'arb-prg-000-024',
                },
                {
                    productSectionType: 'promo',
                    productSectionDisplayName: 'Sandwich 2',
                    productGroupId: 'arb-prg-000-025',
                },
            ],
            price: 6,
            totalPrice: 6,
            calories: 900,
        });
    });
});
