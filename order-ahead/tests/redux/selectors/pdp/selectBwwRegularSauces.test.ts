import { selectBwwRegularSauces } from '../../../../redux/selectors/pdp';
import menu from '../__mocks__/bwwMenuMock.json';
import { wingsTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwRegularSauces', () => {
    it('should select sauces without "on side" options', () => {
        const mockState = {
            domainMenu: { payload: menu },
            pdp: { tallyItem: wingsTallyItem },
        } as any;

        const result = selectBwwRegularSauces(mockState, 'IDPModifierGroup-13393');

        expect(result).toMatchSnapshot();
    });
});
