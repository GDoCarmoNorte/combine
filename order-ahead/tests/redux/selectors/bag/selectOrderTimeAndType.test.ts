import { OrderLocationMethod } from '../../../../redux/orderLocation';
import { selectOrderTimeAndType } from '../../../../redux/selectors/bag';

describe('selectAccount', () => {
    const bagMock = {
        bag: {
            pickupTime: 'pickupTime',
            pickupTimeType: 'future',
            deliveryTime: 'deliveryTime',
            deliveryTimeType: 'future',
        },
    };

    it('should return order time and type for pickup method', () => {
        const result = selectOrderTimeAndType({
            ...bagMock,
            orderLocation: { method: OrderLocationMethod.PICKUP },
        } as any);
        expect(result).toEqual({
            orderTime: 'pickupTime',
            orderTimeType: 'future',
        });
    });

    it('should return order time and type for delivery method', () => {
        const result = selectOrderTimeAndType({
            ...bagMock,
            orderLocation: { method: OrderLocationMethod.DELIVERY },
        } as any);
        expect(result).toEqual({
            orderTime: 'deliveryTime',
            orderTimeType: 'future',
        });
    });

    it('should return order time and type for no selected method', () => {
        const result = selectOrderTimeAndType({
            ...bagMock,
            orderLocation: { method: OrderLocationMethod.NOT_SELECTED },
        } as any);
        expect(result).toEqual({
            orderTime: null,
            orderTimeType: 'asap',
        });
    });
});
