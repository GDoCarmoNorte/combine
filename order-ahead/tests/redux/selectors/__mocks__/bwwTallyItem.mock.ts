import { PDPTallyItem } from '../../../../redux/pdp';
import { ModifierGroupType } from '../../../../redux/types';

export const wingsTallyItem: PDPTallyItem = {
    name: 'Boneless Wings + Fries',
    quantity: 1,
    productId: 'IDPSalesItem-5356',
    price: 9.99,
    modifierGroups: [
        {
            productId: 'IDPModifierGroup-0001',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-0002',
            modifiers: [],
        },
        {
            productId: 'IDPModifdierGroup-0003',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-13393',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-15249',
            metadata: {
                MODIFIER_GROUP_TYPE: 'Selections',
            },
            modifiers: [
                {
                    productId: 'IDPModifier-27032',
                    price: 0.6,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-27031',
                    price: 0.6,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-16159',
            modifiers: [
                {
                    productId: 'IDPModifier-6257',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-16160',
            metadata: {
                MODIFIER_GROUP_TYPE: ModifierGroupType.WINGTYPE,
            },
            modifiers: [
                {
                    productId: 'IDPModifier-6258',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
    childExtras: [],
};

export const burgerTallyItem = {
    name: 'Buffalo Bleu Burger',
    quantity: 1,
    productId: 'IDPSalesItem-4858',
    price: 12.29,
    modifierGroups: [
        {
            productId: 'IDPModifierGroup-0001',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-0002',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-0003',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-12305',
            modifiers: [
                {
                    productId: 'IDPModifier-16411',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-16410',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-16409',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-17317',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-16414',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-27449',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-14295',
            modifiers: [
                {
                    productId: 'IDPModifier-23901',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-16051',
            modifiers: [
                {
                    productId: 'IDPModifier-5874',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-9124',
            modifiers: [
                {
                    productId: 'IDPModifier-3697',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
    childExtras: [],
};

export const burgerTallyItemWithSubModifiers: PDPTallyItem = {
    name: 'Buffalo Bleu Burger',
    quantity: 1,
    productId: 'IDPSalesItem-4858',
    price: 15.79,
    modifierGroups: [
        {
            productId: 'IDPModifierGroup-0001',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-0002',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-0003',
            modifiers: [],
        },
        {
            productId: 'IDPModifierGroup-12305',
            modifiers: [
                {
                    productId: 'IDPModifier-16411',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-16410',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-16409',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-17317',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-16414',
                    price: 0,
                    quantity: 1,
                },
                {
                    productId: 'IDPModifier-27449',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-14295',
            modifiers: [
                {
                    productId: 'IDPModifier-23901',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-16051',
            metadata: {
                MODIFIER_GROUP_ID: '16051',
                MODIFIER_GROUP_TYPE: 'Selections',
            },
            isOnSideChecked: false,
            modifiers: [
                {
                    productId: 'IDPModifier-30749',
                    quantity: 1,
                    price: 1,
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup-11513',
                            metadata: {
                                MODIFIER_GROUP_ID: '11513',
                                MODIFIER_GROUP_TYPE: 'Modifications',
                            },
                            isOnSideChecked: false,
                            modifiers: [
                                {
                                    productId: 'IDPModifier-13117',
                                    quantity: 1,
                                    price: 0.75,
                                },
                                {
                                    productId: 'IDPModifier-13118',
                                    quantity: 1,
                                    price: 1.35,
                                },
                            ],
                        },
                    ],
                },
            ],
        },
        {
            productId: 'IDPModifierGroup-9124',
            modifiers: [
                {
                    productId: 'IDPModifier-3697',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
    childExtras: [],
};
