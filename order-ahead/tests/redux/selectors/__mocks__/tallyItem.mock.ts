import { PDPTallyItem } from '../../../../redux/pdp';

// Classic Roast Beef
export const standaloneTalyItemMock: PDPTallyItem = {
    name: 'Classic Roast Beef',
    quantity: 1,
    productId: 'arb-itm-000-001',
    price: 3.49,
    childExtras: [],
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            metadata: {
                MODIFIER_GROUP_ID: '640214237',
            },
            modifiers: [
                {
                    productId: 'arb-itm-006-001',
                    price: 2,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            metadata: {
                MODIFIER_GROUP_ID: '640214193',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-003',
            metadata: {
                MODIFIER_GROUP_ID: '640214192',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-004',
            metadata: {
                MODIFIER_GROUP_ID: '640214194',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-005',
            metadata: {
                MODIFIER_GROUP_ID: '640214194',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-006',
            metadata: {
                MODIFIER_GROUP_ID: '640221053',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-008',
            metadata: {
                MODIFIER_GROUP_ID: '640214236',
            },
            modifiers: [
                {
                    productId: 'arb-itm-013-001',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
};

// Classic Roast Beef Small Meal
export const comboTallyItemMock: PDPTallyItem = {
    name: 'Classic Roast Beef Meal',
    productId: 'arb-itm-000-005',
    price: 5.99,
    quantity: 1,
    childExtras: [],
    childItems: [
        {
            name: 'Classic Roast Beef',
            productId: 'arb-itm-000-001',
            price: 2.21,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214237',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214193',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-003',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214192',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    metadata: {
                        MODIFIER_GROUP_ID: '640221053',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214236',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-013-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            name: 'Curly Fries',
            productId: 'arb-itm-002-002',
            price: 1.99,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-006',
                    metadata: {
                        MODIFIER_GROUP_ID: '640221053',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
            ],
        },
        {
            name: 'Coca-Cola®',
            productId: 'arb-itm-003-001',
            price: 1.79,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-011',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-016-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-012',
                    metadata: {
                        MODIFIER_GROUP_ID: '640219114',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-017-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
    ],
};

export const tallyItemMealSmallSizeMock: PDPTallyItem = {
    name: 'Classic Roast Beef Meal',
    productId: 'arb-itm-000-005',
    price: 5.99,
    quantity: 1,
    childItems: [
        {
            name: 'Classic Roast Beef',
            productId: 'arb-itm-000-001',
            price: 2.21,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            name: 'Curly Fries',
            productId: 'arb-itm-002-002',
            price: 1.99,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
            ],
        },
        {
            name: 'Coca-Cola®',
            productId: 'arb-itm-003-001',
            price: 1.79,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-011',
                    modifiers: [
                        {
                            productId: 'arb-itm-016-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-012',
                    modifiers: [
                        {
                            productId: 'arb-itm-017-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
    ],
};

export const tallyItemMealMediumMock: PDPTallyItem = {
    name: 'Classic Roast Beef Meal',
    productId: 'arb-itm-000-006',
    price: 6.69,
    quantity: 1,
    childExtras: [],
    childItems: [
        {
            name: 'Classic Roast Beef',
            productId: 'arb-itm-000-001',
            price: 2.21,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214237',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214193',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-003',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214192',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    metadata: {
                        MODIFIER_GROUP_ID: '640221053',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214236',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-013-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            name: 'Curly Fries',
            productId: 'arb-itm-002-003',
            price: 2.49,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-006',
                    metadata: {
                        MODIFIER_GROUP_ID: '640221053',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
            ],
        },
        {
            name: 'Coca-Cola®',
            productId: 'arb-itm-003-002',
            price: 1.99,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-011',
                    metadata: {
                        MODIFIER_GROUP_ID: '640232017',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-016-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-012',
                    metadata: {
                        MODIFIER_GROUP_ID: '640219114',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-017-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
    ],
};

export const tallyItemSingleProductSmalSizeMock: PDPTallyItem = {
    productId: 'arb-itm-000-001',
    price: 3.49,
    quantity: 1,
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            modifiers: [
                {
                    productId: 'arb-itm-006-001',
                    quantity: 1,
                    price: 2,
                },
                {
                    productId: 'arb-itm-006-002',
                    quantity: 1,
                    price: 0.99,
                },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            modifiers: [
                {
                    productId: 'arb-itm-007-001',
                    quantity: 1,
                    price: 0.69,
                },
            ],
        },
        {
            productId: 'arb-prg-001-003',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-004',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-005',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-006',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-008',
            modifiers: [
                {
                    productId: 'arb-itm-013-001',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
};

export const tallyItemSingleProductSnackSizeMock: PDPTallyItem = {
    name: 'Orange Cream Shake',
    quantity: 1,
    productId: 'arb-itm-003-129',
    price: 1.69,
    modifierGroups: [],
};

export const tallyItemSingleProductMediumSizeMock: PDPTallyItem = {
    name: 'Double Roast Beef',
    productId: 'arb-itm-000-002',
    price: 4.99,
    quantity: 1,
    childExtras: [],
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            metadata: {
                MODIFIER_GROUP_ID: '640214237',
            },
            modifiers: [
                {
                    productId: 'arb-itm-006-001',
                    price: 2,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            metadata: {
                MODIFIER_GROUP_ID: '640214193',
            },
            modifiers: [
                {
                    productId: 'arb-itm-007-001',
                    quantity: 1,
                    price: 0.69,
                },
            ],
        },
        {
            productId: 'arb-prg-001-003',
            metadata: {
                MODIFIER_GROUP_ID: '640214192',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-004',
            metadata: {
                MODIFIER_GROUP_ID: '640214194',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-005',
            metadata: {
                MODIFIER_GROUP_ID: '640214194',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-006',
            metadata: {
                MODIFIER_GROUP_ID: '640221053',
            },
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-008',
            metadata: {
                MODIFIER_GROUP_ID: '640214236',
            },
            modifiers: [
                {
                    productId: 'arb-itm-013-001',
                    price: 0,
                    quantity: 1,
                },
            ],
        },
    ],
};

export const tallyItemPromoProduct = {
    name: '2 for $6 Everyday Value',
    productId: 'arb-itm-000-174',
    price: 6,
    quantity: 1,
    childExtras: [],
    childItems: [
        {
            name: "Classic Beef 'n Cheddar",
            productId: 'arb-itm-000-017',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214237',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214193',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214192',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-009-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    metadata: {
                        MODIFIER_GROUP_ID: '640221053',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214236',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-013-002',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            name: "Classic Beef 'n Cheddar",
            productId: 'arb-itm-000-017',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214237',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214193',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214192',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-009-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214194',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    metadata: {
                        MODIFIER_GROUP_ID: '640221053',
                    },
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    metadata: {
                        MODIFIER_GROUP_ID: '640214236',
                    },
                    modifiers: [
                        {
                            productId: 'arb-itm-013-002',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
    ],
};

export const tallyItemPromoProductDifferentChildren = {
    productId: 'arb-itm-000-174',
    price: 6,
    quantity: 1,
    childItems: [
        {
            productId: 'arb-itm-000-134',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-007',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-009',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [
                        {
                            productId: 'arb-itm-008-001',
                            price: 0.29,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-008-004',
                            price: 0,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-008-005',
                            price: 0.29,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-011',
                            price: 0,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-009-015',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-004',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            productId: 'arb-itm-000-164',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-020',
                            price: 0.99,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-006-002',
                            price: 0.79,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-004',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [
                        {
                            productId: 'arb-itm-008-011',
                            price: 0.29,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-017',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
    ],
    childExtras: [],
};

export const singleTallyItemWithModsWithSelectionsMock = {
    name: "King's Hawaiian Action Code Test",
    quantity: 1,
    productId: 'arb-itm-000-169',
    price: 4.49,
    modifierGroups: [
        {
            productId: 'arb-prg-001-001',
            modifiers: [
                {
                    productId: 'arb-itm-006-020',
                    price: 0.99,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-002',
            modifiers: [
                {
                    productId: 'arb-itm-007-004',
                    price: 0.49,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-003',
            modifiers: [
                {
                    productId: 'arb-itm-008-011',
                    price: 0.29,
                    quantity: 1,
                },
                {
                    productId: 'arb-itm-008-005',
                    price: 0.29,
                    quantity: 1,
                },
            ],
        },
        {
            productId: 'arb-prg-001-004',
            modifiers: [
                {
                    productId: 'arb-itm-009-029',
                    quantity: 1,
                    price: 0,
                },
            ],
        },
        {
            productId: 'arb-prg-001-005',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-006',
            modifiers: [],
        },
        {
            productId: 'arb-prg-001-008',
            modifiers: [
                {
                    productId: 'arb-itm-013-010',
                    price: 0.5,
                    quantity: 1,
                },
            ],
        },
    ],
};

export const comboTallyItemWithModsWithSelectionsMock = {
    name: "King's Hawaiian Fish Deluxe Meal",
    productId: 'arb-itm-000-171',
    price: 6.99,
    quantity: 1,
    childItems: [
        {
            productId: 'arb-itm-000-169',
            price: 3.21,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-020',
                            price: 0.99,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-004',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [
                        {
                            productId: 'arb-itm-008-011',
                            price: 0.29,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-008-005',
                            price: 0.29,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-029',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-010',
                            price: 0.5,
                            quantity: 1,
                        },
                    ],
                },
            ],
            name: "King's Hawaiian Action Code Test",
        },
        {
            productId: 'arb-itm-002-002',
            price: 1.99,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
            ],
            name: 'Curly Fries',
        },
        {
            productId: 'arb-itm-003-001',
            price: 1.79,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-011',
                    modifiers: [
                        {
                            productId: 'arb-itm-016-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-012',
                    modifiers: [
                        {
                            productId: 'arb-itm-017-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            name: 'Coca-Cola®',
        },
    ],
};

export const promoTallyItemMock = {
    name: '2 for $6 Everyday Value',
    productId: 'arb-itm-000-186',
    price: 6,
    quantity: 1,
    childItems: [
        {
            productId: 'arb-itm-000-017',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-002',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            name: "Classic Beef 'n Cheddar",
        },
        {
            productId: 'arb-itm-000-017',
            price: 3,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-007-001',
                            price: 0.49,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-002',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
            name: "Classic Beef 'n Cheddar",
        },
    ],
};
