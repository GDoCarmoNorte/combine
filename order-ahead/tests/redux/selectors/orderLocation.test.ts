import { isLocationOrderAheadAvailable, isLocationDeliveryAvailable } from '../../../lib/locations';
import { OrderLocationMethod } from '../../../redux/orderLocation';
import {
    selectOrderMethod,
    selectCurrenOrderLocation,
    selectIsCurrentLocationOAAvailable,
} from '../../../redux/selectors/orderLocation';

import configurationMock from '../../mocks/configFeatureFlag.mock';
import orderLocationMock from './__mocks__/orderLocationMock.json';

jest.mock('../../../lib/locations');

describe('orderLocation', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    describe('selectOrderMethod', () => {
        it('should return order method', () => {
            const mockState = { orderLocation: orderLocationMock } as any;

            const result = selectOrderMethod(mockState);

            expect(result).toEqual(OrderLocationMethod.PICKUP);
        });
    });

    describe('selectCurrenOrderLocation', () => {
        it('should return pickup location if pickup method is selected', () => {
            const mockState = { orderLocation: orderLocationMock } as any;

            const result = selectCurrenOrderLocation(mockState);

            expect(result.id).toBe('ARBYS-99984');
        });

        it('should return delivery location if delivery method is selected', () => {
            const mockState = {
                orderLocation: {
                    ...orderLocationMock,
                    method: OrderLocationMethod.DELIVERY,
                    deliveryAddress: { locationDetails: 'locationDetails' },
                },
            } as any;

            const result = selectCurrenOrderLocation(mockState);

            expect(result).toBe('locationDetails');
        });

        it('should return null if method is not selected', () => {
            const mockState = {
                orderLocation: { ...orderLocationMock, method: OrderLocationMethod.NOT_SELECTED },
            } as any;

            const result = selectCurrenOrderLocation(mockState);

            expect(result).toBeNull();
        });
    });

    describe('selectIsCurrentLocationOAAvailable', () => {
        it('should return false if order method is not selected', () => {
            const mockState = {
                orderLocation: {
                    ...orderLocationMock,
                    method: OrderLocationMethod.NOT_SELECTED,
                },
                configuration: configurationMock,
            } as any;

            const result = selectIsCurrentLocationOAAvailable(mockState);

            expect(result).toBe(false);
        });

        it('should call isLocationOrderAheadAvailable if pickup order method', () => {
            const mockState = { orderLocation: orderLocationMock, configuration: configurationMock } as any;
            (isLocationOrderAheadAvailable as jest.Mock).mockReturnValue(true);

            const result = selectIsCurrentLocationOAAvailable(mockState);

            expect(isLocationOrderAheadAvailable).toHaveBeenCalled();
            expect(result).toBe(true);
        });

        it('should call isLocationDeliveryAvailable if delivery order method', () => {
            const mockState = {
                orderLocation: {
                    ...orderLocationMock,
                    method: OrderLocationMethod.DELIVERY,
                    deliveryAddress: { locationDetails: 'locationDetails' },
                },
                configuration: configurationMock,
            } as any;
            (isLocationDeliveryAvailable as jest.Mock).mockReturnValue(true);

            const result = selectIsCurrentLocationOAAvailable(mockState);

            expect(isLocationDeliveryAvailable).toHaveBeenCalled();
            expect(result).toBe(true);
        });
    });
});
