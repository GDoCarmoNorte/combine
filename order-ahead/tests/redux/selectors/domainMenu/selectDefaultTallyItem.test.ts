import { selectDefaultTallyItem } from '../../../../redux/selectors/domainMenu';

import menu from '../__mocks__/menuMock.json';
import orderLocation from '../__mocks__/orderLocationMock.json';

import { standaloneTalyItemMock, comboTallyItemMock, tallyItemPromoProduct } from '../__mocks__/tallyItem.mock';

describe('selectDefaultTallyItem', () => {
    it('should convert regular product item to tally item', () => {
        const result = selectDefaultTallyItem(
            { domainMenu: { payload: menu }, orderLocation } as any,
            'arb-itm-000-001'
        );

        expect(result).toEqual(standaloneTalyItemMock);
    });

    it('should convert meal product item to tally item', () => {
        const result = selectDefaultTallyItem(
            { domainMenu: { payload: menu }, orderLocation } as any,
            'arb-itm-000-005'
        );

        expect(result).toEqual(comboTallyItemMock);
    });

    it('should convert promo product item to tally item', () => {
        const result = selectDefaultTallyItem(
            { domainMenu: { payload: menu }, orderLocation } as any,
            'arb-itm-000-174'
        );

        expect(result).toEqual(tallyItemPromoProduct);
    });
});
