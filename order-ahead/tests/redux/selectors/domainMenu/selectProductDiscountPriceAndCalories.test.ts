import { selectProductDiscountPriceAndCalories } from '../../../../redux/selectors/domainMenu';

import menu from '../__mocks__/menuMock.json';
import orderLocation from '../__mocks__/orderLocationMock.json';

describe('selectProductDiscountPriceAndCalories', () => {
    const mockState = { domainMenu: { payload: menu }, orderLocation } as any;

    it('should return price and calories', () => {
        const result = selectProductDiscountPriceAndCalories(mockState, 'arb-itm-000-001'); // Classic Roast Beef

        expect(result).toEqual({
            price: 3.49,
            calories: 360,
        });
    });

    it('should return price and calories if product is not provided', () => {
        const result = selectProductDiscountPriceAndCalories(mockState, 'some-id');

        expect(result).toEqual({
            price: null,
            calories: null,
        });
    });
});
