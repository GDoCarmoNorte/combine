import { selectProductWithRootIdById } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectProductWithRootIdById', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return product', () => {
        const result = selectProductWithRootIdById(mockState, 'arb-itm-000-001');

        expect(result.rootProductId).toEqual('arb-prd-000-001');
    });

    it('should not return product', () => {
        const result = selectProductWithRootIdById(mockState, 'some-id');

        expect(result).toBeUndefined();
    });
});
