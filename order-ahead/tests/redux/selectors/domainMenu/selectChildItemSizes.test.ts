import { selectChildItemSizes, selectProductById, selectSizeGroup } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectChildItemSizes', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return all sizes for child item in meal', () => {
        const result = selectChildItemSizes(mockState, 'arb-itm-002-012', 'arb-itm-000-006');

        expect(result).toEqual([
            {
                sizeGroup: selectSizeGroup(mockState, 'arb-itm-000-005'), // Classic Roast Beef Small Meal
                product: selectProductById(mockState, 'arb-itm-002-011'),
                disabled: false,
                isGroupedBySize: true,
            },
            {
                sizeGroup: selectSizeGroup(mockState, 'arb-itm-000-006'), // Classic Roast Beef Medium Meal
                product: selectProductById(mockState, 'arb-itm-002-012'),
                disabled: false,
                isGroupedBySize: true,
            },
            {
                sizeGroup: selectSizeGroup(mockState, 'arb-itm-000-007'), // Classic Roast Beef Large Meal
                product: selectProductById(mockState, 'arb-itm-002-013'),
                disabled: false,
                isGroupedBySize: true,
            },
        ]);
    });

    it('should return null if combo product is not present', () => {
        const result = selectChildItemSizes(mockState, 'arb-itm-002-012', 'some-id');

        expect(result).toBe(null);
    });
});
