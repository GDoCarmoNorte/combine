import { selectDefaultModifiers } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectDefaultModifiers', () => {
    const mockState = { domainMenu: { payload: menu } } as any;
    const selector = selectDefaultModifiers();

    it('should return default modifiers for single product', () => {
        const result = selector(mockState, 'arb-itm-000-001'); // Classic roast beef

        expect(result).toEqual([
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 1,
                name: 'Roast Beef',
                productId: 'arb-itm-006-001',
                calories: null,
                metadata: {
                    MODIFIER_GROUP_ID: '640214237',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 1,
                name: 'Sesame Seed Bun',
                productId: 'arb-itm-013-001',
                calories: 206,
                metadata: {
                    MODIFIER_GROUP_ID: '640214236',
                },
            },
        ]);
    });

    it('should return default modifiers for meal', () => {
        const result = selector(mockState, 'arb-itm-000-007'); // Classic roast beef meal

        expect(result).toEqual([
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 1,
                name: 'Roast Beef',
                productId: 'arb-itm-006-001',
                calories: null,
                metadata: {
                    MODIFIER_GROUP_ID: '640214237',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 1,
                name: 'Sesame Seed Bun',
                productId: 'arb-itm-013-001',
                calories: 206,
                metadata: {
                    MODIFIER_GROUP_ID: '640214236',
                },
            },
        ]);
    });

    it('should return default modifiers for product with modifiers with selections', () => {
        const result = selector(mockState, 'arb-itm-000-169'); // King's Hawaiian Action Code Test

        expect(result).toEqual([
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 1,
                name: 'Crispy Fish Fillet',
                productId: 'arb-itm-006-020',
                calories: null,
                metadata: {
                    MODIFIER_GROUP_ID: '640214237',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 0,
                name: 'Natural Cheddar',
                productId: 'arb-itm-007-004',
                calories: 76,
                metadata: {
                    MODIFIER_GROUP_ID: '640214193',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 0,
                name: 'Shredded Lettuce',
                productId: 'arb-itm-008-011',
                calories: 3,
                metadata: {
                    MODIFIER_GROUP_ID: '640214192',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 0,
                name: 'Tomato',
                productId: 'arb-itm-008-005',
                calories: 7,
                metadata: {
                    MODIFIER_GROUP_ID: '640214192',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 0,
                name: 'Tartar Sauce',
                productId: 'arb-itm-009-027',
                relatedSelections: ['arb-itm-009-030', 'arb-itm-009-028', 'arb-itm-009-029'],
                selection: 'Regular',
                calories: null,
                metadata: {
                    MODIFIER_GROUP_ID: '640214194',
                },
            },
            {
                defaultQuantity: 1,
                maxQuantity: 1,
                minQuantity: 1,
                name: "KING'S HAWAIIAN Bun",
                productId: 'arb-itm-013-010',
                calories: null,
                metadata: {
                    MODIFIER_GROUP_ID: '640214236',
                },
            },
        ]);
    });
});
