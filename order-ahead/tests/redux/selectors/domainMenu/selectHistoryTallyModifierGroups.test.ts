import { IOrderProductModel } from '../../../../@generated/webExpApi';
import { selectHistoryTallyModifierGroups } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectDefaultModifiers', () => {
    const mockState = { domainMenu: { payload: menu } } as any;
    const mockHistoryProduct: IOrderProductModel[] = [
        {
            children: [
                {
                    modifierGroups: [
                        {
                            productId: 'arb-prg-001-001',
                            modifiers: [
                                {
                                    price: 0.79,
                                    productId: 'arb-itm-006-001',
                                    quantity: 1,
                                    modifierGroups: [
                                        {
                                            productId: 'arb-prg-001-003',
                                            modifiers: [
                                                {
                                                    productId: 'arb-itm-008-011',
                                                    price: 1,
                                                    quantity: 1,
                                                },
                                            ],
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
            description: 'Buffalo Bleu Burger',
            id: 'arb-itm-000-001',
            modifiers: [
                {
                    id: 'arb-itm-006-001',
                    quantity: 1,
                },
                {
                    id: 'arb-itm-006-021',
                    quantity: 1,
                },
            ],
        },
    ];

    it('should return select modifiers for history product', () => {
        const result = selectHistoryTallyModifierGroups(mockState, mockHistoryProduct); // Classic roast beef

        expect(result).toEqual([
            [
                {
                    productId: 'arb-prg-001-001',
                    metadata: { MODIFIER_GROUP_ID: '640214237' },
                    isOnSideChecked: false,
                    modifiers: [
                        { productId: 'arb-itm-006-021', price: 2, quantity: 1 },
                        {
                            productId: 'arb-itm-006-001',
                            price: 2,
                            quantity: 1,
                            modifierGroups: [
                                {
                                    isOnSideChecked: false,
                                    modifiers: [
                                        {
                                            price: 1,
                                            productId: 'arb-itm-008-011',
                                            quantity: 1,
                                        },
                                    ],
                                    productId: 'arb-prg-001-003',
                                },
                            ],
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    metadata: { MODIFIER_GROUP_ID: '640214193' },
                    isOnSideChecked: false,
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-003',
                    metadata: { MODIFIER_GROUP_ID: '640214192' },
                    isOnSideChecked: false,
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-004',
                    metadata: { MODIFIER_GROUP_ID: '640214194' },
                    isOnSideChecked: false,
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    metadata: { MODIFIER_GROUP_ID: '640214194' },
                    isOnSideChecked: false,
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    metadata: { MODIFIER_GROUP_ID: '640221053' },
                    isOnSideChecked: false,
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    metadata: { MODIFIER_GROUP_ID: '640214236' },
                    isOnSideChecked: false,
                    modifiers: [],
                },
            ],
        ]);
    });

    it('should return empty array modifiers for history product', () => {
        const result = selectHistoryTallyModifierGroups(mockState, []); // Classic roast beef

        expect(result).toEqual([]);
    });
});
