import { selectProductByIds } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/bwwMenuMock.json';

const mockState = { domainMenu: { payload: menu } } as any;

describe('selectProductByIds', () => {
    it('should select product by ids', () => {
        expect(selectProductByIds(mockState, ['IDPSalesItem-5264', 'IDPSalesItem-5265'])).toEqual(
            expect.objectContaining({
                categoryIds: ['IDPSubMenu-247'],
                id: 'IDPSalesItem-5264',
                isSaleable: false,
                makeItAMeal: false,
                name: 'La Marca Procescco',
                price: {
                    currentPrice: 11.5,
                },
                productGroupId: 'IDPProductGroup-0032',
                sequence: 1,
                sizeGroupId: 'IDPSizeGroup-9999',
            })
        );
    });
});
