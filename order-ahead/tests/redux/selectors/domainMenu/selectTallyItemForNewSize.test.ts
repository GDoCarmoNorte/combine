import { selectTallyItemForNewSize } from '../../../../redux/selectors/domainMenu';

import {
    tallyItemMealMediumMock,
    tallyItemMealSmallSizeMock,
    tallyItemSingleProductMediumSizeMock,
    tallyItemSingleProductSmalSizeMock,
    tallyItemSingleProductSnackSizeMock,
} from '../__mocks__/tallyItem.mock';
import menu from '../__mocks__/menuMock.json';
import orderLocation from '../__mocks__/orderLocationMock.json';

describe('selectTallyItemForNewSize', () => {
    const stateMock = { domainMenu: { payload: menu }, orderLocation } as any;

    it('should select tally item for meal', () => {
        const result = selectTallyItemForNewSize(stateMock, 'arb-itm-000-006', tallyItemMealSmallSizeMock);

        expect(result).toEqual(tallyItemMealMediumMock);
    });

    it('should select tally item for single product', () => {
        const result = selectTallyItemForNewSize(stateMock, 'arb-itm-000-002', tallyItemSingleProductSmalSizeMock);

        expect(result).toEqual(tallyItemSingleProductMediumSizeMock);
    });

    it('should return empty array for product without modifierGroups', () => {
        const result = selectTallyItemForNewSize(stateMock, 'arb-itm-003-129', tallyItemSingleProductSnackSizeMock);

        expect(result.modifierGroups).toEqual([]);
    });
});
