import { selectProductSizes, selectProductById, selectSizeGroup } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectProductSizes', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return all sizes for product', () => {
        const result = selectProductSizes(mockState, 'arb-itm-000-001');

        // selectProductSizes returns products without isSaleable flag
        const removeSaleableFlag = (item: any) => {
            const newItem = item;
            delete newItem.product.isSaleable;
            return newItem;
        };

        expect(result).toEqual(
            [
                {
                    sizeGroup: selectSizeGroup(mockState, 'arb-itm-000-001'),
                    product: selectProductById(mockState, 'arb-itm-000-001'),
                    disabled: false,
                    isGroupedBySize: true,
                },
                {
                    sizeGroup: selectSizeGroup(mockState, 'arb-itm-000-002'),
                    product: selectProductById(mockState, 'arb-itm-000-002'),
                    disabled: false,
                    isGroupedBySize: true,
                },
                {
                    sizeGroup: selectSizeGroup(mockState, 'arb-itm-000-003'),
                    product: selectProductById(mockState, 'arb-itm-000-003'),
                    disabled: false,
                    isGroupedBySize: true,
                },
            ].map(removeSaleableFlag)
        );
    });

    it('should return null if product is not present', () => {
        const result = selectProductSizes(mockState, 'some-id');

        expect(result).toEqual(null);
    });
});
