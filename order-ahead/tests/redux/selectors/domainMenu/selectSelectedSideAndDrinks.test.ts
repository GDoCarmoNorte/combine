import { selectSelectedSideAndDrinks } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';
import { comboTallyItemMock, standaloneTalyItemMock } from '../__mocks__/tallyItem.mock';

describe('selectSelectedSideAndDrinks', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return side and drinks for combo product', () => {
        const result = selectSelectedSideAndDrinks(mockState, comboTallyItemMock);

        expect(result).toEqual(['Curly Fries', 'Coca-Cola®']);
    });

    it('should return empty array for standalone product', () => {
        const result = selectSelectedSideAndDrinks(mockState, standaloneTalyItemMock);

        expect(result).toEqual([]);
    });
});
