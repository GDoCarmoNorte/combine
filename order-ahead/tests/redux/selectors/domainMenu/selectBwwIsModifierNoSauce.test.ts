import { selectBwwIsModifierNoSauce } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/bwwMenuMock.json';

describe('selectBwwOnSideSauces', () => {
    it('should return true if "no sauce" option', () => {
        const mockState = {
            domainMenu: { payload: menu },
        } as any;

        const result = selectBwwIsModifierNoSauce(mockState, 'IDPModifier-25236');

        expect(result).toBe(true);
    });

    it('should return false if no "no sauce" option', () => {
        const mockState = {
            domainMenu: { payload: menu },
        } as any;

        const result = selectBwwIsModifierNoSauce(mockState, 'IDPModifier-25235-RG');

        expect(result).toBe(false);
    });
});
