import { selectComboMainProduct, selectProductById } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectComboMainProduct', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should select main product', () => {
        const result = selectComboMainProduct(mockState, 'arb-itm-000-005'); // Classic roast beef small meal

        expect(result).toEqual(selectProductById(mockState, 'arb-itm-000-001')); // Classic Roast Beef
    });

    it('should return null if product is not combo', () => {
        const result = selectComboMainProduct(mockState, 'arb-itm-000-001');

        expect(result).toEqual(null);
    });
});
