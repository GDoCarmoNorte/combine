import { selectIsProductHasRequiredModifiers } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/bwwMenuMock.json';

describe('selectIsProductHasRequiredModifiers', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return correct value for required modifiers', () => {
        expect(selectIsProductHasRequiredModifiers(mockState, 'IDPModifier-31001')).toEqual(true);
    });

    it('should return correct value for required nested modifiers', () => {
        expect(selectIsProductHasRequiredModifiers(mockState, 'IDPSalesItem-5356')).toEqual(true);
    });

    it('should return correct value for not required nested modifiers', () => {
        expect(selectIsProductHasRequiredModifiers(mockState, 'IDPModifier-24348')).toEqual(false);
    });
});
