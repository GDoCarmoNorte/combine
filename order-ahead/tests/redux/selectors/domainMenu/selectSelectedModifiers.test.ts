import { selectSelectedModifiers } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';
import {
    comboTallyItemMock,
    standaloneTalyItemMock,
    singleTallyItemWithModsWithSelectionsMock,
    comboTallyItemWithModsWithSelectionsMock,
} from '../__mocks__/tallyItem.mock';
import menuBww from '../__mocks__/bwwMenuMock.json';

import { burgerTallyItemWithSubModifiers } from '../__mocks__/bwwTallyItem.mock';

const roastBeefselectedModifiersResult = [
    {
        name: 'Roast Beef',
        price: 2,
        productId: 'arb-itm-006-001',
        quantity: 1,
        calories: null,
        metadata: {
            MODIFIER_GROUP_ID: '640214237',
        },
    },
    {
        name: 'Sesame Seed Bun',
        price: 0,
        productId: 'arb-itm-013-001',
        quantity: 1,
        calories: 206,
        metadata: {
            MODIFIER_GROUP_ID: '640214236',
        },
    },
];

const kingsHawaiianModifiersResult = [
    {
        name: 'Crispy Fish Fillet',
        price: 0.99,
        productId: 'arb-itm-006-020',
        quantity: 1,
        calories: null,
    },
    {
        name: 'Natural Cheddar',
        price: 0.49,
        productId: 'arb-itm-007-004',
        quantity: 1,
        calories: 76,
    },
    {
        name: 'Shredded Lettuce',
        price: 0.29,
        productId: 'arb-itm-008-011',
        quantity: 1,
        calories: 3,
    },
    {
        name: 'Tomato',
        price: 0.29,
        productId: 'arb-itm-008-005',
        quantity: 1,
        calories: 7,
    },
    {
        name: 'Tartar Sauce',
        price: 0,
        productId: 'arb-itm-009-029',
        quantity: 1,
        relatedSelections: ['arb-itm-009-030', 'arb-itm-009-027', 'arb-itm-009-028'],
        selection: 'Light',
        calories: null,
    },
    {
        name: "KING'S HAWAIIAN Bun",
        price: 0.5,
        productId: 'arb-itm-013-010',
        quantity: 1,
        calories: null,
    },
];

const burgerBwwSelectedModifiersResult = [
    {
        name: 'Tomato',
        quantity: 1,
        productId: 'IDPModifier-16411',
        price: 0,
        calories: 10,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Onion',
        quantity: 1,
        productId: 'IDPModifier-16410',
        price: 0,
        calories: 5,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Bun',
        quantity: 1,
        productId: 'IDPModifier-16409',
        price: 0,
        calories: 150,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Bleu Cheese',
        quantity: 1,
        productId: 'IDPModifier-17317',
        price: 0,
        calories: 100,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Pickles',
        quantity: 1,
        productId: 'IDPModifier-16414',
        price: 0,
        calories: 7,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Lettuce',
        quantity: 1,
        productId: 'IDPModifier-27449',
        price: 0,
        calories: 5,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Medium',
        quantity: 1,
        productId: 'IDPModifier-23901',
        price: 0,
        calories: null,
        metadata: undefined,
        modifiers: undefined,
    },
    {
        name: 'Tots',
        quantity: 1,
        productId: 'IDPModifier-30749',
        price: 1,
        calories: null,
        metadata: {
            MODIFIER_GROUP_ID: '16051',
            MODIFIER_GROUP_TYPE: 'Selections',
        },
        modifiers: [
            {
                name: undefined,
                quantity: 1,
                productId: 'IDPModifier-13117',
                price: 0.75,
                calories: null,
            },
            {
                name: undefined,
                quantity: 1,
                productId: 'IDPModifier-13118',
                price: 1.35,
                calories: null,
            },
        ],
    },
    {
        name: 'Bleu Cheese Crumbles',
        quantity: 1,
        productId: 'IDPModifier-3697',
        price: 0,
        calories: 50,
        metadata: undefined,
        modifiers: undefined,
    },
];

describe('selectSelectedModifiers', () => {
    const mockState = { domainMenu: { payload: menu } } as any;
    const mockStateBww = { domainMenu: { payload: menuBww } } as any;
    const selector = selectSelectedModifiers();

    it('should return selected modifiers for combo product', () => {
        const result = selector(mockState, comboTallyItemMock);

        expect(result).toEqual(roastBeefselectedModifiersResult);
    });

    it('should return selected modifiers for standalone product', () => {
        const result = selector(mockState, standaloneTalyItemMock);

        expect(result).toEqual(roastBeefselectedModifiersResult);
    });

    it('should return selected modifiers with selections for standalone product', () => {
        const result = selector(mockState, singleTallyItemWithModsWithSelectionsMock);

        expect(result).toEqual(kingsHawaiianModifiersResult);
    });

    it('should return selected modifiers with combo product', () => {
        const result = selector(mockState, comboTallyItemWithModsWithSelectionsMock);

        expect(result).toEqual(kingsHawaiianModifiersResult);
    });

    it('should return selected modidiers with sub modifers from product', () => {
        const result = selector(mockStateBww, burgerTallyItemWithSubModifiers);

        expect(result).toEqual(burgerBwwSelectedModifiersResult);
    });
});
