import { selectBwwIsRecommendedProduct } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/bwwMenuMock.json';

describe('selectBwwIsRecommendedProduct', () => {
    it('should return true if recommended', () => {
        const mockState = {
            domainMenu: { payload: menu },
        } as any;

        const result = selectBwwIsRecommendedProduct(mockState, 'IDPModifier-25386');

        expect(result).toBe(true);
    });

    it('should return false if not recommended', () => {
        const mockState = {
            domainMenu: { payload: menu },
        } as any;

        const result = selectBwwIsRecommendedProduct(mockState, 'IDPModifier-25235-RG');

        expect(result).toBe(false);
    });
});
