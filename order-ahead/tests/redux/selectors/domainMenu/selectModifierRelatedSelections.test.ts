import { selectModifierRelatedSelections } from '../../../../redux/selectors/domainMenu';
import menu from '../__mocks__/menuMock.json';

describe('selectModifierRelatedSelections', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return selections', () => {
        const result = selectModifierRelatedSelections(mockState, 'arb-itm-000-169', 'arb-itm-009-030');

        expect(result).toEqual([
            {
                sequence: 1,
                max: 1,
                min: 0,
                defaultQuantity: 1,
                itemId: 'arb-itm-009-027',
                overridePrice: {
                    currentPrice: 0,
                },
            },
            {
                sequence: 2,
                max: 1,
                min: 0,
                defaultQuantity: 0,
                itemId: 'arb-itm-009-028',
                overridePrice: {
                    currentPrice: 0,
                },
            },
            {
                sequence: 3,
                max: 1,
                min: 0,
                defaultQuantity: 0,
                itemId: 'arb-itm-009-029',
                overridePrice: {
                    currentPrice: 0,
                },
            },
        ]);
    });

    it('should return null if no selections', () => {
        const result = selectModifierRelatedSelections(mockState, 'arb-itm-000-999', 'arb-itm-006-001');

        expect(result).toBe(null);
    });
});
