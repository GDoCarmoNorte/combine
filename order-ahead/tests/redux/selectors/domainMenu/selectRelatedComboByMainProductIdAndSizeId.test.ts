import { selectRelatedComboByMainProductIdAndSizeId } from '../../../../redux/selectors/domainMenu';
import product from '../__mocks__/product.json';

describe('selectRelatedComboByMainProductIdAndSizeId', () => {
    const mockState = { domainMenu: { payload: { products: { 'arb-prd-000-002': product } } } } as any;

    const productId = 'arb-itm-000-018';
    const comboSizeId = 'arb-sig-000-002';

    it('should return related combo by main product id and size id', () => {
        const result = selectRelatedComboByMainProductIdAndSizeId(mockState, productId, comboSizeId);

        expect(result.id).toEqual('arb-itm-000-025');
    });
});
