import { selectAccountInfo } from '../../../../redux/selectors/account';
import accountMock from '../__mocks__/accountMock.json';

describe('selectAccountInfo', () => {
    it('should return account information correctly', () => {
        const { firstName, lastName, email, preferences } = accountMock;
        const mockState = {
            account: accountMock,
        } as any;

        const result = selectAccountInfo(mockState);
        const expectedResult = {
            firstName,
            lastName,
            email,
            birthDate: '12/31',
            phone: '',
            preferences,
        };
        expect(result).toEqual(expectedResult);
    });

    it('should return account information correctly when get phone number', () => {
        const { firstName, lastName, email, preferences } = accountMock;
        const mockState = {
            account: {
                ...accountMock,
                phones: [{ number: '1231231234' }],
            },
        } as any;

        const result = selectAccountInfo(mockState);
        const expectedResult = {
            firstName,
            lastName,
            email,
            birthDate: '12/31',
            phone: '1231231234',
            preferences,
        };
        expect(result).toEqual(expectedResult);
    });

    it('should return an empty string if phone is not received', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const { phones, ...mockWithoutPhones } = accountMock;
        const mockState = {
            account: {
                ...mockWithoutPhones,
            },
        } as any;

        const result = selectAccountInfo(mockState);

        expect(result.phone).toEqual('');
    });
});
