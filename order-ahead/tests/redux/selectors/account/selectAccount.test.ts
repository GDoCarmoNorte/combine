import { selectAccount } from '../../../../redux/selectors/account';

describe('selectAccount', () => {
    it('should return account', () => {
        const mockState = {
            account: 'test',
        } as any;

        const result = selectAccount(mockState);
        expect(result).toEqual('test');
    });
});
