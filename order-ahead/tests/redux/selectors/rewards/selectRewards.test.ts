import { selectRewards } from '../../../../redux/selectors/rewards';

describe('selectRewards', () => {
    it('should return rewards', () => {
        const mockState = {
            rewards: 'test',
        } as any;

        const result = selectRewards(mockState);
        expect(result).toEqual('test');
    });
});
