import { selectFirstExpiringOffer } from '../../../../redux/selectors/rewards';

import rewardMock from './rewards.mock';

describe('selectFirstExpiringOffer', () => {
    it('should return offer with the most early "endDateTime"', () => {
        const result = selectFirstExpiringOffer(rewardMock as any);
        expect(result).toEqual(rewardMock.rewards.offers[1]);
    });

    it('should return undefinded if offers array is empty', () => {
        const noOffersMock = { rewards: { offers: [] } };
        const result = selectFirstExpiringOffer(noOffersMock as any);
        expect(result).toBeUndefined();
    });
});
