import { selectOffers } from '../../../../redux/selectors/rewards';

describe('selectOffers', () => {
    it('should return offers', () => {
        const mockState = {
            rewards: {
                offers: [
                    {
                        id: 'test-1',
                    },
                ],
            },
        } as any;

        const result = selectOffers(mockState);
        expect(result).toEqual(mockState.rewards.offers);
    });
});
