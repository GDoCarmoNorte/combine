import { selectOfferById } from '../../../../redux/selectors/rewards';

describe('selectOfferById', () => {
    it('should return offer by id', () => {
        const mockState = {
            rewards: {
                offers: [
                    {
                        userOfferId: 'test-1',
                    },
                ],
            },
        } as any;

        const result = selectOfferById('test-1')(mockState);
        expect(result).toEqual({
            userOfferId: 'test-1',
        });
    });
});
