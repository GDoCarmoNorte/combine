export const mockWithApplicability = {
    customerId: '027c9cc6-5f51-4d51-ba4e-20a1ee7d6613',
    offers: [
        {
            id: 'c7f4ae1f-3fc8-4188-92ec-2cf0ca827817',
            posDiscountId: '654112542',
            name: 'free coocki',
            type: 'BUY_X_GET_Y_SET_PRICE',
            startDate: '2021-01-03T12:30:01.999Z',
            endDate: '2021-06-03T12:30:01.999Z',
            terms:
                'Offer valid 30 days after issuance. Offer is not transferable. Offer cannot be combined with any other coupon or offer. Limit one coupon per person, per order, per visit. Offer valid at participating U.S. locations only. Offer and participation in Hawaii and Alaska may vary. TM & © 2019 Arbys IP Holder, LLC. All rights reserved.',
            channels: ['WEBOA'],
            status: 'OPEN',
            termsLanguageCode: 'en',
            description: 'Get any size curly fries for just $1! Excludes loaded curly fries.',
            applicableLocations: ['2090'],
            applicability: {
                isIncludesAll: false,
                eligibleIds: [
                    {
                        menuId: 'arb-itm-000-003',
                        quantity: 1,
                    },
                ],
                buyIds: [
                    {
                        rule: 'all_of',
                        menuIds: [
                            { menuId: 'arb-itm-002-024', quantity: 1 },
                            { menuId: 'arb-itm-002-003', quantity: 1 },
                            { menuId: 'arb-itm-003-002', quantity: 1 },
                            { menuId: 'arb-itm-003-079', quantity: 1 },
                        ],
                    },
                ],
                getIds: [
                    {
                        rule: 'ALL_OF',
                        menuIds: [
                            {
                                menuId: 'arb-itm-000-003',
                                quantity: 1,
                            },
                        ],
                    },
                ],
            },
        },
    ],
};
export default {
    rewards: {
        offers: [
            { id: '1', endDateTime: new Date(2020, 5, 26, 0, 0, 0) },
            { id: '2', endDateTime: new Date(2020, 4, 27, 0, 0, 0) },
            { id: '3', endDateTime: new Date(2020, 5, 23, 0, 0, 0) },
            { id: '4', endDateTime: new Date(2020, 5, 11, 0, 0, 0) },
        ],
    },
};
