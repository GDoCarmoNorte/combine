import { selectTallyPriceAndCalories } from '../../../../redux/selectors/tally';
import menu from '../__mocks__/menuMock.json';
import menuBww from '../__mocks__/bwwMenuMock.json';
import { standaloneTalyItemMock } from '../__mocks__/tallyItem.mock';
import { burgerTallyItemWithSubModifiers } from '../__mocks__/bwwTallyItem.mock';
import getBrandInfo from '../../../../lib/brandInfo';

jest.mock('../../../../lib/brandInfo');

describe('selectTallyPriceAndCalories', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return price and calories', () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Arbys' });
        const result = selectTallyPriceAndCalories(mockState, standaloneTalyItemMock);

        expect(result).toEqual({
            calories: 360,
            price: 3.49,
            totalPrice: 3.49,
        });
    });

    it('should return price and calories: undefined with no info about calories', () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Arbys' });
        const result = selectTallyPriceAndCalories(mockState, {
            quantity: 1,
            productId: 'arb-itm-003-125',
            price: 1.79,
        });

        expect(result).toEqual({
            calories: undefined,
            price: 1.79,
            totalPrice: 1.79,
        });
    });

    it('should return price and calories: 0 with totalCalories: 0', () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Arbys' });
        const result = selectTallyPriceAndCalories(mockState, {
            quantity: 1,
            productId: 'arb-itm-003-112',
            price: 1.79,
        });

        expect(result).toEqual({
            calories: 0,
            price: 1.79,
            totalPrice: 1.79,
        });
    });

    it('should calculate price and calories with sub modifiers', () => {
        const mockBwwState = { domainMenu: { payload: menuBww } } as any;
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Bww' });
        const result = selectTallyPriceAndCalories(mockBwwState, burgerTallyItemWithSubModifiers);

        expect(result).toEqual({
            price: 15.59,
            calories: 1270,
            totalPrice: 18.89,
        });
    });
});
