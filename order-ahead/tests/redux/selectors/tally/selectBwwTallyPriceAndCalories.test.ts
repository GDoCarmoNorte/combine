import { createNextState } from '@reduxjs/toolkit';
import { selectBwwTallyPriceAndCalories } from '../../../../redux/selectors/tally';
import menu from '../__mocks__/bwwMenuMock.json';
import { burgerTallyItem } from '../__mocks__/bwwTallyItem.mock';

describe('selectBwwTallyPriceAndCalories', () => {
    const mockState = { domainMenu: { payload: menu } } as any;

    it('should return price and calories', () => {
        const result = selectBwwTallyPriceAndCalories(mockState, burgerTallyItem);

        expect(result).toEqual({
            calories: 1270,
            price: 12.49,
            totalPrice: 12.29,
        });
    });

    it('should return price and calories: no info about calories', () => {
        const result = selectBwwTallyPriceAndCalories(mockState, {
            quantity: 1,
            productId: 'IDPSalesItem-532223',
            price: 5.99,
        });

        expect(result).toEqual({
            calories: undefined,
            price: 5.99,
            totalPrice: 5.99,
        });
    });

    it('should correctly calculate calories for tally item with unselected default modifier', () => {
        const tallyItem = createNextState(burgerTallyItem, (item) => {
            item.modifierGroups[3].modifiers.splice(5, 1);
        });
        const result = selectBwwTallyPriceAndCalories(mockState, tallyItem);

        expect(result).toEqual({
            calories: 1265,
            price: 12.49,
            totalPrice: 12.29,
        });
    });

    it('should calculate price and calories with extra products', () => {
        const tallyItemWithExtra = { ...burgerTallyItem, childExtras: [burgerTallyItem] };

        const result = selectBwwTallyPriceAndCalories(mockState, tallyItemWithExtra);

        expect(result).toEqual({
            calories: 2540,
            price: 24.98,
            totalPrice: 24.58,
        });
    });
});
