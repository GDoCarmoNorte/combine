import {
    selectPersonalizationFacts,
    selectIsAllDependenciesInitialized,
} from '../../../redux/selectors/personalization';
import { FACTS } from '../../../common/constants/personalization';
import { OrderLocationMethod } from '../../../redux/orderLocation';
import { useAppSelector } from '../../../redux/store';
import configuration from '../../mocks/configFeatureFlag.mock';

jest.mock('../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

describe('personalization selectors', () => {
    beforeEach(() => {
        (useAppSelector as jest.Mock).mockReturnValue(configuration);
    });
    const mock: any = {
        account: {
            idpCustomerId: 1,
        },
        configuration,
        orderLocation: {
            method: OrderLocationMethod.PICKUP,
            pickupAddress: {
                id: '26',
                brandName: 'Buffalo Wild Wings',
                displayName: 'Chanhassen, MN',
                status: 'OPEN',
                timezone: 'America/Chicago',
                isDigitallyEnabled: true,
                services: [
                    {
                        type: 'ORDER_AHEAD',
                        hours: [],
                    },
                ],
                details: {
                    latitude: 44.9339104,
                    longitude: -93.3428846,
                },
                additionalFeatures: {
                    maxOrderAmount: 175,
                },
                legalWarnings: [],
                isOnlineOrderAvailable: true,
                isClosed: false,
                url: 'us/mn/st-louis-park/3820-grand-way/sports-bar-26',
            },
            deliveryAddress: {
                pickUpLocation: {
                    id: 67,
                    isDeliveryEnabled: true,
                    utcOffset: 'UTC-06:00',
                },
                locationDetails: {
                    id: '67',
                    timezone: 'America/Chicago',
                    isDigitallyEnabled: true,
                    services: [
                        {
                            type: 'DELIVERY',
                            hours: [],
                        },
                    ],
                },
            },
        },
        ldp: { locationDetails: {} },
        loyalty: {},
        personalization: {
            dependencies: {
                dep1: {
                    initialized: false,
                },
            },
        },
    };

    const get = (facts, factType) => {
        return facts.find((c) => c.name === factType)?.value;
    };

    describe('selectPersonalizationFacts', () => {
        describe('user related', () => {
            it('authenticated user', () => {
                expect(get(selectPersonalizationFacts(mock), FACTS.AUTHENTICATED_USER)).toEqual(true);
            });

            it('not authenticated user', () => {
                expect(get(selectPersonalizationFacts({ ...mock, account: null }), FACTS.AUTHENTICATED_USER)).toEqual(
                    false
                );
            });
        });

        describe('location related', () => {
            it('no location', () => {
                const data = selectPersonalizationFacts({ ...mock, orderLocation: {} });

                expect(get(data, FACTS.LOCATION_ID)).toEqual(null);
                expect(get(data, FACTS.IS_LOCATION_SELECTED)).toEqual(false);
                expect(get(data, FACTS.IS_OA_LOCATION)).toEqual(false);
            });

            it('pickup location', () => {
                const data = selectPersonalizationFacts(mock);

                expect(get(data, FACTS.LOCATION_ID)).toEqual(mock.orderLocation.pickupAddress.id);
                expect(get(data, FACTS.IS_LOCATION_SELECTED)).toEqual(true);
                expect(get(data, FACTS.IS_OA_LOCATION)).toEqual(true);
            });

            it('delivery location', () => {
                const data = selectPersonalizationFacts({
                    ...mock,
                    orderLocation: { ...mock.orderLocation, method: OrderLocationMethod.DELIVERY },
                });

                expect(get(data, FACTS.LOCATION_ID)).toEqual('67');
                expect(get(data, FACTS.IS_LOCATION_SELECTED)).toEqual(true);
                expect(get(data, FACTS.IS_OA_LOCATION)).toEqual(true);
            });
        });

        describe('time related', () => {
            beforeAll(() => {
                const DateTimeFormat = Intl.DateTimeFormat;
                jest.useFakeTimers('modern').setSystemTime(new Date('2021-01-01T00:00:00z').getTime());
                jest.spyOn(global.Intl, 'DateTimeFormat').mockImplementation(
                    (locale, options) => new DateTimeFormat(locale, { ...options, timeZone: 'UTC' })
                );
            });

            it('user timezone', () => {
                const data = selectPersonalizationFacts({
                    ...mock,
                    orderLocation: {},
                });

                expect(get(data, FACTS.DATE)).toEqual('01/01/2021');
                expect(get(data, FACTS.TIME)).toEqual('00:00');
                expect(get(data, FACTS.DAY_OF_THE_WEEK)).toEqual('Friday');
            });

            it('pickup location timezone', () => {
                const data = selectPersonalizationFacts(mock);

                expect(get(data, FACTS.DATE)).toEqual('12/31/2020');
                expect(get(data, FACTS.TIME)).toEqual('18:00');
                expect(get(data, FACTS.DAY_OF_THE_WEEK)).toEqual('Thursday');
            });

            it('delivery location timezone', () => {
                const data = selectPersonalizationFacts({
                    ...mock,
                    orderLocation: {
                        ...mock.orderLocation,
                        method: OrderLocationMethod.DELIVERY,
                    },
                });

                expect(get(data, FACTS.DATE)).toEqual('12/31/2020');
                expect(get(data, FACTS.TIME)).toEqual('18:00');
                expect(get(data, FACTS.DAY_OF_THE_WEEK)).toEqual('Thursday');
            });
        });
    });

    it('selectIsAllDependenciesInitialized', () => {
        expect(selectIsAllDependenciesInitialized(mock)).toEqual(false);
        expect(selectIsAllDependenciesInitialized({ ...mock, personalization: { dependencies: {} } })).toEqual(true);
    });
});
