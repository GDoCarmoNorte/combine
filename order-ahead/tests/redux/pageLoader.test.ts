import { configureStore } from '@reduxjs/toolkit';
import pageLoaderReducer, { actions } from '../../redux/pageLoader';
import { actions as tallyActions } from '../../redux/tallyOrder';

describe('pageLoader', () => {
    const store = configureStore({
        reducer: {
            pageLoader: pageLoaderReducer,
        },
    });

    it('should show and hide loader on showLoader and hideLoader actions', () => {
        store.dispatch(actions.showLoader());

        expect(store.getState().pageLoader.show).toBe(true);

        store.dispatch(actions.hideLoader());

        expect(store.getState().pageLoader.show).toBe(false);
    });

    it('should hide loader on tally submit rejected action', () => {
        store.dispatch(actions.showLoader());

        expect(store.getState().pageLoader.show).toBe(true);

        store.dispatch(tallyActions.invalid(null));

        expect(store.getState().pageLoader.show).toBe(false);
    });

    it('should hide loader on tally submit reset action', () => {
        store.dispatch(actions.showLoader());

        expect(store.getState().pageLoader.show).toBe(true);

        store.dispatch(tallyActions.reset(null));

        expect(store.getState().pageLoader.show).toBe(false);
    });
});
