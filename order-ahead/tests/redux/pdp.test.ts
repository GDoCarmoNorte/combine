import { configureStore } from '@reduxjs/toolkit';
import pdpReducer, { actions, PDPTallyItem } from '../../redux/pdp';
import { selectDefaultTallyItem } from '../../redux/selectors/domainMenu';
import { AppStore } from '../../redux/store';
import tallyItemMock from '../mocks/tallyItem.mock';

jest.mock('../../redux/selectors/domainMenu');

describe('pdp slice', () => {
    let store: AppStore;

    beforeEach(() => {
        store = configureStore({
            reducer: {
                pdp: pdpReducer,
            } as any,
        });
    });

    const setTallyItemToStore = (mock: PDPTallyItem = tallyItemMock) =>
        store.dispatch(actions.putTallyItem({ pdpTallyItem: mock }));

    const getPdpState = () => store.getState().pdp;

    const defaultTallyItem = { ...tallyItemMock, productId: 'default-tally-item-product-id' };

    describe('putInitialTallyItemId action', () => {
        it('should put initial tally item to store', () => {
            store.dispatch(actions.putInitialTallyItemId({ defaultTallyItemId: defaultTallyItem.productId }));

            expect(getPdpState()).toEqual({
                initialTallyItemId: defaultTallyItem.productId,
                tallyItem: {
                    lineItemId: null,
                    modifierGroups: null,
                    price: null,
                    productId: null,
                    quantity: 1,
                },
            });
        });
    });

    describe('putTallyItem action', () => {
        it('should put tally item to store', () => {
            store.dispatch(actions.putTallyItem({ pdpTallyItem: tallyItemMock }));

            expect(getPdpState()).toEqual({
                modifyingFromBag: false,
                tallyItem: {
                    ...tallyItemMock,
                    madeAMeal: undefined,
                },
            });
        });
    });

    describe('editTallyItemSize action', () => {
        it('should edit tally item size', () => {
            setTallyItemToStore();
            const newSizeTallyItem = { ...tallyItemMock, productId: 'new-size-id' };

            store.dispatch(actions.editTallyItemSize({ pdpTallyItem: newSizeTallyItem }));

            expect(getPdpState()).toEqual({
                modifyingFromBag: false,
                tallyItem: newSizeTallyItem,
            });
        });
    });

    describe('editTallyItemModifiers action', () => {
        it('should edit tally item modifiers', () => {
            setTallyItemToStore();

            const tallyItemEditedModifiers = JSON.parse(JSON.stringify(tallyItemMock));
            tallyItemEditedModifiers.modifierGroups[0].modifiers[0].quantity = 2;

            store.dispatch(actions.editTallyItemModifiers({ pdpTallyItem: tallyItemEditedModifiers }));

            expect(getPdpState()).toEqual({
                modifyingFromBag: false,
                tallyItem: tallyItemEditedModifiers,
            });
        });
    });

    describe('editTallyItemCount action', () => {
        const tallyItemFromBag = { ...tallyItemMock, lineItemId: 1 };

        it('should not edit count when pageProductId is different from pdp tally item id', () => {
            setTallyItemToStore(tallyItemFromBag);

            store.dispatch(actions.editTallyItemCount({ pageProductId: 'some-id', value: 3, lineItemId: 1 }));

            expect(getPdpState()).toEqual(getPdpState());
        });

        it('should not edit count when lineItemId is different from pdp tally item lineItemId', () => {
            setTallyItemToStore(tallyItemFromBag);

            store.dispatch(
                actions.editTallyItemCount({ pageProductId: tallyItemFromBag.productId, value: 3, lineItemId: 0 })
            );

            expect(getPdpState()).toEqual(getPdpState());
        });

        it('should edit count', () => {
            setTallyItemToStore(tallyItemFromBag);

            store.dispatch(
                actions.editTallyItemCount({ pageProductId: tallyItemFromBag.productId, value: 1, lineItemId: 1 })
            );

            expect(getPdpState()).toEqual({
                modifyingFromBag: false,
                tallyItem: {
                    ...tallyItemFromBag,
                    madeAMeal: undefined,
                    quantity: 2,
                },
            });
        });
    });

    describe('resetPdpState', () => {
        it('should reset pdp state to default by initialTallyItemId', () => {
            (selectDefaultTallyItem as jest.Mock).mockReturnValueOnce(defaultTallyItem);
            setTallyItemToStore();
            store.dispatch(actions.putInitialTallyItemId({ defaultTallyItemId: defaultTallyItem.productId }));
            store.dispatch(actions.putTallyItem({ pdpTallyItem: tallyItemMock }));

            const newSizeTallyItem = { ...tallyItemMock, productId: 'new-size-id' };
            store.dispatch(actions.editTallyItemSize({ pdpTallyItem: newSizeTallyItem }));

            store.dispatch(actions.resetPdpState());

            expect(getPdpState()).toEqual({
                modifyingFromBag: false,
                tallyItem: defaultTallyItem,
                initialTallyItemId: defaultTallyItem.productId,
            });
        });
    });
});
