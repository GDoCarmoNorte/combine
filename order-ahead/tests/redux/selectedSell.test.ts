import selectedSellReducer, * as SelectedSellStore from '../../redux/selectedSell';
import { AnyAction } from '@reduxjs/toolkit';

let mockSelectedSellState = {
    correlationId: '',
    recommendationId: '',
    recommendedProducts: [],
};

describe('selected sell reducer', () => {
    beforeEach(() => {
        mockSelectedSellState = {
            correlationId: '',
            recommendationId: '',
            recommendedProducts: [],
        };
    });

    it('should return the initial state', () => {
        expect(selectedSellReducer(undefined, {} as AnyAction)).toEqual(SelectedSellStore.initialState);
    });

    it(`should handle ${SelectedSellStore.actions.setCorrelationId}`, () => {
        const payload = 'some string';
        mockSelectedSellState.correlationId = payload;

        expect(
            selectedSellReducer(SelectedSellStore.initialState, {
                type: SelectedSellStore.actions.setCorrelationId,
                payload,
            })
        ).toEqual(mockSelectedSellState);
    });

    it(`should handle ${SelectedSellStore.actions.fetchRecommendedItemsSuccess}`, () => {
        const payload = {
            productIds: ['arb-itm-002-002', 'arb-itm-002-038', 'arb-itm-002-031'],
            id: '123-id',
        };

        mockSelectedSellState.recommendedProducts = payload.productIds;
        mockSelectedSellState.recommendationId = payload.id;

        expect(
            selectedSellReducer(SelectedSellStore.initialState, {
                type: SelectedSellStore.actions.fetchRecommendedItemsSuccess,
                payload,
            })
        ).toEqual(mockSelectedSellState);
    });

    it(`should handle ${SelectedSellStore.actions.fetchRecommendedItemsFailure}`, () => {
        const payload = {
            productIds: ['arb-itm-002-002', 'arb-itm-002-038', 'arb-itm-002-031'],
            id: '321-id',
        };

        expect(
            selectedSellReducer(SelectedSellStore.initialState, {
                type: SelectedSellStore.actions.fetchRecommendedItemsFailure,
                payload,
            })
        ).toEqual(mockSelectedSellState);
    });
});
