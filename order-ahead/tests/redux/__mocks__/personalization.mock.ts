export const personalizationStateMock = {
    loading: false,
    actions: {
        HPMainHero: { section: 'hero1' },
    },
};
export const actionParametersMock = {
    sys: {
        type: 'Array',
    },
    total: 2,
    skip: 0,
    limit: 100,
    items: [
        {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '4Lv5T4zp90Hukxq7FGyNke',
                type: 'Entry',
                createdAt: '2021-10-29T07:32:05.144Z',
                updatedAt: '2021-10-29T07:34:08.710Z',
                environment: {
                    sys: {
                        id: 'feature-feature-DBBP-49874',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 2,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'actionParameter',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                tableReference: 'hero1',
                section: {
                    metadata: {
                        tags: [],
                    },
                    sys: {
                        space: {
                            sys: {
                                type: 'Link',
                                linkType: 'Space',
                                id: 'l5fkpck1mwg3',
                            },
                        },
                        id: '6cKiA2iDjV4LyPNEbYaFmI',
                        type: 'Entry',
                        createdAt: '2021-10-06T14:46:52.705Z',
                        updatedAt: '2021-10-06T14:46:52.705Z',
                        environment: {
                            sys: {
                                id: 'feature-feature-DBBP-49874',
                                type: 'Link',
                                linkType: 'Environment',
                            },
                        },
                        revision: 1,
                        contentType: {
                            sys: {
                                type: 'Link',
                                linkType: 'ContentType',
                                id: 'mainBanner',
                            },
                        },
                        locale: 'en-US',
                    },
                    fields: {
                        topText: 'Next Fight',
                        mainText: 'UFC 266',
                        bottomText: {
                            data: {},
                            content: [
                                {
                                    data: {},
                                    content: [
                                        {
                                            data: {},
                                            marks: [],
                                            value: ' ',
                                            nodeType: 'text',
                                        },
                                    ],
                                    nodeType: 'paragraph',
                                },
                            ],
                            nodeType: 'document',
                        },
                        backgroundImage: {
                            metadata: {
                                tags: [],
                            },
                            sys: {
                                space: {
                                    sys: {
                                        type: 'Link',
                                        linkType: 'Space',
                                        id: 'l5fkpck1mwg3',
                                    },
                                },
                                id: '3vgDSw2JPpRA2FPoEEbROY',
                                type: 'Asset',
                                createdAt: '2021-10-05T14:46:08.227Z',
                                updatedAt: '2021-10-05T14:46:08.227Z',
                                environment: {
                                    sys: {
                                        id: 'feature-feature-DBBP-49874',
                                        type: 'Link',
                                        linkType: 'Environment',
                                    },
                                },
                                revision: 1,
                                locale: 'en-US',
                            },
                            fields: {
                                title: 'BWW Tailgate Bundle',
                                file: {
                                    url:
                                        '//images.ctfassets.net/l5fkpck1mwg3/3vgDSw2JPpRA2FPoEEbROY/5fd85e654ea69e20e43ffad98c8f5fd3/BWW_Tailgate_Bundle_desktop_4000x1650.png',
                                    details: {
                                        size: 2311111,
                                        image: {
                                            width: 4000,
                                            height: 1650,
                                        },
                                    },
                                    fileName: 'BWW_Tailgate Bundle_desktop_4000x1650.png',
                                    contentType: 'image/png',
                                },
                            },
                        },
                        mainLinkText: 'Get to the Bar',
                        mainLinkHref: {
                            sys: {
                                type: 'Link',
                                linkType: 'Entry',
                                id: '3bLedoxxy7XAwErYCv9cCi',
                            },
                        },
                    },
                },
            },
        },
    ],
    includes: {
        Entry: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '6cKiA2iDjV4LyPNEbYaFmI',
                    type: 'Entry',
                    createdAt: '2021-10-06T14:46:52.705Z',
                    updatedAt: '2021-10-06T14:46:52.705Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-49874',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'mainBanner',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    topText: 'Next Fight',
                    mainText: 'UFC 266',
                    bottomText: {
                        data: {},
                        content: [
                            {
                                data: {},
                                content: [
                                    {
                                        data: {},
                                        marks: [],
                                        value: ' ',
                                        nodeType: 'text',
                                    },
                                ],
                                nodeType: 'paragraph',
                            },
                        ],
                        nodeType: 'document',
                    },
                    backgroundImage: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '3vgDSw2JPpRA2FPoEEbROY',
                            type: 'Asset',
                            createdAt: '2021-10-05T14:46:08.227Z',
                            updatedAt: '2021-10-05T14:46:08.227Z',
                            environment: {
                                sys: {
                                    id: 'feature-feature-DBBP-49874',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'BWW Tailgate Bundle',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/3vgDSw2JPpRA2FPoEEbROY/5fd85e654ea69e20e43ffad98c8f5fd3/BWW_Tailgate_Bundle_desktop_4000x1650.png',
                                details: {
                                    size: 2311111,
                                    image: {
                                        width: 4000,
                                        height: 1650,
                                    },
                                },
                                fileName: 'BWW_Tailgate Bundle_desktop_4000x1650.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    mainLinkText: 'Get to the Bar',
                    mainLinkHref: {
                        sys: {
                            type: 'Link',
                            linkType: 'Entry',
                            id: '3bLedoxxy7XAwErYCv9cCi',
                        },
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '7yodxUkuKXUle4r1FNwktW',
                    type: 'Entry',
                    createdAt: '2021-07-13T15:14:58.809Z',
                    updatedAt: '2021-10-11T18:19:56.540Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-49874',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 7,
                    contentType: {
                        sys: {
                            type: 'Link',
                            linkType: 'ContentType',
                            id: 'mainBanner',
                        },
                    },
                    locale: 'en-US',
                },
                fields: {
                    topText: 'Limited Time Only',
                    mainText: 'New Truffalo Sauce',
                    mainTextLink: {
                        sys: {
                            type: 'Link',
                            linkType: 'Entry',
                            id: '3LNaJ1aSc9KJthmIcDy783',
                        },
                    },
                    bottomText: {
                        nodeType: 'document',
                        data: {},
                        content: [
                            {
                                nodeType: 'paragraph',
                                content: [
                                    {
                                        nodeType: 'text',
                                        value:
                                            '*NO PURCHASE NECESSARY TO PLAY. Open to legal residents of US & DC, 18 or older. Must have valid Blazin’ Rewards account and check in\u202fat a participating BWW\u202f\u202fto be eligible to win prizing. Game times vary by location.\u202fSeason Grand Prize will be allocated equally among eligible team players.\u202f\u202fSee official\u202frules\u202ffor full details, including number, description and ARV of prizes and game play terms at\u202fplay.buffalowildwings.com/rules.\u202f Outcome of game play determined by player’s skill. Void where prohibited by law. Sponsor: Buffalo Wild Wings, Inc. Three Glenlake Parkway NE Atlanta, GA 30328.\u202fBlazin’ Trivia Wednesday Team Night is not combined with points from participation in other Blazin’ Trivia events ',
                                        marks: [],
                                        data: {},
                                    },
                                ],
                                data: {},
                            },
                        ],
                    },
                    rightImage: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '4ACg4dvOf9fDyeBWP7E50u',
                            type: 'Asset',
                            createdAt: '2021-07-13T15:20:47.784Z',
                            updatedAt: '2021-07-13T15:20:47.784Z',
                            environment: {
                                sys: {
                                    id: 'feature-feature-DBBP-49874',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'BWW Truffalo Wings',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/4ACg4dvOf9fDyeBWP7E50u/48776be5a57af563ab9847bd099b6959/truffalo_wings_on_platter.png',
                                details: {
                                    size: 4400578,
                                    image: {
                                        width: 4000,
                                        height: 3000,
                                    },
                                },
                                fileName: 'truffalo_wings_on_platter.png',
                                contentType: 'image/png',
                            },
                        },
                    },
                    backgroundImage: {
                        metadata: {
                            tags: [],
                        },
                        sys: {
                            space: {
                                sys: {
                                    type: 'Link',
                                    linkType: 'Space',
                                    id: 'l5fkpck1mwg3',
                                },
                            },
                            id: '2veSXeRpTOVxJaVDblyrS5',
                            type: 'Asset',
                            createdAt: '2021-07-13T15:14:11.282Z',
                            updatedAt: '2021-07-13T15:14:11.282Z',
                            environment: {
                                sys: {
                                    id: 'feature-feature-DBBP-49874',
                                    type: 'Link',
                                    linkType: 'Environment',
                                },
                            },
                            revision: 1,
                            locale: 'en-US',
                        },
                        fields: {
                            title: 'BWW Distressed Background',
                            file: {
                                url:
                                    '//images.ctfassets.net/l5fkpck1mwg3/2veSXeRpTOVxJaVDblyrS5/3f7acfc34fb33e03f10df164aab90c21/bww_distressed_bckgd.jpeg',
                                details: {
                                    size: 188385,
                                    image: {
                                        width: 1280,
                                        height: 394,
                                    },
                                },
                                fileName: 'bww_distressed_bckgd.jpeg',
                                contentType: 'image/jpeg',
                            },
                        },
                    },
                    mainLinkText: 'Order Now',
                    mainLinkHref: {
                        sys: {
                            type: 'Link',
                            linkType: 'Entry',
                            id: '7xhVqDWiSDsAydvEbfzyR9',
                        },
                    },
                    mainLinkType: 'large',
                },
            },
        ],
        Asset: [
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '2veSXeRpTOVxJaVDblyrS5',
                    type: 'Asset',
                    createdAt: '2021-07-13T15:14:11.282Z',
                    updatedAt: '2021-07-13T15:14:11.282Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-49874',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'BWW Distressed Background',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/2veSXeRpTOVxJaVDblyrS5/3f7acfc34fb33e03f10df164aab90c21/bww_distressed_bckgd.jpeg',
                        details: {
                            size: 188385,
                            image: {
                                width: 1280,
                                height: 394,
                            },
                        },
                        fileName: 'bww_distressed_bckgd.jpeg',
                        contentType: 'image/jpeg',
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '3vgDSw2JPpRA2FPoEEbROY',
                    type: 'Asset',
                    createdAt: '2021-10-05T14:46:08.227Z',
                    updatedAt: '2021-10-05T14:46:08.227Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-49874',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'BWW Tailgate Bundle',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/3vgDSw2JPpRA2FPoEEbROY/5fd85e654ea69e20e43ffad98c8f5fd3/BWW_Tailgate_Bundle_desktop_4000x1650.png',
                        details: {
                            size: 2311111,
                            image: {
                                width: 4000,
                                height: 1650,
                            },
                        },
                        fileName: 'BWW_Tailgate Bundle_desktop_4000x1650.png',
                        contentType: 'image/png',
                    },
                },
            },
            {
                metadata: {
                    tags: [],
                },
                sys: {
                    space: {
                        sys: {
                            type: 'Link',
                            linkType: 'Space',
                            id: 'l5fkpck1mwg3',
                        },
                    },
                    id: '4ACg4dvOf9fDyeBWP7E50u',
                    type: 'Asset',
                    createdAt: '2021-07-13T15:20:47.784Z',
                    updatedAt: '2021-07-13T15:20:47.784Z',
                    environment: {
                        sys: {
                            id: 'feature-feature-DBBP-49874',
                            type: 'Link',
                            linkType: 'Environment',
                        },
                    },
                    revision: 1,
                    locale: 'en-US',
                },
                fields: {
                    title: 'BWW Truffalo Wings',
                    file: {
                        url:
                            '//images.ctfassets.net/l5fkpck1mwg3/4ACg4dvOf9fDyeBWP7E50u/48776be5a57af563ab9847bd099b6959/truffalo_wings_on_platter.png',
                        details: {
                            size: 4400578,
                            image: {
                                width: 4000,
                                height: 3000,
                            },
                        },
                        fileName: 'truffalo_wings_on_platter.png',
                        contentType: 'image/png',
                    },
                },
            },
        ],
    },
};
