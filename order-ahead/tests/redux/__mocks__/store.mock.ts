import { RootState } from '../../../redux/store';

const orderPayloadMock: RootState = {
    bag: {
        LineItems: [
            {
                productId: '674241907',
                quantity: 1,
                modifierGroups: [],
                price: 100,
                lineItemId: 111,
            },
        ],
        isOpen: false,
    },
    justAddedToBag: false,
    pdp: {},
    dismissedAlertBanners: [],
    domainMenu: {
        payload: {},
        loading: false,
        error: false,
    },
    orderLocation: {
        method: 'PICKUP',
        pickupAddress: {
            id: '1',
            displayName: 'Baker st',
            locationContactDetails: [{ contactDetails: [{ addressLine1: '221B' }] }],
        },
    },
    submitOrder: {
        error: null,
        isLoading: false,
        lastOrder: null,
    },
};

export default orderPayloadMock;
