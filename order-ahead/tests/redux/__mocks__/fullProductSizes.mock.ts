//import { ProductDetailDtoSizeEnum } from '../../../@generated/domainMenu/models/ProductDetailDto';
// import { IFullProductSizes } from '../../../redux/domainMenu';

// TODO MENU2.0 Fix tests

const rootProduct = {
    displayName: 'Sandwich',
    sizeSelections: [
        {
            displayName: 'Sandwich',
            name: 'Small',
            size: '',
            isSelected: true,
            productId: 'ARBYS-WEBOA-640223380',
            price: 1,
            enhancedAttributes: {
                calories: '410',
                nutritionalFacts: [
                    {
                        name: 'Serving Weight (g)',
                        value: '128',
                    },
                    {
                        name: 'Calories',
                        value: '410',
                    },
                    {
                        name: 'Calories from Fat',
                        value: '200',
                    },
                    {
                        name: 'Fat - Total (g)',
                        value: '22',
                    },
                    {
                        name: 'Saturated Fat (g)',
                        value: '3',
                    },
                    {
                        name: 'Trans Fat (g)',
                        value: '0',
                    },
                    {
                        name: 'Cholesterol (mg)',
                        value: '0',
                    },
                    {
                        name: 'Sodium (mg)',
                        value: '940',
                    },
                    {
                        name: 'Total Carbohydrates (g)',
                        value: '49',
                    },
                    {
                        name: 'Dietary Fiber (g)',
                        value: '5',
                    },
                    {
                        name: 'Sugars (g)',
                        value: '0',
                    },
                    {
                        name: 'Proteins (g)',
                        value: '5',
                    },
                ],
                allergicInformation: 'CONTAINS:\nWHEAT.',
            },
        },
        {
            displayName: 'Sandwich',
            name: 'Medium',
            size: '',
            isSelected: false,
            productId: 'ARBYS-WEBOA-640223381',
            price: 2,
            enhancedAttributes: {
                calories: '550',
                nutritionalFacts: [
                    {
                        name: 'Serving Weight (g)',
                        value: '170',
                    },
                    {
                        name: 'Calories',
                        value: '550',
                    },
                    {
                        name: 'Calories from Fat',
                        value: '260',
                    },
                    {
                        name: 'Fat - Total (g)',
                        value: '29',
                    },
                    {
                        name: 'Saturated Fat (g)',
                        value: '4',
                    },
                    {
                        name: 'Trans Fat (g)',
                        value: '0',
                    },
                    {
                        name: 'Cholesterol (mg)',
                        value: '0',
                    },
                    {
                        name: 'Sodium (mg)',
                        value: '1250',
                    },
                    {
                        name: 'Total Carbohydrates (g)',
                        value: '65',
                    },
                    {
                        name: 'Dietary Fiber (g)',
                        value: '6',
                    },
                    {
                        name: 'Sugars (g)',
                        value: '0',
                    },
                    {
                        name: 'Proteins (g)',
                        value: '6',
                    },
                ],
                allergicInformation: 'CONTAINS:\nWHEAT.',
            },
        },
        {
            displayName: 'Sandwich',
            name: 'Large',
            size: '',
            isSelected: false,
            productId: 'ARBYS-WEBOA-640223382',
            price: 3,
            enhancedAttributes: {
                calories: '650',
                nutritionalFacts: [
                    {
                        name: 'Serving Weight (g)',
                        value: '201',
                    },
                    {
                        name: 'Calories',
                        value: '650',
                    },
                    {
                        name: 'Calories from Fat',
                        value: '310',
                    },
                    {
                        name: 'Fat - Total (g)',
                        value: '35',
                    },
                    {
                        name: 'Saturated Fat (g)',
                        value: '5',
                    },
                    {
                        name: 'Trans Fat (g)',
                        value: '0',
                    },
                    {
                        name: 'Cholesterol (mg)',
                        value: '0',
                    },
                    {
                        name: 'Sodium (mg)',
                        value: '1480',
                    },
                    {
                        name: 'Total Carbohydrates (g)',
                        value: '77',
                    },
                    {
                        name: 'Dietary Fiber (g)',
                        value: '7',
                    },
                    {
                        name: 'Sugars (g)',
                        value: '0',
                    },
                    {
                        name: 'Proteins (g)',
                        value: '8',
                    },
                ],
                allergicInformation: 'CONTAINS:\nWHEAT.',
            },
        },
    ],
};

const side = {
    displayName: 'Salad',
    sizeSelections: [
        {
            displayName: 'Salad',
            name: 'Small',
            size: '',
            isSelected: true,
            productId: 'ARBYS-WEBOA-640223380',
            price: 1,
            enhancedAttributes: {
                calories: '33',
                nutritionalFacts: [
                    {
                        name: 'Serving Weight (g)',
                        value: '142',
                    },
                    {
                        name: 'Calories',
                        value: '33',
                    },
                    {
                        name: 'Calories from Fat',
                        value: '200',
                    },
                    {
                        name: 'Fat - Total (g)',
                        value: '22',
                    },
                    {
                        name: 'Saturated Fat (g)',
                        value: '3',
                    },
                    {
                        name: 'Trans Fat (g)',
                        value: '0',
                    },
                    {
                        name: 'Cholesterol (mg)',
                        value: '0',
                    },
                    {
                        name: 'Sodium (mg)',
                        value: '940',
                    },
                    {
                        name: 'Total Carbohydrates (g)',
                        value: '49',
                    },
                    {
                        name: 'Dietary Fiber (g)',
                        value: '5',
                    },
                    {
                        name: 'Sugars (g)',
                        value: '0',
                    },
                    {
                        name: 'Proteins (g)',
                        value: '5',
                    },
                ],
                allergicInformation: 'CONTAINS:\nWHEAT.',
            },
        },
        {
            displayName: 'Salad',
            name: 'Medium',
            size: '',
            isSelected: false,
            productId: 'ARBYS-WEBOA-640223381',
            price: 2,
            enhancedAttributes: {
                calories: '22',
                nutritionalFacts: [
                    {
                        name: 'Serving Weight (g)',
                        value: '165',
                    },
                    {
                        name: 'Calories',
                        value: '22',
                    },
                    {
                        name: 'Calories from Fat',
                        value: '260',
                    },
                    {
                        name: 'Fat - Total (g)',
                        value: '29',
                    },
                    {
                        name: 'Saturated Fat (g)',
                        value: '4',
                    },
                    {
                        name: 'Trans Fat (g)',
                        value: '0',
                    },
                    {
                        name: 'Cholesterol (mg)',
                        value: '0',
                    },
                    {
                        name: 'Sodium (mg)',
                        value: '1250',
                    },
                    {
                        name: 'Total Carbohydrates (g)',
                        value: '65',
                    },
                    {
                        name: 'Dietary Fiber (g)',
                        value: '6',
                    },
                    {
                        name: 'Sugars (g)',
                        value: '0',
                    },
                    {
                        name: 'Proteins (g)',
                        value: '6',
                    },
                ],
                allergicInformation: 'CONTAINS:\nWHEAT.',
            },
        },
        {
            name: 'Large',
            displayName: 'Salad',
            size: '',
            isSelected: false,
            productId: 'ARBYS-WEBOA-640223382',
            price: 3,
            enhancedAttributes: {
                calories: '650',
                nutritionalFacts: [
                    {
                        name: 'Serving Weight (g)',
                        value: '201',
                    },
                    {
                        name: 'Calories',
                        value: '650',
                    },
                    {
                        name: 'Calories from Fat',
                        value: '310',
                    },
                    {
                        name: 'Fat - Total (g)',
                        value: '35',
                    },
                    {
                        name: 'Saturated Fat (g)',
                        value: '5',
                    },
                    {
                        name: 'Trans Fat (g)',
                        value: '0',
                    },
                    {
                        name: 'Cholesterol (mg)',
                        value: '0',
                    },
                    {
                        name: 'Sodium (mg)',
                        value: '1480',
                    },
                    {
                        name: 'Total Carbohydrates (g)',
                        value: '77',
                    },
                    {
                        name: 'Dietary Fiber (g)',
                        value: '7',
                    },
                    {
                        name: 'Sugars (g)',
                        value: '0',
                    },
                    {
                        name: 'Proteins (g)',
                        value: '8',
                    },
                ],
                allergicInformation: 'CONTAINS:\nWHEAT.',
            },
        },
    ],
};

const rootProductWithSimilarNutritionData = {
    displayName: 'Sandwich',
    sizeSelections: [
        {
            displayName: 'Sandwich',
            name: 'Small',
            size: '',
            isSelected: true,
            productId: 'ARBYS-WEBOA-640223380',
            enhancedAttributes: rootProduct.sizeSelections[0].enhancedAttributes,
            price: 1,
        },
        {
            displayName: 'Sandwich',
            name: 'Medium',
            size: '',
            isSelected: false,
            productId: 'ARBYS-WEBOA-640223381',
            enhancedAttributes: rootProduct.sizeSelections[0].enhancedAttributes,
            price: 2,
        },
        {
            displayName: 'Sandwich',
            name: 'Large',
            size: '',
            isSelected: false,
            productId: 'ARBYS-WEBOA-640223382',
            enhancedAttributes: rootProduct.sizeSelections[0].enhancedAttributes,
            price: 3,
        },
    ],
};

export const fullProductSizesCombo = {
    rootProduct,
    childItems: [rootProduct, side],
};

export const fullProductSizesSingle = {
    rootProduct,
};

export const fullProductSizesWithSimilarNutritionData = {
    rootProduct: rootProductWithSimilarNutritionData,
};
