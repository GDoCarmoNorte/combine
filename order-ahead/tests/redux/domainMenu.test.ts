import domainMenuReducer, * as domainMenu from '../../redux/domainMenu';
import { AnyAction } from '@reduxjs/toolkit';

import menuMock from './selectors/__mocks__/menuMock.json';

describe('domainMenu', () => {
    it('should return the initial state', () => {
        expect(domainMenuReducer(undefined, {} as AnyAction)).toEqual(domainMenu.initialState);
    });

    it(`should handle ${domainMenu.actions.setDomainMenu.type}`, () => {
        const payload = menuMock;

        expect(
            domainMenuReducer(domainMenu.initialState, {
                type: domainMenu.actions.setDomainMenu.type,
                payload,
            })
        ).toEqual({ ...domainMenu.initialState, payload });
    });
});
