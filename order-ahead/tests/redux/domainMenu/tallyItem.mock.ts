export default {
    productId: 'ARBYS-WEBOA-640239548',
    quantity: 1,
    price: 0,
    childItems: [
        {
            productId: 'ARBYS-WEBOA-640223852',
            quantity: 1,
            price: 2.81,
            modifierGroups: [
                {
                    modifierGroupId: 'ARBYS-WEBOA-640214193_001',
                    modifiers: [
                        {
                            productId: 'ARBYS-WEBOA-640212761',
                            quantity: 1,
                            price: 0.49,
                        },
                    ],
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-640214194_001',
                    modifiers: [
                        {
                            productId: 'ARBYS-WEBOA-640212570',
                            quantity: 1,
                            price: 0,
                        },
                    ],
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-640214237_001',
                    modifiers: [
                        {
                            productId: 'ARBYS-WEBOA-640212325',
                            quantity: 1,
                            price: 2,
                        },
                    ],
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-640214236_001',
                    modifiers: [
                        {
                            productId: 'ARBYS-WEBOA-640212915',
                            quantity: 1,
                            price: 0,
                        },
                    ],
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-640214192_001',
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-640221053_001',
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-700000000_001',
                },
            ],
        },
        {
            productId: 'ARBYS-WEBOA-640223380',
            quantity: 1,
            price: 1.99,
            modifierGroups: [
                {
                    modifierGroupId: 'ARBYS-WEBOA-640221053_036',
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-700000000_036',
                },
            ],
        },
        {
            productId: 'ARBYS-WEBOA-640691952-SM',
            quantity: 1,
            price: 1.79,
            modifierGroups: [
                {
                    modifierGroupId: 'ARBYS-WEBOA-640232017_047',
                    modifiers: [
                        {
                            productId: 'ARBYS-WEBOA-640213030',
                            quantity: 1,
                            price: 0,
                        },
                    ],
                },
                {
                    modifierGroupId: 'ARBYS-WEBOA-640219114_047',
                    modifiers: [
                        {
                            productId: 'ARBYS-WEBOA-640691952',
                            quantity: 1,
                            price: 0,
                        },
                    ],
                },
            ],
        },
    ],
};
