import { advanceTo, clear } from 'jest-date-mock';

import bagReducer, * as BagStore from '../../redux/bag';
import { initialState } from '../../redux/bag';

import { isInRange } from '../../common/helpers/checkoutHelpers';
import { MENU_VERSION } from '../../common/constants/menuVersion';
import { TallyProductModel } from '../../@generated/webExpApi';
import { OrderLocationMethod } from '../../redux/orderLocation';

jest.mock('../../common/helpers/checkoutHelpers', () => ({
    prepareWorkingHours: () => [
        {
            day: '2020-12-28T06:00:00.000Z',
            timeRange: [
                '2020-12-28T16:15:00.000Z',
                '2020-12-28T16:30:00.000Z',
                '2020-12-28T16:45:00.000Z',
                '2020-12-28T17:00:00.000Z',
            ],
        },
    ],
    isInRange: jest.fn(),
}));

const lastEditMockData = 180222122021;

describe('bag reducer', () => {
    beforeAll(() => {
        advanceTo(new Date('2020-12-01T10:20:30Z'));
    });

    afterAll(() => {
        clear();
    });

    it('should return the initial state', () => {
        // TODO: find a better way to pass empty action without ANY type
        expect(bagReducer(undefined, {} as any)).toEqual(BagStore.initialState);
    });

    it(`should handle ${BagStore.actions.addToBag.type}`, () => {
        const bagEntry: TallyProductModel = {
            productId: '11',
            price: 111,
            quantity: 1,
            lineItemId: 1,
        };
        const payload = { bagEntry };
        expect(
            bagReducer(BagStore.initialState, {
                type: BagStore.actions.addToBag.type,
                payload,
            })
        ).toMatchObject({ LineItems: [bagEntry], isOpen: false });
    });

    it(`should handle ${BagStore.actions.updateTooltip.type}`, () => {
        const payload = { tooltipIsOpen: true, updated: true, created: false };

        expect(
            bagReducer(BagStore.initialState, {
                type: BagStore.actions.updateTooltip.type,
                payload,
            })
        ).toMatchObject({ tooltipOpen: true, entryUpdated: true, entryCreated: false });
    });

    it(`should handle ${BagStore.actions.addDealToBag.type}`, () => {
        const payload = { id: 'id' };

        expect(
            bagReducer(BagStore.initialState, {
                type: BagStore.actions.addDealToBag.type,
                payload,
            })
        ).toMatchObject({ ...BagStore.initialState, dealId: 'id' });
    });

    it(`should handle ${BagStore.actions.removeDealFromBag.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: 'id',
            lastRemovedLineItemId: null,
            lastEdit: null,
        };

        expect(
            bagReducer(initialState, {
                type: BagStore.actions.removeDealFromBag.type,
            })
        ).toMatchObject({ ...initialState, dealId: null });
    });

    it(`should handle ${BagStore.actions.removeFromBag.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = { lineItemId: 1 };

        expect(
            bagReducer(initialState, {
                type: BagStore.actions.removeFromBag.type,
                payload,
            })
        ).toMatchObject({ LineItems: [], isOpen: false, lastRemovedLineItemId: 1 });
    });

    it(`should handle ${BagStore.actions.clearLastRemovedLineItemId.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: 5,
            lastEdit: null,
        };

        expect(
            bagReducer(initialState, {
                type: BagStore.actions.clearLastRemovedLineItemId.type,
            })
        ).toMatchObject({ lastRemovedLineItemId: null });
    });

    it(`should handle ${BagStore.actions.removeAllFromBag.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: '12345',
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = { lineItemIdList: [1] };

        expect(
            bagReducer(initialState, {
                type: BagStore.actions.removeAllFromBag.type,
                payload,
            })
        ).toMatchObject({});
    });

    it(`should handle ${BagStore.actions.markAsRemoved.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = { lineItemId: 1 };

        expect(
            bagReducer(initialState, {
                type: BagStore.actions.markAsRemoved.type,
                payload,
            })
        ).toMatchObject({ LineItems: initialState.LineItems, markedAsRemoved: [payload.lineItemId], isOpen: false });
    });

    it(`should handle ${BagStore.actions.markAsRemoved.type} and restore item when marked again`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [1],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = { lineItemId: 1 };

        expect(
            bagReducer(initialState, {
                type: BagStore.actions.markAsRemoved.type,
                payload,
            })
        ).toMatchObject({ LineItems: initialState.LineItems, markedAsRemoved: [], isOpen: false });
    });

    it(`should handle ${BagStore.actions.clearBag.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        expect(
            bagReducer(initialState, {
                type: BagStore.actions.clearBag.type,
            })
        ).toMatchObject({ LineItems: [], isOpen: false });
    });

    it(`should handle ${BagStore.actions.updateBagItem.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = { bagEntryIndex: 0, value: { lineItemId: 1, productId: '111', price: 111, quantity: 1 } };

        const updatedState = bagReducer(initialState, {
            type: BagStore.actions.updateBagItem.type,
            payload,
        });

        expect(updatedState.LineItems[payload.bagEntryIndex]).toEqual(payload.value);
    });

    it(`should handle ${BagStore.actions.updateBagItemCount.type}`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                    modifierGroups: [
                        {
                            productId: '11111',
                            modifiers: [],
                        },
                    ],
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = {
            bagEntryIndex: 0,
            value: 2,
        };

        const updatedState = bagReducer(initialState, {
            type: BagStore.actions.updateBagItemCount.type,
            payload,
        });

        const expectedQuantity = payload.value + initialState.LineItems[payload.bagEntryIndex].quantity; // 3

        expect(updatedState.LineItems[payload.bagEntryIndex].modifierGroups).toHaveLength(1);
        expect(updatedState.LineItems[payload.bagEntryIndex].quantity).toEqual(expectedQuantity);
    });

    it(`should handle ${BagStore.actions.initPickupTimeValues.type} when no pickupTime was set previously`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                    modifierGroups: [
                        {
                            productId: '11111',
                            modifiers: [],
                        },
                    ],
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = {
            openedTimeRanges: [
                {
                    start: '2020-12-29T05:45:00.000Z',
                    end: '2020-12-29T08:45:00.000Z',
                },
            ],
        };

        const updatedState = bagReducer(initialState, {
            type: BagStore.actions.initPickupTimeValues.type,
            payload,
        });

        expect(updatedState).toMatchObject({
            ...initialState,
            pickupTimeValues: [
                {
                    day: '2020-12-28T06:00:00.000Z',
                    timeRange: [
                        '2020-12-28T16:15:00.000Z',
                        '2020-12-28T16:30:00.000Z',
                        '2020-12-28T16:45:00.000Z',
                        '2020-12-28T17:00:00.000Z',
                    ],
                },
            ],
            version: MENU_VERSION,
        });
    });

    it(`should handle ${BagStore.actions.initPickupTimeValues.type} when pickupTime was set previously and correct`, () => {
        (isInRange as jest.Mock).mockReturnValueOnce(true);

        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                    modifierGroups: [
                        {
                            productId: '11111',
                            modifiers: [],
                        },
                    ],
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: '2020-12-29T06:45:00.000Z',
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = {
            openedTimeRanges: [
                {
                    start: '2020-12-29T05:45:00.000Z',
                    end: '2020-12-29T08:45:00.000Z',
                },
            ],
        };

        const updatedState = bagReducer(initialState, {
            type: BagStore.actions.initPickupTimeValues.type,
            payload,
        });

        expect(updatedState).toMatchObject({
            ...initialState,
            pickupTime: '2020-12-29T06:45:00.000Z',
            pickupTimeType: 'future',
            pickupTimeValues: [
                {
                    day: '2020-12-28T06:00:00.000Z',
                    timeRange: [
                        '2020-12-28T16:15:00.000Z',
                        '2020-12-28T16:30:00.000Z',
                        '2020-12-28T16:45:00.000Z',
                        '2020-12-28T17:00:00.000Z',
                    ],
                },
            ],
        });
    });

    it(`should handle ${BagStore.actions.initPickupTimeValues.type} when pickupTime was set previously and not in valid range`, () => {
        (isInRange as jest.Mock).mockReturnValueOnce(false);

        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                    modifierGroups: [
                        {
                            productId: '11111',
                            modifiers: [],
                        },
                    ],
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: '2020-12-28T15:15:00.000Z',
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = {
            openedTimeRanges: [
                {
                    start: '2020-12-29T05:45:00.000Z',
                    end: '2020-12-29T08:45:00.000Z',
                },
            ],
        };

        const updatedState = bagReducer(initialState, {
            type: BagStore.actions.initPickupTimeValues.type,
            payload,
        });

        expect(updatedState).toMatchObject({
            ...initialState,
            pickupTime: null,
            pickupTimeType: 'asap',
            pickupTimeValues: [
                {
                    day: '2020-12-28T06:00:00.000Z',
                    timeRange: [
                        '2020-12-28T16:15:00.000Z',
                        '2020-12-28T16:30:00.000Z',
                        '2020-12-28T16:45:00.000Z',
                        '2020-12-28T17:00:00.000Z',
                    ],
                },
            ],
        });
    });
    it(`should handle ${BagStore.actions.setPickupTimeValues.type} and update timeslots`, () => {
        const initialState: BagStore.BagState = {
            LineItems: [
                {
                    lineItemId: 1,
                    productId: '11',
                    price: 111,
                    quantity: 1,
                    modifierGroups: [
                        {
                            productId: '11111',
                            modifiers: [],
                        },
                    ],
                },
            ],
            markedAsRemoved: [],
            isOpen: false,
            tooltipOpen: false,
            entryCreated: false,
            entryCreatedWithDeal: false,
            entryUpdatedWithDeal: false,
            entryUpdated: false,
            pickupTime: null,
            pickupTimeType: 'asap',
            deliveryTime: null,
            deliveryTimeType: 'asap',
            pickupTimeValues: null,
            version: MENU_VERSION,
            dealId: null,
            lastRemovedLineItemId: null,
            lastEdit: null,
        };
        const payload = [
            {
                day: '2020-12-28T06:00:00.000Z',
                timeRange: [
                    '2020-12-28T16:15:00.000Z',
                    '2020-12-28T16:30:00.000Z',
                    '2020-12-28T16:45:00.000Z',
                    '2020-12-28T17:00:00.000Z',
                ],
            },
        ];

        const updatedState = bagReducer(initialState, {
            type: BagStore.actions.setPickupTimeValues.type,
            payload,
        });

        expect(updatedState).toMatchObject({
            ...initialState,
            pickupTimeValues: [
                {
                    day: '2020-12-28T06:00:00.000Z',
                    timeRange: [
                        '2020-12-28T16:15:00.000Z',
                        '2020-12-28T16:30:00.000Z',
                        '2020-12-28T16:45:00.000Z',
                        '2020-12-28T17:00:00.000Z',
                    ],
                },
            ],
            version: MENU_VERSION,
        });
    });
    it(`should handle ${BagStore.actions.setPickupTime.type} asap pickup`, () => {
        const payload: BagStore.SetPickupTimePayload = {
            asap: true,
            method: OrderLocationMethod.PICKUP,
        };
        const updatedState = bagReducer(BagStore.initialState, {
            type: BagStore.actions.setPickupTime.type,
            payload,
        });

        expect(updatedState.pickupTime).toEqual(BagStore.initialState.pickupTime);
        expect(updatedState.pickupTimeType).toBe('asap');
    });
    it(`should handle ${BagStore.actions.setPickupTime.type} future pickup`, () => {
        const payload: BagStore.SetPickupTimePayload = {
            asap: false,
            time: '2020-12-28T16:15:00.000Z',
            method: OrderLocationMethod.PICKUP,
        };
        const updatedState = bagReducer(BagStore.initialState, {
            type: BagStore.actions.setPickupTime.type,
            payload,
        });

        expect(updatedState.pickupTime).toEqual(payload.time);
        expect(updatedState.pickupTimeType).toBe('future');
    });

    it(`should handle ${BagStore.actions.setPickupTime.type} asap delivery`, () => {
        const payload: BagStore.SetPickupTimePayload = {
            asap: true,
            method: OrderLocationMethod.DELIVERY,
        };
        const updatedState = bagReducer(BagStore.initialState, {
            type: BagStore.actions.setPickupTime.type,
            payload,
        });

        expect(updatedState.deliveryTime).toEqual(BagStore.initialState.deliveryTime);
        expect(updatedState.deliveryTimeType).toBe('asap');
    });
    it(`should handle ${BagStore.actions.setPickupTime.type} future delivery`, () => {
        const payload: BagStore.SetPickupTimePayload = {
            asap: false,
            time: '2020-12-28T16:15:00.000Z',
            method: OrderLocationMethod.DELIVERY,
        };
        const updatedState = bagReducer(BagStore.initialState, {
            type: BagStore.actions.setPickupTime.type,
            payload,
        });

        expect(updatedState.deliveryTime).toEqual(payload.time);
        expect(updatedState.deliveryTimeType).toBe('future');
    });

    it(`should handle ${BagStore.actions.resetPickupTime.type}`, () => {
        const deliveryTimeMock = '2020-12-25T13:10:00.000Z';
        const deliveryTimeTypeMock = 'future';
        const currentState: BagStore.BagState = {
            ...BagStore.initialState,
            pickupTime: '2020-12-28T16:15:00.000Z',
            pickupTimeType: 'future',
            deliveryTime: deliveryTimeMock,
            deliveryTimeType: deliveryTimeTypeMock,
            pickupTimeValues: [
                {
                    day: '2020-12-28T06:00:00.000Z',
                    timeRange: ['2020-12-28T16:45:00.000Z', '2020-12-28T17:00:00.000Z'],
                },
            ],
        };

        const updatedState = bagReducer(currentState, { type: BagStore.actions.resetPickupTime.type });

        expect(updatedState.pickupTime).toEqual(BagStore.initialState.pickupTime);
        expect(updatedState.pickupTimeType).toEqual(BagStore.initialState.pickupTimeType);
        expect(updatedState.deliveryTime).toEqual(deliveryTimeMock);
        expect(updatedState.deliveryTimeType).toEqual(deliveryTimeTypeMock);
        expect(updatedState.pickupTimeValues).toEqual(BagStore.initialState.pickupTimeValues);
    });

    it(`should handle ${BagStore.actions.resetDeliveryTime.type}`, () => {
        const pickupTimeValuesMock = [
            {
                day: '2020-12-28T06:00:00.000Z',
                timeRange: [
                    '2020-12-28T16:15:00.000Z',
                    '2020-12-28T16:30:00.000Z',
                    '2020-12-28T16:45:00.000Z',
                    '2020-12-28T17:00:00.000Z',
                ],
            },
        ];
        const pickupTimeMock = '2020-12-28T16:15:00.000Z';
        const pickupTimeTypeMock = 'future';
        const currentState: BagStore.BagState = {
            ...BagStore.initialState,
            pickupTime: pickupTimeMock,
            pickupTimeType: pickupTimeTypeMock,
            deliveryTime: '2020-12-25T13:10:00.000Z',
            deliveryTimeType: 'future',
            pickupTimeValues: pickupTimeValuesMock,
        };

        const updatedState = bagReducer(currentState, { type: BagStore.actions.resetDeliveryTime.type });

        expect(updatedState.pickupTime).toEqual(pickupTimeMock);
        expect(updatedState.pickupTimeType).toEqual(pickupTimeTypeMock);
        expect(updatedState.deliveryTime).toEqual(BagStore.initialState.deliveryTime);
        expect(updatedState.deliveryTimeType).toEqual(BagStore.initialState.deliveryTimeType);
        expect(updatedState.pickupTimeValues).toEqual(pickupTimeValuesMock);
    });

    it(`should handle ${BagStore.actions.resetOrderTime.type}`, () => {
        const currentState: BagStore.BagState = {
            ...BagStore.initialState,
            pickupTime: '2020-12-28T16:15:00.000Z',
            pickupTimeType: 'future',
            pickupTimeValues: [
                {
                    day: '2020-12-28T06:00:00.000Z',
                    timeRange: [
                        '2020-12-28T16:15:00.000Z',
                        '2020-12-28T16:30:00.000Z',
                        '2020-12-28T16:45:00.000Z',
                        '2020-12-28T17:00:00.000Z',
                    ],
                },
            ],
        };

        const updatedState = bagReducer(currentState, { type: BagStore.actions.resetOrderTime.type });

        expect(updatedState.pickupTime).toEqual(BagStore.initialState.pickupTime);
        expect(updatedState.pickupTimeType).toEqual(BagStore.initialState.pickupTimeType);
        expect(updatedState.deliveryTime).toEqual(BagStore.initialState.deliveryTime);
        expect(updatedState.deliveryTimeType).toEqual(BagStore.initialState.deliveryTimeType);
        expect(updatedState.pickupTimeValues).toEqual(BagStore.initialState.pickupTimeValues);
    });

    it(`should handle ${BagStore.actions.setLastEdit.type}`, () => {
        const currentState: BagStore.BagState = {
            ...BagStore.initialState,
        };

        const updatedState = bagReducer(currentState, {
            type: BagStore.actions.setLastEdit.type,
            payload: lastEditMockData,
        });

        expect(updatedState.lastEdit).toEqual(lastEditMockData);
    });

    it(`should handle ${BagStore.actions.clearLastEdit.type}`, () => {
        const currentState: BagStore.BagState = {
            ...BagStore.initialState,
            lastEdit: lastEditMockData,
        };

        const updatedState = bagReducer(currentState, {
            type: BagStore.actions.clearLastEdit.type,
        });

        expect(updatedState.lastEdit).toEqual(null);
    });
});

describe('bag version:', () => {
    it('version in redux state should be equal version from environment variable', () => {
        const { version } = initialState;

        expect(version).toEqual(MENU_VERSION);
    });
});
