import { clear } from 'jest-date-mock';

import submitOrderReducer, * as SubmitOrderStore from '../../redux/submitOrder';

describe('submitOrder reducer', () => {
    afterAll(() => {
        clear();
    });

    it('should return the initial state', () => {
        expect(submitOrderReducer(undefined, {} as any)).toEqual(SubmitOrderStore.initialState);
    });

    it(`should handle ${SubmitOrderStore.actions.pending.type}`, () => {
        const initialState: SubmitOrderStore.ISubmitOrderState = {
            lastOrder: null,
            lastRequest: null,
            isLoading: false,
            error: null,
            unavailableItems: null,
            isShowAlertModal: false,
        };

        expect(
            submitOrderReducer(initialState, {
                type: SubmitOrderStore.actions.pending.type,
            })
        ).toMatchObject({ isLoading: true });
    });

    it(`should handle ${SubmitOrderStore.actions.hideAlertModal.type}`, () => {
        const initialState: SubmitOrderStore.ISubmitOrderState = {
            lastOrder: null,
            lastRequest: null,
            isLoading: false,
            error: null,
            unavailableItems: null,
            isShowAlertModal: true,
        };

        expect(
            submitOrderReducer(initialState, {
                type: SubmitOrderStore.actions.hideAlertModal.type,
            })
        ).toMatchObject({ isShowAlertModal: false });
    });

    it(`should handle ${SubmitOrderStore.actions.rejected.type}`, () => {
        const initialState: SubmitOrderStore.ISubmitOrderState = {
            lastOrder: null,
            lastRequest: null,
            isLoading: false,
            error: null,
            unavailableItems: null,
            isShowAlertModal: true,
        };

        expect(
            submitOrderReducer(initialState, {
                type: SubmitOrderStore.actions.rejected.type,
                payload: 'error',
            })
        ).toMatchObject({ error: 'error' });
    });

    it(`should handle ${SubmitOrderStore.actions.fulfilled.type}`, () => {
        const initialState: SubmitOrderStore.ISubmitOrderState = {
            lastOrder: null,
            lastRequest: null,
            isLoading: false,
            error: null,
            unavailableItems: null,
            isShowAlertModal: true,
        };

        expect(
            submitOrderReducer(initialState, {
                type: SubmitOrderStore.actions.fulfilled.type,
                payload: {
                    response: 'response',
                    request: 'request',
                },
            })
        ).toMatchObject({ lastOrder: 'response', lastRequest: 'request' });
    });

    it(`should handle ${SubmitOrderStore.actions.setUnavailableItems.type}`, () => {
        const initialState: SubmitOrderStore.ISubmitOrderState = {
            lastOrder: null,
            lastRequest: null,
            isLoading: false,
            error: null,
            unavailableItems: null,
            isShowAlertModal: true,
        };

        expect(
            submitOrderReducer(initialState, {
                type: SubmitOrderStore.actions.setUnavailableItems.type,
                payload: ['IDPSalesItem-6345'],
            })
        ).toMatchObject({ unavailableItems: ['IDPSalesItem-6345'] });
    });

    it(`should handle ${SubmitOrderStore.actions.reset.type}`, () => {
        const initialState: SubmitOrderStore.ISubmitOrderState = {
            lastOrder: null,
            lastRequest: null,
            isLoading: true,
            error: null,
            unavailableItems: ['IDPSalesItem-6345'],
            isShowAlertModal: true,
        };

        expect(
            submitOrderReducer(initialState, {
                type: SubmitOrderStore.actions.reset.type,
                payload: ['IDPSalesItem-6345'],
            })
        ).toMatchObject({ unavailableItems: ['IDPSalesItem-6345'], isLoading: false });
    });
});
