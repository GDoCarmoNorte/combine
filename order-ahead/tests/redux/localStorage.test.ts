import { loadState } from '../../redux/localStorage';
import { MENU_VERSION } from '../../common/constants/menuVersion';

describe('localStorage redux utils', () => {
    describe('loadState', () => {
        let initialState = {};
        beforeEach(() => {
            initialState = {
                test2: 'cde',
            };
        });

        it('should merge states correctly without losing datat', () => {
            (localStorage.getItem as jest.Mock).mockReturnValue('{ "test": "abc" }');

            const state = loadState(initialState);

            expect(state).toEqual({
                test: 'abc',
                test2: 'cde',
            });
        });

        it('should merge states without modifying intiialState', () => {
            (localStorage.getItem as jest.Mock).mockReturnValue('{ "test": "abc" }');

            const state = loadState(initialState);

            expect(state).not.toEqual(initialState);
        });

        it('LineItems must be an empty array if the menu version from config is newer than in local storage.', () => {
            const mock = JSON.stringify({
                bag: {
                    LineItems: ['product', 'product'],
                    version: 0,
                },
            });

            (localStorage.getItem as jest.Mock).mockReturnValue(mock);

            const { bag } = loadState({});

            expect(bag.LineItems.length).toEqual(0);
            expect(bag.LineItems).toEqual([]);
        });

        it('LineItems should not be changed if the menu version from config is no newer than in local storage.', () => {
            const mock = {
                bag: {
                    LineItems: ['product', 'product'],
                    version: MENU_VERSION,
                },
            };

            (localStorage.getItem as jest.Mock).mockReturnValue(JSON.stringify(mock));

            const { bag } = loadState({});

            expect(bag.LineItems.length).toEqual(mock.bag.LineItems.length);
            expect(bag.LineItems).toEqual(mock.bag.LineItems);
        });

        it('LineItems should not be changed if version form  local storage equal undefined.', () => {
            const mock = {
                bag: {
                    LineItems: ['product', 'product'],
                },
            };

            (localStorage.getItem as jest.Mock).mockReturnValue(JSON.stringify(mock));

            const { bag } = loadState({});

            expect(bag.LineItems.length).toEqual(mock.bag.LineItems.length);
            expect(bag.LineItems).toEqual(mock.bag.LineItems);
        });

        it('version from local storage should defined as version from config if local storage version equal unexpected value', () => {
            const unexpectedValues = [{}, [], NaN, true, false, null, 'version'];

            unexpectedValues.forEach((item) => {
                const mock = {
                    bag: {
                        LineItems: ['product', 'product'],
                        version: item,
                    },
                };

                (localStorage.getItem as jest.Mock).mockReturnValue(JSON.stringify(mock));

                const { bag } = loadState({});

                expect(bag.version).toEqual(MENU_VERSION);
            });
        });

        it('should load state bases on locale', () => {
            jest.spyOn(localStorage, 'getItem');
            const LOCALE = process.env.LOCALE;

            process.env.LOCALE = 'en-us';
            loadState(initialState);
            expect(localStorage.getItem).toHaveBeenCalledWith('state');

            process.env.LOCALE = 'en-ca';
            loadState(initialState);
            expect(localStorage.getItem).toHaveBeenCalledWith('state_CA');

            process.env.LOCALE = LOCALE;
        });
    });
});
