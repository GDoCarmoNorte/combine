import { renderHook } from '@testing-library/react-hooks';
import { orderHistoryItemMock, orderHistoryItemMockNotFulfilled } from '../../mocks/orderHistoryItem.mocks';
import UseOrderHistory from '../../../redux/hooks/useOrderHistory';
import { useDispatch, useSelector } from 'react-redux';

jest.mock('react-redux');

describe('UseOrderHistory hooks', () => {
    const orderHistoryStateMock = {
        orderHistory: [orderHistoryItemMock, orderHistoryItemMock, orderHistoryItemMockNotFulfilled],
        loading: false,
    };

    const orderHistoryFulfilledStateMock = [orderHistoryItemMock, orderHistoryItemMock];
    beforeEach(() => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
    });
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should return orderHistory with "FULFILLED" status', () => {
        (useSelector as jest.Mock).mockReturnValue(orderHistoryStateMock);

        const {
            result: {
                current: { orderHistoryFulfilled },
            },
        } = renderHook(() => UseOrderHistory());

        expect(orderHistoryFulfilled).toEqual(orderHistoryFulfilledStateMock);
    });
});
