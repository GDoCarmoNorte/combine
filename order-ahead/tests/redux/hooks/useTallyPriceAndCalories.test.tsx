import React from 'react';
import { mount } from 'enzyme';
import { useTallyPriceAndCalories } from '../../../redux/hooks/domainMenu';
import { standaloneTalyItemMock } from '../selectors/__mocks__/tallyItem.mock';

import menu from '../selectors/__mocks__/menuMock.json';
import { useAppSelector } from '../../../redux/store';
import { selectTallyPriceAndCalories } from '../../../redux/selectors/tally';

jest.mock('react-redux');
jest.mock('../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));

const getTallyPriceAndCaloriesHookSelector = () => {
    let tallyPriceAndCaloriesSelector;
    const Component = () => {
        tallyPriceAndCaloriesSelector = useTallyPriceAndCalories(standaloneTalyItemMock, true);
        return null;
    };
    mount(<Component />);

    return tallyPriceAndCaloriesSelector;
};

describe('useTallyPriceAndCalories', () => {
    it('should return price and calories', () => {
        const mockState = { domainMenu: { payload: menu } } as any;
        (useAppSelector as jest.Mock).mockReturnValueOnce(
            selectTallyPriceAndCalories(mockState, standaloneTalyItemMock)
        );
        const result = getTallyPriceAndCaloriesHookSelector();

        expect(result).toEqual({
            calories: 360,
            price: 3.49,
            totalPrice: 3.49,
        });
    });
});
