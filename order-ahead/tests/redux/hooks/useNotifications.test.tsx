import React from 'react';
import { useDispatch } from 'react-redux';
import { render } from '@testing-library/react';

import useNotifications, { IUseNotifications } from '../../../redux/hooks/useNotifications';

import * as NotificationsStore from '../../../redux/notifications';

jest.mock('react-redux');

const renderNotificationsHook = () => {
    let notification: IUseNotifications;

    const Component = () => {
        notification = useNotifications();

        return null;
    };

    render(<Component />);

    return notification;
};

describe('useNotifications', () => {
    beforeEach(() => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
    });
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should call enqueue action after calling enqueueSuccess', () => {
        jest.spyOn(NotificationsStore.actions, 'enqueue');
        const { actions } = renderNotificationsHook();
        actions.enqueueSuccess({});

        expect(NotificationsStore.actions.enqueue).toHaveBeenCalledTimes(1);
    });

    it('should call dequeue action', () => {
        jest.spyOn(NotificationsStore.actions, 'dequeue');
        const { actions } = renderNotificationsHook();
        actions.dequeue();

        expect(NotificationsStore.actions.dequeue).toHaveBeenCalledTimes(1);
    });
});
