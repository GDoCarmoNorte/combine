import { renderHook } from '@testing-library/react-hooks';
import { usePersonalization, usePersonalizationFactsChangeListener } from '../../../redux/hooks/usePersonalization';
import { useAppSelector, useAppDispatch } from '../../../redux/store';
import * as PersonalizationStore from '../../../redux/personalization';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';
import { personalizationStateMock, actionParametersMock } from '../__mocks__/personalization.mock';

jest.mock('../../../redux/store', () => ({
    useAppSelector: jest.fn(),
    useAppDispatch: jest.fn().mockReturnValue(jest.fn()),
}));

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

describe('Personalization hooks', () => {
    describe('usePersonalization', () => {
        it('should return personalized section', () => {
            (useAppSelector as jest.Mock).mockReturnValue(personalizationStateMock);
            (useGlobalProps as jest.Mock).mockReturnValue({ actionParameters: actionParametersMock });

            const {
                result: {
                    current: {
                        actions: { getPersonalizedSection },
                    },
                },
            } = renderHook(() => usePersonalization());

            expect(getPersonalizedSection('HPMainHero')).toEqual(actionParametersMock.items[0]);
        });

        it('should return undefined', () => {
            (useAppSelector as jest.Mock).mockReturnValue(personalizationStateMock);
            (useGlobalProps as jest.Mock).mockReturnValue({ actionParameters: undefined });

            const {
                result: {
                    current: {
                        actions: { getPersonalizedSection },
                    },
                },
            } = renderHook(() => usePersonalization());

            expect(getPersonalizedSection('HPMainHero')).toEqual(undefined);
        });
    });

    describe('usePersonalizationFactsChangeListener', () => {
        it('deps not initialized case', () => {
            (useAppSelector as jest.Mock).mockReturnValueOnce([{}]);
            (useAppSelector as jest.Mock).mockReturnValueOnce(false);

            const dispatch = useAppDispatch();
            renderHook(() => usePersonalizationFactsChangeListener());

            expect(dispatch).lastCalledWith({
                payload: true,
                type: 'personalization/setLoading',
            });
        });

        it('facts changed case', () => {
            (useAppSelector as jest.Mock).mockReturnValueOnce([{}]);
            (useAppSelector as jest.Mock).mockReturnValueOnce(true);

            const dispatch = useAppDispatch();
            jest.spyOn(PersonalizationStore.actions, 'getActionsMap').mockImplementation(
                (c) =>
                    ({
                        type: 'getActionMapThunk',
                        payload: c,
                    } as any)
            );

            renderHook(() => usePersonalizationFactsChangeListener());

            expect(dispatch).lastCalledWith({
                payload: [{}],
                type: 'getActionMapThunk',
            });
        });
    });
});
