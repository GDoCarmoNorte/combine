import React from 'react';
import { mount } from 'enzyme';
import UseSelectedSell, { UseSelectedSellHook } from '../../../redux/hooks/useSelectedSell';
import fetchRecommendedProductsApi from '../../../common/services/recommendationService';

const selectedSellResponseMock = {
    id: '1234',
    productIds: 'product1, product2, product3',
};

const selectedSellRejectMock = {
    id: '1234',
    productIds: [],
};

const selectedSellRequestMock = {
    correlationId: '1234',
    locationId: '30199',
    productIds: ['product1', 'product2', 'product2'],
};

jest.mock('react-redux');
jest.mock('../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));
jest.mock('../../../common/services/recommendationService');
jest.mock('../../../common/services/addJsonError');

const useSelectedSellHook = () => {
    let selectedSellHook: UseSelectedSellHook;
    const Component = () => {
        selectedSellHook = UseSelectedSell();
        return null;
    };
    mount(<Component />);

    return selectedSellHook;
};

describe('useSelectedSell', () => {
    beforeEach(() => {
        jest.resetModules();
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('should call fetch recommended items with a customerType', () => {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'customerType=PC',
        });
        (fetchRecommendedProductsApi as jest.Mock).mockReturnValue(Promise.resolve(selectedSellResponseMock));
        const selectedSell = useSelectedSellHook();

        selectedSell.actions.fetchRecommendedProducts(selectedSellRequestMock);
        expect(fetchRecommendedProductsApi).toBeCalledWith({ customerType: 'PC', ...selectedSellRequestMock });
    });

    it('should not call fetch recommended items without customerType', () => {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'customerType=',
        });
        (fetchRecommendedProductsApi as jest.Mock).mockReturnValue(Promise.resolve(selectedSellResponseMock));
        const selectedSell = useSelectedSellHook();

        selectedSell.actions.fetchRecommendedProducts(selectedSellRequestMock);
        expect(fetchRecommendedProductsApi).not.toBeCalled();
    });

    it('should return an id and empty product array if promise rejects', async () => {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'customerType=PC',
        });
        (fetchRecommendedProductsApi as jest.Mock).mockReturnValue(Promise.reject(selectedSellRejectMock));

        const selectedSell = useSelectedSellHook();

        selectedSell.actions.fetchRecommendedProducts(selectedSellRequestMock);
        expect(fetchRecommendedProductsApi).toBeCalledWith({ customerType: 'PC', ...selectedSellRequestMock });

        await expect(fetchRecommendedProductsApi).rejects.toEqual(selectedSellRejectMock);
    });
});
