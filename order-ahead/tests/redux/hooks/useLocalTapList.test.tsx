import { renderHook } from '@testing-library/react-hooks';
import useLocalTapList from '../../../redux/hooks/useLocalTapList';
import { useAppSelector, useAppDispatch } from '../../../redux/store';

jest.mock('../../../redux/store', () => ({
    useAppSelector: jest.fn(),
    useAppDispatch: jest.fn().mockReturnValue(jest.fn()),
}));

describe('useLocalTapList', () => {
    const mockData: any = {
        beersByType: {
            DRAFT: {
                count: 1,
                items: [
                    {
                        name: 'draft beer',
                    },
                ],
            },
        },
    };

    it('should return correct data', () => {
        (useAppSelector as any).mockReturnValue({ payload: mockData, loading: false, error: false });

        const {
            result: {
                current: { available, list },
            },
        } = renderHook(() => useLocalTapList());

        expect(available).toEqual(true);
        expect(list).toEqual(mockData.beersByType);
    });

    it('should return correct availability status', () => {
        (useAppSelector as any).mockReturnValue({ payload: null, loading: false, error: false });
        expect(renderHook(() => useLocalTapList()).result.current.available).toEqual(false);

        (useAppSelector as any).mockReturnValue({ payload: {}, loading: false, error: false });
        expect(renderHook(() => useLocalTapList()).result.current.available).toEqual(false);

        (useAppSelector as any).mockReturnValue({ payload: { beersByType: {} }, loading: false, error: false });
        expect(renderHook(() => useLocalTapList()).result.current.available).toEqual(false);

        (useAppSelector as any).mockReturnValue({
            payload: { beersByType: { DRAFT: { count: 0 } } },
            loading: false,
            error: false,
        });
        expect(renderHook(() => useLocalTapList()).result.current.available).toEqual(false);

        (useAppSelector as any).mockReturnValue({ payload: mockData, loading: false, error: false });
        expect(renderHook(() => useLocalTapList()).result.current.available).toEqual(true);
    });

    it('should dispath actions', () => {
        const dispatch = useAppDispatch();
        const {
            result: {
                current: {
                    actions: { setLocalTapList, getLocalTapList },
                },
            },
        } = renderHook(() => useLocalTapList());

        setLocalTapList({} as any);
        getLocalTapList('locationId');

        expect(dispatch).toBeCalledTimes(2);
    });
});
