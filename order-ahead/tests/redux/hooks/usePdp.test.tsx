import React from 'react';
import { useDispatch } from 'react-redux';
import { mount } from 'enzyme';

import usePdp, { UsePDPHook } from '../../../redux/hooks/usePdp';
import { hasUnsavedModifications } from '../../../lib/tallyItem';
import tallyItemMock from '../../mocks/tallyItem.mock';
import useBag from '../../../redux/hooks/useBag';
import { useAppSelector } from '../../../redux/store';
import { useDefaultModifiers } from '../../../redux/hooks/domainMenu';

jest.mock('react-redux');
jest.mock('../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));

jest.mock('../../../lib/tallyItem');
jest.mock('../../../redux/hooks/useBag');
jest.mock('../../../redux/hooks/domainMenu');

const renderPdpHook = () => {
    let pdp: UsePDPHook;
    const Component = () => {
        pdp = usePdp();

        return null;
    };
    mount(<Component />);

    return pdp;
};

describe('usePdp', () => {
    describe('useHasUnsavedModifications', () => {
        beforeEach(() => {
            (useBag as jest.Mock).mockReturnValue({});
            (useDefaultModifiers as jest.Mock).mockReturnValue([]);
        });
        afterEach(() => {
            jest.resetAllMocks();
        });

        it('should compare default item and current item', () => {
            (useAppSelector as jest.Mock)
                .mockReturnValueOnce({})
                .mockReturnValueOnce(tallyItemMock)
                .mockReturnValueOnce(tallyItemMock);
            (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
            (hasUnsavedModifications as jest.Mock).mockReturnValue(false);
            const { useHasUnsavedModifications } = renderPdpHook();
            const result = useHasUnsavedModifications();
            expect(result).toBe(false);
            expect(hasUnsavedModifications).toHaveBeenCalledWith(tallyItemMock, tallyItemMock);
        });

        it('should compare item from bag and current item if item is updating', () => {
            const currentTallyItemMock = {
                ...tallyItemMock,
                lineItemId: 1,
            };
            (useAppSelector as jest.Mock).mockReturnValueOnce({}).mockReturnValueOnce(currentTallyItemMock);
            (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
            (hasUnsavedModifications as jest.Mock).mockReturnValue(false);
            (useBag as jest.Mock).mockReturnValueOnce({ bagEntries: [currentTallyItemMock] });
            const { useHasUnsavedModifications } = renderPdpHook();
            const result = useHasUnsavedModifications();
            expect(result).toBe(false);
            expect(hasUnsavedModifications).toHaveBeenCalledWith(currentTallyItemMock, currentTallyItemMock);
        });
    });
});
