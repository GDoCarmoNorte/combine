import { useDineInOrder } from '../../../redux/hooks/useDineInOrder';

import { act, renderHook } from '@testing-library/react-hooks';
import { waitFor } from '@testing-library/dom';
import { getDineInOrderDetails } from '../../../common/services/orderService/dineInService';
import { IDineInOrderDetailsModel } from '../../../@generated/webExpApi';

const mockedRequest = { locationId: '13', orderId: '9374560' };

const mockOrder = {
    posOrderId: '111275-20005-09302021-chf284',
    isOpen: true,
    dateTime: '2021-09-30T07:13:07.465510Z',
    table: '123',
    serverName: 'Mark',
    guestsCount: 0,
    subTotalBeforeDiscounts: 10.65,
    subTotalAfterDiscounts: 10.65,
    tax: 0.76,
    total: 11.41,
    tip: 0,
    otherDiscount: 0,
    certificateDiscount: 0,
    isLoyaltyMember: false,
    products: [
        {
            description: 'L 6 BONELESS',
            price: 10,
            quantity: 1,
        },
    ],
    location: {
        contactDetails: {
            address: {
                line1: '5500 Wayzata Blvd.',
                line2: 'Ste 13',
                line3: 'Some Mall',
                postalCode: '55416',
                stateProvinceCode: 'MN',
                countryCode: 'US',
            },
            phone: '612-866-9316',
        },
        displayName: 'Buzztime QA 6.7',
        id: '13',
    },
    payments: [],
} as IDineInOrderDetailsModel;

jest.mock('../../../common/services/orderService/dineInService', () => ({
    getDineInOrderDetails: jest.fn().mockImplementation(() => Promise.resolve(mockOrder)),
}));

describe('useDineInOrder', () => {
    it('should handle loading state', async () => {
        const { result } = renderHook(() => useDineInOrder());

        act(() => {
            result.current.getOrder(mockedRequest);
        });

        await waitFor(() => expect(result.current.loading).toEqual(true));

        await waitFor(() => expect(result.current.loading).toEqual(false));
    });

    it('should set orderlocationId', async () => {
        const { result } = renderHook(() => useDineInOrder());

        act(() => {
            result.current.getOrder(mockedRequest);
        });

        await waitFor(() => expect(result.current.orderLocationId).toEqual(mockedRequest.locationId));
    });

    it('should handle success case', async () => {
        const { result } = renderHook(() => useDineInOrder());

        await act(async () => {
            await result.current.getOrder(mockedRequest);
        });

        await waitFor(() => {
            expect(result.current.order).toEqual(mockOrder);
        });
    });

    it('should handle failed case', async () => {
        const mockErrorResponse = {
            type: 'EXTERNAL',
            code: 'GENERIC',
            message: 'GENERIC',
        };
        (getDineInOrderDetails as jest.Mock).mockImplementation(() => Promise.reject(mockErrorResponse));
        const { result } = renderHook(() => useDineInOrder());

        await act(async () => {
            result.current.getOrder(mockedRequest);
        });

        await waitFor(() => {
            expect(result.current.error).not.toEqual(null);
        });

        await waitFor(() => {
            expect(result.current.error).toEqual(mockErrorResponse);
        });
    });
});
