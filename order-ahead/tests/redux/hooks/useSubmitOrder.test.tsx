import useSubmitOrder, { IOrderPayload } from '../../../redux/hooks/useSubmitOrder';
import { useSelector } from 'react-redux';
import { TInitialPaymentTypes } from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { TallyPriceTypeModel } from '../../../@generated/webExpApi';
import { renderHook, act } from '@testing-library/react-hooks';
import { GTM_ERROR_EVENT } from '../../../common/services/gtmService/constants';
import { useAppDispatch } from '../../../redux/store';
import { GtmErrorCategory } from '../../../common/services/gtmService/types';
import orderService from '../../../common/services/orderService/orderService';

jest.mock('react-redux');
jest.mock('../../../redux/store');
jest.mock('../../../common/services/orderService/orderService');

const orderPayload = ({
    tallyOrder: {
        bagId: '123',
        total: 1,
        fulfillment: {
            type: 'pickup',
        },
        products: [
            {
                lineItemId: '1',
                productId: '1',
                quantity: 1,
                priceType: TallyPriceTypeModel.Original,
                price: 0,
                description: '',
                note: '',
                childItems: undefined,
                modifierGroups: undefined,
            },
        ],
    },
    customerInfo: {
        email: 'test@test.com',
        firstName: 'test',
        lastName: 'test',
        phoneNumber: '1231234',
    },
    customerId: '123',
    paymentRequest: {
        chFirstName: 'test',
        chLastName: 'test',
        paymentType: TInitialPaymentTypes.CREDIT_OR_DEBIT,
        paymentKeys: '123',
        cardIssuer: 'VISA',
        maskedCardNumber: '4111XXXXX111',
    },
    paymentInfo: {
        type: TInitialPaymentTypes.CREDIT_OR_DEBIT,
    },
    giftCards: [],
    location: {
        id: '123',
        contactDetails: {
            phone: '123',
        },
    },
    orderTime: new Date(),
    instructions: '',
    deliveryLocation: undefined,
    driverTip: 0,
    serverTip: 0,
    deviceIdentifier: '1234',
} as unknown) as IOrderPayload;

describe('useSubmitOrder', () => {
    it('should dispatch gtm event when order service fails', async () => {
        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        (useSelector as jest.Mock).mockReturnValue({});
        (orderService.createOrder as jest.Mock).mockRejectedValueOnce({
            message:
                'Our connection was interrupted. Please check your internet connection or wait for us to try again.',
        });
        const { result } = renderHook(() => useSubmitOrder());
        await act(async () => {
            result.current.submitOrder(orderPayload);
        });
        const payload = {
            ErrorCategory: GtmErrorCategory.CHECKOUT_SUBMIT,
            ErrorDescription:
                'Our connection was interrupted. Please check your internet connection or wait for us to try again.',
        };
        expect(dispatchMock).toBeCalledWith({ type: GTM_ERROR_EVENT, payload });
    });

    describe('should dispatch error', () => {
        jest.mock('../../../common/services/orderService/orderService');

        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        (useSelector as jest.Mock).mockReturnValue({});
        const { result } = renderHook(() => useSubmitOrder());

        it('when order service returns DELIVERY_ORDER_INVALID_PHONE error', async () => {
            (orderService.createOrder as jest.Mock).mockResolvedValueOnce({
                code: 'DELIVERY_ORDER_INVALID_PHONE',
            });

            await act(async () => {
                result.current.submitOrder(orderPayload);
            });

            expect(dispatchMock).toHaveBeenLastCalledWith({
                type: 'order/submit/rejected',
                payload: new Error(
                    'We apologize. Our delivery partner does not recognize your phone number. Please use a valid phone number for this delivery order.'
                ),
            });
        });

        it('when order service returns PAYMENT_FAILURE error', async () => {
            (orderService.createOrder as jest.Mock).mockResolvedValueOnce({
                code: 'PAYMENT_FAILURE',
            });

            await act(async () => {
                result.current.submitOrder(orderPayload);
            });

            expect(dispatchMock).toHaveBeenLastCalledWith({
                type: 'order/submit/rejected',
                payload: new Error(
                    'Our payment system is unavailable. To continue placing your order, please contact us at 123.'
                ),
            });
        });

        it('when order service returns PAYMENT_AMOUNT_LOWER error', async () => {
            (orderService.createOrder as jest.Mock).mockResolvedValueOnce({
                code: 'PAYMENT_AMOUNT_LOWER',
            });

            await act(async () => {
                result.current.submitOrder(orderPayload);
            });

            expect(dispatchMock).toHaveBeenLastCalledWith({
                type: 'order/submit/rejected',
                payload: new Error(
                    'The payment amount does not match the total order amount. Please review the payments methods amounts and try again.'
                ),
            });
        });

        it('when order service returns PAYMENT_AMOUNT_HIGHER error', async () => {
            (orderService.createOrder as jest.Mock).mockResolvedValueOnce({
                code: 'PAYMENT_AMOUNT_HIGHER',
            });

            await act(async () => {
                result.current.submitOrder(orderPayload);
            });

            expect(dispatchMock).toHaveBeenLastCalledWith({
                type: 'order/submit/rejected',
                payload: new Error(
                    'The payment amount does not match the total order amount. Please review the payments methods amounts and try again.'
                ),
            });
        });

        it('when order service returns PAYMENT_NOT_VALID error', async () => {
            (orderService.createOrder as jest.Mock).mockResolvedValueOnce({
                code: 'PAYMENT_NOT_VALID',
            });

            await act(async () => {
                result.current.submitOrder(orderPayload);
            });

            expect(dispatchMock).toHaveBeenLastCalledWith({
                type: 'order/submit/rejected',
                payload: new Error(
                    'The selected payment method(s) are not valid. Please select different payment method(s) and try again.'
                ),
            });
        });
    });
});
