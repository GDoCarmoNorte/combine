import { renderHook } from '../../utils';
import useConfiguration from '../../../redux/hooks/useConfiguration';

jest.unmock('../../../redux/hooks/useConfiguration');

describe('useConfiguration state hook', () => {
    it('should return configuration state', () => {
        const { result } = renderHook(() => useConfiguration());

        expect(result.current.configuration).toEqual({ isOAEnabled: true, isMyTeamsEnabled: true });
    });
    it('should return setConfiguration function', async () => {
        const { result } = renderHook(() => useConfiguration());

        result.current.actions.setConfiguration({ disableOA: true });

        expect(result.current.configuration).toEqual({ isOAEnabled: false, isMyTeamsEnabled: true });
    });
});
