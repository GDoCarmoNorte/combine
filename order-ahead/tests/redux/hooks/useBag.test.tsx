import React from 'react';
import { useSelector } from 'react-redux';
import { mount } from 'enzyme';
import useBag, { UseBagHook } from '../../../redux/hooks/useBag';
import * as PDPStore from '../../../redux/pdp';
import * as BagStore from '../../../redux/bag';

import * as bagHelper from '../../../common/helpers/bagHelper';

import { getProductItemsFromMenu } from '../../../lib/domainProduct';
import { useAppSelector } from '../../../redux/store';

jest.mock('react-redux');
jest.mock('../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));

jest.mock('../../../lib/domainProduct', () => ({
    formatPrice: jest.fn(),
    getProductItemsFromMenu: jest.fn(),
    getCheapestPriceFromProduct: jest.fn(),
}));

jest.mock('../../../common/helpers/bagHelper');
jest.mock('../../../redux/hooks/useOrderLocation', () => () => ({
    pickupAddress: jest.fn(),
    deliveryAddress: jest.fn(),
    isPickUp: jest.fn(),
}));

const useBagHook = () => {
    let bag: UseBagHook;
    const Component = () => {
        bag = useBag();

        return null;
    };
    mount(<Component />);

    return bag;
};

describe('useBag', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should dispatch removeFromBag and markAsRemoved actions for each marked item when bag is closed', () => {
        (useSelector as jest.Mock).mockReturnValue([2, 3]);

        const bag = useBagHook();

        jest.spyOn(BagStore.actions, 'removeFromBag');
        jest.spyOn(BagStore.actions, 'markAsRemoved');

        bag.actions.toggleIsOpen({ isOpen: false });

        expect(BagStore.actions.removeFromBag).toHaveBeenCalledTimes(2);
        expect(BagStore.actions.removeFromBag).toHaveBeenNthCalledWith(1, { lineItemId: 2 });
        expect(BagStore.actions.removeFromBag).toHaveBeenNthCalledWith(2, { lineItemId: 3 });

        expect(BagStore.actions.markAsRemoved).toHaveBeenCalledTimes(2);
        expect(BagStore.actions.removeFromBag).toHaveBeenNthCalledWith(1, { lineItemId: 2 });
        expect(BagStore.actions.removeFromBag).toHaveBeenNthCalledWith(2, { lineItemId: 3 });
    });

    it('should not dispatch removeFromBag and markAsRemoved actions for each marked item when bag is being opened', () => {
        (useSelector as jest.Mock).mockReturnValue([2, 3]);

        const bag = useBagHook();

        jest.spyOn(BagStore.actions, 'removeFromBag');
        jest.spyOn(BagStore.actions, 'markAsRemoved');

        bag.actions.toggleIsOpen({ isOpen: true });

        expect(BagStore.actions.removeFromBag).toHaveBeenCalledTimes(0);
        expect(BagStore.actions.markAsRemoved).toHaveBeenCalledTimes(0);
    });

    it('should dispatch deleteTallyItem action for each marked item when bag is closed', () => {
        (useSelector as jest.Mock)
            .mockReturnValue([
                { lineItemId: 2, productId: 'productId-2' },
                { lineItemId: 3, productId: 'productId-3' },
            ])
            .mockReturnValueOnce([2, 3]);

        const bag = useBagHook();

        jest.spyOn(PDPStore.actions, 'resetPdpState');

        bag.actions.toggleIsOpen({ isOpen: false });

        expect(PDPStore.actions.resetPdpState).toHaveBeenCalledTimes(2);
    });

    it('should dispatch setPickupTimeValues', () => {
        (useSelector as jest.Mock).mockReturnValue([2, 3]);

        const bag = useBagHook();

        jest.spyOn(BagStore.actions, 'setPickupTimeValues');

        bag.actions.setPickupTimeValues({ day: 'test day', timeRange: ['test time'] } as any);

        expect(BagStore.actions.setPickupTimeValues).toHaveBeenCalledTimes(1);
        expect(BagStore.actions.setPickupTimeValues).toHaveBeenCalledWith({
            day: 'test day',
            timeRange: ['test time'],
        });
    });

    describe('addDefaultToBag function', () => {
        it('should call addToBag for the new item', () => {
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockReturnValue(-1);
            (useSelector as jest.Mock).mockReturnValue({
                domainMenu: {
                    payload: {
                        products: {
                            productId: '1',
                        },
                    },
                },
                bag: {
                    LineItems: [],
                },
            });
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                products: {
                    productId: '1',
                },
            });
            const bag = useBagHook();

            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'addToBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');

            bag.actions.addDefaultToBag({ productId: '1', category: 'category', name: 'name' });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.addToBag).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).not.toBeCalled();
        });

        it('should call updateBagItemCount for the existing in the bag item', () => {
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockReturnValue(1);
            (useSelector as jest.Mock).mockReturnValue({
                domainMenu: {
                    payload: {
                        products: {
                            productId: '1',
                        },
                    },
                },
                bag: {
                    LineItems: [],
                },
            });
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                products: {
                    productId: '1',
                },
            });
            const bag = useBagHook();

            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'addToBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');

            bag.actions.addDefaultToBag({ productId: '1', category: 'category', name: 'name' });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.addToBag).not.toBeCalled();
            expect(BagStore.actions.updateBagItemCount).toHaveBeenCalledTimes(1);
        });

        it('should call markAsRemoved for an item removed from bag and added back from recommended products', () => {
            // TODO: fix this test so it works with the other tests
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockReturnValue(1);
            // (selectDefaultTallyItem as jest.Mock).mockReturnValueOnce(defaultTallyItem);
            const bagEntries = [{ productId: '1', quantity: 1, price: 1, lineItemId: 1 }];
            const mockEntriesMarkedAsRemoved = [1];

            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (useSelector as jest.Mock)
                .mockReturnValueOnce(mockEntriesMarkedAsRemoved)
                .mockReturnValueOnce(true)
                .mockReturnValueOnce(true);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                products: {
                    productId: '1',
                },
            });

            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'addToBag');
            jest.spyOn(BagStore.actions, 'markAsRemoved');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');

            // bag.actions.addDefaultToBag({ productId: '1', category: 'category', name: 'name' });

            // expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).not.toBeCalled();
            // expect(BagStore.actions.markAsRemoved).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.addToBag).not.toBeCalled();
        });
    });

    describe('putToBag function', () => {
        it('should call addToBag for the new item', () => {
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockReturnValue(-1);
            const bagEntries = [];

            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '2' }] },
            } as any);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                productId: '2',
            });

            const bag = useBagHook();

            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'addToBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');

            bag.actions.putToBag({
                pdpTallyItem: { productId: '2', quantity: 1, price: 1 },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.addToBag).toBeCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).not.toBeCalled();
        });

        it('should add extras to bag', () => {
            const bagEntries = [{ productId: '1', quantity: 1, price: 1, lineItemId: 1 }];

            jest.spyOn(bagHelper, 'findItemIndexInBag')
                .mockReturnValueOnce(-1)
                .mockReturnValueOnce(0)
                .mockReturnValue(-1);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '2' }] },
            } as any);
            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                productId: '2',
            });

            const bag = useBagHook();

            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'addToBag');
            jest.spyOn(BagStore.actions, 'addMultipleToBag');
            jest.spyOn(BagStore.actions, 'updateBagItemsCount');

            bag.actions.putToBag({
                pdpTallyItem: {
                    productId: '2',
                    quantity: 1,
                    price: 1,
                    childExtras: [
                        {
                            productId: '1',
                            quantity: 1,
                            price: 1,
                        },
                        {
                            productId: '3',
                            quantity: 1,
                            price: 1,
                        },
                    ],
                },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.addToBag).toBeCalledTimes(1);

            expect(BagStore.actions.addMultipleToBag).toBeCalledWith([
                {
                    bagEntry: {
                        lineItemId: 3,
                        productId: '3',
                        quantity: 1,
                        childExtras: [],
                    },
                },
            ]);
            expect(BagStore.actions.updateBagItemsCount).toBeCalledWith([{ bagEntryIndex: 0, value: 1 }]);
        });

        it('should merge the same items forward', () => {
            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'removeFromBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockImplementation((_, item) => +item.productId - 1);

            const bagEntries = [
                { productId: '1', quantity: 1, price: 1, lineItemId: 1 },
                { productId: '2', quantity: 2, price: 1, lineItemId: 2 },
                { productId: '3', quantity: 3, price: 1, lineItemId: 3 },
            ];

            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '2' }] },
            } as any);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                productId: '2',
            });
            const bag = useBagHook();
            bag.actions.putToBag({
                pdpTallyItem: { productId: '2', quantity: 1, price: 1, lineItemId: 3 },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateTooltip).toBeCalledWith({ tooltipIsOpen: true, updated: true });
            expect(BagStore.actions.removeFromBag).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.removeFromBag).toBeCalledWith({ lineItemId: 3 });
            expect(BagStore.actions.updateBagItemCount).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).toBeCalledWith({ bagEntryIndex: 1, value: 1 });
        });

        it('should merge the same items backward', () => {
            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'removeFromBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockImplementation((_, item) => +item.productId - 1);

            const bagEntries = [
                { productId: '1', quantity: 1, price: 1, lineItemId: 1 },
                { productId: '2', quantity: 2, price: 1, lineItemId: 2 },
                { productId: '3', quantity: 3, price: 1, lineItemId: 3 },
            ];

            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '2' }] },
            } as any);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                productId: '2',
            });

            const bag = useBagHook();

            bag.actions.putToBag({
                pdpTallyItem: { productId: '3', quantity: 1, price: 1, lineItemId: 2 },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateTooltip).toBeCalledWith({ tooltipIsOpen: true, updated: true });
            expect(BagStore.actions.removeFromBag).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.removeFromBag).toBeCalledWith({ lineItemId: 2 });
            expect(BagStore.actions.updateBagItemCount).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).toBeCalledWith({ bagEntryIndex: 1, value: 1 });
        });

        it('should merge the same items quantity', () => {
            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'editBagLineItem');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockImplementation((_, item) => +item.productId - 1);

            const bagEntries = [
                { productId: '1', quantity: 1, price: 1, lineItemId: 1 },
                { productId: '2', quantity: 2, price: 1, lineItemId: 2 },
                { productId: '3', quantity: 3, price: 1, lineItemId: 3 },
            ];

            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '2' }] },
            } as any);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                productId: '2',
            });

            const bag = useBagHook();

            bag.actions.putToBag({
                pdpTallyItem: { productId: '2', quantity: 10, price: 1, lineItemId: 3 },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateTooltip).toBeCalledWith({ tooltipIsOpen: true, updated: true });
            expect(BagStore.actions.updateBagItemCount).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).toBeCalledWith({ bagEntryIndex: 1, value: 10 });
        });

        it('should not have side effects if no changes applied', async () => {
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockReturnValue(1);
            const bagEntries = [
                { productId: '1', quantity: 1, price: 1, lineItemId: 1 },
                { productId: '2', quantity: 2, price: 1, lineItemId: 2 },
                { productId: '3', quantity: 3, price: 1, lineItemId: 3 },
            ];

            (useAppSelector as jest.Mock).mockReturnValueOnce(bagEntries).mockReturnValueOnce(0);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '2' }] },
            } as any);
            (getProductItemsFromMenu as jest.Mock).mockReturnValueOnce({
                productId: '2',
            });

            const bag = useBagHook();

            jest.spyOn(BagStore.actions, 'removeFromBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');

            bag.actions.putToBag({
                pdpTallyItem: { productId: '2', quantity: 2, price: 1, lineItemId: 2 },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.removeFromBag).not.toBeCalled();
            expect(BagStore.actions.updateBagItemCount).not.toBeCalled();
        });

        it('should call updateBagItemCount for the existing in the bag item', () => {
            jest.spyOn(bagHelper, 'findItemIndexInBag').mockReturnValueOnce(1);
            const bagEntries = [{ productId: '1', quantity: 1, price: 1, lineItemId: 1 }];
            (useAppSelector as jest.Mock).mockReturnValue(bagEntries).mockReturnValue(0).mockReturnValue([]);
            (useSelector as jest.Mock).mockReturnValue({
                bag: {
                    LineItems: bagEntries,
                },
                domainMenu: { payload: [{ productId: '1' }] },
            } as any);
            (getProductItemsFromMenu as jest.Mock).mockReturnValue({
                productId: '1',
            });
            (getProductItemsFromMenu as jest.Mock).mockReturnValueOnce(1);
            const bag = useBagHook();

            jest.spyOn(BagStore.actions, 'updateTooltip');
            jest.spyOn(BagStore.actions, 'addToBag');
            jest.spyOn(BagStore.actions, 'updateBagItemCount');

            bag.actions.putToBag({
                pdpTallyItem: {
                    productId: '1',
                    price: 1,
                    quantity: 1,
                    lineItemId: 1,
                },
                category: 'category',
                name: 'name',
            });

            expect(BagStore.actions.updateTooltip).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.updateBagItemCount).toHaveBeenCalledTimes(1);
            expect(BagStore.actions.addToBag).not.toBeCalled();
        });
    });
});
