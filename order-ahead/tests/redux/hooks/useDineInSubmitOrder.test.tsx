import { useDineInSubmitOrder } from '../../../redux/hooks/useDineInSubmitOrder';
import { act, renderHook } from '@testing-library/react-hooks';
import { waitFor } from '@testing-library/react';
import { submitDineInOrder } from '../../../common/services/orderService/dineInService';

jest.mock('../../../common/services/orderService/dineInService', () => ({
    submitDineInOrder: jest.fn().mockImplementation(() => {
        new Promise((resolve) => {
            setTimeout(resolve, 0);
        });
    }),
}));

const mockOrder = {
    orderInfo: {
        orderId: 'id',
        subTotalAfterDiscounts: 14,
        tax: 2,
    },
    paymentRequest: {
        chFirstName: 'fname',
        chLastName: 'lname',
        paymentKeys: ['key'],
        sessionKey: 'sessionkey',
        cardIssuer: 'Visa',
        maskedCardNumber: '41111111XXXX1111',
    },
    deviceId: 'deviceid',
    tipAmount: 2.0,
} as any;

describe('useDineInSubmitOrder', () => {
    it('should handle loading state', async () => {
        const { result } = renderHook(() => useDineInSubmitOrder());

        act(() => {
            result.current.submitOrder(mockOrder);
        });

        await waitFor(() => expect(result.current.loading).toEqual(true));

        await waitFor(() => expect(result.current.loading).toEqual(false));
    });

    it('should handle success case', async () => {
        const { result } = renderHook(() => useDineInSubmitOrder());

        await act(async () => {
            await result.current.submitOrder(mockOrder);
        });

        expect(result.current.loading).toEqual(false);

        expect(submitDineInOrder).toBeCalledWith({
            amount: 16,
            deviceId: 'deviceid',
            orderId: 'id',
            payments: [
                {
                    amount: 18,
                    cardHolderName: 'fname lname',
                    cardIssuer: 'Visa',
                    maskedCardNumber: '41111111XXXX1111',
                    paymentKeys: ['key'],
                    sessionKey: 'sessionkey',
                    type: 'CREDIT',
                },
            ],
            tipAmount: 2,
        });
    });

    // error handling covered by separate story
});
