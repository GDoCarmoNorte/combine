import dismissedAlertBannersReducer, * as DissmissedAlertBannersStore from '../../redux/dismissedAlertBanners';

describe('bag reducer', () => {
    it('should return the initial state', () => {
        // TODO: find a better way to pass empty action without ANY type
        expect(dismissedAlertBannersReducer(undefined, {} as any)).toEqual(DissmissedAlertBannersStore.initialState);
    });

    it(`should handle ${DissmissedAlertBannersStore.actions.dismissBanner.type}`, () => {
        const payload = { bannerEntry: { bannerId: '1' } };
        expect(
            dismissedAlertBannersReducer(DissmissedAlertBannersStore.initialState, {
                type: DissmissedAlertBannersStore.actions.dismissBanner.type,
                payload,
            })
        ).toEqual([payload.bannerEntry]);
    });
});
