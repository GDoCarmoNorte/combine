import { configureStore } from '@reduxjs/toolkit';
import localTapListReducer, * as localTapList from '../../redux/localTapList';

describe('localTapList slice', () => {
    const store = configureStore({
        reducer: {
            localTapList: localTapListReducer,
        },
    });

    it('Should return initial state', () => {
        expect(store.getState().localTapList).toEqual(localTapList.initialState);
    });

    it('Should handle setLocalTapList action', () => {
        store.dispatch(localTapList.actions.setLocalTapList({} as any));

        expect(store.getState().localTapList.payload).toEqual({});
    });
});
