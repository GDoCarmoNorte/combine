import configurationReducer, * as configurationStore from '../../redux/configuration';
import { AnyAction } from 'redux';

describe('configuration reducer', () => {
    it('should initialize with initial state', () => {
        expect(configurationReducer(undefined, {} as AnyAction)).toEqual(configurationStore.initialState);
    });

    it(`should handle ${configurationStore.actions.setConfiguration.type}`, () => {
        const configurationMock = {
            configurationRefreshFrequency: 1,
            personalizationRefreshFrequency: 2,
        };
        expect(
            configurationReducer(configurationStore.initialState, {
                type: configurationStore.actions.setConfiguration.type,
                payload: configurationMock,
            })
        ).toMatchObject({ configuration: configurationMock });
    });
});
