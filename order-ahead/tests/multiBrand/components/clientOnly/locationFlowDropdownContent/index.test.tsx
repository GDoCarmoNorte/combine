import React from 'react';
import { render, screen } from '@testing-library/react';

import LocationFlowDropdownContent from '../../../../../multiBrand/components/clientOnly/locationFlowDropdown/bww.locationFlowDropdownContent';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useOrderLocation, useConfiguration } from '../../../../../redux/hooks';
import { OrderLocationMethod } from '../../../../../redux/orderLocation';
import { useAppSelector } from '../../../../../redux/store';
import configFeatureFlagsMocks from '../../../../mocks/configFeatureFlag.mock';
import { useRouter } from 'next/router';

jest.mock('next/router', () => ({
    useRouter: jest.fn(() => ({
        query: {},
    })),
}));
jest.mock('react-redux');
jest.mock('../../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: () => jest.fn(),
}));
jest.mock('../../../../../redux/hooks/useGlobalProps', () => jest.fn().mockReturnValue({}));
jest.mock('../../../../../redux/hooks/useOrderLocation');
jest.mock('@material-ui/core/useMediaQuery', () => jest.fn().mockReturnValue(false));
jest.mock('../../../../../redux/hooks/useBag', () => jest.fn().mockReturnValue({}));
jest.mock('../../../../../redux/store', () => ({
    useAppSelector: jest.fn(),
}));
jest.mock('../../../../../redux/hooks/useConfiguration');

jest.mock('../../../../../common/hooks/useBagAnalytics', () => ({
    useBagAnalytics: jest.fn().mockReturnValue({
        pushGtmLocationOrder: jest.fn(),
        pushGtmChangeLocation: jest.fn(),
    }),
}));
describe('LocationFlowDropdownContent component', () => {
    (useAppSelector as jest.Mock).mockReturnValue(configFeatureFlagsMocks);
    beforeEach(() => {
        (useOrderLocation as jest.Mock).mockReturnValue({ method: OrderLocationMethod.NOT_SELECTED } as any);
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: configFeatureFlagsMocks,
        });
    });

    it('should render "START AN ORDER" button for desktop if location does not exist', () => {
        render(<LocationFlowDropdownContent isLocationSelected={false} />);

        expect(screen.getByText(/start an order/i)).toBeInTheDocument();
    });

    it('should render "START AN ORDER" button for mobile if location does not exist', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(true);

        render(<LocationFlowDropdownContent isLocationSelected={false} />);

        expect(screen.getByText(/start an order/i)).toBeInTheDocument();
    });

    it('should not render "START AN ORDER" button if mobile viewport on pdp page', () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(true);
        (useRouter as jest.Mock).mockReturnValueOnce({
            query: { productDetailsPageUrl: 'all-american-double-burger' },
        });
        render(<LocationFlowDropdownContent isLocationSelected={false} />);

        expect(screen.queryByText(/start an order/i)).not.toBeInTheDocument();
    });

    it('should render location dropdown if location exists', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: { storeId: '55522' },
        } as any);

        render(<LocationFlowDropdownContent isOAEnabled={true} isLocationSelected={true} />);

        // TODO: update after adding BWW implementation
        expect(screen.getByText(/My Arby’s/i)).toBeInTheDocument();
    });

    it('should hide location block and show disabled CTA start an order', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            pickupAddress: { storeId: '55522' },
        } as any);

        const { container } = render(<LocationFlowDropdownContent isOAEnabled={false} isLocationSelected={true} />);

        expect(container).toMatchSnapshot();
    });
});
