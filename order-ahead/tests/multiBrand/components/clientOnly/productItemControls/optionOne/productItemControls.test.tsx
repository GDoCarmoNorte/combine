import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';
import ProductItemControls from '../../../../../../multiBrand/components/clientOnly/productItemControls/optionOne/productItemControls';
import globalPropsMock from '../../../../../mocks/globalProps.mock';
import { useDomainMenu } from '../../../../../../redux/hooks';
import { useProductHasRequiredModifiers } from '../../../../../../common/hooks/useProductHasRequiredModifiers';

const dispatchMock = jest.fn();

jest.mock('react-redux', () => ({
    useDispatch: () => dispatchMock,
}));

jest.mock('../../../../../../redux/store', () => ({
    __esModule: true,
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));

jest.mock('../../../../../../common/hooks/useProductHasRequiredModifiers', () => ({
    useProductHasRequiredModifiers: jest.fn().mockReturnValue(false),
}));

jest.mock('../../../../../../redux/hooks', () => ({
    useDomainMenu: jest.fn(() => ({ loading: false })),
    usePdp: jest.fn(() => ({ actions: { resetPdpState: jest.fn() } })),
    useGlobalProps: jest.fn(() => undefined).mockImplementation(() => globalPropsMock),
}));

const productMock = { fields: 'productMock' };
const defaultProps = { isSaleable: true, isAvailable: true };

const getModifyLink = () => screen.getByRole('link', { name: /modify/i });
const getAddToBagButton = () => screen.queryByText(/add to bag/i);

describe('ProductItemControls', () => {
    it('should render product item controls with loading state', () => {
        ((useDomainMenu as unknown) as jest.Mock).mockImplementationOnce(() => ({ loading: true }));

        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={{} as any}
                product={productMock as any}
            />
        );

        expect(container).toMatchSnapshot();
    });
    it('should render add to bag button with link viewtype', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={{} as any}
                product={productMock as any}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render view item link with link viewtype', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable={false}
                productDetails={{} as any}
                product={productMock as any}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render add to bag and modify buttons with link viewtype for combo', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isCombo
                isOrderAheadAvailable
                productDetails={{ itemModifierGroups: [{ itemModifiers: [{ itemId: 1 }] }] } as any}
                product={productMock as any}
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render add to bag button with button viewtype', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={undefined}
                product={productMock as any}
                viewType="button"
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render view item link with button viewtype', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable={false}
                productDetails={{} as any}
                product={productMock as any}
                viewType="button"
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should render add to bag button with button viewtype for combo', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isCombo
                isOrderAheadAvailable
                productDetails={{ itemModifierGroups: [] } as any}
                product={productMock as any}
                viewType="button"
            />
        );

        expect(container).toMatchSnapshot();
    });

    it('should contain only add to bag button', () => {
        render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable
                productDetails={{ itemModifierGroups: [] } as any}
                product={productMock as any}
                viewType="link"
            />
        );

        const modifyButton = screen.queryByText(/modify/i);

        expect(getAddToBagButton()).toBeInTheDocument();
        expect(modifyButton).toBeNull();
    });

    it('should render add to bag button and modify button', () => {
        const validItemModifiers = [
            {
                min: 1,
                defaultQuantity: 2,
            },
            {
                min: 2,
                defaultQuantity: 2,
            },
            {
                max: 2,
                defaultQuantity: 2,
            },
            {
                max: 3,
                defaultQuantity: 2,
            },
            {
                min: 1,
                max: 2,
                defaultQuantity: 2,
            },
            {
                min: 2,
                max: 2,
                defaultQuantity: 2,
            },
            {
                min: 1,
                max: 2,
                defaultQuantity: 1,
            },
            {
                min: 1,
                max: 2,
                defaultQuantity: 2,
            },
        ];

        validItemModifiers.forEach((itemModifier) => {
            const productDetailsMock = {
                itemModifierGroups: [
                    {
                        itemModifiers: {
                            'Modifier-1': itemModifier,
                        },
                    },
                ],
            };

            render(
                <ProductItemControls
                    {...defaultProps}
                    isOrderAheadAvailable
                    productDetails={productDetailsMock as any}
                    product={productMock as any}
                    viewType="link"
                />
            );

            expect(getModifyLink()).toBeInTheDocument();
            expect(getAddToBagButton()).toBeInTheDocument();

            cleanup();
        });
    });

    it('should render only view item button when item is not saleable', () => {
        const { container } = render(
            <ProductItemControls
                {...defaultProps}
                isOrderAheadAvailable={true}
                productDetails={{} as any}
                product={productMock as any}
                viewType="button"
                isSaleable={false}
            />
        );

        expect(container).toMatchSnapshot();
    });

    describe('should render only modify button', () => {
        it('with button viewtype', () => {
            (useProductHasRequiredModifiers as jest.Mock).mockReturnValueOnce(true);

            const productDetailsMock = {
                itemModifierGroups: [
                    {
                        min: 1,
                        itemModifiers: {
                            'Modifier-1': {
                                defaultQuantity: 0,
                            },
                        },
                    },
                ],
            };

            render(
                <ProductItemControls
                    {...defaultProps}
                    isOrderAheadAvailable
                    productDetails={productDetailsMock as any}
                    product={productMock as any}
                    viewType="button"
                />
            );

            expect(getModifyLink()).toBeInTheDocument();
            expect(getAddToBagButton()).toBeNull();
        });

        it('with link viewtype', () => {
            (useProductHasRequiredModifiers as jest.Mock).mockReturnValueOnce(true);

            const productDetailsMock = {
                itemModifierGroups: [
                    {
                        min: 1,
                        itemModifiers: {
                            'Modifier-1': {
                                defaultQuantity: 0,
                            },
                        },
                    },
                ],
            };

            render(
                <ProductItemControls
                    {...defaultProps}
                    isOrderAheadAvailable
                    productDetails={productDetailsMock as any}
                    product={productMock as any}
                    viewType="link"
                />
            );

            expect(getModifyLink()).toBeInTheDocument();
            expect(getAddToBagButton()).toBeNull();
        });
    });
});
