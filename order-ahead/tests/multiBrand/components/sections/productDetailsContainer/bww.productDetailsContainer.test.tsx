import React from 'react';
import { render, cleanup, screen } from '@testing-library/react';

import ProductDetailsContainer from '../../../../../multiBrand/components/sections/productDetailsContainer/bww.productDetailsContainer';

import featureFlagsMock from '../../../../mocks/featureFlags.mock';
import { useDispatch } from 'react-redux';
import usePdp from '../../../../../redux/hooks/usePdp';
import useNavIntercept from '../../../../../redux/hooks/useNavIntercept';
import { useLocalization } from '../../../../../common/hooks/useLocalization';
import { useProductIsSaleable } from '../../../../../common/hooks/useProductIsSaleable';
import { NOT_SALEABLE_BUTTON_TEXT } from '../../../../../common/constants/product';

jest.mock('next/head', () => () => 'Head');
jest.mock('../../../../../components/organisms/header/baseHeader', () => () => 'BaseHeader');
jest.mock('../../../../../components/organisms/footer', () => () => 'Footer');
jest.mock('../../../../../components/atoms/Breadcrumbs', () => () => 'Breadcrumbs');
jest.mock('../../../../../components/sections/aboutSection/aboutSection', () => () => 'AboutSection');

jest.mock('react-redux');
jest.mock('next/dynamic', () =>
    jest
        .fn()
        .mockReturnValueOnce(() => 'FloatingBanner')
        .mockReturnValueOnce(() => 'ProductBanner')
);

jest.mock('next/router', () => ({
    useRouter: jest.fn().mockImplementation(() => ({ asPath: '/' })),
}));

jest.mock('../../../../../redux/hooks/domainMenu', () => ({
    useProductSizes: jest.fn(),
    useNutrition: jest.fn(),
    useFullProduct: jest.fn().mockReturnValue({ rootProduct: {} }),
    usePriceAndCalories: jest.fn().mockReturnValue({ price: 'price' }),
    useFullProductSizes: jest.fn(),
    useDomainProduct: jest.fn(),
    reducer: jest.fn().mockReturnValue({}),
    useSelectedModifiers: () => [],
    useDomainProductByContentfulFields: jest.fn(),
    useDomainMenuSelectors: jest.fn().mockReturnValue({
        selectRelatedComboByMainProductIdAndSizeId: 'id',
        selectDefaultTallyItem: 'id',
    }),
}));

jest.mock('../../../../../redux/hooks/pdp/', () => ({
    useBwwDisplayProduct: jest.fn().mockReturnValue({
        productSections: [],
    }),
    useDisplayModifierGroupsByProductId: jest.fn(),
}));
jest.mock('../../../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn().mockReturnValue(false),
}));

jest.mock('../../../../../common/hooks/useLocalization', () => ({
    useLocalization: jest.fn().mockReturnValue({ locationLinkText: 'locationLinkText', isProductAvailable: false }),
}));
jest.mock('../../../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: jest.fn().mockReturnValue({
        isSaleable: true,
    }),
}));
jest.mock('../../../../../redux/hooks/usePdp', () => jest.fn());
jest.mock('../../../../../redux/hooks/useBag', () => jest.fn());
jest.mock('../../../../../redux/hooks/useNavIntercept', () => jest.fn());
jest.mock('../../../../../lib/domainProduct', () => ({
    getUpgradeMealPath: jest.fn().mockReturnValue({}),
}));

jest.mock('../../../../../common/hooks/pdpHooks.ts', () => ({
    useComboPriceIncreaseAnalytics: jest.fn(),
}));
jest.mock('../../../../../redux/hooks/useGlobalProps', () => () => ({}));

jest.mock('../../../../../common/hooks/useUnavailableCategories', () => ({
    useUnavailableCategories: jest.fn().mockReturnValue([]),
}));

jest.mock('../../../../../common/helpers/schemaPdpAndPlp', () => () => jest.fn());

jest.mock('../../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

describe('ProductDetailsContainer', () => {
    afterEach(cleanup);

    test('should render the product details container correctly', async () => {
        (useDispatch as jest.Mock).mockReturnValue(jest.fn());

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useInitTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useHasUnsavedModifications: jest.fn().mockReturnValue(true),
        });

        (useNavIntercept as jest.Mock).mockReturnValue({
            isPending: false,
            hasChanges: false,
            navigate: jest.fn(),
        });

        const { container } = render(
            <ProductDetailsContainer
                canonicalPath="canonicalPath"
                currentCategory={{} as any}
                productNutritionLink={null}
                product={{ fields: { productId: 'productId', name: 'name' } } as any}
                globalProps={
                    {
                        footer: {},
                        alertBanners: {},
                        navigation: {},
                        modifierItemsById: {},
                        productsById: 'productsById',
                    } as any
                }
                featureFlags={featureFlagsMock}
            />
        );

        expect(container).toMatchSnapshot();
    });

    test('should render a location button when order ahead is not available', () => {
        const LOCATION_LINK_TEXT = 'LOCATION_LINK_TEXT';

        (useLocalization as jest.Mock).mockReturnValue({
            locationLinkText: LOCATION_LINK_TEXT,
        });

        render(
            <ProductDetailsContainer
                canonicalPath="canonicalPath"
                currentCategory={{} as any}
                productNutritionLink={null}
                product={{ fields: { productId: 'productId', name: 'name' } } as any}
                globalProps={
                    {
                        footer: {},
                        alertBanners: {},
                        navigation: {},
                        modifierItemsById: {},
                        productsById: 'productsById',
                    } as any
                }
                featureFlags={featureFlagsMock}
            />
        );

        expect(screen.getAllByText(LOCATION_LINK_TEXT)[0]).toBeInTheDocument();
    });

    test('should render a view menu button if item is not saleable', () => {
        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({
            isSaleable: false,
        });

        render(
            <ProductDetailsContainer
                canonicalPath="canonicalPath"
                currentCategory={{} as any}
                productNutritionLink={null}
                product={{ fields: { productId: 'productId', name: 'name' } } as any}
                globalProps={
                    {
                        footer: {},
                        alertBanners: {},
                        navigation: {},
                        modifierItemsById: {},
                        productsById: 'productsById',
                    } as any
                }
                featureFlags={featureFlagsMock}
            />
        );

        expect(screen.getByText(NOT_SALEABLE_BUTTON_TEXT)).toBeInTheDocument();
    });
});
