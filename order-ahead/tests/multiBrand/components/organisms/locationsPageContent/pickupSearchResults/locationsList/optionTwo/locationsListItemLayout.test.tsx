import React from 'react';
import { render } from '@testing-library/react';
import LocationsListItemLayout from '../../../../../../../../multiBrand/components/organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItemLayout/optionTwo/locationsListItemLayout';

describe('LocationsListItemLayout', () => {
    it('should render component', () => {
        const { container } = render(
            <LocationsListItemLayout
                title={<div>title</div>}
                primaryCta={<div>primaryCta</div>}
                secondaryCta={<div>secondaryCta</div>}
                address={<div>address</div>}
                openingHours={<div>openingHours</div>}
                distance={<div>distance</div>}
                storeId={<div>storeId</div>}
                services={<div>services</div>}
            />
        );

        expect(container).toMatchSnapshot();
    });
});
