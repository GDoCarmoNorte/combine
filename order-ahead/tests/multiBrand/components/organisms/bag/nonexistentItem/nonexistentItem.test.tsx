import React from 'react';
import { render, screen } from '@testing-library/react';
import NonexistentItem from '../../../../../../multiBrand/components/organisms/bag/nonexistentItem/bww.nonexistentItem';
import mockContentfulProduct from '../../../../../mocks/contentfulProduct.mock';

describe('NonexistentItem within the shopping bag', () => {
    it('should render component', () => {
        render(<NonexistentItem contentfulProduct={mockContentfulProduct as any} quantity={1} />);
        expect(screen.getByText(/Classic Beef 'N Cheddar Meal/i)).toBeInTheDocument();
    });

    it('should render component with quantity more than 1', () => {
        render(<NonexistentItem contentfulProduct={mockContentfulProduct as any} quantity={2} />);
        expect(screen.getByText(/2X Classic Beef 'N Cheddar Meal/i)).toBeInTheDocument();
    });
});
