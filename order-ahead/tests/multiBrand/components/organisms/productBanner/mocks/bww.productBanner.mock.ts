export default {
    sys: {
        type: 'test',
        id: '',
        createdAt: '',
        updatedAt: '',
        locale: 'en-US',
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'product',
            },
        },
    },
    fields: {
        productId: 1,
        image: {
            fields: {
                file: {
                    url: 'https://google.com',
                },
                description: 'Meals Signatures Gyro Greek',
                title: 'Meals Signatures Gyro Greek',
            },
        },
    },
};
