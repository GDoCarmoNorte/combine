import React from 'react';
import { screen, render, cleanup } from '@testing-library/react';

import productMock from './mocks/bww.productBanner.mock';
import ProductBanner from '../../../../../multiBrand/components/organisms/productBanner/bww.productBanner';
import extrasMock from '../../../../components/organisms/extras/extras.mock';
import { useLocalization } from '../../../../../common/hooks/useLocalization';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useProductIsSaleable } from '../../../../../common/hooks/useProductIsSaleable';
import {
    NOT_SALEABLE_BUTTON_TEXT,
    NOT_AVAILABLE_DESCRIPTION,
    GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
    GTM_NOT_SALEABLE_DESCRIPTION,
} from '../../../../../common/constants/product';
import { useBwwSelectionsText } from '../../../../../redux/hooks/pdp';
import { useProductIsAvailable } from '../../../../../common/hooks/useProductIsAvailable';
import { useDispatch } from 'react-redux';
import { GtmErrorEvent } from '../../../../../common/services/gtmService/types';
import { GTM_ERROR_EVENT } from '../../../../../common/services/gtmService/constants';
import { useConfiguration } from '../../../../../redux/hooks';

jest.mock('@material-ui/core/useMediaQuery', () => jest.fn().mockReturnValue(false));

jest.mock('../../../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

jest.mock('../../../../../redux/hooks/pdp', () => ({
    useExtraProducts: () => extrasMock,
    useChangedModifiersForChildItem: () => ({
        addedModifiers: [],
        removedDefaultModifiers: [],
    }),
    useDisplayModifierGroupsByProductId: () => ({}),
    useBwwSelectionsText: jest.fn().mockReturnValue(''),
    useSelectedExtras: () => null,
}));

jest.mock('../../../../../redux/hooks/domainMenu', () => ({
    useIsProductEditable: () => true,
    useDefaultModifiersByModifierGroupType: () => [],
    useSelectedModifiersByModifierGroupTypes: () => [],
    useSelectedSideAndDrinks: () => [],
    useProductSizes: () => [],
    useNoItemProductIds: () => [],
    useProductItemGroup: () => ({}),
}));

jest.mock('../../../../../common/hooks/useLocalization', () => ({
    useLocalization: jest.fn().mockReturnValue({
        locationLinkText: '',
        descriptionText: '',
        isLocationOrderAheadAvailable: true,
        isProductAvailable: true,
    }),
}));

jest.mock('../../../../../redux/hooks/usePdp', () => () => ({
    pdpTallyItem: {
        productId: 'arb-itm-000-057',
    },
}));

jest.mock('../../../../../components/organisms/modifiersSelection', () =>
    jest.fn(() => <div data-testid="modifiers-selection-mock" />)
);

jest.mock('../../../../../redux/hooks/useGlobalProps', () => () => ({}));

jest.mock('../../../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: jest.fn().mockReturnValue({
        isSaleable: true,
    }),
}));

jest.mock('../../../../../common/hooks/useProductIsAvailable', () => ({
    useProductIsAvailable: jest.fn().mockReturnValue({ isAvailable: true }),
}));

jest.mock('../../../../../common/hooks/useSodiumWarning', () => ({
    useSodiumWarning: () => null,
}));

jest.mock('../../../../../common/hooks/useTallyItemHasSodiumWarning', () => ({
    useTallyItemHasSodiumWarning: () => false,
}));

jest.mock('../../../../../common/hooks/useSodiumLegalWarning', () => ({
    useSodiumLegalWarning: () => null,
}));

jest.mock('react-redux');

const productInfo = {
    price: 100,
    calories: 100,
};

describe('productBanner', () => {
    const mockDispatch = jest.fn();

    beforeEach(() => {
        (useDispatch as jest.Mock).mockReturnValue(mockDispatch);
    });

    afterEach(() => {
        cleanup();
        mockDispatch.mockReset();
    });

    test('should render the product banner correctly', async () => {
        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        expect(screen.getByLabelText(/Product Main Section/)).toMatchSnapshot();
    });

    test('should render the product banner for mobile correctly', async () => {
        (useMediaQuery as jest.Mock).mockReturnValueOnce(true);

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        expect(screen.getByLabelText(/Product Main Section/)).toMatchSnapshot();
    });

    test('should render location order ahead not available message and dispatch gtm error event', () => {
        const onSizeSelect = jest.fn();
        const LOCATION_LINK_MESSAGE = 'LOCATION_LINK_MESSAGE';
        const DESCRIPTION_TEXT = 'DESCRIPTION_TEXT';

        (useLocalization as jest.Mock<any, any>).mockReturnValueOnce({
            locationLinkText: LOCATION_LINK_MESSAGE,
            descriptionText: DESCRIPTION_TEXT,
            isLocationOrderAheadAvailable: false,
        });

        render(
            <ProductBanner
                product={productMock as any}
                onSizeSelect={onSizeSelect as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        const payload = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: DESCRIPTION_TEXT,
        } as GtmErrorEvent;
        expect(mockDispatch).toHaveBeenCalledWith({ type: GTM_ERROR_EVENT, payload });
        expect(screen.getByText(LOCATION_LINK_MESSAGE)).toBeInTheDocument();
        expect(screen.getByText(DESCRIPTION_TEXT)).toBeInTheDocument();
    });

    test('should render item not saleable message and dispatch not salable message', () => {
        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({
            isSaleable: false,
        });

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        const payload = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: GTM_NOT_SALEABLE_DESCRIPTION,
        } as GtmErrorEvent;
        expect(mockDispatch).toHaveBeenCalledWith({ type: GTM_ERROR_EVENT, payload });
        expect(screen.getByText(NOT_SALEABLE_BUTTON_TEXT)).toBeInTheDocument();
    });

    test('should render selections text', () => {
        (useBwwSelectionsText as jest.Mock).mockReturnValueOnce('Product');

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        expect(screen.getByText(/Product/i)).toBeInTheDocument();
    });

    test('should not render selections text if product not saleable or not available', () => {
        const PRODUCT_NAME = 'PRODUCT_NAME';

        (useBwwSelectionsText as jest.Mock).mockReturnValueOnce(PRODUCT_NAME);
        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({
            isSaleable: false,
        });

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        expect(screen.queryByText(PRODUCT_NAME)).toBeNull();

        (useProductIsSaleable as jest.Mock).mockReturnValueOnce({
            isSaleable: true,
        });

        (useLocalization as jest.Mock<any, any>).mockReturnValueOnce({
            locationLinkText: 'text',
            isLocationOrderAheadAvailable: false,
        });

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        expect(screen.queryByText(PRODUCT_NAME)).toBeNull();
    });

    test('should render with specific description if category is not available of time and dispatch not available gtm error event', () => {
        (useProductIsAvailable as jest.Mock).mockReturnValueOnce({
            isAvailable: false,
        });

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="Greek Gyro"
                productType="MEAL"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        const payload = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: NOT_AVAILABLE_DESCRIPTION,
        } as GtmErrorEvent;

        expect(mockDispatch).toHaveBeenCalledWith({ type: GTM_ERROR_EVENT, payload });
        expect(screen.getByText(NOT_AVAILABLE_DESCRIPTION)).toBeInTheDocument();
    });

    test('should show error message when order ahead disabled and disabled CTA', () => {
        (useBwwSelectionsText as jest.Mock).mockReturnValueOnce('Product');
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isOAEnabled: false,
            },
        });

        render(
            <ProductBanner
                product={productMock as any}
                productInfo={productInfo}
                displayName="TRADITIONAL WINGS"
                productType="BOGO"
                modifierItemsById={{} as any}
                productSections={[] as any}
            />
        );

        expect(screen.getByText(/We apologize. Online ordering is not available at this moment./i)).toBeInTheDocument();
    });
});
