import React from 'react';
import { screen, render, waitFor, fireEvent } from '@testing-library/react';

import PickupDeliveryInfo from '../../../../../../multiBrand/components/organisms/checkout/pickupDeliveryInfo/bww.index';

import useBag from '../../../../../../redux/hooks/useBag';
import useOrderLocation from '../../../../../../redux/hooks/useOrderLocation';

import { resolveOpeningHours } from '../../../../../../lib/locations';
import { add } from '../../../../../../common/helpers/dateTime';
import { OrderLocationMethod } from '../../../../../../redux/orderLocation';
import useLocationAvailableTimes from '../../../../../../common/hooks/useLocationAvailableTimes';

jest.mock('../../../../../../redux/hooks/useBag');
jest.mock('../../../../../../redux/hooks/useOrderLocation');
jest.mock('../../../../../../lib/locations');
jest.mock('../../../../../../common/hooks/useLocationAvailableTimes');

const dateReferenceStart = new Date('2020-12-17T06:00:00.000Z');
const dateReferenceEnd = new Date('2020-12-17T19:00:00.000Z');
const openedTimeRanges = [
    {
        start: dateReferenceStart,
        end: dateReferenceEnd,
    },
    {
        start: add(dateReferenceStart, { days: 1 }),
        end: add(dateReferenceEnd, { days: 1 }),
    },
    {
        start: add(dateReferenceStart, { days: 2 }),
        end: add(dateReferenceEnd, { days: 2 }),
    },
    {
        start: add(dateReferenceStart, { days: 3 }),
        end: add(dateReferenceEnd, { days: 3 }),
    },
    {
        start: add(dateReferenceStart, { days: 4 }),
        end: add(dateReferenceEnd, { days: 4 }),
    },
    {
        start: add(dateReferenceStart, { days: 5 }),
        end: add(dateReferenceEnd, { days: 5 }),
    },
    {
        start: add(dateReferenceStart, { days: 6 }),
        end: add(dateReferenceEnd, { days: 6 }),
    },
    {
        start: add(dateReferenceStart, { days: 7 }),
        end: add(dateReferenceEnd, { days: 7 }),
    },
];

const pickupTimeValuesMock = [
    {
        day: '2020-12-17T06:00:00.000Z',
        timeRange: ['2020-12-17T13:15:00.000Z', '2020-12-17T13:30:00.000Z', '2020-12-17T13:45:00.000Z'],
    },
];

const availableTimeSlotsMock = {
    pickup: {
        byDay: {
            '2020-12-17': [
                {
                    display: '07:15 AM',
                    utc: '2020-12-17T13:15:00.000Z',
                },
                {
                    display: '07:15 AM',
                    utc: '2020-12-17T13:30:00.000Z',
                },
                {
                    display: '07:15 AM',
                    utc: '2020-12-17T13:45:00.000Z',
                },
            ],
        },
    },
    delivery: {
        byDay: {
            '2020-12-17': [
                {
                    display: '07:15 AM',
                    utc: '2020-12-17T13:15:00.000Z',
                },
            ],
        },
    },
};

const RealDate = Date.now;

const setDeliveryLocationAvailableTimeSlotsMock = jest.fn();
const setPickupLocationAvailableTimeSlotsMock = jest.fn();
const setPickupTimeValuesMock = jest.fn();
const getLocationAvailableTimeSlotsMock = jest.fn().mockResolvedValue(availableTimeSlotsMock);
const setPickupTime = jest.fn();

describe('pickupInfo component', () => {
    beforeAll(() => {
        global.Date.now = jest.fn(() => new Date('2020-12-17T13:00:00.000Z').getTime());
    });

    afterAll(() => {
        global.Date.now = RealDate;
    });

    beforeEach(() => {
        (useLocationAvailableTimes as jest.Mock).mockReturnValue({
            getLocationAvailableTimeSlots: () => getLocationAvailableTimeSlotsMock(),
        });
        (useBag as jest.Mock).mockReturnValue({
            orderTime: null,
            orderTimeType: 'asap',
            pickupTimeValues: pickupTimeValuesMock,
            pickupTimeIsValid: true,
            actions: {
                setPickupTime,
                setPickupTimeValues: () => setPickupTimeValuesMock(),
            },
        });

        (resolveOpeningHours as jest.Mock).mockReturnValue({
            openedTimeRanges,
            storeTimezone: 'America/New_York',
            isOpen: true,
        });
    });

    it('should show "ASAP" option with corresponding hint for PICKUP, if store is open and have preparation time field', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1, additionalFeatures: { prepTime: 5 } },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(setPickupTimeValuesMock).toBeCalled();
        });
        expect(getLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setPickupLocationAvailableTimeSlotsMock).toBeCalled();

        expect(screen.getByText(/asap/i)).toBeInTheDocument();
        expect(screen.getByText(/(Ready in ~5 minutes)/i)).toBeInTheDocument();
    });

    it('should show "ASAP" option with corresponding hint for DELIVERY, if store is open and have preparation time field', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.DELIVERY,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
                deliveryLocation: { addressLine2: 'test address' },
            },
            currentLocation: { id: 1, additionalFeatures: { prepTime: 5 } },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                fulfilmentTime={new Date('2020-12-17T12:28:00.000Z')}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(setPickupTimeValuesMock).toBeCalled();
        });
        expect(getLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setDeliveryLocationAvailableTimeSlotsMock).toBeCalled();

        expect(screen.getByText(/ASAP 12:28 PM/i)).toBeInTheDocument();
        expect(screen.getByText(/(Ready in ~5 minutes)/i)).toBeInTheDocument();
    });

    it('should show "ASAP" option without corresponding hint, if store is open but not have preparation time field', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(setPickupTimeValuesMock).toBeCalled();
        });
        expect(getLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setPickupLocationAvailableTimeSlotsMock).toBeCalled();

        expect(screen.getByText(/asap/i)).toBeInTheDocument();
        expect(screen.queryByText(/(Ready in ~5 minutes)/i)).not.toBeInTheDocument();
    });

    it('should show "ASAP:TIME" option, if there is ASAP delivery order', async () => {
        (useBag as jest.Mock).mockReturnValue({
            orderTime: null,
            orderTimeType: 'asap',
            pickupTimeValues: pickupTimeValuesMock,
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
                setPickupTimeValues: () => setPickupTimeValuesMock(),
            },
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
                fulfilmentTime={new Date('2020-12-17T12:28:00.000Z')}
            />
        );

        await waitFor(() => {
            expect(setPickupTimeValuesMock).toBeCalled();
        });
        expect(getLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setPickupLocationAvailableTimeSlotsMock).toBeCalled();

        expect(screen.getByText(/ASAP 12:28 PM/i)).toBeInTheDocument();
        expect(screen.queryByText(/(Ready in ~5 minutes)/i)).not.toBeInTheDocument();
    });

    it('should not show "ASAP" option if orderTime type is FUTURE', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });
        (useBag as jest.Mock).mockReturnValue({
            orderTime: '2020-12-17T13:30:00.000Z',
            orderTimeType: 'future',
            pickupTimeValues: pickupTimeValuesMock,
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
                setPickupTimeValues: () => setPickupTimeValuesMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(setPickupTimeValuesMock).toBeCalled();
        });
        expect(getLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setPickupLocationAvailableTimeSlotsMock).toBeCalled();

        expect(screen.queryByText(/asap/i)).not.toBeInTheDocument();
        expect(screen.getByText(/01:30 PM/i)).toBeInTheDocument();
    });

    it('should handle errors during availbale times retrieving', async () => {
        (useLocationAvailableTimes as jest.Mock).mockReturnValue({
            getLocationAvailableTimeSlots: jest.fn().mockRejectedValueOnce('test error'),
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: (value) => setPickupLocationAvailableTimeSlotsMock(value),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(setPickupTimeValuesMock).toBeCalled();
        });
        expect(getLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setPickupLocationAvailableTimeSlotsMock).toBeCalled();
        expect(setPickupLocationAvailableTimeSlotsMock).toBeCalledWith(undefined);
    });

    it('should show Today as day and ASAP as time if no available slots', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });
        (useBag as jest.Mock).mockReturnValue({
            orderTime: null,
            orderTimeType: 'asap',
            pickupTimeValues: [],
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
                setPickupTimeValues: () => setPickupTimeValuesMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(screen.getByText(/asap/i)).toBeInTheDocument();
        });
        expect(screen.getByText(/today/i)).toBeInTheDocument();
    });

    it('should show switch to pickup message and have the dropdown as unavailable', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: false,
            deliveryLocationTimeSlots: {
                pickup: {
                    byDay: {},
                },
                delivery: {
                    byDay: {},
                },
            },
            pickupLocationTimeSlots: {
                pickup: {
                    byDay: {},
                },
                delivery: {
                    byDay: {},
                },
            },
            method: OrderLocationMethod.DELIVERY,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });

        (useBag as jest.Mock).mockReturnValue({
            orderTime: null,
            orderTimeType: 'asap',
            pickupTimeValues: [],
            pickupTimeIsValid: true,
            actions: {
                setPickupTime: jest.fn(),
                setPickupTimeValues: () => setPickupTimeValuesMock(),
            },
        });

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        await waitFor(() => {
            expect(screen.getByText('Select either pickup or a different location')).toBeInTheDocument();
        });
    });

    it('should call setFieldLevelError', () => {
        (useOrderLocation as jest.Mock).mockReturnValue({
            isPickUp: true,
            deliveryLocationTimeSlots: availableTimeSlotsMock,
            pickupLocationTimeSlots: availableTimeSlotsMock,
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: {
                pickUpLocation: { id: 1 },
            },
            currentLocation: { id: 1 },
            actions: {
                setDeliveryLocationAvailableTimeSlots: () => setDeliveryLocationAvailableTimeSlotsMock(),
                setPickupLocationAvailableTimeSlots: () => setPickupLocationAvailableTimeSlotsMock(),
            },
        });
        (useBag as jest.Mock).mockReturnValue({
            orderTime: '2020-12-17T13:30:00.000Z',
            orderTimeType: 'future',
            pickupTimeValues: pickupTimeValuesMock,
            pickupTimeIsValid: true,
            actions: {
                setPickupTime,
                setPickupTimeValues: () => setPickupTimeValuesMock(),
            },
        });
        const setFieldLevelError = jest.fn();
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce([pickupTimeValuesMock[0], jest.fn()])
            .mockReturnValueOnce([false, jest.fn()])
            .mockReturnValueOnce([false, jest.fn()])
            .mockReturnValueOnce([false, jest.fn()])
            .mockReturnValueOnce([null, setFieldLevelError])
            .mockReturnValueOnce([null, jest.fn()]);

        render(
            <PickupDeliveryInfo
                onDeliveryInfoChange={jest.fn()}
                onChange={jest.fn()}
                onInvalidTimeUpdate={jest.fn()}
                loading={false}
            />
        );

        const getPickupDate = () => screen.getAllByText(/Today \(12\/17\/2020\)/i);
        const getFirstPickupTime = () => screen.getByText(/01:30 PM/i);
        const getSecondPickupTime = () => screen.getByText(/01:45 PM/i);

        fireEvent.click(getPickupDate()[0]);
        fireEvent.click(getPickupDate()[1]);
        expect(setFieldLevelError).toHaveBeenCalledTimes(1);

        fireEvent.click(getFirstPickupTime());
        fireEvent.click(getSecondPickupTime());
        expect(setFieldLevelError).toHaveBeenCalledTimes(2);
    });
});
