import getDisplayCalories from '../../../../../multiBrand/components/atoms/productItemPriceAndCalories/bww.getDisplayCalories';

describe('Display calories', () => {
    it('should return Calorie info unavailable', () => {
        const displayCalories = getDisplayCalories();
        expect(displayCalories).toBe('Calorie info unavailable');
    });

    it('should return calories', () => {
        const displayCalories = getDisplayCalories(500);
        expect(displayCalories).toBe('500 cal');
    });
});
