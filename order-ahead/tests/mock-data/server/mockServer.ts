import { setupServer } from 'msw/node';
import { rest } from 'msw';

import navigationMock from '../../mock-data/contentful/contentful-getEntries-navigation.json';
import alertBannerMock from '../../mock-data/contentful/contentful-getEntries-alertBanner.json';
import productMock from '../../mock-data/contentful/contentful-getEntries-product.json';
import productModifierMock from '../../mock-data/contentful/contentful-getEntries-productModifier.json';
import dealMock from '../../mock-data/contentful/contentful-getEntries-deal.json';
import footer from '../../mock-data/contentful/contentful-getEntries-footer.json';
import menuCategoryMock from '../../mock-data/contentful/contentful-getEntries-menuCategory.json';
import menuMock from '../../mock-data/contentful/contentful-getEntries-menu.json';
import menuSubcategoryMock from '../../mock-data/contentful/contentful-getEntries-menuSubcategory.json';
import pagesMock from '../../mock-data/contentful/contentful-getEntries-pages.json';
import dealsPage from '../../mock-data/contentful/contentful-getEntry-page-deals.json';
import locationsPage from '../../mock-data/contentful/contentful-getEntry-page-locations.json';
import confirmationPage from '../../mock-data/contentful/contentful-getEntries-page-confirmation.json';
import checkoutPage from '../../mock-data/contentful/contentful-getEntries-page-checkout.json';
import homePage from '../../mock-data/contentful/contentful-getEntries-page-home.json';
import giftCardsPage from '../../mock-data/contentful/contentful-getEntries-giftCards.json';
import accountDealsPage from '../../mock-data/contentful/contentful-getEntries-accountDealsPage.json';
import userAccountMenu from '../../mock-data/contentful/contentful-getEntries-userAccountMenu.json';
import emptyShoppingBag from '../../mock-data/contentful/contentful-getEntries-emptyShoppingBag.json';
import accountPage from '../../mock-data/contentful/contentful-getEntries-page-account.json';
import rewardsPage from '../../mock-data/contentful/contentful-getEntries-page-rewards.json';
import rewardsRosterPage from '../../mock-data/contentful/contentful-getEntries-page-rewards-roster.json';
import allLocationsPage from '../../mock-data/contentful/contentful-getEntry-page-allLocations.json';
import locationDetailsPage from '../../mock-data/contentful/contentful-getEntry-page-locationDetails.json';
import locationNewsSection from '../../mock-data/contentful/contentful-getEntities-locationNewsSection.json';
import cityLocationsPage from '../../mock-data/contentful/contentful-getEntry-page-cityLocations.json';
import stateLocationsPage from '../../mock-data/contentful/contentful-getEntry-page-stateLocations.json';
import playPage from '../../mock-data/contentful/contentful-getEntries-page-play.json';
import checkoutLegal from '../../mock-data/contentful/contentful-getEntries-checkoutLegal.json';

export const server = setupServer();

export const contentfulHandlers = rest.get(
    'https://cdn.contentful.com:443/spaces/mock-space/environments/mock-env/entries',
    (req, res, ctx) => {
        let response;

        const contentType = req.url.searchParams.get('content_type');
        const sysId = req.url.searchParams.get('sys.id');

        if (contentType) {
            switch (contentType) {
                case 'navigation':
                    response = ctx.json(navigationMock);
                    break;
                case 'alertBanner':
                    response = ctx.json(alertBannerMock);
                    break;
                case 'product':
                    response = ctx.json(productMock);
                    break;
                case 'productModifier':
                    response = ctx.json(productModifierMock);
                    break;
                case 'deal':
                    response = ctx.json(dealMock);
                    break;
                case 'footer':
                    response = ctx.json(footer);
                    break;
                case 'menuCategory':
                    response = ctx.json(menuCategoryMock);
                    break;
                case 'menu':
                    response = ctx.json(menuMock);
                    break;
                case 'menuSubcategory':
                    response = ctx.json(menuSubcategoryMock);
                    break;
                case 'page':
                    response = ctx.json(pagesMock);
                    break;
                case 'userAccountMenu':
                    response = ctx.json(userAccountMenu);
                    break;
                case 'emptyShoppingBag':
                    response = ctx.json(emptyShoppingBag);
                    break;
                case 'locationNewsSection':
                    response = ctx.json(locationNewsSection);
                    break;
                case 'actionParameter':
                    response = ctx.json([]);
                    break;
                case 'sodiumWarning':
                    response = ctx.json({});
                    break;
                case 'productNutritionLink':
                    response = ctx.json({});
                    break;
                case 'checkoutLegal':
                    response = ctx.json(checkoutLegal);
                    break;
                default:
                    console.error(
                        `NO contentful mock response available for contentType: ${contentType}. Returning empty response...`
                    );
                    response = ctx.json({});
                    break;
            }
        }

        if (sysId) {
            switch (sysId) {
                case '2jVCpa521MvlmryW1h8QZT':
                    response = ctx.json(dealsPage);
                    break;
                case '1SSMLPOwxPLSWjXPhXtinC':
                    response = ctx.json(locationsPage);
                    break;
                case '3XcpWUoX3tIsk5ACSJTQbV':
                    response = ctx.json(confirmationPage);
                    break;
                case '5vrxEE5BVN2D2oTe4mR9yB':
                    response = ctx.json(checkoutPage);
                    break;
                case '25dr1IzzV008aiA7rxuPJh':
                    response = ctx.json(homePage);
                    break;
                case '3Rqrj7fwlxIjJrKj3ewWsn':
                    response = ctx.json(giftCardsPage);
                    break;
                case 'J8k4ZyOeBcNQeyyPlXzP6':
                    response = ctx.json(accountDealsPage);
                    break;
                case '5vrxEE5BVN2D2oTe4mR9yD':
                    response = ctx.json(accountPage);
                    break;
                case '3hYcBvbRj2RBuU2Riz6vEH':
                    response = ctx.json(rewardsPage);
                    break;
                case '3ZEdMObo0wM0uws3PjHmO7':
                    response = ctx.json(rewardsRosterPage);
                    break;
                case '6DcLmMR25BVfYY1cXWSkIp':
                    response = ctx.json(allLocationsPage);
                    break;
                case '2Pgsu1NEhDjrNS9DkRe60r':
                    response = ctx.json(locationDetailsPage);
                    break;
                case '6DcLmMR25BVfYY1cXWsdfa':
                    response = ctx.json(cityLocationsPage);
                    break;
                case '6DcLmMR25BVfYY1cXWssta':
                    response = ctx.json(stateLocationsPage);
                    break;
                case '7BrHzAifa2pSjbACHCxJTT':
                    response = ctx.json(playPage);
                    break;
                default:
                    console.error(
                        `NO contentful mock response available for sysId: ${sysId}. Returning empty response...`
                    );
                    response = ctx.json({});
                    break;
            }
        }

        return res(response);
    }
);
