import { act, screen } from '@testing-library/react';
import { axe } from '../../../jest/axe-helper';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { getPage } from '../../lib/integration/getPage';
import { clear } from 'jest-date-mock';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
import { isAccountOn } from '../../../lib/getFeatureFlags';
import { useRouter } from 'next/router';
import userEvent from '@testing-library/user-event';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../lib/getFeatureFlags', () => ({
    __esModule: true,
    isDineInOrdersEnabled: jest.fn().mockReturnValue(true),
    isGiftCardPayEnabled: jest.fn().mockReturnValue(true),
    isAccountOn: jest.fn().mockReturnValue(true),
    getFeatureFlags: jest.fn().mockReturnValue({}),
    default: jest.fn().mockReturnValue({}),
    isCustomerPaymentMethodEnabled: jest.fn().mockReturnValue(true),
    isAccountDealsPageOn: jest.fn().mockReturnValue(false),
    locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
    isRewardsOn: jest.fn().mockReturnValue(true),
    isLocalTapListOn: jest.fn().mockReturnValue(false),
    isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
    locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
    isPersonalizationEnabled: jest.fn().mockReturnValue(false),
    isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
    isOAEnabled: jest.fn().mockReturnValue(true),
    isTippingEnabled: jest.fn().mockReturnValue(true),
    isDeliveryEnabled: jest.fn().mockReturnValue(true),
    isApplePayEnabled: jest.fn().mockReturnValue(true),
    isGooglePayEnabled: jest.fn().mockReturnValue(true),
    isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
    isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
    isFraudCheckEnabled: jest.fn().mockReturnValue(false),
    isPaymentProcessingScreenEnabled: jest.fn().mockReturnValue(false),
    isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
}));

jest.mock('next/router', () => ({
    //@ts-ignore
    ...jest.requireActual('next/router'),
    useRouter: jest.fn(),
}));

const loginStub = jest.fn();

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
            loginWithRedirect: loginStub,
            isLoading: false,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

const deleteAccountMock = jest.fn();
jest.mock('../../../common/services/customerService/account', () => {
    return {
        __esModule: true,
        initAccountService: jest.fn().mockReturnValue({
            deleteAccount: deleteAccountMock,
        }),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
        getNationalMenu: jest.fn().mockResolvedValue(undefined),
    };
});

describe('Delete Account - integration', () => {
    let clearNextHeadMock, renderResult;

    const routerReplaceMock = jest.fn();
    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
        (useRouter as jest.Mock).mockReturnValue({
            replace: routerReplaceMock,
            query: {},
            asPath: '/',
            pathname: '/',
        });

        // @ts-ignore
        global.ReactNativeWebView = { postMessage: jest.fn() };
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();
        server.close();
    });

    const render = async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/account/delete',
            });

            renderResult = render().container;
        });
    };

    test('a11y', async () => {
        await render();
        const results = await axe(renderResult);
        expect(results).toHaveNoViolations();
    });

    test('should go to home page if account functionality is disabled', async () => {
        (isAccountOn as jest.Mock).mockReturnValue(false);

        await render();

        expect(routerReplaceMock).toBeCalledWith('/');
    });

    test('should post cancel message to reactNativeWebView', async () => {
        (isAccountOn as jest.Mock).mockReturnValueOnce(false);

        await render();

        userEvent.click(screen.getByText(/cancel/i));

        //@ts-ignore
        expect(global.ReactNativeWebView.postMessage).toBeCalledWith('{"messageCode":"account_deletion_cancelled"}');
    });

    test('should call redirect to login on delete btn click on are you sure screen', async () => {
        await render();

        userEvent.click(screen.getAllByText(/delete account/i)[2]);

        //@ts-ignore
        expect(loginStub).toBeCalled();
    });

    test('should go to feedback screen after redirect from auth0', async () => {
        (useRouter as jest.Mock).mockReturnValue({
            replace: routerReplaceMock,
            query: { step: 'feedback' },
            asPath: '/',
            pathname: '/',
        });

        await render();

        expect(await screen.findByText(/feedback/i)).toBeInTheDocument();
    });

    test('should go to delete screen after selecting feedback', async () => {
        (useRouter as jest.Mock).mockReturnValue({
            replace: routerReplaceMock,
            query: { step: 'feedback' },
            asPath: '/',
            pathname: '/',
        });

        await render();

        userEvent.click(screen.getByText(/too many bugs or technical issues/i));
        userEvent.click(screen.getByText(/continue/i));
        await render();

        expect(await screen.findByText(/delete my account/i)).toBeInTheDocument();
    });

    test('should delete account after confirming on the delete screen', async () => {
        (useRouter as jest.Mock).mockReturnValue({
            replace: routerReplaceMock,
            query: { step: 'feedback' },
            asPath: '/',
            pathname: '/',
        });
        await render();
        userEvent.click(screen.getByText(/too many bugs or technical issues/i));
        userEvent.click(screen.getByText(/continue/i));
        await render();
        userEvent.click(screen.getByText(/i understand that by deleting my information/i));
        userEvent.click(screen.getByText(/submit request/i));

        await act(() => {
            deleteAccountMock as jest.Mock;
        });

        await render();
        expect(await screen.findByText(/this confirms that your account has been deleted./i)).toBeInTheDocument();
    });
});
