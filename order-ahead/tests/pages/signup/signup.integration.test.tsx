import { act } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';
jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('@auth0/auth0-react');
const loginWithRedirect = jest.fn();

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            isAuthenticated: false,
            loginWithRedirect,
        })),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
        getNationalMenu: jest.fn().mockResolvedValue(undefined),
    };
});

describe('signup page', () => {
    let clearNextHeadMock, renderResult;
    beforeAll(() => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/create-account',
            });

            renderResult = render().container;
        });
    });

    test('a11y', async () => {
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    it('should call loginWithRedirect', () => {
        expect(loginWithRedirect).toBeCalledTimes(1);
    });
});
