import { act, screen } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { advanceTo, clear } from 'jest-date-mock';
import fetchMock from 'jest-fetch-mock';
import mockStoreValues from '../../mock-data/store/confirmationPageSore.mock.json';
import { axe } from '../../../jest/axe-helper';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
import { useAuth0 } from '@auth0/auth0-react';
import { isSingUpBannerOnConfirmationEnabled } from '../../../lib/getFeatureFlags';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/localStorage', () => {
    const {
        domainMenu,
        orderLocation,
        tally,
        submitOrder,
        configuration,
    } = require('../../mock-data/store/confirmationPageSore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            domainMenu,
            orderLocation,
            tally,
            submitOrder,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,
        // @ts-ignore
        ...jest.requireActual('@auth0/auth0-react'),
        useAuth0: jest.fn(),
        withAuthenticationRequired: jest.fn().mockImplementation((component) => component),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
        getNationalMenu: jest.fn().mockResolvedValue(undefined),
    };
});

jest.spyOn(global.console, 'error').mockImplementation(() => null);

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(true),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(false),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

//TODO check after new pick-up/delivery flow will be imblementet successfully
describe('Confirmation Page - Integration', () => {
    const renderPage = async () =>
        await act(async () => {
            const { render } = await getPage({
                route: '/confirmation?id=985880',
            });

            renderResult = render().container;
        });

    beforeAll(async () => {
        fetchMock.mockResponse((req) => {
            if (req.url.includes('/webExpApiPath/v1/menu/type/ALLDAY/id/99984?sellingChannel=WEBOA')) {
                // @ts-ignore
                return Promise.resolve(JSON.stringify(mockStoreValues.domainMenu.payload));
            }
            if (req.url.includes('/webExpApiPath/v1/location/99984')) {
                return Promise.resolve(
                    JSON.stringify({
                        brandName: 'arbys',
                        details: {
                            latitude: 37.9396,
                            longitude: -86.7602,
                        },
                        displayName: "Arby's",
                        timezone: 'America/New_York',
                        contactDetails: {
                            address: {
                                city: 'TELL CITY',
                                countryCode: 'US',
                                line1: '100 E STATE HWY 66',
                                line2: '',
                                line3: '',
                                postalCode: '47586',
                                stateProvinceCode: 'IN',
                            },
                            phone: '812-547-4411',
                        },
                    })
                );
            }
        });

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
        (useAuth0 as jest.Mock).mockReturnValue({ isAuthenticated: false });
        (isSingUpBannerOnConfirmationEnabled as jest.Mock).mockReturnValue(false);
    });

    afterAll(() => {
        clear();

        server.close();
    });

    let renderResult;

    beforeAll(() => {
        advanceTo(new Date('2020-12-01T10:20:30Z'));

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);
    });

    test('a11y', async () => {
        await renderPage();
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('should display pick up date and confirmation number', async () => {
        await renderPage();
        const getTextOrder = () => screen.getByText(/your order will be ready for pickup/i);
        const getTextConfirmation = () => screen.getByText(/Confirmation #985880/i);

        expect(getTextOrder()).toBeInTheDocument();
        expect(getTextConfirmation()).toBeInTheDocument();
    });

    test('should display location information', async () => {
        await renderPage();
        const getLocationName = () => screen.getByRole('heading', { name: /Arby's/i });
        const getLocationAddress = () => screen.getByText(/100 E STATE HWY 66, TELL CITY, IN 47586/i);
        const getLocationPhoneNumber = () => screen.getByRole('link', { name: '(812) 547-4411' });
        const getDirectLink = () => screen.getByRole('link', { name: /Get Directions/i });

        expect(getLocationName()).toBeInTheDocument();
        expect(getLocationAddress()).toBeInTheDocument();
        expect(getLocationPhoneNumber()).toBeInTheDocument();
        expect(getDirectLink()).toBeInTheDocument();
    });

    test('should display pick up instructions', async () => {
        await renderPage();
        const getHeadingPickUpInstructions = () => screen.getByRole('heading', { name: /pickup instructions/i });
        const getTextFromInstroctions = () =>
            screen.getByText(/Look for your name at the online order shelf in the lobby/i);

        expect(getHeadingPickUpInstructions()).toBeInTheDocument();
        expect(getTextFromInstroctions()).toBeInTheDocument();
    });

    test('should display order summary', async () => {
        await renderPage();
        const getQuantity = () => screen.getByText(/1x/i);
        const getSubtotalPrice = () => screen.getAllByText(/\$7.69/i);

        const getModifierPrice = () => screen.getByText(/\$3.91/i);

        const getSubtotulText = () => screen.getByText(/Subtotal/i);
        const getTaxtext = () => screen.getByText(/Tax/i);
        const getTaxPrice = () => screen.getByText(/\$0.54/i);
        const getTotalText = () => screen.getByText('Total');
        const getTotalPrice = () => screen.getByText(/\$8.23/i);
        const getByTotal = () => screen.getByText(/\$8.23/i);

        expect(getQuantity()).toBeInTheDocument();
        expect(getSubtotalPrice()).toHaveLength(2);

        expect(getModifierPrice()).toBeInTheDocument();

        expect(getSubtotulText()).toBeInTheDocument();
        expect(getTaxtext()).toBeInTheDocument();
        expect(getTaxPrice()).toBeInTheDocument();
        expect(getTotalText()).toBeInTheDocument();
        expect(getTotalPrice()).toBeInTheDocument();
        expect(getByTotal()).toBeInTheDocument();
    });

    test('should display sign up link', async () => {
        await renderPage();
        const getSingUpLink = () => screen.getByRole('link', { name: /Sign-up for the Arby’s email program/i });

        expect(getSingUpLink()).toBeInTheDocument();
    });

    test('should show sign up banner for non authenticated users', async () => {
        (isSingUpBannerOnConfirmationEnabled as jest.Mock).mockReturnValue(true);
        await renderPage();

        expect(screen.getByAltText(/icon/i)).toBeInTheDocument();
        expect(screen.getByText(/sign up and earn POINTS on future orders/i)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: /Sign up now/i })).toBeInTheDocument();
    });

    test('should not show sign up banner for authenticated users', async () => {
        (isSingUpBannerOnConfirmationEnabled as jest.Mock).mockReturnValue(true);
        (useAuth0 as jest.Mock).mockReturnValue({ isAuthenticated: true });
        await renderPage();

        expect(screen.queryByAltText(/icon/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/sign up and earn POINTS on future orders/i)).not.toBeInTheDocument();
        expect(screen.queryByRole('link', { name: /Sign up now/i })).not.toBeInTheDocument();
    });

    test('should not show sign up banner *if the feature is disabled', async () => {
        (isSingUpBannerOnConfirmationEnabled as jest.Mock).mockReturnValue(false);
        (useAuth0 as jest.Mock).mockReturnValue({ isAuthenticated: false });
        await renderPage();

        expect(screen.queryByAltText(/icon/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/sign up and earn POINTS on future orders/i)).not.toBeInTheDocument();
        expect(screen.queryByRole('link', { name: /Sign up now/i })).not.toBeInTheDocument();
    });
});
