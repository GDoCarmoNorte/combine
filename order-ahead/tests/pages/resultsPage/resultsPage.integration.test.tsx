import { screen, within, act, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';
import { advanceTo, clear } from 'jest-date-mock';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';

import getBrandInfo from '../../../lib/brandInfo';
import { getMenuCategoryPageTitle } from '../../../common/helpers/getPageTitle';
import mockMenuCategory from '../../mock-data/contentful/contentful-getEntries-menuCategory.json';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(true),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

describe('Results Page - Integration', () => {
    let renderResult;
    let clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        advanceTo(new Date('2020-12-01T10:20:30Z'));

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/menu/signature',
            });

            renderResult = render().container;
        });
    });

    test('a11y', async () => {
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', () => {
        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('menu categories list opens when clicked and closes after clicked on same category item', async () => {
        const productButton = screen.getByText(/^signature/i, { selector: 'div' });

        userEvent.click(productButton);

        const menuContainer = await screen.findByRole('listbox');

        within(menuContainer).getByText(/chicken/i);
        within(menuContainer).getByText(/desserts/i);
        within(menuContainer).getByText(/drinks/i);
        within(menuContainer).getByText(/kids menu/i);
        within(menuContainer).getByText(/limited time/i);
        within(menuContainer).getByText(/market fresh/i);
        within(menuContainer).getByText(/meals/i);
        within(menuContainer).getByText(/roast beef/i);
        within(menuContainer).getByText(/sides/i);
        within(menuContainer).getByText(/signature/i);
        within(menuContainer).getByText(/sliders/i);
        within(menuContainer).getByText(/top picks/i);

        userEvent.click(within(menuContainer).getByText(/signature/i));
        expect(screen.queryByRole('listbox')).not.toBeInTheDocument();
    });

    test("show 6 specific products w/ name, calories, price and 'ADD TO BAG' & 'MODIFY' buttons", async () => {
        // eslint-disable-next-line testing-library/no-node-access
        const getModifierGroupCategoryContainer = (name: RegExp) => screen.getByText(name).closest('div'); //TODO: fix a11y

        expect(screen.queryByRole('listbox')).not.toBeInTheDocument();

        expect(await screen.findAllByRole('button', { name: /ADD TO BAG/i })).toHaveLength(6);
        expect(screen.getAllByRole('link', { name: /MODIFY/i })).toHaveLength(6);

        const smokehouseBrisketCard = getModifierGroupCategoryContainer(/smokehouse brisket/i);
        within(smokehouseBrisketCard).getByText(/\$5.69/i);
        within(smokehouseBrisketCard).getByText(/600 calories/i);

        const greekGyroCard = getModifierGroupCategoryContainer(/greek gyro/i);
        within(greekGyroCard).getByText(/\$4.19/i);
        within(greekGyroCard).getByText(/710 calories/i);

        const roastBeefGyroCard = getModifierGroupCategoryContainer(/roast beef gyro/i);
        within(roastBeefGyroCard).getByText(/\$4.19/i);
        within(roastBeefGyroCard).getByText(/550 calories/i);

        const roastTurkeyGyroCard = getModifierGroupCategoryContainer(/Roast Turkey Gyro/i);
        within(roastTurkeyGyroCard).getByText(/\$4.19/i);
        within(roastTurkeyGyroCard).getByText(/470 calories/i);

        const cornedBeefCard = getModifierGroupCategoryContainer(/Corned Beef Reuben/i);
        within(cornedBeefCard).getByText(/\$5.49/i);
        within(cornedBeefCard)
            .getByText(/680 calories/i)
            // eslint-disable-next-line testing-library/no-node-access
            .closest('div'); //TODO: fix a11y

        const loadedItalianCard = getModifierGroupCategoryContainer(/Loaded Italian/i);
        within(loadedItalianCard).getByText(/\$4.99/i);
        within(loadedItalianCard).getByText(/630 calories/i);

        // the product has 'isVisible: false' so it should not be displayed
        expect(screen.queryByText(/Spicy Greek Gyro/i)).not.toBeInTheDocument();
    });

    describe('location selection', () => {
        const getSelectedLocationName = () => screen.getAllByText(/Dunwoody - Roswell Rd/i)[0];

        test('show "Dunwoody - Roswell Rd" location selected', () => {
            screen.getByText(/pickup from/i);
            getSelectedLocationName();
        });

        test('NOT show location details by default and show it when button clicked and hidden when button clicked again', () => {
            expect(screen.queryByText(/Change Location/i)).not.toBeInTheDocument();

            userEvent.click(getSelectedLocationName());

            const searchContainer = screen.getAllByRole('presentation', {
                hidden: true,
            });

            within(searchContainer[0]).getByRole('button', {
                name: /continue pickup order/i,
                hidden: true,
            });
            within(searchContainer[0]).getByText(/pickup location/i);
            within(searchContainer[0]).getByTitle(/Dunwoody - Roswell Rd/i);
            within(searchContainer[0]).getByTitle(/100 E STATE HWY 66, TELL CITY, IN 00501/i);
            within(searchContainer[0]).getByText(/Change Address/i);

            userEvent.click(getSelectedLocationName());
            expect(screen.queryByText(/Change Location/i)).not.toBeInTheDocument();
        });
    });

    test('menu category page should have correct title', () => {
        const brandInfo = getBrandInfo();

        expect(global.window).toBeDefined();
        expect(global.window.document).toBeDefined();

        expect(brandInfo).toBeDefined();
        expect(brandInfo.brandName).toBeDefined();

        const signatureCategory = mockMenuCategory.items.find(
            (category) => category.fields.categoryName === 'Signature'
        );

        expect(global.window.document.title).toBe(
            getMenuCategoryPageTitle(brandInfo.brandName, signatureCategory?.fields.categoryName)
        );
    });

    test('menu category page should have correct meta description', async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                'content',
                "Come by and try any of our delicious signature options at your local Arby's. Online ordering now available!"
            );
        });
    });
});
