import React from 'react';
import { mount } from 'enzyme';
import { waitFor, render } from '@testing-library/react';
import { useDispatch, useSelector } from 'react-redux';

import HomePage from '../../pages';
import getLocationById from '../../common/services/locationService/getLocationById';
import * as nextRouter from 'next/router';
import isLocationOrderAheadAvailable from '../../lib/locations/isLocationOrderAheadAvailable';
import featureFlagsMock from '../mocks/featureFlags.mock';
import configFeatureFlags from '../mocks/configFeatureFlag.mock';
import { useLocationUnavailableError } from '../../common/hooks/useLocationUnavailableError';
import { usePersonalization } from '../../redux/hooks';

jest.mock('../../common/services/locationService/getLocationById');
jest.mock('../../lib/locations/isLocationOrderAheadAvailable');
usePersonalization;
jest.mock('react-redux');

jest.mock('../../redux/hooks', () => ({
    useDomainMenu: jest.fn(() => ({ loading: false, actions: { getDomainMenu: jest.fn() } })),
    useNotifications: jest.fn(() => ({ actions: { enqueueError: jest.fn() } })),
    useOrderLocation: jest.fn(() => ({ actions: { setPickupLocation: jest.fn() } })),
    useConfiguration: jest.fn(() => ({ configuration: configFeatureFlags })),
    useOrderHistory: jest.fn(() => ({ orderHistory: [] })),
    useGlobalProps: jest.fn().mockReturnValue({
        footer: null,
        alertBanners: null,
        navigation: null,
    }),
    usePersonalization: jest.fn().mockReturnValue({ loading: false }),
    useAccount: jest.fn().mockReturnValue({ account: null }),
    useLoyalty: jest.fn().mockReturnValue({ loyalty: { pointsBalance: 0 } }),
    useRewards: jest.fn().mockReturnValue({ totalCount: 0 }),
}));

jest.mock('../../redux/store', () => ({
    useAppSelector: jest.fn(),
    useAppDispatch: jest.fn(),
}));

jest.mock('../../common/hooks/useBreadcrumbSchema', () => ({
    useBreadcrumbSchema: jest.fn().mockReturnValue({}),
}));

jest.mock('../../common/hooks/useLocationUnavailableError', () => ({
    useLocationUnavailableError: jest.fn().mockReturnValue({
        pushLocationUnavailableError: jest.fn(),
    }),
}));
const page = {
    fields: {
        metaTitle: 'test',
        metaDecription: 'test',
    },
};

describe('HomePage', () => {
    it('when location exists and isOrderAheadAvailable should redirect to menu', async () => {
        const routerPushMock = jest.fn();
        jest.spyOn(nextRouter, 'useRouter').mockImplementationOnce(
            () =>
                ({
                    query: { locationId: '123' },
                    push: routerPushMock,
                } as any)
        );

        (useSelector as jest.Mock).mockReturnValue({ isPending: false, hasChanges: false });
        (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
        (getLocationById as jest.Mock).mockReturnValueOnce({ id: 1, storeId: 1, isDigitallyEnabled: true });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValueOnce(true);

        await mount(
            <HomePage
                isPreviewMode={false}
                page={page as any}
                pageSections={[]}
                featureFlags={featureFlagsMock}
                canonicalPath={''}
                hasPersonalizedContent={false}
            />
        );

        expect(routerPushMock).toHaveBeenCalledWith('/menu');
    });

    it('when location exists but isNotOrderAheadAvailable should redirect to locations', async () => {
        const routerPushMock = jest.fn();
        jest.spyOn(nextRouter, 'useRouter').mockImplementationOnce(
            () =>
                ({
                    query: { locationId: '123' },
                    push: routerPushMock,
                } as any)
        );

        (useSelector as jest.Mock).mockReturnValue({ isPending: false, hasChanges: false });
        (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
        (getLocationById as jest.Mock).mockReturnValueOnce({ id: 1, storeId: 1 });
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValueOnce(false);

        await mount(
            <HomePage
                isPreviewMode={false}
                page={page as any}
                pageSections={[]}
                featureFlags={featureFlagsMock}
                canonicalPath={''}
                hasPersonalizedContent={false}
            />
        );

        expect(routerPushMock).toHaveBeenCalledWith('/locations');
    });

    it('when location is not found should redirect to locations', async () => {
        const routerPushMock = jest.fn();
        jest.spyOn(nextRouter, 'useRouter').mockImplementationOnce(
            () =>
                ({
                    query: { locationId: 'fake-location-id' },
                    push: routerPushMock,
                } as any)
        );

        (useSelector as jest.Mock).mockReturnValue({ isPending: false, hasChanges: false });
        (useDispatch as jest.Mock).mockReturnValueOnce(() => jest.fn());
        (getLocationById as jest.Mock).mockReturnValueOnce(new Error('Location is not found'));
        (isLocationOrderAheadAvailable as jest.Mock).mockReturnValueOnce(false);

        await mount(
            <HomePage
                isPreviewMode={false}
                page={page as any}
                pageSections={[]}
                featureFlags={featureFlagsMock}
                canonicalPath={''}
                hasPersonalizedContent={false}
            />
        );

        expect(routerPushMock).toHaveBeenCalledWith('/locations');
    });

    it('when no locationId in query should not try to handleLocationIdInQuery', async () => {
        const routerPushMock = jest.fn();
        jest.spyOn(nextRouter, 'useRouter').mockImplementationOnce(
            () =>
                ({
                    query: {},
                    push: routerPushMock,
                } as any)
        );

        await mount(
            <HomePage
                isPreviewMode={false}
                page={page as any}
                pageSections={[]}
                featureFlags={featureFlagsMock}
                canonicalPath={''}
                hasPersonalizedContent={false}
            />
        );

        expect(routerPushMock).not.toHaveBeenCalled();
    });

    it('should call pushLocationUnavailableError when isDigitlEnabled false', async () => {
        const routerPushMock = jest.fn();
        jest.spyOn(nextRouter, 'useRouter').mockImplementationOnce(
            () =>
                ({
                    query: { locationId: '123' },
                    push: routerPushMock,
                } as any)
        );
        const pushLocationUnavailableMock = jest.fn();
        (useLocationUnavailableError as jest.Mock).mockReturnValueOnce({
            pushLocationUnavailableError: pushLocationUnavailableMock,
        });
        const locationMockData = {
            id: 1,
            storeId: 1,
            isDigitallyEnabled: false,
            isClosed: true,
            contactDetails: {
                phone: '777-77-7777',
            },
        };
        (getLocationById as jest.Mock).mockReturnValueOnce(locationMockData);

        render(
            <HomePage
                isPreviewMode={false}
                page={page as any}
                pageSections={[]}
                featureFlags={featureFlagsMock}
                canonicalPath={''}
                hasPersonalizedContent={false}
            />
        );

        await waitFor(() => expect(pushLocationUnavailableMock).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(pushLocationUnavailableMock).toHaveBeenCalledWith(locationMockData));
    });
});
