import { act, screen, waitFor, fireEvent, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import fetchMock from 'jest-fetch-mock';
import { getPage } from '../../lib/integration/getPage';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { axe } from '../../../jest/axe-helper';
import { clear } from 'jest-date-mock';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

describe('Gift cards Page - Integration', () => {
    const getCardNumberField = (container) => within(container).getByPlaceholderText(/^Enter Card Number/i);
    const getPinField = (container) => within(container).getByPlaceholderText(/^Enter Pin/i);
    const getCheckBalanceButton = async (container) =>
        await within(container).findByRole('button', { name: /^Check gift card balance/i });

    const getValidationError = (container) => within(container).queryAllByText(/^Numeric characters only/i);
    const getTextBelowCardNumber = (container) => within(container).getByText(/^19-Digits \(No Spaces\)/i);
    const getTextBelowPin = (container) => within(container).getByText(/^4-Digits \(Enter if Present\)/i);

    const getGiftCardContainer = () => screen.getByLabelText(/Gift Card Balance Check/i);

    beforeAll(async () => {
        fetchMock.mockResponse((req) => {
            if (req.url.includes('/gift-card/balance')) {
                const cardNumber = JSON.parse(Buffer.from(req.body).toString()).cardNumber;
                if (cardNumber === '3086500000200000025') {
                    return Promise.resolve(
                        JSON.stringify({
                            balance: 10,
                        })
                    );
                } else if (cardNumber === '9999999999999999999') {
                    return Promise.resolve(
                        JSON.stringify({
                            code: 'INVALID',
                            message: 'Invalid Card or PIN',
                        })
                    );
                } else return Promise.reject({ message: 'some-message', code: 'some-code' });
            }
            return Promise.reject(new Error(`URL not mocked! - ${req.url}`));
        });

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();

        server.close();
    });

    let renderResult;

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/gift-cards',
            });

            renderResult = render().container;
        });
    });

    test('a11y', async () => {
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', () => {
        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('show all fields empty by default and check gift card balance button disabled', async () => {
        const giftCardContainer = getGiftCardContainer();

        const getBuyClassicButton = () => screen.getByRole('link', { name: /buy classic/i });
        const getBuyInBulkButton = () => screen.getByRole('link', { name: /buy in bulk/i });

        expect(getBuyClassicButton()).toBeInTheDocument();
        expect(getBuyInBulkButton()).toBeInTheDocument();
        expect(getCardNumberField(giftCardContainer)).toHaveValue('');
        expect(getPinField(giftCardContainer)).toHaveValue('');
        expect(await getCheckBalanceButton(giftCardContainer)).toBeDisabled();
        expect(getTextBelowCardNumber(giftCardContainer)).toBeInTheDocument();
        expect(getTextBelowPin(giftCardContainer)).toBeInTheDocument();
    });

    test('should disable button if gift card number starts with 30865', async () => {
        const giftCardContainer = getGiftCardContainer();
        expect(getValidationError(giftCardContainer)).toHaveLength(0);
        userEvent.type(getCardNumberField(giftCardContainer), '308650000020000010');
        expect(getPinField(giftCardContainer)).toHaveValue('');
        expect(await getCheckBalanceButton(giftCardContainer)).toBeDisabled();
    });

    test('should NOT disable button if gift card number starts with 30865', async () => {
        const giftCardContainer = getGiftCardContainer();
        expect(getValidationError(giftCardContainer)).toHaveLength(0);
        userEvent.type(getCardNumberField(giftCardContainer), '3086100000200000100');
        expect(getPinField(giftCardContainer)).toHaveValue('');
        expect(await getCheckBalanceButton(giftCardContainer)).toBeEnabled();
    });

    test('display "$10.00" card balance when successful response from service', async () => {
        const giftCardContainer = getGiftCardContainer();

        userEvent.type(getCardNumberField(giftCardContainer), '3086500000200000025');
        userEvent.type(getPinField(giftCardContainer), '5535');
        fireEvent.click(await getCheckBalanceButton(giftCardContainer));
        await waitFor(() => {
            expect(getValidationError(giftCardContainer)).toHaveLength(0);
            within(giftCardContainer).getByText(/\$10.00/i);
            within(giftCardContainer).getByText(
                /^You can pay with this gift card in any Arby’s restaurant located in the U.S./i
            );
        });
    });

    test('display "INVALID CARD OR PIN" when successful response from service w/ error"', async () => {
        const giftCardContainer = getGiftCardContainer();
        const getErrorTitle = () => within(giftCardContainer).queryByText(/^We’re sorry/i);
        const getErrorDescription = () => within(giftCardContainer).queryByText(/^Invalid Card or PIN/i);
        const getErrorDetails = () =>
            within(giftCardContainer).queryByText(/^Please try entering your gift card number and PIN again./i);
        expect(getErrorTitle()).not.toBeInTheDocument();
        expect(getErrorDescription()).not.toBeInTheDocument();
        expect(getErrorDetails()).not.toBeInTheDocument();
        userEvent.type(getCardNumberField(giftCardContainer), '9999999999999999999');
        userEvent.type(getPinField(giftCardContainer), '1111');
        fireEvent.click(await getCheckBalanceButton(giftCardContainer));
        await waitFor(() => {
            expect(getErrorTitle()).toBeInTheDocument();
        });

        expect(getErrorDescription()).toBeInTheDocument();
        expect(getErrorDetails()).toBeInTheDocument();
    });

    test('display "CARD INFORMATION NOT AVAILABLE" and hide form when fail response from service', async () => {
        const giftCardContainer = getGiftCardContainer();
        const getErrorTitle = () => within(giftCardContainer).queryByText(/OPERATION DECLINED/i);
        const getErrorDescription = () => within(giftCardContainer).queryByText(/CARD INFORMATION NOT AVAILABLE/i);
        const getErrorDetails = () =>
            within(giftCardContainer).queryByText(/Please try entering your gift card number and PIN again./i);

        expect(getErrorTitle()).not.toBeInTheDocument();
        expect(getErrorDescription()).not.toBeInTheDocument();
        expect(getErrorDetails()).not.toBeInTheDocument();

        userEvent.type(getCardNumberField(giftCardContainer), '1111111111111111111');
        fireEvent.click(await getCheckBalanceButton(giftCardContainer));

        await waitFor(() => {
            expect(getErrorTitle()).toBeInTheDocument();

            // CSS not loaded, thus not working
            // https://stackoverflow.com/questions/52813527/cannot-check-expectelm-not-tobevisible-for-semantic-ui-react-component
            // expect(within(giftCardContainer).queryByPlaceholderText(/^Enter Card Number/i)).not.toBeVisible();
            // expect(within(giftCardContainer).queryByPlaceholderText(/^Enter Pin/i)).not.toBeVisible();

            // expect(within(giftCardContainer).queryByPlaceholderText(/^19-Digits \(No Spaces\)/i)).not.toBeVisible();
            // expect(
            //     within(giftCardContainer).queryByPlaceholderText(/^4-Digits \(Enter if Present\)/i)
            // ).not.toBeVisible();
        });

        expect(getErrorDescription()).toBeInTheDocument();
        expect(getErrorDetails()).toBeInTheDocument();
    });
});
