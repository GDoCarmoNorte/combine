export const globalPropsMock: any = {
    navigation: {
        items: [
            {
                fields: { name: 'Web', logo: { fields: { file: { url: 'logo.png' } } } },
            },
        ],
    },
};

export const orderMock = {
    posOrderId: '111275-20005-09302021-chf284',
    isOpen: true,
    dateTime: '2021-09-30T07:13:07.465510Z',
    table: '123',
    serverName: 'Mark',
    guestsCount: 0,
    subTotalBeforeDiscounts: 11.78,
    subTotalAfterDiscounts: 11.78,
    tax: 0.83,
    total: 0,
    tip: 0,
    otherDiscount: 0,
    certificateDiscount: 0,
    isLoyaltyMember: false,
    products: [
        { description: '10 WINGS', price: 8.49, quantity: 1 },
        { description: '20 OZ DRINK', price: 3.29, quantity: 1 },
    ],
    payments: [
        { type: 'CREDIT', lastFourDigits: '1111' },
        { type: 'CREDIT', lastFourDigits: '1111' },
    ],
    location: {
        id: '13',
        displayName: 'Buzztime QA 6.7',
        contactDetails: {
            type: 'STORE',
            address: {
                line1: '5500 Wayzata Blvd.',
                line2: 'Ste 13',
                line3: 'Some Mall',
                postalCode: '55416',
                cityName: 'Minneapolis',
                stateProvinceCode: 'MN',
                countryName: 'United States',
                countryCode: 'US',
            },
            email: 'asathe@inspirebrands.com',
            phone: '612-866-9316',
            fax: '3242',
        },
    },
};
