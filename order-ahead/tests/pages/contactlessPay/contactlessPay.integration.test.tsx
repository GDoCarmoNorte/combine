import { screen, waitFor, within } from '@testing-library/react';
import { axe } from '../../../jest/axe-helper';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { clear } from 'jest-date-mock';
import { orderMock } from './contactless.mock';
import { getDineInOrderDetails } from '../../../common/services/orderService/dineInService';
import { isDineInOrdersEnabled, isGiftCardPayEnabled } from '../../../lib/getFeatureFlags';
import { getPage } from '../../lib/integration/getPage';
import { usePayment } from '../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../common/hooks/useFreedomPay';
import { injectScript } from '../../../lib/injectScriptOnce';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
import { errorTexts } from '../../../components/clientOnly/contactlessPayError/constants';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isDineInOrdersEnabled: jest.fn().mockReturnValue(true),
        isGiftCardPayEnabled: jest.fn().mockReturnValue(true),
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isCustomerPaymentMethodEnabled: jest.fn().mockReturnValue(true),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isFraudCheckEnabled: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../common/services/orderService/dineInService', () => ({
    getDineInOrderDetails: jest.fn(),
}));
jest.mock('../../../common/hooks/usePayment');
jest.mock('../../../common/hooks/useFreedomPay');
jest.mock('../../../lib/injectScriptOnce');
jest.mock('../../../common/hooks/useTips', () => ({
    useTips: jest.fn(() => ({
        tipsItems: [
            { amount: 1, percentage: 10 },
            { amount: 2, percentage: 15 },
            { amount: 3, percentage: 20 },
        ],
        titleText: 'Add tip:',
        isError: false,
        errorMessage: '',
        defaultTipsId: 1,
    })),
}));

jest.mock('../../../redux/hooks/useOrderLocation', () => () => ({
    pickupAddress: jest.fn(),
    deliveryAddress: jest.fn(),
    isPickUp: jest.fn(),
    actions: {
        setPickupLocation: jest.fn(),
        setDeliveryLocation: jest.fn(),
        setPreviousLocation: jest.fn(),
        flushDeliveryLocation: jest.fn(),
    },
}));

const injectScriptMock = jest.fn();

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
        getNationalMenu: jest.fn().mockResolvedValue(undefined),
    };
});

const getContactlessPageContainer = () => screen.getByLabelText(/Contactless Page/i);
const getContactlessPayHeader = () => screen.getByLabelText(/Contactless Header/i);

describe('Contactless Pay - integration', () => {
    let clearNextHeadMock;

    const renderComponent = async (route = '/contactless-pay?locationId=13&orderId=1234') => {
        const { render } = await getPage({ route });

        return render();
    };

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });
    (getDineInOrderDetails as jest.Mock).mockResolvedValue(orderMock);
    (usePayment as jest.Mock).mockReturnValue({
        payment: {
            sessionKey: 'sessionKey',
            iframePayload: 'iframePayload',
            iframeHtml: '<div>CC payment section</div>',
        },
    });
    (useFreedomPay as jest.Mock).mockReturnValue({ height: 460, errors: [], isValid: false, payment: null });
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    (injectScript as jest.Mock) = injectScriptMock;

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    describe('when contactless pay page is enabled', () => {
        test('a11y', async () => {
            const renderResult = (await renderComponent()).container;
            const results = await axe(renderResult);
            expect(results).toHaveNoViolations();
        });

        test('display order data', async () => {
            await renderComponent();
            const pageHeader = getContactlessPayHeader();

            await waitFor(() => {
                within(pageHeader).getByText(/Location/i);
            });

            within(pageHeader).getByText(/Buzztime QA 6.7/i);

            const pageContainer = getContactlessPageContainer();

            within(pageContainer).getByText(/Dining info/i);
            within(pageContainer).getByText(/Server:/i);
            within(pageContainer).getByText(/Mark/i);
            within(pageContainer).getByText(/Table:/i);
            within(pageContainer).getByText(/123/i);
            within(pageContainer).getByText(/Guests:/i);
            within(pageContainer).getByText(/^0$/i);
            within(pageContainer).getByText(/Order type:/i);
            within(pageContainer).getByText(/Dine In/i);
            within(pageContainer).getByText(/Order number:/i);
            within(pageContainer).getByText(/111275-20005-09302021-chf284/i);

            within(pageContainer).getByText(/Order summary/i);
            within(pageContainer).getByText(/10 WINGS/i);
            within(pageContainer).getByText(/8.49/i);
            within(pageContainer).getByText(/20 OZ DRINK/i);
            within(pageContainer).getByText(/3.29/i);

            within(pageContainer).getByText(/Subtotal/i);
            within(pageContainer).getByText(/\$11.78/i);
            within(pageContainer).getByText('Tip (15%)');
            within(pageContainer).getByText(/\$2.00/i);
            within(pageContainer).getByText(/Tax/i);
            within(pageContainer).getByText(/\$0.83/i);
            within(pageContainer).getByText(/^Total$/i);
            within(pageContainer).getByText(/\$14.61/i);
        });

        test('render cc payment iframe', async () => {
            await renderComponent();

            await waitFor(() => {
                expect(screen.getByText(/Payment info/i)).toBeInTheDocument();
            });
            expect(screen.getByText(/CC payment section/i)).toBeInTheDocument();
        });

        test('render tip section', async () => {
            await renderComponent();

            await waitFor(() => {
                expect(screen.getByText(/Add tip:/i)).toBeInTheDocument();
            });
            expect(screen.getByText(/Add tip:/i)).toBeInTheDocument();
            expect(screen.getByText(/10%/i)).toBeInTheDocument();
            expect(screen.getByText('15%')).toBeInTheDocument();
            expect(screen.getByText(/20%/i)).toBeInTheDocument();
            expect(screen.getByText(/Custom/i)).toBeInTheDocument();
        });

        test('render gc section', async () => {
            await renderComponent();

            await waitFor(() => {
                expect(screen.getByText(/Pay with gift card/i)).toBeInTheDocument();
            });
            expect(screen.getByText(/Max of 2/i)).toBeInTheDocument();
        });
    });

    describe('when contactless page is confirmation page', () => {
        test('should show contactless confirmation page', async () => {
            (getDineInOrderDetails as jest.Mock).mockResolvedValueOnce({ ...orderMock, isOpen: false });

            await renderComponent('/contactless-pay/confirmation?locationId=13&orderId=1234');

            await waitFor(() => {
                expect(screen.getByText(/payment received/i)).toBeInTheDocument();
            });
            expect(screen.getByText(/^thank you$/i)).toBeInTheDocument();
            expect(screen.getByText(/Confirmation #1234/i)).toBeInTheDocument();

            expect(screen.getByText(/Card Number/i)).toBeInTheDocument();
            expect(screen.getByText(/\*\*\*\*\*\*\*\*\*\*\*\*1111/i)).toBeInTheDocument();
            expect(screen.getByText(/THANK YOU AND WE’LL SEE YOU NEXT TIME/i)).toBeInTheDocument();

            expect(screen.queryByLabelText(/Payment info/i)).not.toBeInTheDocument();
            expect(screen.queryByLabelText(/CC payment section/i)).not.toBeInTheDocument();
        });
    });

    describe('when gift card payment is disabled', () => {
        test('should hide gift card section', async () => {
            (isGiftCardPayEnabled as jest.Mock).mockReturnValue(false);

            await renderComponent();

            await waitFor(() => {
                expect(() => screen.getByText(/Pay with gift card/i)).toThrowError();
            });
            expect(() => screen.getByText(/Max of 2/i)).toThrowError();
        });
    });

    describe('when no payment required', () => {
        test('should show no payment page', async () => {
            (getDineInOrderDetails as jest.Mock).mockResolvedValueOnce({
                ...orderMock,
                subTotalAfterDiscounts: 0,
                tax: 0,
                certificateDiscount: 11.78,
            });

            await renderComponent();

            await waitFor(() => {
                screen.getByText(/dining info/i);
            });
            screen.getByText(/dining info/i);
            screen.getByText(/order summary/i);
            screen.getByText(/reward certificate\(s\)/i);
            screen.getByText('-$11.78');
            screen.getByText(/no payment required/i);

            expect(screen.queryByLabelText(/tip/i)).not.toBeInTheDocument();
            expect(screen.queryByLabelText(/payment info/i)).not.toBeInTheDocument();
        });

        test('should show "Order Error" page if code is "CHECK_NOT_FOUND"', async () => {
            (getDineInOrderDetails as jest.Mock).mockRejectedValueOnce({
                code: 'CHECK_NOT_FOUND',
            });
            const getErrorText = errorTexts.default;

            await renderComponent();

            await waitFor(() => {
                expect(screen.getByText(getErrorText.header)).toBeInTheDocument();
            });
            expect(screen.getByText(getErrorText.subheader)).toBeInTheDocument();
            expect(screen.getByText(getErrorText.description)).toBeInTheDocument();

            expect(screen.queryByText(/dining info/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/order summary/i)).not.toBeInTheDocument();
        });

        test('should show "Order Error" page if code is "GENERIC"', async () => {
            (getDineInOrderDetails as jest.Mock).mockRejectedValueOnce({ code: 'GENERIC' });
            const getErrorText = errorTexts.disabled;

            await renderComponent();

            await waitFor(() => {
                expect(screen.getByText(getErrorText.header)).toBeInTheDocument();
            });
            expect(screen.getByText(getErrorText.subheader)).toBeInTheDocument();
            expect(screen.getByText(getErrorText.description)).toBeInTheDocument();

            expect(screen.queryByText(/dining info/i)).not.toBeInTheDocument();
            expect(screen.queryByText(/order summary/i)).not.toBeInTheDocument();
        });
    });

    describe('when contactless page is disabled', () => {
        test('should hide contactless page and redirect to 404', async () => {
            (isDineInOrdersEnabled as jest.Mock).mockReturnValue(false);

            await renderComponent();

            await waitFor(() => {
                expect(screen.queryByLabelText(/Contactless Page/i)).not.toBeInTheDocument();
            });
            screen.getByText('Uh Oh');
            screen.getByText('Service Not Available');
            screen.getByText('Please Pay Using Terminal or Ask a Restaurant Staff');
        });
    });
});
