import path from 'path';
import { ReactElement } from 'react';
import ReactDOMServer from 'react-dom/server';

export const PAGE_RENDER_TIMEOUT = 90 * 1000; // 90 seconds

export const getNextRoot = (): string => {
    return process.cwd().split(path.sep).join(path.posix.sep);
};

type ClearFn = () => void;

export const mockNextHead = (): ClearFn => {
    jest.mock('next/head', () => {
        return {
            __esModule: true,
            default: ({ children }: { children: ReactElement }) => {
                if (children && global.document) {
                    global.document.head.insertAdjacentHTML(
                        'afterbegin',
                        ReactDOMServer.renderToString(children) || ''
                    );
                }

                return null;
            },
        };
    });

    return (): void => {
        jest.unmock('next/head');
    };
};
