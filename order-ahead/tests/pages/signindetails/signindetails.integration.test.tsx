/* eslint-disable jest/valid-expect */
import { act, screen, waitFor } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { contentfulHandlers, server } from '../../mock-data/server/mockServer';
import { clear } from 'jest-date-mock';
import userEvent from '@testing-library/user-event';
import TestUtils from 'react-dom/test-utils';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';

// Open issue about testing components with userEvent and react-text-mask
// https://github.com/sanniassin/react-input-mask/issues/174#issuecomment-743882703
function changeInputMaskValue(element, value) {
    // eslint-disable-next-line no-param-reassign
    element.value = value;
    // eslint-disable-next-line no-param-reassign
    element.selectionStart = element.selectionEnd = value.length;
    TestUtils.Simulate.change(element);
}

jest.mock('../../../lib/jwt', () => ({
    verifyJWT: jest.fn().mockReturnValue('12345'),
}));

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());
jest.unmock('../../../redux/hooks/useConfiguration');

describe('signindetails page', () => {
    let clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/signindetails?token=12345',
            });

            render().container;
        });
    });

    it('should display navigation with link to main page', async () => {
        await waitFor(() => {
            expect(screen.getByRole('link', { name: /Main Page/i })).toHaveAttribute('href', 'http://www.test.app.url');
        });
    });

    it('should display heading and subheading', () => {
        screen.getByRole('heading', { name: /Hello!/i });
        screen.getByRole('heading', { name: /COME AND GET YOUR MEATS/i });
        screen.getByRole('heading', { name: /Please provide additional info to complete your sign-up/i });
    });

    it('should display info text', () => {
        expect(screen.getByText(/We are missing your email and date of birth to send you deals/i));
        expect(screen.getByText(/Don’t worry, we wont tell anyone how old are you/i));
    });

    it('should display email, date of birth and marketing opt in fields', () => {
        expect(screen.getByLabelText(/Email/));
        expect(screen.getByLabelText(/Date Of Birth/i));
        expect(screen.getByLabelText(/I would like to receive email communication from Arby's/i));
    });

    it('should have submit button disabled initially', async () => {
        expect(screen.getByRole('button', { name: /SAVE/i })).toHaveAttribute('disabled');
    });

    it('should display error when nothing is typed in email field', async () => {
        userEvent.type(screen.getByLabelText(/Email/), '');
        userEvent.click(screen.getByLabelText(/Date Of Birth/));

        await waitFor(() => {
            expect(screen.getByText(/Email is incomplete/i));
        });
    });

    it('should display error when invalid email is typed in email field', async () => {
        userEvent.type(screen.getByLabelText(/Email/), '12345');
        userEvent.click(screen.getByLabelText(/Date Of Birth/i));

        await waitFor(() => {
            expect(screen.getByText(/Incorrect email/i));
        });
    });

    it('should display error when invalid DOB is typed in DOB field', async () => {
        userEvent.type(screen.getByLabelText(/Date Of Birth/i), '00/00');
        changeInputMaskValue(screen.getByLabelText(/Date Of Birth/i), '00/00');
        userEvent.click(screen.getByLabelText(/Email/));

        await waitFor(() => {
            expect(screen.getByText(/Incorrect format of Date Of Birth/i));
        });
    });
});
