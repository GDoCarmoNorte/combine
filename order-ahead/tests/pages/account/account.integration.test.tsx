import { act, screen, within, waitFor } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';

import { isAccountOn, isCustomerPaymentMethodEnabled } from '../../../lib/getFeatureFlags';
import { useCustomerPaymentMethod } from '../../../common/hooks/useCustomerPaymentMethod';
import { clear } from 'jest-date-mock';
import accountMock from './account.mock';
import userEvent from '@testing-library/user-event';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';

import { TErrorCodeModel } from '../../../@generated/webExpApi';
import { RequestError } from '../../../common/services/createErrorWrapper';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../common/hooks/useCustomerPaymentMethod');

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/accountPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/accountPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.location),
    };
});

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isCustomerPaymentMethodEnabled: jest.fn().mockReturnValue(true),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
    };
});

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

const getAccountMock = jest.fn();
const updateAccountMock = jest.fn(() => ({ idpCustomerId: 'id' }));

jest.mock('../../../common/services/customerService/account', () => {
    return {
        __esModule: true,
        initAccountService: jest.fn().mockReturnValue({
            getAccount: getAccountMock,
            updateAccount: updateAccountMock,
        }),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const getAccountPageContainer = () => screen.getByLabelText(/Account Page/i);
const getFirstNameField = () => screen.getByPlaceholderText(/Enter First Name/i);
const getLastNameField = () => screen.getByPlaceholderText(/Enter Last Name/i);
const getEmailField = () => screen.getByPlaceholderText(/Enter Email/i);
const getPhoneField = () => screen.getByLabelText(/Phone */i);
const getBirthDateField = () => screen.getByPlaceholderText(/MM\/DD/i);
const getSaveButton = () => screen.getByText(/SAVE/i);
const getLogoutButton = () => screen.getByText(/LOGOUT/i);
const getDealsEmailCheckbox = () => screen.getByRole('checkbox');
const getModalContainer = () => screen.getByRole('presentation');
const getModalCloseButton = () => screen.getByRole('button', { name: 'No, return to my accounts' });
const getSubmitButton = () => screen.getByRole('button', { name: /submit/i });
const getErrorMessage = () => screen.getByText(/We are experiencing technical difficulties. Please try again later./i);
const getPhoneErrorMessage = () =>
    screen.getByText(/Phone number already in use. Please change the number and try again./i);
const getUnsubscribedMessage = () => screen.getByText(/You are successfully unsubscribed!/i);

describe('Account - integration', () => {
    let clearNextHeadMock, renderResult;
    beforeAll(async () => {
        jest.mock('../../../redux/localStorage', () => {
            return {
                __esModule: true,
                loadState: jest.fn().mockReturnValue({
                    account: accountMock,
                }),
                saveState: jest.fn(),
            };
        });

        (useCustomerPaymentMethod as jest.Mock).mockReturnValue({
            __esModule: true,
            paymentMethods: [],
            loading: false,
        });

        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    describe('when account page is enabled', () => {
        beforeEach(async () => {
            await act(async () => {
                const { render } = await getPage({
                    route: '/account',
                });

                renderResult = render().container;
            });
        });

        test('a11y', async () => {
            const results = await axe(renderResult);
            expect(results).toHaveNoViolations();
        });

        test('display terms and conditions and privacy policy links', async () => {
            const accountPageContainer = getAccountPageContainer();

            expect(within(accountPageContainer).getByText(/terms and conditions/i)).toBeInTheDocument();
            expect(within(accountPageContainer).getByText(/privacy policy/i)).toBeInTheDocument();
        });

        test('display logout button', async () => {
            const logoutButtonContainer = getLogoutButton();
            expect(within(logoutButtonContainer).getByText(/LOGOUT/i)).toBeInTheDocument();
        });

        test('subscribe/unsubscribe checkbox should be in document', () => {
            const dealsEmailCheckbox = getDealsEmailCheckbox();
            expect(dealsEmailCheckbox).toBeInTheDocument();
            expect(dealsEmailCheckbox).toHaveClass('checkboxSelected');
        });

        test('when clicking on subscribe/unsubscribe checkbox is should open modal/tooltip when checkbox is selected', async () => {
            const dealsEmailCheckbox = getDealsEmailCheckbox();
            expect(dealsEmailCheckbox).toBeInTheDocument();
            userEvent.click(dealsEmailCheckbox);
            const modalContainer = getModalContainer();
            const closeButton = getModalCloseButton();
            expect(modalContainer).toBeInTheDocument();
            userEvent.click(closeButton);
            expect(modalContainer).not.toBeInTheDocument();
        });

        test('should show success popup/notifiction in case if user successfully subscribed/unsubscribed', async () => {
            const dealsEmailCheckbox = getDealsEmailCheckbox();
            userEvent.click(dealsEmailCheckbox);
            const modalContainer = getModalContainer();
            const submitButton = getSubmitButton();

            expect(screen.getByText(/Unsubscribe me from your email list/i)).toBeInTheDocument();
            userEvent.click(submitButton);
            await act(() => {
                (getAccountMock as jest.Mock).mockReturnValueOnce({ idpCustomerId: 1 });
            });
            expect(getUnsubscribedMessage()).toBeInTheDocument();
            expect(modalContainer).not.toBeInTheDocument();
        });

        test('should show timeout error when  subscribne/unsubsribe is throwing  timeout error', async () => {
            const dealsEmailCheckbox = getDealsEmailCheckbox();
            userEvent.click(dealsEmailCheckbox);
            const modalContainer = getModalContainer();
            const submitButton = getSubmitButton();
            userEvent.click(submitButton);
            const errMsg = 'Request Timeout';
            await getAccountMock.mockImplementation(() => {
                throw new Error(errMsg);
            });
            expect(screen.getByText(errMsg)).toBeInTheDocument();
            expect(modalContainer).not.toBeInTheDocument();
        });

        test('should show error popup/notifiction in case if error accrued', async () => {
            const dealsEmailCheckbox = getDealsEmailCheckbox();
            userEvent.click(dealsEmailCheckbox);
            const modalContainer = getModalContainer();
            const submitButton = getSubmitButton();
            userEvent.click(submitButton);
            const errMsg = 'We are experiencing technical difficulties. Please try again later.';
            await getAccountMock.mockImplementation(() => {
                throw new Error(errMsg);
            });

            expect(getErrorMessage()).toBeInTheDocument();
            expect(modalContainer).not.toBeInTheDocument();
        });

        test('fields should be typed correctly', async () => {
            expect(getFirstNameField()).toHaveValue('John');
            expect(getLastNameField()).toHaveValue('Doe');
            expect(getEmailField()).toHaveValue('john.doe@gmail34.com');
            expect(getPhoneField()).toHaveValue('(123) 123-1234');
            expect(getBirthDateField()).toHaveValue('12/31');
        });

        test('validation error messages should be displayed when fields are not filled and save button should be disabled', async () => {
            const getFirstNameFieldValidationError = () => screen.queryByText(/^First Name is incomplete/i);
            const getLastNameFieldValidationError = () => screen.queryByText(/^Last Name is incomplete/i);
            const getPhoneFieldValidationError = () => screen.queryByText(/^Phone number is incomplete/i);

            expect(getFirstNameFieldValidationError()).not.toBeInTheDocument();
            userEvent.clear(getFirstNameField());
            getFirstNameField().focus();
            userEvent.tab();
            await waitFor(() => expect(getFirstNameFieldValidationError()).toBeInTheDocument());

            expect(getLastNameFieldValidationError()).not.toBeInTheDocument();
            userEvent.clear(getLastNameField());
            getLastNameField().focus();
            userEvent.tab();
            await waitFor(() => expect(getLastNameFieldValidationError()).toBeInTheDocument());

            expect(getPhoneFieldValidationError()).not.toBeInTheDocument();
            userEvent.clear(getPhoneField());
            getPhoneField().focus();
            userEvent.tab();
            await waitFor(() => expect(getPhoneFieldValidationError()).toBeInTheDocument());

            expect(getSaveButton()).toBeDisabled();
        });

        test('validation error messages should be displayed when fields are filled incorrect and save button should be disabled', async () => {
            const getAplphacaracterValidationError = () => screen.queryByText(/^Alpha characters only/i);

            expect(getAplphacaracterValidationError()).not.toBeInTheDocument();
            userEvent.clear(getFirstNameField());
            userEvent.type(getFirstNameField(), '@#!$!@DFD');

            getFirstNameField().focus();

            userEvent.tab();

            await waitFor(() => expect(getAplphacaracterValidationError()).toBeInTheDocument());
            userEvent.type(getFirstNameField(), 'test');

            userEvent.clear(getLastNameField());
            userEvent.type(getLastNameField(), '@#123213');
            getLastNameField().focus();
            userEvent.tab();
            await waitFor(() => expect(getAplphacaracterValidationError()).toBeInTheDocument());

            expect(getSaveButton()).toBeDisabled();
        });

        test('email and birthday fields should be disabled by default', async () => {
            expect(getBirthDateField()).toBeDisabled();
            expect(getEmailField()).toBeDisabled();
        });

        test('should display error message when error code PhoneNumberAlreadyUsed', async () => {
            userEvent.type(getFirstNameField(), 'test firstname');
            userEvent.type(getLastNameField(), 'test firstname');
            userEvent.type(getPhoneField(), '(999) 999-9999');
            userEvent.type(getEmailField(), 'np6048@yopmail.com');
            userEvent.type(getBirthDateField(), '05/06/1993');

            const saveButton = getSaveButton();
            userEvent.click(saveButton);
            updateAccountMock.mockImplementation(() => {
                throw new RequestError(
                    500,
                    'Phone number already in use. Please change the number and try again.',
                    TErrorCodeModel.PhoneNumberAlreadyUsed
                );
            });
            await act(async () => {
                expect(screen.getByRole('alert')).toBeInTheDocument();
            });
            await act(async () => {
                expect(getPhoneErrorMessage()).toBeInTheDocument();
            });
        });
    });

    describe('Payment methods section display logic', () => {
        test('should display payment section when enabled', async () => {
            (isCustomerPaymentMethodEnabled as jest.Mock).mockImplementation(() => true);

            await act(async () => {
                const { render } = await getPage({
                    route: '/account',
                });

                renderResult = render().container;
            });

            const getPaymnentMethodsTitle = () => screen.queryByText(/^Payment methods/i);

            expect(getPaymnentMethodsTitle()).toBeInTheDocument();
        });

        test('should not display payment section when disabled', async () => {
            (isCustomerPaymentMethodEnabled as jest.Mock).mockImplementation(() => false);

            await act(async () => {
                const { render } = await getPage({
                    route: '/account',
                });

                renderResult = render().container;
            });

            const getPaymnentMethodsTitle = () => screen.queryByText(/^Payment methods/i);

            expect(getPaymnentMethodsTitle()).not.toBeInTheDocument();
        });
    });

    describe('when account page is disabled', () => {
        beforeAll(() => {
            delete window.location;
            (window.location as any) = { replace: jest.fn() };
        });

        afterAll(() => {
            window.location = location;
        });

        test('should hide account page and redirect to 404', async () => {
            (isAccountOn as jest.Mock).mockReturnValue(false);

            await act(async () => {
                const { render } = await getPage({
                    route: '/account',
                });

                renderResult = render().container;
            });

            expect(window.location.replace).toHaveBeenCalledWith('/404');
            expect(screen.queryByLabelText(/Account Page Content/i)).not.toBeInTheDocument();
        });
    });
});
