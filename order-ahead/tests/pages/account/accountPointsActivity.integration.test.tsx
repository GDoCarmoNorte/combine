import { act, screen, waitFor } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';
import { contentfulHandlers, server } from '../../mock-data/server/mockServer';
import { clear } from 'jest-date-mock';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import rewardsMock from '../../mock-data/provider/rewards.mock.json';
import { useRewards, useGlobalProps } from '../../../redux/hooks';
import { isRewardsOn } from '../../../lib/getFeatureFlags';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());
jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../redux/hooks/useRewards');

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
        getNationalMenu: jest.fn().mockResolvedValue({}),
    };
});

describe('Account Points Activity Page - integration', () => {
    let clearNextHeadMock, renderResult;
    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    describe('Account Points Activity Page is enabled', () => {
        const renderPage = async () =>
            await act(async () => {
                const { render } = await getPage({
                    route: '/account/activity',
                });

                renderResult = render().container;
            });

        beforeAll(() => {
            (useRewards as jest.Mock).mockReturnValue({
                rewardsActivityHistory: rewardsMock.rewardsActivityHistory,
                rewardsActivityHistoryLoading: false,
                actions: {
                    setRewardsActivityHistory: jest.fn(),
                    setRewardsActivityHistoryLoading: jest.fn(),
                },
            });
        });

        test('a11y', async () => {
            await renderPage();

            const results = await axe(renderResult);
            expect(results).toHaveNoViolations();
        });

        test('Should contain POINTS ACTIVITY heading', async () => {
            await renderPage();

            expect(screen.getByRole(/heading/i, { name: /points activity/i })).toBeInTheDocument();
        });
    });

    describe('when account points activity page is disabled', () => {
        const routerReplaceMock = jest.fn();
        jest.mock('next/router', () => ({
            //@ts-ignore
            ...jest.requireActual('next/router'),
            default: {
                //@ts-ignore
                ...jest.requireActual('next/router').default,
                replace: routerReplaceMock,
            },
            useRouter: () => ({
                push: jest.fn(),
                replace: routerReplaceMock,
                route: '/',
                pathname: '',
                query: '',
                asPath: '',
            }),
        }));

        test('should hide account page and redirect to 404', async () => {
            (isRewardsOn as jest.Mock).mockReturnValue(false);

            const { render } = await getPage({
                route: '/account/activity',
            });

            renderResult = render();

            await waitFor(() => {
                expect(routerReplaceMock).toHaveBeenCalledWith('/404');
            });
        });
    });
});
