import React from 'react';
import { shallow } from 'enzyme';

import DealPage from '../../../pages/account/deals/deal';
import { useLocationOrderAheadAvailability } from '../../../common/hooks/useLocationOrderAheadAvailability';
import { useAuth0 } from '@auth0/auth0-react';
import useRewards from '../../../redux/hooks/useRewards';
import useBag from '../../../redux/hooks/useBag';
import { useRouter } from 'next/router';

jest.mock('../../../common/hooks/useLocationOrderAheadAvailability');
jest.mock('../../../redux/hooks/useGlobalProps', () =>
    jest.fn().mockReturnValue({
        dealItemsById: { default: { fields: { image: 'img.src' } } },
    })
);

jest.mock('../../../common/hooks/useDeal', () =>
    jest.fn().mockReturnValue({
        getDealImage: jest.fn().mockReturnValue({ fields: { image: 'img.src' } }),
    })
);
jest.mock('next/router');
jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,
        // @ts-ignore
        ...jest.requireActual('@auth0/auth0-react'),
        useAuth0: jest.fn(),
        withAuthenticationRequired: jest.fn().mockImplementation((component) => component),
    };
});
jest.mock('../../../redux/hooks/useRewards');
jest.mock('../../../redux/hooks/useBag');

const accountSectionsMock = {
    accountHeader: {} as any,
};

describe('ProductDetailPage', () => {
    it('should match snapshot, loading', () => {
        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({});
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: true });
        (useRewards as jest.Mock).mockReturnValue({ getOfferById: jest.fn(), isLoading: true, offers: [] });
        (useBag as jest.Mock).mockReturnValue({
            actions: { updateTooltip: jest.fn(), addToBag: jest.fn(), toggleIsOpen: jest.fn() },
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot, redeem online and in store', () => {
        const offerMock = {
            isRedeemableOnlineOnly: false,
            isRedeemableInStoreOnly: false,
            endDateTime: '2021-07-07T12:54:37.254+00:00',
        };

        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({
            location: {},
            isLocationOrderAheadAvailable: true,
        });
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: false });
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(offerMock),
            isLoading: false,
            offers: [],
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot, redeem online only', () => {
        const offerMock = {
            isRedeemableOnlineOnly: true,
            isRedeemableInStoreOnly: false,
            endDateTime: '2021-07-07T12:54:37.254+00:00',
        };

        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({
            location: {},
            isLocationOrderAheadAvailable: true,
        });
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: false });
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(offerMock),
            isLoading: false,
            offers: [],
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot, redeem in store only', () => {
        const offerMock = {
            isRedeemableOnlineOnly: false,
            isRedeemableInStoreOnly: true,
            endDateTime: '2021-07-07T12:54:37.254+00:00',
        };

        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({
            location: {},
            isLocationOrderAheadAvailable: true,
        });
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: false });
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(offerMock),
            isLoading: false,
            offers: [],
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should match snapshot, cta select location', () => {
        const offerMock = {
            isRedeemableOnlineOnly: false,
            isRedeemableInStoreOnly: false,
            endDateTime: '2021-07-07T12:54:37.254+00:00',
        };

        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({});
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: false });
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(offerMock),
            isLoading: false,
            offers: [],
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);

        expect(wrapper).toMatchSnapshot();
    });

    it('should navigate to select location on select location click', () => {
        const offerMock = {
            isRedeemableOnlineOnly: false,
            isRedeemableInStoreOnly: false,
            endDateTime: '2021-07-07T12:54:37.254+00:00',
        };
        const routerPushMock = jest.fn();
        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
            push: routerPushMock,
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({});
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: false });
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(offerMock),
            isLoading: false,
            offers: [],
        });
        (useBag as jest.Mock).mockReturnValue({
            actions: { updateTooltip: jest.fn(), addToBag: jest.fn(), toggleIsOpen: jest.fn() },
            deal: {
                dealID: '1231',
            },
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);
        const deal = wrapper.find('[primaryCtaText="Select a location"]') as any;

        deal.props().onPrimaryCtaClick({ type: 'type', id: 'id' });

        expect(routerPushMock).toHaveBeenCalledWith('/locations');
    });

    it('should navigate to locations when location is selected but not OA Available', () => {
        const offerMock = {
            isRedeemableOnlineOnly: false,
            isRedeemableInStoreOnly: false,
            endDateTime: '2021-07-07T12:54:37.254+00:00',
        };
        const routerPushMock = jest.fn();
        (useRouter as jest.Mock).mockReturnValue({
            query: { id: '1234-5678' },
            push: routerPushMock,
        });
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue({
            location: true,
            isLocationOrderAheadAvailable: false,
        });
        (useAuth0 as jest.Mock).mockReturnValue({ isLoading: false });
        (useRewards as jest.Mock).mockReturnValue({
            getOfferById: jest.fn().mockReturnValue(offerMock),
            isLoading: false,
            offers: [],
        });
        (useBag as jest.Mock).mockReturnValue({
            actions: { updateTooltip: jest.fn(), addToBag: jest.fn(), toggleIsOpen: jest.fn() },
            deal: {
                dealID: '1231',
            },
        });

        const wrapper = shallow(<DealPage accountSections={accountSectionsMock} />);
        const deal = wrapper.find('[primaryCtaText="Select a location"]') as any;

        deal.props().onPrimaryCtaClick({ type: 'type', id: 'id' });

        expect(routerPushMock).toHaveBeenCalledWith('/locations');
    });
});
