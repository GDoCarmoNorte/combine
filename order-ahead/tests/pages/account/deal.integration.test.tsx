import { act, screen, waitFor, within } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';

import { clear } from 'jest-date-mock';
import userEvent from '@testing-library/user-event';

global.URL.createObjectURL = jest.fn().mockImplementation((data) => data);
jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../common/hooks/useDealsService', () => {
    return {
        useDealsService: jest.fn().mockReturnValue({
            getQRCodeForOfferApi: jest
                .fn()
                .mockResolvedValue(
                    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXIAAAFyCAAAAADNTw7VAAADlElEQVR42u3bQXLDIBAEQP//08kHXC6WHRYn6jk6kQyNLiPw60eG80KAHLkgRy7IkQty5MgFOXJBjlyQIxfkyJELcuSCHLkgR45ckCMX5MgFOXJBjhy5IEcuyJFLi/wVSf1+7674/Nnn0U/MDTly5MiRI0eOHDly5MiR98k3Km4Zq8Nbv0t6bsiRI0eOHDly5MiRI0eOPEneqeqrxTs99foyZOaGHDly5MiRI0eOHDly5MhvkE9sVWeWATly5MiRI0eOHDly5MiR/4fCn1muzusA5MiRI0eOHDly5MiRI0f+zeQT16bZ7s8NOXLkyJEjR44cOXLkyJGv3LqTVaJbn2Xmhhw5cuTIkSNHjhw5cuTId8nTyZTxDuC1mSNHjhw5cuTIkSNHjhw58gWiVbZ69c8cK+4ct87MHDly5MiRI0eOHDly5MiR98nTQ6h/78Y0G1vfyJEjR44cOXLkyJEjR458jnwC69xf79d85MiRI0eOHDly5MiRI0d+ZoATy9Cp+XXo0FIjR44cOXLkyJEjR44cOfIj5BNHjc/xprfSkSNHjhw5cuTIkSNHjhx5MvXyvDrNztHlzouBzoyQI0eOHDly5MiRI0eOHPlpyvTPWydeDHzVT22RI0eOHDly5MiRI0eO/DHkmZJ9rvCfq/5fcNgZOXLkyJEjR44cOXLkyB9IninZ6XLfWcJMkUeOHDly5MiRI0eOHDly5H3ydD3OLEhnaSZeXCBHjhw5cuTIkSNHjhw58mThz0BnXiVksDrzRY4cOXLkyJEjR44cOXLkycKf3tjNbAVnNrKRI0eOHDly5MiRI0eOHPld8vqwOldkvrc+vnMbz8iRI0eOHDly5MiRI0eOfC6Z4Xc2qOv36/wfcuTIkSNHjhw5cuTIkSPfrfmZ8pzmSBf5zoOAHDly5MiRI0eOHDly5Mh3ySfKeJ2t/sBkrhgp/MiRI0eOHDly5MiRI0f+aPJOoc4sdX1BZpcaOXLkyJEjR44cOXLkyJHfJc8sV2eBM3f+Q4UfOXLkyJEjR44cOXLkyB9Nfm6LN13VO2NBjhw5cuTIkSNHjhw5cuRnbt2p1qtsndI+sejIkSNHjhw5cuTIkSNHjnyXPHNIOLOdmz7inHlMkCNHjhw5cuTIkSNHjhz5Lrmkghw5ckGOXJAjF+TIkQty5IIcuSBHLsiRIxfkyAU5ckGOHLkgRy7IkQty5IIcOXJBjlyQI5e3+QVaI1bsABY4CwAAAABJRU5ErkJggg=='
                ),
        }),
    };
});

jest.mock('../../../redux/hooks/useRewards', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockReturnValue({
            rewards: mockStoreValues.rewards,
            offers: mockStoreValues.rewards.offers,
            isLoading: false,
            getOfferById: (offerId: string) => {
                return mockStoreValues.rewards.offers.find((item) => item.id === offerId);
            },
            rewardsActivityHistory: [],
            rewardsActivityHistoryLoading: false,
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
        }),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(true),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

const getAccountDealsPageContainer = () => screen.getByLabelText(/Account Page Content/i);
const getRedeemInStore = () => screen.getByText(/redeem in store/i);
const getBackLink = () => screen.getByText(/back/i);

describe('Account Deals Page - integration', () => {
    let clearNextHeadMock, renderResult;

    const renderPage = async () =>
        await act(async () => {
            const { render } = await getPage({
                route: '/account/deals/deal?id=0eb24fa8-7028-49f3-8610-5603852c869a',
            });

            renderResult = render().container;
        });

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    test('a11y', async () => {
        await renderPage();

        const results = await axe(renderResult);
        expect(results).toHaveNoViolations();
    });

    test('display deal details', async () => {
        await renderPage();

        const accountDealsPageContainer = getAccountDealsPageContainer();

        expect(within(accountDealsPageContainer).getByText(/\$1 Classic Roast Beef/i)).toBeInTheDocument();
        expect(within(accountDealsPageContainer).getByText(/Expires 07\/10/i)).toBeInTheDocument();
        expect(
            within(accountDealsPageContainer).getByText(/Get a Classic Roast Beef for just a buck!/i)
        ).toBeInTheDocument();
        expect(within(accountDealsPageContainer).getByText(/Terms and conditions of the deal/i)).toBeInTheDocument();
        expect(within(accountDealsPageContainer).getByText(/Offer valid for 30 days/i)).toBeInTheDocument();
    });

    test('show deal QR code', async () => {
        await renderPage();

        const accountDealsPageContainer = getAccountDealsPageContainer();
        const redeemInStore = getRedeemInStore();

        userEvent.click(redeemInStore);

        await waitFor(() =>
            expect(within(accountDealsPageContainer).getByText(/Please show QR code to cashier./i)).toBeInTheDocument()
        );
    });

    test('go back from QR should show deal', async () => {
        await renderPage();

        const accountDealsPageContainer = getAccountDealsPageContainer();

        const redeemInStore = getRedeemInStore();

        userEvent.click(redeemInStore);

        const backLink = getBackLink();

        userEvent.click(backLink);

        await waitFor(() =>
            expect(
                within(accountDealsPageContainer).queryByText(/Please show QR code to cashier./i)
            ).not.toBeInTheDocument()
        );

        expect(within(accountDealsPageContainer).queryByAltText(/qr/i)).not.toBeInTheDocument();
    });

    test('click on QR should disable button', async () => {
        await renderPage();
        const redeemInStore = getRedeemInStore();

        userEvent.click(redeemInStore);

        await waitFor(() => expect(redeemInStore).toBeDisabled());
    });
});
