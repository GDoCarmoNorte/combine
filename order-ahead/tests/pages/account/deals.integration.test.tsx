import { act, screen, within } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import useRewards from '../../../redux/hooks/useRewards';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { clear } from 'jest-date-mock';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../redux/hooks/useRewards', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockReturnValue({
            rewards: mockStoreValues.rewards,
            offers: mockStoreValues.rewards.offers,
            isLoading: false,
            rewardsActivityHistory: [],
            getOfferById: jest.fn(),
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
        }),
        isOfferModel: jest.fn().mockReturnValue(() => true),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(true),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/accountDealsPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

const getAccountDealsPageContainer = () => screen.getByLabelText(/Account Page Content/i);
const getOffers = () => screen.getAllByTestId(/deals-list-item/i);
const getMyDealEmpty = () => screen.getByTestId(/my-deals-empty/i);

describe('Account Deals Page - integration', () => {
    let clearNextHeadMock, renderResult;

    const renderPage = async () =>
        await act(async () => {
            const { render } = await getPage({
                route: '/account/deals',
            });

            renderResult = render().container;
        });

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    test('a11y', async () => {
        await renderPage();

        const results = await axe(renderResult);
        expect(results).toHaveNoViolations();
    });

    test('display header', async () => {
        await renderPage();

        const accountDealsPageContainer = getAccountDealsPageContainer();
        expect(within(accountDealsPageContainer).getByText(/Your Deals/i)).toBeInTheDocument();
    });

    test('count of rewards is correct', async () => {
        await renderPage();

        const offers = getOffers();
        expect(offers).toHaveLength(2);
    });

    test('display all received rewards', async () => {
        await renderPage();

        const accountDealsPageContainer = getAccountDealsPageContainer();
        expect(within(accountDealsPageContainer).getByText(/\$1 Classic Roast Beef/i)).toBeInTheDocument();
        expect(within(accountDealsPageContainer).getByText(/\$1 Roast Beef/i)).toBeInTheDocument();
    });

    test('display Online Only badge on $1 Roast Beef reward', async () => {
        await renderPage();

        const offers = getOffers();
        expect(offers[1]).toHaveTextContent(/Online Only/);
    });

    test('display expiresDate and view deal link in rewards', async () => {
        await renderPage();

        const offers = getOffers();
        expect(offers[0]).toHaveTextContent(/Expires 07\/10/);
        expect(offers[0]).toHaveTextContent(/Expires 07\/10/);
        expect(offers[1]).toHaveTextContent(/view deal/);
        expect(offers[1]).toHaveTextContent(/view deal/);
    });

    test('should not display deals', async () => {
        (useRewards as jest.Mock).mockReturnValue({
            rewards: {},
            offers: [],
            isLoading: false,
            rewardsActivityHistory: [],
            getOfferById: jest.fn(),
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
        });

        await renderPage();

        expect(screen.queryByText(/^view deal/i)).not.toBeInTheDocument();
    });

    test('view my deals empty', async () => {
        (useRewards as jest.Mock).mockReturnValue({
            rewards: {},
            offers: [],
            isLoading: false,
            rewardsActivityHistory: [],
            getOfferById: jest.fn(),
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
        });

        await renderPage();

        const empty = getMyDealEmpty();

        expect(empty).toBeInTheDocument();
    });
});
