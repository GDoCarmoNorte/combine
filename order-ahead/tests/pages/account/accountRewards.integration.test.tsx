import { act, screen, waitFor, within } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';
import { contentfulHandlers, server } from '../../mock-data/server/mockServer';
import { clear } from 'jest-date-mock';
import { useRewardsService } from '../../../common/hooks/useRewardsService';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import userEvent from '@testing-library/user-event';
import { useRewards, useGlobalProps } from '../../../redux/hooks';
import {
    offersAndCertificates,
    inactiveOffers,
} from '../../components/organisms/offersAndCertificates/offersAndCertificates.mock';
import { useDealsService } from '../../../common/hooks/useDealsService';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { getTextWithoutHtmlTags } from '../../utils';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../common/hooks/useRewardsService');
jest.mock('../../../common/hooks/useLoyaltyService');
jest.mock('../../../common/hooks/useCheckin', () => {
    return {
        useCheckin: jest.fn().mockReturnValue({ checkin: jest.fn(), isShowTooltip: false }),
    };
});
jest.mock('../../../common/hooks/useDealsService', () => {
    return {
        useDealsService: jest.fn().mockReturnValue({
            claimPointsApi: jest.fn().mockImplementation(() => Promise.resolve()),
        }),
    };
});
jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../redux/hooks/useRewards');

jest.mock('../../../redux/hooks/useLoyalty', () =>
    jest.fn().mockReturnValue({
        loyalty: {
            pointsBalance: 300,
            pointsExpiring: 0,
            pointsExpiringDate: null,
        },
        isLoading: false,
        actions: {
            setLoyaltyPointsLoading: jest.fn(),
        },
    })
);

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    return {
        __esModule: true,
        getNationalMenu: jest.fn().mockResolvedValue({}),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../common/hooks/useRewardsActivityHistory', () => ({
    useRewardsActivityHistory: jest.fn().mockReturnValue({
        getRewardsActivityHistory: jest.fn(),
    }),
}));

describe('Account Rewards Page', () => {
    let clearNextHeadMock, renderResult;

    const renderPage = async () =>
        await act(async () => {
            const { render } = await getPage({
                route: '/account/rewards',
            });

            renderResult = render().container;
        });

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);

        (useRewards as jest.Mock).mockReturnValue({
            offers: offersAndCertificates.offers,
            inactiveOffers: inactiveOffers,
            certificates: offersAndCertificates.certificates,
            certificatesSortedByStatus: offersAndCertificates.certificates,
            rewardsActivityHistory: [],
            actions: {
                setRewards: jest.fn(),
                setRewardsLoading: jest.fn(),
            },
            loading: false,
            getOfferById: jest.fn().mockReturnValue({ name: 'name' }),
        });
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    test('a11y', async () => {
        await renderPage();

        const results = await axe(renderResult);
        expect(results).toHaveNoViolations();
    });

    test('should contain find rewards button', async () => {
        await renderPage();
        screen.getByRole('button', { name: /find rewards/i });
    });

    test('should open modal on view details click and close modal with offer details when close icon clicked', async () => {
        await renderPage();
        const viewDetailsButtons = screen.getAllByRole('button', { name: /details/i });
        userEvent.click(viewDetailsButtons[0]);
        const modalContainer = screen.getByRole('presentation');
        expect(modalContainer).toBeInTheDocument();
        const closeModalButton = screen.getByRole('button', { name: /Close Icon/i });
        userEvent.click(closeModalButton);
        expect(modalContainer).not.toBeInTheDocument();
    });

    test('should not activate offer after unsuccessful request and show error message', async () => {
        (useRewardsService as jest.Mock).mockReturnValue({
            activateOffer: jest.fn().mockImplementation(() => Promise.reject()),
        });
        await renderPage();
        const activateButton = screen.getByRole('button', { name: /activate offer/i });
        userEvent.click(activateButton);
        screen.getByRole('progressbar');
        await waitFor(() => {
            expect(screen.getByText(/We couldn't activate your offer. Please try again later./i)).toBeInTheDocument();
        });
    });

    test('should activate offer from modal', async () => {
        (useRewardsService as jest.Mock).mockReturnValue({
            activateOffer: jest.fn().mockImplementation(() => Promise.resolve()),
        });
        await renderPage();
        const viewDetailsButtons = screen.getAllByRole('button', { name: /details/i });
        userEvent.click(viewDetailsButtons[0]);
        const modalContainer = screen.getByRole('presentation');
        expect(modalContainer).toBeInTheDocument();
        const activateButton = screen.getByRole('button', { name: /activate button/i });
        userEvent.click(activateButton);
        screen.getByRole('progressbar');
        await waitFor(() => {
            expect(within(modalContainer).getByText(/ACTIVATE OFFER/i)).toBeInTheDocument();
        });
    });

    test('should activate offer from listing', async () => {
        (useRewardsService as jest.Mock).mockReturnValue({
            activateOffer: jest.fn().mockImplementation(() => Promise.resolve()),
        });
        await renderPage();
        const activateButton = screen.getByRole('button', { name: /activate offer/i });
        userEvent.click(activateButton);
        screen.getByRole('progressbar');
    });

    test('should show success modal in case if claimPointsAPi return response', async () => {
        let modalContainer;
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest.fn().mockImplementation(() => Promise.resolve({})),
        });
        await renderPage();

        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });

        userEvent.click(submitButton);
        await waitFor(() => {
            modalContainer = screen.getByRole('presentation');
        });
        expect(modalContainer).toBeInTheDocument();
        screen.getByText(/success!/i);
        const closeModalButton = screen.getByRole('button', { name: /Close Icon/i });
        userEvent.click(closeModalButton);
        expect(modalContainer).not.toBeInTheDocument();
    });

    test('should show error modal in case if claimPointsAPi return error', async () => {
        let modalContainer;
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest.fn().mockImplementation(() => Promise.reject('something  went wrong')),
        });
        await renderPage();
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });

        userEvent.click(submitButton);
        await waitFor(() => {
            modalContainer = screen.getByRole('presentation');
        });
        expect(modalContainer).toBeInTheDocument();
        screen.getByText('UH OH');
        const closeModalButton = screen.getByRole('button', { name: /Close Icon/i });
        userEvent.click(closeModalButton);
        expect(modalContainer).not.toBeInTheDocument();
    });

    test('should show error modal and handled message when provided from exp-api', async () => {
        let modalContainer;
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest
                .fn()
                .mockImplementation(() => Promise.resolve({ code: 'GENERIC', message: 'something went wrong' })),
        });
        await renderPage();
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });
        userEvent.click(submitButton);
        await waitFor(() => {
            modalContainer = screen.getByRole('presentation');
        });
        expect(modalContainer).toBeInTheDocument();
        expect(screen.getByText(/uh oh/i)).toBeInTheDocument();
        expect(screen.getByText(/something went wrong/i)).toBeInTheDocument();
        const closeModalButton = screen.getByRole('button', { name: /Close Icon/i });
        userEvent.click(closeModalButton);
        expect(modalContainer).not.toBeInTheDocument();
    });

    test('it should attach support email when errorCode is Generic', async () => {
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest
                .fn()
                .mockImplementation(() =>
                    Promise.resolve({ code: 'GENERIC', message: 'Error occurred while processing request' })
                ),
        });
        await renderPage();
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });
        userEvent.click(submitButton);
        const expectedText =
            'Error occurred while processing request. Please contact Customer Service support@inspirebrands.com or contact us.';
        await waitFor(() => {
            expect(screen.getByText(/uh oh/i)).toBeInTheDocument();
        });
        expect(getTextWithoutHtmlTags(expectedText)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: 'support email' })).toHaveAttribute(
            'href',
            'mailto:support@inspirebrands.com'
        );
    });

    test('it should attach support email when errorCode is ALREADY_AWARDED', async () => {
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    code: 'ALREADY_AWARDED',
                    message: 'This transaction has already been awarded. Please contact Customer Service.',
                })
            ),
        });
        await renderPage();
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });
        const expectedText =
            'This transaction has already been awarded. Please contact Customer Service support@inspirebrands.com or contact us.';
        userEvent.click(submitButton);
        await waitFor(() => {
            expect(screen.getByText(/uh oh/i)).toBeInTheDocument();
        });
        expect(getTextWithoutHtmlTags(expectedText)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: 'support email' })).toHaveAttribute(
            'href',
            'mailto:support@inspirebrands.com'
        );
    });

    test('it should attach support email when errorCode is INVALID_PAST_DATE_BEFORE_ENROLLMENT', async () => {
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    code: 'INVALID_PAST_DATE_BEFORE_ENROLLMENT',
                    message:
                        'Transaction cannot be more than 30 days prior to your enrollment date, please contact Customer Service.',
                })
            ),
        });
        await renderPage();
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });

        userEvent.click(submitButton);
        const expectedText =
            'Transaction cannot be more than 30 days prior to your enrollment date, please contact Customer Service support@inspirebrands.com or contact us.';
        await waitFor(() => {
            expect(screen.getByText(/uh oh/i)).toBeInTheDocument();
        });
        expect(getTextWithoutHtmlTags(expectedText)).toBeInTheDocument();
        expect(screen.getByRole('link', { name: 'support email' })).toHaveAttribute(
            'href',
            'mailto:support@inspirebrands.com'
        );
    });

    test('it should show success banner when certificate has been redeemed', async () => {
        (useDealsService as jest.Mock).mockReturnValue({
            claimPointsApi: jest.fn().mockImplementation(() => Promise.resolve({ balance: 10 })),
        });

        await renderPage();
        const claimNumberInput = screen.getByLabelText(/claim number/i);
        userEvent.type(claimNumberInput, '1234-210528-11-12');
        const submitButton = screen.getByRole('button', { name: /submit/i });
        await waitFor(() => {
            expect(submitButton).not.toBeDisabled();
        });

        userEvent.click(submitButton);
        const expectedText =
            'You have successfully submitted your receipt and your points will be added to your account shorty!';
        await waitFor(() => {
            expect(screen.getByText(/SUCCESS!/i)).toBeInTheDocument();
        });
        expect(getTextWithoutHtmlTags(expectedText)).toBeInTheDocument();
    });
});
