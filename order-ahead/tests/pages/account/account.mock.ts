export default {
    idpCustomerId: '36e1369e-c448-4af0-a2a6-02177f63f6ce',
    firstName: 'John',
    lastName: 'Doe',
    email: 'john.doe@gmail34.com',
    birthDate: '12/31',
    references: [
        {
            type: 'OMS',
            id: '132e7d92-a841-11eb-9745-66a33d40bccf',
        },
    ],
    phones: [{ number: '1231231234', isPreferred: true }],
    preferences: {
        postalCode: '27511',
        marketing: [
            {
                type: 'EMAIL',
            },
        ],
        locations: [
            {
                id: '22323',
                isPreferred: true,
            },
        ],
    },
    termsAndConditions: {},
};
