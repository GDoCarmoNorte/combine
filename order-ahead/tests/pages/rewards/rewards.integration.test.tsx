import { screen, within } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { useAuth0 } from '@auth0/auth0-react';
import { clear } from 'jest-date-mock';

import useAccount from '../../../redux/hooks/useAccount';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { isRewardsOn } from '../../../lib/getFeatureFlags';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../redux/hooks/useAccount', () => {
    return {
        __esModule: true,
        default: jest.fn().mockReturnValue({
            accountInfo: {},
        }),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

const routerReplaceMock = jest.fn();

jest.mock('next/router', () => ({
    //@ts-ignore
    ...jest.requireActual('next/router'),
    default: {
        //@ts-ignore
        ...jest.requireActual('next/router').default,
        replace: (data) => routerReplaceMock(data),
    },
    useRouter: () => ({
        push: jest.fn(),
        replace: (data) => routerReplaceMock(data),
        route: '/',
        pathname: '',
        query: '',
        asPath: '',
    }),
}));

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isRewardsOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,

        useAuth0: jest.fn().mockImplementation(() => ({
            getIdTokenClaims: jest.fn().mockImplementation(() =>
                Promise.resolve({
                    __raw: 'token',
                })
            ),
            user: {},
            isAuthenticated: true,
        })),
        withAuthenticationRequired: jest.fn().mockImplementation((item) => item),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const getRewardsRosterContainer = () => screen.getByLabelText(/Rewards Roster Page/i);
const getRewardsPageContainer = () => screen.getByLabelText(/Rewards Page/i);

describe('Rewards - integration', () => {
    let clearNextHeadMock;
    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    describe('when rewards page is enabled and user is authenticated', () => {
        beforeEach(async () => {
            const { render } = await getPage({
                route: '/rewards',
            });

            await render();
        });

        test('display rewards roster page', async () => {
            const rewardsRosterContainer = getRewardsRosterContainer();

            expect(
                within(rewardsRosterContainer).getByRole('heading', { name: /rewards roster/i })
            ).toBeInTheDocument();
        });
    });

    describe('when rewards page is enabled and user is guest', () => {
        beforeEach(async () => {
            (useAccount as jest.Mock).mockReturnValue({
                accountInfo: null,
            });
            (useAuth0 as jest.Mock).mockReturnValue({
                getIdTokenClaims: jest.fn().mockImplementation(() =>
                    Promise.resolve({
                        __raw: 'token',
                    })
                ),
                user: null,
                isAuthenticated: false,
            });

            const { render } = await getPage({
                route: '/rewards',
            });

            await render();
        });

        test('display rewards page', async () => {
            const rewardsPageContainer = getRewardsPageContainer();

            expect(within(rewardsPageContainer).getByText(/blazin’ rewards/i)).toBeInTheDocument();
        });
    });

    describe('when rewards page is disabled', () => {
        test('should hide rewards page and redirect to 404', async () => {
            (isRewardsOn as jest.Mock).mockReturnValue(false);

            const { render } = await getPage({
                route: '/rewards',
            });

            await render();

            expect(routerReplaceMock).toHaveBeenCalledWith('/404');
        });
    });
});
