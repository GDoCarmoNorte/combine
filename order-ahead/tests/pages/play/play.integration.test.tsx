import { screen, act } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { isPlayExperienceEnabled } from '../../../lib/getFeatureFlags';
import { getLocationById } from '../../../common/services/locationService';
import { getCoordinates } from '../../../common/helpers/getCoordinates';
import { useAuth0 } from '@auth0/auth0-react';
import { useAccount, useOrderHistory, useGlobalProps } from '../../../redux/hooks';
import { useCheckin } from '../../../common/hooks/useCheckin';
import { getCookieValue } from '../../../common/helpers/cookieHelper';
import { useMediaQuery } from '@material-ui/core';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';

jest.unmock('../../../redux/hooks/useConfiguration');
const routerReplaceMock = jest.fn();
jest.mock('next/router', () => ({
    //@ts-ignore
    ...jest.requireActual('next/router'),
    default: {
        //@ts-ignore
        ...jest.requireActual('next/router').default,
        replace: (data) => routerReplaceMock(data),
    },
    useRouter: () => ({
        push: jest.fn(),
        replace: (data) => routerReplaceMock(data),
        route: '/',
        pathname: '',
        query: '',
        asPath: '',
    }),
}));

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(false),
        isTippingEnabled: jest.fn().mockReturnValue(false),
        isDeliveryEnabled: jest.fn().mockReturnValue(false),
        isApplePayEnabled: jest.fn().mockReturnValue(false),
        isGooglePayEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.location),
    };
});

jest.mock('../../../common/services/locationService');
jest.mock('../../../redux/hooks/useOrderHistory');
jest.mock('../../../redux/hooks/useAccount');
jest.mock('../../../common/hooks/useCheckin');
jest.mock('../../../common/helpers/getCoordinates');
jest.mock('@auth0/auth0-react', () => {
    return {
        __esModule: true,
        // @ts-ignore
        ...jest.requireActual('@auth0/auth0-react'),
        useAuth0: jest.fn(),
        withAuthenticationRequired: jest.fn().mockImplementation((component) => component),
    };
});
jest.mock('@material-ui/core/useMediaQuery');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const userAccountMock = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'john.doe@gmail34.com',
    references: [
        {
            type: 'LMS',
            id: '100500',
        },
    ],
    phones: [{ number: '1231231234' }],
    preferences: {
        postalCode: '12345',
    },
};
const hashedCookieMock = 'test hashed string';
const latMock = 1;
const longMock = 1;
const loginWithRedirectMock = jest.fn().mockResolvedValue({});
const checkinMock = jest.fn().mockResolvedValue({});
const sha256Mock = jest.fn().mockReturnValue(hashedCookieMock);
const playPageUrlMock = 'https://play.com';
const valueEnv = process.env.NEXT_PUBLIC_PLAY_PAGE_URL;
const integrationKey = process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY;
const locatioMock = {
    id: 1,
    contactDetails: {
        address: {
            stateProvinceCode: 'MN',
            postalCode: '12345',
        },
    },
};

jest.mock('js-sha256', () => {
    return { sha256: () => sha256Mock() };
});

const routeMock = `/play`;

describe('Play Page - Integration', () => {
    let renderResult, clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();
        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);

        process.env.NEXT_PUBLIC_PLAY_PAGE_URL = playPageUrlMock;
        process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY = '';
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
        jest.clearAllMocks();
        process.env.NEXT_PUBLIC_PLAY_PAGE_URL = valueEnv;
        process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY = integrationKey;
    });

    beforeEach(async () => {
        (useAccount as jest.Mock).mockReturnValue({
            account: userAccountMock,
            accountInfo: userAccountMock,
            actions: {
                setAccount: jest.fn(),
            },
        });
        (useAuth0 as jest.Mock).mockReturnValue({ loginWithRedirect: loginWithRedirectMock, isAuthenticated: true });
        (useOrderHistory as jest.Mock).mockReturnValue({
            orderHistory: [
                {
                    fulfillment: {
                        location: {
                            id: '123',
                        },
                    },
                },
            ],
            isLoading: false,
            actions: {
                setOrderHistory: jest.fn(),
                setOrderHistoryLoading: jest.fn(),
            },
        });
        (getLocationById as jest.Mock).mockReturnValue(locatioMock);
        (getCoordinates as jest.Mock).mockResolvedValue({ coords: { latitude: latMock, longitude: longMock } });
        (useCheckin as jest.Mock).mockReturnValue({ checkin: checkinMock, payload: { storeId: 7, message: '' } });
        (useMediaQuery as jest.Mock).mockReturnValue(false);
    });

    describe('when Play page feature is turned off', () => {
        beforeEach(async () => {
            (isPlayExperienceEnabled as jest.Mock).mockReturnValue(false);
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });
        test('should redirect to 404', async () => {
            expect(routerReplaceMock).toHaveBeenCalledTimes(1);
            expect(routerReplaceMock).toHaveBeenCalledWith('/404');
        });
    });

    describe('when Play page feature is turned on', () => {
        beforeEach(async () => {
            (isPlayExperienceEnabled as jest.Mock).mockReturnValue(true);
        });

        describe('mobile web', () => {
            beforeEach(async () => {
                await act(async () => {
                    const { render } = await getPage({
                        route: routeMock,
                    });

                    renderResult = render().container;
                });
            });

            test('a11y', async () => {
                const results = await axe(renderResult);
                expect(results).toHaveNoViolations();
            });

            test('should render iFrame and set cookie value', async () => {
                expect(checkinMock).toHaveBeenCalled();
                expect(getCoordinates).toHaveBeenCalled();
                expect(sha256Mock).toHaveBeenCalled();

                expect(screen.getByLabelText(/Play/i)).toBeInTheDocument();

                const expectedCookie = {
                    profileId: userAccountMock.references[0].id,
                    email: userAccountMock.email,
                    firstName: userAccountMock.firstName,
                    lastName: userAccountMock.lastName,
                    userZipcode: userAccountMock.preferences.postalCode,
                    phoneNumber: userAccountMock.phones[0].number,
                    eCommerceDate: '',
                    eCommerceStoreId: '123',
                    latitude: latMock,
                    longitude: longMock,
                    appIdentifier: '',
                    GTMContainerId: '',
                    deviceType: 'web',
                };

                const cookieString = getCookieValue('blazin_rewards');

                expect(cookieString).not.toBeNull();
                expect(cookieString).toEqual(JSON.stringify(expectedCookie));
            });
        });

        describe('desktopp web', () => {
            beforeEach(async () => {
                (useMediaQuery as jest.Mock).mockReturnValue(true);
                await act(async () => {
                    const { render } = await getPage({
                        route: routeMock,
                    });

                    renderResult = render().container;
                });
            });

            test('should redirect to external Play page', async () => {
                expect(routerReplaceMock).toHaveBeenCalled();
                expect(routerReplaceMock).toHaveBeenLastCalledWith(playPageUrlMock);
            });
        });
    });
});
