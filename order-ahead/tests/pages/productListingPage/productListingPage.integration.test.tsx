/* eslint-disable testing-library/no-node-access */
import { getPage } from '../../lib/integration/getPage';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
import { getGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import { screen } from '@testing-library/react';
import { getLocationMenu } from '../../../common/services/domainMenu';
import { createNextState } from '@reduxjs/toolkit';

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/globalContentfulProps', () => ({
    getGlobalContentfulProps: jest.fn(),
}));

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

describe('Product Details Page - Integration', () => {
    let clearNextHeadMock;

    beforeAll(async () => {
        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        clearNextHeadMock = mockNextHead();

        server.listen();
        server.use(contentfulHandlers);

        (getGlobalContentfulProps as jest.Mock).mockResolvedValue(globalContentfulPropsMock);
        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clearNextHeadMock();

        server.close();
    });

    test('should correctly render signature category products ("categoryIdList" field in contentful, id: "arb-cat-000-003")', async () => {
        const { render } = await getPage({
            route: '/menu/signature',
        });

        render();

        expect(screen.getByText(/Corned Beef Reuben/i)).toBeInTheDocument();
        expect(screen.getByText(/Roast Turkey Gyro/i)).toBeInTheDocument();
        expect(screen.getByText(/Roast Beef Gyro/i)).toBeInTheDocument();
        expect(screen.getByText(/Greek Gyro/i)).toBeInTheDocument();
        expect(screen.getByText(/Smokehouse Brisket/i)).toBeInTheDocument();
    });

    test('should correctly render signature category products ("categoryIdList" field in contentful, id: "test-multi-id")', async () => {
        const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

        (getLocationMenu as jest.Mock).mockResolvedValueOnce(
            createNextState(mockStoreValues.domainMenu.payload, (payload) => {
                const p = payload;
                p.categories['test-multi-id'] = payload.categories['arb-cat-000-003'];
                p.categories['test-multi-id'].id = 'test-multi-id';

                delete p.categories['arb-cat-000-003'];

                return p;
            })
        );

        const { render } = await getPage({
            route: '/menu/signature',
        });

        render();

        expect(screen.getByText(/Corned Beef Reuben/i)).toBeInTheDocument();
        expect(screen.getByText(/Roast Turkey Gyro/i)).toBeInTheDocument();
        expect(screen.getByText(/Roast Beef Gyro/i)).toBeInTheDocument();
        expect(screen.getByText(/Greek Gyro/i)).toBeInTheDocument();
        expect(screen.getByText(/Smokehouse Brisket/i)).toBeInTheDocument();
    });

    test('should correctly render chicken category products ("categoryId" field in contentful)', async () => {
        const { render } = await getPage({
            route: '/menu/chicken',
        });

        render();

        expect(screen.getByText(/Chicken Tenders 3PC/i)).toBeInTheDocument();
        expect(screen.getByText(/Chicken Tenders 5PC/i)).toBeInTheDocument();
        expect(screen.getByText(/Buffalo Crispy Chicken Sandwich/i)).toBeInTheDocument();
        expect(screen.getByText(/Classic Roast Chicken Sandwich/i)).toBeInTheDocument();
        expect(screen.getByText(/Roast Chicken Bacon & Swiss Sandwich/i)).toBeInTheDocument();
    });
});
