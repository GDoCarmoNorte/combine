import { screen, act, waitFor, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';

import fetchMock from 'jest-fetch-mock';
import { advanceTo, clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';

import mockStoreValues from '../../mock-data/store/productsListingPageStore.mock.json';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import getBrandInfo from '../../../lib/brandInfo';
import { getPageTitle } from '../../../common/helpers/getPageTitle';
import mockDealsPage from '../../mock-data/contentful/contentful-getEntry-page-deals.json';
import {
    DUPLICATE_EMAIL_MESSAGE_TITLE,
    DUPLICATE_EMAIL_MESSAGE_TEXT,
    DEFAULT_ERROR_MESSAGE_TEXT,
} from '../../../components/sections/emailSignup/constants';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';

jest.unmock('../../../redux/hooks/useConfiguration');
jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

const getFirstNameField = () => screen.getByPlaceholderText(/Enter First Name/i);
const getLastNameField = () => screen.getByPlaceholderText(/Enter Last Name/i);
const getEmailField = () => screen.getByPlaceholderText(/Enter Email/i);
const getZipCodeField = () => screen.getByPlaceholderText(/Enter Zip Code/i);
const getBirthDateField = () => screen.getByPlaceholderText(/MM\/DD/i);

const getSignUpButton = () => screen.getByRole('button', { name: /sing up/i });
const getSuccessMessageHeader = () => screen.queryByRole('heading', { name: /Good Move/i });
const getDuplicateEmailMessageHeader = () => screen.queryByRole('heading', { name: DUPLICATE_EMAIL_MESSAGE_TITLE });
const getDuplicateEmailMessageText = () => screen.queryByText(DUPLICATE_EMAIL_MESSAGE_TEXT);
const getDefaultErrorMessage = () => screen.queryByText(DEFAULT_ERROR_MESSAGE_TEXT);

describe('Deals Page - Integration', () => {
    let clearHeadMock, renderResult;

    beforeAll(async () => {
        fetchMock.mockResponse(async (req) => {
            if (req.url === 'https://maps.googleapis.com/maps/api/geocode/json?address=00501&key=mock-google-key')
                return JSON.stringify({ results: [{ geometry: { location: { lat: 10, lng: 20 } } }] });
            if (req.url.match(/\/webExpApiPath\/v1\/location\/*/)) {
                return JSON.stringify({
                    totalElements: 1,
                    totalPages: 1,
                    pageNumber: 0,
                    pageSize: 10,
                    isLastPage: false,
                    locations: [(mockStoreValues as any).orderLocation.pickupAddress],
                });
            }
            const signupRequestUrl = '/domainCustomerPath/customer/v2/brand/ARB/marketing';
            if (req.url === signupRequestUrl) {
                const payload = await req.json();

                if (payload.email === 'dulpicated@email.com') {
                    return {
                        status: 400,
                        body: JSON.stringify({
                            referenceId: 'b1879c852ffd651b93c3646774f4e699',
                            path: '/customer/v2/brand/ARB/marketing',
                            errorMessage: 'Customer already Opted in for Marketing Promotions.',
                            errors: [
                                {
                                    code: 'C10001',
                                    message: 'Customer already Opted in for Marketing Promotions.',
                                    source: 'CustomerDomainService',
                                    brand: 'ARB',
                                    reasonCode: 'CDS_MKTG_PREF_OPTED_IN',
                                    severity: 0,
                                },
                            ],
                        }),
                    };
                }
                return JSON.stringify({
                    preferences: {
                        emailSignup: {
                            status: 'RECEIVED',
                        },
                    },
                });
            }

            return Promise.reject(new Error(`URL not mocked! - ${req.url}`));
        });

        advanceTo(new Date('2020-12-01T10:20:30Z'));

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        global.scrollTo = jest.fn();

        clearHeadMock = mockNextHead();

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearHeadMock();

        server.close();
    });

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/deals',
            });

            renderResult = render().container;
        });
    });

    test('a11y', async () => {
        const results = await axe(renderResult);
        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', () => {
        screen.getByText(/skip to main content/i);
    });

    test('entering a zipcode starting with a zero', async () => {
        const getZipCodeFieldValidationError = () => screen.queryByText(/^Zip Code is incomplete/i);

        userEvent.clear(getZipCodeField());
        userEvent.type(getZipCodeField(), '00501');
        expect(getZipCodeFieldValidationError()).not.toBeInTheDocument();

        await waitFor(() => {
            expect(getZipCodeField()).toHaveDisplayValue('00501');
        });
    });

    test('show all fields empty by default except zipcode with "47586" zip & location "Dunwoody - Roswell Rd" and sign up button disabled', () => {
        screen.getByRole('heading', { name: /email sign up/i });

        expect(getFirstNameField()).toHaveValue('');
        expect(getLastNameField()).toHaveValue('');
        expect(getEmailField()).toHaveValue('');
        expect(getZipCodeField()).toHaveDisplayValue('00501');
        expect(screen.getAllByText(/Dunwoody - Roswell Rd/i)).toHaveLength(2);
        expect(getBirthDateField()).toHaveValue('');
        expect(screen.getByText('Privacy Policy', { selector: 'form > span > p > a' })).toHaveAttribute(
            'href',
            'http://localhost:3000/privacy-policy/'
        );

        expect(getSignUpButton()).toBeDisabled();
    });

    test('show validation error messages when leaving fields focus', async () => {
        const getFirstNameFieldValidationError = () => screen.queryByText(/^First Name is incomplete/i);
        const getLastNameFieldValidationError = () => screen.queryByText(/^Last Name is incomplete/i);
        const getEmailFieldValidationError = () => screen.queryByText(/^Email is incomplete/i);
        const getZipCodeFieldValidationError = () => screen.queryByText(/^Zip Code is incomplete/i);

        expect(getFirstNameFieldValidationError()).not.toBeInTheDocument();
        getFirstNameField().focus();
        userEvent.tab();
        expect(getFirstNameFieldValidationError()).toBeInTheDocument();

        expect(getLastNameFieldValidationError()).not.toBeInTheDocument();
        getLastNameField().focus();
        userEvent.tab();
        expect(getLastNameFieldValidationError()).toBeInTheDocument();

        expect(getEmailFieldValidationError()).not.toBeInTheDocument();
        getEmailField().focus();
        userEvent.tab();
        expect(getEmailFieldValidationError()).toBeInTheDocument();

        expect(getZipCodeFieldValidationError()).not.toBeInTheDocument();
        userEvent.clear(getZipCodeField());
        userEvent.tab();

        await waitFor(() => {
            expect(getZipCodeFieldValidationError()).toBeInTheDocument();
        });
    });

    test('should submit form data and show success message', async () => {
        userEvent.type(getFirstNameField(), 'MyName');
        userEvent.type(getLastNameField(), 'MyLastName');
        userEvent.type(getEmailField(), 'name@lastname.com');

        await waitFor(() => {
            expect(getSignUpButton()).toBeEnabled();
        });

        fireEvent.click(getSignUpButton()); // userEvent doesn't trigger formik submit

        await waitFor(() => {
            expect(getSuccessMessageHeader()).toBeInTheDocument();
        });

        expect(getDuplicateEmailMessageHeader()).not.toBeInTheDocument();
        expect(getDuplicateEmailMessageText()).not.toBeInTheDocument();
    });

    test('should submit form data and show duplicate email message', async () => {
        userEvent.type(getFirstNameField(), 'MyName');
        userEvent.type(getLastNameField(), 'MyLastName');
        userEvent.type(getEmailField(), 'dulpicated@email.com');

        await waitFor(() => {
            expect(getSignUpButton()).toBeEnabled();
        });

        fireEvent.click(getSignUpButton());

        await waitFor(() => {
            expect(getDuplicateEmailMessageHeader()).toBeInTheDocument();
        });

        expect(getDuplicateEmailMessageText()).toBeInTheDocument();
        expect(getSuccessMessageHeader()).not.toBeInTheDocument();
    });

    test('should submit form data and show default error message', async () => {
        fetchMock.mockResponseOnce(async () => {
            return {
                status: 400,
                body: JSON.stringify({
                    referenceId: '284df9d20578873af8e9add5d663f9ed',
                    path: '/customer/v2/brand/ARB/marketing',
                    errorMessage: 'Request has been rejected.',
                    errors: [
                        {
                            code: 'C10003',
                            message: 'Request has been rejected.',
                            source: 'CustomerDomainService',
                            brand: 'ARB',
                            reasonCode: 'CDS_BAD_REQUEST',
                            severity: 1,
                        },
                    ],
                }),
            };
        });

        userEvent.type(getFirstNameField(), 'MyName');
        userEvent.type(getLastNameField(), 'MyLastName');
        userEvent.type(getEmailField(), 'name@lastname.com');

        await waitFor(() => {
            expect(getSignUpButton()).toBeEnabled();
        });

        fireEvent.click(getSignUpButton());

        await waitFor(() => {
            expect(getDefaultErrorMessage()).toBeInTheDocument();
        });

        expect(getDuplicateEmailMessageHeader()).not.toBeInTheDocument();
        expect(getDuplicateEmailMessageText()).not.toBeInTheDocument();
        expect(getSuccessMessageHeader()).not.toBeInTheDocument();
    });

    test('deals page should have correct title', async () => {
        const brandInfo = getBrandInfo();

        expect(global.window).toBeDefined();
        expect(global.window.document).toBeDefined();

        expect(brandInfo).toBeDefined();
        expect(brandInfo.brandName).toBeDefined();

        await waitFor(() => {
            expect(global.window.document.title).toBe(
                getPageTitle(brandInfo.brandName, mockDealsPage.items[0].fields.metaTitle)
            );
        });
    });

    test('deals page should contain footer', () => {
        screen.getByRole('contentinfo');
    });
});
