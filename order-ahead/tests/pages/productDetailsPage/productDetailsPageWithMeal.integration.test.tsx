/* eslint-disable testing-library/no-node-access */
import { screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';
import { getGlobalContentfulProps } from '../../../common/services/globalContentfulProps';

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/pdpGreekGyroMealStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/pdpGreekGyroMealStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/globalContentfulProps', () => ({
    getGlobalContentfulProps: jest.fn(),
}));

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/pdpGreekGyroMealStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(true),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const getElementCard = (container: HTMLElement, name: RegExp): HTMLElement => {
    return within(container).getByRole('listitem', { name });
};

const getProductBannerContainer = async () =>
    (
        await screen.findAllByRole('heading', {
            name: /^Greek Gyro Meal/i,
        })
    )[0].closest('div');

const getProductModifiersContainer = async () => await screen.findByRole('presentation');
const getProductSpecificModifiersContainer = (productModifiersContainer: HTMLElement, modifierName: RegExp) =>
    within(productModifiersContainer).getByRole('heading', { name: modifierName }).closest('div').parentElement; //TODO: fix a11y
const getSidesSection = () => screen.getByLabelText(/Side Section/i);
const getDrinkSection = () => screen.getByLabelText(/Drink Section/i);

describe('PDP - Meal integration', () => {
    beforeAll(async () => {
        jest.resetAllMocks();
        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);
        (getGlobalContentfulProps as jest.Mock).mockResolvedValue(globalContentfulPropsMock);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        jest.resetAllMocks();
        server.close();
    });

    beforeEach(async () => {
        const { render } = await getPage({
            route: '/menu/meals/greek-gyro-meal',
        });

        await render();
    });

    test('SkipLink exists on the page', () => {
        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('Meal details displayed', async () => {
        const productBannerContainer = await getProductBannerContainer();
        const defaultSides = within(productBannerContainer).getByLabelText(/Current Sides and Modifications/i);
        const priceContainer = within(productBannerContainer).getByLabelText(/Product Price and Calories/i);

        expect(screen.getByText(/^GREEK GYRO MEAL/i)).toBeInTheDocument();
        expect(screen.getByText(/^SMALL/i)).toBeInTheDocument();
        expect(screen.getByText(/^MEDIUM/i)).toBeInTheDocument();
        expect(screen.getByText(/^LARGE/i)).toBeInTheDocument();

        expect(defaultSides).toHaveTextContent(/Sides: Curly Fries, Coca-Cola®/i);
        expect(priceContainer).toHaveTextContent(/\$6.79 • 1280 calories/i);
    });

    test('Display sandwich modifiers', async () => {
        const productBannerContainer = await getProductBannerContainer();
        userEvent.click(
            within(productBannerContainer).getByRole('link', {
                name: /modify sandwich/i,
            })
        );

        const modifyContainer = await getProductModifiersContainer();

        const testModifiersSection = (
            sectionName: RegExp,
            modifiers: {
                name: RegExp;
                price?: RegExp;
                calories: RegExp;
                checked: boolean;
                selections?: { name: RegExp; isActive?: boolean }[];
            }[]
        ) => {
            const sectionContainer = getProductSpecificModifiersContainer(modifyContainer, sectionName);
            modifiers.forEach(({ name, price, calories, checked, selections }) => {
                const container = getElementCard(sectionContainer, name);
                if (price) {
                    within(container).getByText(price);
                }

                within(container).getByText(calories);

                within(container).getByRole('checkbox', { checked });

                if (selections) {
                    selections.forEach((it) => {
                        const button = within(container).getByRole('radio', { name: it.name });
                        expect(button).toBeInTheDocument();
                        if (it.isActive) {
                            expect(button).toHaveAttribute('aria-checked', 'true');
                        } else {
                            expect(button).toHaveAttribute('aria-checked', 'false');
                        }
                    });
                }
            });
        };

        testModifiersSection(/meat/i, [
            { name: /pepper bacon/i, checked: false, price: /\+ \$0.79/i, calories: /68 calories/i },
            { name: /Double Greek Meat/i, checked: false, price: /\+ \$2.00/i, calories: /314 calories/i },
        ]);
        testModifiersSection(/cheese/i, [
            { name: /Big Eye Swiss/i, checked: false, price: /\+ \$0.49/i, calories: /76 calories/i },
            { name: /natural cheddar/i, checked: false, price: /\+ \$0.49/i, calories: /76 calories/i },
        ]);
        testModifiersSection(/veggies/i, [
            { name: /Shredded Lettuce/i, checked: true, calories: /3 calories/i },
            { name: /Red Onion/i, checked: true, calories: /2 calories/i },
            { name: /tomato/i, checked: true, calories: /7 calories/i },
            { name: /Diced Jalapeño/i, checked: false, calories: /2 calories/i },
        ]);
        testModifiersSection(/sauces/i, [
            { name: /Tzatziki Sauce/i, checked: true, calories: /119 calories/i },
            { name: /Spicy Chili Sauce/i, checked: false, calories: /25 calories/i },
            {
                name: /Tartar Sauce/i,
                checked: true,
                calories: /0 calories/i,
                selections: [
                    { name: /Regular/i, isActive: true },
                    { name: /Side/i },
                    { name: /Light/i },
                    { name: /Extra/i },
                ],
            },
        ]);
        testModifiersSection(/sauce packets/i, [
            { name: /Horsey Sauce Packet/i, checked: false, calories: /14 calories/i },
            { name: /Arby's Sauce Packet/i, checked: false, calories: /14 calories/i },
            { name: /Ketchup Packet/i, checked: false, calories: /9 calories/i },
        ]);
        testModifiersSection(/OTHER CONDIMENTS/i, [
            { name: /Cup of Cheddar Sauce/i, checked: false, price: /\+ \$1.59/i, calories: /26 calories/i },
        ]);
        testModifiersSection(/Seasonings/i, [{ name: /Greek Seasoning/i, checked: true, calories: /0 calories/i }]);
    });

    test('remove and change modifier with intensity', async () => {
        const productBannerContainer = await getProductBannerContainer();
        userEvent.click(
            within(productBannerContainer).getByRole('link', {
                name: /modify sandwich/i,
            })
        );

        const modifyContainer = await getProductModifiersContainer();

        const tatarSauce = getElementCard(modifyContainer, /Tartar Sauce/i);
        const sideIntensity = within(tatarSauce).queryByRole('radio', { name: /Side/i });

        await userEvent.click(sideIntensity); // Change intensity

        within(tatarSauce).getByText(/\+ \$0.1/i);
        within(tatarSauce).getByText(/10 calories/i);

        within(productBannerContainer).getByText(/\$6.89/i);
        within(productBannerContainer).getByText(/1290 calories/i);
        within(productBannerContainer).getByText(/Modifications: Add Tartar Sauce\(Side\)/i);

        await userEvent.click(within(tatarSauce).queryByRole('checkbox', { checked: true })); // remove modifier

        within(productBannerContainer).getByText(/\$6.79/i);
        within(productBannerContainer).getByText(/1280 calories/i);
        within(productBannerContainer).getByText(/Modifications: Remove Tartar Sauce\(Regular\)/i);
    });

    test('Сheck “your side” section contain all available sides w/ specific name and calories', async () => {
        const sidesSection = getSidesSection();

        const curlyFries = getElementCard(sidesSection, /^Curly Fries/i);
        within(curlyFries).getByText(/\$1.99/i);
        within(curlyFries).getByText(/410 calories/i);
        within(curlyFries).getByRole('checkbox', { checked: true });

        const loadedCurlyFries = getElementCard(sidesSection, /^Loaded Curly Fries/i);
        within(loadedCurlyFries).getByText(/\$3.29/i);
        within(loadedCurlyFries).getByText(/670 calories/i);
        within(loadedCurlyFries).getByRole('checkbox', { checked: false });

        const potatoCakes = getElementCard(sidesSection, /^Potato Cakes/i);
        within(potatoCakes).getByText(/\$1.99/i);
        within(potatoCakes).getByText(/250 calories/i);
        within(potatoCakes).getByRole('checkbox', { checked: false });

        const mozzarellaSticks = getElementCard(sidesSection, /^Mozzarella Sticks/i);
        within(mozzarellaSticks).getByText(/\$2.99/i);
        within(mozzarellaSticks).getByText(/440 calories/i);
        within(mozzarellaSticks).getByRole('checkbox', { checked: false });

        const jalapenoBites = getElementCard(sidesSection, /^Jalapeño Bites/i);
        within(jalapenoBites).getByText(/\$2.99/i);
        within(jalapenoBites).getByText(/290 calories/i);
        within(jalapenoBites).getByRole('checkbox', { checked: false });

        const sideSaladwithRanchDressing = getElementCard(sidesSection, /^Mozzarella Sticks/i);
        within(sideSaladwithRanchDressing).getByText(/\$2.99/i);
        within(sideSaladwithRanchDressing).getByText(/440 calories/i);
        within(sideSaladwithRanchDressing).getByRole('checkbox', { checked: false });

        const sideSalad = getElementCard(sidesSection, /^Side Salad with ranch dressing/i);
        within(sideSalad).getByText(/\$1.99/i);
        within(sideSalad).getByText(/70 calories/i);
        within(sideSalad).getByRole('checkbox', { checked: false });
    });

    test('Сheck “your drink” section contain all available drinks w/ specific name and calories', async () => {
        const drinksSection = getDrinkSection();

        const cocaCola = getElementCard(drinksSection, /^Coca-Cola®$/i);
        within(cocaCola).getByText(/\$1.79/);
        within(cocaCola).getByText(/160 calories/i);
        within(cocaCola).getByRole('checkbox', { checked: true });

        const dietCoke = getElementCard(drinksSection, /^Diet Coke®/i);
        within(dietCoke).getByText(/\$1.79/);
        within(dietCoke).getByText(/0 calories/i);
        within(dietCoke).getByRole('checkbox', { checked: false });

        const sprite = getElementCard(drinksSection, /^Sprite®/i);
        within(sprite).getByText(/\$1.79/);
        within(sprite).getByText(/160 calories/i);
        within(sprite).getByRole('checkbox', { checked: false });

        const drPepper = getElementCard(drinksSection, /^Dr Pepper®/i);
        within(drPepper).getByText(/\$1.79/);
        within(drPepper).getByText(/190 calories/i);
        within(drPepper).getByRole('checkbox', { checked: false });

        const dietDrPepper = getElementCard(drinksSection, /^Diet Dr Pepper®/i);
        within(dietDrPepper).getByText(/\$1.79/);
        within(dietDrPepper).getByText(/0 calories/i);
        within(dietDrPepper).getByRole('checkbox', { checked: false });

        const melloYello = getElementCard(drinksSection, /^Mello Yello®/i);
        within(melloYello).getByText(/\$1.79/);
        within(melloYello).getByText(/170 calories/i);
        within(melloYello).getByRole('checkbox', { checked: false });

        const barqsRootBeer = getElementCard(drinksSection, /^Barq's Root Beer®/i);
        within(barqsRootBeer).getByText(/\$1.79/);
        within(barqsRootBeer).getByText(/180 calories/i);
        within(barqsRootBeer).getByRole('checkbox', { checked: false });

        const hiCFlashinFruitPunch = getElementCard(drinksSection, /^Hi-C Flashin' Fruit Punch®/i);
        within(hiCFlashinFruitPunch).getByText(/\$1.79/);
        within(hiCFlashinFruitPunch).getByText(/170 calories/i);
        within(hiCFlashinFruitPunch).getByRole('checkbox', { checked: false });

        const cocaColaZeroSugar = getElementCard(drinksSection, /^Coca-Cola® Zero Sugar/i);
        within(cocaColaZeroSugar).getByText(/\$1.79/);
        within(cocaColaZeroSugar).getByText(/0 calories/i);
        within(cocaColaZeroSugar).getByRole('checkbox', { checked: false });

        const fantaOrange = getElementCard(drinksSection, /^Fanta® Orange/i);
        within(fantaOrange).getByText(/\$1.79/);
        within(fantaOrange).getByText(/170 calories/i);
        within(fantaOrange).getByRole('checkbox', { checked: false });

        const powerade = getElementCard(drinksSection, /^Powerade®/i);
        within(powerade).getByText(/\$1.79/);
        within(powerade).getByText(/90 calories/i);
        within(powerade).getByRole('checkbox', { checked: false });

        const vanillaShake = getElementCard(drinksSection, /^Vanilla Shake/i);
        within(vanillaShake).getByText(/\$2.59/);
        within(vanillaShake).getByText(/490 calories/i);
        within(vanillaShake).getByRole('checkbox', { checked: false });

        const chocolateShake = getElementCard(drinksSection, /^Chocolate Shake/i);
        within(chocolateShake).getByText(/\$2.59/);
        within(chocolateShake).getByText(/590 calories/i);
        within(chocolateShake).getByRole('checkbox', { checked: false });

        const jamochaShake = getElementCard(drinksSection, /^Jamocha Shake/i);
        within(jamochaShake).getByText(/\$2.59/);
        within(jamochaShake).getByText(/592 calories/i);
        within(jamochaShake).getByRole('checkbox', { checked: false });

        const bottledWater = getElementCard(drinksSection, /^Bottled Water/i);
        within(bottledWater).getByText(/\$1.79/);
        within(bottledWater).getByText(/0 calories/i);
        within(bottledWater).getByRole('checkbox', { checked: false });

        const icedTea = getElementCard(drinksSection, /^Iced Tea/i);
        within(icedTea).getByText(/\$1.79/);
        within(icedTea).getByText(/0 calories/i);
        within(icedTea).getByRole('checkbox', { checked: false });
    });

    test('Select a different side, price and calories should be updated', async () => {
        const productBannerContainer = await getProductBannerContainer();
        const sides = within(productBannerContainer).getByLabelText(/Current Sides and Modifications/i);
        const priceContainer = within(productBannerContainer).getByLabelText(/Product Price and Calories/i);

        userEvent.click(await screen.findByText(/Loaded Curly Fries/i));

        expect(sides).toHaveTextContent(/Sides: Loaded Curly Fries, Coca-Cola®/i);
        expect(priceContainer).toHaveTextContent(/\$8.09 • 1540 calories/i);
    });

    test('Select a different drink, price and calories should be updated', async () => {
        const productBannerContainer = await getProductBannerContainer();
        const sides = within(productBannerContainer).getByLabelText(/Current Sides and Modifications/i);
        const priceContainer = within(productBannerContainer).getByLabelText(/Product Price and Calories/i);

        userEvent.click(await screen.findByText(/Chocolate Shake/));

        expect(sides).toHaveTextContent(/Sides: Curly Fries, Chocolate Shake/i);
        expect(priceContainer).toHaveTextContent(/\$7.59 • 1710 calories/i);
    });
});
