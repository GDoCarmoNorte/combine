/* eslint-disable testing-library/no-node-access */
import { screen, within, act, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';
import { axe } from '../../../jest/axe-helper';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';

import getBrandInfo from '../../../lib/brandInfo';
import { getPdpPageTitle } from '../../../common/helpers/getPageTitle';
import mockProducts from '../../mock-data/contentful/contentful-getEntries-product.json';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
import { getGlobalContentfulProps } from '../../../common/services/globalContentfulProps';

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/globalContentfulProps', () => ({
    getGlobalContentfulProps: jest.fn(),
}));

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const getElementCard = (container: HTMLElement, name: RegExp): HTMLElement => {
    return within(container).getByRole('listitem', { name });
};

const getProductBannerContainer = async () =>
    (
        await screen.findAllByRole('heading', {
            name: /^Smokehouse Brisket/i,
        })
    )[0].closest('div'); //TODO: fix a11y

const getProductModifiersContainer = async () => await screen.findByRole('presentation'); //TODO: a11y

const getProductSpecificModifiersContainer = (productModifiersContainer: HTMLElement, modifierName: RegExp) =>
    within(productModifiersContainer).getByRole('heading', { name: modifierName }).closest('div').parentElement; //TODO: fix a11y

const getSaveAndCloseButton = (container: HTMLElement) =>
    within(container).queryAllByRole('button', { name: /Save & Close/i })[0];

const getNutritionContainer = () => screen.getByLabelText(/Nutrition Information/i);

const getAvailableItemsOnBagContainer = () => screen.getByLabelText(/bag items/i);

const getModifySandwichLink = (container: HTMLElement) =>
    within(container).getByRole('link', {
        name: /modify sandwich/i,
    });

const addToBag = (container: HTMLElement) =>
    userEvent.click(within(container).getByRole('button', { name: /Add to bag/i, hidden: true }));

const openBag = () => userEvent.click(screen.getByRole('button', { name: /open bag/i, hidden: true }));

describe('Product Details Page - Integration', () => {
    let clearNextHeadMock;
    let productContainer, renderResult;

    beforeAll(async () => {
        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        clearNextHeadMock = mockNextHead();

        server.listen();
        server.use(contentfulHandlers);

        (getGlobalContentfulProps as jest.Mock).mockResolvedValue(globalContentfulPropsMock);
        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clearNextHeadMock();

        server.close();
    });

    beforeEach(async () => {
        const { render } = await getPage({
            route: '/menu/signature/smokehouse-brisket',
        });

        renderResult = render().container;

        await act(async () => {
            productContainer = await getProductBannerContainer();
        });
    });

    test('a11y', async () => {
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', () => {
        screen.getByText(/skip to main content/i);
    });

    test('should have correct canonical value', async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            const canonicalPath = document.querySelector("link[rel='canonical']").getAttribute('href');
            expect(canonicalPath.endsWith('/')).toBeTruthy();
        });
    });

    test("show product name, price and calories, 'MAKE IT A MEAL' & 'ADD TO BAG' & 'Modify Sandwich' buttons and what's included", async () => {
        within(productContainer).getByText(/\$5.69/i);
        within(productContainer).getByText(/600 calories/i);
        within(productContainer).getByRole('button', {
            name: /make it a meal/i,
        });
        within(productContainer).getByRole('button', { name: /add to bag/i });
        getModifySandwichLink(productContainer);

        //TODO: a11y
        const aboutContainer = screen.getByRole('heading', { name: /About the Sandwich/i }).closest('div') //TODO: fix a11y
            .parentElement;

        within(aboutContainer).getByText(/^smoked gouda/i);
        within(aboutContainer).getByText(/^Crispy Onion Strings/i);
        within(aboutContainer).getByText(/^smokey bbq sauce/i);
        within(aboutContainer).getByText(/^mayonnaise/i);
        within(aboutContainer).getByText(/^smoked brisket/i);
        within(aboutContainer).getByText(/^toasted bun/i);
        within(aboutContainer).getByText(/^Tartar Sauce/i);

        screen.getByText(
            'We set out to make a sandwich with layers of smoky flavor, and this is the result. Our brisket is smoked for at least 13 hours in a pit smoker. We top that delicious smoked beef with smoked gouda, crispy onions, mayo and BBQ sauce and serve it all on an artisan-style roll.'
        );
    });

    test('show all modifiers while modifying sandwich', async () => {
        userEvent.click(getModifySandwichLink(productContainer));

        const modifyContainer = await getProductModifiersContainer();

        const testModifiersSection = (
            sectionName: RegExp,
            modifiers: {
                name: RegExp;
                price?: RegExp;
                calories: RegExp;
                checked: boolean;
                selections?: { name: RegExp; isActive?: boolean }[];
            }[]
        ) => {
            const sectionContainer = getProductSpecificModifiersContainer(modifyContainer, sectionName);
            modifiers.forEach(({ name, price, calories, checked, selections }) => {
                const container = getElementCard(sectionContainer, name);
                if (price) {
                    within(container).getByText(price);
                }

                within(container).getByText(calories);

                within(container).getByRole('checkbox', { checked });

                if (selections) {
                    selections.forEach((it) => {
                        const button = within(container).getByRole('radio', { name: it.name });
                        expect(button).toBeInTheDocument();
                        if (it.isActive) {
                            expect(button).toHaveAttribute('aria-checked', 'true');
                        } else {
                            expect(button).toHaveAttribute('aria-checked', 'false');
                        }
                    });
                }
            });
        };

        testModifiersSection(/meat/i, [
            { name: /pepper bacon/i, checked: false, price: /\+ \$0.79/i, calories: /68 calories/i },
            { name: /double brisket/i, checked: false, price: /\+ \$2.00/i, calories: /225 calories/i },
        ]);
        testModifiersSection(/cheese/i, [
            { name: /smoked gouda/i, checked: true, calories: /69 calories/i },
            { name: /natural cheddar/i, checked: false, price: /\+ \$0.49/i, calories: /76 calories/i },
        ]);
        testModifiersSection(/veggies/i, [
            { name: /Leaf Lettuce/i, checked: false, price: /\+ \$0.29/i, calories: /1 calories/i },
            { name: /Crispy Onion Strings/i, checked: true, calories: /14 calories/i },
            { name: /tomato/i, checked: false, price: /\+ \$0.29/i, calories: /7 calories/i },
            { name: /Diced Jalapeño/i, checked: false, calories: /2 calories/i },
        ]);
        testModifiersSection(/sauces/i, [
            { name: /Mayonnaise/i, checked: true, calories: /52 calories/i },
            { name: /Smokey BBQ Sauce/i, checked: true, calories: /11 calories/i },
            { name: /ranch/i, checked: false, calories: /43 calories/i },
            {
                name: /Tartar Sauce/i,
                checked: true,
                calories: /0 calories/i,
                selections: [
                    { name: /Regular/i, isActive: true },
                    { name: /Side/i },
                    { name: /Light/i },
                    { name: /Extra/i },
                ],
            },
        ]);
        testModifiersSection(/sauce packets/i, [
            { name: /Horsey Sauce Packet/i, checked: false, calories: /14 calories/i },
            { name: /Arby's Sauce Packet/i, checked: false, calories: /14 calories/i },
            { name: /Ketchup Packet/i, checked: false, calories: /9 calories/i },
        ]);
        testModifiersSection(/OTHER CONDIMENTS/i, [
            { name: /Cup of Cheddar Sauce/i, checked: false, price: /\+ \$1.59/i, calories: /26 calories/i },
        ]);
        testModifiersSection(/Bread/i, [{ name: /No Bun/i, checked: false, calories: /Calorie info unavailable/i }]);
    });

    test('remove and change modifier with intensity', async () => {
        userEvent.click(getModifySandwichLink(productContainer));

        const modifyContainer = await getProductModifiersContainer();

        const tatarSauce = getElementCard(modifyContainer, /Tartar Sauce/i);
        const sideIntensity = within(tatarSauce).queryByRole('radio', { name: /Side/i });

        await userEvent.click(sideIntensity); // Change intensity

        within(tatarSauce).getByText(/\+ \$0.1/i);
        within(tatarSauce).getByText(/10 calories/i);

        within(productContainer).getByText(/\$5.79/i);
        within(productContainer).getByText(/610 calories/i);
        within(productContainer).getByText(/Modifications: Add Tartar Sauce\(Side\)/i);

        await userEvent.click(within(tatarSauce).queryByRole('checkbox', { checked: true })); // remove modifier

        within(productContainer).getByText(/\$5.69/i);
        within(productContainer).getByText(/600 calories/i);
        within(productContainer).getByText(/Modifications: Remove Tartar Sauce\(Regular\)/i);

        // verify price, modifiers on bag
        addToBag(productContainer);
        openBag();
        const availableItemsOnBagContainer = await getAvailableItemsOnBagContainer();
        const smokehouseBrisketContainer = within(availableItemsOnBagContainer).getByLabelText(/Smokehouse Brisket/i);
        within(smokehouseBrisketContainer).getByText(/\$5.69/i);
        within(smokehouseBrisketContainer).getByText(/Remove Tartar Sauce/i);
    });

    test("show updated price, calories and 'Pepper Bacon' modifier added after adding it", async () => {
        within(productContainer).getByText(/\$5.69/i);
        within(productContainer).getByText(/600 calories/i);

        userEvent.click(getModifySandwichLink(productContainer));

        const modifyContainer = await getProductModifiersContainer();

        within(modifyContainer).getByRole('heading', {
            name: /modify sandwich/i,
        });
        within(modifyContainer).getByRole('heading', {
            name: /Smokehouse Brisket/i,
        });

        const pepperBaconContainer = within(modifyContainer).getByText(/Pepper Bacon/).parentElement; //TODO: fix a11y

        within(pepperBaconContainer).getByText(/68 calories/i);
        expect(within(pepperBaconContainer).queryByText('+ $0.79')).not.toBeInTheDocument();

        await userEvent.click(pepperBaconContainer);
        within(pepperBaconContainer).getByText(/68 calories/i); //TODO: a11y
        within(pepperBaconContainer).queryByText('+ $0.79');

        userEvent.click(getSaveAndCloseButton(modifyContainer));

        // verify updated price, calories, modifiers on main page
        within(productContainer).getByText(/\$6.48/i);
        within(productContainer).getByText(/668 calories/i);
        within(productContainer).getByText(/add Pepper Bacon/i);

        // verify price, modifiers on bag
        addToBag(productContainer);
        openBag();
        const availableItemsOnBagContainer = await getAvailableItemsOnBagContainer();
        const smokehouseBrisketContainer = within(availableItemsOnBagContainer).getByLabelText(/Smokehouse Brisket/i);
        within(smokehouseBrisketContainer).getByText(/\$6.48/i);
        within(smokehouseBrisketContainer).getByText(/add Pepper Bacon/i);
        within(smokehouseBrisketContainer).getByText(/\$0.79/i);
    });

    test("show same price but decreased calories with 'Smoked gouda', 'Smokey BBQ Sauce', 'Mayonnaise' modifiers removed after removing them", async () => {
        userEvent.click(getModifySandwichLink(productContainer));

        const modifyContainer = await getProductModifiersContainer();

        userEvent.click(within(modifyContainer).getByText(/Smoked gouda/i));
        userEvent.click(within(modifyContainer).getByText(/Smokey BBQ Sauce/i));
        userEvent.click(within(modifyContainer).getByText(/Mayonnaise/i));

        userEvent.click(getSaveAndCloseButton(modifyContainer));

        // verify updated price and calories
        within(productContainer).getByText(/\$5.69/i);

        within(productContainer).getByText(/468 calories/i);

        within(productContainer).getByText(/Modifications:/i);
        within(productContainer).getByText(
            /Modifications: remove Smoked Gouda, remove Smokey BBQ Sauce, remove Mayonnaise/i
        );

        // verify price, modifiers on bag
        addToBag(productContainer);
        openBag();
        const availableItemsOnBagContainer = await getAvailableItemsOnBagContainer();
        const smokehouseBrisketContainer = within(availableItemsOnBagContainer).getByLabelText(/Smokehouse Brisket/i);
        within(smokehouseBrisketContainer).getByText(/\$5.69/i);
        within(smokehouseBrisketContainer).getByText(/Remove Smoked Gouda/i);
        within(smokehouseBrisketContainer).getByText(/Remove Smokey BBQ Sauce/i);
        within(smokehouseBrisketContainer).getByText(/Remove Mayonnaise/i);
    });

    test('product details page should have correct title', async () => {
        const brandInfo = getBrandInfo();

        expect(global.window).toBeDefined();
        expect(global.window.document).toBeDefined();

        expect(brandInfo).toBeDefined();
        expect(brandInfo.brandName).toBeDefined();

        const product = mockProducts.items.find((product) => product.fields.name === 'Smokehouse Brisket');

        await waitFor(() => {
            expect(global.window.document.title).toBe(getPdpPageTitle(brandInfo.brandName, product.fields.name));
        });
    });

    test('product details page should have correct meta description', async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                'content',
                "Come by and try the Smokehouse Brisket Sandwich, or any of our other delicious signature options at your local Arby's. Online ordering now available!"
            );
        });
    });

    test('script tag "application/ld+json" is present containing product schema', () => {
        // eslint-disable-next-line testing-library/no-node-access
        const content = document.querySelector('script[type="application/ld+json"]');

        expect(content).toMatchSnapshot();
    });

    test('Nutrition hidden/visible', async () => {
        const scrollMock = jest.fn();
        global.scroll = scrollMock;

        const nutritionContainer = getNutritionContainer();

        within(nutritionContainer).getByText(/View nutrition information/i);
        expect(within(nutritionContainer).queryByText(/Hide nutrition information/i)).not.toBeInTheDocument();

        userEvent.click(within(nutritionContainer).getByText(/View nutrition information/i));

        within(nutritionContainer).getByText(/Hide nutrition information/i);
        expect(within(nutritionContainer).queryByText(/View nutrition information/i)).not.toBeInTheDocument();

        const nutritionItems = [
            ['Serving Weight (g)', '203'],
            ['Calories', '600'],
            ['Calories from Fat', '310'],
            ['Fat - Total (g)', '35'],
            ['Saturated Fat (g)', '12'],
            ['Trans Fat (g)', '1'],
            ['Cholesterol (mg)', '100'],
            ['Sodium (mg)', '1250'],
            ['Total Carbohydrates (g)', '42'],
            ['Dietary Fiber (g)', '3'],
            ['Sugars (g)', '8'],
            ['Proteins (g)', '34'],
        ];

        nutritionItems.forEach(([key, value]) => {
            expect(nutritionContainer).toHaveTextContent(`${key}${value}`);
        });

        // hide back
        userEvent.click(within(nutritionContainer).getByText(/Hide nutrition information/i));

        expect(scrollMock).toHaveBeenCalled();

        within(nutritionContainer).getByText(/View nutrition information/i);
        expect(within(nutritionContainer).queryByText(/Hide nutrition information/i)).not.toBeInTheDocument();

        nutritionItems.forEach(([key, value]) => {
            expect(nutritionContainer).not.toHaveTextContent(`${key}${value}`);
        });
    });

    test('show sides modifiers in bag for combo menu item: ketchup packet', async () => {
        userEvent.click(getModifySandwichLink(productContainer));

        const modifyContainer = await getProductModifiersContainer();

        const ketchupPacketContainer = within(modifyContainer).getByText(/Ketchup Packet/).parentElement; //TODO: fix a11y
        userEvent.click(ketchupPacketContainer);
        userEvent.click(getSaveAndCloseButton(modifyContainer));

        // verify updated price and calories
        within(productContainer).getByText(/\$5.69/i);
        within(productContainer).getByText(/609 calories/i);
        within(productContainer).getByText(/Modifications: add Ketchup Packet/i);

        // verify price, modifiers on bag
        addToBag(productContainer);
        openBag();
        const availableItemsOnBagContainer = await getAvailableItemsOnBagContainer();
        const smokehouseBrisketContainer = within(availableItemsOnBagContainer).getByLabelText(/Smokehouse Brisket/i);
        within(smokehouseBrisketContainer).getByText(/\$5.69/i);
        within(smokehouseBrisketContainer).getByText(/Add Ketchup Packet/i);
    });
});
