import { screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';
import { getGlobalContentfulProps } from '../../../common/services/globalContentfulProps';

jest.mock('../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: () => true,
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/pdpXfor$YPromoStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/pdpXfor$YPromoStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/pdpXfor$YPromoStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());
jest.mock('../../../common/services/globalContentfulProps', () => ({
    getGlobalContentfulProps: jest.fn(),
}));

const getProductBannerContainer = async () => screen.getByLabelText('Product Main Section');

const getSandwich1Section = async () => screen.getByLabelText(/Sandwich 1 Section/i);
const getSandwich2Section = async () => screen.getByLabelText(/Sandwich 2 Section/i);

const getElementCard = (container: HTMLElement, name: RegExp): HTMLElement => {
    return within(container).getByRole('listitem', { name });
};

const getProductModifiersContainer = async () => screen.findByRole('presentation');
const getProductSpecificModifiersContainer = (productModifiersContainer: HTMLElement, modifierName: RegExp) =>
    // eslint-disable-next-line testing-library/no-node-access
    within(productModifiersContainer).getByRole('heading', { name: modifierName }).closest('div').parentElement; //TODO: fix a11y

const testModifiersSection = (
    modifyContainer: HTMLElement,
    sectionName: RegExp,
    modifiers: {
        name: RegExp;
        price?: RegExp;
        calories: RegExp;
        checked: boolean;
        selections?: { name: RegExp; isActive?: boolean }[];
    }[]
) => {
    const sectionContainer = getProductSpecificModifiersContainer(modifyContainer, sectionName);
    modifiers.forEach(({ name, price, calories, checked, selections }) => {
        const container = getElementCard(sectionContainer, name);
        if (price) {
            within(container).getByText(price);
        }

        within(container).getByText(calories);

        within(container).getByRole('checkbox', { checked });

        if (selections) {
            selections.forEach((it) => {
                const button = within(container).getByRole('radio', { name: it.name });
                expect(button).toBeInTheDocument();
                if (it.isActive) {
                    expect(button).toHaveAttribute('aria-checked', 'true');
                } else {
                    expect(button).toHaveAttribute('aria-checked', 'false');
                }
            });
        }
    });
};

describe('PDP - Meal integration', () => {
    beforeAll(async () => {
        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);
        (getGlobalContentfulProps as jest.Mock).mockResolvedValue(globalContentfulPropsMock);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        server.close();
    });

    beforeEach(async () => {
        const { render } = await getPage({
            route: '/menu/limited-time/2-for-6-every-day-value',
        });
        await render();
    });

    test('SkipLink exists on the page', () => {
        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('Combo details displayed', async () => {
        const productBannerContainer = await getProductBannerContainer();
        const defaultSides = within(productBannerContainer).getByLabelText(/Current Sides and Modifications/i);
        const priceContainer = within(productBannerContainer).getByLabelText(/Product Price and Calories/i);

        within(productBannerContainer).getByRole('heading', { name: '2 for $6 Everyday Value', level: 1 });
        expect(defaultSides).toHaveTextContent(/Selections: Classic Beef 'n Cheddar, Classic Beef 'n Cheddar/i);
        expect(priceContainer).toHaveTextContent(/\$6.00 • 900 calories/i);

        const sandwich1Section = await getSandwich1Section();

        const classicBNC1 = getElementCard(sandwich1Section, /Classic Beef 'n Cheddar/i);
        within(classicBNC1).getByText(/450 calories/i);
        within(classicBNC1).getByRole('checkbox', { checked: true });

        const greekGyro1 = getElementCard(sandwich1Section, /^Greek Gyro/i);
        within(greekGyro1).getByText(/710 calories/i);
        within(greekGyro1).getByRole('checkbox', { checked: false });

        const roastBeefGyro1 = getElementCard(sandwich1Section, /^Roast Beef Gyro/i);
        within(roastBeefGyro1).getByText(/550 calories/i);
        within(greekGyro1).getByRole('checkbox', { checked: false });

        const roastTurkeyGyro1 = getElementCard(sandwich1Section, /^Roast Turkey Gyro/i);
        within(roastTurkeyGyro1).getByText(/470 calories/i);
        within(roastTurkeyGyro1).getByRole('checkbox', { checked: false });

        const spicyGreekGyro1 = getElementCard(sandwich1Section, /^Spicy Greek Gyro/i);
        within(spicyGreekGyro1).getByText(/720 calories/i);
        within(spicyGreekGyro1).getByRole('checkbox', { checked: false });

        const chickenTenders3pc1 = getElementCard(sandwich1Section, /^Chicken Tenders 3PC/i);
        within(chickenTenders3pc1).getByText(/370 calories/i);
        within(chickenTenders3pc1).getByRole('checkbox', { checked: false });

        const classicBNC1ActionCodeTest1 = getElementCard(sandwich1Section, /^Classic BNC Action Code Test/i);
        within(classicBNC1ActionCodeTest1).getByText(/450 calories/i);
        within(classicBNC1ActionCodeTest1).getByRole('checkbox', { checked: false });

        const sandwich2Section = await getSandwich2Section();
        const classicBNC2 = getElementCard(sandwich2Section, /Classic Beef 'n Cheddar/i);

        within(classicBNC2).getByText(/450 calories/i);
        within(classicBNC2).getByRole('checkbox', { checked: true });

        const greekGyro2 = getElementCard(sandwich2Section, /^Greek Gyro/i);
        within(greekGyro2).getByText(/710 calories/i);
        within(greekGyro2).getByRole('checkbox', { checked: false });

        const roastBeefGyro2 = getElementCard(sandwich2Section, /^Roast Beef Gyro/i);
        within(roastBeefGyro2).getByText(/550 calories/i);
        within(roastBeefGyro2).getByRole('checkbox', { checked: false });

        const roastTurkeyGyro2 = getElementCard(sandwich2Section, /^Roast Turkey Gyro/i);
        within(roastTurkeyGyro2).getByText(/470 calories/i);
        within(roastTurkeyGyro2).getByRole('checkbox', { checked: false });

        const spicyGreekGyro2 = getElementCard(sandwich2Section, /^Spicy Greek Gyro/i);
        within(spicyGreekGyro2).getByText(/720 calories/i);
        within(spicyGreekGyro2).getByRole('checkbox', { checked: false });

        const chickenTenders3pc2 = getElementCard(sandwich2Section, /^Chicken Tenders 3PC/i);
        within(chickenTenders3pc2).getByText(/370 calories/i);
        within(chickenTenders3pc2).getByRole('checkbox', { checked: false });

        const classicBNC1ActionCodeTest2 = getElementCard(sandwich2Section, /^Classic BNC Action Code Test/i);
        within(classicBNC1ActionCodeTest2).getByText(/450 calories/i);
        within(classicBNC1ActionCodeTest2).getByRole('checkbox', { checked: false });
    });

    test('Display sandwich modifiers', async () => {
        const sandwich1Section = await getSandwich1Section();
        const classicBNC1ActionCodeTest1 = getElementCard(sandwich1Section, /^Classic BNC Action Code Test/i);
        userEvent.click(
            within(classicBNC1ActionCodeTest1).getByRole('button', {
                name: /modify/i,
            })
        );

        const modifyContainer = await getProductModifiersContainer();

        testModifiersSection(modifyContainer, /meats/i, [
            { name: /pepper bacon/i, checked: false, price: /\+ \$0.99/i, calories: /68 calories/i },
        ]);
        testModifiersSection(modifyContainer, /cheese/i, [
            { name: /natural cheddar/i, checked: false, price: /\+ \$0.49/i, calories: /76 calories/i },
            { name: /Big Eye Swiss/i, checked: false, price: /\+ \$0.49/i, calories: /76 calories/i },
            {
                name: /Cheddar sauce/i,
                checked: true,
                calories: /26 calories/i,
                selections: [
                    { name: /Light/i },
                    { name: /Regular/i, isActive: true },
                    { name: /Extra/i },
                    { name: /On Side/i },
                ],
            },
        ]);
        testModifiersSection(modifyContainer, /veggies/i, [
            { name: /Leaf Lettuce/i, checked: false, price: /\+ \$0.29/i, calories: /1 calories/i },
            { name: /Crispy Onion Strings/i, checked: false, calories: /74 calories/i },
            { name: /Red Onion/i, checked: false, calories: /2 calories/i },
            { name: /tomato/i, checked: false, price: /\+ \$0.29/i, calories: /7 calories/i },
            { name: /Diced Jalapeño/i, checked: false, calories: /2 calories/i },
        ]);
        testModifiersSection(modifyContainer, /sauces/i, [
            {
                name: /Red Ranch/i,
                checked: true,
                calories: /69 calories/i,
                selections: [
                    { name: /Light/i },
                    { name: /Regular/i, isActive: true },
                    { name: /Extra/i },
                    { name: /On Side/i },
                ],
            },
        ]);
        testModifiersSection(modifyContainer, /sauce packets/i, [
            { name: /Horsey Sauce Packet/i, checked: false, calories: /14 calories/i },
            { name: /Arby's Sauce Packet/i, checked: false, calories: /14 calories/i },
            { name: /Ketchup Packet/i, checked: false, calories: /9 calories/i },
        ]);
        testModifiersSection(modifyContainer, /OTHER CONDIMENTS/i, [
            { name: /Cup of Cheddar Sauce/i, checked: false, price: /\+ \$0.59/i, calories: /26 calories/i },
        ]);
        testModifiersSection(modifyContainer, /Bread/i, [
            { name: /No Bun/i, checked: false, calories: /Calorie info unavailable/i },
            { name: /Sesame Seed Bun/i, checked: false, calories: /Calorie info unavailable/i },
        ]);
    });

    test('remove and change modifier with intensity', async () => {
        const productBannerContainer = await getProductBannerContainer();
        const selectionsTextContainer = within(productBannerContainer).getByLabelText(
            /Current Sides and Modifications/i
        );
        const priceContainer = within(productBannerContainer).getByLabelText(/Product Price and Calories/i);

        const sandwich1Section = await getSandwich1Section();
        const classicBNC1ActionCodeTest1 = getElementCard(sandwich1Section, /^Classic BNC Action Code Test/i);
        userEvent.click(
            within(classicBNC1ActionCodeTest1).getByRole('button', {
                name: /modify/i,
            })
        );

        const modifyContainer = await getProductModifiersContainer();

        const cheddarSauce = getElementCard(modifyContainer, /^Cheddar Sauce/i);
        const sideIntensity = within(cheddarSauce).queryByRole('radio', { name: /Extra/i });

        userEvent.click(sideIntensity); // Change intensity

        within(classicBNC1ActionCodeTest1).getByText(/460 calories/i);
        within(classicBNC1ActionCodeTest1).getByText(/Add Cheddar Sauce\(Extra\)/i);

        expect(priceContainer).toHaveTextContent(/\$6.49 • 910 calories/i);
        expect(selectionsTextContainer).toHaveTextContent(
            /Selections: Classic BNC Action Code Test, Classic Beef 'n Cheddar/i
        );

        userEvent.click(within(cheddarSauce).queryByRole('checkbox', { checked: true })); // remove modifier

        within(classicBNC1ActionCodeTest1).getByText(/424 calories/i);
        within(classicBNC1ActionCodeTest1).getByText(/Remove Cheddar Sauce\(Regular\)/i);

        expect(priceContainer).toHaveTextContent(/\$6.00 • 874 calories/i);
    });
});
