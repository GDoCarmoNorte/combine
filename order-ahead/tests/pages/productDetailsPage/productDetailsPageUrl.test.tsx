import React from 'react';
import { mount } from 'enzyme';
import { useDispatch } from 'react-redux';

import ProductDetailPage, {
    IProductDetailsPageProps,
} from '../../../pages/menu/[menuCategoryUrl]/[productDetailsPageUrl]';
import usePdp from '../../../redux/hooks/usePdp';
import useNavIntercept from '../../../redux/hooks/useNavIntercept';
import NotificationModal from '../../../components/molecules/notificationModal';
import { actImmediate } from '../../components/clientOnly/formik/formikTestUtils';

import featureFlagsMock from '../../mocks/featureFlags.mock';

jest.mock('next/head', () => () => 'Head');
jest.mock('../../../components/organisms/header/baseHeader', () => () => 'BaseHeader');
jest.mock('../../../components/organisms/footer', () => () => 'Footer');
jest.mock('../../../components/atoms/Breadcrumbs', () => () => 'Breadcrumbs');
jest.mock('../../../components/atoms/button', () => ({ InspireButton: () => 'InspireButton' }));
jest.mock('../../../components/sections/aboutSection/aboutSection', () => () => 'AboutSection');

jest.mock('react-redux');
jest.mock('next/dynamic', () =>
    jest
        .fn()
        .mockReturnValueOnce(() => 'FloatingBanner')
        .mockReturnValueOnce(() => 'ProductBanner')
);

jest.mock('next/router', () => ({
    useRouter: jest.fn().mockImplementation(() => ({ asPath: '/' })),
}));

jest.mock('../../../redux/hooks/domainMenu', () => ({
    useProductSizes: jest.fn(),
    useNutrition: jest.fn(),
    useFullProduct: jest.fn().mockReturnValue({ rootProduct: {} }),
    usePriceAndCalories: jest.fn().mockReturnValue({ price: 'price' }),
    useFullProductSizes: jest.fn(),
    useDomainProduct: jest.fn(),
    useDomainProductByContentfulFields: jest.fn(),
    reducer: jest.fn().mockReturnValue({}),
    useDomainMenuSelectors: jest.fn().mockReturnValue({
        selectRelatedComboByMainProductIdAndSizeId: 'id',
        selectDefaultTallyItem: 'id',
    }),
}));

jest.mock('../../../redux/hooks/pdp/', () => ({
    useDisplayProduct: jest.fn().mockReturnValue({
        productSections: [],
    }),
    useDisplayModifierGroupsByProductId: jest.fn(),
}));

jest.mock('../../../common/hooks/useLocalization', () => ({
    useLocalization: jest.fn().mockReturnValue({ locationLinkText: 'locationLinkText', isProductAvailable: false }),
}));

jest.mock('../../../common/hooks/useProductIsSaleable', () => ({
    useProductIsSaleable: () => ({
        isSaleable: true,
    }),
}));

jest.mock('../../../redux/hooks/usePdp', () => jest.fn());
jest.mock('../../../redux/hooks/useBag', () => jest.fn());
jest.mock('../../../common/helpers/schemaPdpAndPlp', () => jest.fn());
jest.mock('../../../redux/hooks/useNavIntercept', () => jest.fn());
jest.mock('../../../lib/domainProduct', () => ({
    getUpgradeMealPath: jest.fn().mockReturnValue({}),
}));

jest.mock('../../../common/hooks/pdpHooks.ts', () => ({
    useComboPriceIncreaseAnalytics: jest.fn(),
}));

jest.mock('../../../redux/hooks/useGlobalProps', () =>
    jest.fn().mockReturnValue({
        footer: {},
        alertBanners: {},
        navigation: {},
        modifierItemsById: {},
        productsById: 'productsById',
    })
);

const renderComponent = () => {
    const wrapper = mount(
        <ProductDetailPage
            canonicalPath="canonicalPath"
            currentCategory={{} as IProductDetailsPageProps['currentCategory']}
            productNutritionLink={null}
            product={{ fields: { productId: 'productId', name: 'name' } } as IProductDetailsPageProps['product']}
            featureFlags={featureFlagsMock}
        />
    );

    return wrapper;
};

describe('ProductDetailPage', () => {
    it('should match snapshot', () => {
        (useDispatch as jest.Mock).mockReturnValue(jest.fn());

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useInitTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useHasUnsavedModifications: jest.fn().mockReturnValue(true),
        });

        (useNavIntercept as jest.Mock).mockReturnValue({
            isPending: false,
            hasChanges: false,
            navigate: jest.fn(),
        });

        const view = renderComponent();

        expect(view).toMatchSnapshot();
    });

    it('should intercept redirect if has unsaved modifications and cancen interception if doesnt', async () => {
        (useDispatch as jest.Mock).mockReturnValue(jest.fn());

        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useInitTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useHasUnsavedModifications: jest.fn().mockReturnValue(true),
            actions: {
                resetPdpState: jest.fn(),
            },
        });

        const navigate = jest.fn();

        (useNavIntercept as jest.Mock).mockReturnValueOnce({
            isPending: true,
            navigate,
        });

        const view = renderComponent();

        await actImmediate(view);

        expect(useNavIntercept).toHaveBeenCalled();

        const notificationModal = view.find(NotificationModal).at(0);

        expect(notificationModal).toBeDefined();

        notificationModal.find('span.brand-icon').at(0).simulate('click');

        expect(navigate).toBeCalledWith(true);
    });

    it('should proceed navigation if notification modal is dismissed', () => {
        const onDismiss = jest.fn();
        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useInitTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useHasUnsavedModifications: jest.fn().mockReturnValue(false),
            actions: {
                resetPdpState: onDismiss,
            },
        });

        const view = renderComponent();
        const { onClick } = view.find(NotificationModal).props().confirmButtonProps;
        onClick({} as any);
        expect(onDismiss).toBeCalled();
    });

    it('should stop navigation if notification modal is accepted', () => {
        const onConfirm = jest.fn();
        (usePdp as jest.Mock).mockReturnValue({
            pdpTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useInitTallyItem: jest.fn().mockReturnValue({ productId: 'productId' }),
            useHasUnsavedModifications: jest.fn().mockReturnValue(false),
        });

        (useNavIntercept as jest.Mock).mockReturnValueOnce({
            isPending: true,
            navigate: onConfirm,
        });
        const view = renderComponent();
        const { onClick } = view.find(NotificationModal).props().rejectButtonProps;
        onClick({} as any);
        expect(onConfirm).toBeCalled();
    });
});
