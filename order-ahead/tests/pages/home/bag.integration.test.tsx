import { act, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';

import { axe } from '../../../jest/axe-helper';
import { getPage } from '../../lib/integration/getPage';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isOAEnabled: jest.fn().mockReturnValue(true),
        isAccountOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(true),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isMainDealsBannerOn: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const getBagIcon = () => screen.queryByLabelText(/bag toggler/i);
const getBagCounter = () => screen.queryByLabelText(/bag counter/i);
const getBagCloseButton = () => screen.queryByText(/close bag/i);
const getAddToBagButtons = () => screen.queryAllByText(/add to bag/i);
const getModalContainer = () => screen.queryByRole(/presentation/i);

const findBagCounter = async () => await screen.findByLabelText(/bag counter/i);
const findBagCloseButton = async () => await screen.findByText(/close bag/i);
const findModalContainer = async () => await screen.findByRole(/presentation/i);
const findBagList = async (modalContainer: HTMLElement) => await within(modalContainer).findByLabelText(/bag items/i);

describe('Home - Bag component integration', () => {
    beforeAll(async () => {
        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    let renderResult;

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({ route: '/' });

            renderResult = render().container;
        });
    });

    test('a11y', async () => {
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', () => {
        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('Bag should be closed, Bag icon should be displayed with no items', async () => {
        expect(getBagIcon()).toBeInTheDocument();
        expect(getBagCounter()).not.toBeInTheDocument();
        expect(getBagCloseButton()).not.toBeInTheDocument();
    });

    test('Bag should be displayed with no items on Bag icon click', async () => {
        expect(getBagIcon()).toBeInTheDocument();

        userEvent.click(getBagIcon());

        await findBagCloseButton();
    });

    test('Bag should be closed on other Bag icon click', async () => {
        expect(getBagIcon()).toBeInTheDocument();

        userEvent.click(getBagIcon());

        await findBagCloseButton();

        userEvent.click(getBagIcon());

        await waitFor(() => {
            expect(getBagCloseButton()).not.toBeInTheDocument();
        });
    });

    test('Bag should contain product after product added from home page', async () => {
        expect(getBagIcon()).toBeInTheDocument();

        expect(getBagCounter()).not.toBeInTheDocument();
        expect(getBagCloseButton()).not.toBeInTheDocument();

        const addToBagButton = getAddToBagButtons()[0];

        userEvent.click(addToBagButton);

        await findBagCounter();

        userEvent.click(getBagIcon());

        await findBagCloseButton();

        const modalContainer = await findModalContainer();

        // retrieve product name from gtm id data attribute
        const productName = addToBagButton.dataset['gtmId'].split('-').pop().replace(/_/g, ' ');

        await within(modalContainer).findByText(new RegExp(productName, 'i'));
    });

    test('Bag total should be updated on Bag item increment', async () => {
        // King's Hawaiian Fish Deluxe Meal - $3.41 + $1.79 + $1.69 = 6.89
        const addToBagButton = getAddToBagButtons()[0];

        userEvent.click(addToBagButton);

        await findBagCounter();

        userEvent.click(getBagIcon());

        const modalContainer = await findModalContainer();
        const bagList = await findBagList(modalContainer);

        const incrementButton = within(bagList).queryByLabelText(/increase quantity/i);

        // $6.89 + $6.89 = $13.78
        userEvent.click(incrementButton);

        await waitFor(() => {
            const totalNode = within(modalContainer).queryByLabelText(/bag total price/i);

            expect(totalNode.textContent).toEqual('$13.78');
        });
    });

    test('Bag should be empty on remove single bag item', async () => {
        const addToBagButton = getAddToBagButtons()[0];

        userEvent.click(addToBagButton);

        await findBagCounter();

        userEvent.click(getBagIcon());

        const modalContainer = await findModalContainer();
        const bagList = await findBagList(modalContainer);

        const removeButton = within(bagList).getByText(/remove/i);

        userEvent.click(removeButton);

        await waitFor(() => {
            expect(removeButton).not.toBeInTheDocument();
        });

        within(modalContainer).getByText(/was removed!/i);
    });

    test('Bag should display other added product and correct total', async () => {
        let bagCounter;

        expect(getAddToBagButtons().length).toBeGreaterThan(1);

        // King's Hawaiian Fish Deluxe Meal - $3.41 + $1.79 + $1.69 = 6.89
        userEvent.click(getAddToBagButtons()[0]);

        bagCounter = await findBagCounter();
        expect(bagCounter.textContent).toBe('1');

        // Crispy Fish Sandwich Meal - $2.41 + $1.79 + $1.69 = $5.89
        userEvent.click(getAddToBagButtons()[1]);

        bagCounter = await findBagCounter();
        expect(getBagCounter().textContent).toBe('2');

        userEvent.click(getBagIcon());

        const modalContainer = await findModalContainer();
        const bagList = await findBagList(modalContainer);

        const totalNode = within(modalContainer).queryByLabelText(/bag total price/i);
        const removeButtons = within(bagList).queryAllByLabelText(/remove bag item/i);

        expect(removeButtons.length).toBe(2);

        // $6.89 + $5.89 = $12.78
        expect(totalNode.textContent).toEqual('$12.78');
    });

    test('Bag should display two added products and the only product after second remove', async () => {
        let bagCounter;

        expect(getAddToBagButtons().length).toBeGreaterThan(1);

        // $6.89
        userEvent.click(getAddToBagButtons()[0]);

        bagCounter = await findBagCounter();
        expect(bagCounter.textContent).toBe('1');

        // $5.89
        userEvent.click(getAddToBagButtons()[1]);

        bagCounter = await findBagCounter();
        expect(bagCounter.textContent).toBe('2');

        userEvent.click(getBagIcon());

        const modalContainer = await getModalContainer();
        const bagList = await findBagList(modalContainer);

        const getBagTotal = () => within(modalContainer).queryByLabelText(/bag total price/i);
        const getRemoveButtons = () => within(bagList).queryAllByLabelText(/remove bag item/i);

        expect(getRemoveButtons().length).toBe(2);

        // $6.89 + $5.89 = $12.78
        expect(getBagTotal().textContent).toEqual('$12.78');

        userEvent.click(getRemoveButtons()[1]);

        await waitFor(() => {
            expect(getRemoveButtons().length).toBe(1);
        });
        expect(getBagTotal().textContent).toEqual('$6.89');
    });

    test('Unavailable section should be desplayed', async () => {
        expect(getBagIcon()).toBeInTheDocument();

        userEvent.click(getBagIcon());

        const modalContainer = await getModalContainer();

        expect(within(modalContainer).getByText(/unavailable/i, { selector: 'h4' })).toBeInTheDocument();
        expect(within(modalContainer).getByText(/remove all/i, { selector: 'button' })).toBeInTheDocument();
        expect(
            within(modalContainer).getByText(/these items are currently unavailable at this location/i)
        ).toBeInTheDocument();
    });

    test('Unavailable section should be removed after [Remove All] clicked', async () => {
        expect(getBagIcon()).toBeInTheDocument();

        userEvent.click(getBagIcon());

        const modalContainer = await getModalContainer();
        const removeAll = within(modalContainer).queryByText(/remove all/i, { selector: 'button' });

        userEvent.click(removeAll);

        await waitFor(() => {
            expect(within(modalContainer).queryByText(/unavailable/i, { selector: 'h4' })).not.toBeInTheDocument();
        });

        expect(
            within(modalContainer).queryByText(/these items are currently unavailable at this location/i)
        ).not.toBeInTheDocument();
        expect(within(modalContainer).getByText(/there’s no meat here/i)).toBeInTheDocument();
    });

    test('Item count should not show in bag if no items', async () => {
        userEvent.click(getBagIcon());
        const modalContainer = await getModalContainer();

        expect(within(modalContainer).queryByLabelText(/bag item count/i)).not.toBeInTheDocument();
    });

    test('Item count should show 1 in bag if there is 1 item', async () => {
        userEvent.click(getAddToBagButtons()[0]);
        userEvent.click(getBagIcon());
        const modalContainer = await getModalContainer();
        const getItemCountTotal = () => within(modalContainer).queryByLabelText(/bag item count/i);
        expect(getItemCountTotal().textContent).toEqual('(1 ITEM)');
    });

    test('Item count should show as plural in bag if there is more than 1 item', async () => {
        userEvent.click(getBagIcon());
        userEvent.click(getAddToBagButtons()[0]);
        userEvent.click(getAddToBagButtons()[1]);
        const modalContainer = await getModalContainer();
        const getItemCountTotal = () => within(modalContainer).queryByLabelText(/bag item count/i);
        expect(getItemCountTotal().textContent).toEqual(`(2 ITEMS)`);
    });

    afterAll(() => {
        server.close();
    });
});
