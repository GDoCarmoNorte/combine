/* eslint-disable testing-library/no-node-access */
import { act, screen, within, waitFor } from '@testing-library/react';
import { axe } from '../../../jest/axe-helper';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { isCaloriesDisplayEnabled } from '../../../lib/getFeatureFlags';

jest.mock('../../../lib/getFeatureFlags', () => {
    const originalModule = jest.requireActual('../../../lib/getFeatureFlags');

    return {
        __esModule: true,
        ...originalModule,
        isCaloriesDisplayEnabled: jest.fn().mockReturnValue(true),
    };
});
import { getPage } from '../../lib/integration/getPage';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps, usePersonalization } from '../../../redux/hooks';

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());
jest.mock('../../../redux/hooks/usePersonalization', () => ({ usePersonalization: jest.fn() }));

const getTopPicksContainer = () => screen.getByLabelText(/Top Pick Products/i);
const getMenuCategoryContainer = () => screen.getByLabelText(/Category Menu Selector/i);
const getInfoBlockContainer = () => screen.getByText(/More to Love/i).closest('.sectionWrapper') as HTMLElement; //TODO: fix a11y

const getTopPicksItems = () => {
    const topPicksContainer = getTopPicksContainer();
    return within(topPicksContainer).getAllByLabelText(/Product Card/i);
};
const getCategoryCards = () => {
    const menuCategoryContainer = getMenuCategoryContainer();
    return within(menuCategoryContainer).getAllByLabelText(/Menu Category/i);
};

describe('Home - Menu integration', () => {
    let clearNextHeadMock, renderResult;

    const renderPage = async () =>
        await act(async () => {
            const { render } = await getPage({ route: '/' }); //

            renderResult = render().container; //
        });

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clearNextHeadMock();

        server.close();
    });

    beforeEach(() => {
        (usePersonalization as jest.Mock).mockReturnValue({
            loading: false,
            actions: { initializePersonalizationDependency: jest.fn(), getPersonalizedSection: jest.fn() },
        });
    });

    test('a11y', async () => {
        await renderPage();
        const results = await axe(renderResult);
        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', async () => {
        await renderPage();

        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('“Top Picks” section should display 6 number of products', async () => {
        await renderPage();

        const topPicksItems = getTopPicksItems();
        expect(topPicksItems.length).toBe(6);
    });

    test('Each product from “Top Picks” section should display specific values (name, price, calories)  and “ADD TO BAG” & “MODIFY” buttons', async () => {
        await renderPage();

        const topPicksContainer = getTopPicksContainer();

        const getProductCardByName = (name: RegExp): HTMLElement => {
            return within(topPicksContainer).getByText(name).closest('[aria-label="Product Card"]'); //TODO: fix a11y
        };

        const doubleRoastBeefMeal = getProductCardByName(/^Double Roast Beef Meal/i);
        within(doubleRoastBeefMeal).getByText(/\$7.59/);
        within(doubleRoastBeefMeal).getByText(/1080 calories/i);
        within(doubleRoastBeefMeal).getByText(/ADD TO BAG/i);
        within(doubleRoastBeefMeal).getByText(/MODIFY/i);

        const chickenBaconSwissSandwichMeal = getProductCardByName(/^Chicken Bacon & Swiss Sandwich Meal/i);
        within(chickenBaconSwissSandwichMeal).getByText(/\$7.49/);
        within(chickenBaconSwissSandwichMeal).getByText(/1180 calories/i);
        within(chickenBaconSwissSandwichMeal).getByText(/ADD TO BAG/i);
        within(chickenBaconSwissSandwichMeal).getByText(/MODIFY/i);

        const deepFriedTurkeyCranberrySandwichMeal = getProductCardByName(/^Classic Beef n Cheddar Meal/i);
        within(deepFriedTurkeyCranberrySandwichMeal).getByText(/\$6.39/);
        within(deepFriedTurkeyCranberrySandwichMeal).getByText(/1020 calories/i);
        within(deepFriedTurkeyCranberrySandwichMeal).getByText(/ADD TO BAG/i);
        within(deepFriedTurkeyCranberrySandwichMeal).getByText(/MODIFY/i);

        const smokehouseBrisketMeal = getProductCardByName(/^Smokehouse Brisket Meal/i);
        within(smokehouseBrisketMeal).getByText(/\$7.99/);
        within(smokehouseBrisketMeal).getByText(/1170 calories/i);
        within(smokehouseBrisketMeal).getByText(/ADD TO BAG/i);
        within(smokehouseBrisketMeal).getByText(/MODIFY/i);

        const everyDayValue = getProductCardByName(/^2\/\$6 Every Day Value/i);
        within(everyDayValue).getByText(/\$6.00/);
        within(everyDayValue).getByText(/900 calories/i);
        within(everyDayValue).getByText(/ADD TO BAG/i);
        within(everyDayValue).getByText(/MODIFY/i);
    });

    test('Each product from “Top Picks” section should not display calories value for Canada website', async () => {
        (isCaloriesDisplayEnabled as jest.Mock).mockReturnValue(false);
        await renderPage();

        const topPicksContainer = getTopPicksContainer();

        const getProductCardByName = (name: RegExp): HTMLElement => {
            return within(topPicksContainer).getByText(name).closest('[aria-label="Product Card"]'); //TODO: fix a11y
        };

        const doubleRoastBeefMeal = getProductCardByName(/^Double Roast Beef Meal/i);

        expect(within(doubleRoastBeefMeal).queryByText(/1080 calories/i)).not.toBeInTheDocument();

        const chickenBaconSwissSandwichMeal = getProductCardByName(/^Chicken Bacon & Swiss Sandwich Meal/i);

        expect(within(chickenBaconSwissSandwichMeal).queryByText(/1180 calories/i)).not.toBeInTheDocument();

        const deepFriedTurkeyCranberrySandwichMeal = getProductCardByName(/^Classic Beef n Cheddar Meal/i);

        expect(within(deepFriedTurkeyCranberrySandwichMeal).queryByText(/1020 calories/i)).not.toBeInTheDocument();

        const smokehouseBrisketMeal = getProductCardByName(/^Smokehouse Brisket Meal/i);

        expect(within(smokehouseBrisketMeal).queryByText(/1170 calories/i)).not.toBeInTheDocument();

        const everyDayValue = getProductCardByName(/^2\/\$6 Every Day Value/i);

        expect(within(everyDayValue).queryByText(/900 calories/i)).not.toBeInTheDocument();
    });

    test('“Our menu” section should display 11 number of categories', async () => {
        await renderPage();

        const categoryCards = getCategoryCards();
        expect(categoryCards.length).toBe(11);
    });

    test('Each category from “our menu” section should display specific name, along with specific number of items and “ORDER” button', async () => {
        await renderPage();

        const menuCategoryContainer = getMenuCategoryContainer();

        const getCategoryCardByName = (name: RegExp): HTMLElement => {
            return within(menuCategoryContainer).getByText(name).closest('[aria-label="Menu Category"]'); //TODO: fix a11y
        };

        const meals = getCategoryCardByName(/^Meals/i);
        within(meals).getByText(/32 items/i);
        within(meals).getByText(/ORDER/i);

        const Limited = getCategoryCardByName(/^Limited Time/i);
        within(Limited).getByText(/6 items/i);
        within(Limited).getByText(/ORDER/i);

        const Signature = getCategoryCardByName(/^Signature/i);
        within(Signature).getByText(/6 items/i);
        within(Signature).getByText(/ORDER/i);

        const Market = getCategoryCardByName(/^Market Fresh/i);
        within(Market).getByText(/9 items/i);
        within(Market).getByText(/ORDER/i);

        const Chicken = getCategoryCardByName(/^Chicken/i);
        within(Chicken).getByText(/8 items/i);
        within(Chicken).getByText(/ORDER/i);

        const Roast = getCategoryCardByName(/^Roast Beef/i);
        within(Roast).getByText(/7 items/i);
        within(Roast).getByText(/ORDER/i);

        const Sliders = getCategoryCardByName(/^Sliders/i);
        within(Sliders).getByText(/7 items/i);
        within(Sliders).getByText(/ORDER/i);

        const Sides = getCategoryCardByName(/^Sides/i);
        within(Sides).getByText(/7 items/i);
        within(Sides).getByText(/ORDER/i);

        const Desserts = getCategoryCardByName(/^Desserts/i);
        within(Desserts).getByText(/8 items/i);
        within(Desserts).getByText(/ORDER/i);

        const Drinks = getCategoryCardByName(/^Drinks/i);
        within(Drinks).getByText(/14 items/i);
        within(Drinks).getByText(/ORDER/i);

        const Kids = getCategoryCardByName(/^Kids Menu/i);
        within(Kids).getByText(/10 items/i);
        within(Kids).getByText(/ORDER/i);
    });

    test('home page should have correct meta description', async () => {
        await renderPage();

        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                'content',
                "Arby's sandwich shops are known for slow roasted roast beef, turkey, and premium Angus beef sandwiches, sliced fresh every day."
            );
        });
    });

    test('home page should not contain footer', async () => {
        await renderPage();

        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('footer')).toBeNull();
        });
    });

    test('home page should have correct headings', async () => {
        await renderPage();

        screen.getByRole('heading', { level: 1, name: /Arby's/i });

        screen.getByRole('heading', { level: 2, name: /What Fish Sandwiches Should Be/i });

        screen.getByRole('heading', { level: 2, name: /Top picks/i });

        const menuCategoryContainer = getMenuCategoryContainer();
        screen.getByRole('heading', { level: 2, name: /Our Menu/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Meals/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Limited Time/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Signature/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Market Fresh/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Chicken/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Roast Beef/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Sliders/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Sides/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Desserts/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Drinks/i });
        within(menuCategoryContainer).getByRole('heading', { level: 3, name: /^Kids Menu/i });

        const infoBlockContainer = getInfoBlockContainer();
        within(infoBlockContainer).getByRole('heading', { level: 2, name: /More to love/i });
        within(infoBlockContainer).getByRole('heading', { level: 3, name: /Exclusive Deals/i });
        within(infoBlockContainer).getByRole('heading', { level: 3, name: /Get It Delivered/i });
        within(infoBlockContainer).getByRole('heading', { level: 3, name: /Market Fresh/i });
    });

    test('home page should show only one loading spinner', async () => {
        // TODO add test for the full personalization functionality(dependencies initialization, rendering personalized content)

        (usePersonalization as jest.Mock).mockReturnValue({
            loading: true,
            actions: { initializePersonalizationDependency: jest.fn(), getPersonalizedSection: jest.fn() },
        });

        await renderPage();

        screen.getByRole('progressbar');
    });
});
