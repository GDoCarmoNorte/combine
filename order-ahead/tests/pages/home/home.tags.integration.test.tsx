import { waitFor } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';
import ReactDOMServer from 'react-dom/server';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import { useGlobalProps } from '../../../redux/hooks';

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../redux/localStorage', () => {
    const mockStoreValues = require('../../mock-data/store/homePageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue(mockStoreValues),
        saveState: jest.fn(),
    };
});

jest.mock('next/head', () => {
    return {
        __esModule: true,
        default: ({ children }: { children: React.ReactElement | null }) => {
            if (children && global.document) {
                global.document.head.insertAdjacentHTML('afterbegin', ReactDOMServer.renderToString(children) || '');
            }
            return null;
        },
    };
});

describe('Home - tag integration', () => {
    beforeAll(async () => {
        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    beforeEach(async () => {
        const { render } = await getPage({
            route: '/',
        });

        await render();
    });

    afterAll(() => {
        server.close();
    });

    test('home page should have robots & viewport meta tags', async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="viewport"]')).toHaveAttribute(
                'content',
                'minimum-scale=1, initial-scale=1, width=device-width'
            );
        });
    });

    test("home page shouldn't have robots noindex meta tag", async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="robots"]')).not.toBeInTheDocument();
        });
    });

    test('home page should have correct manifest link', async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('link[rel="manifest"]')).toHaveAttribute(
                'href',
                '/brands/arbys/site.webmanifest'
            );
        });
    });

    test('home page should have correct set of brand-specific css imports', async () => {
        // eslint-disable-next-line testing-library/no-node-access
        const stylesheets = document.querySelectorAll('link[rel="stylesheet"]');

        const [
            legacyIconsStylesheet,
            themeStylesheet,
            brandIconsStylesheet,
            fontStylesheet,
            typographyStylesheet,
            listsStylesheet,
            moleculesStylesheet,
            globalStylesheet,
        ] = stylesheets;

        expect(legacyIconsStylesheet).toHaveAttribute('type', 'text/css');
        expect(legacyIconsStylesheet).toHaveAttribute('href', '/legacy/inspire-icons.css');

        expect(themeStylesheet).toHaveAttribute('type', 'text/css');
        expect(themeStylesheet).toHaveAttribute('href', '/brands/arbys/theme.css');

        expect(brandIconsStylesheet).toHaveAttribute('type', 'text/css');
        expect(brandIconsStylesheet).toHaveAttribute('href', '/brands/arbys/brand-icons.css');

        expect(fontStylesheet).toHaveAttribute('type', 'text/css');
        expect(fontStylesheet).toHaveAttribute('href', '/brands/arbys/fonts.css');

        expect(typographyStylesheet).toHaveAttribute('type', 'text/css');
        expect(typographyStylesheet).toHaveAttribute('href', '/brands/arbys/typography.css');

        expect(listsStylesheet).toHaveAttribute('type', 'text/css');
        expect(listsStylesheet).toHaveAttribute('href', '/brands/arbys/lists.css');

        expect(moleculesStylesheet).toHaveAttribute('type', 'text/css');
        expect(moleculesStylesheet).toHaveAttribute('href', '/brands/arbys/molecules.css');

        expect(globalStylesheet).toHaveAttribute('type', 'text/css');
        expect(globalStylesheet).toHaveAttribute('href', '/brands/arbys/global.css');
    });

    test('home page should have correct icon links', async () => {
        // eslint-disable-next-line testing-library/no-node-access
        const links = document.querySelectorAll(
            'link[rel="icon"], link[rel="apple-touch-icon"], link[rel="mask-icon"]'
        );
        const linksArr = Array.from(links);

        expect(linksArr).toMatchInlineSnapshot(`
                Array [
                  <link
                    data-reactroot=""
                    href="/brands/arbys/favicon-16x16.png"
                    rel="icon"
                    sizes="16x16"
                    type="image/png"
                  />,
                  <link
                    data-reactroot=""
                    href="/brands/arbys/favicon-32x32.png"
                    rel="icon"
                    sizes="32x32"
                    type="image/png"
                  />,
                  <link
                    data-reactroot=""
                    href="/brands/arbys/favicon-48x48.png"
                    rel="icon"
                    sizes="48x48"
                    type="image/png"
                  />,
                  <link
                    data-reactroot=""
                    href="/brands/arbys/apple-touch-icon.png"
                    rel="apple-touch-icon"
                    sizes="180x180"
                  />,
                  <link
                    data-reactroot=""
                    href="/brands/arbys/logo.svg"
                    rel="mask-icon"
                  />,
                ]
            `);
    });
});
