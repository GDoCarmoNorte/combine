import { act, screen, waitFor, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';

import fetchMock from 'jest-fetch-mock';
import { advanceTo, clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';

import { PAGE_RENDER_TIMEOUT } from '../pageUtils';

import mockTallyResponse from '../../mock-data/store/checkoutPageTally.mock.json';
import useAccount from '../../../redux/hooks/useAccount';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { injectScript } from '../../../lib/injectScriptOnce';
import { isFraudCheckEnabled, isMarketingPreferencesSignupEnabled } from '../../../lib/getFeatureFlags';
import { TErrorTypeEXTERNALModel, TTallyErrorCodeModel } from '../../../@generated/webExpApi/models';
import { useGlobalProps, useSubmitOrder } from '../../../redux/hooks';

import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';

jest.mock('../../../redux/hooks/useSubmitOrder');
jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isAccountOn: jest.fn().mockReturnValue(false),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(false),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPlaceholderPaymentOptionEnabled: jest.fn().mockReturnValue(true),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isFraudCheckEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isGiftCardPayEnabled: jest.fn().mockReturnValue(true),
        isCreditOrDebitPayEnabled: jest.fn().mockReturnValue(true),
        isCardOnFilePayEnabled: jest.fn().mockReturnValue(true),
        isPayAtStoredEnabled: jest.fn().mockReturnValue(true),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
        isMarketingPreferencesSignupEnabled: jest.fn().mockReturnValue(true),
    };
});

jest.spyOn(global.console, 'error').mockImplementation(() => null);

jest.mock('../../../redux/hooks/useAccount', () => ({
    __esModule: true,
    default: jest.fn().mockReturnValue({
        account: {},
        accountInfo: {},
        actions: {
            setAccount: jest.fn(),
        },
    }),
}));

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/checkoutPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/checkoutPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/checkoutPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/hooks/useCustomerPaymentMethod', () => {
    return {
        __esModule: true,
        useCustomerPaymentMethod: jest.fn().mockResolvedValue({
            loading: false,
            paymentMethods: null,
        }),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const injectScriptMock = jest.fn();

const mockInitPaymentSuccessResponse = {
    sessionKey:
        'eyJhbGciOiJBMjU2R0NNS1ciLCJlbmMiOiJBMjU2Q0JDLUhTNTEyIiwiaXYiOiJuVTZHMXRnUWp2RWtldEc1IiwidGFnIjoic3lWcEpXZHJQbWJFSWlmX3RTQlV4USIsInppcCI6IkRFRiJ9.ZJUkceAzaJS5v0tXwTiOoZwaq_raxzJ3rS_I-M0y1IOpEYyowUsrZqGCtaEIHNKRKUWmUUXG9UVeQTABt9vWYA.iBQig7ZJeKsq910imqVcoA.cJnmwJkk0Cy-IC8QD2TdLm4A4ybsrVJGVb0MUK0qtkkOeNuExuR7W5RQQfh2yHtdN4jJlNU0QKXUZUdgvVE8ZIH6Bo-Lk-wwUBQjIu08n8edvshUA0hVfU3ujbFB2ibE3wbg9h1myprVJ6OAki6K0laCdHiNPZMRtzKh0kAvSgF8iz3-wkSUAcAJisKdbg1rwT35kf6_WA_sFbbroUatdYHJOlt51JLQtlWDCcFaMKk3Q8o5VidpWwpL8cQr5hfskk0GVVwqAGRmPsZoPfBv3FaBb3Xts4QXUQIYO-ZQJ34twCJgfmeK3ZqJHKwjwInhpmZmXKNENwFtqvsRYD1HS5DkOsYYDrSlKqMsv9jEDx6EdKqoRw1NPGVYGIJHMVfPec1OJ1VHtUPHQijHRC8JCyVbr3Anc4m3GB6HGDYcHb9AqRiWGvGOqR10V9Y7IIyvpefaBZRq5nsMCB8zz6qdUgiA-YxNEp04PDJqSsGnLb0pQTM_NXK8hUmze-M18UFR.OkcQ4tkjWL-AlLmvOXescP8csd1keg_ptOmT58ebdOE',
    iframePayload: '<iframe frameborder="0" scrolling="no"></iframe>',
};

const mockInitPaymentErrorResponse = {
    textError: 'Connection error',
    header: 'Temporarily unavailable',
    description: 'The operation could not be completed. Please refresh and try again.',
};

describe('Checkout Page - Integration', () => {
    let renderResult;

    const renderPage = async () =>
        await act(async () => {
            const { render } = await getPage({
                route: '/checkout',
            });

            renderResult = render().container;
        });

    beforeAll(async () => {
        (useSubmitOrder as jest.Mock).mockReturnValue({
            error: null,
            isLoading: false,
            lastOrder: null,
            submitOrder: jest.fn(),
            resetSubmitOrder: jest.fn(),
            isShowAlertModal: false,
            hideAlertModal: jest.fn(),
            setUnavailableItems: jest.fn(),
        });
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (injectScript as jest.Mock) = injectScriptMock;
        advanceTo(new Date('2020-12-01T10:20:30Z'));

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
        (isMarketingPreferencesSignupEnabled as jest.Mock).mockReturnValueOnce(true);
    });

    afterAll(() => {
        clear();

        server.close();
    });

    test('a11y', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );

        await renderPage();
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );

        await renderPage();

        expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
    });

    test('checkout info: display', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );

        await renderPage();

        const getOrderItem = () => screen.getByLabelText(/Order Item/i);
        const getOrderTotalSection = () => screen.getByLabelText(/Order Total Section/i);
        const getPickupInfoSection = () => screen.getByLabelText(/Pickup Info Section/i);
        const getSelectLocationSection = () => screen.getByLabelText(/Select Location Section/i);
        const getOrderDetailsItems = () => screen.getAllByLabelText(/Order Details Item/i);

        // Check location section
        expect(within(getSelectLocationSection()).getByText(/^Pickup Location/i)).toBeInTheDocument();
        expect(
            within(getSelectLocationSection()).getByRole('heading', { name: /Dunwoody - Roswell Rd/i })
        ).toBeInTheDocument();
        expect(
            within(getSelectLocationSection()).getByText(/^100 E STATE HWY 66, TELL CITY, IN 47586/i)
        ).toBeInTheDocument();

        // Check pickup info section
        expect(within(getPickupInfoSection()).getByRole('heading', { name: /Pickup Info/i })).toBeInTheDocument();
        expect(within(getPickupInfoSection()).getByText(/^Today/i)).toBeInTheDocument();
        expect(within(getPickupInfoSection()).getByText(/^ASAP/i)).toBeInTheDocument();

        // Check customer info section
        expect(screen.getByPlaceholderText(/^First Name/i)).toHaveValue('');
        expect(screen.getByPlaceholderText(/^Last Name/i)).toHaveValue('');
        expect(screen.getByPlaceholderText(/^Phone Number/i)).toHaveValue('');
        expect(screen.getByPlaceholderText(/^Email/i)).toHaveValue('');

        // Check order section
        expect(screen.getByLabelText(/^Review Order Section/i)).toBeInTheDocument();
        expect(screen.getByText(/^Review Order/i)).toBeInTheDocument();
        expect(within(getOrderItem()).getByText(/Roast Beef Gyro/i)).toBeInTheDocument();
        expect(within(getOrderItem()).getByText(/\$6.79/i)).toBeInTheDocument();

        // Check order items
        expect(within(getOrderDetailsItems()[0]).getByText(/Roast Beef Gyro/i)).toBeInTheDocument();
        expect(within(getOrderDetailsItems()[0]).getByText(/\$3.01/i)).toBeInTheDocument();
        expect(within(getOrderDetailsItems()[1]).getByText(/Curly Fries \(S\)/i)).toBeInTheDocument();
        expect(within(getOrderDetailsItems()[1]).getByText(/\$1.99/i)).toBeInTheDocument();
        expect(within(getOrderDetailsItems()[2]).getByText(/Diet Coke/i)).toBeInTheDocument();
        expect(within(getOrderDetailsItems()[2]).getByText(/\$1.79/i)).toBeInTheDocument();

        // Check order total
        expect(within(getOrderTotalSection()).getByText(/Subtotal/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/\$6.79/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/Tax/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/\$0.48/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/^Tip/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/\$2/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/^Total/i)).toBeInTheDocument();
        expect(within(getOrderTotalSection()).getByText(/\$9.27/i)).toBeInTheDocument();

        // Check payment section
        expect(screen.getByLabelText(/^Payment Info/i)).toBeInTheDocument();
        expect(screen.getByPlaceholderText(/^Enter Cardholder First Name/i)).toHaveValue('');
        expect(screen.getByPlaceholderText(/^Enter Cardholder Last Name/i)).toHaveValue('');

        expect(screen.getByLabelText(/^Pay Button Overlay/i)).toHaveAttribute('aria-disabled', 'true');
        expect(screen.getByLabelText(/^By clicking "PAY"/i)).toBeInTheDocument();
    });

    test('customer info section: check validation', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );

        await renderPage();

        const getFirstNameInput = () => screen.queryByPlaceholderText(/^First Name/i);
        const getLastNameInput = () => screen.queryByPlaceholderText(/^Last Name/i);
        const getPhoneNumberInput = () => screen.queryByPlaceholderText(/^Phone Number/i);
        const getEmailInput = () => screen.queryByPlaceholderText(/^Email/i);

        userEvent.type(getFirstNameInput(), '111');
        userEvent.type(getLastNameInput(), '   ');
        userEvent.type(getPhoneNumberInput(), '111');
        userEvent.type(getEmailInput(), '111');

        userEvent.click(screen.getByLabelText(/^Review Order Section/i));

        await waitFor(() => {
            expect(screen.getByText(/^Alpha characters only/i)).toBeInTheDocument();
        });
        expect(screen.getByText(/^Last Name is incomplete/i)).toBeInTheDocument();
        expect(screen.getByText(/^Incorrect phone/i)).toBeInTheDocument();
        expect(screen.getByText(/^Incorrect email/i)).toBeInTheDocument();

        userEvent.clear(getFirstNameInput());
        userEvent.clear(getLastNameInput());
        userEvent.clear(getPhoneNumberInput());
        userEvent.clear(getEmailInput());

        userEvent.type(getFirstNameInput(), 'test');
        userEvent.type(getLastNameInput(), 'test');
        userEvent.type(getPhoneNumberInput(), '1111111111');
        userEvent.type(getEmailInput(), 'test@test.com');

        await waitFor(() => {
            expect(screen.queryByText(/^Alpha characters only/i)).not.toBeInTheDocument();
        });
        expect(screen.queryByText(/^Last Name is incomplete/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/^Incorrect phone/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/^Incorrect email/i)).not.toBeInTheDocument();
    });

    test('should display account info in customer info section if user signed in', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );

        (useAccount as jest.Mock).mockReturnValue({
            account: {},
            accountInfo: {
                firstName: 'John',
                lastName: 'Doe',
                email: 'john.doe@gmail34.com',
                references: [],
                phone: '1231231234',
                preferences: {},
            },
            actions: {
                setAccount: jest.fn(),
            },
        });

        await renderPage();

        const getFirstNameInput = () => screen.queryByPlaceholderText(/^First Name/i);
        const getLastNameInput = () => screen.queryByPlaceholderText(/^Last Name/i);
        const getPhoneNumberInput = () => screen.queryByPlaceholderText(/^Phone Number/i);
        const getEmailInput = () => screen.queryByPlaceholderText(/^Email/i);

        await waitFor(() => {
            expect(getFirstNameInput()).toHaveValue('John');
        });
        expect(getLastNameInput()).toHaveValue('Doe');
        expect(getEmailInput()).toHaveValue('john.doe@gmail34.com');
        expect(getPhoneNumberInput()).toHaveValue('1231231234');

        expect(screen.queryByText(/^Alpha characters only/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/^Last Name is incomplete/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/^Incorrect phone/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/^Incorrect email/i)).not.toBeInTheDocument();
    });

    test('pickup info section: select order day and time', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );

        await renderPage();

        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);
        const getMessageLabel = () => screen.queryByText(/^Select time/i);
        const getLoadingLabel = () => screen.queryByText(/^Refreshing order data/i);

        expect(getDayButton()).toBeInTheDocument();
        userEvent.click(getDayButton());

        await waitFor(() => {
            expect(getDayListItem()).toBeInTheDocument();
        });

        userEvent.click(getDayListItem());

        await waitFor(() => {
            expect(getMessageLabel()).toBeInTheDocument();
        });

        userEvent.click(getTimeButton());

        await waitFor(() => {
            expect(getTimeListItem()).toBeInTheDocument();
        });

        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getLoadingLabel()).toBeInTheDocument();
        });

        await waitFor(() => {
            expect(getDayButton()).toBeEnabled();
        });
        expect(getTimeButton()).toBeEnabled();
    });

    test('should display an error when there is a service error', async () => {
        fetchMock.resetMocks();

        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentErrorResponse), { status: 500 }]
        );

        await renderPage();

        const getDayButton = () => screen.queryByText(/^Pickup Date/i);
        const getTypeError = () => screen.getByText(/Connection error/i);
        const getHeaderText = () => screen.getByText(/Temporarily unavailable/i);
        const getButtonRefresh = () => screen.getByRole('button', { name: /refresh/i });

        expect(getDayButton()).toBeInTheDocument();
        expect(getTypeError()).toBeInTheDocument();
        expect(getHeaderText()).toBeInTheDocument();
        expect(getButtonRefresh()).toBeInTheDocument();
    });

    test('should refresh when there is an error displayed and refresh button is clicked', async () => {
        fetchMock.resetMocks();

        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentErrorResponse), { status: 500 }]
        );

        await renderPage();

        const getButtonRefresh = () => screen.queryByRole('button', { name: /refresh/i });
        const getPaymentForm = () => screen.queryByLabelText('Payment Info');

        expect(getButtonRefresh()).toBeInTheDocument();
        expect(getPaymentForm()).not.toBeInTheDocument();

        fetchMock.mockResponse(JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 });

        userEvent.click(getButtonRefresh());

        await waitFor(() => {
            expect(getButtonRefresh()).not.toBeInTheDocument();
        });
        expect(getPaymentForm()).toBeInTheDocument();
    });

    test('should not try to embed Kount script if Fraud check feature disabled', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentErrorResponse), { status: 500 }]
        );

        await renderPage();

        await waitFor(() => {
            expect(injectScriptMock).toHaveBeenCalledTimes(0);
        });
    });
    test('should embed Kount script if Fraud check feature enabled', async () => {
        (isFraudCheckEnabled as jest.Mock).mockReturnValueOnce(true);
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentErrorResponse), { status: 500 }]
        );

        await renderPage();

        await waitFor(() => {
            expect(injectScriptMock).toHaveBeenCalledTimes(1);
        });
    });

    test('error notification: should show a DeliveryProviderDeliveryTimeNotAvailable error message', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [
                JSON.stringify({
                    code: TTallyErrorCodeModel.DeliveryProviderDeliveryTimeNotAvailable,
                    message:
                        'The earliest delivery time is 04:30 AM on the selected date. Please select a new date/time and try again.',
                    type: TErrorTypeEXTERNALModel,
                    data: {
                        earliestTime: '04:30 AM',
                    },
                }),
                { status: 500 },
            ]
        );

        await renderPage();
        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);
        const getErrorNotification = () =>
            within(screen.getByRole('alert')).getByText(
                /The earliest delivery time is 04:30 AM on the selected date. Please select a new date\/time and try again./i
            );

        userEvent.click(getDayButton());
        userEvent.click(getDayListItem());
        userEvent.click(getTimeButton());
        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getErrorNotification()).toBeInTheDocument();
        });
    });

    test('error notification: should show a OrderTimeNotValid error message', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [
                JSON.stringify({
                    code: TTallyErrorCodeModel.OrderTimeNotValid,
                    message:
                        'We apologize. We do not have any available timeslots for the time you chose. Please switch your order to pickup or select a new delivery time to proceed with your order.',
                    type: TErrorTypeEXTERNALModel,
                    data: {},
                }),
                { status: 500 },
            ]
        );

        await renderPage();
        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);
        const getErrorNotification = () =>
            within(screen.getByRole('alert')).getByText(
                /We apologize. We do not have any available timeslots for the time you chose. Please switch your order to pickup or select a new delivery time to proceed with your order./i
            );

        userEvent.click(getDayButton());
        userEvent.click(getDayListItem());
        userEvent.click(getTimeButton());
        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getErrorNotification()).toBeInTheDocument();
        });
    });

    test('error notification: should show a LocationNotAvailable error message', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [
                JSON.stringify({
                    code: TTallyErrorCodeModel.LocationNotAvailable,
                    message: 'The requested location is not available. Please select another location and try again.',
                    type: TErrorTypeEXTERNALModel,
                    data: {},
                }),
                { status: 500 },
            ]
        );

        await renderPage();
        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);
        const getErrorNotification = () =>
            within(screen.getByRole('alert')).getByText(
                /The requested location is not available. Please select another location and try again./i
            );

        userEvent.click(getDayButton());
        userEvent.click(getDayListItem());
        userEvent.click(getTimeButton());
        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getErrorNotification()).toBeInTheDocument();
        });
    });

    test('error notification: should show a QuantitiesModifierGroupNotValid error message', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [
                JSON.stringify({
                    code: TTallyErrorCodeModel.QuantitiesModifierGroupNotValid,
                    message: 'Some products contain invalid quantities. Please modify the quantities and try again.',
                    type: TErrorTypeEXTERNALModel,
                    data: {},
                }),
                { status: 500 },
            ]
        );

        await renderPage();
        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);

        const getErrorNotification = () =>
            within(screen.getByRole('alert')).getByText(
                /Some products contain invalid quantities. Please modify the quantities and try again./i
            );

        userEvent.click(getDayButton());
        userEvent.click(getDayListItem());
        userEvent.click(getTimeButton());
        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getErrorNotification()).toBeInTheDocument();
        });
    });

    test('error notification: should show a QuantitiesModifiersNotValid error message', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [
                JSON.stringify({
                    code: TTallyErrorCodeModel.QuantitiesModifiersNotValid,
                    message: 'Some products contain invalid quantities. Please modify the quantities and try again.',
                    type: TErrorTypeEXTERNALModel,
                    data: {},
                }),
                { status: 500 },
            ]
        );

        await renderPage();
        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);

        const getErrorNotification = () =>
            within(screen.getByRole('alert')).getByText(
                /Some products contain invalid quantities. Please modify the quantities and try again./i
            );

        userEvent.click(getDayButton());
        userEvent.click(getDayListItem());
        userEvent.click(getTimeButton());
        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getErrorNotification()).toBeInTheDocument();
        });
    });

    test('error notification: should show a ExceededMaxOrderAmount error message', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [
                JSON.stringify({
                    code: TTallyErrorCodeModel.ExceededMaxOrderAmount,
                    message:
                        'We apologize. Your order exceeds ${orderMaxValue}. If you still want to place your order, call us at {phoneNumber}, or remove items from your bag.',
                    type: TErrorTypeEXTERNALModel,
                    data: {
                        max: '100',
                        requested: '120',
                    },
                }),
                { status: 500 },
            ]
        );

        await renderPage();
        const getDayButton = () => within(screen.queryByTestId(/^Pickup Date/i)).getByRole('button');
        const getTimeButton = () => within(screen.queryByTestId(/^Pickup Time/i)).getByRole('button');
        const getDayListItem = () => screen.queryByText(/^12\/02\/2020/i);
        const getTimeListItem = () => screen.queryByText(/^02:30 AM/i);

        const getErrorNotification = () =>
            within(screen.getByRole('alert')).getByText(
                /We apologize. Your order exceeds \$100. If you still want to place your order, call us at 812-547-4411, or remove items from your bag./i
            );

        userEvent.click(getDayButton());
        userEvent.click(getDayListItem());
        userEvent.click(getTimeButton());
        userEvent.click(getTimeListItem());

        await waitFor(() => {
            expect(getErrorNotification()).toBeInTheDocument();
        });
    });

    test('Should show payment processing loader on order submit', async () => {
        fetchMock.mockResponses(
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockTallyResponse), { status: 200 }],
            [JSON.stringify(mockInitPaymentSuccessResponse), { status: 200 }]
        );
        (useSubmitOrder as jest.Mock).mockReturnValue({
            error: null,
            isLoading: true,
        });

        await renderPage();

        expect(screen.getByText(/payment processing.../i)).toBeInTheDocument();
        expect(screen.getByText(/Please do not close this tab or refresh your browser./i)).toBeInTheDocument();
    });
});
