import { screen, act, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { getPage } from '../../lib/integration/getPage';
import { clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { allLocationsMock, emptyAllLocationsMock } from '../../mocks/expLocations.mock';
import mockAllLocationPage from '../../mock-data/contentful/contentful-getEntry-page-allLocations.json';
import useLocationList from '../../../common/hooks/useLocationList';
import { isLocationPagesOn } from '../../../lib/getFeatureFlags';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';

jest.unmock('../../../redux/hooks/useConfiguration');
const routerReplaceMock = jest.fn();
jest.mock('next/router', () => ({
    //@ts-ignore
    ...jest.requireActual('next/router'),
    default: {
        //@ts-ignore
        ...jest.requireActual('next/router').default,
        replace: routerReplaceMock,
    },
    useRouter: () => ({
        push: jest.fn(),
        replace: routerReplaceMock,
        route: '/',
        pathname: '',
        query: '',
        asPath: '',
    }),
}));
jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.location),
    };
});

jest.mock('../../../common/hooks/useLocationList');

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const getLocationsContainer = () => screen.getByText(`${allLocationsMock.totalCount} locations`);

describe('AllLocations Page - Integration', () => {
    let renderResult, clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();
        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
        jest.clearAllMocks();
    });

    describe('when no location results', () => {
        beforeEach(async () => {
            await act(async () => {
                const { render } = await getPage({
                    route: '/locations/all',
                });

                renderResult = render().container;
            });
        });
        test('show corresponding message', async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(emptyAllLocationsMock)),
            });
            const noLocationFoundBlockFields = mockAllLocationPage.includes.Entry[1].fields;
            expect(screen.getByText(noLocationFoundBlockFields.header)).toBeInTheDocument();
            expect(screen.getByText(noLocationFoundBlockFields.body)).toBeInTheDocument();
        });
        test('show corresponding message if error during locations list retrieving', async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.reject('something went wrong')),
            });
            const noLocationFoundBlockFields = mockAllLocationPage.includes.Entry[1].fields;
            expect(screen.getByText(noLocationFoundBlockFields.header)).toBeInTheDocument();
            expect(screen.getByText(noLocationFoundBlockFields.body)).toBeInTheDocument();
        });
    });
    describe('when locations list is returned', () => {
        beforeEach(async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(allLocationsMock)),
            });
            await act(async () => {
                const { render } = await getPage({
                    route: '/locations/all',
                });

                renderResult = render().container;
            });
        });

        test('a11y', async () => {
            const results = await axe(renderResult);
            expect(results).toHaveNoViolations();
        });

        test('show total amount of locations', async () => {
            const locationsContainer = getLocationsContainer();
            expect(locationsContainer).toBeInTheDocument();
        });
        test('show all countries', async () => {
            allLocationsMock.locationsByCountry.forEach((c) => {
                expect(screen.getByText(c.countryName)).toBeInTheDocument();
            });
        });
        test('show all states accordions collapsed by default', async () => {
            allLocationsMock.locationsByCountry.forEach((c) => {
                c.statesOrProvinces.forEach(() => {
                    const countryContainer = screen.getByText(c.countryName);
                    const expandedStateAccordionIcons = within(countryContainer).queryAllByLabelText(/Collapse/i);
                    expect(expandedStateAccordionIcons.length).toBe(0);
                });
            });
        });
        test('expand state accordion when clicked', async () => {
            const accordionIconComponent = screen.getAllByLabelText(/Expand/i)[0];
            userEvent.click(accordionIconComponent);
            expect(accordionIconComponent).toHaveClass('direction-up');
            userEvent.click(accordionIconComponent);
            expect(accordionIconComponent).toHaveClass('direction-down');
        });
        test('show all cities for expanded state accordion', async () => {
            const stateMocked = allLocationsMock.locationsByCountry[0].statesOrProvinces[0];
            const accordionIconComponent = screen.getAllByLabelText(/Expand/i)[0];
            userEvent.click(accordionIconComponent);
            stateMocked.cities.forEach((c) => {
                expect(screen.getByText(c.name)).toBeInTheDocument();
            });
        });
    });
    describe('when location page feature is turned off', () => {
        const getLocationListMock = jest.fn().mockImplementation(() => Promise.resolve(allLocationsMock));

        beforeEach(async () => {
            (isLocationPagesOn as jest.Mock).mockReturnValue(false);
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: getLocationListMock,
            });

            await act(async () => {
                const { render } = await getPage({
                    route: '/locations/all',
                });

                renderResult = render().container;
            });
        });
        test('should not call services', async () => {
            expect(routerReplaceMock).toHaveBeenCalledTimes(1);
            expect(routerReplaceMock).toHaveBeenCalledWith('/404');
            expect(getLocationListMock).toHaveBeenCalledTimes(0);
        });
    });
});
