import { screen, waitFor, act } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { locationUrlsMock, allLocationsMock, emptyAllLocationsMock } from '../../mocks/expLocations.mock';
import mockStateLocationsPage from '../../mock-data/contentful/contentful-getEntry-page-stateLocations.json';
import useLocationList from '../../../common/hooks/useLocationList';
import { isLocationPagesOn } from '../../../lib/getFeatureFlags';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';

jest.unmock('../../../redux/hooks/useConfiguration');
const routerReplaceMock = jest.fn();
jest.mock('next/router', () => ({
    //@ts-ignore
    ...jest.requireActual('next/router'),
    default: {
        //@ts-ignore
        ...jest.requireActual('next/router').default,
        replace: routerReplaceMock,
    },
    useRouter: () => ({
        push: jest.fn(),
        replace: routerReplaceMock,
        route: '/',
        pathname: '',
        query: '',
        asPath: '',
    }),
}));

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.location),
    };
});

jest.mock('../../../common/hooks/useLocationList');

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const routeMock = `/locations/us/mn`;
const getHeader = () => screen.getByText(/4 Minnesota locations/i);
const geCityName = () => screen.getByText(/Minneapolis/i);

describe('State Locations Page - Integration', () => {
    let renderResult, clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();
        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
        jest.clearAllMocks();
    });

    describe('when no location results', () => {
        beforeEach(async () => {
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });

        test('show corresponding message', async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(emptyAllLocationsMock)),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            const noLocationFoundBlockFields = mockStateLocationsPage.includes.Entry[1].fields;
            expect(screen.getByText(noLocationFoundBlockFields.header)).toBeInTheDocument();
            expect(screen.getByText(noLocationFoundBlockFields.body)).toBeInTheDocument();
        });

        test('show corresponding message if error during locations list retrieving', async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.reject('something went wrong')),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            const noLocationFoundBlockFields = mockStateLocationsPage.includes.Entry[1].fields;
            expect(screen.getByText(noLocationFoundBlockFields.header)).toBeInTheDocument();
            expect(screen.getByText(noLocationFoundBlockFields.body)).toBeInTheDocument();
        });
    });

    describe('when locations list is returned', () => {
        beforeEach(async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(allLocationsMock)),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });

        test('a11y', async () => {
            const results = await axe(renderResult);
            expect(results).toHaveNoViolations();
        });

        test('should have correct meta description', async () => {
            await waitFor(() => {
                // eslint-disable-next-line testing-library/no-node-access
                expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                    'content',
                    'State Locations page description from CMS'
                );
            });
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('title')).toHaveTextContent("Arby's State Locations");

            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('link[rel="canonical"]')).toHaveAttribute(
                'href',
                `${process.env.NEXT_PUBLIC_APP_URL}/locations/us/mn/`
            );
        });

        test('should render data as expected', async () => {
            const ciryyName = geCityName();
            expect(ciryyName).toBeInTheDocument();

            const header = getHeader();
            expect(header).toBeInTheDocument();
        });

        test('should display all cities', async () => {
            allLocationsMock.locationsByCountry[0].statesOrProvinces[0].cities.forEach((c) => {
                expect(screen.getByText(c.name)).toBeInTheDocument();
            });
        });
    });

    describe('when location page feature is turned off', () => {
        const getLocationListMock = jest.fn().mockImplementation(() => Promise.resolve(allLocationsMock));
        const getLocationUrlsListMock = jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock));

        beforeEach(async () => {
            (isLocationPagesOn as jest.Mock).mockReturnValue(false);
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: getLocationListMock,
                getLocationUrlsList: getLocationUrlsListMock,
            });
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });
        test('should not call services', async () => {
            expect(routerReplaceMock).toHaveBeenCalledTimes(1);
            expect(routerReplaceMock).toHaveBeenCalledWith('/404');
            expect(getLocationListMock).toHaveBeenCalledTimes(0);
            expect(getLocationUrlsListMock).toHaveBeenCalledTimes(0);
        });
    });
});
