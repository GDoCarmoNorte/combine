import { screen, waitFor, act, fireEvent, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as reactRedux from 'react-redux';
import { getPage } from '../../lib/integration/getPage';
import { advanceTo, clear } from 'jest-date-mock';
import fetchMock from 'jest-fetch-mock';
import { axe } from '../../../jest/axe-helper';
import GoogleMapReact from 'google-map-react';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { locationsPage1, locationsPage2 } from '../../mocks/expLocations.mock';

import getBrandInfo from '../../../lib/brandInfo';
import {
    GTM_MAP_DOUBLE_CLICK,
    GTM_MAP_DRAG,
    GTM_MAP_DRAG_END,
    GTM_MAP_DRAG_START,
    GTM_MAP_LIST_LOCATION_CLICK,
    GTM_MAP_LOCATION_CLICK,
    GTM_SEARCH_NEW_LOCATION,
} from '../../../common/services/gtmService/constants';

import { getPageTitle } from '../../../common/helpers/getPageTitle';
import mockLocationPage from '../../mock-data/contentful/contentful-getEntry-page-locations.json';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';

const locationsPages = [locationsPage1, locationsPage2];

const dispatchMock = jest.fn();

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        isAccountOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isLocationsShowAsOrderedList: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.doMock('react-redux', () => ({
    ...reactRedux,
    useDispatch: jest.fn().mockReturnValue(dispatchMock),
}));

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('google-map-react', () => {
    let _onClickCallback;
    let _onDragCallback;
    let _onDragEndCallback;
    let _onFirstLocationSelectCallback;
    let _onLastLocationSelectCallback;

    return {
        __esModule: true,
        default: ({ children, onClick, onDrag, onDragEnd }) => {
            _onClickCallback = onClick;
            _onDragCallback = onDrag;
            _onDragEndCallback = onDragEnd;
            _onFirstLocationSelectCallback = children[0]?.props.onLocationSelect;
            _onLastLocationSelectCallback = children[children.length - 1]?.props.onLocationSelect;

            return null;
        },
        get onClickCallback() {
            return _onClickCallback;
        },
        get onDragCallback() {
            return _onDragCallback;
        },
        get onDragEndCallback() {
            return _onDragEndCallback;
        },
        get onFirstLocationSelectCallback() {
            return _onFirstLocationSelectCallback;
        },
        get onLastLocationSelectCallback() {
            return _onLastLocationSelectCallback;
        },
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const mapMock = require('google-map-react') as jest.MockedClass<
    typeof GoogleMapReact & {
        onClickCallback: () => void;
        onDragCallback: () => void;
        onDragEndCallback: () => void;
        onFirstLocationSelectCallback: () => void;
        onLastLocationSelectCallback: () => void;
    }
>;

describe('Locations Page: Pickup and delivery flow - Integration', () => {
    let renderResult;
    let clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        advanceTo(new Date('2020-12-01T10:20:30Z'));

        fetchMock.mockResponse((req: Request) => {
            if (req.url === 'https://maps.googleapis.com/maps/api/geocode/json?address=47586&key=mock-google-key') {
                return Promise.resolve(JSON.stringify({ results: [{ geometry: { location: { lat: 10, lng: 20 } } }] }));
            }

            if (req.url.match(/\/webExpApiPath\/v1\/location\/*/)) {
                const url = new URL(req.url, 'https://localhost');
                const page = Number(url.searchParams.get('page'));

                return Promise.resolve(JSON.stringify(locationsPages[page]));
            }

            return Promise.reject(new Error(`URL not mocked! - ${req.url}`));
        });

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    beforeEach(async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/locations',
            });

            renderResult = render().container;
        });
    });

    afterEach(() => {
        dispatchMock.mockReset();
    });

    test('a11y', async () => {
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', () => {
        screen.getByText(/skip to main content/i);
    });

    test('locations page should have correct title', async () => {
        const brandInfo = getBrandInfo();

        expect(global.window).toBeDefined();
        expect(global.window.document).toBeDefined();

        expect(brandInfo).toBeDefined();
        expect(brandInfo.brandName).toBeDefined();

        await waitFor(() => {
            expect(global.window.document.title).toBe(
                getPageTitle(brandInfo.brandName, mockLocationPage.items[0].fields.metaTitle)
            );
        });
    });

    test('locations page should have correct meta description', async () => {
        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                'content',
                'Location Page description from CMS'
            );
        });

        // eslint-disable-next-line testing-library/no-node-access
        expect(document.querySelector('link[rel="canonical"]')).toHaveAttribute(
            'href',
            `${process.env.NEXT_PUBLIC_APP_URL}/locations`
        );
    });

    test('should show tabs and pickup tab opened by default', async () => {
        const tabs = screen.getByRole('tablist');
        within(tabs).getByRole('tab', { name: /Pickup/i });
        within(tabs).getByRole('tab', { name: /Delivery/i });

        const pickupTabPanel = screen.getByRole('tabpanel', { name: /Pickup/i, hidden: false });
        within(pickupTabPanel).getByRole('textbox', { name: /Search Location Input/i });
        within(pickupTabPanel).getByRole('button', { name: /Use my location/i });
        within(pickupTabPanel).getByRole('heading', { level: 2, name: /find a Location/i });
        within(pickupTabPanel).getByText(/Enter your location to find your local store./i);
        within(pickupTabPanel).getByRole('link', { name: /All Locations/i });
    });

    test('should search pickup locations and load more locations', async () => {
        const pickupTabPanel = screen.getByRole('tabpanel', { name: /Pickup/i, hidden: false });

        const searchInput = within(pickupTabPanel).getByRole('textbox', { name: /Search Location Input/i });

        userEvent.type(searchInput, '47586');

        userEvent.click(screen.getByRole('button', { name: /Search Location/i }));

        await waitFor(() => {
            expect(screen.getByText(/Chardon – Water St/i)).toBeInTheDocument();
        });

        userEvent.click(screen.getByRole('button', { name: /View more locations/i }));

        await waitFor(() => {
            within(pickupTabPanel).getByText(/Willowick – Vine St/i);
        });

        expect(screen.queryByRole('button', { name: /View more locations/i })).not.toBeInTheDocument();
    });

    test('locations page should fire GTM events', async () => {
        const pickupTabPanel = screen.getByRole('tabpanel', { name: /Pickup/i, hidden: false });

        const searchInput = within(pickupTabPanel).getByRole('textbox', { name: /Search Location Input/i });

        userEvent.type(searchInput, '47586');

        userEvent.click(screen.getByRole('button', { name: /Search Location/i }));

        await waitFor(() => {
            expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_SEARCH_NEW_LOCATION });
        });

        mapMock.onDragCallback();
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DRAG_START });
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DRAG });

        mapMock.onDragEndCallback();
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DRAG_END });

        act(() => {
            mapMock.onLastLocationSelectCallback();
        });
        expect(dispatchMock).toHaveBeenCalledWith(expect.objectContaining({ type: GTM_MAP_LOCATION_CLICK }));

        fireEvent.dblClick(screen.queryByLabelText('locationsMap'));
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DOUBLE_CLICK });

        userEvent.click(screen.queryByRole('button', { name: 'Select Mentor – Reynolds Rd location' }));
        expect(dispatchMock).toHaveBeenCalledWith(expect.objectContaining({ type: GTM_MAP_LIST_LOCATION_CLICK }));
    });
});
