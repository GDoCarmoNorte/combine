import { act, screen, waitFor } from '@testing-library/react';
import { getPage } from '../../lib/integration/getPage';
import { clear } from 'jest-date-mock';
import { axe } from '../../../jest/axe-helper';
import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';
import { contentfulHandlers, server } from '../../mock-data/server/mockServer';
import locationsPage1, {
    emptyLocationsByStateMock,
    locationDescriptionMapping,
    locationDetailsUrlMock,
    locationsByStateMock,
    locationUrlsMock,
    tapListResponseMock,
} from '../../mocks/expLocations.mock';
import mockLocationDetailsPage from '../../mock-data/contentful/contentful-getEntry-page-locationDetails.json';
import useLocationList from '../../../common/hooks/useLocationList';
import usePickupLocationsSearch from '../../../common/hooks/usePickupLocationsSearch';
import { LoadingStatusEnum } from '../../../common/types';
import { isLocationPagesOn } from '../../../lib/getFeatureFlags';
import { IServiceTypeModel, THourDayOfWeekModel } from '../../../@generated/webExpApi';
import { LocationServicesModel } from '../../../common/constants/locations';
import { getBeerMenuByLocationId } from '../../../common/services/localTapList';
import useLocalTapList from '../../../redux/hooks/useLocalTapList';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';
import { usePersonalization } from '../../../redux/hooks/usePersonalization';

jest.unmock('../../../redux/hooks/useConfiguration');
const routerReplaceMock = jest.fn();
jest.mock('next/router', () => ({
    //@ts-ignore
    ...jest.requireActual('next/router'),
    default: {
        //@ts-ignore
        ...jest.requireActual('next/router').default,
        replace: routerReplaceMock,
    },
    useRouter: () => ({
        push: jest.fn(),
        replace: routerReplaceMock,
        route: '/',
        pathname: '',
        query: '',
        asPath: '',
    }),
}));

jest.mock('../../../lib/getFeatureFlags', () => {
    return {
        __esModule: true,
        getFeatureFlags: jest.fn().mockReturnValue({}),
        default: jest.fn().mockReturnValue({}),
        isAccountDealsPageOn: jest.fn().mockReturnValue(false),
        locationSpecificMenuCategoriesEnabled: jest.fn().mockReturnValue(false),
        isLocationPagesOn: jest.fn().mockReturnValue(true),
        isRewardsOn: jest.fn().mockReturnValue(true),
        isLocalTapListOn: jest.fn().mockReturnValue(false),
        isShoppingBagLocationSelectComponentEnabled: jest.fn().mockReturnValue(false),
        locationTimeSlotsEnabled: jest.fn().mockReturnValue(false),
        isOpenTableEnabled: jest.fn().mockReturnValue(false),
        isDineTimeEnabled: jest.fn().mockReturnValue(false),
        isPersonalizationEnabled: jest.fn().mockReturnValue(false),
        isPlayExperienceEnabled: jest.fn().mockReturnValue(false),
        isOAEnabled: jest.fn().mockReturnValue(true),
        isTippingEnabled: jest.fn().mockReturnValue(true),
        isDeliveryEnabled: jest.fn().mockReturnValue(true),
        isApplePayEnabled: jest.fn().mockReturnValue(true),
        isGooglePayEnabled: jest.fn().mockReturnValue(true),
        isDeliveryFlowEnabled: jest.fn().mockReturnValue(true),
        isBreadcrumbSchemaOn: jest.fn().mockReturnValue(false),
        isSingUpBannerOnConfirmationEnabled: jest.fn().mockReturnValue(false),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/rewardsPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.location),
    };
});

jest.mock('../../../common/hooks/useLocationList');
jest.mock('../../../common/hooks/usePickupLocationsSearch');
jest.mock('../../../common/services/localTapList');
jest.mock('../../../redux/hooks/useLocalTapList');
jest.mock('../../../redux/hooks/useLdp', () => {
    return {
        __esModule: true,
        default: jest.fn(() => ({ actions: { putLocationDetails: jest.fn(), resetLocationDetails: jest.fn() } })),
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());
jest.mock('../../../redux/hooks/usePersonalization', () => ({ usePersonalization: jest.fn() }));

const searchLocationsByQueryMock = jest.fn();
const searchLocationsByCoordinates = jest.fn();
const resetSearchResultMock = jest.fn();
const fetchMoreLocationsMock = jest.fn();
const searchLocationsByUserGeolocationMock = jest.fn();

const nearbyLocationMock = locationsPage1.locations[0];
const routeMock = `/locations/${locationDetailsUrlMock}`;

const getSecondaryBannerContainer = () => screen.getByText(/Game-time energy. Lifetime experience./i);
const getNotificationBannerContainer = () =>
    screen.getByText(
        /Hours of operation may vary. Drive-Thru and Carry Out available in most locations. Please call your local restaurant to confirm./i
    );

describe('Location Details Page - Integration', () => {
    let renderResult, clearNextHeadMock;

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();
        jest.setTimeout(PAGE_RENDER_TIMEOUT);
        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    beforeEach(() => {
        (usePickupLocationsSearch as jest.Mock).mockReturnValue({
            searchLocationsByQuery: searchLocationsByQueryMock,
            searchLocationsByCoordinates: searchLocationsByCoordinates,
            searchLocationsByUserGeolocation: searchLocationsByUserGeolocationMock,
            fetchMoreLocations: fetchMoreLocationsMock,
            resetSearchResult: resetSearchResultMock,
            locationsSearchStatus: LoadingStatusEnum.Idle,
            locationsSearchResult: {
                locations: [nearbyLocationMock],
            },
            moreLocationsFetchStatus: LoadingStatusEnum.Idle,
            isAbleToFetchMoreLocations: false,
        });
        (getBeerMenuByLocationId as jest.Mock).mockResolvedValue(tapListResponseMock);
        (useLocalTapList as any).mockReturnValue({
            list: {},
            available: false,
            loading: false,
            error: false,
            actions: {
                getLocalTapList: jest.fn(),
                setLocalTapList: jest.fn(),
            },
        });
        (usePersonalization as jest.Mock).mockReturnValue({
            loading: false,
            actions: { initializePersonalizationDependency: jest.fn(), getPersonalizedSection: jest.fn() },
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('when no location results', () => {
        test('show corresponding message', async () => {
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });

            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(emptyLocationsByStateMock)),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            const noLocationFoundBlockFields = mockLocationDetailsPage.includes.Entry[1].fields;
            expect(screen.getByText(noLocationFoundBlockFields.header)).toBeInTheDocument();
            expect(screen.getByText(noLocationFoundBlockFields.body)).toBeInTheDocument();
        });
        test('show corresponding message if error during locations list retrieving', async () => {
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });

            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.reject('something went wrong')),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            const noLocationFoundBlockFields = mockLocationDetailsPage.includes.Entry[1].fields;
            expect(screen.getByText(noLocationFoundBlockFields.header)).toBeInTheDocument();
            expect(screen.getByText(noLocationFoundBlockFields.body)).toBeInTheDocument();
        });

        it('show loader if personalized content is loading', async () => {
            (usePersonalization as jest.Mock).mockReturnValue({
                loading: true,
                actions: { initializePersonalizationDependency: jest.fn(), getPersonalizedSection: jest.fn() },
            });

            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });

            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(emptyLocationsByStateMock)),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });

            screen.getByRole('progressbar');
            const noLocationFoundBlockFields = mockLocationDetailsPage.includes.Entry[1].fields;
            expect(screen.queryByText(noLocationFoundBlockFields.header)).not.toBeInTheDocument();
            expect(screen.queryByText(noLocationFoundBlockFields.body)).not.toBeInTheDocument();
        });
    });
    describe('when locations list is returned', () => {
        beforeEach(async () => {
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(locationsByStateMock)),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });

        test('a11y', async () => {
            const results = await axe(renderResult);
            expect(results).toHaveNoViolations();
        });
        test('should have correct meta description', async () => {
            const locationMock = locationsByStateMock.stateOrProvince.cities[0].locations[0];
            await waitFor(() => {
                // eslint-disable-next-line testing-library/no-node-access
                expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                    'content',
                    'Location Details page description from CMS'
                );
            });

            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('title')).toHaveTextContent("Gahanna store | Arby's");
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('link[rel="canonical"]')).toHaveAttribute(
                'href',
                `${process.env.NEXT_PUBLIC_APP_URL}/locations/${locationMock.url}`
            );
        });
        test('should have secondary banner if provided', async () => {
            const secondaryBannerContainer = getSecondaryBannerContainer();
            expect(secondaryBannerContainer).toBeInTheDocument();
        });
        test('should have notification banner if provided', async () => {
            const notificationBannerContainer = getNotificationBannerContainer();
            expect(notificationBannerContainer).toBeInTheDocument();
        });
        test('should show nearby locations if provided', async () => {
            const locationMock = locationsByStateMock.stateOrProvince.cities[0].locations[0];
            expect(screen.getByText(/locations nearby/i)).toBeInTheDocument();
            expect(searchLocationsByCoordinates).toBeCalledTimes(1);
            expect(searchLocationsByCoordinates).toBeCalledWith({
                lat: locationMock.details?.latitude,
                lng: locationMock.details?.longitude,
            });
        });
        test('should have location news section if provided', async () => {
            const goLocationNewsSection = screen.getByText(/Facilities/i);
            expect(goLocationNewsSection).toBeInTheDocument();
        });
        test('should not have location news section if not applicable', async () => {
            const goLocationNewsSection = screen.queryByText(/go location/i);
            expect(goLocationNewsSection).not.toBeInTheDocument();
        });
        test('should have location info section if provided with common description', async () => {
            const locationDescriptionSection = screen.getByText(locationDescriptionMapping.GENERAL_LOCATION);
            expect(locationDescriptionSection).toBeInTheDocument();
        });
        test('should have locationInfo section with locationHours', async () => {
            screen.getByText(/STORE HOURS/i);
            screen.getByText(THourDayOfWeekModel.Sunday);
            screen.getByText(THourDayOfWeekModel.Monday);
            screen.getByText(THourDayOfWeekModel.Tuesday);
            screen.getByText(THourDayOfWeekModel.Wednesday);
            screen.getByText(THourDayOfWeekModel.Thursday);
            screen.getByText(THourDayOfWeekModel.Friday);
            screen.getByText(THourDayOfWeekModel.Saturday);
        });
        test('should have locationInfo section with locationServices', async () => {
            screen.getByText(/SERVICES/i);
            screen.getByText(LocationServicesModel[IServiceTypeModel.UfcViewing]);
        });
        test('should have promo section if provided', async () => {
            const promoSection = screen.getByText(/tall house beer every day/i);
            expect(promoSection).toBeInTheDocument();
        });
        test('should have secondaryBanner section with updated styles', async () => {
            // eslint-disable-next-line testing-library/no-node-access
            const container = screen.getByText(/LOOKING FOR A DREAM JOB\?/i).parentElement.parentElement;
            // eslint-disable-next-line testing-library/no-node-access
            const wrapper = screen.getByText(/LOOKING FOR A DREAM JOB\?/i).parentElement;
            const mainText = screen.getByText(/LOOKING FOR A DREAM JOB\?/i);
            // eslint-disable-next-line testing-library/no-node-access
            const descriptionBlock = screen.getByText(/At Buffalo Wild Wings, we forste a winning/i).parentElement;
            const title = screen.getByText(/Game-time energy\. Lifetime experience\./i);
            const description = screen.getByText(/At Buffalo Wild Wings, we forste a winning/i);

            expect(container).toHaveClass('secondaryBanner');
            expect(wrapper).toHaveClass('wrapperConf');
            expect(mainText).toHaveClass('sectionBannerMainText');
            expect(descriptionBlock).toHaveClass('sectionBannerDescriptionBlock');
            expect(title).toHaveClass('sectionBannerTitle');
            expect(description).toHaveClass('sectionBannerDescription');
        });

        test('should have infoBlock section with updated styles', async () => {
            // eslint-disable-next-line testing-library/no-node-access
            const cardSubheader = screen.getByText(/Grab a beer\. Catch all the action/i).parentElement;
            const cardLink =
                // eslint-disable-next-line testing-library/no-node-access
                screen.getByText(/Grab a beer\. Catch all the action/i).parentElement.parentElement.parentElement;

            expect(cardSubheader).toHaveClass('cardSubheader');
            expect(cardLink).toHaveClass('link card');
        });

        test('should have sections with updated styles', async () => {
            const mainTitle = screen.getByText(/In bar experience/i);
            // eslint-disable-next-line testing-library/no-node-access
            const wrapper = screen.getByText(/In bar experience/i).parentElement.parentElement;

            // eslint-disable-next-line testing-library/no-node-access
            const mainContent = screen.getByText(/In bar experience/i).parentElement.parentElement.nextElementSibling;

            expect(mainTitle).toHaveClass('sectionLayoutMainTitle');
            expect(wrapper).toHaveClass('wrapperComponents');
            expect(mainContent).toHaveClass('sectionFlexContainer');
        });

        test('should have tap list section if provided', async () => {
            expect(screen.getByText(/Angry Orchard Crisp Apple Cider/i)).toBeInTheDocument();
        });
    });
    describe('when location has specific type', () => {
        beforeEach(async () => {
            const goLocationMock = {
                ...locationsByStateMock,
            };
            goLocationMock.stateOrProvince.cities[0].locations[0].services = ['GO_LOCATION'];
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: jest.fn().mockImplementation(() => Promise.resolve(goLocationMock)),
                getLocationUrlsList: jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock)),
            });
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });
        test('should have location info section if provided with specific description', async () => {
            const locationDescriptionSection = screen.getByText(locationDescriptionMapping.GO_LOCATION);
            expect(locationDescriptionSection).toBeInTheDocument();
        });
    });
    describe('when location page feature is turned off', () => {
        const getLocationListMock = jest.fn().mockImplementation(() => Promise.resolve(locationsByStateMock));
        const getLocationUrlsListMock = jest.fn().mockImplementation(() => Promise.resolve(locationUrlsMock));
        beforeEach(async () => {
            (isLocationPagesOn as jest.Mock).mockReturnValue(false);
            (useLocationList as jest.Mock).mockReturnValue({
                getLocationList: getLocationListMock,
                getLocationUrlsList: getLocationUrlsListMock,
            });
            await act(async () => {
                const { render } = await getPage({
                    route: routeMock,
                });

                renderResult = render().container;
            });
        });
        test('should not call services', async () => {
            expect(routerReplaceMock).toHaveBeenCalledTimes(1);
            expect(routerReplaceMock).toHaveBeenCalledWith('/404');
            expect(getLocationListMock).toHaveBeenCalledTimes(0);
            expect(getLocationUrlsListMock).toHaveBeenCalledTimes(0);
        });
    });
});
