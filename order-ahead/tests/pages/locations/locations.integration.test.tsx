import { screen, waitFor, act, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as reactRedux from 'react-redux';
import { getPage } from '../../lib/integration/getPage';
import { advanceTo, clear } from 'jest-date-mock';
import fetchMock from 'jest-fetch-mock';
import { axe } from '../../../jest/axe-helper';
import GoogleMapReact from 'google-map-react';

import { mockNextHead, PAGE_RENDER_TIMEOUT } from '../pageUtils';

import { server, contentfulHandlers } from '../../mock-data/server/mockServer';
import { locationsPage1, locationsPage2 } from '../../mocks/expLocations.mock';
import configFeatureFlagsMocks from '../../mocks/configFeatureFlag.mock';

import getBrandInfo from '../../../lib/brandInfo';
import {
    GTM_MAP_DOUBLE_CLICK,
    GTM_MAP_DRAG,
    GTM_MAP_DRAG_END,
    GTM_MAP_DRAG_START,
    GTM_MAP_LIST_LOCATION_CLICK,
    GTM_MAP_LOCATION_CLICK,
    GTM_SEARCH_NEW_LOCATION,
} from '../../../common/services/gtmService/constants';

import { getPageTitle } from '../../../common/helpers/getPageTitle';
import mockLocationPage from '../../mock-data/contentful/contentful-getEntry-page-locations.json';
import globalContentfulPropsMock from '../../mock-data/contentful/globalContentfulPropsMock.json';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';

const locationsPages = [locationsPage1, locationsPage2];

const dispatchMock = jest.fn();

jest.doMock('react-redux', () => ({
    ...reactRedux,
    useDispatch: jest.fn().mockReturnValue(dispatchMock),
}));
jest.mock('../../../redux/hooks/useConfiguration', () =>
    jest.fn().mockReturnValue({
        configuration: {
            ...configFeatureFlagsMocks.configuration,
            isDeliveryEnabled: false,
        },
        lastSyncedAt: new Date(),
        actions: { setConfiguration: jest.fn() },
    })
);

jest.mock('../../../redux/localStorage', () => {
    const {
        bag,
        dismissedAlertBanners,
        domainMenu,
        orderLocation,
        configuration,
    } = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        loadState: jest.fn().mockReturnValue({
            bag,
            dismissedAlertBanners,
            domainMenu,
            orderLocation,
            configuration,
        }),
        saveState: jest.fn(),
    };
});

jest.mock('../../../common/services/locationService/getLocationById', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        default: jest.fn().mockResolvedValue(mockStoreValues.orderLocation.pickupAddress),
    };
});

jest.mock('../../../common/services/domainMenu', () => {
    const mockStoreValues = require('../../mock-data/store/productsListingPageStore.mock.json');

    return {
        __esModule: true,
        getLocationMenu: jest.fn().mockResolvedValue(mockStoreValues.domainMenu.payload),
    };
});

jest.mock('google-map-react', () => {
    let _onClickCallback;
    let _onDragCallback;
    let _onDragEndCallback;
    let _onFirstLocationSelectCallback;
    let _onLastLocationSelectCallback;

    return {
        __esModule: true,
        default: ({ children, onClick, onDrag, onDragEnd }) => {
            _onClickCallback = onClick;
            _onDragCallback = onDrag;
            _onDragEndCallback = onDragEnd;
            _onFirstLocationSelectCallback = children[0]?.props.onLocationSelect;
            _onLastLocationSelectCallback = children[children.length - 1]?.props.onLocationSelect;

            return null;
        },
        get onClickCallback() {
            return _onClickCallback;
        },
        get onDragCallback() {
            return _onDragCallback;
        },
        get onDragEndCallback() {
            return _onDragEndCallback;
        },
        get onFirstLocationSelectCallback() {
            return _onFirstLocationSelectCallback;
        },
        get onLastLocationSelectCallback() {
            return _onLastLocationSelectCallback;
        },
    };
});

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());

const mapMock = require('google-map-react') as jest.MockedClass<
    typeof GoogleMapReact & {
        onClickCallback: () => void;
        onDragCallback: () => void;
        onDragEndCallback: () => void;
        onFirstLocationSelectCallback: () => void;
        onLastLocationSelectCallback: () => void;
    }
>;

describe('Locations Page - Integration', () => {
    let renderResult;
    let clearNextHeadMock;

    const renderComponent = async () => {
        await act(async () => {
            const { render } = await getPage({
                route: '/locations?q=47586',
            });

            renderResult = render().container;
        });
    };

    beforeAll(async () => {
        clearNextHeadMock = mockNextHead();

        advanceTo(new Date('2020-12-01T10:20:30Z'));

        fetchMock.mockResponse((req: Request) => {
            if (req.url === 'https://maps.googleapis.com/maps/api/geocode/json?address=47586&key=mock-google-key') {
                return Promise.resolve(JSON.stringify({ results: [{ geometry: { location: { lat: 10, lng: 20 } } }] }));
            }

            if (req.url.match(/\/webExpApiPath\/v1\/location\/*/)) {
                const url = new URL(req.url, 'https://localhost');
                const page = Number(url.searchParams.get('page'));

                return Promise.resolve(JSON.stringify(locationsPages[page]));
            }

            return Promise.reject(new Error(`URL not mocked! - ${req.url}`));
        });

        jest.setTimeout(PAGE_RENDER_TIMEOUT);

        server.listen();
        server.use(contentfulHandlers);

        (useGlobalProps as jest.Mock).mockReturnValue(globalContentfulPropsMock);
    });

    afterAll(() => {
        clear();
        clearNextHeadMock();

        server.close();
    });

    afterEach(() => {
        dispatchMock.mockReset();
    });

    test('a11y', async () => {
        await renderComponent();
        const results = await axe(renderResult);

        expect(results).toHaveNoViolations();
    });

    test('SkipLink exists on the page', async () => {
        await renderComponent();

        await waitFor(() => {
            expect(screen.getByText(/skip to main content/i)).toBeInTheDocument();
        });
    });

    test('show locations list and load more locations', async () => {
        await renderComponent();

        await waitFor(() => {
            screen.getAllByText('find an arby’s');
        });

        expect(screen.getByText('Dunwoody - Roswell Rd')).toBeInTheDocument();

        userEvent.click(screen.getByRole('button', { name: 'View more locations' }));

        await waitFor(() => {
            screen.getByText('Willowick – Vine St');
        });

        expect(screen.queryByRole('button', { name: 'View more locations' })).not.toBeInTheDocument();
    });

    test('locations page should have correct title', async () => {
        const brandInfo = getBrandInfo();

        await renderComponent();

        await waitFor(() => {
            expect(global.window).toBeDefined();
        });

        expect(global.window.document).toBeDefined();

        expect(brandInfo).toBeDefined();
        expect(brandInfo.brandName).toBeDefined();

        await waitFor(() => {
            expect(global.window.document.title).toBe(
                getPageTitle(brandInfo.brandName, mockLocationPage.items[0].fields.metaTitle)
            );
        });
    });

    test('home page should have correct meta description', async () => {
        await renderComponent();

        await waitFor(() => {
            // eslint-disable-next-line testing-library/no-node-access
            expect(document.querySelector('meta[name="description"]')).toHaveAttribute(
                'content',
                'Location Page description from CMS'
            );
        });

        // eslint-disable-next-line testing-library/no-node-access
        expect(document.querySelector('link[rel="canonical"]')).toHaveAttribute(
            'href',
            `${process.env.NEXT_PUBLIC_APP_URL}/locations?q=47586`
        );
    });

    test('locations page should fire GTM events', async () => {
        await renderComponent();

        await waitFor(() => {
            expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_SEARCH_NEW_LOCATION });
        });

        mapMock.onDragCallback();
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DRAG_START });
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DRAG });

        mapMock.onDragEndCallback();
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DRAG_END });

        act(() => {
            mapMock.onLastLocationSelectCallback();
        });
        expect(dispatchMock).toHaveBeenCalledWith(expect.objectContaining({ type: GTM_MAP_LOCATION_CLICK }));

        fireEvent.dblClick(screen.queryByLabelText('locationsMap'));
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_MAP_DOUBLE_CLICK });

        userEvent.click(screen.queryByRole('button', { name: 'Select Mentor – Reynolds Rd location' }));
        expect(dispatchMock).toHaveBeenCalledWith(expect.objectContaining({ type: GTM_MAP_LIST_LOCATION_CLICK }));
    });
});
