import { renderHook } from '../../utils';
import useAddToBagFromOrderHistory from '../../../common/hooks/useAddToBagFromOrderHistory';
import { orderHistoryItemMock } from '../../mocks/orderHistoryItem.mocks';
import { useBag } from '../../../redux/hooks';

jest.mock('../../../redux/hooks/useBag', () =>
    jest.fn().mockReturnValue({
        actions: {
            putToBag: jest.fn(),
        },
    })
);

describe('useAddToBagFromOrderHistory hook', () => {
    it('should return modifierGroups', () => {
        const { result } = renderHook(() => useAddToBagFromOrderHistory(orderHistoryItemMock.products));

        const { modifierGroups } = result.current;
        expect(modifierGroups).toMatchInlineSnapshot(`
            Array [
              Array [
                Object {
                  "isOnSideChecked": false,
                  "metadata": Object {
                    "MODIFIER_GROUP_TYPE": "Extras",
                  },
                  "modifiers": Array [],
                  "productId": "IDPModifierGroup-0001",
                },
                Object {
                  "isOnSideChecked": false,
                  "metadata": Object {
                    "MODIFIER_GROUP_TYPE": "Extras",
                  },
                  "modifiers": Array [],
                  "productId": "IDPModifierGroup-0002",
                },
                Object {
                  "isOnSideChecked": false,
                  "metadata": Object {
                    "MODIFIER_GROUP_TYPE": "Extras",
                  },
                  "modifiers": Array [],
                  "productId": "IDPModifierGroup-0003",
                },
                Object {
                  "isOnSideChecked": false,
                  "metadata": Object {
                    "IS_VISIBLE": "TRUE",
                    "MODIFIER_GROUP_ID": "11686",
                    "MODIFIER_GROUP_TYPE": "Modifications",
                  },
                  "modifiers": Array [],
                  "productId": "IDPModifierGroup-11686",
                },
                Object {
                  "isOnSideChecked": false,
                  "metadata": Object {
                    "IS_VISIBLE": "TRUE",
                    "MODIFIER_GROUP_ID": "12564",
                    "MODIFIER_GROUP_TYPE": "Sauces",
                  },
                  "modifiers": Array [],
                  "productId": "IDPModifierGroup-12564",
                },
              ],
            ]
        `);
    });
    it('should call putToBag redux action in the returned callback', () => {
        const { result } = renderHook(() => useAddToBagFromOrderHistory(orderHistoryItemMock.products));

        const { addFromOrderHistory } = result.current;
        const {
            actions: { putToBag },
        } = useBag();

        addFromOrderHistory();

        expect(putToBag).toBeCalled();
    });
});
