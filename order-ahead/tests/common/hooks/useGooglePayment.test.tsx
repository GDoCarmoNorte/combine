import { renderHook, act } from '@testing-library/react-hooks';
import createErrorWrapper from '../../../common/services/createErrorWrapper';
import { injectScriptOnce } from '../../../lib/injectScriptOnce';
import { useGooglePayment } from '../../../common/hooks/useGooglePayment';
import { useConfiguration } from '../../../common/hooks/useConfiguration';

jest.mock('react-redux');
jest.mock('../../../common/services/createErrorWrapper');
jest.mock('../../../@generated/webExpApi/apis/index');
jest.mock('../../../lib/injectScriptOnce');
jest.mock('../../../lib/getFeatureFlags');
jest.mock('../../../common/hooks/useConfiguration', () => ({
    useConfiguration: jest.fn(),
}));

const injectScriptMock = jest.fn(() => {
    return Promise.resolve('Success');
});

const googleObjectForCompatibleDeviceMock = {
    payments: {
        api: {
            PaymentsClient: jest.fn().mockImplementation(() => {
                return {
                    isReadyToPay: jest.fn(() => {
                        return Promise.resolve({ result: true });
                    }),
                };
            }),
        },
    },
    maps: window.google?.maps,
};

const googleObjectForNotCompatibleDeviceMock = {
    payments: {
        api: {
            PaymentsClient: jest.fn().mockImplementation(() => {
                return {
                    isReadyToPay: jest.fn(() => {
                        return Promise.resolve({ result: false });
                    }),
                };
            }),
        },
    },
    maps: window.google?.maps,
};

const CurrentGoogle = window.google;

describe('useGooglePayment', () => {
    beforeAll(() => {
        window.google = googleObjectForCompatibleDeviceMock;
    });

    afterAll(() => {
        window.google = CurrentGoogle;
    });

    beforeEach(() => {
        (injectScriptOnce as jest.Mock) = injectScriptMock;
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isGooglePayEnabled: true,
            },
        });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should pass full success flow when -> isGooglePayEnabled: true, canMakePayments: true, initGooglePayment: success', async () => {
        const mockedResult = { sessionKey: 'test', iframeHtml: document.createElement('div') };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result, waitForNextUpdate } = renderHook(() => useGooglePayment());

        expect(result.current.isLoading).toBeTruthy();
        await waitForNextUpdate();

        expect(injectScriptOnce).toHaveBeenCalledWith(process.env.NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL);
        expect(result.current.isCompatible).toBeTruthy();
        expect(result.current.isLoading).toBeFalsy();
        expect(createErrorWrapper).toHaveBeenCalledTimes(1);

        await act(async () => {
            await result.current.initPayment({
                locationId: '13',
                totalAmount: 10,
            });
        });

        expect(result.current.payment).toEqual(mockedResult);
        expect(result.current.error).toBeNull();
        expect(result.current.isLoading).toBeFalsy();

        await act(async () => {
            await result.current.reset();
        });
        expect(result.current.payment).toBeNull();
        expect(result.current.error).toBeNull();
        expect(result.current.isLoading).toBeFalsy();
    });

    it('Should skip if -> isGooglePayEnabled: false', async () => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isGooglePayEnabled: false,
            },
        });
        const { result } = renderHook(() => useGooglePayment());

        expect(injectScriptOnce).toHaveBeenCalledTimes(0);
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.isCompatible).toBeFalsy();
    });

    it('Should fill error if -> initGooglePayment: rejects', async () => {
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.reject());

        const { result, waitForNextUpdate } = renderHook(() => useGooglePayment());

        await waitForNextUpdate();

        expect(injectScriptOnce).toHaveBeenCalledWith(process.env.NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL);
        expect(result.current.isCompatible).toBeTruthy();
        expect(result.current.isLoading).toBeFalsy();
        expect(createErrorWrapper).toHaveBeenCalledTimes(1);

        await act(async () => {
            await result.current.initPayment({
                locationId: '13',
                totalAmount: 10,
            });
        });

        expect(result.current.payment).toBeNull();
        expect(result.current.error).not.toBeNull();
        expect(result.current.isLoading).toBeFalsy();
    });

    it('Should skip init payment -> isReadyToPay: false', async () => {
        window.google = googleObjectForNotCompatibleDeviceMock;
        (injectScriptOnce as jest.Mock).mockImplementation(() => Promise.resolve());

        const { result, waitForNextUpdate } = renderHook(() => useGooglePayment());

        expect(result.current.isLoading).toBeTruthy();
        await waitForNextUpdate();
        expect(injectScriptOnce).toHaveBeenCalledWith(process.env.NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL);
        expect(result.current.isCompatible).toBeFalsy();
        expect(result.current.isLoading).toBeFalsy();
        window.google = googleObjectForCompatibleDeviceMock;
    });
});
