import getRichTextOptions from '../../../common/helpers/getRichTextOptions';
import useRichTextOptions from '../../../common/hooks/useRichTextOptions';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';

jest.mock('../../../redux/hooks/useGlobalProps', () => jest.fn());
jest.mock('../../../common/helpers/getRichTextOptions', () => jest.fn().mockReturnValue('options'));

describe('useRichTextOptions', () => {
    it('should call getRichTextOptions func with pdp paths', () => {
        (useGlobalProps as jest.Mock).mockReturnValueOnce({ productDetailsPagePaths: 'paths' });

        const result = useRichTextOptions();

        expect(result).toBe('options');
        expect(getRichTextOptions).toHaveBeenCalledWith('paths', undefined);
    });
});
