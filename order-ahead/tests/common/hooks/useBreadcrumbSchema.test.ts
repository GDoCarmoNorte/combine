import { useBreadcrumbSchema } from '../../../common/hooks/useBreadcrumbSchema';
import { useBreadcrumbs } from '../../../common/hooks/useBreadcrumbs';

jest.mock('../../../common/hooks/useBreadcrumbs');

describe('useBreadcrumbSchema', () => {
    it('should return correct data', () => {
        (useBreadcrumbs as jest.Mock).mockReturnValue([
            { as: '/path', href: '/path', title: 'path' },
            { as: '/path/path-name2', href: '/path/[path-name2]', title: 'path name2' },
        ]);

        expect(useBreadcrumbSchema()).toEqual({
            '@context': 'https://schema.org',
            '@type': 'BreadcrumbList',
            itemListElement: [
                {
                    '@type': 'ListItem',
                    position: 1,
                    item: {
                        '@id': process.env.NEXT_PUBLIC_APP_URL,
                        name: 'Home',
                    },
                },
                {
                    '@type': 'ListItem',
                    position: 2,
                    item: {
                        '@id': `${process.env.NEXT_PUBLIC_APP_URL}/path`,
                        name: 'Path',
                    },
                },
                {
                    '@type': 'ListItem',
                    position: 3,
                    item: {
                        '@id': `${process.env.NEXT_PUBLIC_APP_URL}/path/path-name2`,
                        name: 'Path name2',
                    },
                },
            ],
        });
    });

    it('should return correct data for only home page', () => {
        (useBreadcrumbs as jest.Mock).mockReturnValue([]);

        expect(useBreadcrumbSchema()).toEqual({
            '@context': 'https://schema.org',
            '@type': 'BreadcrumbList',
            itemListElement: [
                {
                    '@type': 'ListItem',
                    position: 1,
                    item: {
                        '@id': process.env.NEXT_PUBLIC_APP_URL,
                        name: 'Home',
                    },
                },
            ],
        });
    });
});
