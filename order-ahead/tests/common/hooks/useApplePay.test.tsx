import { renderHook, act } from '@testing-library/react-hooks';
import { useApplePay } from '../../../common/hooks/useApplePay';
import createErrorWrapper from '../../../common/services/createErrorWrapper';
import { injectScriptOnce } from '../../../lib/injectScriptOnce';
import { useConfiguration } from '../../../common/hooks/useConfiguration';

jest.mock('react-redux');
jest.mock('../../../common/services/createErrorWrapper');
jest.mock('../../../@generated/webExpApi/apis/index');
jest.mock('../../../lib/injectScriptOnce');
jest.mock('../../../lib/getFeatureFlags');
jest.mock('../../../lib/isLocalRun', () => jest.fn().mockReturnValue(false));
jest.mock('../../../common/hooks/useConfiguration', () => ({
    useConfiguration: jest.fn(),
}));

describe('useApplePay', () => {
    afterEach(() => {
        jest.resetAllMocks();
        // @ts-ignore
        delete window.FreedomPay;
    });

    it('should pass full success flow when -> isApplePayEnabled: true, canMakePayments: true, initApplePayment: success', async () => {
        const canMakePayments = jest.fn().mockReturnValue(true);
        const freedomPayMock = { Apm: { ApplePay: { canMakePayments } } };
        Object.defineProperty(window, 'FreedomPay', { value: freedomPayMock });

        (injectScriptOnce as jest.Mock).mockImplementation(() => Promise.resolve());
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isApplePayEnabled: true,
            },
        });

        const mockedResult = { sessionKey: 'test', iframeHtml: document.createElement('div') };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result, waitForNextUpdate } = renderHook(() => useApplePay());

        expect(result.current.isLoading).toBeTruthy();
        await waitForNextUpdate();

        expect(injectScriptOnce).toHaveBeenCalledWith(process.env.NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL);
        expect(canMakePayments).toHaveBeenCalledTimes(1);
        expect(result.current.isCompatible).toBeTruthy();
        expect(result.current.isLoading).toBeFalsy();
        expect(createErrorWrapper).toHaveBeenCalledTimes(1);

        await act(async () => {
            await result.current.initPayment({
                locationId: '13',
                totalAmount: 10,
            });
        });

        expect(result.current.payment).toEqual(mockedResult);
        expect(result.current.error).toBeNull();
        expect(result.current.isLoading).toBeFalsy();

        // testing flush
        await act(async () => {
            await result.current.flush();
        });
        expect(result.current.payment).toBeNull();
        expect(result.current.error).toBeNull();
        expect(result.current.isLoading).toBeFalsy();
    });

    it('Should skip if -> isApplePayEnabled: false', async () => {
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isApplePayEnabled: false,
            },
        });
        const { result } = renderHook(() => useApplePay());

        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.isCompatible).toBeFalsy();
    });

    it('Should skip init payment -> canMakePayments: false', async () => {
        const canMakePayments = jest.fn().mockReturnValue(false);
        // @ts-ignore
        window.FreedomPay.Apm.ApplePay.canMakePayments = canMakePayments;

        (injectScriptOnce as jest.Mock).mockImplementation(() => Promise.resolve());
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isApplePayEnabled: true,
            },
        });
        const { result, waitForNextUpdate } = renderHook(() => useApplePay());

        expect(result.current.isLoading).toBeTruthy();
        await waitForNextUpdate();
        expect(injectScriptOnce).toHaveBeenCalledWith(process.env.NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL);
        expect(canMakePayments).toHaveBeenCalledTimes(1);
        expect(result.current.isCompatible).toBeFalsy();
        expect(result.current.isLoading).toBeFalsy();
    });

    it('Should fill error if -> initApplePayment: rejects', async () => {
        const canMakePayments = jest.fn().mockReturnValue(true);
        // @ts-ignore
        window.FreedomPay.Apm.ApplePay.canMakePayments = canMakePayments;

        (injectScriptOnce as jest.Mock).mockImplementation(() => Promise.resolve());
        (useConfiguration as jest.Mock).mockReturnValue({
            configuration: {
                isApplePayEnabled: true,
            },
        });
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.reject());

        const { result, waitForNextUpdate } = renderHook(() => useApplePay());

        expect(result.current.isLoading).toBeTruthy();
        await waitForNextUpdate();

        expect(injectScriptOnce).toHaveBeenCalledWith(process.env.NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL);
        expect(canMakePayments).toHaveBeenCalledTimes(1);
        expect(result.current.isCompatible).toBeTruthy();
        expect(result.current.isLoading).toBeFalsy();
        expect(createErrorWrapper).toHaveBeenCalledTimes(1);

        await act(async () => {
            await result.current.initPayment({
                locationId: '13',
                totalAmount: 10,
            });
        });

        expect(result.current.payment).toBeNull();
        expect(result.current.error).not.toBeNull();
        expect(result.current.isLoading).toBeFalsy();

        // testing flush
        await act(async () => {
            await result.current.flush();
        });
        expect(result.current.payment).toBeNull();
        expect(result.current.error).toBeNull();
        expect(result.current.isLoading).toBeFalsy();
    });
});
