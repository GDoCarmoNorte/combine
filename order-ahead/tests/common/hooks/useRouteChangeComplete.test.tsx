import { Router } from 'next/router';
import useRouteChangeComplete from '../../../common/hooks/useRouteChangeComplete';
import { renderHook } from '@testing-library/react-hooks';

const routeChangeCompleteCallback = jest.fn();

let routeChangeCompleteMock;
Router.events.on = jest.fn((_, callback) => {
    routeChangeCompleteMock = callback;
});

describe('useRouteChangeComplete hook', () => {
    it('should invoke callback when route is changed', async () => {
        renderHook(() => useRouteChangeComplete(routeChangeCompleteCallback));

        routeChangeCompleteMock();

        expect(routeChangeCompleteCallback).toHaveBeenCalled();
        expect(routeChangeCompleteCallback).toHaveBeenCalledTimes(1);
    });
});
