import React from 'react';
import { mount } from 'enzyme';
import { useSelector, useDispatch } from 'react-redux';

import useNavIntercept, { INavInterceptHook } from '../../../redux/hooks/useNavIntercept';
import { track, untrack } from '../../../common/services/navInterceptDecorator';
import navigateService from '../../../common/services/navInterceptService';

import { actImmediate } from '../../components/clientOnly/formik/formikTestUtils';
// import { act } from 'react-dom/test-utils';

jest.mock('react-redux');

jest.mock('../../../common/services/navInterceptService', () => ({
    navigate: jest.fn(),
}));
jest.mock('../../../common/services/navInterceptDecorator', () => ({
    track: jest.fn(),
    untrack: jest.fn(),
}));

const renderNavInterceptHook = async () => {
    let navInterceptHook: INavInterceptHook;

    const Component = () => {
        navInterceptHook = useNavIntercept();

        return null;
    };
    const wrapper = mount(<Component />);

    await actImmediate(wrapper);

    return navInterceptHook;
};

describe('useNavIntercept', () => {
    it('should render hook', async () => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
        (useSelector as jest.Mock).mockReturnValue({ isPending: false, hasChanges: false });
        const { isPending, navigate } = await renderNavInterceptHook();

        expect(isPending).toBe(false);
        expect(navigate).toBeDefined();
    });

    it('should track routing on changes', async () => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
        (useSelector as jest.Mock).mockReturnValue({ isPending: false, hasChanges: true });

        await renderNavInterceptHook();

        expect(track).toHaveBeenCalled();
    });

    it('should untrack routing on changes', async () => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
        (useSelector as jest.Mock).mockReturnValue({ isPending: false, hasChanges: false });

        await renderNavInterceptHook();

        expect(untrack).toHaveBeenCalled();
    });

    it('should navigate', async () => {
        (useDispatch as jest.Mock).mockReturnValue(() => jest.fn());
        (useSelector as jest.Mock).mockReturnValue({ isPending: true, hasChanges: true });

        const { isPending, navigate } = await renderNavInterceptHook();

        expect(isPending).toBe(true);

        navigate(true);

        expect(navigateService.navigate).toHaveBeenCalled();
    });
});
