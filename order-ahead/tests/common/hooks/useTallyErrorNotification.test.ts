import { useTallyErrorNotification } from '../../../common/hooks/useTallyErrorNotification';
import { TErrorTypeEXTERNALModel, TTallyErrorCodeModel } from '../../../@generated/webExpApi/models';
import { experiencingTechnicalDifficultiesError } from '../../../common/services/createErrorWrapper';

const enqueueErrorMock = jest.fn();
jest.mock('../../../redux/hooks', () => ({
    useNotifications: jest.fn(() => ({ actions: { enqueueError: enqueueErrorMock } })),
}));

describe('useTallyErrorNotification', () => {
    const mockTallyError = {
        code: TTallyErrorCodeModel.DeliveryProviderDeliveryTimeNotAvailable,
        message: 'Test message',
        type: TErrorTypeEXTERNALModel,
        data: {},
    };

    const mockReqestedError = {
        name: 'Test name',
        message: '',
    };
    it('should return a function wich will call enqueueError with a message from ITallyError500ExternalResponseModel', () => {
        useTallyErrorNotification()(mockTallyError as any);
        expect(enqueueErrorMock).toBeCalledWith({
            message: mockTallyError?.message,
        });
    });

    it('should return a function wich will call enqueueError with a message from RequestError', () => {
        useTallyErrorNotification()(mockReqestedError as any);
        expect(enqueueErrorMock).toBeCalledWith({
            message: experiencingTechnicalDifficultiesError,
        });
    });
});
