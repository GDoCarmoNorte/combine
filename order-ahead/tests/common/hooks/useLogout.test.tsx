import { renderHook } from '@testing-library/react-hooks';
import { useLogout } from '../../../common/hooks/useLogout';
import { getCookieValue, setCookieValue } from '../../../common/helpers/cookieHelper';
import { RAW_BLAZIN_REWARDS_COOKIE_NAME } from '../../../components/organisms/play/constants';

const logout = jest.fn();
jest.mock('@auth0/auth0-react', () => {
    return {
        useAuth0: () => {
            return {
                isAuthenticated: true,
                loginWithRedirect: jest.fn(),
                logout,
            };
        },
        Auth0Provider: jest.fn(),
        withAuthenticationRequired: jest.fn(),
    };
});

describe('useLogout', () => {
    const { location } = window;
    beforeAll(() => {
        delete window.location;
        (window.location as any) = { origin: 'http://localhost' };
    });

    afterAll(() => {
        window.location = location;
    });

    it('should remove blazin rewards related cookie and call auth0 logout', () => {
        const { result } = renderHook(() => useLogout());

        setCookieValue(RAW_BLAZIN_REWARDS_COOKIE_NAME, 'test');
        result.current.logoutAndClearCookies();

        expect(logout).toHaveBeenCalledWith({ returnTo: window.location.origin });
        expect(getCookieValue(RAW_BLAZIN_REWARDS_COOKIE_NAME)).toEqual('');
    });
});
