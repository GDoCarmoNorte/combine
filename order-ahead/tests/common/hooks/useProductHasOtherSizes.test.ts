import { useProductHasOtherSizes } from '../../../common/hooks/useProductHasOtherSizes';
import { useProductSizes } from '../../../redux/hooks/domainMenu';

jest.mock('../../../redux/hooks/domainMenu', () => ({
    useProductSizes: jest.fn(),
}));

describe('useProductHasOtherSizes', () => {
    it('should return true', () => {
        (useProductSizes as jest.Mock).mockReturnValue(['test', 'test']);
        const result = useProductHasOtherSizes('id');

        expect(result).toEqual(true);
    });

    it('should return false when input = undefined', () => {
        (useProductSizes as jest.Mock).mockReturnValue(undefined);
        const result = useProductHasOtherSizes('id');

        expect(result).toEqual(false);
    });

    it('should return false when input = empty array', () => {
        (useProductSizes as jest.Mock).mockReturnValue([]);
        const result = useProductHasOtherSizes('id');

        expect(result).toEqual(false);
    });
});
