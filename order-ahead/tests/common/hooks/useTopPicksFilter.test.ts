import { renderHook } from '@testing-library/react-hooks';
import useTopPicksFilter from '../../../common/hooks/useTopPicksFilter';
import getBrandInfo, { BRANDS } from '../../../lib/brandInfo';
import { useProducts } from '../../../redux/hooks/domainMenu';
import useOrderLocation from '../../../redux/hooks/useOrderLocation';

jest.mock('../../../redux/hooks/domainMenu');
jest.mock('../../../redux/hooks/useOrderLocation');
jest.mock('../../../lib/brandInfo');

const contentfulProductsMock: any[] = [
    {
        fields: { productId: '1' },
    },
    {
        fields: { productId: '2' },
    },
];

describe('useTopPicksFilter', () => {
    beforeEach(() => {
        (useProducts as jest.Mock).mockReturnValue({
            '1': { availability: { isAvailable: true }, isSaleable: true },
            '2': { availability: { isAvailable: true }, isSaleable: true },
        });
    });

    it(`[${BRANDS.bww.brandId}]: should return all products, location is not selected`, () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: BRANDS.bww.brandId });
        (useOrderLocation as jest.Mock).mockReturnValue({});

        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useTopPicksFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { productId: '1' },
            },
            {
                fields: { productId: '2' },
            },
        ]);
    });

    it(`[${BRANDS.bww.brandId}]: should return all products, location is selected`, () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: BRANDS.bww.brandId });
        (useOrderLocation as jest.Mock).mockReturnValue({ isCurrentLocationOAAvailable: true });

        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useTopPicksFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { productId: '1' },
            },
            {
                fields: { productId: '2' },
            },
        ]);
    });

    it(`[${BRANDS.arbys.brandId}]:should return all products, location is selected`, () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: BRANDS.arbys.brandId });
        (useOrderLocation as jest.Mock).mockReturnValue({ isCurrentLocationOAAvailable: true });
        (useProducts as jest.Mock).mockReturnValue({
            '1': { availability: { isAvailable: false }, isSaleable: true },
            '2': { availability: { isAvailable: false }, isSaleable: true },
        });

        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useTopPicksFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { productId: '1' },
            },
            {
                fields: { productId: '2' },
            },
        ]);
    });

    it(`[${BRANDS.bww.brandId}]: should return one available product, location is selected`, () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: BRANDS.bww.brandId });
        (useOrderLocation as jest.Mock).mockReturnValue({ isCurrentLocationOAAvailable: true });
        (useProducts as jest.Mock).mockReturnValue({
            '1': { availability: { isAvailable: true }, isSaleable: true },
            '2': { availability: { isAvailable: false }, isSaleable: true },
        });

        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useTopPicksFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { productId: '1' },
            },
        ]);
    });
});
