import { renderHook } from '@testing-library/react-hooks';
import { IUseTips, useTips } from '../../../common/hooks/useTips';

jest.mock('../../../redux/hooks/useOrderLocation');

const getMockTipsData = ({
    tipsItems = [
        { amount: 1, percentage: 0 },
        { amount: 2, percentage: 0 },
        { amount: 3, percentage: 0 },
    ],
    titleText = 'Add tip:',
    isError = false,
    errorMessage = '',
    defaultTipsId = 1,
}): IUseTips => ({
    tipsItems,
    titleText,
    isError,
    errorMessage,
    defaultTipsId,
});

describe('useTips hook', () => {
    describe('pick-up', () => {
        it('should return correct tips data without error for pickup', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(9, '', true));

            expect(current).toEqual(getMockTipsData({}));
        });

        it('should return correct tips data without error when custom tips more than $10 for pickup (only the first button has a percentage field)', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(12, '', true));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 1.2, percentage: 10 },
                        { amount: 2, percentage: 0 },
                        { amount: 3, percentage: 0 },
                    ],
                })
            );
        });

        it('should return correct tips data with error for pickup', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(9, '10', true));

            expect(current).toEqual(
                getMockTipsData({ isError: true, errorMessage: 'Enter amount less than your subtotal $9' })
            );
        });
    });

    describe('delivery', () => {
        it('should return correct tips data without error for delivery', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(9, '', false));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 2, percentage: 0 },
                        { amount: 3, percentage: 0 },
                        { amount: 4, percentage: 0 },
                    ],
                    titleText: 'Add driver tip:',
                    defaultTipsId: 1,
                })
            );
        });

        it('should return correct tips data with error for delivery', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(45, '40', false));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 7, percentage: 0 },
                        { amount: 8, percentage: 0 },
                        { amount: 9, percentage: 0 },
                    ],
                    titleText: 'Add driver tip:',
                    defaultTipsId: 1,
                    isError: true,
                    errorMessage: 'Enter amount less than $39',
                })
            );
        });

        it('should return correct tips data without error for delivery order if subtotal less than 17$', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(5, '35', false));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 2, percentage: 0 },
                        { amount: 3, percentage: 0 },
                        { amount: 4, percentage: 0 },
                    ],
                    titleText: 'Add driver tip:',
                    defaultTipsId: 1,
                    isError: false,
                    errorMessage: '',
                })
            );
        });

        it('should return correct tips data with error for delivery order if subtotal more than 17$', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(22.28, '35', false));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 3, percentage: 0 },
                        { amount: 4, percentage: 0 },
                        { amount: 5, percentage: 0 },
                    ],
                    titleText: 'Add driver tip:',
                    defaultTipsId: 1,
                    isError: true,
                    errorMessage: 'Enter amount less than your subtotal $22.28',
                })
            );
        });
    });

    describe('contactless', () => {
        it('should return correct tips data for contactless if subtotal less than threashold', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(11, '', false, true));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 1, percentage: 0 },
                        { amount: 2, percentage: 0 },
                        { amount: 3, percentage: 0 },
                    ],
                    titleText: 'Add tip:',
                    defaultTipsId: 1,
                })
            );
        });

        it('should return correct tips data for contactless if subtotal higher than threashold', () => {
            const {
                result: { current },
            } = renderHook(() => useTips(12, '', false, true));

            expect(current).toEqual(
                getMockTipsData({
                    tipsItems: [
                        { amount: 1.8, percentage: 15 },
                        { amount: 2.16, percentage: 18 },
                        { amount: 2.4, percentage: 20 },
                    ],
                    titleText: 'Add tip:',
                    defaultTipsId: 1,
                })
            );
        });
    });
});
