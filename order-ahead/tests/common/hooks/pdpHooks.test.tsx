import React from 'react';
import { mount } from 'enzyme';
import { useComboPriceIncreaseAnalytics } from '../../../common/hooks/pdpHooks';
import { useAppDispatch } from '../../../redux/store';
import { IDisplayFullProduct, ProductTypesEnum } from '../../../redux/types';
import { PDPTallyItem } from '../../../redux/pdp';

jest.mock('../../../redux/store', () => ({
    useAppDispatch: jest.fn(),
}));

jest.mock('../../../common/hooks/useGtmComboData', () => () => undefined);

interface IProps {
    pdpTallyItem: PDPTallyItem;
    displayProduct: IDisplayFullProduct;
    productPath: string;
}

const Component = (props: IProps) => {
    useComboPriceIncreaseAnalytics(props.pdpTallyItem, props.displayProduct, props.productPath);
    return null;
};

const displayProductMock: Partial<IDisplayFullProduct> = {
    price: 10,
    productType: ProductTypesEnum.Meal,
    productSections: [
        {
            productSectionType: 'main',
        },
        {
            productSectionType: 'side',
        },
        {
            productSectionType: 'drink',
        },
    ],
};

const tallyItemMock = {
    productId: 'arb-itm-000-057',
    price: 6.89,
    quantity: 1,
    childItems: [
        {
            productId: 'arb-itm-000-055',
            price: 3.11,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-011',
                            price: 2,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-002',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-003',
                    modifiers: [
                        {
                            productId: 'arb-itm-008-011',
                            price: 0.29,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-008-004',
                            price: 0,
                            quantity: 1,
                        },
                        {
                            productId: 'arb-itm-008-005',
                            price: 0.29,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-004',
                    modifiers: [
                        {
                            productId: 'arb-itm-009-013',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-008',
                    modifiers: [
                        {
                            productId: 'arb-itm-013-007',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-010',
                    modifiers: [
                        {
                            productId: 'arb-itm-015-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
        {
            productId: 'arb-itm-002-002',
            price: 1.99,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-006',
                    modifiers: [],
                },
                {
                    productId: 'arb-prg-001-005',
                    modifiers: [],
                },
            ],
        },
        {
            productId: 'arb-itm-003-001',
            price: 1.79,
            quantity: 1,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-011',
                    modifiers: [
                        {
                            productId: 'arb-itm-016-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
                {
                    productId: 'arb-prg-001-012',
                    modifiers: [
                        {
                            productId: 'arb-itm-017-001',
                            price: 0,
                            quantity: 1,
                        },
                    ],
                },
            ],
        },
    ],
};

describe('useComboPriceIncreaseAnalytics hook', () => {
    it('should fire GA event when price inreases', async () => {
        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const mounted = mount(
            <Component
                pdpTallyItem={tallyItemMock}
                productPath="productPath"
                displayProduct={displayProductMock as IDisplayFullProduct}
            />
        );
        mounted.setProps({
            displayProduct: { ...displayProductMock, price: 11 },
        });

        expect(dispatchMock).toHaveBeenCalled();
    });
    it('should fire GA event when price was increased and side product with same sum price is selected', async () => {
        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        // let mounted: ReactWrapper;
        const mounted = mount(
            <Component
                pdpTallyItem={tallyItemMock}
                productPath="productPath"
                displayProduct={displayProductMock as IDisplayFullProduct}
            />
        );
        mounted.setProps({
            displayProduct: { ...displayProductMock, price: 11 },
        });

        const modifiedProductSections = displayProductMock.productSections.slice();
        modifiedProductSections[2] = {
            productSectionType: 'drink',
            productSectionDisplayName: 'Drinks',
        };
        mounted.setProps({
            displayProduct: {
                ...displayProductMock,
                price: 12,
                productSections: modifiedProductSections,
            },
        });

        expect(dispatchMock).toHaveBeenCalledTimes(2);
    });

    it('should not fire GA event when price decreases', async () => {
        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const mounted = mount(
            <Component
                pdpTallyItem={tallyItemMock}
                productPath="productPath"
                displayProduct={displayProductMock as IDisplayFullProduct}
            />
        );
        mounted.setProps({
            displayProduct: { ...displayProductMock, price: 9 },
        });

        expect(dispatchMock).not.toHaveBeenCalled();
    });

    it('should not fire GA event when price inreases but product is not combo', async () => {
        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const mounted = mount(
            <Component
                pdpTallyItem={tallyItemMock}
                productPath="productPath"
                displayProduct={displayProductMock as IDisplayFullProduct}
            />
        );
        mounted.setProps({
            displayProduct: { ...displayProductMock, price: 11, productType: ProductTypesEnum.Single },
        });

        expect(dispatchMock).not.toHaveBeenCalled();
    });
});
