import { renderHook } from '@testing-library/react-hooks';
import useMenuCategoryProductsFilter from '../../../common/hooks/useMenuCategoryProductsFilter';
import getBrandInfo from '../../../lib/brandInfo';
import { useProducts } from '../../../redux/hooks/domainMenu';
import useOrderLocation from '../../../redux/hooks/useOrderLocation';

jest.mock('../../../redux/hooks/domainMenu');
jest.mock('../../../redux/hooks/useOrderLocation');
jest.mock('../../../lib/brandInfo');

const contentfulProductsMock: any[] = [
    {
        fields: { isVisible: true, productId: '1' },
    },
    {
        fields: { isVisible: false, productId: '2' },
    },
    {
        fields: { isVisible: true, productId: '3' },
    },
];

describe('useMenuCategoryProducts', () => {
    beforeEach(() => {
        (useProducts as jest.Mock).mockReturnValue({
            '1': { isSaleable: true, availability: { isAvailable: true } },
            '2': { isSaleable: true, availability: { isAvailable: true } },
            '3': { isSaleable: true, availability: { isAvailable: false }, itemGroupId: '3' },
            '4': { isSaleable: false, availability: { isAvailable: false } },
            '5': { isSaleable: true, availability: { isAvailable: true }, itemGroupId: '5' },
        });
    });

    it('should return all isVisible products, Arbys', () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Arbys' });
        (useOrderLocation as jest.Mock).mockReturnValue({ currentLocation: {} });
        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useMenuCategoryProductsFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { isVisible: true, productId: '1' },
            },
            {
                fields: { isVisible: true, productId: '3' },
            },
        ]);
    });

    it('should return all isVisible products, location is non oa available, BWW', () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Bww' });
        (useOrderLocation as jest.Mock).mockReturnValue({ isCurrentLocationOAAvailable: false });

        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useMenuCategoryProductsFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { isVisible: true, productId: '1' },
            },
            {
                fields: { isVisible: true, productId: '3' },
            },
        ]);
    });

    it('should return available and saleable products, location is oa available, BWW', () => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Bww' });
        (useOrderLocation as jest.Mock).mockReturnValue({ isCurrentLocationOAAvailable: true });

        const {
            result: { current },
        } = renderHook(() => {
            const productsFilter = useMenuCategoryProductsFilter();
            return contentfulProductsMock.filter(productsFilter);
        });

        expect(current).toEqual([
            {
                fields: { isVisible: true, productId: '1' },
            },
        ]);
    });
});
