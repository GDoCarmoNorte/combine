import { renderHook } from '@testing-library/react-hooks';
import { useBagRenderItems } from '../../../common/hooks/useBagRenderItems';
import { useDomainProducts } from '../../../redux/hooks/domainMenu';
import domainProductMock from '../../mocks/domainProduct.mock';

const bogoProductMock = {
    ...domainProductMock,
    id: 'bogo',
    itemGroupId: 'IDPItemGroup-0001',
    isSaleable: true,
};

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch,
    createSelectorHook: jest.fn(),
}));

jest.mock('../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn(),
}));

jest.mock('../../../redux/hooks/domainMenu');

jest.mock('../../../redux/hooks/useGlobalProps', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        productsById: {
            [domainProductMock.id]: domainProductMock,
            [bogoProductMock.id]: bogoProductMock,
            'unavailable product': {
                productId: 'unavailable product',
                displayName: 'unavailable product',
                isAvailable: false,
            },
            'without price': {
                productId: 'without price',
                displayName: 'without price',
                isAvailable: true,
            },
        },
    })),
}));

jest.mock('../../../redux/hooks/useBag', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        bagEntries: [
            {
                lineItemId: 0,
                price: 2,
                productId: domainProductMock.id,
                quantity: 1,
                modifierGroups: [],
            },
            {
                lineItemId: 5,
                price: 6,
                productId: bogoProductMock.id,
                quantity: 1,
                modifierGroups: [],
            },
            {
                lineItemId: 1,
                price: 10.99,
                productId: 'unavailable product',
                quantity: 2,
                modifierGroups: [],
            },
            {
                lineItemId: 1,
                price: null,
                productId: 'without price',
                quantity: 1,
                modifierGroups: [],
            },
        ],
        entriesMarkedAsRemoved: [1],
    })),
}));

jest.mock('../../../redux/hooks/domainMenu');

jest.mock('.../../../redux/hooks/useTally', () => ({
    useTallyItemIsPromoList: () => ({ 'arb-itm-003-050': true }),
}));

jest.mock('.../../../redux/hooks/useOrderLocation', () => ({
    __esModule: true,
    default: jest.fn().mockReturnValue({
        timezone: 'America/New_York',
        contactDetails: {
            address: {
                line1: '31219 Vine St',
                line2: '',
                line3: '',
                postalCode: '44095-3555',
                stateProvinceCode: 'OH',
                countryCode: 'US',
                city: 'WILLOWICK',
            },
            phone: '440-944-7717',
        },
        additionalFeatures: {
            isPayAtStoreEnabled: true,
            maxOrderAmount: 100,
        },
        isOnlineOrderAvailable: true,
        isClosed: false,
    }),
}));

jest.mock('../../../lib/domainProduct', () => ({
    __esModule: true,
    getProductPath: jest.fn().mockReturnValue(''),
    getRelatedProduct: jest.fn(),
    isProductBogo: jest.requireActual('../../../lib/domainProduct').isProductBogo,
}));

jest.mock('../../../common/hooks/useUnavailableCategories', () => ({
    useUnavailableCategories: () => [],
}));

describe('useBagRenderItems', () => {
    beforeEach(() => {
        (useDomainProducts as jest.Mock).mockReturnValue({
            [domainProductMock.id]: { ...domainProductMock, isSaleable: true },
            [bogoProductMock.id]: bogoProductMock,
        });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should return correct result', () => {
        const {
            result: { current: renderItems },
        } = renderHook(() => useBagRenderItems());

        expect(renderItems.length).toEqual(4);

        expect(renderItems[0].isAvailable).toBe(true);
        expect(renderItems[0].markedAsRemoved).toBe(false);

        // bogo product should be available
        expect(renderItems[1].isAvailable).toBe(true);

        // unavailable product
        expect(renderItems[2].isAvailable).toBe(false);
        expect(renderItems[2].markedAsRemoved).toBe(true);

        // renderItem should be unavailable if it has no price
        expect(renderItems[3].isAvailable).toBe(false);
    });
});
