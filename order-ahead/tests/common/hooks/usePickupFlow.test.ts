import { renderHook, act } from '@testing-library/react-hooks';
import usePickupLocationsSearch from '../../../common/hooks/usePickupLocationsSearch';
import usePickupFlow from '../../../common/hooks/usePickupFlow';
import { LoadingStatusEnum } from '../../../common/types';
import { OrderLocationMethod } from '../../../redux/orderLocation';
import { useRouter } from 'next/router';
import { useAppDispatch } from '../../../redux/store';
import {
    GTM_MAP_LIST_LOCATION_CLICK,
    GTM_MAP_LOCATION_CLICK,
    GTM_SEARCH_NEW_LOCATION,
} from '../../../common/services/gtmService/constants';
import { useOrderLocation, useDomainMenu } from '../../../redux/hooks';

jest.mock('../../../redux/store');
jest.mock('../../../common/hooks/usePickupLocationsSearch');
jest.mock('next/router');
jest.mock('../../../redux/hooks');
jest.mock('../../../redux/hooks/useTallyOrder', () => ({
    __esModule: true,
    default: jest.fn().mockReturnValue({
        setUnavailableTallyItems: jest.fn(),
    }),
}));

describe('usePickupFlow', () => {
    const dispatchMock = jest.fn();
    const setLocationMock = jest.fn();
    const getDomainMenuMock = jest.fn();
    const searchLocationsByQueryMock = jest.fn();
    const searchLocationsByCoordinates = jest.fn();
    const resetSearchResultMock = jest.fn();
    const fetchMoreLocationsMock = jest.fn();
    const routerPush = jest.fn();
    const searchLocationsByUserGeolocationMock = jest.fn();
    const setPreviousLocationMock = jest.fn();

    beforeEach(() => {
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        (useOrderLocation as jest.Mock).mockReturnValue({
            actions: { setPickupLocation: setLocationMock, setPreviousLocation: setPreviousLocationMock },
            method: OrderLocationMethod.NOT_SELECTED,
            pickupAddress: null,
            deliveryAddress: null,
        });
        (useDomainMenu as jest.Mock).mockReturnValue({ actions: { getDomainMenu: getDomainMenuMock } });
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                q: '',
            },
            push: routerPush,
        });
        (usePickupLocationsSearch as jest.Mock).mockReturnValue({
            searchLocationsByQuery: searchLocationsByQueryMock,
            searchLocationsByCoordinates: searchLocationsByCoordinates,
            searchLocationsByUserGeolocation: searchLocationsByUserGeolocationMock,
            fetchMoreLocations: fetchMoreLocationsMock,
            resetSearchResult: resetSearchResultMock,
            locationsSearchStatus: LoadingStatusEnum.Idle,
            locationsSearchResult: null,
            moreLocationsFetchStatus: LoadingStatusEnum.Idle,
            isAbleToFetchMoreLocations: false,
        });
        (sessionStorage.getItem as jest.Mock).mockReturnValue(null);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should not reset searh result if location query is not defined', () => {
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                q: undefined,
            },
            push: routerPush,
        });
        const { result } = renderHook(() => usePickupFlow());

        expect(searchLocationsByQueryMock).not.toHaveBeenCalled();
        expect(resetSearchResultMock).not.toHaveBeenCalled();
        expect(result.current.locationQuery).toBeUndefined();
    });

    it('should reset searh result if location query is empty', () => {
        const { result } = renderHook(() => usePickupFlow());

        expect(searchLocationsByQueryMock).not.toHaveBeenCalled();
        expect(resetSearchResultMock).toHaveBeenCalled();
        expect(result.current.locationQuery).toBe('');
    });

    it('should set search value from query and search locations by query', () => {
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                q: 'jasper',
            },
            push: jest.fn(),
        });

        const { result } = renderHook(() => usePickupFlow());

        expect(searchLocationsByQueryMock).toHaveBeenCalledWith('jasper');
        expect(result.current.locationQuery).toBe('jasper');
        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_SEARCH_NEW_LOCATION });
    });

    it('should search locations by coordinates if no location query and lat long are provided', () => {
        (sessionStorage.getItem as jest.Mock).mockReturnValue('10,10');

        const { result } = renderHook(() => usePickupFlow());

        expect(searchLocationsByQueryMock).not.toHaveBeenCalled();
        expect(searchLocationsByCoordinates).toHaveBeenCalledWith({ lat: 10, lng: 10 });
        expect(result.current.locationQuery).toBe('');
    });

    it('should show current location in top if intop query is specified', () => {
        (usePickupLocationsSearch as jest.Mock).mockReturnValue({
            searchLocationsByQuery: jest.fn(),
            searchLocationsByCoordinates: jest.fn(),
            fetchMoreLocations: jest.fn(),
            resetSearchResult: jest.fn(),
            locationsSearchStatus: LoadingStatusEnum.Idle,
            locationsSearchResult: { locations: [{ id: '1' }, { id: '2' }] },
            moreLocationsFetchStatus: LoadingStatusEnum.Idle,
            isAbleToFetchMoreLocations: false,
        });
        (useRouter as jest.Mock).mockReturnValue({
            query: {
                q: 'jasper',
                intop: true,
            },
            push: jest.fn(),
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            actions: { setPickupLocation: setLocationMock },
            method: OrderLocationMethod.PICKUP,
            pickupAddress: { id: '2' },
            deliveryAddress: null,
        });

        const { result } = renderHook(() => usePickupFlow());

        expect(result.current.locationsList).toEqual([{ id: '2' }, { id: '1' }]);
    });

    describe('onMapLocationSelect', () => {
        it('should set selected location and dispatch gtm event', () => {
            const { result } = renderHook(() => usePickupFlow());
            const locationMock = { id: '1' } as any;

            act(() => {
                result.current.onMapLocationSelect(locationMock);
            });

            expect(result.current.selectedLocation).toBe(locationMock);
            expect(dispatchMock).toHaveBeenCalledWith({
                type: GTM_MAP_LOCATION_CLICK,
                payload: { location: locationMock },
            });
        });
    });

    describe('onViewMoreLocationsClick', () => {
        it('should fetch more locations', () => {
            const { result } = renderHook(() => usePickupFlow());

            act(() => {
                result.current.onViewMoreLocationsClick();
            });

            expect(fetchMoreLocationsMock).toHaveBeenCalled();
        });
    });

    describe('onUseMyLocationClick', () => {
        it('should call searchLocationsByUserGeolocation', () => {
            const { result } = renderHook(() => usePickupFlow());

            act(() => {
                result.current.onUseMyLocationClick();
            });

            expect(searchLocationsByUserGeolocationMock).toHaveBeenCalled();
        });
    });

    describe('onSearchClick', () => {
        it('should not search and reset selected location if locations search status is loading', () => {
            (usePickupLocationsSearch as jest.Mock).mockReturnValue({
                searchLocationsByQuery: jest.fn(),
                searchLocationsByCoordinates: jest.fn(),
                fetchMoreLocations: jest.fn(),
                resetSearchResult: jest.fn(),
                locationsSearchStatus: LoadingStatusEnum.Loading,
                locationsSearchResult: { locations: [{ id: '1' }, { id: '2' }] },
                moreLocationsFetchStatus: LoadingStatusEnum.Idle,
                isAbleToFetchMoreLocations: false,
            });

            const { result } = renderHook(() => usePickupFlow());

            const locationMock = { id: '1' } as any;

            act(() => {
                // set selected location
                result.current.onLocationSelect(locationMock);
            });

            expect(result.current.selectedLocation).toBe(locationMock);

            act(() => {
                result.current.onSearchClick();
            });

            expect(routerPush).not.toHaveBeenCalled();
            expect(result.current.selectedLocation).toBe(locationMock);
        });

        it('should search locations and reset selected location', () => {
            const { result } = renderHook(() => usePickupFlow());
            const locationMock = { id: '1' } as any;

            act(() => {
                // set selected location
                result.current.onLocationSelect(locationMock);
            });

            expect(result.current.selectedLocation).toBe(locationMock);

            act(() => {
                result.current.onSearchClick();
            });

            expect(result.current.selectedLocation).toBe(null);
            expect(routerPush).toHaveBeenCalledWith('/locations?q=', null, {
                shallow: true,
            });
        });

        it('should search locations and reset selected location if q is not specified', () => {
            const { result } = renderHook(() => usePickupFlow());
            const locationMock = { id: '1' } as any;
            const routerPushMock = jest.fn();

            (useRouter as jest.Mock).mockReturnValue({
                query: {},
                push: routerPushMock,
            });

            act(() => {
                // set selected location
                result.current.onLocationSelect(locationMock);
            });

            expect(result.current.selectedLocation).toBe(locationMock);

            act(() => {
                result.current.onSearchClick();
            });

            expect(result.current.selectedLocation).toBe(null);
            expect(routerPushMock).toHaveBeenCalledWith('/locations?q=', null, {
                shallow: true,
            });
        });

        it('should perform a new search if click on search with the same query', () => {
            const routerPushMock = jest.fn();

            (useRouter as jest.Mock).mockReturnValue({
                query: {
                    q: 'jasper',
                },
                push: routerPushMock,
            });

            const { result } = renderHook(() => usePickupFlow());

            expect(searchLocationsByQueryMock).toHaveBeenNthCalledWith(1, 'jasper');

            act(() => {
                result.current.onSearchClick();
            });

            expect(routerPushMock).toHaveBeenCalledWith('/locations?q=jasper', null, {
                shallow: true,
            });

            expect(searchLocationsByQueryMock).toHaveBeenNthCalledWith(2, 'jasper');
        });
    });

    describe('onSearchInputChange', () => {
        it('should change searchInputValue', () => {
            const { result } = renderHook(() => usePickupFlow());

            act(() => {
                result.current.onSearchInputChange({ target: { value: 'new value' } } as any);
            });

            expect(result.current.searchInputValue).toBe('new value');
        });
    });

    describe('onSearchInputKeyPress', () => {
        it('should trigger search on enter key press', () => {
            const { result } = renderHook(() => usePickupFlow());

            act(() => {
                result.current.onSearchInputKeyPress({ key: 'Enter' } as any);
            });

            expect(routerPush).toHaveBeenCalledWith('/locations?q=', null, {
                shallow: true,
            });
        });

        it('should not trigger search on not enter key press', () => {
            const { result } = renderHook(() => usePickupFlow());

            act(() => {
                result.current.onSearchInputKeyPress({ key: 'Alt' } as any);
            });

            expect(routerPush).not.toHaveBeenCalled();
        });
    });

    describe('onLocationSelect', () => {
        it('should set selected location and dispatch gtm event', () => {
            const { result } = renderHook(() => usePickupFlow());
            const locationMock = { id: '1' } as any;

            act(() => {
                result.current.onLocationSelect(locationMock);
            });

            expect(result.current.selectedLocation).toBe(locationMock);
            expect(dispatchMock).toHaveBeenCalledWith({
                type: GTM_MAP_LIST_LOCATION_CLICK,
                payload: { location: locationMock },
            });
        });
    });

    describe('onLocationSet', () => {
        it('should call setLocation and request domain menu', () => {
            const { result } = renderHook(() => usePickupFlow());
            const locationMock = { id: '1' } as any;

            act(() => {
                result.current.onLocationSet(locationMock);
            });

            expect(setLocationMock).toHaveBeenCalledWith(locationMock);
            expect(getDomainMenuMock).toHaveBeenCalledWith(locationMock);
            expect(setPreviousLocationMock).toHaveBeenCalledWith(null);
        });
    });
});
