import { renderHook } from '@testing-library/react-hooks';
import { fireEvent, waitFor } from '@testing-library/react';
import { useFreedomPay } from '../../../common/hooks/useFreedomPay';
import {
    TFPPaymentTypes,
    TInitialPaymentTypes,
} from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import logger from '../../../common/services/logger';
import { CARD_ON_FILE } from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/constants';

jest.mock('react-redux');
jest.mock('../../../common/services/addJsonError');
jest.mock('../../../@generated/webExpApi/apis/index');
jest.mock('../../../common/services/logger');

describe('useFreedomPay', () => {
    const currentPaymentDomain = process.env.NEXT_PUBLIC_PAYMENT_DOMAIN;
    const paymentDomainMock = 'https://test.com';

    beforeAll(() => {
        process.env.NEXT_PUBLIC_PAYMENT_DOMAIN = paymentDomainMock;
    });

    afterAll(() => {
        process.env.NEXT_PUBLIC_PAYMENT_DOMAIN = currentPaymentDomain;
    });

    it('should parse correctly Google payment response', async () => {
        const googlePayResponseMock = {
            attributes: [
                {
                    Key: 'CardIssuer',
                    Value: 'Visa',
                },
                {
                    Key: 'MaskedCardNumber',
                    Value: 'Visa •••• 1111',
                },
                {
                    Key: 'BillingAddress',
                    Value: {
                        postalCode: '12345',
                        name: 'Test User',
                    },
                },
                {
                    Key: 'Name',
                    Value: 'Test User',
                },
            ],
            paymentKeys: ['b4939708-63f1-4a9d-a8c6-c950131452d1'],
            paymentType: 'GooglePay',
        };
        const expectedResult = {
            billingPostalCode: '12345',
            cardHolderName: 'Test User',
            cardIssuer: 'Visa',
            keys: ['b4939708-63f1-4a9d-a8c6-c950131452d1'],
            maskedCardNumber: 'Visa •••• 1111',
            paymentType: TInitialPaymentTypes.GOOGLE_PAY,
        };
        const { result } = renderHook(() => useFreedomPay(TInitialPaymentTypes.GOOGLE_PAY));

        const messageEvent = new MessageEvent('message', {
            data: { type: 3, data: googlePayResponseMock },
            origin: paymentDomainMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(result.current.payment).toEqual(expectedResult);
        });

        await waitFor(() => {
            expect(result.current.errors.length).toEqual(0);
        });
    });
    it('should parse correctly Google payment response if no attributes provided', async () => {
        const googlePayResponseMock = {
            paymentKeys: ['b4939708-63f1-4a9d-a8c6-c950131452d1'],
            paymentType: 'GooglePay',
        };
        const expectedResult = {
            keys: ['b4939708-63f1-4a9d-a8c6-c950131452d1'],
            paymentType: TInitialPaymentTypes.GOOGLE_PAY,
        };
        const { result } = renderHook(() => useFreedomPay(TInitialPaymentTypes.GOOGLE_PAY));

        const messageEvent = new MessageEvent('message', {
            data: { type: 3, data: googlePayResponseMock },
            origin: paymentDomainMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(result.current.payment).toEqual(expectedResult);
        });

        expect(result.current.errors.length).toEqual(0);
    });

    it('should parse correctly CC payment response', async () => {
        const ccPayResponseMock = {
            attributes: [
                {
                    Key: 'CardIssuer',
                    Value: 'Visa',
                },
                {
                    Key: 'MaskedCardNumber',
                    Value: '411111XXXXXX1111',
                },
                {
                    Key: 'ExpirationDate',
                    Value: '12/21',
                },
                {
                    Key: 'PostalCode',
                    Value: '12345',
                },
            ],
            paymentKeys: ['9d598f03-d333-4f77-a7aa-21283337d465', 'b46be251-3aab-48ce-9938-465b8e66f227'],
            paymentType: 'Card',
        };
        const expectedResult = {
            keys: ['9d598f03-d333-4f77-a7aa-21283337d465', 'b46be251-3aab-48ce-9938-465b8e66f227'],
            cardIssuer: 'Visa',
            maskedCardNumber: '411111XXXXXX1111',
            paymentType: TInitialPaymentTypes.CREDIT_OR_DEBIT,
        };
        const { result } = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

        const messageEvent = new MessageEvent('message', {
            data: { type: 3, data: ccPayResponseMock },
            origin: paymentDomainMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(result.current.payment).toEqual(expectedResult);
        });

        expect(result.current.errors.length).toEqual(0);
    });

    it('should parse correctly card on file payment response', async () => {
        const cardOnFileResponseMock = {
            paymentKeys: ['b4939708-63f1-4a9d-a8c6-c950131452d1'],
            paymentType: TFPPaymentTypes.CARD_ON_FILE,
        };
        const expectedResult = {
            keys: ['b4939708-63f1-4a9d-a8c6-c950131452d1'],
            paymentType: CARD_ON_FILE,
        };
        const { result } = renderHook(() => useFreedomPay(CARD_ON_FILE));

        const messageEvent = new MessageEvent('message', {
            data: { type: 3, data: cardOnFileResponseMock },
            origin: paymentDomainMock,
        });
        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(result.current.payment).toEqual(expectedResult);
        });

        expect(result.current.errors.length).toEqual(0);
    });

    it('should parse correctly CC payment response if no attribute provided', async () => {
        const ccPayResponseMock = {
            paymentKeys: ['9d598f03-d333-4f77-a7aa-21283337d465', 'b46be251-3aab-48ce-9938-465b8e66f227'],
            paymentType: 'Card',
        };
        const expectedResult = {
            keys: ['9d598f03-d333-4f77-a7aa-21283337d465', 'b46be251-3aab-48ce-9938-465b8e66f227'],
            paymentType: TInitialPaymentTypes.CREDIT_OR_DEBIT,
        };
        const { result } = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

        const messageEvent = new MessageEvent('message', {
            data: { type: 3, data: ccPayResponseMock },
            origin: paymentDomainMock,
        });

        fireEvent(window, messageEvent);

        await waitFor(() => {
            expect(result.current.payment).toEqual(expectedResult);
        });

        expect(result.current.errors.length).toEqual(0);
    });

    describe('event types from freedom pay', () => {
        let renderHookResult;

        describe('when event type: error', () => {
            const EVENT_TYPE = 1;
            const errorsData = ['some', 'next', 'another one'];

            afterAll(() => jest.clearAllMocks());

            beforeAll(async () => {
                renderHookResult = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

                const messageEvent = new MessageEvent('message', {
                    data: { type: EVENT_TYPE, data: { errors: errorsData } },
                    origin: paymentDomainMock,
                });

                fireEvent(window, messageEvent);
            });

            it('should return "payment" = null, "height" = null, "isValid" = false and all fields under "fieldsStatus" = false', () => {
                const { result } = renderHookResult;
                expect(result.current.payment).toBeNull();
                expect(result.current.height).toBeNull();
                expect(result.current.isValid).toBeFalsy();
                expect(result.current.fieldsStatus).toEqual({
                    CardNumber: false,
                    ExpirationDate: false,
                    PostalCode: false,
                    SecurityCode: false,
                });
            });

            it('should return "errors"', () => {
                const { result } = renderHookResult;
                expect(result.current.errors).toEqual(errorsData);
            });

            it('should log "payment_iframe_error" error w/ "type" = "CREDIT_OR_DEBIT" and "errors" fields to NR', async () => {
                expect(logger.logError).toHaveBeenCalledWith('payment_iframe_error', {
                    type: 'CREDIT_OR_DEBIT',
                    errors: errorsData,
                });
            });
        });

        describe('when event type: frame height"', () => {
            const EVENT_TYPE = 2;
            const heightData = 987;

            afterAll(() => jest.clearAllMocks());

            beforeAll(async () => {
                renderHookResult = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

                const messageEvent = new MessageEvent('message', {
                    data: { type: EVENT_TYPE, data: { height: heightData } },
                    origin: paymentDomainMock,
                });

                fireEvent(window, messageEvent);
            });

            it('should return "payment" = null, "errors" = null, "isValid" = false and all fields under "fieldsStatus" = false', () => {
                const { result } = renderHookResult;
                expect(result.current.payment).toBeNull();
                expect(result.current.errors).toBeNull();
                expect(result.current.isValid).toBeFalsy();
                expect(result.current.fieldsStatus).toEqual({
                    CardNumber: false,
                    ExpirationDate: false,
                    PostalCode: false,
                    SecurityCode: false,
                });
            });

            it('should return "height" = 987', () => {
                const { result } = renderHookResult;
                expect(result.current.height).toEqual(heightData);
            });

            it('should NOT log error to NR', async () => {
                expect(logger.logError).not.toHaveBeenCalled();
            });
        });

        describe('when event type: payment keys"', () => {
            const EVENT_TYPE = 3;

            describe('when "attributes" is an array', () => {
                const paymentData = {
                    paymentType: TFPPaymentTypes.CREDIT_OR_DEBIT,
                    paymentKeys: ['one', 'two'],
                    attributes: [
                        { Key: 'CardIssuer', Value: 'some' },
                        { Key: 'MaskedCardNumber', Value: 'othervaluehere' },
                        { Key: 'BillingAddress', Value: { name: 'myname-here', postalCode: '11122' } },
                    ],
                };
                afterAll(() => jest.clearAllMocks());

                beforeAll(async () => {
                    renderHookResult = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

                    const messageEvent = new MessageEvent('message', {
                        data: { type: EVENT_TYPE, data: paymentData },
                        origin: paymentDomainMock,
                    });

                    fireEvent(window, messageEvent);
                });

                it('should return "height" = null, empty "errors" array, "isValid" = false and all fields under "fieldsStatus" = false', () => {
                    const { result } = renderHookResult;
                    expect(result.current.height).toBeNull();
                    expect(result.current.errors).toHaveLength(0);
                    expect(result.current.isValid).toBeFalsy();
                    expect(result.current.fieldsStatus).toEqual({
                        CardNumber: false,
                        ExpirationDate: false,
                        PostalCode: false,
                        SecurityCode: false,
                    });
                });

                it('should return "payment" w/ "keys" and "paymentType"', () => {
                    const { result } = renderHookResult;
                    expect(result.current.payment).toMatchSnapshot();
                });

                it('should NOT log error to NR', async () => {
                    expect(logger.logError).not.toHaveBeenCalled();
                });
            });

            describe('when "attributes" is NOT an array', () => {
                const paymentData = {
                    paymentType: TFPPaymentTypes.CREDIT_OR_DEBIT,
                    paymentKeys: ['one', 'two'],
                };
                afterAll(() => jest.clearAllMocks());

                beforeAll(async () => {
                    renderHookResult = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

                    const messageEvent = new MessageEvent('message', {
                        data: { type: EVENT_TYPE, data: paymentData },
                        origin: paymentDomainMock,
                    });

                    fireEvent(window, messageEvent);
                });

                it('should return "height" = null, empty "errors" array, "isValid" = false and all fields under "fieldsStatus" = false', () => {
                    const { result } = renderHookResult;
                    expect(result.current.height).toBeNull();
                    expect(result.current.errors).toHaveLength(0);
                    expect(result.current.isValid).toBeFalsy();
                    expect(result.current.fieldsStatus).toEqual({
                        CardNumber: false,
                        ExpirationDate: false,
                        PostalCode: false,
                        SecurityCode: false,
                    });
                });

                it('should return "payment" w/ "keys" and "paymentType"', () => {
                    const { result } = renderHookResult;
                    expect(result.current.payment).toMatchSnapshot();
                });

                it('should NOT log error to NR', async () => {
                    expect(logger.logError).not.toHaveBeenCalled();
                });
            });
        });

        describe('when event type: others"', () => {
            const EVENT_TYPE = 4;
            const scenarios = ['CardNumber', 'ExpirationDate', 'SecurityCode', 'PostalCode'];

            describe.each(scenarios)('when emittedType.%s', (scenario) => {
                const othersData = {
                    emittedBy: scenario,
                    isValid: true,
                };

                afterAll(() => jest.clearAllMocks());

                beforeAll(async () => {
                    renderHookResult = renderHook(() => useFreedomPay(TInitialPaymentTypes.CREDIT_OR_DEBIT));

                    const messageEvent = new MessageEvent('message', {
                        data: { type: EVENT_TYPE, data: othersData },
                        origin: paymentDomainMock,
                    });
                    await fireEvent(window, messageEvent);
                });

                it('should return "payment" = null, "errors" = null, "isValid" = false', () => {
                    const { result } = renderHookResult;
                    expect(result.current.payment).toBeNull();
                    expect(result.current.errors).toBeNull();
                    expect(result.current.isValid).toBeFalsy();
                });

                it(`should return "${scenario}" = true`, () => {
                    const { result } = renderHookResult;
                    expect(result.current.fieldsStatus[scenario]).toBeTruthy();
                });

                it('should NOT log error to NR', async () => {
                    expect(logger.logError).not.toHaveBeenCalled();
                });
            });
        });
    });
});
