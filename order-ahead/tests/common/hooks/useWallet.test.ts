import createErrorWrapper from '../../../common/services/createErrorWrapper';
import { renderHook } from '@testing-library/react-hooks';
import { useWallet } from '../../../common/hooks/useWallet';

jest.mock('../../../common/services/createErrorWrapper');
jest.mock('../../../@generated/webExpApi/apis/index');

describe('useWallet', () => {
    it('should initialize wallet once loaded', async () => {
        const mockedResult = { sessionKey: 'test', iframeHtml: document.createElement('div') };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));
        const { result, waitForNextUpdate } = renderHook(() => useWallet());
        expect(result.current.isLoading).toBeTruthy();
        await waitForNextUpdate();
        expect(createErrorWrapper).toHaveBeenCalled();
        expect(result.current.isLoading).toBeFalsy();
    });
});
