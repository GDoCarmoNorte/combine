import { renderHook } from '@testing-library/react-hooks';
import { useAuth0 } from '@auth0/auth0-react';
import { useCustomerPaymentMethod } from '../../../common/hooks/useCustomerPaymentMethod';

jest.mock('@auth0/auth0-react');

jest.mock('../../../redux/hooks', () => ({
    useNotifications: jest.fn(() => ({ actions: { enqueueError: jest.fn() } })),
}));

jest.mock('../../../common/services/paymentMethods', () => ({
    __esModule: true,
    initPaymentMethodsService: jest.fn().mockReturnValue({
        fetchPaymentMethods: jest.fn().mockImplementation(() => Promise.resolve('mocked-payment-methods')),
        deleteCreditCard: jest.fn(),
    }),
}));

jest.mock('../../../lib/getFeatureFlags', () => ({
    isCustomerPaymentMethodEnabled: jest.fn().mockReturnValue(true),
}));

describe('useCustomerPaymentMethod hook', () => {
    it('should return no methods if user not authenticated', () => {
        (useAuth0 as jest.Mock).mockReturnValue({
            isAuthenticated: false,
        });

        const { result } = renderHook(() => useCustomerPaymentMethod());

        expect(result.current).toMatchObject({
            paymentMethods: null,
            loading: false,
        });
    });

    it('should return methods if user authenticated and isCustomerPaymentMethodEnabled', async () => {
        (useAuth0 as jest.Mock).mockReturnValue({
            isAuthenticated: true,
            user: { test: true },
            getIdTokenClaims: jest.fn().mockResolvedValue({ __raw: 'jwtTest' }),
        });

        const { result, waitForNextUpdate } = renderHook(() => useCustomerPaymentMethod());
        await waitForNextUpdate();

        expect(result.current.loading).toBe(false);
        expect(result.current.paymentMethods).toBe('mocked-payment-methods');
    });
});
