import { renderHook } from '@testing-library/react-hooks';
import useSurvey, { TypeSurveyEnum } from '../../../common/hooks/useSurvey';
import { useIDToken } from '../../../common/hooks/useIDToken';
import { useSurveyService } from '../../../common/hooks/useSurveyService';
import { act } from 'react-dom/test-utils';
import { useAccount, useNotifications, useMyTeams } from '../../../redux/hooks';

jest.mock('react-redux');
jest.mock('../../../common/hooks/useIDToken');
jest.mock('../../../common/hooks/useSurveyService');

jest.mock('../../../redux/hooks');

describe('useSurvey hook', () => {
    const enqueueErrorMock = jest.fn();
    const setMyTeamsLoading = jest.fn();
    beforeEach(() => {
        (useAccount as jest.Mock).mockReturnValue({
            account: null,
        });
        (useNotifications as jest.Mock).mockReturnValue({
            actions: { enqueueError: enqueueErrorMock },
        });
        (useMyTeams as jest.Mock).mockReturnValue({
            myTeams: {
                loading: false,
            },
            actions: {
                setMyTeams: jest.fn(),
                setMyTeamsLoading,
            },
        });
        (useIDToken as jest.Mock).mockReturnValue({ idToken: '' });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should call updateSurveys with correct error message when type survey is delete', async () => {
        const updateSurveys = jest.fn().mockImplementation(() => Promise.reject());
        (useSurveyService as jest.Mock).mockReturnValue({ getSurveys: jest.fn(), updateSurveys });

        const {
            result: { current },
        } = renderHook(() => useSurvey());

        await act(async () => {
            await current.actions.updateSurveys({
                surveyId: '',
                surveyRespond: [],
                typeSurvey: TypeSurveyEnum.DELETE,
            });
        });

        expect(enqueueErrorMock).toBeCalledWith({ message: "We couldn't remove the team. Please try again later." });
    });

    it('should call updateSurveys with default error message', async () => {
        const updateSurveys = jest.fn().mockImplementation(() => Promise.reject());
        (useSurveyService as jest.Mock).mockReturnValue({ getSurveys: jest.fn(), updateSurveys });

        const {
            result: { current },
        } = renderHook(() => useSurvey());

        await act(async () => {
            await current.actions.updateSurveys({
                surveyId: '',
                surveyRespond: [],
            });
        });

        expect(enqueueErrorMock).toBeCalledWith({ message: 'Something was wrong. Please try again later.' });
    });

    it('should not call setMyTeamsLoading with true when user saves teams', async () => {
        const updateSurveys = jest.fn().mockImplementation(() => Promise.resolve());
        (useSurveyService as jest.Mock).mockReturnValue({ getSurveys: jest.fn(), updateSurveys });

        const {
            result: { current },
        } = renderHook(() => useSurvey());

        await act(async () => {
            await current.actions.updateSurveys({
                surveyId: '',
                surveyRespond: [],
            });
        });

        expect(setMyTeamsLoading).not.toBeCalledWith(true);
        expect(setMyTeamsLoading).toBeCalledWith(false);
    });
});
