import usePickupLocationsSearch, { IUsePickupLocationsSearch } from '../../../common/hooks/usePickupLocationsSearch';
import { locationSearch, getCoordinates } from '../../../common/services/locationService';
import { LoadingStatusEnum } from '../../../common/types';
import { renderHook, act, RenderResult, cleanup } from '@testing-library/react-hooks';

jest.mock('../../../common/services/locationService');

describe('usePickupLocationsSearch', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    const searchResultMock = { metadata: { isLastPage: false, pageNumber: 0 }, locations: [1, 2] };

    describe('searchLocationsByQuery', () => {
        it('should not search if query is not provided', async () => {
            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByQuery('');
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Idle);
        });

        it('should search locations successfully', async () => {
            (locationSearch as jest.Mock).mockResolvedValueOnce(searchResultMock);
            (getCoordinates as jest.Mock).mockResolvedValueOnce({ lat: 1, lng: 1 });

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByQuery('tell city');
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Success);
            expect(result.current.locationsSearchResult).toBe(searchResultMock);
        });

        it('should handle error if an error occurs', async () => {
            (locationSearch as jest.Mock).mockRejectedValueOnce(new Error());
            (getCoordinates as jest.Mock).mockResolvedValueOnce({ lat: 1, lng: 1 });

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByQuery('tell city');
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Error);
            expect(result.current.locationsSearchResult).toBe(null);
        });
    });

    describe('searchLocationsByCoordinates', () => {
        it('should not search if  coordinates are not provided', async () => {
            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByCoordinates({} as any);
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Idle);
        });

        it('should search locations successfully', async () => {
            (locationSearch as jest.Mock).mockResolvedValueOnce(searchResultMock);

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByCoordinates({ lat: 1, lng: 1 });
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Success);
            expect(result.current.locationsSearchResult).toBe(searchResultMock);
        });

        it('should handle error if an error occurs', async () => {
            (locationSearch as jest.Mock).mockRejectedValueOnce(new Error());

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByCoordinates({ lat: 1, lng: 1 });
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Error);
            expect(result.current.locationsSearchResult).toBe(null);
        });
    });

    describe('searchLocationsByUserGeolocation', () => {
        it('should search locations successfully', async () => {
            (navigator.geolocation.getCurrentPosition as jest.Mock).mockImplementationOnce((onSuccess) =>
                onSuccess({
                    coords: { latitude: 1, longitude: 1 },
                })
            );
            (locationSearch as jest.Mock).mockResolvedValueOnce(searchResultMock);

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByUserGeolocation();
            });

            expect(locationSearch).toHaveBeenLastCalledWith({ lat: 1, lng: 1 });
            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Success);
            expect(result.current.locationsSearchResult).toBe(searchResultMock);
        });

        it('should handle error if an error occurs', async () => {
            (navigator.geolocation.getCurrentPosition as jest.Mock).mockImplementationOnce((_, onError) =>
                onError(new Error())
            );
            (locationSearch as jest.Mock).mockResolvedValueOnce(searchResultMock);

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByUserGeolocation();
            });

            expect(locationSearch).not.toHaveBeenLastCalledWith({ lat: 1, lng: 1 });
            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Error);
            expect(result.current.locationsSearchResult).toBe(null);
        });
    });

    describe('resetSearchResult', () => {
        it('should reset search result', async () => {
            (locationSearch as jest.Mock).mockResolvedValueOnce(searchResultMock);

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByCoordinates({ lat: 1, lng: 1 });
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Success);
            expect(result.current.locationsSearchResult).toBe(searchResultMock);

            act(() => {
                result.current.resetSearchResult();
            });

            expect(result.current.locationsSearchStatus).toBe(LoadingStatusEnum.Idle);
            expect(result.current.locationsSearchResult).toBe(null);
        });
    });

    describe('fetchMoreLocations', () => {
        let hookResult: RenderResult<IUsePickupLocationsSearch>;

        beforeEach(async () => {
            (locationSearch as jest.Mock).mockResolvedValueOnce(searchResultMock);

            const { result } = renderHook(() => usePickupLocationsSearch());

            await act(async () => {
                await result.current.searchLocationsByCoordinates({ lat: 1, lng: 1 });
            });

            hookResult = result;
        });

        afterEach(async () => {
            await cleanup();
        });

        it('should not fetch if no locations search result', async () => {
            act(() => {
                hookResult.current.resetSearchResult();
            });

            await act(async () => {
                hookResult.current.fetchMoreLocations();
            });

            expect(hookResult.current.moreLocationsFetchStatus).toBe(LoadingStatusEnum.Idle);
        });

        it('should fetch more locations successfully and not fetch if last page', async () => {
            (locationSearch as jest.Mock).mockResolvedValueOnce({
                metadata: { isLastPage: true, pageNumber: 1 },
                locations: [3, 4],
            });

            await act(async () => {
                await hookResult.current.fetchMoreLocations();
            });

            expect(hookResult.current.moreLocationsFetchStatus).toBe(LoadingStatusEnum.Success);
            expect(hookResult.current.isAbleToFetchMoreLocations).toBe(false);
            expect(hookResult.current.locationsSearchResult).toEqual({
                metadata: { isLastPage: true, pageNumber: 1 },
                locations: [1, 2, 3, 4],
            });

            await act(async () => {
                await hookResult.current.fetchMoreLocations();
            });

            expect(hookResult.current.moreLocationsFetchStatus).toBe(LoadingStatusEnum.Success);
            expect(hookResult.current.isAbleToFetchMoreLocations).toBe(false);
            expect(hookResult.current.locationsSearchResult).toEqual({
                metadata: { isLastPage: true, pageNumber: 1 },
                locations: [1, 2, 3, 4],
            });
        });

        it('should handle error if an error occurs', async () => {
            (locationSearch as jest.Mock).mockRejectedValueOnce(new Error());

            await act(async () => {
                await hookResult.current.fetchMoreLocations();
            });

            expect(hookResult.current.moreLocationsFetchStatus).toBe(LoadingStatusEnum.Error);
            expect(hookResult.current.locationsSearchResult).toEqual({
                metadata: { isLastPage: false, pageNumber: 0 },
                locations: [1, 2],
            });
        });
    });
});
