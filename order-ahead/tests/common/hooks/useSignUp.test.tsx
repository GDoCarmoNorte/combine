import { signUp } from '../../../common/services/customerService';
import { act, renderHook } from '@testing-library/react-hooks';
import { GTM_EMAIL_SIGN_UP } from '../../../common/services/gtmService/constants';
import { useSignUp } from '../../../common/hooks/useSignUp';
import { ISignUpPayload } from '../../../common/services/customerService/signUp';
import { isMarketingPreferencesSignupEnabled } from '../../../lib/getFeatureFlags';
import { useAppDispatch } from '../../../redux/store';

jest.mock('../../../redux/store');
jest.mock('../../../common/services/customerService');
jest.mock('../../../lib/getFeatureFlags');
jest.mock('../../../common/hooks/useConfiguration', () => ({
    createConfiguration: jest.fn(),
}));

describe('useSignUp', () => {
    const mockPayload = {
        firstName: 'firstName',
        lastName: 'lastName',
        email: 'email@test.com',
        postalCode: '12345',
        storeId: '1',
    } as ISignUpPayload;

    it('should successfully submit signup when feature flag is enabled', async () => {
        const dispatchMock = jest.fn();

        (signUp as jest.Mock).mockImplementationOnce(() => new Promise((res) => res(true)));
        (isMarketingPreferencesSignupEnabled as jest.Mock).mockReturnValue(true);
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const { result } = renderHook(() => useSignUp());

        await act(async () => {
            await result.current.signUpMarketingPreferences(mockPayload);
        });

        expect(dispatchMock).toHaveBeenCalledWith({ type: GTM_EMAIL_SIGN_UP });
    });

    it('should not submit signup if feature flag is off', async () => {
        const dispatchMock = jest.fn();

        (signUp as jest.Mock).mockImplementationOnce(() => new Promise((res) => res(true)));
        (isMarketingPreferencesSignupEnabled as jest.Mock).mockReturnValue(false);
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const { result } = renderHook(() => useSignUp());

        await act(async () => {
            await result.current.signUpMarketingPreferences(mockPayload);
        });

        expect(dispatchMock).not.toHaveBeenCalled();
    });
});
