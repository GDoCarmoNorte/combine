import { useProductIsSaleable } from '../../../common/hooks/useProductIsSaleable';
import { useDomainProduct } from '../../../redux/hooks/domainMenu';

jest.mock('../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn(),
}));

describe('useProductIsSaleable', () => {
    it('should return correct saleable state', () => {
        (useDomainProduct as jest.Mock).mockReturnValue({ isSaleable: true });
        expect(useProductIsSaleable('').isSaleable).toEqual(true);

        (useDomainProduct as jest.Mock).mockReturnValue({ isSaleable: false });
        expect(useProductIsSaleable('').isSaleable).toEqual(false);
    });
});
