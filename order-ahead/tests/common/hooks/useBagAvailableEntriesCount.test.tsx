import { renderHook } from '@testing-library/react-hooks';
import { useSelector } from 'react-redux';
import useBagAvailableEntriesCount from '../../../common/hooks/useBagAvailableEntriesCount';
import { useBag } from '../../../redux/hooks';
import { useDomainProducts } from '../../../redux/hooks/domainMenu';

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch,
    createSelectorHook: jest.fn(),
}));

jest.mock('../../../redux/store', () => ({
    useAppDispatch: () => jest.fn(),
    useAppSelector: jest.fn().mockReturnValue(3),
}));

jest.mock('.../../../redux/hooks/useBag');
jest.mock('.../../../redux/hooks/domainMenu');

describe('useBagAvailableEntriesCount', () => {
    beforeEach(() => {
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    productId: 'arb-itm-003-125',
                    lineItemId: 1,
                    quantity: 2,
                },
                {
                    productId: 'arb-itm-000-075',
                    lineItemId: 2,
                    quantity: 1,
                },
            ],
        });
        (useDomainProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-075': { id: 'arb-itm-000-075' },

            'arb-itm-003-125': { id: 'arb-itm-003-125' },
        });
        (useSelector as jest.Mock).mockReturnValue([]);
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should return total bag entries count if all available', () => {
        const {
            result: { current },
        } = renderHook(() => useBagAvailableEntriesCount());
        expect(current).toEqual(3);
    });
    it('should remove not saleable items number from total', () => {
        (useDomainProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-075': { id: 'arb-itm-000-075' },
            'arb-itm-003-125': { id: 'arb-itm-003-125', isSaleable: false },
        });
        const {
            result: { current },
        } = renderHook(() => useBagAvailableEntriesCount());
        expect(current).toEqual(1);
    });
    it('should remove marked as removed items number from total', () => {
        (useSelector as jest.Mock).mockReturnValue([2]);
        (useDomainProducts as jest.Mock).mockReturnValue({
            'arb-itm-000-075': { id: 'arb-itm-000-075' },
            'arb-itm-003-125': { id: 'arb-itm-003-125' },
        });
        const {
            result: { current },
        } = renderHook(() => useBagAvailableEntriesCount());
        expect(current).toEqual(2);
    });
});
