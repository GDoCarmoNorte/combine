import { renderHook, act } from '@testing-library/react-hooks';
import { useBagAnalytics } from '../../../common/hooks/useBagAnalytics';
import { useBag, useOrderLocation } from '../../../redux/hooks';
import { OrderLocationMethod } from '../../../redux/orderLocation';
import locationsMock from '../../mocks/expLocations.mock';
import deliveryAddressMock from '../../mocks/deliveryAddess.mock';
import { useAppDispatch } from '../../../redux/store';
import { resolveOpeningHours } from '../../../lib/locations';
import { GTM_LOCATION_ORDER, GTM_CHANGE_LOCATION } from '../../../common/services/gtmService/constants';

jest.mock('../../../lib/locations');
jest.mock('../../../redux/hooks/useBag');
jest.mock('../../../redux/hooks/useOrderLocation');
jest.mock('../../../redux/store', () => ({
    __esModule: true,
    useAppDispatch: jest.fn(),
}));

describe('useBagAnalytics', () => {
    beforeAll(() => {
        (useBag as jest.Mock).mockReturnValue({
            orderTime: '2021-01-14T22:15:00.000Z',
        });
        (useOrderLocation as jest.Mock).mockReturnValue({
            method: OrderLocationMethod.PICKUP,
            deliveryAddress: deliveryAddressMock,
            pickupAddress: locationsMock.locations[1],
            currentLocation: locationsMock.locations[1],
        });
        (resolveOpeningHours as jest.Mock).mockReturnValue({
            storeTimezone: 'America/Chicago',
            isOpen: true,
        });
    });

    it('should send Gtm event pushGtmLocationOrder with correct data', async () => {
        const dispatchMock = jest.fn();
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const { result } = renderHook(() => useBagAnalytics());
        act(() => {
            result.current.pushGtmLocationOrder(OrderLocationMethod.PICKUP, '1');
        });
        const payload = {
            category: 'Order',
            action: 'Click PICKUP',
            label: '1',
            date: '01/14/2021',
            time: '04:15 PM',
        };

        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_LOCATION_ORDER,
            payload,
        });
        expect(dispatchMock).toHaveBeenCalledTimes(1);
    });

    it('should send Gtm event pushGtmChangeOrderDate with correct data', async () => {
        const dispatchMock = jest.fn();
        const pickupTime = '2021-01-13T22:15:00.000Z';
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const { result } = renderHook(() => useBagAnalytics());
        act(() => {
            result.current.pushGtmChangeOrderDate(OrderLocationMethod.DELIVERY, pickupTime);
        });

        const payload = {
            category: 'Order',
            action: 'Change Handoff Time',
            label: '5',
            date: '01/13/2021',
            time: '04:15 PM',
        };

        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_LOCATION_ORDER,
            payload,
        });
        expect(dispatchMock).toHaveBeenCalledTimes(1);
    });

    it('should send Gtm event pushGtmChangeLocation', async () => {
        const dispatchMock = jest.fn();

        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);

        const { result } = renderHook(() => useBagAnalytics());
        act(() => {
            result.current.pushGtmChangeLocation();
        });

        const payload = {
            category: 'Order',
            action: 'Change Address',
            label: '99982',
        };

        expect(dispatchMock).toHaveBeenCalledWith({
            type: GTM_CHANGE_LOCATION,
            payload,
        });
        expect(dispatchMock).toHaveBeenCalledTimes(1);
    });
});
