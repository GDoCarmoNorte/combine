import { renderHook } from '@testing-library/react-hooks';
import { useUnavailableItems } from '../../../common/hooks/useUnavailableItems';
import { useBag } from '../../../redux/hooks';
import { useProducts } from '../../../redux/hooks/domainMenu';

jest.mock('.../../../redux/hooks/useBag');
jest.mock('.../../../redux/hooks/domainMenu');

describe('useUnavailableItems hook', () => {
    beforeEach(() => {
        (useProducts as jest.Mock).mockReturnValue({
            'IDPSalesItem-5240': {
                id: 'IDPSalesItem-5240',
                itemModifierGroups: [
                    {
                        productGroupId: 'IDPModifierGroup-12565',
                        max: 1,
                        min: 1,
                        itemModifiers: {
                            'IDPModifier-17157-RG': {
                                max: 1,
                                min: 0,
                                defaultQuantity: 0,
                                itemId: 'IDPModifier-17157-RG',
                            },
                        },
                    },
                    {
                        productGroupId: 'IDPModifierGroup-8678',
                        max: 2,
                        min: 1,
                        itemModifiers: {
                            'IDPModifier-4103': {
                                max: 2,
                                min: 0,
                                defaultQuantity: 1,
                                itemId: 'IDPModifier-4103',
                            },
                            'IDPModifier-4100': {
                                max: 2,
                                min: 0,
                                defaultQuantity: 1,
                                itemId: 'IDPModifier-4100',
                            },
                        },
                    },
                ],
            },
            'IDPSalesItem-4865': {
                id: 'IDPSalesItem-4865',
                itemModifierGroups: [
                    {
                        productGroupId: 'IDPModifierGroup-14053',
                        max: 1,
                        min: 1,
                        itemModifiers: {
                            'IDPModifier-21849': {
                                max: 1,
                                min: 0,
                                defaultQuantity: 1,
                                itemId: 'IDPModifier-21849',
                            },
                        },
                    },
                ],
            },
            'IDPSalesItem-4866': {
                id: 'IDPSalesItem-4866',
                itemModifierGroups: [
                    {
                        productGroupId: 'IDPModifierGroup-14053',
                        max: 1,
                        min: 1,
                        itemModifiers: {
                            'IDPModifier-21849': {
                                max: 1,
                                min: 0,
                                defaultQuantity: 1,
                                itemId: 'IDPModifier-21849',
                            },
                        },
                    },
                ],
            },
            'IDPModifier-23924': {
                id: 'IDPModifier-23924',
                itemModifierGroups: [
                    {
                        productGroupId: 'IDPModifierGroup-14001',
                        max: 1,
                        min: 1,
                        itemModifiers: {
                            'IDPModifier-17157-RGA': {
                                max: 1,
                                min: 1,
                                defaultQuantity: 0,
                                itemId: 'IDPModifier-17157-RGA',
                            },
                        },
                    },
                ],
            },
        });
        (useBag as jest.Mock).mockReturnValue({
            bagEntries: [
                {
                    name: '6 Boneless Wings2',
                    quantity: 1,
                    productId: 'IDPSalesItem-5240',
                    price: 9.99,
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup-12565',
                            modifiers: [
                                {
                                    productId: 'IDPModifier-17157-RG',
                                    quantity: 1,
                                    price: 0,
                                },
                            ],
                        },
                        {
                            productId: 'IDPModifierGroup-8678',
                            modifiers: [
                                {
                                    productId: 'IDPModifier-4103',
                                    price: 0.6,
                                    quantity: 1,
                                },
                                {
                                    productId: 'IDPModifier-4100',
                                    price: 0.6,
                                    quantity: 1,
                                },
                            ],
                        },
                    ],
                    lineItemId: 1,
                    category: 'Hand-Spun Wings - Everyday reg price',
                },
                {
                    name: 'All-American Burger',
                    quantity: 1,
                    productId: 'IDPSalesItem-4865',
                    price: 14.79,
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup-14053',
                            modifiers: [
                                {
                                    productId: 'IDPModifier-21849',
                                    price: 0,
                                    quantity: 1,
                                },
                            ],
                        },
                        {
                            productId: 'IDPModifierGroup-14301',
                            modifiers: [
                                {
                                    productId: 'IDPModifier-23919',
                                    price: 0,
                                    quantity: 1,
                                },
                                {
                                    productId: 'IDPModifier-23923',
                                    price: 0,
                                    quantity: 1,
                                },
                                {
                                    productId: 'IDPModifier-23924',
                                    price: 0,
                                    quantity: 1,
                                    modifierGroups: [
                                        {
                                            productId: 'IDPModifierGroup-14000',
                                            modifiers: [
                                                {
                                                    productId: 'IDPModifier-22222',
                                                    price: 0,
                                                    quantity: 1,
                                                },
                                            ],
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                    lineItemId: 2,
                    category: 'Burgers',
                },
                {
                    name: 'All-American Burger With Unavailable default sub modifiers',
                    quantity: 1,
                    productId: 'IDPSalesItem-4866',
                    price: 14.79,
                    modifierGroups: [
                        {
                            productId: 'IDPModifierGroup-14053',
                            modifiers: [
                                {
                                    productId: 'IDPModifier-23924',
                                    price: 0,
                                    quantity: 1,
                                    modifierGroups: [
                                        {
                                            productId: 'IDPModifierGroup-14001',
                                            modifiers: [
                                                {
                                                    productId: 'IDPModifier-17157-RGA',
                                                    quantity: 1,
                                                    price: 0,
                                                },
                                            ],
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                    lineItemId: 2,
                    category: 'Burgers',
                },
            ],
        });
    });

    afterAll(() => {
        jest.clearAllMocks();
    });

    it('should return empty array if no unavailable ids are provided', () => {
        const mockUnavailableItems = [];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([]);
    });

    it('should return empty array if unavailable id is not in the bag', () => {
        const mockUnavailableItems = ['Item-ID'];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([]);
    });

    it('should return correct value for all unavailable items', () => {
        const mockUnavailableItems = ['IDPSalesItem-5240', 'IDPSalesItem-4865'];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([
            {
                isWholeProductUnavailable: true,
                isDefaultModifiersUnavailable: false,
                modifierIds: [],
                subModifierIds: [],
                productId: 'IDPSalesItem-5240',
                lineItemId: 1,
            },
            {
                isWholeProductUnavailable: true,
                isDefaultModifiersUnavailable: false,
                modifierIds: [],
                subModifierIds: [],
                productId: 'IDPSalesItem-4865',
                lineItemId: 2,
            },
        ]);
    });

    it('should return correct value for unavailable modifiers', () => {
        const mockUnavailableItems = ['IDPModifier-4103', 'IDPModifier-23919', 'IDPModifier-23923'];

        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([
            {
                isDefaultModifiersUnavailable: false,
                isWholeProductUnavailable: false,
                modifierIds: ['IDPModifier-4103'],
                subModifierIds: [],
                productId: 'IDPSalesItem-5240',
                lineItemId: 1,
            },
            {
                isDefaultModifiersUnavailable: false,
                isWholeProductUnavailable: false,
                modifierIds: ['IDPModifier-23919', 'IDPModifier-23923'],
                subModifierIds: [],
                productId: 'IDPSalesItem-4865',
                lineItemId: 2,
            },
        ]);
    });

    it('should return correct value for unavailable default modifiers', () => {
        const mockUnavailableItems = ['IDPModifier-17157-RG', 'IDPModifier-21849'];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([
            {
                isDefaultModifiersUnavailable: true,
                isWholeProductUnavailable: false,
                modifierIds: ['IDPModifier-17157-RG'],
                subModifierIds: [],
                productId: 'IDPSalesItem-5240',
                lineItemId: 1,
            },
            {
                isDefaultModifiersUnavailable: true,
                isWholeProductUnavailable: false,
                modifierIds: ['IDPModifier-21849'],
                subModifierIds: [],
                productId: 'IDPSalesItem-4865',
                lineItemId: 2,
            },
        ]);
    });

    it('should return correct value for unavailable default sub modifiers', () => {
        const mockUnavailableItems = ['IDPModifier-17157-RGA'];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([
            {
                isDefaultModifiersUnavailable: true,
                isWholeProductUnavailable: false,
                lineItemId: 2,
                modifierIds: [],
                productId: 'IDPSalesItem-4866',
                subModifierIds: ['IDPModifier-17157-RGA'],
            },
        ]);
    });

    it('should return correct value for unavailable sub modifiers', () => {
        const mockUnavailableItems = ['IDPModifier-22222'];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([
            {
                isDefaultModifiersUnavailable: false,
                isWholeProductUnavailable: false,
                modifierIds: [],
                subModifierIds: ['IDPModifier-22222'],
                productId: 'IDPSalesItem-4865',
                lineItemId: 2,
            },
        ]);
    });

    it('should return correct value for unavailable sub modifiers and modifiers', () => {
        const mockUnavailableItems = ['IDPModifier-22222', 'IDPModifier-23919'];
        const {
            result: { current },
        } = renderHook(() => useUnavailableItems(mockUnavailableItems));

        expect(current.unavailableItems).toEqual([
            {
                isDefaultModifiersUnavailable: false,
                isWholeProductUnavailable: false,
                modifierIds: ['IDPModifier-23919'],
                subModifierIds: ['IDPModifier-22222'],
                productId: 'IDPSalesItem-4865',
                lineItemId: 2,
            },
        ]);
    });
});
