import { act, renderHook } from '@testing-library/react-hooks';
import useAddCardToWallet from '../../../common/hooks/useAddCardToWallet';

jest.mock('../../../common/services/addCardToWalletService/addCardToWalletService', () => ({
    addCardToWalletService: jest.fn().mockImplementation(() => {
        throw new Error();
    }),
}));

describe('useAddCardToWallet', () => {
    it('should addCard and return the response', () => {
        const { result } = renderHook(() => useAddCardToWallet());
        expect(result.current).toMatchObject({
            error: null,
            isLoading: false,
        });
    });

    it('should populate error when service api fails', async () => {
        const { result } = renderHook(() => useAddCardToWallet());
        await act(async () => {
            await result.current.addCardToWallet({}, '');
        });
        expect(result.current).toMatchObject({
            error: new Error('Could not save card to wallet. Please, try again later.'),
            isLoading: false,
        });
    });
});
