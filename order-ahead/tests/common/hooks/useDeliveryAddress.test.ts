import { renderHook, act } from '@testing-library/react-hooks';
import { OrderLocationMethod } from '../../../redux/orderLocation';

import { useAppDispatch } from '../../../redux/store';
import deliveryMock from '../../mocks/useDeliveryAdressMock';

import { useOrderLocation, useDomainMenu } from '../../../redux/hooks';
import { getLocationById } from '../../../common/services/locationService';
import { useDeliveryAddress } from '../../../common/hooks/useDeliveryAddress';
import { waitFor } from '@testing-library/react';
import { useLocationUnavailableError } from '../../../common/hooks/useLocationUnavailableError';

jest.mock('../../../redux/store');
jest.mock('../../../common/hooks/usePickupLocationsSearch');
jest.mock('../../../common/services/locationService/getLocationById');
jest.mock('next/router');

jest.mock('../../../redux/hooks', () => ({
    useDomainMenu: jest.fn(() => ({ loading: false, actions: { getDomainMenu: jest.fn() } })),
    useOrderLocation: jest.fn(() => ({ actions: { setDeliveryLocation: jest.fn() } })),
    useTallyOrder: jest.fn().mockReturnValue({
        setUnavailableTallyItems: jest.fn(),
    }),
}));

jest.mock('../../../common/hooks/useLocationUnavailableError', () => ({
    useLocationUnavailableError: jest.fn().mockReturnValue({
        pushLocationUnavailableError: jest.fn(),
    }),
}));

describe('useDeliveryAddress', () => {
    const dispatchMock = jest.fn();
    const setLocationMock = jest.fn();
    const getDomainMenuMock = jest.fn();
    const setPreviousLocationMock = jest.fn();

    beforeEach(() => {
        (useAppDispatch as jest.Mock).mockReturnValue(dispatchMock);
        (useOrderLocation as jest.Mock).mockReturnValue({
            actions: { setDeliveryLocation: setLocationMock, setPreviousLocation: setPreviousLocationMock },
            method: OrderLocationMethod.DELIVERY,
            pickupAddress: null,
            deliveryAddress: null,
        });
        (useDomainMenu as jest.Mock).mockReturnValue({ actions: { getDomainMenu: getDomainMenuMock } });

        (getLocationById as jest.Mock).mockReturnValueOnce({
            id: 1,
            storeId: 1,
            isDigitallyEnabled: false,
            isClosed: true,
            contactDetails: {
                phone: '777-77-7777',
            },
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should call pushLocationUnavailableError when isDigitlEnabled false', async () => {
        const pushLocationUnavailableErrorMock = jest.fn();
        (useLocationUnavailableError as jest.Mock).mockReturnValueOnce({
            pushLocationUnavailableError: pushLocationUnavailableErrorMock,
        });
        const locationMockData = {
            id: 1,
            isDigitallyEnabled: false,
            contactDetails: {
                phone: '777-77-7777',
            },
            isClosed: true,
            storeId: 1,
        };
        (getLocationById as jest.Mock).mockReturnValue(locationMockData);

        const { result } = renderHook(() => useDeliveryAddress());
        act(() => {
            result.current.setDeliveryAddress(deliveryMock);
        });
        await waitFor(() => expect(pushLocationUnavailableErrorMock).toHaveBeenCalledTimes(1));
        await waitFor(() => expect(pushLocationUnavailableErrorMock).toHaveBeenCalledWith(locationMockData));
        await waitFor(() => expect(setPreviousLocationMock).toHaveBeenCalled());
    });
});
