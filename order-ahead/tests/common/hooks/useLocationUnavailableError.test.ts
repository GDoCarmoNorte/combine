import { renderHook, act } from '@testing-library/react-hooks';
import { useNotifications } from '../../../redux/hooks';

import { useLocationUnavailableError } from '../../../common/hooks/useLocationUnavailableError';
import { waitFor } from '@testing-library/react';
import { GtmErrorCategory } from '../../../common/services/gtmService/types';
import { useGtmErrorEvent } from '../../../common/hooks/useGtmErrorEvent';

jest.mock('../../../redux/hooks', () => ({
    useNotifications: jest.fn(() => ({ actions: { enqueueError: jest.fn() } })),
}));

jest.mock('../../../common/hooks/useGtmErrorEvent', () => ({
    useGtmErrorEvent: jest.fn().mockReturnValue({
        pushGtmErrorEvent: jest.fn(),
    }),
}));

describe('useLocationUnavailableError', () => {
    it('should send Gtm error event and show error message', async () => {
        const pushGtmErrorEventMock = jest.fn();
        (useGtmErrorEvent as jest.Mock).mockReturnValueOnce({
            pushGtmErrorEvent: pushGtmErrorEventMock,
        });
        const enqueueErrorMock = jest.fn();
        (useNotifications as jest.Mock).mockReturnValueOnce({
            actions: {
                enqueueError: enqueueErrorMock,
            },
        });
        const errorMessage =
            'We apologize. We are currently not taking online orders. You can call us at 777-77-7777 to place your order over the phone, or stop by and place your order directly with a team member!.';

        const payload = {
            ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_LOCATION,
            ErrorDescription: errorMessage,
        };

        const { result } = renderHook(() => useLocationUnavailableError());
        act(() => {
            result.current.pushLocationUnavailableError({
                contactDetails: {
                    address: {
                        line1: '550 W 79th St.',
                        line2: '',
                        line3: 'Crossroads Center',
                        postalCode: '55317-9530',
                        stateProvinceCode: 'MN',
                        countryCode: 'US',
                        city: 'Chanhassen',
                    },
                    phone: '777-77-7777',
                },
                additionalFeatures: {
                    maxOrderAmount: 0,
                    isPayAtStoreEnabled: false,
                },
                isOnlineOrderAvailable: false,
                isClosed: false,
                isDigitallyEnabled: false,
            });
        });
        await waitFor(() => expect(pushGtmErrorEventMock).toHaveBeenCalledWith(payload));
        await waitFor(() => expect(enqueueErrorMock).toHaveBeenCalledWith({ message: errorMessage }));
        expect(pushGtmErrorEventMock).toHaveBeenCalledTimes(1);
    });
});
