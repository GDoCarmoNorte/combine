import React from 'react';
import { mount } from 'enzyme';
import { actImmediate } from '../../components/clientOnly/formik/formikTestUtils';
import { useLocalization, IUseLocalization } from '../../../common/hooks/useLocalization';
import { useDomainProduct } from '../../../redux/hooks/domainMenu';
import { useLocationOrderAheadAvailability } from '../../../common/hooks/useLocationOrderAheadAvailability';
import { LOCATION_NOT_SELECTED_DESCRIPTION_TEXT } from '../../../components/sections/productDetailsContainer/constants';

jest.mock('../../../redux/hooks/domainMenu', () => ({
    useDomainProduct: jest.fn(), //todo
}));

jest.mock('../../../common/hooks/useLocationOrderAheadAvailability', () => ({
    useLocationOrderAheadAvailability: jest.fn(), //todo
}));

const renderUseLocalizationHook = async () => {
    let localizationHook: IUseLocalization;

    const Component = () => {
        localizationHook = useLocalization('productId');

        return null;
    };
    const wrapper = mount(<Component />);

    await actImmediate(wrapper);

    return localizationHook;
};

describe('useLocalization', () => {
    it('should return correct data, location is not selected, product is not available', async () => {
        const domainProductMock: any = { availability: { isAvailable: false } };
        const locationOrderAheadAvailability: any = { isLocationOrderAheadAvailable: false, location: null };

        (useDomainProduct as jest.Mock).mockReturnValue(domainProductMock);
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue(locationOrderAheadAvailability);

        const {
            locationLinkText,
            descriptionText,
            isProductAvailable,
            isLocationOrderAheadAvailable,
        } = await renderUseLocalizationHook();

        expect(locationLinkText).toBe('Select a location');
        expect(descriptionText).toBe(LOCATION_NOT_SELECTED_DESCRIPTION_TEXT);
        expect(isProductAvailable).toBe(false);
        expect(isLocationOrderAheadAvailable).toBe(false);
    });

    it('locationLinkText, location is selected, product is not available', async () => {
        const domainProductMock: any = { availability: { isAvailable: false } };
        const locationOrderAheadAvailability: any = { isLocationOrderAheadAvailable: false, location: {} };

        (useDomainProduct as jest.Mock).mockReturnValue(domainProductMock);
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue(locationOrderAheadAvailability);

        const { locationLinkText } = await renderUseLocalizationHook();

        expect(locationLinkText).toBe('Select a different location');
    });

    it('locationLinkText, location selected, product available, isLocationOrderAheadAvailable false', async () => {
        const domainProductMock: any = { availability: { isAvailable: true } };
        const locationOrderAheadAvailability: any = { isLocationOrderAheadAvailable: false, location: {} };

        (useDomainProduct as jest.Mock).mockReturnValue(domainProductMock);
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue(locationOrderAheadAvailability);

        const { locationLinkText } = await renderUseLocalizationHook();

        expect(locationLinkText).toBe('Select a different location');
    });

    it('locationLinkText, location is not selected, product available, isLocationOrderAheadAvailable true', async () => {
        const domainProductMock: any = { availability: { isAvailable: true } };
        const locationOrderAheadAvailability: any = { isLocationOrderAheadAvailable: true, location: null };

        (useDomainProduct as jest.Mock).mockReturnValue(domainProductMock);
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue(locationOrderAheadAvailability);

        const { descriptionText } = await renderUseLocalizationHook();

        expect(descriptionText).toBe('Find an Arby’s for pricing, availability, or to start an online order.');
    });

    it('locationLinkText, location selected, product is not available, isLocationOrderAheadAvailable true', async () => {
        const domainProductMock: any = { availability: { isAvailable: false } };
        const locationOrderAheadAvailability: any = { isLocationOrderAheadAvailable: true, location: {} };

        (useDomainProduct as jest.Mock).mockReturnValue(domainProductMock);
        (useLocationOrderAheadAvailability as jest.Mock).mockReturnValue(locationOrderAheadAvailability);

        const { descriptionText } = await renderUseLocalizationHook();

        expect(descriptionText).toBe('<b>Item not available</b> at this location.');
    });
});
