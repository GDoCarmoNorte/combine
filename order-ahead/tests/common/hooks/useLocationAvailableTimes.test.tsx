import React from 'react';
import { mount } from 'enzyme';
import useLocationAvailableTimes, {
    UseLocationAvailableTimesHook,
} from '../../../common/hooks/useLocationAvailableTimes';
import getLocationAvailableTimes from '../../../common/services/locationService/getLocationAvailableTimes';

jest.mock('react-redux');
jest.mock('../../../common/services/locationService/getLocationAvailableTimes');

const locationAvailableTimeSlotsMock = {
    pickup: {},
    delivery: {},
};

const requestMock = {
    locationId: 'testId',
};

const useLocationTimeSlotsHook = () => {
    let availableTimeSlots: UseLocationAvailableTimesHook;
    const Component = () => {
        availableTimeSlots = useLocationAvailableTimes();

        return null;
    };
    mount(<Component />);

    return availableTimeSlots;
};

describe('useLocationAvailableTimes', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('getLocationAvailableTimeSlots', () => {
        it('should call LocationAvailableTimes service', async () => {
            (getLocationAvailableTimes as jest.Mock).mockReturnValue(Promise.resolve(locationAvailableTimeSlotsMock));
            const availableTimeSlots = useLocationTimeSlotsHook();

            const actualResult = await availableTimeSlots.getLocationAvailableTimeSlots(requestMock);
            expect(actualResult).toEqual(locationAvailableTimeSlotsMock);
        });

        it('should throw an error if promise rejects', async () => {
            const error = { message: 'some error' };
            const errorResponse = () => Promise.reject(error);
            (getLocationAvailableTimes as jest.Mock).mockReturnValue(errorResponse);
            const availableTimeSlots = useLocationTimeSlotsHook();
            await expect(await availableTimeSlots.getLocationAvailableTimeSlots(requestMock)).rejects.toEqual(error);
        });
    });
});
