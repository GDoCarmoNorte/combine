import { renderHook } from '@testing-library/react-hooks';

import useStartAnOrderLink from '../../../common/hooks/useStartAnOrderLink';
import { useOrderLocation } from '../../../redux/hooks';
import { OrderLocationMethod } from '../../../redux/orderLocation';

jest.mock('../../../redux/hooks');

describe('useStartAnOrderLink hook', () => {
    it('should return /locations if location is not selected', () => {
        (useOrderLocation as jest.Mock).mockReturnValueOnce({ method: OrderLocationMethod.NOT_SELECTED });
        const {
            result: { current },
        } = renderHook(() => useStartAnOrderLink());

        expect(current).toBe('/locations');
    });

    it('should return /menu if location is selected', () => {
        (useOrderLocation as jest.Mock).mockReturnValueOnce({ method: OrderLocationMethod.DELIVERY });
        const {
            result: { current },
        } = renderHook(() => useStartAnOrderLink());

        expect(current).toBe('/menu');
    });
});
