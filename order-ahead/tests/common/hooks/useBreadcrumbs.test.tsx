import { useBreadcrumbs } from '../../../common/hooks/useBreadcrumbs';
import { useRouter } from 'next/router';

jest.mock('next/router', () => ({
    useRouter: jest.fn(),
}));

describe('useBreadcrumbs hook', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('should return correct paths if they are taken from route', () => {
        const expectedResult = [
            { as: '/path', href: '/path', title: 'path' },
            { as: '/path/path-name2', href: '/path/[path-name2]', title: 'path name2' },
            { as: '/path/path-name2/path-name3', href: '/path/[path-name2]/[path-name3]', title: 'path name3' },
        ];

        (useRouter as jest.Mock).mockReturnValue({
            asPath: 'path/path-name2/path-name3/',
            pathname: 'path/[path-name2]/[path-name3]',
        });

        const result = useBreadcrumbs();

        expect(result).toStrictEqual(expectedResult);
    });
});
