import { useSafeSetTimeout } from '../../../common/hooks/useSafeSetTimeout';
import { renderHook, cleanup } from '@testing-library/react-hooks';

jest.useFakeTimers('legacy');

describe('useSafeSetTimeout hook', () => {
    it('should return setTimeout-like function and accept callback and timeout value', () => {
        const {
            result: { current },
        } = renderHook(() => useSafeSetTimeout());

        const safeSetTimeout = current;
        const callback = jest.fn();
        const timeout = 1000;

        safeSetTimeout(callback, timeout);

        expect(setTimeout).toBeCalledWith(callback, timeout);
        jest.advanceTimersByTime(timeout);
        expect(callback).toBeCalledTimes(1);
    });

    it('should clear timeout on component unmount', () => {
        const {
            result: { current },
        } = renderHook(() => useSafeSetTimeout());

        const safeSetTimeout = current;
        const callback = jest.fn();
        const timeout = 1000;
        const timeoutId = safeSetTimeout(callback, timeout);

        expect(setTimeout).toBeCalledWith(callback, timeout);
        jest.advanceTimersByTime(timeout / 2);
        cleanup();
        expect(callback).toBeCalledTimes(0);
        expect(clearTimeout).toBeCalledWith(timeoutId);
    });
});
