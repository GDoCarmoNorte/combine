import { useInitApp } from '../../../common/hooks/useInitApp';
import { renderHook } from '@testing-library/react-hooks';

jest.mock('../../../redux/store', () => ({
    useAppDispatch: jest.fn(),
}));

describe('useInitApp hook', () => {
    it('should fire GA event when location is persisted in store', async () => {
        const state = {
            orderLocation: {
                method: 'PICKUP',
                pickupAddress: {
                    storeId: '1',
                    displayName: 'displayName',
                },
            },
        };
        const dispatchMock = jest.fn();
        const getStateMock = jest.fn().mockReturnValue(state);
        (window.sessionStorage.getItem as jest.Mock).mockReturnValue(false);

        const store: any = {
            getState: getStateMock,
            dispatch: dispatchMock,
        };

        renderHook(() => useInitApp(store));

        expect(dispatchMock).toHaveBeenCalled();
    });
    it('should not fire GA event when location is not persisted in store', async () => {
        const state = {
            location: null,
        };
        const dispatchMock = jest.fn();
        const getStateMock = jest.fn().mockReturnValue(state);
        (window.sessionStorage.getItem as jest.Mock).mockReturnValue(false);

        const store: any = {
            getState: getStateMock,
            dispatch: dispatchMock,
        };

        renderHook(() => useInitApp(store));

        expect(dispatchMock).not.toHaveBeenCalled();
    });
    it('should not fire GA event when location persisted in store but event was fired in current session', async () => {
        const state = {
            orderLocation: {
                method: 'PICKUP',
                pickupAddress: {
                    storeId: '1',
                    displayName: 'displayName',
                },
            },
        };
        const dispatchMock = jest.fn();
        const getStateMock = jest.fn().mockReturnValue(state);
        (window.sessionStorage.getItem as jest.Mock).mockReturnValue(true);

        const store: any = {
            getState: getStateMock,
            dispatch: dispatchMock,
        };

        renderHook(() => useInitApp(store));

        expect(dispatchMock).not.toHaveBeenCalled();
    });
    // TODO should be updated with GA update @ga-31325
    it('should not fire GA event when delivery method is selected', async () => {
        const state = {
            orderLocation: {
                method: 'DELIVERY',
            },
        };
        const dispatchMock = jest.fn();
        const getStateMock = jest.fn().mockReturnValue(state);
        (window.sessionStorage.getItem as jest.Mock).mockReturnValue(false);

        const store: any = {
            getState: getStateMock,
            dispatch: dispatchMock,
        };

        renderHook(() => useInitApp(store));

        expect(dispatchMock).not.toHaveBeenCalled();
    });
});
