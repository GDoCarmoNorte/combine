import createErrorWrapper from '../../../common/services/createErrorWrapper';
import { act, renderHook } from '@testing-library/react-hooks';
import useBwwCardBalance from '../../../common/hooks/useBwwCardBalance';
import { IGiftCardBWWBalanceRequestModel } from '../../../@generated/webExpApi';

jest.mock('../../../common/services/createErrorWrapper');
jest.mock('../../../@generated/webExpApi/apis/index');

describe('useBwwCardBalance', () => {
    const mockRequest = {
        locationId: '1',
        sessionKey: 'abc123',
        paymentKey: 'abc123',
        fulfillmentType: 'PICKUP',
    } as IGiftCardBWWBalanceRequestModel;

    it('should get gift card balance successfully', async () => {
        const mockedResult = { balance: 12.5 };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result } = renderHook(() => useBwwCardBalance());
        await act(async () => {
            await result.current.getBalance(mockRequest);
        });

        expect(result.current.balance).toEqual(12.5);
        expect(result.current.loading).toBeFalsy();
        expect(result.current.errorMessage).toBeNull();
    });

    it('should get gift card balance even if result is 0', async () => {
        const mockedResult = { balance: 0 };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result } = renderHook(() => useBwwCardBalance());
        await act(async () => {
            await result.current.getBalance(mockRequest);
        });

        expect(result.current.balance).toEqual(0);
        expect(result.current.loading).toBeFalsy();
        expect(result.current.errorMessage).toBeNull();
    });

    it('should set error message when balance is undefined', async () => {
        const mockedResult = { balance: undefined, message: 'error message', code: 'GENERIC' };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result } = renderHook(() => useBwwCardBalance());
        await act(async () => {
            await result.current.getBalance(mockRequest);
        });

        expect(result.current.balance).toBeNull();
        expect(result.current.loading).toBeFalsy();
        expect(result.current.errorMessage).toEqual('error message');
        expect(result.current.code).toEqual('GENERIC');
    });

    it('should set exp-api error message when balance is not a number', async () => {
        const mockedResult = { balance: 'balance', message: 'error message', code: 'GENERIC' };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result } = renderHook(() => useBwwCardBalance());
        await act(async () => {
            await result.current.getBalance(mockRequest);
        });

        expect(result.current.balance).toBeNull();
        expect(result.current.loading).toBeFalsy();
        expect(result.current.errorMessage).toEqual('error message');
        expect(result.current.code).toEqual('GENERIC');
    });

    it('should set specific error message when error occurs', async () => {
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.reject(new Error('Error')));

        const { result } = renderHook(() => useBwwCardBalance());
        await act(async () => {
            await result.current.getBalance(mockRequest);
        });

        expect(result.current.balance).toBeNull();
        expect(result.current.loading).toBeFalsy();
        expect(result.current.errorMessage).toEqual('Card Information Not Available');
    });
});
