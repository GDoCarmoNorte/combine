import { useCheckin } from '../../../common/hooks/useCheckin';
import { act, renderHook } from '@testing-library/react-hooks';
import { useRewardsService } from '../../../common/hooks/useRewardsService';
import { LOCATION_SERVICES_DISABLED_ERROR_CODE } from '../../../common/helpers/getCoordinates';
import { useAppDispatch } from '../../../redux/store';
import { GTM_CHECK_IN } from '../../../common/services/gtmService/constants';

jest.mock('../../../common/hooks/useRewardsService', () => ({
    useRewardsService: jest.fn(),
}));

jest.mock('../../../redux/store', () => ({
    useAppDispatch: jest.fn(),
}));

describe('useCheckin', () => {
    const mockDispatch = jest.fn();

    beforeAll(() => {
        jest.useFakeTimers();
        (useAppDispatch as jest.Mock).mockReturnValue(mockDispatch);
    });

    afterAll(() => {
        mockDispatch.mockReset();
    });

    it('Should work correctly', async () => {
        jest.spyOn(global.navigator.geolocation, 'getCurrentPosition').mockImplementation((cb) => {
            cb({ coords: { latitude: 0, longitude: 0 } } as any);
        });
        const checkin = jest.fn().mockReturnValue({});
        (useRewardsService as jest.Mock).mockReturnValue({ checkin });

        const { result: hook } = renderHook(() => useCheckin());

        await act(async () => {
            await hook.current.checkin();
        });

        expect(checkin).toBeCalledTimes(1);
        expect(hook.current.loading).toEqual(false);
        expect(hook.current.payload).toEqual({});
        expect(hook.current.error).toEqual(null);
    });

    it('Should handle location services disabled', async () => {
        jest.spyOn(global.navigator.geolocation, 'getCurrentPosition').mockImplementation((cb, err) => {
            err({ code: LOCATION_SERVICES_DISABLED_ERROR_CODE } as any);
        });

        const { result: hook } = renderHook(() => useCheckin());

        await act(async () => {
            await hook.current.checkin();
        });

        expect(hook.current.loading).toEqual(false);
        expect(hook.current.payload).toEqual(null);
        expect(hook.current.error).toEqual({ code: LOCATION_SERVICES_DISABLED_ERROR_CODE });
    });

    it('Should handle api errors', async () => {
        jest.spyOn(global.navigator.geolocation, 'getCurrentPosition').mockImplementation((cb) => {
            cb({ coords: { latitude: 0, longitude: 0 } } as any);
        });
        const checkin = jest.fn().mockImplementation(() => {
            throw {};
        });
        (useRewardsService as jest.Mock).mockReturnValue({ checkin });

        const { result: hook } = renderHook(() => useCheckin());

        await act(async () => {
            await hook.current.checkin();
        });

        expect(checkin).toBeCalledTimes(1);
        expect(hook.current.loading).toEqual(false);
        expect(hook.current.payload).toEqual(null);
        expect(hook.current.error).toEqual({});
    });

    it('should call gtmService with location id on check in click', async () => {
        const response = { message: '', storeId: 123 };
        jest.spyOn(global.navigator.geolocation, 'getCurrentPosition').mockImplementation((cb) => {
            cb({ coords: { latitude: 0, longitude: 0 } } as any);
        });
        const checkin = jest.fn().mockReturnValue(response);
        (useRewardsService as jest.Mock).mockReturnValue({ checkin });

        const { result: hook } = renderHook(() => useCheckin());

        await act(async () => {
            await hook.current.checkin();
        });

        expect(checkin).toBeCalledTimes(1);
        expect(hook.current.payload).toEqual(response);
        expect(mockDispatch).toHaveBeenCalledWith({
            type: GTM_CHECK_IN,
            payload: 123,
        });
    });

    it('should call gtmService without location id if service fails or no location id was provided', async () => {
        jest.spyOn(global.navigator.geolocation, 'getCurrentPosition').mockImplementation((cb) => {
            cb({ coords: { latitude: 0, longitude: 0 } } as any);
        });
        const checkin = jest.fn().mockImplementation(() => {
            throw {};
        });
        (useRewardsService as jest.Mock).mockReturnValue({ checkin });

        const { result: hook } = renderHook(() => useCheckin());

        await act(async () => {
            await hook.current.checkin();
        });

        expect(mockDispatch).toHaveBeenCalledWith({
            type: GTM_CHECK_IN,
            payload: undefined,
        });
    });
});
