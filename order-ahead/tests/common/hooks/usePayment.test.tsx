import { renderHook, act } from '@testing-library/react-hooks';
import { TInitialPaymentTypes } from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { usePayment } from '../../../common/hooks/usePayment';
import { useOrderLocation } from '../../../redux/hooks';
import getBrandInfo from '../../../lib/brandInfo';
import createErrorWrapper from '../../../common/services/createErrorWrapper';

jest.mock('react-redux');
jest.mock('../../../redux/hooks/useOrderLocation');
jest.mock('../../../lib/brandInfo');
jest.mock('../../../common/services/createErrorWrapper');

const locationIdMock = '13';
const sessionKey = 'test session key';
const iframeContent = document.createElement('div');
const mockedResult = { sessionKey: sessionKey, iframeHtml: iframeContent };
const createErrorWrapperMock = jest.fn().mockReturnValue(() => Promise.resolve(mockedResult));

describe('usePayment', () => {
    beforeEach(() => {
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: 'Arbys' });
    });
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should try to init CC payment method on hook init for CREDIT_OR_DEBIT payment type', async () => {
        (useOrderLocation as jest.Mock).mockReturnValue({ currentLocation: { id: locationIdMock } });
        (createErrorWrapper as jest.Mock).mockImplementation((value) => createErrorWrapperMock(value));
        const { result, waitForNextUpdate } = renderHook(() => usePayment(TInitialPaymentTypes.CREDIT_OR_DEBIT));

        expect(result.current.isLoading).toBeTruthy();

        await waitForNextUpdate();

        expect(createErrorWrapperMock).toHaveBeenCalledTimes(1);
        expect(createErrorWrapperMock).toHaveBeenCalledWith('initIframeCC');
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.payment.sessionKey).toEqual(sessionKey);
        expect(result.current.payment.iframeHtml).toEqual(iframeContent);
        expect(result.current.error).toBeNull();
    });

    it('should set error if iframe init failed', async () => {
        const createErrorWrapperRejectMock = jest.fn().mockReturnValue(() => Promise.reject('error'));
        (createErrorWrapper as jest.Mock).mockImplementation((value) => createErrorWrapperRejectMock(value));
        const { result, waitForNextUpdate } = renderHook(() => usePayment(TInitialPaymentTypes.CREDIT_OR_DEBIT));

        expect(result.current.isLoading).toBeTruthy();

        await waitForNextUpdate();

        expect(createErrorWrapperRejectMock).toHaveBeenCalled();
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.payment).toBeNull();
        expect(result.current.error).toEqual(new Error('Payment service is not available. Please, try again later.'));
    });

    it('should try to init Wallet payment method on hook init for CARD_ON_FILE payment type', async () => {
        (createErrorWrapper as jest.Mock).mockImplementation((value) => createErrorWrapperMock(value));
        const { result, waitForNextUpdate } = renderHook(() => usePayment('CARD_ON_FILE'));

        expect(result.current.isLoading).toBeTruthy();

        await waitForNextUpdate();

        expect(createErrorWrapperMock).toHaveBeenCalledTimes(1);
        expect(createErrorWrapperMock).toHaveBeenCalledWith('initIframeCC');
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.payment.sessionKey).toEqual(sessionKey);
        expect(result.current.payment.iframeHtml).toEqual(iframeContent);
        expect(result.current.error).toBeNull();
    });

    it('should try to init GC payment method on hook init for empty payment type', async () => {
        (createErrorWrapper as jest.Mock).mockImplementation((value) => createErrorWrapperMock(value));
        const { result, waitForNextUpdate } = renderHook(() => usePayment(TInitialPaymentTypes.PLACEHOLDER));

        expect(result.current.isLoading).toBeTruthy();

        await waitForNextUpdate();

        expect(createErrorWrapperMock).toHaveBeenCalledTimes(1);
        expect(createErrorWrapperMock).toHaveBeenCalledWith('initIframeGC');
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.payment.sessionKey).toEqual(sessionKey);
        expect(result.current.payment.iframeHtml).toEqual(iframeContent);
        expect(result.current.error).toBeNull();
    });

    it('should re-init iframe on reset called', async () => {
        (createErrorWrapper as jest.Mock).mockImplementation((value) => createErrorWrapperMock(value));
        const { result, waitForNextUpdate } = renderHook(() => usePayment(TInitialPaymentTypes.CREDIT_OR_DEBIT));

        expect(result.current.isLoading).toBeTruthy();

        await waitForNextUpdate();

        await act(() => {
            result.current.reset();
        });

        expect(createErrorWrapperMock).toHaveBeenCalled();
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.payment.sessionKey).toEqual(sessionKey);
        expect(result.current.payment.iframeHtml).toEqual(iframeContent);
        expect(result.current.error).toBeNull();
    });
});
