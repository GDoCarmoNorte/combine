import { renderHook } from '@testing-library/react-hooks';
import { IOrderModel } from '../../../@generated/webExpApi/models';
import { useOrderItem } from '../../../common/hooks/useOrderItem';
import { useDomainMenuSelectors, useDomainProducts } from '../../../redux/hooks/domainMenu';
import { domainProductsMock, orderHistoryItemMock, transformedOrderItemMock } from '../../mocks/orderHistoryItem.mocks';

jest.mock('../../../redux/hooks/domainMenu');

describe('useOrderItem', () => {
    it('should return correct value', () => {
        (useDomainProducts as jest.Mock).mockReturnValue(domainProductsMock);
        (useDomainMenuSelectors as jest.Mock).mockReturnValue({ selectProductSize: () => 'size' });

        const { result } = renderHook(() => useOrderItem(orderHistoryItemMock as IOrderModel));

        expect(result.current).toEqual(transformedOrderItemMock);
    });
});
