import { useConfiguration } from '../../../common/hooks/useConfiguration';
import configurationMock from '../../mocks/configuration.mock';

jest.mock('../../../redux/hooks/useConfiguration');

jest.mock('../../../redux/hooks/useFeatureFlags', () => ({
    useFeatureFlags: jest.fn().mockReturnValue({
        featureFlags: {
            FFFromFile: true,
        },
    }),
}));

describe('useConfiguration hook', () => {
    it('should return configuration and actions', () => {
        const { configuration, actions } = useConfiguration();
        expect(configuration).toEqual(configurationMock);
        expect(typeof actions).toBe('object');
    });
});
