import createErrorWrapper from '../../../common/services/createErrorWrapper';
import { act, renderHook } from '@testing-library/react-hooks';
import { useClaimPoints } from '../../../common/hooks/useClaimPoints';
import { IClaimMissingPointsFromClientModel } from '../../../@generated/webExpApi';
import { useRewardsActivityHistory } from '../../../common/hooks/useRewardsActivityHistory';

jest.mock('../../../common/services/createErrorWrapper');
jest.mock('../../../@generated/webExpApi/apis/index');
jest.mock('../../../common/hooks/useRewardsActivityHistory', () => ({
    useRewardsActivityHistory: jest.fn().mockReturnValue({
        getRewardsActivityHistory: jest.fn(),
    }),
}));

describe('useClaimPoints', () => {
    const mockOptions = {
        locationId: '1300',
        date: '12/21/2021',
        checkNumber: '12345678',
        subTotal: 10,
    } as IClaimMissingPointsFromClientModel;

    const mockResetForm = jest.fn();
    const mockUpdateRewardsActivity = jest.fn();

    it('should return handled error message', async () => {
        const mockedResult = { code: 'GENERIC', message: 'Generic error message', type: 'EXTERNAL' };
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result } = renderHook(() => useClaimPoints());

        await act(async () => {
            await result.current.onClaimPoints(mockOptions, mockResetForm);
        });

        expect(result.current.errorMessage).toEqual('Generic error message');
        expect(result.current.errorCode).toEqual('GENERIC');
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.hasError).toBeTruthy();
        expect(mockResetForm).not.toHaveBeenCalled();
    });

    it('should set unhandled error', async () => {
        const mockedResult = {};
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.reject(mockedResult));

        const { result } = renderHook(() => useClaimPoints());

        await act(async () => {
            await result.current.onClaimPoints(mockOptions, mockResetForm);
        });

        expect(result.current.errorMessage).toEqual('');
        expect(result.current.errorCode).toEqual(null);
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.hasError).toBeTruthy();
        expect(mockResetForm).not.toHaveBeenCalled();
    });

    it('should be successful and reset form', async () => {
        const mockedResult = {};
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));

        const { result } = renderHook(() => useClaimPoints());

        await act(async () => {
            await result.current.onClaimPoints(mockOptions, mockResetForm);
        });

        expect(result.current.errorMessage).toEqual('');
        expect(result.current.errorCode).toEqual(null);
        expect(result.current.isLoading).toBeFalsy();
        expect(result.current.hasError).toBeFalsy();
        expect(mockResetForm).toHaveBeenCalled();
    });

    it('should be call updateRewardsActivity when successful response from service', async () => {
        const mockedResult = {};
        (useRewardsActivityHistory as jest.Mock).mockReturnValue({
            getRewardsActivityHistory: mockUpdateRewardsActivity,
        });
        (createErrorWrapper as jest.Mock).mockReturnValue(() => Promise.resolve(mockedResult));
        const { result } = renderHook(() => useClaimPoints());

        await act(async () => {
            await result.current.onClaimPoints(mockOptions, mockResetForm);
        });

        expect(mockUpdateRewardsActivity).toHaveBeenCalled();
    });
});
