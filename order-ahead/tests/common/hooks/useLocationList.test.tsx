import React from 'react';
import { mount } from 'enzyme';
import useLocationList, { UseLocationListHook } from '../../../common/hooks/useLocationList';
import searchLocations from '../../../common/services/locationService/searchLocations';
import getLocationUrlList from '../../../common/services/locationService/getLocationUrlList';

jest.mock('react-redux');
jest.mock('../../../common/services/locationService/searchLocations');
jest.mock('../../../common/services/locationService/getLocationUrlList');

const locationListMock = {
    totalCount: 0,
    locationsByCountry: [],
};

const locationUrlsListMock = {
    urls: [],
};

const useLocationListHook = () => {
    let locationList: UseLocationListHook;
    const Component = () => {
        locationList = useLocationList();

        return null;
    };
    mount(<Component />);

    return locationList;
};

describe('useLocationList', () => {
    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('getLocationList', () => {
        it('should call locationsListSearch service', async () => {
            (searchLocations as jest.Mock).mockReturnValue(Promise.resolve(locationListMock));
            const locationList = useLocationListHook();

            const actualResult = await locationList.getLocationList();
            expect(actualResult).toEqual(locationListMock);
        });

        it('should throw an error if promise rejects', async () => {
            const error = { message: 'some error' };
            const errorResponse = () => Promise.reject(error);
            (searchLocations as jest.Mock).mockReturnValue(errorResponse);
            const locationList = useLocationListHook();
            await expect(await locationList.getLocationList()).rejects.toEqual(error);
        });
    });
    describe('getLocationUrlsList', () => {
        it('should call getLocationUrlList service', async () => {
            (getLocationUrlList as jest.Mock).mockReturnValue(Promise.resolve(locationUrlsListMock));
            const locationList = useLocationListHook();

            const actualResult = await locationList.getLocationUrlsList();
            expect(actualResult).toEqual(locationUrlsListMock);
        });

        it('should throw an error if promise rejects', async () => {
            const error = { message: 'some error' };
            const errorResponse = () => Promise.reject(error);
            (getLocationUrlList as jest.Mock).mockReturnValue(errorResponse);
            const locationList = useLocationListHook();
            await expect(await locationList.getLocationUrlsList()).rejects.toEqual(error);
        });
    });
});
