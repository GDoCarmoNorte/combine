import GtmService from '../../../../common/services/gtmService/gtmService';
import {
    GtmErrorCategory,
    GtmEventNames,
    GtmModifierCustomizationCategory,
    GtmActionFields,
    GtmCategory,
    AccountDeletedFeedbackData,
    AccountDeletedSubmittedData,
} from '../../../../common/services/gtmService/types';
import { MAX_PAYLOAD_SIZE } from '../../../../common/services/gtmService/constants';
import { selectProductSize } from '../../../../redux/selectors/domainMenu';
import { TInitialPaymentTypes } from '../../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

jest.mock('../../../../redux/selectors/domainMenu');

describe('GtmService', () => {
    const push = jest.fn();
    const gtmData = { data: 'data' } as any;
    const signInGtmData = { signInType: 'email' };

    beforeEach(() => {
        (window as any).dataLayer = { push };
        push.mockReset();
    });

    afterEach(() => {
        (window as any).dataLayer = undefined;
        push.mockReset();
    });

    describe('pushPageEvent', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushPageEvent(gtmData);

            expect(push).toBeCalledWith({
                event: GtmEventNames.PageEvent,
                ...gtmData,
            });
        });
    });

    describe('pushSignInSuccessEvent', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushSignInSuccessEvent(signInGtmData);
            expect(push).toBeCalledWith({
                event: GtmEventNames.SignInSuccess,
                ...signInGtmData,
            });
        });
    });

    describe('pushSignInFailureEvent', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushSignInFailureEvent(signInGtmData);
            expect(push).toBeCalledWith({
                event: GtmEventNames.SignInFailure,
                ...signInGtmData,
            });
        });
    });

    describe('pushApiEvent', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushApiEvent(gtmData);

            expect(push).toBeCalledWith({
                event: GtmEventNames.ApiEvent,
                ...gtmData,
            });
        });
    });

    describe('push', () => {
        it('should log warning when payload is too big', () => {
            const payload = [];
            payload.length = MAX_PAYLOAD_SIZE + 1;
            jest.spyOn(JSON, 'stringify').mockReturnValueOnce((payload as unknown) as string);
            const spy = jest.spyOn(console, 'warn').mockImplementation();
            GtmService.pushPageEvent(gtmData);

            expect(console.warn).toHaveBeenCalledWith({
                error: expect.objectContaining({
                    message: `GTM max payload size exceed for event ${GtmEventNames.PageEvent}`,
                }),
            });
            spy.mockRestore();
        });
    });

    describe('pushTransactionInfoEvent', () => {
        it('should push data in dataLayer', () => {
            const gtmData = {
                order: {
                    fulfillment: {
                        time: '2021-01-14T15:00:00Z',
                        fulfillmentType: 'pickup',
                        storeLocation: {
                            locationId: '1',
                        },
                    },
                },
                tallyOrder: {
                    fulfillment: {
                        type: 'pickup',
                    },
                },
                request: {
                    payments: [
                        {
                            details: {
                                cardIssuer: 'Visa',
                            },
                        },
                    ],
                    orderData: {
                        isAsap: true,
                        locationId: '1',
                    },
                },
                location: {
                    timezone: 'America/New_York',
                },
                loginType: 'guest',
            };

            GtmService.pushTransactionInfoEvent(gtmData as any);

            expect(push).toBeCalledWith({
                event: GtmEventNames.TransactionInfo,
                fulfillmentDate: '01/14/2021',
                fulfillmentTime: '10:00',
                fulfillmentType: gtmData.tallyOrder.fulfillment.type,
                isAsap: gtmData.request.orderData.isAsap,
                storeNumber: gtmData.request.orderData.locationId,
                paymentMethod: gtmData.request.payments[0].details.cardIssuer,
                loginType: 'guest',
            });
        });
    });

    describe('pushModifierCustomizationEvent', () => {
        it('should push data in dataLayer', () => {
            const gtmData = {
                category: GtmModifierCustomizationCategory.addition,
                productName: '1',
                modifierName: '1',
            };

            GtmService.pushModifierCustomizationEvent(gtmData);

            expect(push).toBeCalledWith({
                event: GtmEventNames.ModifierCustomization,
                category: gtmData.category,
                label: gtmData.productName,
                action: gtmData.modifierName,
            });
        });
    });

    describe('pushGiftCardBalanceFormSubmitSuccess', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushGiftCardBalanceFormSubmitSuccess();

            expect(push).toBeCalledWith({
                event: GtmEventNames.GiftCardBalanceFormSubmit,
            });
        });
    });

    describe('pushComboSelectionEvent', () => {
        it('should push data in dataLayer', () => {
            const gtmData = {
                comboDrink: 'Minute Maid® Zero Sugar',
                comboName: "Classic Beef 'n Cheddar Meal",
                comboPrice: 9.48,
                comboSide: 'Curly Fries',
                comboSize: 'Medium',
            };

            GtmService.pushComboSelectionEvent(gtmData as any);

            expect(push).toBeCalledWith({
                event: GtmEventNames.ComboSelection,
                ...gtmData,
            });
        });
        it('should push data in dataLayer when there is no side product', () => {
            const gtmData = {
                comboDrink: 'Minute Maid® Zero Sugar',
                comboName: "Classic Beef 'n Cheddar Meal",
                comboPrice: 9.48,
                comboSide: undefined,
                comboSize: 'Medium',
            };

            GtmService.pushComboSelectionEvent(gtmData as any);

            expect(push).toBeCalledWith({
                event: GtmEventNames.ComboSelection,
                ...gtmData,
            });
        });
    });

    describe('pushAddToCartEvent', () => {
        ((selectProductSize as unknown) as jest.Mock).mockReturnValue('Medium');

        it('should push data in dataLayer with child items', () => {
            const beforeGtmData = {
                bagEntry: {
                    childItems: [
                        {
                            childItems: undefined,
                            lineItemId: 1,
                            name: "Classic Beef 'n Cheddar",
                            price: 3.41,
                            productId: 'arb-itm-000-017',
                            quantity: 1,
                        },
                        {
                            childItems: undefined,
                            lineItemId: 1,
                            name: 'Curly Fries',
                            price: 1.99,
                            productId: 'arb-itm-002-002',
                            quantity: 1,
                        },
                        {
                            childItems: undefined,
                            lineItemId: 1,
                            name: 'Coca-Cola®',
                            price: 1.89,
                            productId: 'arb-itm-003-001',
                            quantity: 1,
                        },
                    ],
                    lineItemId: 1,
                    name: "Classic Beef 'n Cheddar Meal",
                    price: 7.29,
                    productId: 'arb-itm-000-021',
                    quantity: 1,
                },
                category: 'meals',
                name: "Classic Beef 'n Cheddar Meal",
            };
            const gtmData = {
                ecommerce: {
                    add: {
                        products: [
                            {
                                category: 'Meals',
                                id: 'arb-itm-000-021',
                                onceRemoved: false,
                                name: "Classic Beef 'n Cheddar Meal",
                                price: 7.29,
                                quantity: 1,
                                sideDrink: 'Coca-Cola®',
                                sideFood: 'Curly Fries',
                            },
                        ],
                    },
                    currencyCode: 'USD',
                },
                comboName: "Classic Beef 'n Cheddar Meal",
                comboSize: 'Medium',
                comboSide: 'Curly Fries',
                comboDrink: 'Coca-Cola®',
                comboPrice: 7.29,
                event: 'AddToCart',
            };

            GtmService.pushAddToCardEvent(beforeGtmData as any, {} as any);

            expect(push).toBeCalledWith({
                event: GtmEventNames.AddToCart,
                ...gtmData,
            });
        });
        it('should push data in dataLayer without child items', () => {
            const beforeGtmData = {
                bagEntry: {
                    childItems: undefined,
                    lineItemId: 2,
                    name: 'Chicken Tenders 5PC',
                    price: 5.49,
                    productId: 'arb-itm-000-106',
                    quantity: 1,
                    reAddCart: false,
                },
                category: 'chicken',
                name: 'Chicken Tenders 5PC',
            };
            const gtmData = {
                ecommerce: {
                    add: {
                        products: [
                            {
                                category: 'Chicken',
                                id: 'arb-itm-000-106',
                                onceRemoved: false,
                                name: 'Chicken Tenders 5PC',
                                price: 5.49,
                                quantity: 1,
                                sideDrink: 'none',
                                sideFood: 'none',
                            },
                        ],
                    },
                    currencyCode: 'USD',
                },
                event: 'AddToCart',
            };

            GtmService.pushAddToCardEvent(beforeGtmData as any, {} as any);

            expect(push).toBeCalledWith({
                event: GtmEventNames.AddToCart,
                ...gtmData,
            });
        });
    });

    describe('pushCheckoutPaymentMethodType', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushCheckoutPaymentMethodType(TInitialPaymentTypes.CREDIT_OR_DEBIT);
            expect(push).toBeCalledWith({
                event: 'Checkout',
                action: 'Click',
                label: 'CREDIT_OR_DEBIT',
            });
        });
    });

    describe('pushError', () => {
        it('should push error for item unavailable', () => {
            const payload = {
                ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_ITEMS,
                ErrorDescription: 'A modification or side in your bag is no longer available.',
            };
            GtmService.pushError(payload);
            expect(push).toBeCalledWith({
                event: GtmEventNames.Error,
                error: {
                    ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_ITEMS,
                    ErrorDescription: 'A modification or side in your bag is no longer available.',
                },
            });
        });
    });

    describe('pushTipSelection', () => {
        it('should push data in dataLayer', () => {
            GtmService.pushTipSelection({ tipAmount: 4 });
            expect(push).toBeCalledWith({
                event: 'Checkout',
                action: 'Tip Selection',
                label: 4,
            });
        });
    });

    describe('pushAccountDeletedFeedback', () => {
        it('should push data in dataLayer', () => {
            const mockReasons = [
                'I don’t receive offers that are relevant to me',
                'I don’t receive enough offers',
                'I was looking for a Loyalty program',
            ];
            const gtmData = {
                category: GtmCategory.ACCOUNT,
                action: GtmActionFields.Delete,
                reasons: mockReasons,
            } as AccountDeletedFeedbackData;

            GtmService.pushAccountDeletedFeedback(gtmData as AccountDeletedFeedbackData);

            expect(push).toBeCalledWith({
                event: GtmEventNames.AccountDeletedFeedback,
                ...gtmData,
            });
        });
        it('should push data in dataLayer when there is no reasons', () => {
            const mockReasons = [];
            const gtmData = {
                category: GtmCategory.ACCOUNT,
                action: GtmActionFields.Delete,
                reasons: mockReasons,
            } as AccountDeletedFeedbackData;

            GtmService.pushAccountDeletedFeedback(gtmData as AccountDeletedFeedbackData);

            expect(push).toBeCalledWith({
                event: GtmEventNames.AccountDeletedFeedback,
                ...gtmData,
            });
        });
    });

    describe('pushAccountDeletedSubmitted', () => {
        it('should push data in dataLayer', () => {
            const gtmData = {
                category: GtmCategory.ACCOUNT,
                action: GtmActionFields.Delete,
                label: 'Confirmation',
            } as AccountDeletedSubmittedData;

            GtmService.pushAccountDeletedSubmitted(gtmData as AccountDeletedSubmittedData);

            expect(push).toBeCalledWith({
                event: GtmEventNames.AccountDeletedSubmitted,
                ...gtmData,
            });
        });
    });
});
