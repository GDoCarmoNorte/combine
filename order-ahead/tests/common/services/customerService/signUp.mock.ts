export const payloadMock = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@mail.com',
    postalCode: '1234',
    storeId: '123',
    storeName: 'john',
    birthDate: '10/20/1988',
};

export const payloadWithoutBirthDateMock = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@mail.com',
    postalCode: '1234',
    storeId: '123',
    storeName: 'john',
    birthDate: '',
};

export const rejectMock = {
    message: 'Something went wrong!',
};

export const arbysBrandId = 'Arbys';
export const arbysBrandIdMapping = 'ARB';

export const jimmyJohnsBrandId = 'Jje';
export const jimmyJohnsBrandIdMapping = 'JJE';

export const calledWithPayloadMock = {
    cHANNELID: 'WEBOA',
    customerSignUpRequestModel: {
        firstName: payloadMock.firstName,
        lastName: payloadMock.lastName,
        email: payloadMock.email,
        birthDate: payloadMock.birthDate,
        preferences: {
            locations: [
                {
                    id: payloadMock.storeId,
                    isPreferred: true,
                },
            ],
            marketing: [
                {
                    type: 'EMAIL',
                },
            ],
            postalCode: payloadMock.postalCode,
        },
    },
};
