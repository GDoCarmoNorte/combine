import { CustomerControllerV2Api } from '../../../../@generated/domainCustomer/apis';

import { signUp } from '../../../../common/services/customerService';
import getBrandInfo from '../../../../lib/brandInfo';
import addJsonError from '../../../../common/services/addJsonError';

import {
    payloadMock,
    payloadWithoutBirthDateMock,
    rejectMock,
    arbysBrandId,
    arbysBrandIdMapping,
    jimmyJohnsBrandId,
    jimmyJohnsBrandIdMapping,
    calledWithPayloadMock,
} from './signUp.mock';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../lib/brandInfo');
jest.mock('../../../../@generated/domainCustomer/apis/index');

const MockedCustomerControllerApi = CustomerControllerV2Api as jest.MockedClass<typeof CustomerControllerV2Api>;

describe('signUp', () => {
    let domainCustomerSignUpMockFn;

    beforeAll(() => {
        domainCustomerSignUpMockFn = jest.fn().mockReturnValue('Promise');
        (MockedCustomerControllerApi as jest.Mock).mockReturnValue({
            signup: domainCustomerSignUpMockFn,
        });
    });

    afterEach(() => {
        domainCustomerSignUpMockFn.mockReset();
    });

    it('should call with correct parameters and return value', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(payloadMock));
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: arbysBrandId });

        const response = await signUp(payloadMock);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledTimes(1);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledWith({
            brandId: arbysBrandIdMapping,
            ...calledWithPayloadMock,
        });
        expect(response).toEqual(payloadMock);
    });

    it('should call with correct parameters with second brand too', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(payloadMock));
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: jimmyJohnsBrandId });

        const response = await signUp(payloadMock);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledTimes(1);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledWith({
            brandId: jimmyJohnsBrandIdMapping,
            ...calledWithPayloadMock,
        });
        expect(response).toEqual(payloadMock);
    });

    it('should fall when rejecting promise', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.reject(rejectMock));
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: arbysBrandId });

        await expect(signUp(payloadMock)).rejects.toEqual(rejectMock);

        expect(domainCustomerSignUpMockFn).toHaveBeenCalledTimes(1);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledWith({
            brandId: arbysBrandIdMapping,
            ...calledWithPayloadMock,
        });
    });

    it('should call with payload without optional parameter and return value', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(payloadWithoutBirthDateMock));
        (getBrandInfo as jest.Mock).mockReturnValue({ brandId: arbysBrandId });
        const calledWithPayloadWithoutDataMock = {
            ...calledWithPayloadMock,
        };
        calledWithPayloadWithoutDataMock.customerSignUpRequestModel.birthDate = undefined;

        const response = await signUp(payloadWithoutBirthDateMock);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledTimes(1);
        expect(domainCustomerSignUpMockFn).toHaveBeenCalledWith({
            brandId: arbysBrandIdMapping,
            ...calledWithPayloadWithoutDataMock,
        });
        expect(response).toEqual(payloadWithoutBirthDateMock);
    });
});
