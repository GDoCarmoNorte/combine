import logger from '../../../common/services/logger';

describe('logger', () => {
    const newrelicMock = (global as any).newrelic;

    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe('init', () => {
        beforeEach(() => {
            process.env.NEXT_PUBLIC_ARTIFACT_VERSION = '0.558.0-rc-arbys-arb-dev01-291220171448';
            process.env.NEXT_PUBLIC_ENVIRONMENT_NAME = 'arb-dev01';
        });

        afterAll(() => {
            delete process.env.NEXT_PUBLIC_ARTIFACT_VERSION;
            delete process.env.NEXT_PUBLIC_ENVIRONMENT_NAME;
        });

        it('should initialize newrelic', () => {
            logger.init();

            expect(newrelicMock.setCustomAttribute).toHaveBeenCalledWith('environment', 'arb-dev01');
            expect(newrelicMock.setCustomAttribute).toHaveBeenCalledWith('labels.environment', 'arb-dev01');
            expect(newrelicMock.setCustomAttribute).toHaveBeenCalledWith('labels.revision', '0.558.0');
            expect(newrelicMock.addRelease).toHaveBeenCalledWith('0.558.0', '0.558.0');
        });

        it('should intialize newrelic if version is not specified', () => {
            delete process.env.NEXT_PUBLIC_ARTIFACT_VERSION;

            logger.init();

            expect(newrelicMock.setCustomAttribute).toHaveBeenCalledWith('environment', 'arb-dev01');
            expect(newrelicMock.setCustomAttribute).toHaveBeenCalledWith('labels.environment', 'arb-dev01');
            expect(newrelicMock.setCustomAttribute).toHaveBeenCalledWith('labels.revision', 'no version');
            expect(newrelicMock.addRelease).toHaveBeenCalledWith('no version', 'no version');
        });
    });

    describe('logError', () => {
        const realConsoleError = console.error;

        beforeAll(() => {
            console.error = jest.fn();
        });

        beforeEach(() => {
            jest.clearAllMocks();
        });

        afterAll(() => {
            console.error = realConsoleError;
        });

        it('should log error to newrelic', () => {
            const error = 'error';
            const info = {
                foo: 'bar',
            };

            logger.logError(error, info);

            expect(newrelicMock.noticeError).toHaveBeenCalledWith(error, info);
        });
    });

    describe('logEvent', () => {
        const realConsoleLog = console.log;

        beforeAll(() => {
            console.log = jest.fn();
        });

        beforeEach(() => {
            jest.clearAllMocks();
        });

        afterAll(() => {
            console.log = realConsoleLog;
        });

        it('should sent page action to newrelic', () => {
            const actionName = 'action name';
            const attributes = {
                foo: 'bar',
            };

            logger.logEvent(actionName, attributes);

            expect(newrelicMock.addPageAction).toHaveBeenCalledWith(actionName, attributes);
        });
    });
});
