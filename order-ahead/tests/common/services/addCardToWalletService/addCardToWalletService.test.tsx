import {
    AddCreditCardRequest,
    IAccountPaymentMethodModel,
    IAddCreditCardRequestModel,
    WebExperienceApi,
} from '../../../../@generated/webExpApi';
import addJsonError from '../../../../common/services/addJsonError';
import addCardToWalletService from '../../../../common/services/addCardToWalletService';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

describe('addCardToWalletService', () => {
    const MockedWebExperienceApi = WebExperienceApi as jest.MockedClass<typeof WebExperienceApi>;
    let addCreditCardMock;

    const request = {
        iAddCreditCardRequestModel: {} as IAddCreditCardRequestModel,
    } as AddCreditCardRequest;

    const mockJwt = 'mockJwt';

    const successResponse = { label: 'testResponse' } as IAccountPaymentMethodModel;

    beforeAll(() => {
        addCreditCardMock = jest.fn().mockReturnValue('Promise');
        (MockedWebExperienceApi as jest.Mock).mockReturnValue({
            addCreditCard: addCreditCardMock,
        });
    });

    afterEach(() => {
        addCreditCardMock.mockReset();
    });

    it('should succeed and return the response', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(successResponse));

        const response = await addCardToWalletService(request, mockJwt);
        expect(addCreditCardMock).toHaveBeenCalledTimes(1);
        expect(addCreditCardMock).toHaveBeenCalledWith(request);
        expect(response).toEqual(successResponse);
    });

    it('should fail when api fails', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.reject({}));

        await expect(addCardToWalletService(request, mockJwt)).rejects.toEqual({});
        expect(addCreditCardMock).toHaveBeenCalledTimes(1);
        expect(addCreditCardMock).toHaveBeenCalledWith(request);
    });
});
