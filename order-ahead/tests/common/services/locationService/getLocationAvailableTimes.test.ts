import getLocationAvailableTimes from '../../../../common/services/locationService/getLocationAvailableTimes';
import addJsonError from '../../../../common/services/addJsonError';
import { NetworkErrorMessage } from '../../../../common/services/createErrorWrapper';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

describe('getLocationAvailableTimes', () => {
    const request = {
        locationId: 'testId',
    };
    it('should call with correct parameters and return value', async () => {
        const mockedResult = 'locationsAvailableTimesMock';
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(mockedResult));

        const result = await getLocationAvailableTimes(request);
        expect(addJsonError).toHaveBeenCalledTimes(1);

        expect(result).toEqual(mockedResult);
    });
    it('should fall when rejecting promise', async () => {
        (addJsonError as jest.Mock).mockImplementation(() =>
            Promise.reject({
                message: 'Something went wrong!',
            })
        );

        await expect(getLocationAvailableTimes(request)).rejects.toThrowError(NetworkErrorMessage);
    });
});
