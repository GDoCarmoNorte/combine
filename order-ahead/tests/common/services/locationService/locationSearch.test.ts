import * as DomainLocation from '../../../../@generated/webExpApi/apis/index';
import locationSearch from '../../../../common/services/locationService/locationSearch';
import addJsonError from '../../../../common/services/addJsonError';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

const MockedLocationControllerImplApi = DomainLocation.WebExperienceApi as jest.MockedClass<
    typeof DomainLocation.WebExperienceApi
>;

const coordinates = { lat: 10, lng: 20 };
const page = 1;

describe('locationSearch', () => {
    it('should call with correct parameters and return value', async () => {
        const searchLocation = jest.fn().mockReturnValue('Promise');
        (MockedLocationControllerImplApi as jest.Mock).mockReturnValue({
            searchLocation: searchLocation,
        });

        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve('locationsPageMock'));

        const result = await locationSearch(coordinates, page);

        expect(addJsonError).toHaveBeenCalledTimes(1);
        expect(addJsonError).toHaveBeenCalledWith('searchLocationByCoordinates', 'Promise');

        expect(searchLocation).toHaveBeenCalledTimes(1);
        expect(searchLocation).toHaveBeenCalledWith({
            latitude: coordinates.lat,
            longitude: coordinates.lng,
            radius: 50,
            limit: 10,
            page: 1,
            locale: 'en-us',
        });

        expect(result).toEqual('locationsPageMock');
    });
});
