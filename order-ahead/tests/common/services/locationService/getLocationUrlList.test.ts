import getLocationUrlList from '../../../../common/services/locationService/getLocationUrlList';
import addJsonError from '../../../../common/services/addJsonError';
import { NetworkErrorMessage } from '../../../../common/services/createErrorWrapper';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

describe('getLocationUrlList', () => {
    it('should call with correct parameters and return value', async () => {
        const mockedResult = 'locationsListPageMock';
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(mockedResult));

        const result = await getLocationUrlList();
        expect(addJsonError).toHaveBeenCalledTimes(1);

        expect(result).toEqual(mockedResult);
    });
    it('should fall when rejecting promise', async () => {
        (addJsonError as jest.Mock).mockImplementation(() =>
            Promise.reject({
                message: 'Something went wrong!',
            })
        );

        await expect(getLocationUrlList()).rejects.toThrowError(NetworkErrorMessage);
    });
});
