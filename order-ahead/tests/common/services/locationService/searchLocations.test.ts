import searchLocations from '../../../../common/services/locationService/searchLocations';
import addJsonError from '../../../../common/services/addJsonError';
import { NetworkErrorMessage } from '../../../../common/services/createErrorWrapper';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

const countryCode = 'US';
const stateOrProvinceCode = 'MN';

describe('locationListSearch', () => {
    it('should call with correct parameters and return value', async () => {
        const mockedResult = 'locationsListPageMock';
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(mockedResult));

        const result = await searchLocations(countryCode, stateOrProvinceCode);
        expect(addJsonError).toHaveBeenCalledTimes(1);

        expect(result).toEqual(mockedResult);
    });
    it('should fall when rejecting promise', async () => {
        (addJsonError as jest.Mock).mockImplementation(() =>
            Promise.reject({
                message: 'Something went wrong!',
            })
        );

        await expect(searchLocations(countryCode, stateOrProvinceCode)).rejects.toThrowError(NetworkErrorMessage);
    });
});
