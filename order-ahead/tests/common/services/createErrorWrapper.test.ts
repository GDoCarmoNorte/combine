import createErrorWrapper, {
    defaultHttpStatusCodeErrorMap,
    FetchError,
    NetworkErrorMessage,
} from '../../../common/services/createErrorWrapper';

jest.mock('../../../common/services/logger');

describe('createErrorWrapper', () => {
    beforeAll(() => (console.log = jest.fn()));
    afterAll(() => jest.clearAllMocks());

    test('should call return response for success apiCall', async () => {
        const apiCallMock = () => Promise.resolve('data');

        const request = createErrorWrapper('testApi', apiCallMock);

        expect(await request({})).toBe('data');
    });

    test('should call api with provided args', async () => {
        const apiCallMock = jest.fn();

        const request = createErrorWrapper('testApi', (args) => apiCallMock(args));

        await request({ id: 1 });

        expect(apiCallMock).toBeCalledWith({ id: 1 });
        expect(apiCallMock).toBeCalledTimes(1);
    });

    test('should return error message from errorMap for rejected apiCall', async () => {
        const errorMap = {
            400: 'Message 400',
        };

        const apiCallMock = () =>
            Promise.reject({
                status: 400,
                json: () => Promise.resolve('Error 400'),
            });

        const request = createErrorWrapper('testApi', apiCallMock, errorMap);

        await expect(request({})).rejects.toThrowError('Message 400');
    });

    test('should return default error message for status code for rejected apiCall', async () => {
        Object.keys(defaultHttpStatusCodeErrorMap).forEach(async (code) => {
            const apiCallMock = () =>
                Promise.reject({
                    status: code,
                    json: () => Promise.resolve(`Error Code ${code}`),
                });

            const request = createErrorWrapper('testApi', apiCallMock);

            await expect(request({})).rejects.toThrowError(defaultHttpStatusCodeErrorMap[code]);
        });
    });

    test('should return default error message from errorMap for rejected apiCall', async () => {
        const errorMap = {
            400: 'Message 400',
            default: 'Default Message',
        };

        const apiCallMock = () =>
            Promise.reject({
                status: 999,
                json: () => Promise.resolve('Error 999'),
            });

        const request = createErrorWrapper('testApi', apiCallMock, errorMap);

        await expect(request({})).rejects.toThrowError('Default Message');
    });

    test('should return default error message from errorMap for rejected apiCall without status code', async () => {
        const apiCallMock = () => Promise.reject();

        const request = createErrorWrapper('testApi', apiCallMock);

        await expect(request({})).rejects.toThrowError(NetworkErrorMessage);
    });

    test('should return handled error for PRODUCTS_NOT_AVAILABLE', async () => {
        const errorMap = {
            [FetchError.NetworkError]: 'Network Error Message',
        };

        const PRODUCTS_NOT_AVAILABLE = {
            type: 'EXTERNAL',
            code: 'PRODUCTS_NOT_AVAILABLE',
            message: 'Some products are not available.',
            details: ['IDPSalesItem-6348', 'IDPSalesItem-6345'],
        };

        const apiCallMock = () => Promise.reject(PRODUCTS_NOT_AVAILABLE);

        const request = createErrorWrapper('testApi', apiCallMock, errorMap);

        await expect(request({})).resolves.toEqual(PRODUCTS_NOT_AVAILABLE);
    });
});
