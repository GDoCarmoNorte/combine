import { getAndWriteContentfulProps } from '../../../../common/services/globalContentfulProps/getAndWriteContentfulProps';
import { ContentfulDelivery } from '../../../../lib/contentfulDelivery';
import productsMock from '../../../mock-data/contentful/contentful-getEntries-product.json';
import productModifiersMock from '../../../mock-data/contentful/contentful-getEntries-productModifier.json';
import productDetailsPagePathsMock from '../../../mock-data/contentful/contentful-getEntries-pages.json';
import alertBannersMock from '../../../mock-data/contentful/contentful-getEntries-alertBanner.json';
import navigationMock from '../../../mock-data/contentful/contentful-getEntries-navigation.json';
import footerMock from '../../../mock-data/contentful/contentful-getEntries-footer.json';
import dealsMock from '../../../mock-data/contentful/contentful-getEntries-deal.json';
import userAccountMenuMock from '../../../mock-data/contentful/contentful-getEntries-userAccountMenu.json';
import emptyShoppingBagMock from '../../../mock-data/contentful/contentful-getEntries-emptyShoppingBag.json';
import sodiumWarningsMock from '../../../mock-data/contentful/contentful-getEntries-sodium-warnings.json';
import actionParametersMock from '../../../mock-data/contentful/contentful-getEntries-action-parameters.json';

import fs from 'fs';
import { CMS_FILE_PATH } from '../../../../common/services/globalContentfulProps/constants';

jest.mock('fs', () => ({
    writeFileSync: jest.fn(),
}));

jest.mock('../../../../lib/contentfulDelivery', () => ({
    ContentfulDelivery: jest.fn(),
}));

describe('getAndWriteContentfulProps', () => {
    beforeEach(() => {
        fs.writeFileSync = jest.fn();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should write to file data if request was successful', async () => {
        (ContentfulDelivery as jest.Mock).mockReturnValueOnce({
            getNavigation: jest.fn().mockReturnValueOnce(navigationMock),
            getAlertBanners: jest.fn().mockReturnValueOnce(alertBannersMock),
            getProducts: jest.fn().mockReturnValueOnce(productsMock),
            getProductModifiers: jest.fn().mockReturnValueOnce(productModifiersMock),
            getFooter: jest.fn().mockReturnValueOnce(footerMock),
            getProductDetailsPagePaths: jest.fn().mockReturnValueOnce(productDetailsPagePathsMock),
            getDeals: jest.fn().mockReturnValueOnce(dealsMock),
            getUserAccountMenu: jest.fn().mockReturnValueOnce(userAccountMenuMock),
            getEmptyShoppingBag: jest.fn().mockReturnValueOnce(emptyShoppingBagMock),
            getSodiumWarnings: jest.fn().mockReturnValueOnce(sodiumWarningsMock),
            getActionParameters: jest.fn().mockReturnValueOnce(actionParametersMock),
        });

        const data = await getAndWriteContentfulProps({ preview: false });
        expect(fs.writeFileSync).toHaveBeenCalledTimes(1);
        expect(fs.writeFileSync).toHaveBeenCalledWith(CMS_FILE_PATH, JSON.stringify(data), 'utf8');
    });

    it('should log error in case some of the requests fail', async () => {
        const consoleSpy = jest.spyOn(console, 'log');

        (ContentfulDelivery as jest.Mock).mockReturnValueOnce({
            getNavigation: jest.fn().mockReturnValueOnce(navigationMock),
            getAlertBanners: jest.fn().mockReturnValueOnce(alertBannersMock),
            getProducts: jest.fn().mockRejectedValue(undefined),
            getProductModifiers: jest.fn().mockReturnValueOnce(productModifiersMock),
            getFooter: jest.fn().mockReturnValueOnce(footerMock),
            getProductDetailsPagePaths: jest.fn().mockReturnValueOnce(productDetailsPagePathsMock),
            getDeals: jest.fn().mockReturnValueOnce(dealsMock),
            getUserAccountMenu: jest.fn().mockReturnValueOnce(userAccountMenuMock),
            getEmptyShoppingBag: jest.fn().mockReturnValueOnce(emptyShoppingBagMock),
            getSodiumWarnings: jest.fn().mockReturnValueOnce(sodiumWarningsMock),
            getActionParameters: jest.fn().mockReturnValueOnce(actionParametersMock),
        });

        await getAndWriteContentfulProps({ preview: false });

        expect(consoleSpy).toHaveBeenCalledWith('problems with writing CMS data');
    });

    it('should log error to console in case there was error with writing data', async () => {
        (ContentfulDelivery as jest.Mock).mockReturnValueOnce({
            getNavigation: jest.fn().mockReturnValueOnce(navigationMock),
            getAlertBanners: jest.fn().mockReturnValueOnce(alertBannersMock),
            getProducts: jest.fn().mockReturnValueOnce(productsMock),
            getProductModifiers: jest.fn().mockReturnValueOnce(productModifiersMock),
            getFooter: jest.fn().mockReturnValueOnce(footerMock),
            getProductDetailsPagePaths: jest.fn().mockReturnValueOnce(productDetailsPagePathsMock),
            getDeals: jest.fn().mockReturnValueOnce(dealsMock),
            getUserAccountMenu: jest.fn().mockReturnValueOnce(userAccountMenuMock),
            getEmptyShoppingBag: jest.fn().mockReturnValueOnce(emptyShoppingBagMock),
            getSodiumWarnings: jest.fn().mockReturnValueOnce(sodiumWarningsMock),
            getActionParameters: jest.fn().mockReturnValueOnce(actionParametersMock),
        });
        const error = new Error('error has been occured');
        const consoleSpy = jest.spyOn(console, 'log');

        (fs.writeFileSync as jest.Mock).mockImplementation(() => {
            throw error;
        });

        await getAndWriteContentfulProps({ preview: false });
        expect(consoleSpy).toHaveBeenCalledTimes(2);
        expect(consoleSpy).toHaveBeenCalledWith(error);
        expect(consoleSpy).toHaveBeenCalledWith('problems with writing CMS data');
    });
});
