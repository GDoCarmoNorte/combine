import { getGlobalContentfulProps, IGlobalContentfulProps } from '../../../../common/services/globalContentfulProps';
import { getAndWriteContentfulProps } from '../../../../common/services/globalContentfulProps/getAndWriteContentfulProps';
import fs from 'fs';

jest.mock('fs', () => ({
    existsSync: jest.fn(),
    readFileSync: jest.fn(),
}));

jest.mock('../../../../common/services/globalContentfulProps/getAndWriteContentfulProps', () => ({
    getAndWriteContentfulProps: jest.fn(),
}));

describe('getGlobalContentfulProps', () => {
    beforeEach(() => {
        fs.existsSync = jest.fn();
        fs.readFileSync = jest.fn();
    });

    it('should call API to retrieve data in case parsing from json was not successful', async () => {
        (fs.existsSync as jest.Mock).mockReturnValue(true);
        (fs.readFileSync as jest.Mock).mockReturnValue(undefined);
        (getAndWriteContentfulProps as jest.Mock).mockResolvedValue('data.json');
        await getGlobalContentfulProps({ preview: false });
        expect(getAndWriteContentfulProps).toHaveBeenCalledTimes(1);
    });

    it('should return correct data in case JSON parsing was successful', async () => {
        (fs.existsSync as jest.Mock).mockReturnValue(true);
        const expected = {
            navigation: {},
            alertBanners: {},
            productsById: {},
            modifierItemsById: {},
            footer: {},
            productDetailsPagePaths: {},
            dealItemsById: {},
            userAccountMenu: {},
            emptyShoppingBag: {},
            sodiumWarnings: {},
            actionParameters: {},
        } as IGlobalContentfulProps;
        (fs.readFileSync as jest.Mock).mockReturnValue(JSON.stringify(expected));
        const actual = await getGlobalContentfulProps({ preview: false });
        expect(actual).toEqual(expected);
    });
});
