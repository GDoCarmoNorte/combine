import { WebExperienceApi } from '../../../../@generated/webExpApi/apis/index';
import fetchRecommendedProducts from '../../../../common/services/recommendationService/recommendedProducts';
import addJsonError from '../../../../common/services/addJsonError';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

const MockedWebExperienceApi = WebExperienceApi as jest.MockedClass<typeof WebExperienceApi>;

const payloadMockForCall = {
    sellingChannel: 'WEBOA',
    menuType: 'ALLDAY',
    productsRecommendationRequestModel: {
        customerType: 'PC',
        productIds: ['arb-itm-000-067'],
        correlationId: 'de2dfb5f-4cc0-4e10-9f28-392c0ff447f8',
        locationId: '99981',
    },
};

const payloadMock = payloadMockForCall.productsRecommendationRequestModel;

describe('recommendedProducts', () => {
    it('should call with correct parameters and return value', async () => {
        const fetchRecommendedItemsMockedFn = jest.fn().mockReturnValue('Promise');
        (MockedWebExperienceApi as jest.Mock).mockReturnValue({
            fetchRecommendedItems: fetchRecommendedItemsMockedFn,
        });

        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(payloadMock));

        const result = await fetchRecommendedProducts(payloadMock);

        expect(addJsonError).toHaveBeenCalledTimes(1);
        expect(addJsonError).toHaveBeenCalledWith('fetchRecommendedItems', 'Promise');

        expect(fetchRecommendedItemsMockedFn).toHaveBeenCalledTimes(1);
        expect(fetchRecommendedItemsMockedFn).toHaveBeenCalledWith(payloadMockForCall);

        expect(result).toEqual(payloadMockForCall.productsRecommendationRequestModel);
    });
});
