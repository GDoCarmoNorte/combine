import addJsonError from '../../../common/services/addJsonError';
import logger from '../../../common/services/logger';

jest.mock('../../../common/services/logger');

describe('addJsonError', () => {
    it('should call logEvent and return response for success apiCall', async () => {
        const responseMock = 'data';
        const apiCallMock = Promise.resolve(responseMock);

        const logEventMock = jest.fn();
        const apiMock = 'testApi';

        jest.spyOn(logger, 'logEvent').mockImplementationOnce(logEventMock);

        const result = await addJsonError(apiMock, apiCallMock);

        expect(logEventMock).toHaveBeenCalledWith(`Domain api latency for ${apiMock}`, {
            api: apiMock,
            latency: expect.anything(),
        });

        expect(result).toBe(responseMock);
    });

    it('should call logError and return error for rejected apiCall, status >= 400', () => {
        const errorMock = { message: 'Something went wrong!' };
        const jsonMock = () => Promise.resolve(errorMock);

        [400, 404, 500, 503, 700].forEach(async (statusMock) => {
            const rejectMock = {
                status: statusMock,
                json: jsonMock,
            };

            const apiCallMock = Promise.reject(rejectMock);

            const logEventMock = jest.fn();
            const apiMock = 'testApi';

            jest.spyOn(logger, 'logError').mockImplementationOnce(logEventMock);

            await expect(addJsonError(apiMock, apiCallMock)).rejects.toEqual({ ...errorMock, status: statusMock });
            expect(logEventMock).toHaveBeenCalledWith(`Error while invoking domain service ${apiMock}`, {
                api: apiMock,
                apiStatusCode: statusMock,
                apiResponse: { ...errorMock },
                ...rejectMock,
            });
        });
    });

    it('should show specific error, xml', async () => {
        const statusMock = 404;
        const rejectMock = {
            status: statusMock,
            // no json method
        };

        const apiCallMock = Promise.reject(rejectMock);

        const logEventMock = jest.fn();
        const apiMock = 'testApi';

        jest.spyOn(logger, 'logError').mockImplementationOnce(logEventMock);

        await expect(addJsonError(apiMock, apiCallMock)).rejects.toEqual(rejectMock);
        expect(logEventMock).toHaveBeenCalledWith(`Error while invoking domain service ${apiMock}`, {
            api: apiMock,
            ...rejectMock,
            apiResponse: undefined,
            apiStatusCode: statusMock,
        });
    });

    it('should show specific error, json() throws', async () => {
        const statusMock = 500;
        const jsonMock = () => Promise.reject();
        const rejectMock = {
            status: statusMock,
            json: jsonMock,
        };

        const apiCallMock = Promise.reject(rejectMock);

        const logEventMock = jest.fn();
        const apiMock = 'testApi';

        jest.spyOn(logger, 'logError').mockImplementationOnce(logEventMock);

        await expect(addJsonError(apiMock, apiCallMock)).rejects.toEqual(rejectMock);
        expect(logEventMock).toHaveBeenCalledWith(`Error while invoking domain service ${apiMock}`, {
            api: apiMock,
            ...rejectMock,
            apiResponse: undefined,
            apiStatusCode: statusMock,
        });
    });
});
