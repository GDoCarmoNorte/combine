import navInterceptService from '../../../common/services/navInterceptService';

describe('navInterceptService', () => {
    describe('requestNavIntercept', () => {
        it('should pause and then proceed navigation', (done) => {
            navInterceptService.intercept().then((result) => {
                expect(result).toBe(true);
                done();
            });

            navInterceptService.navigate(true);
        });

        it('should pause and then cancel navigation', (done) => {
            navInterceptService.intercept().then((result) => {
                expect(result).toBe(false);
                done();
            });

            navInterceptService.navigate(false);
        });
    });
});
