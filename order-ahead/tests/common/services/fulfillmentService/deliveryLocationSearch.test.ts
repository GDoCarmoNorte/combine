import { WebExperienceApi } from '../../../../@generated/webExpApi/apis';
import deliveryLocationSearch from '../../../../common/services/fulfillmentService/deliveryLocationSearch';
import addJsonError from '../../../../common/services/addJsonError';

import { payloadMock, rejectMock } from './deliveryLocationSearch.mock';

jest.mock('../../../../common/services/addJsonError');
jest.mock('../../../../@generated/webExpApi/apis/index');

const MockedWebExperienceApi = WebExperienceApi as jest.MockedClass<typeof WebExperienceApi>;

describe('deliveryLocationSearch', () => {
    let searchDeliveryLocationMockFn;

    beforeAll(() => {
        searchDeliveryLocationMockFn = jest.fn().mockReturnValue('Promise');
        (MockedWebExperienceApi as jest.Mock).mockReturnValue({
            searchDeliveryLocation: searchDeliveryLocationMockFn,
        });
    });

    afterEach(() => {
        searchDeliveryLocationMockFn.mockReset();
    });

    it('should call with correct parameters and return value', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.resolve(payloadMock));

        const response = await deliveryLocationSearch(payloadMock);
        expect(searchDeliveryLocationMockFn).toHaveBeenCalledTimes(1);
        expect(searchDeliveryLocationMockFn).toHaveBeenCalledWith(payloadMock);
        expect(response).toEqual(payloadMock);
    });

    it('should fall when rejecting promise', async () => {
        (addJsonError as jest.Mock).mockImplementation(() => Promise.reject(rejectMock));

        await expect(deliveryLocationSearch(payloadMock)).rejects.toEqual(rejectMock);

        expect(searchDeliveryLocationMockFn).toHaveBeenCalledTimes(1);
        expect(searchDeliveryLocationMockFn).toHaveBeenCalledWith(payloadMock);
    });
});
