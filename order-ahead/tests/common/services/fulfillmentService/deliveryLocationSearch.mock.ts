export const payloadMock = {
    latitude: 123,
    longitude: 456,
};

export const rejectMock = {
    message: 'Something went wrong!',
};

export const calledWithPayloadMock = {
    ...payloadMock,
};
