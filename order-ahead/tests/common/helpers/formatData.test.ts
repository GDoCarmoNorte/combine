import { formatAccountName } from '../../../common/helpers/formatData';

describe('formatData function', () => {
    describe('formatAccountName function', () => {
        it('should return 70 characters if account name has more', () => {
            expect(
                formatAccountName('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbb', 70)
            ).toEqual('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
        });

        it('should return full account name if account name has less than 70 characters', () => {
            expect(formatAccountName('bbbb', 70)).toEqual('bbbb');
        });

        it('should return empty string if account name is empty', () => {
            expect(formatAccountName('', 70)).toEqual('');
        });
    });
});
