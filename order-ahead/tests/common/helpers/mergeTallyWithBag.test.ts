import { mergeTallyWithBag } from '../../../common/helpers/mergeTallyWithBag';
import { bagProductsMock, mergedTallyItems, tallyResponseMock } from './mergeTallyWithBag.mock';

describe('mergeTallyWithBag', () => {
    it('should map correct', () => {
        expect(mergeTallyWithBag(tallyResponseMock, bagProductsMock)).toEqual(mergedTallyItems);
    });
});
