import { getPreviousLocationData } from '../../../common/helpers/getPreviousLocationData';
import { OrderLocationMethod } from '../../../redux/orderLocation';

describe('getPreviousLocationData', () => {
    it('should return pickup address for pickup method', () => {
        expect(
            getPreviousLocationData({
                method: OrderLocationMethod.PICKUP,
                pickupAddress: 'Pickup address' as any,
                deliveryAddress: null,
            })
        ).toBe('Pickup address');
    });

    it('should return delivery address for delivery method', () => {
        expect(
            getPreviousLocationData({
                method: OrderLocationMethod.DELIVERY,
                pickupAddress: null,
                deliveryAddress: 'Delivery address' as any,
            })
        ).toBe('Delivery address');
    });

    it('should return null', () => {
        expect(
            getPreviousLocationData({
                method: OrderLocationMethod.NOT_SELECTED,
                pickupAddress: null,
                deliveryAddress: null,
            })
        ).toBe(null);
    });
});
