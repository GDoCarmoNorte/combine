import { capitalizeString } from '../../../common/helpers/capitalizeString';

describe('capitalizeString', () => {
    it('should convert "ABC" -> "Abc"', () => {
        expect(capitalizeString('ABC')).toBe('Abc');
    });

    it('should convert "FOO bAR arbys" -> "Foo Bar Arbys"', () => {
        expect(capitalizeString('FOO bAR arbys')).toBe('Foo Bar Arbys');
    });
});
