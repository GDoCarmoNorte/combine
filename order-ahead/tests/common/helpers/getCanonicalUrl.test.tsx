import { ContentTypeLink } from 'contentful';
import { IPageLink } from '../../../@generated/@types/contentful';
import { getCanonicalUrl } from '../../../common/helpers/getCanonicalUrl';

const TEST_CANONICAL_URL = 'testCanonicalUrl';

describe('getCanonicalUrl', () => {
    it('should return value from the provided source', () => {
        const pageEntryMock = {
            fields: { canonicalUrl: TEST_CANONICAL_URL, metaDescription: '', metaTitle: '', link: {} as IPageLink },
            sys: { id: '', contentType: { sys: {} as ContentTypeLink } },
        };

        const canonicalUrl = getCanonicalUrl(pageEntryMock, '/');
        expect(canonicalUrl).toBe(`/${TEST_CANONICAL_URL}/`);
    });

    it('should return default value if provided source does not contain url', () => {
        const emptyPageEntryMock = {
            fields: { canonicalUrl: '', metaDescription: '', metaTitle: '', link: {} as IPageLink },
            sys: { id: '', contentType: { sys: {} as ContentTypeLink } },
        };

        const canonicalUrl = getCanonicalUrl(emptyPageEntryMock, TEST_CANONICAL_URL);
        expect(canonicalUrl).toBe(TEST_CANONICAL_URL);
    });
});
