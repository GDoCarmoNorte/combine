import { getAccountNavLinks } from '../../../common/helpers/accountHelper';
import { isAccountDealsPageOn, isRewardsOn } from '../../../lib/getFeatureFlags';
import MockData from '../../__mocks__/contentful/userAccountMenu.mock.json';
import configurationMock from '../../mocks/configuration.mock';

jest.mock('../../../lib/getFeatureFlags');

const configuration = {
    ...configurationMock,
    isMyTeamsEnabled: false,
};

describe('account helpers', () => {
    describe('getAccountNavLinks', () => {
        it('should return default nav links', () => {
            const result = getAccountNavLinks(MockData as any, 0, configuration);

            expect(result).toEqual([
                {
                    link: '/account',
                    name: 'PROFILE',
                    trackingId: 'accountProfile',
                    type: 'PROFILE',
                    exact: true,
                },
            ]);
        });

        it('should return nav links with deals page if accountDealsPage flag is on', () => {
            (isAccountDealsPageOn as jest.Mock).mockReturnValueOnce(true);
            (isRewardsOn as jest.Mock).mockReturnValueOnce(false);
            const result = getAccountNavLinks(MockData as any, 0, configuration);

            expect(result).toEqual([
                {
                    link: '/account',
                    name: 'PROFILE',
                    type: 'PROFILE',
                    trackingId: 'accountProfile',
                    exact: true,
                },
                {
                    link: '/account/deals',
                    name: 'DEALS',
                    type: 'DEALS',
                    trackingId: 'accountDeals',
                    exact: false,
                },
            ]);
        });
        it('should return nav links with rewards page if isRewardsOn flag is on', () => {
            (isAccountDealsPageOn as jest.Mock).mockReturnValueOnce(false);
            (isRewardsOn as jest.Mock).mockReturnValueOnce(true);
            const result = getAccountNavLinks(MockData as any, 0, configuration);

            expect(result).toEqual([
                {
                    link: '/account',
                    name: 'PROFILE',
                    type: 'PROFILE',
                    trackingId: 'accountProfile',
                    exact: true,
                },
                {
                    link: '/account/rewards',
                    name: 'Rewards',
                    type: 'REWARDS',
                    trackingId: 'accountRewards',
                    exact: false,
                },
            ]);
        });
        it('should return nav links with my teams page if isMyTeamsEnabled flag is on', () => {
            const result = getAccountNavLinks(MockData as any, 0, {
                ...configuration,
                isMyTeamsEnabled: true,
            });

            expect(result).toEqual([
                {
                    link: '/account',
                    name: 'PROFILE',
                    type: 'PROFILE',
                    trackingId: 'accountProfile',
                    exact: true,
                },
                {
                    link: '/account/my-teams',
                    name: 'My Teams',
                    type: 'MY_TEAMS',
                    trackingId: 'accountMy_teams',
                    exact: false,
                },
            ]);
        });
    });
});
