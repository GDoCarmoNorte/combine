export const gPayEventMock = {
    isTrusted: true,
    bubbles: false,
    cancelBubble: false,
    cancelable: false,
    composed: false,
    data: {
        data: {
            attributes: [
                {
                    Key: 'CardIssuer',
                    Value: 'Visa',
                },
                {
                    Key: 'MaskedCardNumber',
                    Value: 'Visa •••• 1623',
                },
                {
                    Key: 'BillingAddress',
                    Value: {
                        address3: '',
                        sortingCode: '',
                        address2: '',
                        countryCode: 'US',
                        address1: 'D',
                        postalCode: '19703',
                        name: 'David Nazaryan',
                        locality: 'CLAYMONT',
                        administrativeArea: 'DE',
                    },
                },
                {
                    Key: 'Name',
                    Value: 'David Nazaryan',
                },
            ],
            paymentKeys: ['6f6dc04d-3649-4cf5-a8ce-6bf0a85e9426'],
            paymentType: 'GooglePay',
        },
        messageCode: undefined,
        type: 3,
    },
    defaultPrevented: false,
    eventPhase: 0,
    lastEventId: '',
    origin: 'https://hpc.uat.freedompay.test',
    returnValue: true,
    source: null,
    timeStamp: 272093,
    type: 'message',
    userActivation: null,
};

export const aPayEventMock = {
    isTrusted: true,
    bubbles: false,
    cancelBubble: false,
    cancelable: false,
    composed: false,
    data: {
        data: {
            attributes: [
                {
                    Key: 'CardIssuer',
                    Value: 'Visa',
                },
                {
                    Key: 'MaskedCardNumber',
                    Value: 'Visa •••• 1623',
                },
                {
                    Key: 'BillingAddress',
                    Value: {
                        address3: '',
                        sortingCode: '',
                        address2: '',
                        countryCode: 'US',
                        address1: 'D',
                        postalCode: '19703',
                        name: 'David Nazaryan',
                        locality: 'CLAYMONT',
                        administrativeArea: 'DE',
                    },
                },
                {
                    Key: 'Name',
                    Value: 'Davit Nazaryan',
                },
            ],
            paymentKeys: ['6f6dc04d-3649-4cf5-a8ce-6bf0a85e9426'],
            paymentType: 'ApplePay',
        },
        messageCode: undefined,
        type: 3,
    },
    defaultPrevented: false,
    eventPhase: 0,
    lastEventId: '',
    origin: 'http://www.test.app.url',
    returnValue: true,
    source: null,
    timeStamp: 272093,
    type: 'message',
    userActivation: null,
};

export const randomEventMock = {
    isTrusted: true,
    bubbles: false,
    cancelBubble: false,
    cancelable: false,
    composed: false,
    data: {
        test: 'mock',
    },
    defaultPrevented: false,
    eventPhase: 0,
    lastEventId: '',
    origin: 'https://test.mock.lib',
    returnValue: true,
    source: null,
    timeStamp: 272093,
    type: 'message',
    userActivation: null,
};
