import {
    createSubmitOrderModifierGroup,
    createSubmitOrderRequest,
    createDineInSubmitOrderRequest,
} from '../../../common/helpers/submitOrderHelper';
import { ICustomerInfo } from '../../../components/organisms/checkout/customerInfo';
import locationsMock from '../../mocks/expLocations.mock';
import { PaymentTypeModel, TallyResponseModel } from '../../../@generated/webExpApi';
import { IPaymentState } from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { IPaymentRequest } from '../../../components/clientOnly/paymentInfoContainer/paymentInfo';
import { IGiftCard } from '../../../components/clientOnly/paymentInfoContainer/types';
import { getFloatNumber } from '../../../common/helpers/getFloatNumber';

const location = locationsMock.locations[0];

const tallyOrder = {
    products: [],
    total: 0,
    tax: 0,
    subTotalAfterDiscounts: 0,
    totalSaving: 0,
    fulfillment: {
        email: 'email@gmail.com',
        fulfillmentContactFirstName: 'name',
        fulfillmentContactLastName: 'last name',
        phoneNumber: '+11234241234',
        pickupInstructions: ['Look for your name at the online order shelf in the lobby'],
        orderTime: '2021-02-23T08:48:36.123Z',
        storeLocation: location,
        type: 'PickUp',
    },
    sellingChannel: 'WEBOA',
    brandId: 'arbys',
};

const customerInfo = {
    email: 'tests@gmail.com',
    phone: '1111111111',
    firstName: 'first name',
    lastName: 'last name',
};

const paymentRequest = {
    paymentKeys: ['payment key1', 'payment key2'],
    sessionKey: 'session key',
    cardIssuer: 'cardIssuer',
    maskedCardNumber: 'maskedCardNumber',
    chFirstName: 'chFirstName',
    chLastName: 'chLastName',
};

const paymentInfo = {
    type: 'CREDIT_OR_DEBIT',
};

const giftCard = {
    sessionKey: 'session key',
    paymentToken: {
        keys: ['key1', 'key2'],
    },
    balance: 8,
};

jest.mock('uuid', () => ({
    __esModule: true,
    v4: jest.fn().mockReturnValue('0'),
}));

describe('submitOrderHelper', () => {
    it('createSubmitOrderRequest: should return correct value', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: (tallyOrder as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            paymentRequest: paymentRequest as IPaymentRequest,
            paymentInfo: paymentInfo as IPaymentState,
            location: location as any,
            orderTime: null,
            driverTip: 0,
            serverTip: 0,
        });

        expect(result).toStrictEqual({
            orderRequestModel: {
                deviceId: '',
                orderData: {
                    locationId: location.id,
                    menuType: 'ALLDAY',
                    fulfillmentType: tallyOrder.fulfillment.type,
                    isAsap: true,
                    requestedDateTime: undefined,
                    customerId: undefined,
                    cartId: '0',
                    driverTip: 0,
                    serverTip: 0,
                    email: customerInfo.email,
                    firstName: customerInfo.firstName,
                    lastName: customerInfo.lastName,
                    phoneNumber: customerInfo.phone,
                    instructions: undefined,
                    products: [],
                },
                payments: [
                    {
                        type: PaymentTypeModel.Card,
                        details: {
                            keys: paymentRequest.paymentKeys,
                            sessionKey: paymentRequest.sessionKey,
                            cardIssuer: paymentRequest.cardIssuer,
                            maskedCardNumber: paymentRequest.maskedCardNumber,
                            cardHolderName: 'chFirstName chLastName',
                            amount: tallyOrder.total,
                        },
                    },
                ],
            },
        });
    });

    it('createSubmitOrderRequest: should return customerId as undefined if passed customerId is empty string', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: (tallyOrder as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            customerId: '',
            paymentInfo: paymentInfo as IPaymentState,
            location: location as any,
            orderTime: null,
        });

        expect(result).toMatchObject({
            orderRequestModel: {
                orderData: {
                    customerId: undefined,
                },
            },
        });
    });

    it('createSubmitOrderRequest: should send payment details as null if no payment is required', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: (tallyOrder as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            paymentRequest: {
                paymentKeys: null,
                sessionKey: null,
                cardIssuer: null,
                maskedCardNumber: null,
                chFirstName: null,
                chLastName: null,
            } as IPaymentRequest,
            paymentInfo: { type: 'NO_PAYMENT' } as IPaymentState,
            location: location as any,
            orderTime: null,
            driverTip: 0,
            serverTip: 0,
        });
        expect(result).toStrictEqual({
            orderRequestModel: {
                deviceId: '',
                orderData: {
                    locationId: location.id,
                    menuType: 'ALLDAY',
                    fulfillmentType: tallyOrder.fulfillment.type,
                    isAsap: true,
                    requestedDateTime: undefined,
                    customerId: undefined,
                    cartId: '0',
                    driverTip: 0,
                    serverTip: 0,
                    email: customerInfo.email,
                    firstName: customerInfo.firstName,
                    lastName: customerInfo.lastName,
                    phoneNumber: customerInfo.phone,
                    instructions: undefined,
                    products: [],
                },
                payments: [
                    {
                        type: PaymentTypeModel.NoPayment,
                    },
                ],
            },
        });
    });

    it('createSubmitOrderRequest: should return customerId as undefined if passed customerId is undefined', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: (tallyOrder as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            paymentInfo: paymentInfo as IPaymentState,
            location: location as any,
            orderTime: null,
        });

        expect(result).toMatchObject({
            orderRequestModel: {
                orderData: {
                    customerId: undefined,
                },
            },
        });
    });

    it('createSubmitOrderRequest: should return correct value with pickup instractions and pickup time', () => {
        const result = createSubmitOrderModifierGroup({
            productId: 'product-1',
            modifiers: [
                {
                    productId: 'modifier-1',
                    price: 0,
                    quantity: 0,
                },
            ],
        });

        expect(result).toStrictEqual({
            productId: 'product-1',
            modifiers: [
                {
                    productId: 'modifier-1',
                    price: 0,
                    quantity: 0,
                },
            ],
        });
    });

    it('createSubmitOrderRequest: should return correct value for gift card payment only', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: ({
                ...tallyOrder,
                total: 5,
            } as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            giftCards: [giftCard as IGiftCard],
            paymentInfo: paymentInfo as IPaymentState,
            location: location as any,
            orderTime: null,
        });

        expect(result).toMatchObject({
            orderRequestModel: {
                payments: [
                    {
                        type: PaymentTypeModel.GiftCard,
                        details: {
                            keys: [giftCard.paymentToken.keys[1]],
                            sessionKey: giftCard.sessionKey,
                            amount: 5,
                        },
                    },
                ],
            },
        });
    });

    it('createSubmitOrderRequest: should return correct value for gift card and credit card payment', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: ({
                ...tallyOrder,
                total: 15,
            } as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            giftCards: [giftCard as IGiftCard],
            paymentRequest: paymentRequest as IPaymentRequest,
            paymentInfo: paymentInfo as IPaymentState,
            location: location as any,
            orderTime: null,
        });

        expect(result).toMatchObject({
            orderRequestModel: {
                payments: [
                    {
                        type: PaymentTypeModel.GiftCard,
                        details: {
                            keys: [giftCard.paymentToken.keys[1]],
                            sessionKey: giftCard.sessionKey,
                            amount: 8,
                        },
                    },
                    {
                        type: PaymentTypeModel.Card,
                        details: {
                            keys: paymentRequest.paymentKeys,
                            sessionKey: paymentRequest.sessionKey,
                            cardIssuer: paymentRequest.cardIssuer,
                            maskedCardNumber: paymentRequest.maskedCardNumber,
                            cardHolderName: 'chFirstName chLastName',
                            amount: 7,
                        },
                    },
                ],
            },
        });
    });

    it('createSubmitOrderRequest: should return correct value for card on file payment', () => {
        const result = createSubmitOrderRequest({
            tallyOrder: ({
                ...tallyOrder,
                total: 15,
            } as unknown) as TallyResponseModel,
            customerInfo: customerInfo as ICustomerInfo,
            paymentRequest: { ...paymentRequest, paymentType: 'CARD_ON_FILE', cardToken: 'cardToken' },
            paymentInfo: paymentInfo as IPaymentState,
            location: location as any,
            orderTime: null,
            driverTip: 5,
        });
        expect(result).toMatchObject({
            orderRequestModel: {
                payments: [
                    {
                        type: PaymentTypeModel.Wallet,
                        details: {
                            keys: paymentRequest.paymentKeys,
                            sessionKey: paymentRequest.sessionKey,
                            cardToken: 'cardToken',
                            amount: 20,
                        },
                    },
                ],
            },
        });
    });

    describe('createSubmitOrderRequest: should, return correct value for multiple gift cards and credit card payment', () => {
        it('when gift cards balance is enough', () => {
            const result = createSubmitOrderRequest({
                tallyOrder: ({
                    ...tallyOrder,
                    total: 15,
                } as unknown) as TallyResponseModel,
                customerInfo: customerInfo as ICustomerInfo,
                giftCards: [giftCard as IGiftCard, giftCard as IGiftCard],
                paymentRequest: paymentRequest as IPaymentRequest,
                paymentInfo: paymentInfo as IPaymentState,
                location: location as any,
                orderTime: null,
            });

            expect(result).toMatchObject({
                orderRequestModel: {
                    payments: [
                        {
                            type: PaymentTypeModel.GiftCard,
                            details: {
                                amount: 8,
                            },
                        },
                        {
                            type: PaymentTypeModel.GiftCard,
                            details: {
                                amount: 7,
                            },
                        },
                        {
                            type: PaymentTypeModel.Card,
                            details: {
                                amount: 0,
                            },
                        },
                    ],
                },
            });
        });

        it('when gift cards balance is exactly enough', () => {
            const result = createSubmitOrderRequest({
                tallyOrder: ({
                    ...tallyOrder,
                    total: 16,
                } as unknown) as TallyResponseModel,
                customerInfo: customerInfo as ICustomerInfo,
                giftCards: [giftCard as IGiftCard, giftCard as IGiftCard],
                paymentRequest: paymentRequest as IPaymentRequest,
                paymentInfo: paymentInfo as IPaymentState,
                location: location as any,
                orderTime: null,
            });

            expect(result).toMatchObject({
                orderRequestModel: {
                    payments: [
                        {
                            type: PaymentTypeModel.GiftCard,
                            details: {
                                amount: 8,
                            },
                        },
                        {
                            type: PaymentTypeModel.GiftCard,
                            details: {
                                amount: 8,
                            },
                        },
                        {
                            type: PaymentTypeModel.Card,
                            details: {
                                amount: 0,
                            },
                        },
                    ],
                },
            });
        });

        it('when gift cards balance is not enough', () => {
            const result = createSubmitOrderRequest({
                tallyOrder: ({
                    ...tallyOrder,
                    total: 25,
                } as unknown) as TallyResponseModel,
                customerInfo: customerInfo as ICustomerInfo,
                giftCards: [giftCard as IGiftCard, giftCard as IGiftCard],
                paymentRequest: paymentRequest as IPaymentRequest,
                paymentInfo: paymentInfo as IPaymentState,
                location: location as any,
                orderTime: null,
            });

            expect(result).toMatchObject({
                orderRequestModel: {
                    payments: [
                        {
                            type: PaymentTypeModel.GiftCard,
                            details: {
                                amount: 8,
                            },
                        },
                        {
                            type: PaymentTypeModel.GiftCard,
                            details: {
                                amount: 8,
                            },
                        },
                        {
                            type: PaymentTypeModel.Card,
                            details: {
                                amount: 9,
                            },
                        },
                    ],
                },
            });
        });
    });

    describe('createDineInSubmitOrderRequest', () => {
        const mock = {
            orderInfo: {
                orderId: 'id',
                subTotalAfterDiscounts: 11.22,
                tax: 1.1,
            },
            paymentRequest: {
                chFirstName: 'fname',
                chLastName: 'lname',
                paymentKeys: ['key'],
                sessionKey: 'sessionkey',
                cardIssuer: 'Visa',
                maskedCardNumber: '41111111XXXX1111',
            },
            deviceId: 'deviceid',
            tipAmount: 2.3,
        } as any;

        const giftCardMock1 = {
            balance: 15.0,
            paymentToken: {
                keys: ['key'],
                cardIssuer: 'Unknown',
                maskedCardNumber: '41111111XXXX1111',
            },
            sessionKey: 'sessionkey',
        };
        const giftCardMock2 = {
            balance: 15.0,
            paymentToken: {
                keys: ['key1'],
                cardIssuer: 'Unknown',
                maskedCardNumber: '41111111XXXX1112',
            },
            sessionKey: 'sessionkey1',
        };

        it('should send correct payload for credit card payment', () => {
            expect(createDineInSubmitOrderRequest(mock)).toEqual({
                amount: mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax,
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(
                            mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax + mock.tipAmount
                        ),
                        cardHolderName: `${mock.paymentRequest.chFirstName} ${mock.paymentRequest.chLastName}`,
                        cardIssuer: mock.paymentRequest.cardIssuer,
                        maskedCardNumber: mock.paymentRequest.maskedCardNumber,
                        paymentKeys: mock.paymentRequest.paymentKeys,
                        sessionKey: mock.paymentRequest.sessionKey,
                        type: 'CREDIT',
                    },
                ],
                tipAmount: mock.tipAmount,
            });
        });

        it('should send correct payload for credit card payment without cardHolderName', () => {
            expect(
                createDineInSubmitOrderRequest({
                    ...mock,
                    paymentRequest: { ...mock.paymentRequest, chFirstName: '', chLastName: '' },
                })
            ).toEqual({
                amount: mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax,
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(
                            mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax + mock.tipAmount
                        ),
                        cardIssuer: mock.paymentRequest.cardIssuer,
                        maskedCardNumber: mock.paymentRequest.maskedCardNumber,
                        paymentKeys: mock.paymentRequest.paymentKeys,
                        sessionKey: mock.paymentRequest.sessionKey,
                        type: 'CREDIT',
                    },
                ],
                tipAmount: mock.tipAmount,
            });
        });

        it('should send correct payload for credit card payment without payment info', () => {
            expect(
                createDineInSubmitOrderRequest({
                    ...mock,
                    paymentRequest: null,
                })
            ).toEqual({
                amount: getFloatNumber(mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax),
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [],
                tipAmount: mock.tipAmount,
            });
        });

        it('should send correct payload for credit card payment without tips', () => {
            expect(
                createDineInSubmitOrderRequest({
                    ...mock,
                    tipAmount: undefined,
                })
            ).toEqual({
                amount: mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax,
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax),
                        cardIssuer: mock.paymentRequest.cardIssuer,
                        maskedCardNumber: mock.paymentRequest.maskedCardNumber,
                        cardHolderName: `${mock.paymentRequest.chFirstName} ${mock.paymentRequest.chLastName}`,
                        paymentKeys: mock.paymentRequest.paymentKeys,
                        sessionKey: mock.paymentRequest.sessionKey,
                        type: 'CREDIT',
                    },
                ],
            });
        });

        it('should send correct payload for payment by gift card', () => {
            expect(createDineInSubmitOrderRequest({ ...mock, giftCards: [giftCardMock1] })).toEqual({
                amount: mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax,
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(
                            mock.orderInfo.subTotalAfterDiscounts + mock.orderInfo.tax + mock.tipAmount
                        ),
                        cardIssuer: giftCardMock1.paymentToken.cardIssuer,
                        maskedCardNumber: giftCardMock1.paymentToken.maskedCardNumber,
                        paymentKeys: giftCardMock1.paymentToken.keys,
                        sessionKey: giftCardMock1.sessionKey,
                        type: 'GIFTCARD',
                    },
                ],
                tipAmount: mock.tipAmount,
            });
        });

        it('should send correct payload for payment by two gift cards', () => {
            const subtotal = 16.44;
            expect(
                createDineInSubmitOrderRequest({
                    ...mock,
                    orderInfo: { ...mock.orderInfo, subTotalAfterDiscounts: subtotal },
                    giftCards: [giftCardMock1, giftCardMock2],
                })
            ).toEqual({
                amount: getFloatNumber(subtotal + mock.orderInfo.tax),
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(giftCardMock1.balance),
                        cardIssuer: giftCardMock1.paymentToken.cardIssuer,
                        maskedCardNumber: giftCardMock1.paymentToken.maskedCardNumber,
                        paymentKeys: giftCardMock1.paymentToken.keys,
                        sessionKey: giftCardMock1.sessionKey,
                        type: 'GIFTCARD',
                    },
                    {
                        amount: getFloatNumber(subtotal + mock.orderInfo.tax + mock.tipAmount - giftCardMock1.balance),
                        cardIssuer: giftCardMock2.paymentToken.cardIssuer,
                        maskedCardNumber: giftCardMock2.paymentToken.maskedCardNumber,
                        paymentKeys: giftCardMock2.paymentToken.keys,
                        sessionKey: giftCardMock2.sessionKey,
                        type: 'GIFTCARD',
                    },
                ],
                tipAmount: mock.tipAmount,
            });
        });

        it('should send correct payload for split payment (1gc/1cc)', () => {
            const subtotal = 16.44;
            expect(
                createDineInSubmitOrderRequest({
                    ...mock,
                    orderInfo: { ...mock.orderInfo, subTotalAfterDiscounts: subtotal },
                    giftCards: [giftCardMock1],
                })
            ).toEqual({
                amount: getFloatNumber(subtotal + mock.orderInfo.tax),
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(giftCardMock1.balance),
                        cardIssuer: giftCardMock1.paymentToken.cardIssuer,
                        maskedCardNumber: giftCardMock1.paymentToken.maskedCardNumber,
                        paymentKeys: giftCardMock1.paymentToken.keys,
                        sessionKey: giftCardMock1.sessionKey,
                        type: 'GIFTCARD',
                    },
                    {
                        amount: getFloatNumber(subtotal + mock.orderInfo.tax + mock.tipAmount - giftCardMock1.balance),
                        cardIssuer: mock.paymentRequest.cardIssuer,
                        maskedCardNumber: mock.paymentRequest.maskedCardNumber,
                        cardHolderName: `${mock.paymentRequest.chFirstName} ${mock.paymentRequest.chLastName}`,
                        paymentKeys: mock.paymentRequest.paymentKeys,
                        sessionKey: mock.paymentRequest.sessionKey,
                        type: 'CREDIT',
                    },
                ],
                tipAmount: mock.tipAmount,
            });
        });

        it('should send correct payload for split payment (2gc/1cc)', () => {
            const subtotal = 33.44;
            expect(
                createDineInSubmitOrderRequest({
                    ...mock,
                    orderInfo: { ...mock.orderInfo, subTotalAfterDiscounts: subtotal },
                    giftCards: [giftCardMock1, giftCardMock2],
                })
            ).toEqual({
                amount: getFloatNumber(subtotal + mock.orderInfo.tax),
                deviceId: mock.deviceId,
                orderId: mock.orderInfo.orderId,
                payments: [
                    {
                        amount: getFloatNumber(giftCardMock1.balance),
                        cardIssuer: giftCardMock1.paymentToken.cardIssuer,
                        maskedCardNumber: giftCardMock1.paymentToken.maskedCardNumber,
                        paymentKeys: giftCardMock1.paymentToken.keys,
                        sessionKey: giftCardMock1.sessionKey,
                        type: 'GIFTCARD',
                    },
                    {
                        amount: getFloatNumber(giftCardMock2.balance),
                        cardIssuer: giftCardMock2.paymentToken.cardIssuer,
                        maskedCardNumber: giftCardMock2.paymentToken.maskedCardNumber,
                        paymentKeys: giftCardMock2.paymentToken.keys,
                        sessionKey: giftCardMock2.sessionKey,
                        type: 'GIFTCARD',
                    },
                    {
                        amount: getFloatNumber(
                            subtotal +
                                mock.orderInfo.tax +
                                mock.tipAmount -
                                giftCardMock1.balance -
                                giftCardMock2.balance
                        ),
                        cardIssuer: mock.paymentRequest.cardIssuer,
                        maskedCardNumber: mock.paymentRequest.maskedCardNumber,
                        cardHolderName: `${mock.paymentRequest.chFirstName} ${mock.paymentRequest.chLastName}`,
                        paymentKeys: mock.paymentRequest.paymentKeys,
                        sessionKey: mock.paymentRequest.sessionKey,
                        type: 'CREDIT',
                    },
                ],
                tipAmount: mock.tipAmount,
            });
        });
    });
});
