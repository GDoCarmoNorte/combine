export const tallyResponseMock = {
    bagId: '72adb6e4-dad3-4d9e-971a-90c330859bc9',
    subTotalBeforeDiscounts: 15.79,
    discountsAmount: 0,
    subTotalAfterDiscounts: 15.79,
    tax: 0.32,
    total: 16.11,
    currencyCode: 'USD',
    products: [
        {
            lineItemId: 1,
            description: 'All-American Cheeseburger none',
            price: 14.79,
            productId: 'IDPSalesItem-4865',
            quantity: 1,
            discounts: [
                {
                    quantity: 1,
                    amount: 1,
                },
            ],
            modifierGroups: [
                {
                    productId: 'IDPModifierGroup-14053',
                    modifiers: [
                        {
                            productId: 'IDPModifier-21849',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                    ],
                },
                {
                    productId: 'IDPModifierGroup-14301',
                    modifiers: [
                        {
                            productId: 'IDPModifier-23919',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                        {
                            productId: 'IDPModifier-23923',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                        {
                            productId: 'IDPModifier-23924',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                        {
                            productId: 'IDPModifier-23925',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                        {
                            productId: 'IDPModifier-23920',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                        {
                            productId: 'IDPModifier-23921',
                            price: 0,
                            quantity: 1,
                            lineItemId: 1,
                        },
                    ],
                },
                {
                    productId: 'IDPModifierGroup-16051',
                    modifiers: [
                        {
                            productId: 'IDPModifier-30749',
                            price: 1,
                            quantity: 1,
                            actionCode: 'ADD',
                            modifierGroups: [
                                {
                                    productId: 'IDPModifierGroup-11513',
                                },
                            ],
                            lineItemId: 1,
                        },
                    ],
                },
            ],
            childItems: [],
        },
        {
            lineItemId: 2,
            description: 'Pepsi Bottle none',
            price: 2,
            productId: 'IDPSalesItem-4351',
            quantity: 1,
            childItems: [],
        },
    ],
    driverTip: 0,
    serverTip: 0,
    deliveryFee: 0,
    serviceFee: 0,
    surchargeFee: 0,
    fulfillment: {
        time: '2021-12-14T14:40:00.000Z',
        type: 'PickUp',
    },
    sellingChannel: 'WEBOA',
} as any;

export const bagProductsMock = [
    {
        lineItemId: 1,
        productId: 'IDPSalesItem-4865',
        quantity: 1,
        price: 14.79,
        modifierGroups: [
            {
                productId: 'IDPModifierGroup-0001',
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-0002',
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-0003',
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-12904',
                modifiers: [],
            },
            {
                productId: 'IDPModifierGroup-14053',
                modifiers: [
                    {
                        productId: 'IDPModifier-21849',
                        quantity: 1,
                        price: 0,
                    },
                ],
            },
            {
                productId: 'IDPModifierGroup-14301',
                modifiers: [
                    {
                        productId: 'IDPModifier-23919',
                        quantity: 1,
                        price: 0,
                    },
                    {
                        productId: 'IDPModifier-23923',
                        quantity: 1,
                        price: 0,
                    },
                    {
                        productId: 'IDPModifier-23924',
                        quantity: 1,
                        price: 0,
                    },
                    {
                        productId: 'IDPModifier-23925',
                        quantity: 1,
                        price: 0,
                    },
                    {
                        productId: 'IDPModifier-23920',
                        quantity: 1,
                        price: 0,
                    },
                    {
                        productId: 'IDPModifier-23921',
                        quantity: 1,
                        price: 0,
                    },
                ],
            },
            {
                productId: 'IDPModifierGroup-16051',
                modifiers: [
                    {
                        productId: 'IDPModifier-30749',
                        quantity: 1,
                        price: 1,
                        modifierGroups: [
                            {
                                productId: 'IDPModifierGroup-11513',
                                metadata: {
                                    MODIFIER_GROUP_TYPE: 'Modifications',
                                    MODIFIER_GROUP_ID: '11513',
                                },
                                isOnSideChecked: false,
                                modifiers: [],
                            },
                        ],
                    },
                ],
            },
        ],
    },
    {
        lineItemId: 2,
        productId: 'IDPSalesItem-4351',
        quantity: 1,
        price: 2,
    },
] as any;

export const mergedTallyItems = {
    bagId: '72adb6e4-dad3-4d9e-971a-90c330859bc9',
    currencyCode: 'USD',
    deliveryFee: 0,
    discountsAmount: 0,
    driverTip: 0,
    fulfillment: { time: '2021-12-14T14:40:00.000Z', type: 'PickUp' },
    products: [
        {
            discounts: [{ amount: 1, quantity: 1 }],
            lineItemId: 1,
            modifierGroups: [
                { modifiers: [], productId: 'IDPModifierGroup-0001' },
                { modifiers: [], productId: 'IDPModifierGroup-0002' },
                { modifiers: [], productId: 'IDPModifierGroup-0003' },
                { modifiers: [], productId: 'IDPModifierGroup-12904' },
                {
                    modifiers: [{ price: 0, productId: 'IDPModifier-21849', quantity: 1 }],
                    productId: 'IDPModifierGroup-14053',
                },
                {
                    modifiers: [
                        { price: 0, productId: 'IDPModifier-23919', quantity: 1 },
                        { price: 0, productId: 'IDPModifier-23923', quantity: 1 },
                        { price: 0, productId: 'IDPModifier-23924', quantity: 1 },
                        { price: 0, productId: 'IDPModifier-23925', quantity: 1 },
                        { price: 0, productId: 'IDPModifier-23920', quantity: 1 },
                        { price: 0, productId: 'IDPModifier-23921', quantity: 1 },
                    ],
                    productId: 'IDPModifierGroup-14301',
                },
                {
                    modifiers: [
                        {
                            actionCode: 'ADD',
                            modifierGroups: [
                                {
                                    isOnSideChecked: false,
                                    metadata: { MODIFIER_GROUP_ID: '11513', MODIFIER_GROUP_TYPE: 'Modifications' },
                                    modifiers: [],
                                    productId: 'IDPModifierGroup-11513',
                                },
                            ],
                            price: 1,
                            productId: 'IDPModifier-30749',
                            quantity: 1,
                        },
                    ],
                    productId: 'IDPModifierGroup-16051',
                },
            ],
            price: 14.79,
            productId: 'IDPSalesItem-4865',
            quantity: 1,
        },
        {
            lineItemId: 2,
            price: 2,
            productId: 'IDPSalesItem-4351',
            quantity: 1,
        },
    ],
    sellingChannel: 'WEBOA',
    serverTip: 0,
    serviceFee: 0,
    subTotalAfterDiscounts: 15.79,
    subTotalBeforeDiscounts: 15.79,
    surchargeFee: 0,
    tax: 0.32,
    total: 16.11,
};
