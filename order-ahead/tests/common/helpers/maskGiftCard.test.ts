import maskGiftCard from '../../../common/helpers/maskGiftCard';

describe('maskGiftCard function', () => {
    it('should mask credit card', () => {
        const creditCard = '3086500000200000025';

        expect(maskGiftCard(creditCard)).toEqual('***************0025');
    });
});
