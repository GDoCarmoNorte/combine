/* eslint-disable react/jsx-key */
import React from 'react';
import { stringReplaceToArray } from '../../../common/helpers/stringReplaceToArray';

describe('stringReplaceToArray', () => {
    it('should accept a string at the front', () => {
        const result = stringReplaceToArray('One two three', 'One', 'replaced');

        expect(result).toEqual(['replaced', ' two three']);
    });

    it('should accept a string at the middle', () => {
        const result = stringReplaceToArray('One two three', 'two', 'replaced');

        expect(result).toEqual(['One ', 'replaced', ' three']);
    });

    it('should accept a string at the end', () => {
        const result = stringReplaceToArray('One two three', 'three', 'replaced');

        expect(result).toEqual(['One two ', 'replaced']);
    });

    it('should replace all occurrences of a regex pattern', () => {
        const result = stringReplaceToArray('One two three three two one', /two/, 'replaced');

        expect(result).toEqual(['One ', 'replaced', ' three three ', 'replaced', ' one']);
    });

    it('should replace to object', () => {
        const result = stringReplaceToArray('One two three', 'three', { obj: true });

        expect(result).toEqual(['One two ', { obj: true }]);
    });

    it('should replace to JSX', () => {
        const result = stringReplaceToArray('One two three', 'three', <div>three</div>);

        expect(result).toEqual(['One two ', <div>three</div>]);
    });
});
