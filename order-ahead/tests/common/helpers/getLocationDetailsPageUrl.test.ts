import { createNextState } from '@reduxjs/toolkit';
import getLocationDetailsPageUrl from '../../../common/helpers/getLocationDetailsPageUrl';
import { LocationWithDetailsModel } from '../../../common/services/locationService/types';
import { isLocationPagesOn } from '../../../lib/getFeatureFlags';
import locationsMock from '../../mocks/expLocations.mock';

jest.mock('../../../lib/getFeatureFlags', () => ({
    isLocationPagesOn: jest.fn().mockReturnValue(true),
}));

describe('getLocationDetailsPageUrl', () => {
    const rioEnvVar = process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN;
    const locationMock = createNextState(locationsMock.locations[0] as LocationWithDetailsModel, (state) => {
        state.url = 'us/oh/Chardon/417-water-st/store-5398';
    });

    beforeAll(() => {
        process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = 'undefined';
    });

    afterAll(() => {
        process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = rioEnvVar;
    });

    it('should return url', () => {
        const result = getLocationDetailsPageUrl(locationMock);

        expect(result).toBe('/locations/us/oh/Chardon/417-water-st/store-5398');
    });

    it('should return null if location url is not provided', () => {
        const result = getLocationDetailsPageUrl({ ...locationMock, url: undefined });

        expect(result).toBe(null);
    });

    it('should return null if location is not provided', () => {
        const result = getLocationDetailsPageUrl(null);

        expect(result).toBe(null);
    });

    it('should return null if location store id is not provided', () => {
        const result = getLocationDetailsPageUrl({ ...locationMock, id: null });

        expect(result).toBe(null);
    });

    describe('rio pages', () => {
        const url = 'https://locations.arbys.com/';

        beforeAll(() => {
            (isLocationPagesOn as jest.Mock).mockReturnValue(false);
        });

        it('should return null if NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN env is not specified', () => {
            const result = getLocationDetailsPageUrl(locationMock);

            expect(result).toBe(null);
        });

        it('should return url', () => {
            process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = url;
            const result = getLocationDetailsPageUrl(locationMock);

            expect(result).toBe('https://locations.arbys.com/locationId/5398');
        });
    });
});
