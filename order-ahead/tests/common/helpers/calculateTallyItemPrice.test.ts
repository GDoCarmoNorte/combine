import { calculateTallyItemPrice } from '../../../common/helpers/calculateTallyItemPrice';
import { singleProductMock, mealProductMock } from '../../mocks/TallyProduct.mock';
import { TallyProductModel } from '../../../@generated/webExpApi';

describe('calculateTallyItemPrice:', () => {
    it('should be return right value for single product without modifierGroups and childItems', () => {
        const mock = {
            ...singleProductMock,
            modifierGroups: undefined,
            childItems: undefined,
        };
        const result = calculateTallyItemPrice(mock);

        expect(result).toEqual(mock.price);
    });

    it('should be return right value for single product without modifiers', () => {
        const mock = {
            ...singleProductMock,
            modifierGroups: singleProductMock.modifierGroups.map((item) => ({ ...item, modifiers: undefined })),
        };
        const result = calculateTallyItemPrice(mock);

        expect(result).toEqual(mock.price);
    });

    it('should be return right value for single product with modifierGroups', () => {
        const result = calculateTallyItemPrice(singleProductMock as TallyProductModel);

        expect(result).toEqual(7.16);
    });

    it('should be return right value for meal product without modifierGroups', () => {
        const mock = {
            ...mealProductMock,
            childItems: mealProductMock.childItems.map((item) => ({ ...item, modifierGroups: undefined })),
        };
        const result = calculateTallyItemPrice(mock);
        const expectedPrice = mock.childItems.reduce((acc, curr) => acc + curr.price, 0);

        expect(result).toEqual(expectedPrice);
    });

    it('should be return right value for meal product with modifierGroups', () => {
        const result = calculateTallyItemPrice(mealProductMock as TallyProductModel);

        expect(result).toEqual(10.57);
    });

    it('should be return right value for single product that has a modifier quantity other than 1', () => {
        const mockWithQuantity = {
            ...singleProductMock,
            modifierGroups: singleProductMock.modifierGroups.map((item) => ({
                ...item,
                modifiers: item.modifiers.map((modifier) => ({ ...modifier, quantity: 2 })),
            })),
        };
        const result = calculateTallyItemPrice(mockWithQuantity as TallyProductModel);

        expect(result).toEqual(8.93);
    });

    it('should be return right value for several identical products', () => {
        const mockWithQuantity = {
            ...singleProductMock,
            quantity: 2,
        };
        const result = calculateTallyItemPrice(mockWithQuantity as TallyProductModel);

        expect(result).toEqual(14.32);
    });
});
