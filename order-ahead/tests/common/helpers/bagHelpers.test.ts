import { Dictionary } from '@reduxjs/toolkit';

import { ItemModel, TallyModifierGroupModel, TallyProductModel } from '../../../@generated/webExpApi';
import {
    areAllProductsAvailable,
    checkIsUnavailabeProduct,
    compareModifierGroups,
    compareTallyItems,
    findItemIndexInBag,
    getAvailableEntries,
    getBagItemSizeLabel,
    isAvailableProductByTally,
    isItemUnavailableByModifiers,
    isItemWithUnavailableModifires,
    isItemWithUnavailableSubModifires,
    isWholeItemUnavailable,
    isWholeItemUnavailableByModifiers,
    removeUnavailableModifiers,
} from '../../../common/helpers/bagHelper';
import tallyMock from '../../mocks/tallyItem.mock';
import mockBagRenderItem from '../../mocks/bagRenderItem.mock';

describe('bagHelpers', () => {
    describe('getBagItemSizeLabel function', () => {
        it('should return empty string for product without name property or sizes', () => {
            const label = getBagItemSizeLabel();
            const noneSizeLabel = getBagItemSizeLabel('None');

            expect(label).toEqual('');
            expect(noneSizeLabel).toEqual('');
        });

        it('should return empty string for products whose have item size none', () => {
            const noneSizeLabel = getBagItemSizeLabel('none');
            expect(noneSizeLabel).toEqual('');
        });

        it('should return correct label for product with Small, Medium, Large sizes', () => {
            const smLabel = getBagItemSizeLabel('Small');
            const mdLabel = getBagItemSizeLabel('Medium');
            const lgLabel = getBagItemSizeLabel('Large');

            expect(smLabel).toEqual('S');
            expect(mdLabel).toEqual('M');
            expect(lgLabel).toEqual('L');
        });

        it('should return correct label for product with custom size names', () => {
            const label = getBagItemSizeLabel('3 PC');

            expect(label).toEqual('3 PC');
        });
    });

    describe('getAvailableEntries function', () => {
        it('should return only entries which has isAvailable as true', () => {
            const bagEntries = [{ productId: '1' }, { productId: '2' }, { productId: '3' }] as TallyProductModel[];
            const domainProducts = {
                '1': { availability: { isAvailable: true } },
                '2': { availability: { isAvailable: false } },
                '3': { availability: { isAvailable: true } },
            } as Dictionary<ItemModel>;

            const result = getAvailableEntries(bagEntries, domainProducts, []);

            expect(result).toEqual([{ productId: '1' }, { productId: '3' }]);
        });
    });

    describe('areAllProductsAvailable function', () => {
        it('should return true if all products has isAvailable as true', () => {
            const bagEntries = [{ productId: '1' }, { productId: '2' }, { productId: '3' }] as TallyProductModel[];
            const domainProducts = {
                '1': { availability: { isAvailable: true } },
                '2': { availability: { isAvailable: true } },
                '3': { availability: { isAvailable: true } },
            } as Dictionary<ItemModel>;

            const result = areAllProductsAvailable(bagEntries, domainProducts, []);

            expect(result).toEqual(true);
        });

        it('should return false if some product has isAvailable as false', () => {
            const bagEntries = [{ productId: '1' }, { productId: '2' }, { productId: '3' }] as TallyProductModel[];
            const domainProducts = {
                '1': { availability: { isAvailable: true } },
                '2': { availability: { isAvailable: true } },
                '3': { availability: { isAvailable: false } },
            } as Dictionary<ItemModel>;

            const result = areAllProductsAvailable(bagEntries, domainProducts, []);

            expect(result).toEqual(false);
        });
    });

    describe('compareModifierGroups function', () => {
        it('should return true if modifierGroups are equal', () => {
            const modifierGroups1: TallyModifierGroupModel[] = [
                {
                    productId: '1',
                    modifiers: [{ productId: '1', price: 1, quantity: 1 }],
                },
                // Multiple modifiers
                {
                    productId: '2',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1 },
                        { productId: '2', price: 2, quantity: 2 },
                    ],
                },
                // Shuffle keys in Modifier
                {
                    productId: '3',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1 },
                        { productId: '2', quantity: 2, price: 2 },
                    ],
                },
                // Undefined keys
                {
                    productId: '4',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1, modifierGroups: undefined },
                        { productId: '2', price: 2, quantity: 2, modifierGroups: undefined },
                    ],
                },
            ];

            const modifierGroups2: TallyModifierGroupModel[] = [
                {
                    productId: '1',
                    modifiers: [{ productId: '1', price: 1, quantity: 1 }],
                },
                {
                    productId: '2',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1 },
                        { productId: '2', price: 2, quantity: 2 },
                    ],
                },
                {
                    productId: '3',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1 },
                        { productId: '2', price: 2, quantity: 2 },
                    ],
                },
                {
                    productId: '4',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1 },
                        { productId: '2', price: 2, quantity: 2 },
                    ],
                },
            ];

            const result = compareModifierGroups(modifierGroups1, modifierGroups2);

            expect(result).toEqual(true);
        });

        it('should return false if modifierGroups are not equal', () => {
            const modifierGroups1: TallyModifierGroupModel[] = [
                {
                    productId: '1',
                    modifiers: [{ productId: '1', price: 1, quantity: 1 }],
                },
                {
                    productId: '2',
                    modifiers: [
                        { productId: '1', price: 1, quantity: 1 },
                        { productId: '2', price: 2, quantity: 2 },
                    ],
                },
            ];

            const modifierGroups2: TallyModifierGroupModel[] = [
                {
                    productId: '1',
                    modifiers: [{ productId: '1', price: 1, quantity: 1 }],
                },
                {
                    productId: '2',
                },
            ];

            const result = compareModifierGroups(modifierGroups1, modifierGroups2);

            expect(result).toEqual(false);
        });
    });

    describe('compareTallyItems function', () => {
        it('should return true if tallyItems are equal', () => {
            const tallyItem1: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
            };
            const tallyItem2: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
            };

            const result = compareTallyItems(tallyItem1, tallyItem2);

            expect(result).toEqual(true);
        });

        it('should return false if tallyItems are not equal', () => {
            const tallyItem1: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
            };
            const tallyItem2: TallyProductModel = {
                productId: '2',
                lineItemId: 2,
                price: 2,
                quantity: 2,
            };

            const result = compareTallyItems(tallyItem1, tallyItem2);

            expect(result).toEqual(false);
        });

        it('should not compare lineItemId property', () => {
            const tallyItem1: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
            };
            const tallyItem2: TallyProductModel = {
                productId: '1',
                lineItemId: 2,
                price: 1,
                quantity: 1,
            };

            const result = compareTallyItems(tallyItem1, tallyItem2);

            expect(result).toEqual(true);
        });

        it('should compare childItems', () => {
            const tallyItem1: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
                childItems: [
                    {
                        productId: '10',
                        lineItemId: 10,
                        price: 10,
                        quantity: 10,
                    },
                    {
                        productId: '20',
                        lineItemId: 20,
                        price: 20,
                        quantity: 20,
                    },
                ],
            };

            const tallyItem2: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
                childItems: [
                    {
                        productId: '10',
                        lineItemId: 10,
                        price: 10,
                        quantity: 10,
                    },
                    {
                        productId: '20',
                        lineItemId: 20,
                        price: 20,
                        quantity: 20,
                    },
                ],
            };

            const result = compareTallyItems(tallyItem1, tallyItem2);

            expect(result).toEqual(true);
        });

        it('should compare modifierGroups', () => {
            const tallyItem1: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
                modifierGroups: [
                    {
                        productId: '1',
                        modifiers: [{ productId: '1', price: 1, quantity: 1 }],
                    },
                    {
                        productId: '2',
                        modifiers: [
                            { productId: '1', price: 1, quantity: 1 },
                            { productId: '2', price: 2, quantity: 2 },
                        ],
                    },
                ],
            };

            const tallyItem2: TallyProductModel = {
                productId: '1',
                lineItemId: 1,
                price: 1,
                quantity: 1,
                modifierGroups: [
                    {
                        productId: '1',
                        modifiers: [{ productId: '1', price: 1, quantity: 1 }],
                    },
                    {
                        productId: '2',
                        modifiers: [
                            { productId: '1', price: 1, quantity: 1 },
                            { productId: '2', price: 2, quantity: 2 },
                        ],
                    },
                ],
            };

            const result = compareTallyItems(tallyItem1, tallyItem2);

            expect(result).toEqual(true);
        });
    });

    describe('findItemIndexInBag function', () => {
        it('should find correct index based productId', () => {
            const bagEntries = [{ productId: '1' }, { productId: '2' }, { productId: '3' }] as TallyProductModel[];
            const tallyItem = { productId: '2' } as TallyProductModel;

            const result = findItemIndexInBag(bagEntries, tallyItem);

            expect(result).toEqual(1);
        });

        it('should find correct index based modifierGroups', () => {
            const bagEntries = [
                { productId: '1' },
                {
                    productId: '2',
                    modifierGroups: [
                        {
                            productId: '1',
                            modifiers: [{ productId: '1' }, { productId: '2' }],
                        },
                    ],
                },
                {
                    productId: '2',
                    modifierGroups: [
                        {
                            productId: '1',
                            modifiers: [{ productId: '1' }, { productId: '2' }, { productId: '3' }],
                        },
                    ],
                },
            ] as TallyProductModel[];

            const tallyItem = {
                productId: '2',
                modifierGroups: [
                    {
                        productId: '1',
                        modifiers: [{ productId: '1' }, { productId: '2' }, { productId: '3' }],
                    },
                ],
            } as TallyProductModel;

            const result = findItemIndexInBag(bagEntries, tallyItem);

            expect(result).toEqual(2);
        });

        it('should find correct index based modifierGroups independently of items order', () => {
            const bagEntries = [
                { productId: '1' },
                {
                    productId: '2',
                    modifierGroups: [
                        {
                            productId: '1',
                            modifiers: [{ productId: '1' }, { productId: '2' }],
                        },
                    ],
                },
                {
                    productId: '2',
                    modifierGroups: [
                        {
                            productId: '1',
                            modifiers: [{ productId: '1' }, { productId: '2' }, { productId: '3' }],
                        },
                    ],
                },
            ] as TallyProductModel[];

            const tallyItem = {
                productId: '2',
                modifierGroups: [
                    {
                        productId: '1',
                        modifiers: [{ productId: '3' }, { productId: '2' }, { productId: '1' }],
                    },
                ],
            } as TallyProductModel;

            const result = findItemIndexInBag(bagEntries, tallyItem);

            expect(result).toEqual(2);
        });

        it('should find correct index based childItems', () => {
            const bagEntries = [
                { productId: '1' },
                {
                    productId: '2',
                    childItems: [
                        {
                            productId: '1',
                            modifierGroups: [
                                {
                                    productId: '1',
                                    modifiers: [{ productId: '1' }, { productId: '2' }],
                                },
                            ],
                        },
                    ],
                },
                {
                    productId: '2',
                    childItems: [
                        {
                            productId: '1',
                            modifierGroups: [
                                {
                                    productId: '1',
                                    modifiers: [{ productId: '1' }, { productId: '2' }, { productId: '3' }],
                                },
                            ],
                        },
                    ],
                },
            ] as TallyProductModel[];

            const tallyItem = {
                productId: '2',
                childItems: [
                    {
                        productId: '1',
                        modifierGroups: [
                            {
                                productId: '1',
                                modifiers: [{ productId: '1' }, { productId: '2' }, { productId: '3' }],
                            },
                        ],
                    },
                ],
            } as TallyProductModel;

            const result = findItemIndexInBag(bagEntries, tallyItem);

            expect(result).toEqual(2);
        });

        it('should return correct index for an item not represented in the bag entries', () => {
            const bagEntries = [{ productId: '1' }, { productId: '2' }, { productId: '3' }] as TallyProductModel[];
            const tallyItem = { productId: '4' } as TallyProductModel;

            const result = findItemIndexInBag(bagEntries, tallyItem);

            expect(result).toEqual(-1);
        });
    });

    describe('checkIsUnavailabeProduct', () => {
        it('Should return true if product is available', () => {
            const productCategoryIds = ['IDPSubMenu-180'];
            const unavailableCategories = ['IDPSubMenu-180', 'IDPSubMenu-100'];
            expect(checkIsUnavailabeProduct(productCategoryIds, unavailableCategories)).toEqual(true);
        });

        it('Should return false if product is not available', () => {
            const productCategoryIds = ['IDPSubMenu-180', 'IDPSubMenu-182', 'IDPSubMenu-380'];
            const unavailableCategories = ['IDPSubMenu-180', 'IDPSubMenu-100'];
            expect(checkIsUnavailabeProduct(productCategoryIds, unavailableCategories)).toEqual(false);
        });
    });

    describe('removeUnavailableModifiers', () => {
        const unavailableModifiers = ['arb-itm-006-001', 'arb-itm-006-002'];
        const unavailableSubModifiers = ['arb-itm-011-001'];
        const tallyItemMock = {
            ...tallyMock,
            modifierGroups: [
                ...tallyMock.modifierGroups.slice(0, 1),
                {
                    productId: 'arb-prg-002-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-021',
                            quantity: 1,
                            price: 2,
                            modifierGroups: [
                                {
                                    productId: 'arb-prg-001-008',
                                    modifiers: [{ productId: 'arb-itm-011-001', quantity: 1, price: 2 }],
                                },
                            ],
                        },
                    ],
                },
            ],
        };
        const expectValue = {
            quantity: 1,
            productId: 'arb-itm-000-001',
            price: 3.49,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [{ productId: 'arb-itm-006-021', quantity: 1, price: 2 }],
                },
                {
                    productId: 'arb-prg-002-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-021',
                            quantity: 1,
                            price: 2,
                            modifierGroups: [
                                {
                                    productId: 'arb-prg-001-008',
                                    modifiers: [{ productId: 'arb-itm-011-001', quantity: 1, price: 2 }],
                                },
                            ],
                        },
                    ],
                },
            ],
        };

        const expectValueForUnavailableSubmodifiers = {
            ...expectValue,
            modifierGroups: [
                {
                    productId: 'arb-prg-001-001',
                    modifiers: [{ productId: 'arb-itm-006-021', quantity: 1, price: 2 }],
                },
                {
                    productId: 'arb-prg-002-002',
                    modifiers: [
                        {
                            productId: 'arb-itm-006-021',
                            quantity: 1,
                            price: 2,
                            modifierGroups: [
                                {
                                    productId: 'arb-prg-001-008',
                                    modifiers: [],
                                },
                            ],
                        },
                    ],
                },
            ],
        };

        it('Should remove modifiers from bag Item if empty unavailableSubModifiers', () => {
            expect(removeUnavailableModifiers(tallyItemMock, unavailableModifiers, [])).toEqual(expectValue);
        });

        it('Should remove modifiers and sub modifiers from bag Item', () => {
            expect(removeUnavailableModifiers(tallyItemMock, unavailableModifiers, unavailableSubModifiers)).toEqual(
                expectValueForUnavailableSubmodifiers
            );
        });

        it("Shouldn't remove unavailable modifiers if not unavailableModifiers and unavailableSubModifiers", () => {
            expect(removeUnavailableModifiers(tallyItemMock, undefined, undefined)).toEqual(tallyItemMock);
        });
    });

    describe('unavability of modefiers, submodefiers and items by tally', () => {
        it('isItemWithUnavailableModifires should return true if there are unavailable modifiers', () => {
            const unavailableModifiers = ['IDPModifier-16409', 'IDPModifier-23901'];
            expect(isItemWithUnavailableModifires(mockBagRenderItem as any, unavailableModifiers)).toEqual(true);
        });

        it('isItemWithUnavailableModifires should return false if there are not unavailable modifiers', () => {
            const unavailableModifiers = ['IDPModifier-11111', 'IDPModifier-00000'];
            expect(isItemWithUnavailableModifires(mockBagRenderItem as any, unavailableModifiers)).toEqual(false);
        });

        it('isItemWithUnavailableSubModifires should return true if there are unavailable submodifiers', () => {
            const unavailableSubModifiers = ['IDPModifier-13117', 'IDPModifier-13118'];
            expect(isItemWithUnavailableSubModifires(mockBagRenderItem as any, unavailableSubModifiers)).toEqual(true);
        });

        it('isItemWithUnavailableSubModifires should return false if there are not unavailable submodifiers', () => {
            const unavailableSubModifiers = ['IDPModifier-11111', 'IDPModifier-00000'];
            expect(isItemWithUnavailableSubModifires(mockBagRenderItem as any, unavailableSubModifiers)).toEqual(false);
        });

        it('isItemUnavailableByModifiers should return true if item has unavailable modifiers or submodifiers', () => {
            const unavailableTallyItems = [
                {
                    productId: 'IDPSalesItem-4858',
                    lineItemId: 1,
                    modifierIds: [],
                    subModifierIds: ['IDPModifier-13117', 'IDPModifier-13118'],
                    isWholeProductUnavailable: false,
                    isDefaultModifiersUnavailable: false,
                },
            ];
            expect(isItemUnavailableByModifiers(mockBagRenderItem as any, unavailableTallyItems as any)).toEqual(true);
        });

        it('isItemUnavailableByModifiers should return true if item has unavailable default modifier', () => {
            const unavailableTallyItems = [
                {
                    productId: 'IDPSalesItem-4858',
                    lineItemId: 1,
                    modifierIds: [],
                    subModifierIds: ['IDPModifier-13117', 'IDPModifier-13118'],
                    isWholeProductUnavailable: false,
                    isDefaultModifiersUnavailable: true,
                },
            ];
            expect(isItemUnavailableByModifiers(mockBagRenderItem as any, unavailableTallyItems as any)).toEqual(true);
        });

        it('isItemUnavailableByModifiers should return false if item does not have unavailable modifiers or submodifiers or item is fully unavailable', () => {
            const unavailableTallyItems = [
                {
                    productId: 'IDPSalesItem-4858',
                    lineItemId: 1,
                    modifierIds: [],
                    subModifierIds: ['IDPModifier-13117', 'IDPModifier-13118'],
                    isWholeProductUnavailable: true,
                    isDefaultModifiersUnavailable: false,
                },
            ];
            expect(isItemUnavailableByModifiers(mockBagRenderItem as any, unavailableTallyItems as any)).toEqual(false);
        });

        it('isWholeItemUnavailable should return true if item fully unavailable', () => {
            const unavailableTallyItems = [
                {
                    productId: 'IDPSalesItem-4858',
                    lineItemId: 1,
                    modifierIds: [],
                    subModifierIds: [],
                    isWholeProductUnavailable: true,
                    isDefaultModifiersUnavailable: false,
                },
            ];
            expect(isWholeItemUnavailable(mockBagRenderItem as any, unavailableTallyItems as any)).toEqual(true);
        });

        it('isWholeItemUnavailableByModifiers should return true if item fully unavailable by modifiers', () => {
            const unavailableTallyItems = [
                {
                    productId: 'IDPSalesItem-4858',
                    lineItemId: 1,
                    modifierIds: ['IDPModifier-5874'],
                    subModifierIds: [],
                    isWholeProductUnavailable: false,
                    isDefaultModifiersUnavailable: true,
                },
            ];
            expect(isWholeItemUnavailableByModifiers(mockBagRenderItem as any, unavailableTallyItems as any)).toEqual(
                true
            );
        });

        it('isAvailableProductByTally should return true if item available', () => {
            const unavailableTallyItems = [
                {
                    productId: 'IDPSalesItem-4858',
                    lineItemId: 1,
                    modifierIds: ['IDPModifier-0000'],
                    subModifierIds: [],
                    isWholeProductUnavailable: false,
                    isDefaultModifiersUnavailable: false,
                },
            ];
            expect(isAvailableProductByTally(mockBagRenderItem as any, unavailableTallyItems as any)).toEqual(true);
        });
    });
});
