import { getChangedModifiers } from '../../../common/helpers/getChangedModifiers';

const selectedModifiers = [
    { name: 'name-1', quantity: 1, productId: 'id-1', price: 1, calories: 100, modifiers: [] },
    { name: 'name-2', quantity: 1, productId: 'id-2', price: 1, calories: 100, modifiers: [], removedModifiers: [] },
    { name: 'name-3', quantity: 1, productId: 'id-3', price: 1, calories: 100, modifiers: [], removedModifiers: [] },
];
const defaultModifiersMock = [
    { name: 'name-1', defaultQuantity: 1, productId: 'id-1', minQuantity: 1, maxQuantity: 1, price: 1, calories: 100 },
    { name: 'name-2', defaultQuantity: 1, productId: 'id-2', minQuantity: 1, maxQuantity: 1, price: 1, calories: 100 },
    { name: 'name-3', defaultQuantity: 1, productId: 'id-3', minQuantity: 1, maxQuantity: 1, price: 1, calories: 100 },
];

const selectedModifiersWithSub = [
    {
        name: 'name-3',
        quantity: 1,
        productId: 'id-3',
        price: 1,
        calories: 100,
        modifiers: [
            {
                name: 'name-4',
                quantity: 1,
                productId: 'id-4',
                price: 0,
                calories: 110,
                modifiers: [],
            },
            {
                name: 'name-5',
                quantity: 1,
                productId: 'id-5',
                price: 0,
                calories: 50,
                modifiers: [],
            },
        ],
    },
];

describe('getChangedModifiers', () => {
    it('should return empty arrays and modifiersIsChanged equal false', () => {
        const result = getChangedModifiers([], []);

        expect(result).toStrictEqual({
            addedModifiers: [],
            removedDefaultModifiers: [],
            modifiersIsChanged: false,
        });
    });

    it('should return added modifiers', () => {
        const result = getChangedModifiers(selectedModifiers, defaultModifiersMock.slice(0, -1));

        expect(result).toStrictEqual({
            addedModifiers: [selectedModifiers[2]],
            removedDefaultModifiers: [],
            modifiersIsChanged: true,
        });
    });

    it('should return added modifiers including quantity', () => {
        const selectedModifiers = [
            {
                name: 'name-1',
                quantity: 2,
                productId: 'id-1',
                price: 1,
                calories: 100,
                modifiers: [],
                removedModifiers: [],
            },
        ];
        const defaultModifiersMock = [
            {
                name: 'name-1',
                defaultQuantity: 1,
                productId: 'id-1',
                minQuantity: 1,
                maxQuantity: 1,
                price: 1,
                calories: 100,
            },
        ];

        const includeQuantity = true;

        const result = getChangedModifiers(selectedModifiers, defaultModifiersMock, null, includeQuantity);

        expect(result).toStrictEqual({
            addedModifiers: [selectedModifiers[0]],
            removedDefaultModifiers: [],
            modifiersIsChanged: true,
        });
    });

    it('should return removed modifiers', () => {
        const result = getChangedModifiers(selectedModifiers.slice(0, -1), defaultModifiersMock);

        expect(result).toStrictEqual({
            addedModifiers: [],
            removedDefaultModifiers: [defaultModifiersMock[2]],
            modifiersIsChanged: true,
        });
    });

    it('should return removed and added modifiers. compare arrays correctly', () => {
        const addedItem = {
            name: 'name-4',
            quantity: 1,
            productId: 'id-4',
            price: 1,
            calories: 200,
            modifiers: [],
            removedModifiers: [],
        };
        const result = getChangedModifiers([...selectedModifiers.slice(0, -1), addedItem], defaultModifiersMock);

        expect(result).toStrictEqual({
            addedModifiers: [addedItem],
            removedDefaultModifiers: [defaultModifiersMock[2]],
            modifiersIsChanged: true,
        });
    });

    it('should remove default sub modifiers from products', () => {
        const result = getChangedModifiers(selectedModifiersWithSub, defaultModifiersMock, [
            {
                name: 'name-5',
                defaultQuantity: 1,
                minQuantity: 0,
                maxQuantity: 7,
                productId: 'id-5',
                price: 0,
                calories: 50,
            },
        ]);

        expect(result).toStrictEqual({
            addedModifiers: [
                {
                    name: 'name-3',
                    quantity: 1,
                    productId: 'id-3',
                    price: 1,
                    calories: 100,
                    modifiers: [
                        {
                            name: 'name-4',
                            quantity: 1,
                            productId: 'id-4',
                            price: 0,
                            calories: 110,
                            modifiers: [],
                        },
                    ],
                    removedModifiers: [],
                },
            ],
            removedDefaultModifiers: defaultModifiersMock.slice(0, -1),
            modifiersIsChanged: true,
        });
    });

    it('should add removed default sub modifiers', () => {
        const defaultSubModiers = [
            {
                name: 'name-5',
                defaultQuantity: 1,
                minQuantity: 0,
                maxQuantity: 7,
                productId: 'id-5',
                price: 0,
                calories: 50,
            },
            {
                name: 'name-6',
                defaultQuantity: 1,
                minQuantity: 0,
                maxQuantity: 7,
                productId: 'id-6',
                price: 0,
                calories: 50,
            },
        ];

        const result = getChangedModifiers(selectedModifiersWithSub, defaultModifiersMock, defaultSubModiers);

        expect(result).toStrictEqual({
            addedModifiers: [
                {
                    name: 'name-3',
                    quantity: 1,
                    productId: 'id-3',
                    price: 1,
                    calories: 100,
                    modifiers: [
                        {
                            name: 'name-4',
                            quantity: 1,
                            productId: 'id-4',
                            price: 0,
                            calories: 110,
                            modifiers: [],
                        },
                    ],
                    removedModifiers: [
                        {
                            name: 'name-6',
                            defaultQuantity: 1,
                            minQuantity: 0,
                            maxQuantity: 7,
                            productId: 'id-6',
                            price: 0,
                            calories: 50,
                        },
                    ],
                },
            ],
            removedDefaultModifiers: defaultModifiersMock.slice(0, -1),
            modifiersIsChanged: true,
        });
    });
});
