import { getOrganizationSchema } from '../../../common/helpers/getOrganizationSchema';
import getBrandInfo, { BRANDS } from '../../../lib/brandInfo';

jest.mock('../../../lib/brandInfo');

describe('getOrganizationSchema', () => {
    it('should return correct data for BWW', () => {
        (getBrandInfo as jest.Mock).mockReturnValue(BRANDS.bww);

        expect(getOrganizationSchema()).toEqual({
            '@context': 'http://schema.org',
            '@type': 'Organization',
            name: 'Buffalo Wild Wings',
            url: process.env.NEXT_PUBLIC_APP_URL,
            logo: `${process.env.NEXT_PUBLIC_APP_URL}/brands/bww/logo.svg`,
            sameAs: [
                'https://www.facebook.com/BuffaloWildWings/',
                'https://twitter.com/bwwings/',
                'https://www.instagram.com/bwwings/',
                'https://www.youtube.com/user/buffalowildwings',
            ],
        });
    });

    it('should return correct data for Arbys', () => {
        (getBrandInfo as jest.Mock).mockReturnValue(BRANDS.arbys);

        expect(getOrganizationSchema()).toEqual({
            '@context': 'http://schema.org',
            '@type': 'Organization',
            name: "Arby's",
            url: process.env.NEXT_PUBLIC_APP_URL,
            logo: `${process.env.NEXT_PUBLIC_APP_URL}/brands/arbys/logo.svg`,
            sameAs: [
                'https://www.facebook.com/arbys/',
                'https://twitter.com/Arbys/',
                'https://www.instagram.com/arbys/',
                'https://www.youtube.com/user/arbys',
            ],
        });
    });
});
