import { categoryNameModify } from '../../../common/helpers/categoryNameModify';

describe('modify product category naming', () => {
    it('should return modified category name or input value', () => {
        const testCases = [
            {
                input: 'meals',
                expected: 'Meals',
            },
            {
                input: 'roast-beaf',
                expected: 'Roast Beaf',
            },
            {
                input: 'limited-time',
                expected: 'Limited Time',
            },
            {
                input: 'top-picks',
                expected: 'Top Picks',
            },
            {
                input: 'Top Picks',
                expected: 'Top Picks',
            },
            {
                input: '',
                expected: '',
            },
            {
                input: null,
                expected: null,
            },
            {
                input: undefined,
                expected: undefined,
            },
        ];

        testCases.forEach(({ input, expected }) => expect(categoryNameModify(input)).toEqual(expected));
    });
});
