import React from 'react';
import { render, screen } from '@testing-library/react';
import getRichTextOptions from '../../../common/helpers/getRichTextOptions';
import { INLINES, Block, Text, BLOCKS } from '@contentful/rich-text-types';
import { getLinkDetails } from '../../../lib/link';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';

jest.mock('../../../lib/link');

const richTextData: any = {
    data: {},
    content: [
        {
            data: {},
            content: [{ marks: [], value: 'h1', nodeType: 'text' }],
            nodeType: 'heading-1',
        },
        {
            data: {},
            content: [{ marks: [], value: 'h2', nodeType: 'text' }],
            nodeType: 'heading-2',
        },
        {
            data: {},
            content: [{ marks: [], value: 'h3', nodeType: 'text' }],
            nodeType: 'heading-3',
        },
        {
            data: {},
            content: [{ marks: [], value: 'h4', nodeType: 'text' }],
            nodeType: 'heading-4',
        },
        {
            data: {},
            content: [{ marks: [], value: 'h5', nodeType: 'text' }],
            nodeType: 'heading-5',
        },
        {
            data: {},
            content: [{ marks: [], value: 'p', nodeType: 'text' }],
            nodeType: 'paragraph',
        },
        {
            data: {},
            content: [{ marks: [], value: 'ul', nodeType: 'text' }],
            nodeType: 'unordered-list',
        },
        {
            data: {},
            content: [{ marks: [], value: 'ol', nodeType: 'text' }],
            nodeType: 'ordered-list',
        },
        {
            data: {
                uri: 'http://localhost',
            },
            content: [{ marks: [], value: 'a', nodeType: 'text' }],
            nodeType: 'hyperlink',
        },
        {
            data: {},
            content: [{ marks: [], value: 'hyperlink', nodeType: 'text' }],
            nodeType: 'hyperlink',
        },
    ],
    nodeType: BLOCKS.DOCUMENT,
};

describe('getRichTextOptions', () => {
    describe('entry hyperlink', () => {
        const node: Block = {
            data: {
                target: {},
            },
            content: [
                {
                    value: 'link text',
                    nodeType: 'text',
                    marks: [],
                    data: undefined,
                } as Text,
            ],
            nodeType: BLOCKS.DOCUMENT,
        };

        it('should return correct data', () => {
            const obj = getRichTextOptions([]);
            expect(obj.renderNode[INLINES.ENTRY_HYPERLINK]).toBeDefined();
        });

        it('should return internal link', () => {
            (getLinkDetails as jest.Mock).mockReturnValueOnce({
                href: 'https://google.com',
                isPhone: false,
                isExternal: false,
                name: 'link name',
            });

            const obj = getRichTextOptions([]);

            expect(obj.renderNode[INLINES.ENTRY_HYPERLINK](node, undefined)).toEqual(
                '<a href=https://google.com class="t-link">link text</a>'
            );
        });

        it('should return external link', () => {
            (getLinkDetails as jest.Mock).mockReturnValueOnce({
                href: 'https://google.com',
                isPhone: false,
                isExternal: true,
                name: 'link name',
            });

            const obj = getRichTextOptions([]);

            expect(obj.renderNode[INLINES.ENTRY_HYPERLINK](node, undefined)).toEqual(
                '<a href=https://google.com target="_blank" rel="noreferrer" class="t-link">link text</a>'
            );
        });

        it('should return phone link', () => {
            (getLinkDetails as jest.Mock).mockReturnValueOnce({
                href: '123-456-789',
                isPhone: true,
                isExternal: false,
                name: 'link name',
            });

            const obj = getRichTextOptions([]);

            expect(obj.renderNode[INLINES.ENTRY_HYPERLINK](node, undefined)).toEqual(
                '<a href=tel:123-456-789 class="t-link">link text</a>'
            );
        });
    });

    it('should set correct classes for rich text', () => {
        const RichText = () => (
            <div dangerouslySetInnerHTML={{ __html: documentToHtmlString(richTextData, getRichTextOptions([])) }}></div>
        );
        render(<RichText />);

        expect(screen.getByText('h1').classList[0]).toEqual('t-header-hero');
        expect(screen.getByText('h2').classList[0]).toEqual('t-header-h1');
        expect(screen.getByText('h3').classList[0]).toEqual('t-header-h2');
        expect(screen.getByText('h4').classList[0]).toEqual('t-header-h3');
        expect(screen.getByText('h5').classList[0]).toEqual('t-header-h3');
        expect(screen.getByText('p').classList[0]).toEqual('t-paragraph');
        expect(screen.getByText('ul').classList[0]).toEqual('list-unordered');
        expect(screen.getByText('ol').classList[0]).toEqual('list-ordered');
        expect(screen.getByText('a').classList[0]).toEqual('t-link');
    });
});
