import { createTallyRequest } from '../../../common/helpers/tallyHelper';
import { TallyFulfillmentTypeModel } from '../../../@generated/webExpApi';

const productsMock = [
    {
        lineItemId: 1,
        productId: 'arb-itm-000-047',
        quantity: 1,
        price: 0,
        childItems: [
            {
                lineItemId: 101,
                productId: 'arb-itm-000-045',
                quantity: 1,
                price: 4.91,
                modifierGroups: [
                    {
                        productId: 'arb-prg-001-001',
                        modifiers: [
                            {
                                productId: 'arb-itm-006-008',
                                quantity: 1,
                                price: 2,
                            },
                        ],
                    },
                    {
                        productId: 'arb-prg-001-002',
                        modifiers: [
                            {
                                productId: 'arb-itm-007-005',
                                quantity: 1,
                                price: 0.49,
                            },
                        ],
                    },
                    {
                        productId: 'arb-prg-001-003',
                        modifiers: [
                            {
                                productId: 'arb-itm-008-012',
                                quantity: 1,
                                price: 0,
                            },
                        ],
                    },
                    {
                        productId: 'arb-prg-001-004',
                        modifiers: [
                            {
                                productId: 'arb-itm-009-010',
                                quantity: 1,
                                price: 0,
                            },
                        ],
                    },
                    {
                        productId: 'arb-prg-001-005',
                        modifiers: [],
                    },
                    {
                        productId: 'arb-prg-001-006',
                        modifiers: [],
                    },
                    {
                        productId: 'arb-prg-001-008',
                        modifiers: [
                            {
                                productId: 'arb-itm-013-005',
                                quantity: 1,
                                price: 0,
                            },
                        ],
                    },
                ],
            },
            {
                lineItemId: 102,
                productId: 'arb-itm-002-002',
                quantity: 1,
                price: 2.49,
                modifierGroups: [
                    {
                        productId: 'arb-prg-001-006',
                        modifiers: [],
                    },
                    {
                        productId: 'arb-prg-001-005',
                        modifiers: [],
                    },
                ],
            },
            {
                lineItemId: 103,
                productId: 'arb-itm-003-001',
                quantity: 1,
                price: 1.99,
                modifierGroups: [
                    {
                        productId: 'arb-prg-001-011',
                        modifiers: [
                            {
                                productId: 'arb-itm-016-001',
                                quantity: 1,
                                price: 0,
                            },
                        ],
                    },
                    {
                        productId: 'arb-prg-001-012',
                        modifiers: [
                            {
                                productId: 'arb-itm-017-001',
                                quantity: 1,
                                price: 0,
                            },
                        ],
                    },
                ],
            },
        ],
    },
];

describe('tallyHelper', () => {
    describe('createTallyRequest', () => {
        test('should return correct line item ids for product and child items', () => {
            const request = createTallyRequest({
                products: productsMock,
                locationId: '1000',
                orderTime: new Date(),
                dealId: null,
                customerId: null,
                ssCorrelationId: '98af2411-54ce-4638-8bd6-faaafb747aba',
                fulfillmentType: TallyFulfillmentTypeModel.PickUp,
            });

            const parentId = request.products[0].lineItemId;
            const firstChildId = request.products[0].childItems[0].lineItemId;
            const secondChildId = request.products[0].childItems[1].lineItemId;
            const thirdChildId = request.products[0].childItems[2].lineItemId;

            expect(parentId).toEqual(1);
            expect(firstChildId).toEqual(101);
            expect(secondChildId).toEqual(102);
            expect(thirdChildId).toEqual(103);

            expect(firstChildId).not.toEqual(parentId);

            expect(secondChildId).not.toEqual(parentId);
            expect(secondChildId).not.toEqual(firstChildId);

            expect(thirdChildId).not.toEqual(parentId);
            expect(thirdChildId).not.toEqual(firstChildId);
            expect(thirdChildId).not.toEqual(secondChildId);
        });
    });
});
