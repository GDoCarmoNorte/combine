import {
    getMenuCategoryPageDescription,
    getPDPPageDescription,
    getPageDescription,
    getPageDescriptionByMetaInfo,
    getStatePageDescription,
    getCityPageDescription,
} from '../../../common/helpers/getPageDescription';

import pageEntriesMock from '../../mock-data/contentful/contentful-getEntries-pages.json';
import productEntriesMock from '../../mock-data/contentful/contentful-getEntries-product.json';
import menuCategoryPageEntriesMock from '../../mock-data/contentful/contentful-getEntries-menuCategory.json';

const cityTemplateMock =
    'Visit the Buffalo Wild Wings in {city}, {state_abbreviation} to get together with your friends, watch sports, drink beer, and eat wings.';
const stateTemplateMock = 'Stop in at any of our Buffalo Wild Wings locations in {state} to watch sports.';
const cityMock = 'City';
const stateMock = 'State';

describe('getPageDescription function', () => {
    it('should return description from CMS when available', () => {
        const pageMock = pageEntriesMock.items.find((i) => i.fields.metaDescription);
        const pageDescription = getPageDescription(pageMock as any);

        expect(pageDescription).toMatch('Meta Description');
    });
    it('should return empty string when description is not available from CMS', () => {
        const pageMock = pageEntriesMock.items.find((i) => !i.fields.metaDescription);
        const pageDescription = getPageDescription(pageMock as any);

        expect(pageDescription).toEqual('');
    });
});

describe('getPageDescriptionByMetaInfo function', () => {
    it('should return description from CMS when available', () => {
        const metaDescription = 'Meta Description';
        const pageDescription = getPageDescriptionByMetaInfo(metaDescription);

        expect(pageDescription).toMatch(metaDescription);
    });
    it('should return empty string when description is not available from CMS', () => {
        const pageDescription = getPageDescriptionByMetaInfo(undefined);

        expect(pageDescription).toEqual('');
    });
});

describe('getPDPPageDescription function', () => {
    it('should return description from CMS when available', () => {
        const productWithMetaDescription = productEntriesMock.items.find((i) => i.fields.metaDescription);
        const pageDescription = getPDPPageDescription(productWithMetaDescription as any);

        expect(pageDescription).toMatch(
            "Come by and try the Jamocha Shake or any of our other delicious dessert options at your local Arby's. Online ordering now available!"
        );
    });

    it('should return product name from CMS when description is not available', () => {
        const productWithoutMetaDescription = productEntriesMock.items.find((i) => !i.fields.metaDescription);
        const pageDescription = getPDPPageDescription(productWithoutMetaDescription as any);

        expect(pageDescription).toMatch('Test Product');
    });
});

describe('getMetuCategoryPageDescription function', () => {
    it('should return description from CMS when available', () => {
        const menuCategoryPageMock = menuCategoryPageEntriesMock.items.find((i) => i.fields.metaDescription);
        const pageDescription = getMenuCategoryPageDescription(menuCategoryPageMock as any);

        expect(pageDescription).toMatch("Arby's Top Picks");
    });

    it('should return category name from CMS when description is not available', () => {
        let menuCategoryPageMock = menuCategoryPageEntriesMock.items.find((i) => i.fields.metaDescription);

        menuCategoryPageMock = JSON.parse(JSON.stringify(menuCategoryPageMock));
        delete menuCategoryPageMock.fields.metaDescription;

        const pageDescription = getMenuCategoryPageDescription(menuCategoryPageMock as any);

        expect(pageDescription).toEqual('Top Picks');
    });
});

describe('getStatePageDescription function', () => {
    it('should return description from CMS when available', () => {
        const pageDescription = getStatePageDescription(stateTemplateMock, stateMock);

        expect(pageDescription).toMatch('Stop in at any of our Buffalo Wild Wings locations in State to watch sports.');
    });
    it('should return empty string when description is not available from CMS', () => {
        const pageDescription = getStatePageDescription(undefined, undefined);

        expect(pageDescription).toEqual('');
    });
});

describe('getCityPageDescription function', () => {
    it('should return description from CMS when available', () => {
        const pageDescription = getCityPageDescription(cityTemplateMock, stateMock, cityMock);

        expect(pageDescription).toMatch(
            'Visit the Buffalo Wild Wings in City, State to get together with your friends, watch sports, drink beer, and eat wings.'
        );
    });
    it('should return empty string when description is not available from CMS', () => {
        const pageDescription = getCityPageDescription(undefined, undefined, undefined);

        expect(pageDescription).toEqual('');
    });
});
