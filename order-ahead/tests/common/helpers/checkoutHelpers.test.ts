import { add } from '../../../common/helpers/dateTime';
import {
    addASAPFieldToRange,
    enhanceDaysWithLabel,
    enhanceTimesWithLabel,
    isInRange,
    prepareDays,
    prepareTimes,
    prepareWorkingHours,
} from '../../../common/helpers/checkoutHelpers';

const dateReferenceStart = new Date('2020-12-17T06:00:00.000Z');
const dateReferenceEnd = new Date('2020-12-17T19:00:00.000Z');

const timezone = 'America/New_York';

const openedTimeRanges = [
    {
        start: dateReferenceStart,
        end: dateReferenceEnd,
        displayEnd: dateReferenceEnd,
    },
    {
        start: add(dateReferenceStart, { days: 1 }),
        end: add(dateReferenceEnd, { days: 1 }),
        displayEnd: add(dateReferenceEnd, { days: 1 }),
    },
    {
        start: add(dateReferenceStart, { days: 2 }),
        end: add(dateReferenceEnd, { days: 2 }),
        displayEnd: add(dateReferenceEnd, { days: 2 }),
    },
    {
        start: add(dateReferenceStart, { days: 3 }),
        end: add(dateReferenceEnd, { days: 3 }),
        displayEnd: add(dateReferenceEnd, { days: 3 }),
    },
    {
        start: add(dateReferenceStart, { days: 4 }),
        end: add(dateReferenceEnd, { days: 4 }),
        displayEnd: add(dateReferenceEnd, { days: 4 }),
    },
    {
        start: add(dateReferenceStart, { days: 5 }),
        end: add(dateReferenceEnd, { days: 5 }),
        displayEnd: add(dateReferenceEnd, { days: 5 }),
    },
    {
        start: add(dateReferenceStart, { days: 6 }),
        end: add(dateReferenceEnd, { days: 6 }),
        displayEnd: add(dateReferenceEnd, { days: 6 }),
    },
    {
        start: add(dateReferenceStart, { days: 7 }),
        end: add(dateReferenceEnd, { days: 7 }),
        displayEnd: add(dateReferenceEnd, { days: 7 }),
    },
];
const RealDate = Date.now;

describe('checkout helpers', () => {
    beforeAll(() => {
        global.Date.now = jest.fn(() => new Date('2020-12-17T13:00:00.000Z').getTime());
    });

    afterAll(() => {
        global.Date.now = RealDate;
    });

    describe('prepareDays helper', () => {
        it('should return correct day range values', () => {
            const dayRange = prepareDays(openedTimeRanges, timezone);

            expect(dayRange[0]).toEqual('2020-12-17T06:00:00.000Z');
            expect(dayRange[6]).toEqual('2020-12-23T06:00:00.000Z');
        });

        it('should return correct day range length', () => {
            const dayRange = prepareDays(openedTimeRanges, timezone);

            expect(dayRange).toHaveLength(7);
        });

        it('should return correct day range when store is closed for one day', () => {
            const modifiedTimeRanges = openedTimeRanges.slice();
            modifiedTimeRanges.splice(2, 1);

            const dayRange = prepareDays(modifiedTimeRanges, timezone);

            expect(dayRange).toHaveLength(6);
        });
    });

    describe('prepareTimes helper', () => {
        it('should return correct time range values', () => {
            const timeRange = prepareTimes(openedTimeRanges, openedTimeRanges[1].start.toISOString());

            expect(timeRange[0]).toEqual('2020-12-18T06:15:00.000Z');
            expect(timeRange).toHaveLength(52);
        });

        it('should return empty array if selected day is out of range', () => {
            const timeRange = prepareTimes(openedTimeRanges, '2020-12-15T13:00:00.000Z');

            expect(timeRange).toEqual([]);
        });

        it('should return filter time range values if selected day is today', () => {
            const timeRange = prepareTimes(openedTimeRanges, openedTimeRanges[0].start.toISOString());

            expect(timeRange[0]).toEqual('2020-12-17T13:15:00.000Z');
            expect(timeRange).toHaveLength(24);
        });
    });

    describe('enhanceDaysWithLabel helper', () => {
        it('should assign correct labels', () => {
            const daysWithLabels = enhanceDaysWithLabel(
                ['2020-12-17T06:15:00.000Z', '2020-12-18T06:15:00.000Z'],
                timezone
            );

            expect(daysWithLabels[0]).toHaveProperty('label', 'Today (12/17/2020)');
            expect(daysWithLabels[1]).toHaveProperty('label', '12/18/2020');
            expect(daysWithLabels).toHaveLength(2);
        });
    });

    describe('enhanceTimesWithLabel helper', () => {
        it('should assign correct labels', () => {
            const timesWithLabels = enhanceTimesWithLabel(
                ['2020-12-17T06:00:00.000Z', '2020-12-18T06:15:00.000Z'],
                timezone
            );

            expect(timesWithLabels[0]).toHaveProperty('label', '01:00 AM');
            expect(timesWithLabels[1]).toHaveProperty('label', '01:15 AM');
            expect(timesWithLabels).toHaveLength(2);
        });
    });

    describe('prepareWorkingHours helper', () => {
        it('should return correct working hours values', () => {
            const workingHours = prepareWorkingHours(openedTimeRanges, timezone);

            expect(workingHours).toHaveLength(7);
            expect(workingHours[0]).toMatchObject({
                day: '2020-12-17T13:15:00.000Z',
                timeRange: [
                    '2020-12-17T13:15:00.000Z',
                    '2020-12-17T13:30:00.000Z',
                    '2020-12-17T13:45:00.000Z',
                    '2020-12-17T14:00:00.000Z',
                    '2020-12-17T14:15:00.000Z',
                    '2020-12-17T14:30:00.000Z',
                    '2020-12-17T14:45:00.000Z',
                    '2020-12-17T15:00:00.000Z',
                    '2020-12-17T15:15:00.000Z',
                    '2020-12-17T15:30:00.000Z',
                    '2020-12-17T15:45:00.000Z',
                    '2020-12-17T16:00:00.000Z',
                    '2020-12-17T16:15:00.000Z',
                    '2020-12-17T16:30:00.000Z',
                    '2020-12-17T16:45:00.000Z',
                    '2020-12-17T17:00:00.000Z',
                    '2020-12-17T17:15:00.000Z',
                    '2020-12-17T17:30:00.000Z',
                    '2020-12-17T17:45:00.000Z',
                    '2020-12-17T18:00:00.000Z',
                    '2020-12-17T18:15:00.000Z',
                    '2020-12-17T18:30:00.000Z',
                    '2020-12-17T18:45:00.000Z',
                    '2020-12-17T19:00:00.000Z',
                ],
            });
        });
    });

    describe('isInRange helper', () => {
        it('should return true for correct pickup time', () => {
            const workingHours = prepareWorkingHours(openedTimeRanges, timezone);
            const pickupTime = '2020-12-17T13:15:00.000Z';

            const inRange = isInRange(pickupTime, workingHours);

            expect(inRange).toBe(true);
        });

        it('should return true for incorrect pickup time', () => {
            const workingHours = prepareWorkingHours(openedTimeRanges, timezone);
            const pickupTime = '2020-12-17T12:15:00.000Z';

            const inRange = isInRange(pickupTime, workingHours);

            expect(inRange).toBe(false);
        });

        it('should return correct working hours values when the days matches in openedTimeRanges', () => {
            const openedTimeRanges = [
                {
                    start: dateReferenceStart,
                    end: dateReferenceEnd,
                    displayEnd: dateReferenceEnd,
                },
                {
                    start: new Date('2020-12-17T19:15:00.000Z'),
                    end: new Date('2020-12-17T22:00:00.000Z'),
                    displayEnd: new Date('2020-12-17T22:00:00.000Z'),
                },
                {
                    start: add(dateReferenceStart, { days: 1 }),
                    end: add(dateReferenceEnd, { days: 1 }),
                    displayEnd: add(dateReferenceEnd, { days: 1 }),
                },
                {
                    start: add(dateReferenceStart, { days: 2 }),
                    end: add(dateReferenceEnd, { days: 2 }),
                    displayEnd: add(dateReferenceEnd, { days: 2 }),
                },
            ];
            const workingHours = prepareWorkingHours(openedTimeRanges, timezone);

            expect(workingHours).toHaveLength(3);
            expect(workingHours[0]).toMatchObject({
                day: '2020-12-17T13:15:00.000Z',
                timeRange: [
                    '2020-12-17T13:15:00.000Z',
                    '2020-12-17T13:30:00.000Z',
                    '2020-12-17T13:45:00.000Z',
                    '2020-12-17T14:00:00.000Z',
                    '2020-12-17T14:15:00.000Z',
                    '2020-12-17T14:30:00.000Z',
                    '2020-12-17T14:45:00.000Z',
                    '2020-12-17T15:00:00.000Z',
                    '2020-12-17T15:15:00.000Z',
                    '2020-12-17T15:30:00.000Z',
                    '2020-12-17T15:45:00.000Z',
                    '2020-12-17T16:00:00.000Z',
                    '2020-12-17T16:15:00.000Z',
                    '2020-12-17T16:30:00.000Z',
                    '2020-12-17T16:45:00.000Z',
                    '2020-12-17T17:00:00.000Z',
                    '2020-12-17T17:15:00.000Z',
                    '2020-12-17T17:30:00.000Z',
                    '2020-12-17T17:45:00.000Z',
                    '2020-12-17T18:00:00.000Z',
                    '2020-12-17T18:15:00.000Z',
                    '2020-12-17T18:30:00.000Z',
                    '2020-12-17T18:45:00.000Z',
                    '2020-12-17T19:00:00.000Z',
                    '2020-12-17T19:30:00.000Z',
                    '2020-12-17T19:45:00.000Z',
                    '2020-12-17T20:00:00.000Z',
                    '2020-12-17T20:15:00.000Z',
                    '2020-12-17T20:30:00.000Z',
                    '2020-12-17T20:45:00.000Z',
                    '2020-12-17T21:00:00.000Z',
                    '2020-12-17T21:15:00.000Z',
                    '2020-12-17T21:30:00.000Z',
                    '2020-12-17T21:45:00.000Z',
                    '2020-12-17T22:00:00.000Z',
                ],
            });
        });
    });

    describe('addASAPFieldToRange', () => {
        it('should return correct range if selected day is today', () => {
            const timeValues = [
                { value: '2020-12-17T06:00:00.000Z', label: '01:00 AM' },
                { value: '2020-12-18T06:15:00.000Z', label: '01:15 AM' },
            ];
            const timesWithLabels = addASAPFieldToRange({
                timeRange: timeValues,
                selectedDay: '2020-12-17T06:00:00.000Z',
                timezone,
            });

            expect(timesWithLabels).toHaveLength(2);
            expect(timesWithLabels[0]).toMatchObject({
                label: 'ASAP (01:00 AM)',
                value: '2020-12-17T06:00:00.000Z',
            });
        });

        it('should return correct range if selected day is today without brackets in label', () => {
            const timeValues = [
                { value: '2020-12-17T06:00:00.000Z', label: '01:00 AM' },
                { value: '2020-12-18T06:15:00.000Z', label: '01:15 AM' },
            ];
            const timesWithLabels = addASAPFieldToRange({
                timeRange: timeValues,
                selectedDay: '2020-12-17T06:00:00.000Z',
                timezone,
                prepTime: null,
                asapWithoutBrackets: true,
            });

            expect(timesWithLabels).toHaveLength(2);
            expect(timesWithLabels[0]).toMatchObject({
                label: 'ASAP 01:00 AM',
                value: '2020-12-17T06:00:00.000Z',
            });
        });

        it('should return correct range if selected day for delivery orders', () => {
            const timeValues = [
                { value: '2020-12-17T06:00:00.000Z', label: '01:00 AM' },
                { value: '2020-12-18T06:15:00.000Z', label: '01:15 AM' },
            ];
            const timesWithLabels = addASAPFieldToRange({
                timeRange: timeValues,
                selectedDay: '2020-12-17T06:00:00.000Z',
                timezone,
                prepTime: null,
                asapWithoutBrackets: true,
                isAdditionalASAP: true,
            });

            expect(timesWithLabels).toHaveLength(3);
            expect(timesWithLabels[0]).toMatchObject({
                label: 'ASAP',
                value: null,
            });
        });

        it('should return correct range if selected day is today without brackets in label for delivery orders on the checkout page', () => {
            const timeValues = [
                { value: '2020-12-17T06:00:00.000Z', label: '01:00 AM' },
                { value: '2020-12-18T06:15:00.000Z', label: '01:15 AM' },
            ];
            const timesWithLabels = addASAPFieldToRange({
                timeRange: timeValues,
                selectedDay: '2020-12-17T06:00:00.000Z',
                timezone,
                prepTime: null,
                asapWithoutBrackets: true,
                isAdditionalASAP: true,
            });

            expect(timesWithLabels).toHaveLength(3);
            expect(timesWithLabels[0]).toMatchObject({
                label: 'ASAP',
                value: null,
            });
        });

        it('should return original range if selected day is not today', () => {
            const timeValues = [
                { value: '2020-12-17T06:00:00.000Z', label: '01:00 AM' },
                { value: '2020-12-18T06:15:00.000Z', label: '01:15 AM' },
            ];
            const timesWithLabels = addASAPFieldToRange({
                timeRange: timeValues,
                selectedDay: '2020-12-18T06:00:00.000Z',
                timezone,
            });

            expect(timesWithLabels).toHaveLength(2);
            expect(timesWithLabels).toMatchObject(timeValues);
        });

        it('should return asap with preparation time info, if prepTime is provided', () => {
            const timeValues = [
                { value: '2020-12-17T06:00:00.000Z', label: '01:00 AM' },
                { value: '2020-12-18T06:15:00.000Z', label: '01:15 AM' },
            ];
            const timesWithLabels = addASAPFieldToRange({
                timeRange: timeValues,
                selectedDay: '2020-12-17T06:00:00.000Z',
                timezone,
                prepTime: 5,
            });

            expect(timesWithLabels).toHaveLength(2);
            expect(timesWithLabels[0]).toMatchObject({
                label: 'ASAP (01:00 AM)',
                value: '2020-12-17T06:00:00.000Z',
                hint: '(Ready in ~5 minutes)',
            });
        });
    });
});
