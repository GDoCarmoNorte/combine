import {
    getHomePageTitle,
    getPdpPageTitle,
    getMenuCategoryPageTitle,
    getPageTitle,
    getLocationDetailsPageTitle,
    getStateLocationsPageTitle,
    getCityLocationsPageTitle,
} from '../../../common/helpers/getPageTitle';
import getBrandInfo from '../../../lib/brandInfo';

jest.mock('../../../lib/brandInfo');

const brandNameMock = 'Arbys';
const metaTitleMock = 'Meta title';
const productNameMock = 'Roast Chicken Bacon Swiss Sandwich';
const categoryNameMock = 'Chiken';
const locationNameMock = 'test location';
const cityTemplateMock = 'Locations in {City}, {State}';
const stateTemplateMock = 'Locations in {State}';
const cityMock = 'City';
const stateMock = 'Texas';

describe('getPageTitle:', () => {
    describe('getHomePageTitle', () => {
        it(`should return correct a string "${brandNameMock} | ${metaTitleMock}"`, () => {
            const result = getHomePageTitle(brandNameMock, metaTitleMock);
            const expectResult = `${brandNameMock} | ${metaTitleMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " | "', () => {
            const result = getHomePageTitle(undefined, undefined);

            expect(result).toEqual(' | ');
        });
    });

    describe('getPdpPageTitle', () => {
        it(`should return correct a string "${productNameMock} - Nearby For Delivery or Pick Up | ${brandNameMock}"`, () => {
            const result = getPdpPageTitle(brandNameMock, productNameMock);
            const expectResult = `${productNameMock} - Nearby For Delivery or Pick Up | ${brandNameMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " - Order Online |  Menu"', () => {
            const result = getPdpPageTitle(undefined, undefined);

            expect(result).toEqual(' - Nearby For Delivery or Pick Up | ');
        });
    });

    describe('getMenuCategoryPageTitle', () => {
        it(`should return correct a string "${categoryNameMock} and More | ${brandNameMock} Menu"`, () => {
            const result = getMenuCategoryPageTitle(brandNameMock, categoryNameMock);
            const expectResult = `${categoryNameMock} Nearby For Delivery or Pick Up | ${brandNameMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " and More |  Menu"', () => {
            const result = getMenuCategoryPageTitle(undefined, undefined);

            expect(result).toEqual(' Nearby For Delivery or Pick Up | ');
        });
    });

    describe('getPageTitle', () => {
        it(`should return correct a string "${metaTitleMock} | ${brandNameMock}"`, () => {
            const result = getPageTitle(brandNameMock, metaTitleMock);
            const expectResult = `${metaTitleMock} | ${brandNameMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " | "', () => {
            const result = getPageTitle(undefined, undefined);

            expect(result).toEqual(' | ');
        });
    });

    describe('getLocayionDetailsPageTitle', () => {
        it(`should return correct a string "${metaTitleMock} ${locationNameMock} | ${brandNameMock}"`, () => {
            (getBrandInfo as jest.Mock).mockReturnValue({ brandName: brandNameMock });
            const result = getLocationDetailsPageTitle(metaTitleMock, locationNameMock);
            const expectResult = `${metaTitleMock} | ${brandNameMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " Location Details | "', () => {
            (getBrandInfo as jest.Mock).mockReturnValue({ brandName: '' });
            const result = getLocationDetailsPageTitle(undefined, undefined);

            expect(result).toEqual('Location Details | ');
        });
    });

    describe('getStateLocationsPageTitle', () => {
        it(`should return correct a string "${brandNameMock} Locations in ${stateMock}"`, () => {
            (getBrandInfo as jest.Mock).mockReturnValue({ brandName: brandNameMock });
            const result = getStateLocationsPageTitle(stateTemplateMock, stateMock);
            const expectResult = `${brandNameMock} Locations in ${stateMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " "', () => {
            (getBrandInfo as jest.Mock).mockReturnValue({ brandName: '' });
            const result = getStateLocationsPageTitle(undefined, undefined);

            expect(result).toEqual(' ');
        });
    });

    describe('getCityLocationsPageTitle', () => {
        it(`should return correct a string "${brandNameMock} Locations in ${cityMock}, ${stateMock}"`, () => {
            (getBrandInfo as jest.Mock).mockReturnValue({ brandName: brandNameMock });
            const result = getCityLocationsPageTitle(cityTemplateMock, stateMock, cityMock);
            const expectResult = `${brandNameMock} Locations in ${cityMock}, ${stateMock}`;

            expect(result).toEqual(expectResult);
        });

        it('should return correct a string " "', () => {
            (getBrandInfo as jest.Mock).mockReturnValue({ brandName: '' });
            const result = getCityLocationsPageTitle(undefined, undefined, undefined);

            expect(result).toEqual(' ');
        });
    });
});
