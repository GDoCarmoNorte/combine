import {
    getCreditCardIssuerPrefixFromFreedomPayName,
    getCreditCardIssuerFromFreedomPayType,
} from '../../../common/helpers/creditCardIssuerMapper';

describe('creditCardIssueMapper', () => {
    it('should map freedom pay name to idp card issuer prefix correctly', () => {
        const visaResult = getCreditCardIssuerPrefixFromFreedomPayName('Visa');
        const mcResult = getCreditCardIssuerPrefixFromFreedomPayName('Mastercard');
        const americanResult = getCreditCardIssuerPrefixFromFreedomPayName('AmericanExpress');
        const discoverResult = getCreditCardIssuerPrefixFromFreedomPayName('Discover');
        const notValid = getCreditCardIssuerPrefixFromFreedomPayName('NewIssuer');

        expect(visaResult).toEqual('VI');
        expect(mcResult).toEqual('MC');
        expect(americanResult).toEqual('AX');
        expect(discoverResult).toEqual('DS');
        expect(notValid).toEqual('UN');
    });

    it('should map freedom pay type to idp card issuer name correctly', () => {
        const visaResult = getCreditCardIssuerFromFreedomPayType('VI');
        const mcResult = getCreditCardIssuerFromFreedomPayType('MC');
        const americanResult = getCreditCardIssuerFromFreedomPayType('AX');
        const discoverResult = getCreditCardIssuerFromFreedomPayType('DS');
        const japanCreditBureauResalt = getCreditCardIssuerFromFreedomPayType('JC');
        const carteBlancheResalt = getCreditCardIssuerFromFreedomPayType('CB');
        const dinersClubResult = getCreditCardIssuerFromFreedomPayType('DN');
        const notValid = getCreditCardIssuerFromFreedomPayType('MF');

        expect(visaResult).toEqual('Visa');
        expect(mcResult).toEqual('Mastercard');
        expect(americanResult).toEqual('AmericanExpress');
        expect(japanCreditBureauResalt).toEqual('JapanCreditBureau');
        expect(carteBlancheResalt).toEqual('CarteBlanche');
        expect(dinersClubResult).toEqual('DinersClub');
        expect(discoverResult).toEqual('Discover');
        expect(notValid).toEqual('');
    });
});
