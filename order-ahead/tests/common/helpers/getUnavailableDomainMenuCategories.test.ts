import { CategoryModel, IntervalTypeEnumModel } from '../../../@generated/webExpApi/models';
import {
    checkIsValidDateAndTime,
    checkIsValidDayOfWeek,
    getCurrentZonedDate,
    getParsedInt,
    getUnavailableDomainMenuCategories,
    IValidityIntervals,
} from '../../../common/helpers/getUnavailableDomainMenuCategories';

const mockValidityInterval: IValidityIntervals = {
    Date: [
        {
            value: '2011-01-01/2099-01-01',
            intervalType: IntervalTypeEnumModel.Date,
            dayOfTheWeek: 'TUESDAY',
        },
    ],
    Time: [
        {
            value: '13:00/15:00',
            intervalType: IntervalTypeEnumModel.Time,
            dayOfTheWeek: 'TUESDAY',
        },
    ],
};

const mockCategories: {
    [key: string]: CategoryModel;
} = {
    'IDPSubMenu-160': {
        id: 'IDPSubMenu-160',
        name: 'APPETIZERS',
        metadata: { IS_VISIBLE: 'TRUE' },
        availability: {
            isAvailable: true,
            validityIntervals: mockValidityInterval,
        },
    },
    'IDPSubMenu-002': {
        id: 'IDPSubMenu-002',
        name: 'CHECKBOX',
        metadata: { IS_VISIBLE: 'FALSE' },
        parentCategoryId: 'IDPSubMenu-000',
        availability: { isAvailable: true },
    },
};

describe('getUnavailableDomainMenuCategories functions', () => {
    describe('getParsedInt', () => {
        it('should works correctly', () => {
            expect(getParsedInt('10')).toEqual(10);
        });
    });

    describe('getCurrentZonedDate', () => {
        it('should works correctly with pickupTime', () => {
            expect(getCurrentZonedDate('2021-11-14T01:30:00.000Z', 'America/Chicago')).toEqual(
                new Date('2021-11-13T19:30:00.000Z')
            );
        });
    });

    describe('checkIsValidDayOfWeek', () => {
        it('should returns true if day is valid', () => {
            expect(checkIsValidDayOfWeek(['MONDAY', 'SUNDAY'], new Date('2021-11-15T12:30:00.000Z'))).toEqual(true);
        });

        it('should returns false if day is not valid', () => {
            expect(checkIsValidDayOfWeek(['MONDAY', 'SUNDAY'], new Date('2021-11-16T12:30:00.000Z'))).toEqual(false);
        });
    });

    describe('isValidDateAndTime', () => {
        it('should returns true if date and time are valid', () => {
            expect(checkIsValidDateAndTime(mockValidityInterval, new Date('2021-11-16T13:30:00.000Z'))).toEqual(true);
        });

        it('should returns false if date and time are valid', () => {
            expect(checkIsValidDateAndTime(mockValidityInterval, new Date('2021-11-16T12:30:00.000Z'))).toEqual(false);
        });

        it('should returns true if no have a validityIntervals', () => {
            expect(checkIsValidDateAndTime(undefined, new Date('2021-11-16T12:30:00.000Z'))).toEqual(true);
        });
    });

    describe('getUnavailableDomainMenuCategories', () => {
        it('should returns an empty array if all categories are available', () => {
            expect(
                getUnavailableDomainMenuCategories(mockCategories, '2021-11-16T19:30:00.000Z', 'America/Chicago')
            ).toEqual([]);
        });

        it('should returns unavailable categories', () => {
            expect(
                getUnavailableDomainMenuCategories(mockCategories, '2021-11-16T08:30:00.000Z', 'America/Chicago')
            ).toEqual(['IDPSubMenu-160']);
        });
    });
});
