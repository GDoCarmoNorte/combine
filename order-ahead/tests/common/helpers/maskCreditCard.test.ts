import maskCreditCard from '../../../common/helpers/maskCreditCard';

describe('maskCreditCard function', () => {
    it('should mask credit card', () => {
        const creditCard = '3086500000200001225';

        expect(maskCreditCard(creditCard)).toEqual('**** 1225');
    });
});
