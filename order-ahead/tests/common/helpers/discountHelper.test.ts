import {
    getDiscountAvailability,
    getDiscountType,
    getDiscountPriceByType,
    getDiscountPrice,
    DiscountType,
} from '../../../common/helpers/discountHelper';

import { resolveOpeningHours } from '../../../lib/locations';
import domainProductWithHappyHourMock from '../../mocks/domainProductHappyHour.mock';
import domainProductWithoutHappyHourMock from '../../mocks/domainProduct.mock';

jest.mock('../../../lib/locations');

describe('discountHelper', () => {
    beforeEach(() => {
        (resolveOpeningHours as jest.Mock).mockReturnValue({ isOpen: true });
    });

    describe('getDiscountAvailability function', () => {
        test('should return correct discount availability for available service', () => {
            const discountAvailability = getDiscountAvailability({} as any);

            expect(discountAvailability).toEqual({ [DiscountType.HappyHour]: true, [DiscountType.Promo]: true });
        });

        test('should return correct discount availability for unavailable service', () => {
            (resolveOpeningHours as jest.Mock).mockReturnValue({ isOpen: false });

            const discountAvailability = getDiscountAvailability({} as any);

            expect(discountAvailability).toEqual({ [DiscountType.HappyHour]: false, [DiscountType.Promo]: true });
        });
    });

    describe('getDiscountType function', () => {
        test('should return correct discount type for product with happy hour price when happy hour service is available', () => {
            const discountType = getDiscountType(domainProductWithHappyHourMock, {} as any);

            expect(discountType).toEqual(DiscountType.HappyHour);
        });

        test('should return null for product with happy hour price when happy hour service is unavailable', () => {
            (resolveOpeningHours as jest.Mock).mockReturnValue({ isOpen: false });

            const discountType = getDiscountType(domainProductWithHappyHourMock, {} as any);

            expect(discountType).toEqual(null);
        });

        test('should return null for product without happy hour price when happy hour service available', () => {
            const discountType = getDiscountType(domainProductWithoutHappyHourMock, {} as any);

            expect(discountType).toEqual(null);
        });
    });

    describe('getDiscountPriceByType function', () => {
        test('should return correct discount price for product with happy hour price', () => {
            const happyHourPrice = getDiscountPriceByType(domainProductWithHappyHourMock, DiscountType.HappyHour);

            expect(happyHourPrice).toEqual(1);
        });

        test('should return correct discount price independently on service availability', () => {
            (resolveOpeningHours as jest.Mock).mockReturnValue({ isOpen: false });

            const happyHourPrice = getDiscountPriceByType(domainProductWithHappyHourMock, DiscountType.HappyHour);

            expect(happyHourPrice).toEqual(1);
        });

        test('should return null for product without happy hour price', () => {
            const happyHourPrice = getDiscountPriceByType(domainProductWithoutHappyHourMock, DiscountType.HappyHour);

            expect(happyHourPrice).toEqual(null);
        });
    });

    describe('getDiscountPrice function', () => {
        test('should return correct discount price for product with happy hour price when happy hour service is available', () => {
            const happyHourPrice = getDiscountPrice(domainProductWithHappyHourMock, {} as any);

            expect(happyHourPrice).toEqual(1);
        });

        test('should return null for product with happy hour price when happy hour service is unavailable', () => {
            (resolveOpeningHours as jest.Mock).mockReturnValue({ isOpen: false });

            const happyHourPrice = getDiscountPrice(domainProductWithHappyHourMock, {} as any);

            expect(happyHourPrice).toEqual(null);
        });

        test('should return null for product without happy hour price', () => {
            const happyHourPrice = getDiscountPrice(domainProductWithoutHappyHourMock, {} as any);

            expect(happyHourPrice).toEqual(null);
        });
    });
});
