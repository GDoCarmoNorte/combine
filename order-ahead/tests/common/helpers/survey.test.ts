import { formatSurveyName, getDefaultSurveyRespond } from '../../../common/helpers/survey';
import { initialState } from '../../../redux/myTeams';

const mockSurvey = {
    ...initialState,
    questions: [
        {
            id: 'id',
            text: 'NFL:',
            answers: [
                {
                    id: 'answerid',
                    text: 'Arizona Cardinals',
                },
            ],
        },
        {
            id: 'id2',
            text: 'NBA:',
            answers: [
                {
                    id: 'answerid3',
                    text: 'Team Not Selected',
                    isDefault: true,
                },
            ],
        },
    ],
    replies: [],
    hasResponded: true,
};

describe('survey helpers', () => {
    describe('formatSurveyName', () => {
        it('should format survey name', () => {
            expect(formatSurveyName('NFL:')).toEqual('NFL');
        });
    });

    describe('getDefaultSurveyRespond', () => {
        it('should return respond with default teams', () => {
            expect(getDefaultSurveyRespond(mockSurvey as any, [{ questionId: 'id', answerId: 'answerid' }])).toEqual([
                { questionId: 'id2', answerId: 'answerid3' },
            ]);
        });
    });
});
