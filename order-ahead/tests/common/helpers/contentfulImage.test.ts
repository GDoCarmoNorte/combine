import { getImageUrl, getImageSrcset } from '../../../common/helpers/contentfulImage';

const imageMock = {
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '18lLHEMB2mIrRYtWYUdCtU',
        type: 'Asset',
        createdAt: '2020-09-25T18:13:21.427Z',
        updatedAt: '2020-09-25T18:13:21.427Z',
        environment: {
            sys: {
                id: 'uat-stg',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        locale: 'en-US',
    },
    fields: {
        title: 'Meals Signatures Gyro Roast Beef',
        file: {
            url: '//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png',
            details: {
                size: 2345415,
                image: {
                    width: 4000,
                    height: 3000,
                },
            },
            fileName: 'Website_Meals_Signatures_Gyro_RB.png',
            contentType: 'image/png',
        },
    },
} as any;

describe('contenful image helpers', () => {
    describe('getImageUrl', () => {
        it('should return image url', () => {
            const result = getImageUrl(imageMock, 1000);

            expect(result).toBe('//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png?w=1000');
        });

        it('should return image url with default width', () => {
            const result = getImageUrl(imageMock);

            expect(result).toBe('//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png?w=1500');
        });

        it('should return image url without width', () => {
            const result = getImageUrl(imageMock, null);

            expect(result).toBe('//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png');
        });
    });

    describe('getImageSrcset', () => {
        it('should return image url', () => {
            const result = getImageSrcset(imageMock, 1000);

            expect(result).toBe('//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png?fm=webp&w=1000');
        });

        it('should return image url with default width', () => {
            const result = getImageSrcset(imageMock);

            expect(result).toBe('//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png?fm=webp&w=1500');
        });

        it('should return image url without width', () => {
            const result = getImageSrcset(imageMock, null);

            expect(result).toBe('//images.ctfassets.net/Website_Meals_Signatures_Gyro_RB.png?fm=webp');
        });
    });
});
