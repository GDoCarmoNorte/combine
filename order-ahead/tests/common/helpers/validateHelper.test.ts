import {
    atLeastOneLowercaseValidator,
    atLeastOneNumberValidator,
    atLeastOneSymbolValidator,
    atLeastOneUppercaseValidator,
    charValidator,
    createPatternValidator,
    dateValidator,
    dateAndYearValidator,
    emailValidator,
    lowercaseValidator,
    maxLengthValidator,
    maxNumberValidator,
    minLengthValidator,
    minNumberValidator,
    numberValidator,
    optionalValidator,
    phoneValidator,
    phoneWithDashesValidator,
    requireBooleanValidator,
    requireNumberEmptyValidator,
    requireNumberValidator,
    requireValidator,
    symbolValidator,
    uppercaseValidator,
    zipCodeValidator,
} from '../../../common/helpers/validateHelper';

describe('validateHelper', () => {
    describe('createPatternValidator function', () => {
        it('should return a FieldValidator', () => {
            expect(createPatternValidator(/^[0-9]{3}$/, 'Error')('123')).toEqual(undefined);
            expect(createPatternValidator(/^[0-9]{3}$/, 'Error')('anythingElse')).toEqual('Error');
        });
    });

    describe('requireValidator function', () => {
        it(`should return error message if field is not valid`, () => {
            expect(requireValidator('Error')('')).toEqual('Error');
        });

        it('should nothing if field is valid', () => {
            expect(requireValidator('Error')('anything')).toEqual(undefined);
        });
    });

    describe('optionalValidator function', () => {
        it('should not return error if value is missing and passed validator expects non empty value', () => {
            expect(requireValidator('Error')('')).toEqual('Error');
            expect(optionalValidator(requireValidator('Error'))('')).toEqual(undefined);
        });
    });

    describe('requireBooleanValidator function', () => {
        it(`should return error message if field is not valid`, () => {
            expect(requireBooleanValidator('Error')(false)).toEqual('Error');
        });

        it('should nothing if field is valid', () => {
            expect(requireBooleanValidator('Error')(true)).toEqual(undefined);
        });
    });

    describe('requireNumberValidator function', () => {
        it('should return error message if field is not valid (null)', () => {
            expect(requireNumberValidator('Error')(null)).toEqual('Error');
        });

        it('should return error message if field is not valid (0)', () => {
            expect(requireNumberValidator('Error')(0)).toEqual('Error');
        });

        it('should nothing if field is valid', () => {
            expect(requireNumberValidator('Error')(1)).toEqual(undefined);
        });
    });

    //what part of this validates an empty number?
    describe('requireNumberEmptyValidator function', () => {
        it(`should return error message if field is not valid`, () => {
            expect(requireNumberEmptyValidator()(null)).toEqual(' ');
        });

        it('should return error message if field is not valid (0)', () => {
            expect(requireNumberEmptyValidator()(0)).toEqual(' ');
        });

        it('should return error message if field is not valid (1)', () => {
            expect(requireNumberEmptyValidator()(1)).toEqual(undefined);
        });
    });

    describe('maxNumberValidator function', () => {
        it(`should return error message if field is not valid`, () => {
            expect(maxNumberValidator(1, 'Error')(3)).toEqual('Error');
            expect(maxNumberValidator(-3, 'Error')(3)).toEqual('Error');
            expect(maxNumberValidator(3, 'Error')(33)).toEqual('Error');
        });

        it('should nothing if field is equal (valid)', () => {
            expect(maxNumberValidator(3, 'Error')(3)).toEqual(undefined);
        });

        it('should nothing if field is valid', () => {
            expect(maxNumberValidator(3, 'Error')(1)).toEqual(undefined);
        });
    });

    describe('minNumberValidator function', () => {
        it(`should return error message if field is not valid`, () => {
            expect(minNumberValidator(5, 'Error')(3)).toEqual('Error');
            expect(minNumberValidator(33, 'Error')(-3)).toEqual('Error');
            expect(minNumberValidator(33, 'Error')(3)).toEqual('Error');
        });

        it('should nothing if field is equal (valid)', () => {
            expect(minNumberValidator(3, 'Error')(3)).toEqual(undefined);
        });

        it('should nothing if field is valid', () => {
            expect(minNumberValidator(1, 'Error')(3)).toEqual(undefined);
        });
    });

    describe('maxLengthValidator function', () => {
        it(`should return error message if maxLength is exceeded`, () => {
            expect(maxLengthValidator(3, 'ERROR')('inspirebrands@arbys.com@')).toEqual('ERROR');
        });

        it('should return nothing if maxLength is met', () => {
            expect(maxLengthValidator(3, 'ERROR')('123')).toEqual(undefined);
        });

        it('should return nothing if maxLength is not met', () => {
            expect(maxLengthValidator(3, 'ERROR')('ab')).toEqual(undefined);
        });
    });

    describe('minLengthValidator function', () => {
        it(`should return error message if min Length is not met`, () => {
            expect(minLengthValidator(3, 'ERROR')('12')).toEqual('ERROR');
        });

        it('should return nothing if minLength is met', () => {
            expect(minLengthValidator(3, 'ERROR')('123')).toEqual(undefined);
        });

        it('should return nothing if minLength is not exceeded', () => {
            expect(minLengthValidator(3, 'ERROR')('abcd')).toEqual(undefined);
        });
    });

    describe('emailValidator function', () => {
        it(`should return error message if email is not valid`, () => {
            expect(emailValidator('ERROR')('sdf@')).toEqual('ERROR');
        });

        it('should return "Alpha characters only"', () => {
            expect(emailValidator('ERROR')('inspirebrands@arbys.com')).toEqual(undefined);
        });
    });

    describe('phoneWithDashesValidator function', () => {
        it(`should return error message if phone number is not valid`, () => {
            expect(phoneWithDashesValidator('ERROR')('sdf')).toEqual('ERROR');
            expect(phoneWithDashesValidator('ERROR')('123')).toEqual('ERROR');
            expect(phoneWithDashesValidator('ERROR')('999999999999999')).toEqual('ERROR');
            expect(phoneWithDashesValidator('ERROR')('')).toEqual('ERROR');
        });

        it(`should return error message if phone number does not have dashes`, () => {
            expect(phoneWithDashesValidator('ERROR')('1324567890')).toEqual('ERROR');
        });

        it(`should return nothing if phone number is  valid`, () => {
            expect(phoneWithDashesValidator('ERROR')('123-456-7890')).toEqual(undefined);
        });
    });

    describe('phoneValidator function', () => {
        it(`should return error message if phone number is not valid`, () => {
            expect(phoneValidator('ERROR')('sdf')).toEqual('ERROR');
            expect(phoneValidator('ERROR')('123')).toEqual('ERROR');
            expect(phoneValidator('ERROR')('999999999999999')).toEqual('ERROR');
            expect(phoneValidator('ERROR')('')).toEqual('ERROR');
        });

        it(`should return error message if phone number is formatted`, () => {
            expect(phoneValidator('ERROR')('132-456-7890')).toEqual('ERROR');
            expect(phoneValidator('ERROR')('(132) 456-7890')).toEqual('ERROR');
        });

        it(`should return nothing if phone number is  valid`, () => {
            expect(phoneValidator('ERROR')('1234567890')).toEqual(undefined);
        });
    });

    describe('zipCodeValidator function', () => {
        it(`should return error message if zip code is not valid`, () => {
            expect(zipCodeValidator('ERROR')('')).toEqual('ERROR');
            expect(zipCodeValidator('ERROR')('1234')).toEqual('ERROR');
            expect(zipCodeValidator('ERROR')('1234-12345')).toEqual('ERROR');
            expect(zipCodeValidator('ERROR')('abc')).toEqual('ERROR');
        });

        it('should return nothing if zip code is valid', () => {
            expect(zipCodeValidator('ERROR')('12345')).toEqual(undefined);
            expect(zipCodeValidator('ERROR')('12345-6789')).toEqual(undefined);
        });
    });

    //does not seem to be working as intended
    describe('symbolValidator function', () => {
        it('should return nothing if only symbols are provided', () => {
            expect(symbolValidator('ERROR')('')).toEqual(undefined);
            expect(symbolValidator('ERROR')('abc')).toEqual(undefined);
        });

        it(`should return error message if given non-symbols`, () => {
            expect(symbolValidator('ERROR')('12-34')).toEqual('ERROR');
            expect(symbolValidator('ERROR')('#')).toEqual('ERROR');
            expect(symbolValidator('ERROR')('@&^#%')).toEqual('ERROR');
        });
    });

    describe('charValidator function', () => {
        it(`should return error message if given a non-character`, () => {
            expect(charValidator('ERROR')('12A34')).toEqual('ERROR');
            expect(charValidator('ERROR')('&*^%')).toEqual('ERROR');
        });

        it('should return nothing if only characters are provided', () => {
            expect(charValidator('ERROR')('')).toEqual(undefined);
            expect(charValidator('ERROR')('a')).toEqual(undefined);
            expect(charValidator('ERROR')('aLongerString')).toEqual(undefined);
        });
    });

    describe('numberValidator function', () => {
        it(`should return error message if given non-numbers`, () => {
            expect(numberValidator('ERROR')('(*7&%')).toEqual('ERROR');
            expect(numberValidator('ERROR')('abc')).toEqual('ERROR');
        });

        it('should return nothing if a number is provided', () => {
            expect(numberValidator('ERROR')('')).toEqual(undefined);
            expect(numberValidator('ERROR')('0')).toEqual(undefined);
            expect(numberValidator('ERROR')('1234567890')).toEqual(undefined);
        });
    });

    describe('dateValidator function', () => {
        it(`should return error if 00/00 is passed as value`, () => {
            expect(dateValidator('ERROR')('00/00')).toEqual('ERROR');
        });
    });

    describe('dateAndYearValidator function', () => {
        it(`should return error if 00/00/0000 is passed as value`, () => {
            expect(dateAndYearValidator('ERROR')('00/00/0000')).toEqual('ERROR');
        });
    });

    describe('uppercaseValidator function', () => {
        it(`should return error message if given non-uppercase characters`, () => {
            expect(uppercaseValidator('ERROR')('(*7&%')).toEqual('ERROR');
            expect(uppercaseValidator('ERROR')('Abc')).toEqual('ERROR');
        });

        it('should return nothing if uppercase characters are provided', () => {
            expect(uppercaseValidator('ERROR')('')).toEqual(undefined);
            expect(uppercaseValidator('ERROR')('A')).toEqual(undefined);
            expect(uppercaseValidator('ERROR')('ABCDEF GHIJK')).toEqual(undefined);
        });
    });

    describe('lowercaseValidator function', () => {
        it(`should return error message if given non-lowercase characters`, () => {
            expect(lowercaseValidator('ERROR')('(*7&%')).toEqual('ERROR');
            expect(lowercaseValidator('ERROR')('Abc')).toEqual('ERROR');
        });

        it('should return nothing if a lowercase characters are provided', () => {
            expect(lowercaseValidator('ERROR')('')).toEqual(undefined);
            expect(lowercaseValidator('ERROR')('a')).toEqual(undefined);
            expect(lowercaseValidator('ERROR')('abc def')).toEqual(undefined);
        });
    });

    describe('atLeastOneNumberValidator function', () => {
        it(`should return error message if given non-numbers`, () => {
            expect(atLeastOneNumberValidator('ERROR')('')).toEqual('ERROR');
            expect(atLeastOneNumberValidator('ERROR')('(*&%')).toEqual('ERROR');
            expect(atLeastOneNumberValidator('ERROR')('abc')).toEqual('ERROR');
        });

        it('should return nothing if a number is provided', () => {
            expect(atLeastOneNumberValidator('ERROR')('0')).toEqual(undefined);
            expect(atLeastOneNumberValidator('ERROR')('b123-456A')).toEqual(undefined);
            expect(atLeastOneNumberValidator('ERROR')('1234567890')).toEqual(undefined);
        });
    });

    describe('atLeastOneUppercaseValidator function', () => {
        it(`should return error message if given non-uppercase characters`, () => {
            expect(atLeastOneUppercaseValidator('ERROR')('')).toEqual('ERROR');
            expect(atLeastOneUppercaseValidator('ERROR')('(*&%')).toEqual('ERROR');
            expect(atLeastOneUppercaseValidator('ERROR')('abc')).toEqual('ERROR');
        });

        it('should return nothing if an uppercase character is provided', () => {
            expect(atLeastOneUppercaseValidator('ERROR')('A')).toEqual(undefined);
            expect(atLeastOneUppercaseValidator('ERROR')('b123-456A')).toEqual(undefined);
            expect(atLeastOneUppercaseValidator('ERROR')('ABCDEFGH')).toEqual(undefined);
        });
    });

    describe('atLeastOneLowercaseValidator function', () => {
        it(`should return error message if given non-lowercase characters`, () => {
            expect(atLeastOneLowercaseValidator('ERROR')('')).toEqual('ERROR');
            expect(atLeastOneLowercaseValidator('ERROR')('(*&%')).toEqual('ERROR');
            expect(atLeastOneLowercaseValidator('ERROR')('ABC')).toEqual('ERROR');
        });

        it('should return nothing if a lowercase character is provided', () => {
            expect(atLeastOneLowercaseValidator('ERROR')('a')).toEqual(undefined);
            expect(atLeastOneLowercaseValidator('ERROR')('b123-456A')).toEqual(undefined);
            expect(atLeastOneLowercaseValidator('ERROR')('abcdefgh')).toEqual(undefined);
        });
    });

    describe('atLeastOneSymbolValidator function', () => {
        it(`should return error message if given non-symbols`, () => {
            expect(atLeastOneSymbolValidator('ERROR')('')).toEqual('ERROR');
            expect(atLeastOneSymbolValidator('ERROR')('12AB')).toEqual('ERROR');
            expect(atLeastOneSymbolValidator('ERROR')('abc')).toEqual('ERROR');
        });

        it('should return nothing if a symbol is provided', () => {
            expect(atLeastOneSymbolValidator('ERROR')('@')).toEqual(undefined);
            expect(atLeastOneSymbolValidator('ERROR')('b123-456A')).toEqual(undefined);
            expect(atLeastOneSymbolValidator('ERROR')('!@#$%^&*()')).toEqual(undefined);
        });
    });
});
