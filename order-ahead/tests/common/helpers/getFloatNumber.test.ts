import { getFloatNumber } from '../../../common/helpers/getFloatNumber';

describe('getFloatNumber', () => {
    it('Should get number with decimals from string', () => {
        expect(getFloatNumber('21.542', 1)).toEqual(21.5);
    });
    it('Should get number with decimals from string without the dec argument', () => {
        expect(getFloatNumber('21.542')).toEqual(21.54);
    });
    it('should get number with decimals from integer', () => {
        expect(getFloatNumber(21.542)).toEqual(21.54);
    });
});
