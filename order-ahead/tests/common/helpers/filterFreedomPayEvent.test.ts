import { filterFreedomPayEvent } from '../../../common/helpers/filterFreedomPayEvent';
import { gPayEventMock, aPayEventMock, randomEventMock } from './filterFreedomPayEvent.mock';

describe('filterFreedomPayEvent', () => {
    it('should pass the google pay event', () => {
        expect(filterFreedomPayEvent(gPayEventMock as any)).toBeTruthy();
    });
    it('should pass the apple pay event', () => {
        expect(filterFreedomPayEvent(aPayEventMock as any)).toBeTruthy();
    });
    it('should not pass the non freedom pay event', () => {
        expect(filterFreedomPayEvent(randomEventMock as any)).toBeFalsy();
    });
});
