import { getCountryCodeFromLocale } from '../../../common/helpers/locale';

describe('locale utils', () => {
    describe('getCountryCodeFromLocale', () => {
        it('should return country code from locale (CA)', () => {
            expect(getCountryCodeFromLocale('en-ca')).toBe('CA');
        });

        it('should return country code from locale (US)', () => {
            expect(getCountryCodeFromLocale('en-us')).toBe('US');
        });

        it("should return '' by default if param is empty", () => {
            expect(getCountryCodeFromLocale('')).toBe('');
        });

        it("should return '' by default if param is incorrect", () => {
            expect(getCountryCodeFromLocale('enus')).toBe('');
        });
    });
});
