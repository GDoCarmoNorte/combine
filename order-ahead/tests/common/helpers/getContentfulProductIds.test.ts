import { getContentfulProductIdsByFields } from '../../../common/helpers/getContentfulProductIdsByFields';

describe('getContentfulProductIdsByFields', () => {
    it('should extract single product id', () => {
        expect(getContentfulProductIdsByFields({ productId: '1' } as any)).toEqual(['1']);
        expect(getContentfulProductIdsByFields({ productId: '1', productIdList: [] } as any)).toEqual(['1']);
    });

    it('should extract product id list', () => {
        expect(getContentfulProductIdsByFields({ productId: '1', productIdList: ['1', '2'] } as any)).toEqual([
            '1',
            '2',
        ]);
        expect(getContentfulProductIdsByFields({ productId: '1', productIdList: ['1'] } as any)).toEqual(['1']);
        expect(getContentfulProductIdsByFields({ productIdList: ['1'] } as any)).toEqual(['1']);
    });
});
