import { IServiceTypeModel } from '../../../@generated/webExpApi';
import {
    getLocationDescription,
    getLocationNewsSectionsToExpand,
    getLocationPath,
} from '../../../common/helpers/locationHelper';
import { locationDetailsMock } from '../../components/organisms/locationDetailsPageContent/locationDetailsPageLayout.mock';
import {
    locationNewsSectionMock,
    locationNewsSectioWithoutTilesnMock,
} from '../../components/sections/locationNewsSection/locationNewsSection.mock';
import { locationDescriptionMapping } from '../../mocks/expLocations.mock';
import { locationDataMock } from '../../components/organisms/LocationHeader/locationHeader.mock';

describe('location helper', () => {
    describe('getLocationDescription', () => {
        it(`should return generic description by default`, () => {
            const locationMock = {
                ...locationDetailsMock,
                services: [],
            };
            const result = getLocationDescription(locationMock, locationDescriptionMapping);
            expect(result).toEqual(locationDescriptionMapping.GENERAL_LOCATION);
        });
        it(`should return specific location description if location is not general`, () => {
            const locationMock = {
                ...locationDetailsMock,
                services: [IServiceTypeModel.ExpressLocation],
            };
            const result = getLocationDescription(locationMock, locationDescriptionMapping);
            expect(result).toEqual(locationDescriptionMapping.EXPRESS_LOCATION);
        });
        it('should return undefined if there is no description', () => {
            const locationMock = {
                ...locationDetailsMock,
                services: [],
            };
            const result = getLocationDescription(locationMock, null);
            expect(result).toBeUndefined();
        });
        it('should return generic description if there are no location services', () => {
            const locationMock = {
                ...locationDetailsMock,
                services: null,
            };
            const result = getLocationDescription(locationMock, locationDescriptionMapping);
            expect(result).toEqual(locationDescriptionMapping.GENERAL_LOCATION);
        });
    });
    describe('getLocationNewsSectionsToExpand', () => {
        it(`should return empty array if no sections`, () => {
            const result = getLocationNewsSectionsToExpand(undefined);
            expect(result.length).toEqual(0);
        });
        it(`should return empty array if more then one section specified`, () => {
            const result = getLocationNewsSectionsToExpand([
                locationNewsSectionMock,
                locationNewsSectioWithoutTilesnMock,
            ] as any);
            expect(result.length).toEqual(0);
        });
        it(`should return section id if only one section specified`, () => {
            const result = getLocationNewsSectionsToExpand([locationNewsSectioWithoutTilesnMock as any]);
            expect(result.length).toEqual(1);
            expect(result[0]).toEqual('016Gxb8Lfh7fzOIEX84QYQ');
        });
    });

    describe('getLocationPath', () => {
        it('should return empty string if there is no url', () => {
            const result = getLocationPath(null);

            expect(result).toBe('');
        });

        it('should return location path', () => {
            const result = getLocationPath(locationDataMock.url);
            const paths = locationDataMock.url.split('/');
            const expectedResult = `${paths[0]}/${paths[1]}/${paths[2]}`;

            expect(result).toBe(expectedResult);
        });
    });
});
