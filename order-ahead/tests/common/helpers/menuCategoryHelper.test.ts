import {
    getContentfulMenuCategoryIdsByFields,
    getDomainMenuCategoryIdByIds,
    isMenuCategoryUnavailable,
} from '../../../common/helpers/menuCategoryHelper';

describe('menuCategoryHelper', () => {
    it('getContentfulMenuCategoryIdsByFields: should correctly extract contentful ids', () => {
        expect(
            getContentfulMenuCategoryIdsByFields({ categoryId: 'singleid', categoryIdList: ['idlist'] } as any)
        ).toEqual(['idlist']);

        expect(
            getContentfulMenuCategoryIdsByFields({ categoryId: 'singleid', categoryIdList: undefined } as any)
        ).toEqual(['singleid']);

        expect(
            getContentfulMenuCategoryIdsByFields({ categoryId: 'singleid', categoryIdList: undefined } as any)
        ).toEqual(['singleid']);
    });

    it('getDomainMenuCategoryIdByIds: should correctly extract domain category id', () => {
        expect(getDomainMenuCategoryIdByIds(['1', '2'], { '2': {} })).toEqual('2');
        expect(getDomainMenuCategoryIdByIds(['1', '2'], {})).toEqual(null);
    });

    it('isMenuCategoryUnavailable: should correctly handle unavailable categories', () => {
        expect(isMenuCategoryUnavailable({ fields: { categoryIdList: ['1'] } } as any, ['1'], { '1': {} })).toEqual(
            true
        );

        expect(isMenuCategoryUnavailable({ fields: { categoryIdList: ['1'] } } as any, [], { '1': {} })).toEqual(false);
    });
});
