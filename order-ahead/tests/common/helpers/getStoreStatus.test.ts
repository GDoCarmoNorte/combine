import { getStoreStatus } from '../../../common/helpers/getStoreStatus';
import { locationDetailsMock } from '../../mocks/locationDetails.mock';

describe('getStoreStatus', () => {
    beforeEach(() => {
        jest.restoreAllMocks();
    });

    it('should return null if there is no timezone', () => {
        const result = getStoreStatus(null, null);

        expect(result).toBeNull();
    });

    it('should return null if there is no hoursByDay', () => {
        const result = getStoreStatus(null, null);

        expect(result).toBeNull();
    });

    it('should return correct statusText and isClosingSoon values if store is open 24h', () => {
        const hoursData = {
            isOpen: true,
            openTime: '9:00',
            closeTime: '23:00',
            isTwentyFourHourService: true,
            isClosingSoon: false,
        };
        const expectedResult = { statusText: 'Open 24H', isClosingSoon: hoursData.isClosingSoon };

        jest.spyOn(require('../../../lib/locations/resolveOpeningHours'), 'getHoursWithTimezone').mockReturnValue(
            hoursData
        );

        const result = getStoreStatus(locationDetailsMock.hoursByDay, locationDetailsMock.timezone);

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return correct statusText and isClosingSoon values if store is open', () => {
        const hoursData = {
            isOpen: true,
            openTime: '9:00',
            closeTime: '23:00',
            isTwentyFourHourService: false,
            isClosingSoon: false,
        };
        const expectedResult = {
            statusText: `Open Now - Closes ${hoursData.closeTime}`,
            isClosingSoon: hoursData.isClosingSoon,
        };

        jest.spyOn(require('../../../lib/locations/resolveOpeningHours'), 'getHoursWithTimezone').mockReturnValue(
            hoursData
        );

        const result = getStoreStatus(locationDetailsMock.hoursByDay, locationDetailsMock.timezone);

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return correct statusText and isClosingSoon values if store is open and closing soon', () => {
        const hoursData = {
            isOpen: true,
            openTime: '9:00',
            closeTime: '23:00',
            isTwentyFourHourService: false,
            isClosingSoon: true,
        };
        const expectedResult = {
            statusText: `Open Now - Closes ${hoursData.closeTime}`,
            isClosingSoon: hoursData.isClosingSoon,
        };

        jest.spyOn(require('../../../lib/locations/resolveOpeningHours'), 'getHoursWithTimezone').mockReturnValue(
            hoursData
        );

        const result = getStoreStatus(locationDetailsMock.hoursByDay, locationDetailsMock.timezone);

        expect(result).toStrictEqual(expectedResult);
    });

    it('should return correct statusText and isClosingSoon values if store is closed', () => {
        const hoursData = {
            isOpen: false,
            openTime: '9:00',
            closeTime: '23:00',
            isTwentyFourHourService: false,
            isClosingSoon: false,
        };
        const expectedResult = {
            statusText: `Closed Now - Opens ${hoursData.openTime}`,
            isClosingSoon: hoursData.isClosingSoon,
        };

        jest.spyOn(require('../../../lib/locations/resolveOpeningHours'), 'getHoursWithTimezone').mockReturnValue(
            hoursData
        );

        const result = getStoreStatus(locationDetailsMock.hoursByDay, locationDetailsMock.timezone);

        expect(result).toStrictEqual(expectedResult);
    });
});
