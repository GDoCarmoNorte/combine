import { calculateAge } from '../../../common/helpers/calculateAge';

jest.useFakeTimers('modern');
jest.setSystemTime(new Date('2020-10-10T00:00:00.000Z'));

describe('calculateAge', () => {
    it('should return correct age value', () => {
        const testCases = [
            {
                input: new Date('2020-10-10T00:00:00.000Z'),
                expected: 0,
            },
            {
                input: new Date('2002-10-10T00:00:00.000Z'),
                expected: 18,
            },
            {
                input: new Date('2003-10-10T00:00:00.000Z'),
                expected: 17,
            },
            {
                input: new Date('2001-10-10T00:00:00.000Z'),
                expected: 19,
            },
            {
                input: new Date('2002-09-10T00:00:00.000Z'),
                expected: 18,
            },
            {
                input: new Date('2002-11-10T00:00:00.000Z'),
                expected: 17,
            },
            {
                input: new Date('2002-10-09T00:00:00.000Z'),
                expected: 18,
            },
            {
                input: new Date('2002-10-12T00:00:00.000Z'),
                expected: 17,
            },
            {
                input: new Date('1955-11-05T00:00:00.000Z'),
                expected: 64,
            },
            {
                input: new Date('2002-10-11T00:00:00.000Z'),
                expected: 17,
            },
        ];

        testCases.forEach(({ input, expected }) => expect(calculateAge(input)).toEqual(expected));
    });
});
