import { groupSiteMapData } from '../../../common/helpers/siteMapHelper';
import globalPropsMock from '../../mocks/globalProps.mock';

const menuPageMock = {
    fields: {
        section: [
            {
                sys: {
                    contentType: {
                        sys: {
                            id: 'menuCategorySection',
                        },
                    },
                },
                fields: {
                    categories: [
                        {
                            sys: {
                                contentType: {
                                    sys: {
                                        id: 'menuCategory',
                                    },
                                },
                            },
                            fields: {
                                products: [
                                    {
                                        fields: {
                                            name: 'Curly Fries',
                                            nameInUrl: 'curly-fries',
                                            isVisible: true,
                                        },
                                    },
                                    {
                                        fields: {
                                            name: 'Curly Fries',
                                            nameInUrl: 'curly-fries-large',
                                        },
                                    },
                                ],
                                categoryName: 'Sides',
                                link: {
                                    fields: {
                                        nameInUrl: '/sides',
                                        name: 'Sides',
                                    },
                                },
                            },
                        },
                    ],
                },
            },
        ],
    },
} as any;

const siteMapCategoriesMock = {
    items: [
        {
            fields: {
                name: 'Contact Us',
                mainLink: '/contact-us',
                links: [
                    {
                        sys: {
                            contentType: {
                                sys: {
                                    id: 'socialMediaLink',
                                },
                            },
                        },
                        fields: {
                            name: 'Instagram',
                            url: 'Instagram.com',
                        },
                    },
                    {
                        sys: {
                            contentType: {
                                sys: {
                                    id: 'documentLink',
                                },
                            },
                        },
                        fields: {
                            name: 'PDF link',
                            document: {
                                fields: {
                                    file: {
                                        url: 'external.com/file.pdf',
                                    },
                                },
                            },
                        },
                    },
                    {
                        sys: {
                            contentType: {
                                sys: {
                                    id: 'externalLink',
                                },
                            },
                        },
                        fields: {
                            name: 'External',
                            nameInUrl: 'external.com',
                        },
                    },
                    {
                        sys: {
                            contentType: {
                                sys: {
                                    id: 'pageLink',
                                },
                            },
                        },
                        fields: {
                            name: 'Home',
                            nameInUrl: 'contact-us',
                        },
                    },
                ],
            },
        },
    ],
} as any;

describe('groupSiteMapData', () => {
    it('should group site map data correctly', () => {
        const result = groupSiteMapData(menuPageMock, siteMapCategoriesMock, globalPropsMock as any);

        expect(result).toMatchSnapshot();
    });
});
