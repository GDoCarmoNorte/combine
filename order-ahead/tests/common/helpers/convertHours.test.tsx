import { convertHours } from '../../../common/helpers/convertHours';

describe('convertHours', () => {
    it('should return time converted into 12h format for 00:59', () => {
        const timeString = '00:59';
        const expectedResult = '12:59AM';

        const result = convertHours(timeString);

        expect(result).toBe(expectedResult);
    });

    it('should return time converted into 12h format for 23:10', () => {
        const timeString = '23:10';
        const expectedResult = '11:10PM';

        const result = convertHours(timeString);

        expect(result).toBe(expectedResult);
    });

    it('should return empty string if time string format is wrong', () => {
        const timeString = '24:12';
        const expectedResult = '';

        const result = convertHours(timeString);

        expect(result).toBe(expectedResult);
    });

    it('should return empty string if there is no time string', () => {
        const expectedResult = '';

        const result = convertHours(null);

        expect(result).toBe(expectedResult);
    });
});
