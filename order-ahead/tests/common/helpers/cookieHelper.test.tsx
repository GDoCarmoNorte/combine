import { clearCookie, getCookieValue, setCookieValue } from '../../../common/helpers/cookieHelper';

Object.defineProperty(window.document, 'cookie', {
    writable: true,
    value: 'customerType=PC',
});

describe('cookieHelpers', () => {
    it('getCookieValue: should return correct cookie value', () => {
        expect(getCookieValue('customerType')).toEqual('PC');
    });

    it('getCookieValue: should return undefined for cookie name that does not exist', () => {
        expect(getCookieValue('badCookie')).toEqual(undefined);
    });

    it('setCookieValue: should set passed cookie value', () => {
        const cookieName = 'blazin_rewards';
        const cookieValue = 'test';
        setCookieValue(cookieName, cookieValue, 10);
        expect(getCookieValue(cookieName)).toEqual(cookieValue);
    });

    it('clearCookie: should delete cookie value', () => {
        const cookieName = 'blazin_rewards';
        const cookieValue = 'test';
        setCookieValue(cookieName, cookieValue);
        expect(getCookieValue(cookieName)).toEqual(cookieValue);

        clearCookie(cookieName);
        expect(getCookieValue(cookieName)).toEqual('');
    });
});
