import SchemaPdpAndPlp from '../../../common/helpers/schemaPdpAndPlp';
import { BRANDS } from '../../../lib/brandInfo';
import productEntriesMock from '../../mock-data/contentful/contentful-getEntries-product.json';
import productMock from '../../redux/selectors/__mocks__/product.json';

jest.mock('../../../redux/hooks/useGlobalProps', () =>
    jest.fn().mockImplementation(() => ({
        navigation: {
            items: [
                {
                    fields: {
                        name: 'Web',
                        logo: {
                            fields: {
                                file: {
                                    url:
                                        '//images.ctfassets.net/o19mhvm9a2cm/66jS9A0OMglRmNhtA52PSH/de6520ae1cf12b4a46b8d8a64d9af0cf/logo.svg',
                                },
                            },
                        },
                    },
                },
            ],
        },
    }))
);

const menuCategory = {
    fields: {
        categoryId: 'arb-cat-000-001',
        categoryName: 'Meals',
        metaDescription:
            "Choose from favorites like the Classic Beef 'N Cheddar or Chicken Bacon & Swiss sandwiches and pair with your choice of side and a drink for the ultimate meal combo.",
        image: {
            fields: {
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png',
                },
            },
        },
    },
};

const product = productEntriesMock.items.find((i) => i.fields.productId === 'arb-itm-000-018');

describe('schemaPdpAndPlp', () => {
    it('should return correct data for Arbys brand, when all data is available', async () => {
        const nutritionMock = Object.values(productMock.items).find((i) => i.id === 'arb-itm-000-018').nutrition;
        const result = SchemaPdpAndPlp(menuCategory as any, BRANDS.arbys, product as any, nutritionMock);
        const menuItem = result.hasMenu.hasMenuSection.hasMenuItem;
        const menuSection = result.hasMenu.hasMenuSection;
        const nutrition = result.hasMenu.hasMenuSection.hasMenuItem.nutrition;

        expect(result.name).toBe("Arby's");
        expect(result.description).toBe(
            "Arby's sandwich shops are known for slow roasted roast beef, turkey, and premium Angus beef sandwiches, sliced fresh every day."
        );
        expect(result.servesCuisine[0]).toBe('Fast Food Restaurant');
        expect(menuSection.name).toBe('Meals');
        expect(menuSection.description).toBe(
            "Choose from favorites like the Classic Beef 'N Cheddar or Chicken Bacon & Swiss sandwiches and pair with your choice of side and a drink for the ultimate meal combo."
        );
        expect(menuSection.image[0]).toBe(
            'https://images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png?fm=webp&w=200'
        );
        expect(menuItem.name).toBe("Double Beef 'N Cheddar");
        expect(menuItem.description).toBe(
            "Come by and try the Double Beef 'n Cheddar, or any of our other delicious roast beef sandwiches at your local Arby's. Online ordering now available!"
        );
        expect(nutrition.calories).toBe('560 calories');
        expect(nutrition.carbohydrateContent).toBe('45 grams');
        expect(nutrition.sodiumContent).toBe('1710 milligrams');
        expect(nutrition.cholesterolContent).toBe('80 milligrams');
        expect(nutrition.fatContent).toBe('27 grams');
    });

    it('should return correct data for Bww brand, when all data is available', async () => {
        const nutritionMock = Object.values(productMock.items).find((i) => i.id === 'arb-itm-000-018').nutrition;
        const result = SchemaPdpAndPlp(menuCategory as any, BRANDS.bww, product as any, nutritionMock);
        const menuItem = result.hasMenu.hasMenuSection.hasMenuItem;
        const menuSection = result.hasMenu.hasMenuSection;
        const nutrition = result.hasMenu.hasMenuSection.hasMenuItem.nutrition;

        expect(result.name).toBe('Buffalo Wild Wings');
        expect(result.description).toBe(
            'Enjoy all Buffalo Wild Wings® has to offer when you order online or stop by a location near you. Buffalo Wild Wings® is the ultimate place to get together with your friends, watch sports, drink beer, and eat wings.'
        );
        expect(result.servesCuisine[0]).toBe('Chicken Wings Restaurant');
        expect(menuSection.name).toBe('Meals');
        expect(menuSection.description).toBe(
            "Choose from favorites like the Classic Beef 'N Cheddar or Chicken Bacon & Swiss sandwiches and pair with your choice of side and a drink for the ultimate meal combo."
        );
        expect(menuSection.image[0]).toBe(
            'https://images.ctfassets.net/o19mhvm9a2cm/3YI5HMcNcwCFza65gsq1qc/2d361d20f278125262b463b0d2432787/Website_Meals_Signatures_Gyro_Greek.png?fm=webp&w=200'
        );
        expect(menuItem.name).toBe("Double Beef 'N Cheddar");
        expect(menuItem.description).toBe(
            "Come by and try the Double Beef 'n Cheddar, or any of our other delicious roast beef sandwiches at your local Arby's. Online ordering now available!"
        );
        expect(nutrition.calories).toBe('560 calories');
        expect(nutrition.carbohydrateContent).toBe('45 grams');
        expect(nutrition.sodiumContent).toBe('1710 milligrams');
        expect(nutrition.cholesterolContent).toBe('80 milligrams');
        expect(nutrition.fatContent).toBe('27 grams');
    });

    it('should return correct data, when no nutrition info is available', () => {
        const result = SchemaPdpAndPlp(menuCategory as any, BRANDS.arbys, product as any, undefined);
        const nutrition = result.hasMenu.hasMenuSection.hasMenuItem.nutrition;

        expect(nutrition).toEqual({ '@type': 'NutritionInformation', calories: '0 calories' });
    });

    it('should return correct data, when nutrition info is available', () => {
        const nutritionMock = Object.values(productMock.items).find((i) => i.id === 'arb-itm-000-018').nutrition;
        const result = SchemaPdpAndPlp(menuCategory as any, BRANDS.arbys, product as any, nutritionMock as any);
        const nutrition = result.hasMenu.hasMenuSection.hasMenuItem.nutrition;

        expect(nutrition.calories).toBe('560 calories');
        expect(nutrition.carbohydrateContent).toBe('45 grams');
        expect(nutrition.sodiumContent).toBe('1710 milligrams');
        expect(nutrition.cholesterolContent).toBe('80 milligrams');
        expect(nutrition.fatContent).toBe('27 grams');
    });

    it('should return correct data, when NO macroNutrients', () => {
        const nutritionMock = { totalCalories: 600 };
        const result = SchemaPdpAndPlp(menuCategory as any, BRANDS.arbys, product as any, nutritionMock as any);
        const nutrition = result.hasMenu.hasMenuSection.hasMenuItem.nutrition;

        expect(nutrition).toEqual({ '@type': 'NutritionInformation', calories: '600 calories' });
    });
});
