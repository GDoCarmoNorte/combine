import {
    validateAlpha,
    arbysValidateDateOfBirth,
    bwwValidateDateOfBirth,
    validateEmail,
    validateGiftCardNumber,
    validateGiftCardPin,
    validatePasswordComplexity,
    validatePhone,
    validatePhoneWithDashes,
    validateRequire,
    validateRequireBoolean,
    validateZipCode,
} from '../../../common/helpers/complexValidateHelper';

const FIELD_NAME = 'FieldName';

const INCOMPLETE_MESSAGE = `${FIELD_NAME} is incomplete`;
const INVALID_MESSAGE = `${FIELD_NAME} is invalid`;

const RealDate = Date.now;

describe('complexValidateHelper', () => {
    describe('validateAlpha function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validateAlpha(FIELD_NAME)('')).toEqual(INCOMPLETE_MESSAGE);
        });

        it('should return "Alpha characters only"', () => {
            expect(validateAlpha(FIELD_NAME)('34dsf')).toEqual('Alpha characters only');
        });

        it('should return "No longer than 5 characters"', () => {
            expect(validateAlpha(FIELD_NAME, 5)('sdfsdfsdf')).toEqual('No longer than 5 characters');
        });

        it('should return undefined for valid field value', () => {
            expect(validateAlpha(FIELD_NAME, 5)('asdf')).toEqual(undefined);
        });

        it('should return custom error message for empty value if messageMap.require argument is provided', () => {
            expect(validateAlpha(FIELD_NAME, 5, { require: 'CUSTOM_MESSAGE' })('')).toEqual('CUSTOM_MESSAGE');
        });

        it('should return custom error message for invalid character value if messageMap.char argument is provided', () => {
            expect(validateAlpha(FIELD_NAME, 5, { char: 'CUSTOM_MESSAGE' })('12345')).toEqual('CUSTOM_MESSAGE');
        });
        it('should return custom error message for invalid max value if messageMap.maxLength argument is provided', () => {
            expect(validateAlpha(FIELD_NAME, 5, { maxLength: 'CUSTOM_MESSAGE' })('abcdefg')).toEqual('CUSTOM_MESSAGE');
        });
    });

    describe('validateZipCode function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validateZipCode(FIELD_NAME)(undefined)).toEqual(INCOMPLETE_MESSAGE);
        });

        it(`should return ${INVALID_MESSAGE}`, () => {
            expect(validateZipCode(FIELD_NAME)(123)).toEqual(INVALID_MESSAGE);
        });

        it(`should return ${INVALID_MESSAGE}`, () => {
            expect(validateZipCode(FIELD_NAME)(123451)).toEqual(INVALID_MESSAGE);
        });

        it(`should return undefined for valid field value`, () => {
            expect(validateZipCode(FIELD_NAME)(12345)).toEqual(undefined);
        });

        it(`should return undefined for valid field value starting with a Zero (0)`, () => {
            expect(validateZipCode(FIELD_NAME)('01234')).toEqual(undefined);
        });
    });

    describe('validatePhone function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validatePhone(FIELD_NAME)('')).toEqual(INCOMPLETE_MESSAGE);
        });

        it(`should return "Incorrect phone`, () => {
            expect(validatePhone(FIELD_NAME)('asd-546-6184')).toEqual('Incorrect phone');
        });

        it(`should return undefined for valid field value`, () => {
            expect(validatePhone(FIELD_NAME)('1234567890')).toEqual(undefined);
        });

        it('should return custom error message for required value if messageMap.require argument is provided', () => {
            expect(validatePhone(FIELD_NAME, { require: 'CUSTOM_MESSAGE' })('')).toEqual('CUSTOM_MESSAGE');
        });
        it('should return custom error message for invalid value if messageMap.invalid argument is provided', () => {
            expect(validatePhone(FIELD_NAME, { invalid: 'CUSTOM_MESSAGE' })('abcd')).toEqual('CUSTOM_MESSAGE');
        });
    });

    describe('validatePhoneWithDashes function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validatePhoneWithDashes(FIELD_NAME)('')).toEqual(INCOMPLETE_MESSAGE);
        });

        it(`should return "Incorrect phone`, () => {
            expect(validatePhoneWithDashes(FIELD_NAME)('asd-546-6184')).toEqual('Incorrect phone');
        });

        it(`should return undefined for valid field value`, () => {
            expect(validatePhoneWithDashes(FIELD_NAME)('123-456-7890')).toEqual(undefined);
        });

        it('should return custom error message for required value if messageMap.require argument is provided', () => {
            expect(validatePhoneWithDashes(FIELD_NAME, { require: 'CUSTOM_MESSAGE' })('')).toEqual('CUSTOM_MESSAGE');
        });
        it('should return custom error message for invalid value if messageMap.invalid argument is provided', () => {
            expect(validatePhoneWithDashes(FIELD_NAME, { invalid: 'CUSTOM_MESSAGE' })('abcd')).toEqual(
                'CUSTOM_MESSAGE'
            );
        });
    });

    describe('validateEmail function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validateEmail(FIELD_NAME)('')).toEqual(INCOMPLETE_MESSAGE);
        });

        it(`should return "Incorrect email`, () => {
            expect(validateEmail(FIELD_NAME)('qwedgf')).toEqual('Incorrect email');
        });

        it(`should return undefined for valid field value`, () => {
            expect(validateEmail(FIELD_NAME)('name@mail.su')).toEqual(undefined);
        });
    });

    describe('validateRequire function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validateRequire(FIELD_NAME)('')).toEqual(INCOMPLETE_MESSAGE);
        });

        it(`should return undefined for valid field value`, () => {
            expect(validateRequire(FIELD_NAME)('werwerwer')).toEqual(undefined);
        });
    });

    describe('validateRequireBoolean function', () => {
        it(`should return ${INCOMPLETE_MESSAGE}`, () => {
            expect(validateRequireBoolean(FIELD_NAME)(false)).toEqual(INCOMPLETE_MESSAGE);
        });

        it(`should return undefined for valid field value`, () => {
            expect(validateRequireBoolean(FIELD_NAME)(true)).toEqual(undefined);
        });
    });

    describe('[arbys] validateDateOfBirth function', () => {
        it(`should return Incorrect format of ${FIELD_NAME} (DD/MM)`, () => {
            expect(arbysValidateDateOfBirth(FIELD_NAME)('10/1')).toEqual(`Incorrect format of ${FIELD_NAME} (MM/DD)`);
        });

        it(`should return undefined for valid field value with "/" as separator`, () => {
            expect(arbysValidateDateOfBirth(FIELD_NAME)('05/05')).toEqual(undefined);
        });

        it(`should return undefined for valid field value with "-" as separator`, () => {
            expect(arbysValidateDateOfBirth(FIELD_NAME)('05-05')).toEqual(undefined);
        });
    });

    describe('[bww] validateDateOfBirth function', () => {
        beforeAll(() => {
            global.Date.now = jest.fn(() => new Date('2020-10-10T00:00:00.000Z').getTime());
        });

        afterAll(() => {
            global.Date.now = RealDate;
        });

        it(`should return Please enter a valid ${FIELD_NAME} (DD/MM/YYYY)`, () => {
            expect(bwwValidateDateOfBirth(FIELD_NAME)('10/1')).toEqual(
                `Please enter a valid ${FIELD_NAME} (MM/DD/YYYY)`
            );
        });

        it(`should return Please enter a valid ${FIELD_NAME} (DD/MM/YYYY)`, () => {
            expect(bwwValidateDateOfBirth(FIELD_NAME)('10/10/10')).toEqual(
                `Please enter a valid ${FIELD_NAME} (MM/DD/YYYY)`
            );
        });

        it(`should return You must be 18 years old to join`, () => {
            expect(bwwValidateDateOfBirth(FIELD_NAME)('05/05/2020')).toEqual('You must be 18 years old to join');
        });

        it(`should return undefined for valid field value with "/" as separator`, () => {
            expect(bwwValidateDateOfBirth(FIELD_NAME)('05/05/1980')).toEqual(undefined);
        });

        it(`should return undefined for valid field value with "-" as separator`, () => {
            expect(bwwValidateDateOfBirth(FIELD_NAME)('05-05-1980')).toEqual(undefined);
        });
    });

    describe('validateGiftCardNumber function', () => {
        it(`should return empty error for 18-length field value`, () => {
            expect(validateGiftCardNumber()('308610000020000010')).toEqual(' ');
            expect(validateGiftCardNumber()('')).toEqual(' ');
        });

        it(`should return undefined for valid field value`, () => {
            expect(validateGiftCardNumber()('3086100000200000100')).toEqual(undefined);
            expect(validateGiftCardNumber()('')).toEqual(' ');
        });
    });

    describe('validateGiftCardPin function', () => {
        it(`should return correct message if field is not required`, () => {
            expect(validateGiftCardPin(false)('')).toEqual(undefined);
            expect(validateGiftCardPin(false)('12344')).toEqual(undefined);
        });

        it(`should return undefined for valid field value`, () => {
            expect(validateGiftCardPin(true)('0505')).toEqual(undefined);
            expect(validateGiftCardPin(true)('')).toEqual(' ');
        });
    });

    describe('validatePasswordComplexity function', () => {
        it(`should return correct message if field is invalid`, () => {
            expect(validatePasswordComplexity()('aB1aaaa8')).toEqual('At least 1 symbol must be entered (e.g. /#$)');
            expect(validatePasswordComplexity()('aBa!___a')).toEqual('At least 1 digit must be entered');
            expect(validatePasswordComplexity()('AB1!___8')).toEqual('At least 1 lowercase must be entered');
            expect(validatePasswordComplexity()('ab1!___8')).toEqual('At least 1 uppercase must be entered');
            expect(validatePasswordComplexity()('aB1!__7')).toEqual('Password must be at least 8 characters');
        });

        it(`should return undefined for valid field value`, () => {
            expect(validatePasswordComplexity()('aB1!___8')).toEqual(undefined);
        });
    });
});
