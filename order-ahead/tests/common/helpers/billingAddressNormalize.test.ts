import { defaultNormalizer, applePayNormalizer } from '../../../common/helpers/billingAddressNormalize';
import {
    completeBillingObjectMock,
    noNameBillingObjectMock,
    noPostalCodeBillingObjectMock,
    emptyBillingObjectMock,
    defaultCardHolderName,
    defaultPostalCode,
    applePayBillingAddressMock,
    noPostalCodeApplePayBillingAddressMock,
    noFamilyNameApplePayBillingAddressMock,
    noGivenNameApplePayBillingAddressMock,
    emptyApplePayBillingAddressMock,
    applePayCardHolderName,
    applePayPostalCode,
} from './billingAddressNormalize.mock';

describe('billingAddressNormalize', () => {
    describe('defaultNormalizer function', () => {
        it('should return "cardHolderName" and "billingPostalCode" from billing address object', () => {
            expect(defaultNormalizer(completeBillingObjectMock)).toEqual({
                cardHolderName: defaultCardHolderName,
                billingPostalCode: defaultPostalCode,
            });
        });
        it('should return "billingPostalCode" only if no "name" provided inside billing address object', () => {
            expect(defaultNormalizer(noNameBillingObjectMock)).toEqual({
                billingPostalCode: defaultPostalCode,
            });
        });
        it('should return "cardHolderName" only if no "postalCode" provided inside billing address object', () => {
            expect(defaultNormalizer(noPostalCodeBillingObjectMock)).toEqual({
                cardHolderName: defaultCardHolderName,
            });
        });
        it('should return empty object if no billing address Value provided', () => {
            expect(defaultNormalizer(emptyBillingObjectMock)).toEqual({});
        });
        it('should return empty object if no billing address provided', () => {
            expect(defaultNormalizer(null as any)).toEqual({});
        });
    });
    describe('applePayNormalizer function', () => {
        it('should return "cardHolderName" and "billingPostalCode" from billing address object', () => {
            expect(applePayNormalizer(applePayBillingAddressMock)).toEqual({
                cardHolderName: applePayCardHolderName,
                billingPostalCode: applePayPostalCode,
            });
        });
        it('should return "cardHolderName" only if no "postalCode" provided inside billing address object', () => {
            expect(applePayNormalizer(noPostalCodeApplePayBillingAddressMock)).toEqual({
                cardHolderName: applePayCardHolderName,
            });
        });
        it('should return "billingPostalCode" only if no "familyName" provided inside billing address object', () => {
            expect(applePayNormalizer(noFamilyNameApplePayBillingAddressMock)).toEqual({
                billingPostalCode: applePayPostalCode,
            });
        });
        it('should return "billingPostalCode" only if no "givenName" provided inside billing address object', () => {
            expect(applePayNormalizer(noGivenNameApplePayBillingAddressMock)).toEqual({
                billingPostalCode: applePayPostalCode,
            });
        });
        it('should return empty object if no billing address Value provided', () => {
            expect(applePayNormalizer(emptyApplePayBillingAddressMock)).toEqual({});
        });
        it('should return empty object if no billing address provided', () => {
            expect(applePayNormalizer(null as any)).toEqual({});
        });
    });
});
