export const completeBillingObjectMock = {
    Key: 'BillingAddress',
    Value: {
        address3: '',
        sortingCode: '',
        address2: '',
        countryCode: 'US',
        address1: 'D',
        postalCode: '19703',
        name: 'David Nazaryan',
        locality: 'CLAYMONT',
        administrativeArea: 'DE',
    },
};

export const noPostalCodeBillingObjectMock = {
    Key: 'BillingAddress',
    Value: {
        address3: '',
        sortingCode: '',
        address2: '',
        countryCode: 'US',
        address1: 'D',
        name: 'David Nazaryan',
        locality: 'CLAYMONT',
        administrativeArea: 'DE',
    },
};

export const noNameBillingObjectMock = {
    Key: 'BillingAddress',
    Value: {
        address3: '',
        sortingCode: '',
        address2: '',
        countryCode: 'US',
        address1: 'D',
        postalCode: '19703',
        locality: 'CLAYMONT',
        administrativeArea: 'DE',
    },
};

export const emptyBillingObjectMock = {
    Key: 'BillingAddress',
};

export const defaultCardHolderName = 'David Nazaryan';
export const defaultPostalCode = '19703';

export const applePayBillingAddressMock = {
    Key: 'billingContact',
    Value:
        '{"addressLines":["1 Apple Park Way"],"administrativeArea":"CA","country":"United States","countryCode":"US","familyName":"Nazaryan","givenName":"Davit","locality":"Cupertino","phoneticFamilyName":"","phoneticGivenName":"","postalCode":"95014","subAdministrativeArea":"","subLocality":""}',
};
export const noPostalCodeApplePayBillingAddressMock = {
    Key: 'billingContact',
    Value:
        '{"addressLines":["1 Apple Park Way"],"administrativeArea":"CA","country":"United States","countryCode":"US","familyName":"Nazaryan","givenName":"Davit","locality":"Cupertino","phoneticFamilyName":"","phoneticGivenName":"","subAdministrativeArea":"","subLocality":""}',
};
export const noFamilyNameApplePayBillingAddressMock = {
    Key: 'billingContact',
    Value:
        '{"addressLines":["1 Apple Park Way"],"administrativeArea":"CA","country":"United States","countryCode":"US","givenName":"Davit","locality":"Cupertino","phoneticFamilyName":"","phoneticGivenName":"","postalCode":"95014","subAdministrativeArea":"","subLocality":""}',
};
export const noGivenNameApplePayBillingAddressMock = {
    Key: 'billingContact',
    Value:
        '{"addressLines":["1 Apple Park Way"],"administrativeArea":"CA","country":"United States","countryCode":"US","familyName":"Nazaryan","locality":"Cupertino","phoneticFamilyName":"","phoneticGivenName":"","postalCode":"95014","subAdministrativeArea":"","subLocality":""}',
};
export const emptyApplePayBillingAddressMock = {
    Key: 'billingContact',
};

export const applePayCardHolderName = 'Davit Nazaryan';
export const applePayPostalCode = '95014';
