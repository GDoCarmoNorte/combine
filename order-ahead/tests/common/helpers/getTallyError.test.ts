import { TErrorTypeEXTERNALModel, TTallyErrorCodeModel } from '../../../@generated/webExpApi/models';
import { getTallyError } from '../../../common/helpers/getTallyError';
describe('getTallyError', () => {
    it('should return a correct error if the error code is DeliveryProviderDeliveryTimeNotAvailable', () => {
        const mockError = {
            code: TTallyErrorCodeModel.DeliveryProviderDeliveryTimeNotAvailable,
            message:
                'The earliest delivery time is {earliestTime} on the selected date. Please select a new date/time and try again.',
            type: TErrorTypeEXTERNALModel,
            data: {
                earliestTime: '04:30 AM',
            },
        };
        expect(getTallyError(mockError as any)).toEqual({
            code: TTallyErrorCodeModel.DeliveryProviderDeliveryTimeNotAvailable,
            message:
                'The earliest delivery time is 04:30 AM on the selected date. Please select a new date/time and try again.',
            type: TErrorTypeEXTERNALModel,
            data: {
                earliestTime: '04:30 AM',
            },
        });
    });

    it('should return a correct error if the error code is OrderTimeNotValid', () => {
        const mockError = {
            code: TTallyErrorCodeModel.OrderTimeNotValid,
            message:
                'We apologize. We do not have any available timeslots for the time you chose. Please switch your order to pickup or select a new delivery time to proceed with your order.',
            type: TErrorTypeEXTERNALModel,
            data: {},
        };
        expect(getTallyError(mockError as any)).toEqual(mockError);
    });

    it('should return a correct error if the error code is LocationNotAvailable', () => {
        const mockError = {
            code: TTallyErrorCodeModel.LocationNotAvailable,
            message: 'The requested location is not available. Please select another location and try again.',
            type: TErrorTypeEXTERNALModel,
            data: {},
        };
        expect(getTallyError(mockError as any)).toEqual(mockError);
    });

    it('should return a correct error if the error code is QuantitiesModifierGroupNotValid', () => {
        const mockError = {
            code: TTallyErrorCodeModel.QuantitiesModifierGroupNotValid,
            message: 'Some products contain invalid quantities. Please modify the quantities and try again.',
            type: TErrorTypeEXTERNALModel,
            data: {},
        };
        expect(getTallyError(mockError as any)).toEqual(mockError);
    });

    it('should return a correct error if the error code is QuantitiesModifiersNotValid', () => {
        const mockError = {
            code: TTallyErrorCodeModel.QuantitiesModifiersNotValid,
            message: 'Some products contain invalid quantities. Please modify the quantities and try again.',
            type: TErrorTypeEXTERNALModel,
            data: {},
        };
        expect(getTallyError(mockError as any)).toEqual(mockError);
    });

    it('should return a correct error if the error code is ExceededMaxOrderAmount', () => {
        const mockError = {
            code: TTallyErrorCodeModel.ExceededMaxOrderAmount,
            message:
                'We apologize. Your order exceeds ${orderMaxValue}. If you still want to place your order, call us at {phoneNumber}, or remove items from your bag.',
            type: TErrorTypeEXTERNALModel,
            data: {
                max: '100',
                requested: '120',
            },
        };
        expect(getTallyError(mockError as any, '612-866-9316')).toEqual({
            code: TTallyErrorCodeModel.ExceededMaxOrderAmount,
            message:
                'We apologize. Your order exceeds $100. If you still want to place your order, call us at 612-866-9316, or remove items from your bag.',
            type: TErrorTypeEXTERNALModel,
            data: {
                max: '100',
                requested: '120',
            },
        });
    });
});
