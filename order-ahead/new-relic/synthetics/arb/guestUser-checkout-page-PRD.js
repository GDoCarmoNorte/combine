/**
 * Feel free to explore, or check out the full documentation
 * https://docs.newrelic.com/docs/synthetics/new-relic-synthetics/scripting-monitors/writing-scripted-browsers
 * for details.
 */

const moment = require('moment');

const WINDOW_SIZE_X = 1920;
const WINDOW_SIZE_Y = 1080;

const WEBSITE = 'https://www.arbys.com';
const LOCATION_SEARCH_TERM = '2701 Hwy 78 E, JASPER, AL 35501'; //open 24hs
const STORE_NAME = 'Jasper – Highway 78 E'; //7066

const PRODUCT_SELECTION_CATEGORY = 'Slow Roasted Beef';
const PRODUCT_SELECTION_NAME = 'Smokehouse Brisket';

const CUSTOMER_FIRST_NAME = 'ArbysProduction';
const CUSTOMER_LAST_NAME = 'Synthetic LastName';
const CUSTOMER_PHONE = '2220001111';
const CUSTOMER_CC_NUMBER = '4111111111111111';
const CUSTOMER_CC_EXPIRATION = '02/25';
const CUSTOMER_CC_CVV = '111';
const CUSTOMER_CC_ZIP = '76262';
const CUSTOMER_EMAIL = 'synthetics-prd@inspirebrands.com';

let orderDate;

$browser
    .get(`${WEBSITE}/brands/arbys/favicon.ico`)
    .then(() => $browser.manage().window().setSize(WINDOW_SIZE_X, WINDOW_SIZE_Y))
    .then(() => $browser.executeScript("window.sessionStorage.setItem('continueAnyway', true)"))
    .then(() => $browser.get(WEBSITE))
    .then(() =>
        $browser
            .findElement($driver.By.xpath("//footer//*[contains(text(), 'All Rights Reserved')]"))
            .then((versionElement) => {
                return versionElement.getText().then((text) => console.log('Webapp version: ', text.substr(-10)));
            })
    )
    .then(() =>
        $browser
            .findElement($driver.By.xpath("//*[contains(text(), 'Find an Arby’s')]"))
            .then((element) => element.click())
            .then(() =>
                $browser.findElement(
                    $driver.By.xpath("//input[@placeholder='Search by Address, City and State or Zip Code']")
                )
            )
            .then((searchInputElement) => searchInputElement.sendKeys(LOCATION_SEARCH_TERM, $driver.Key.ENTER))
    )
    .then(() =>
        $browser
            .findElement($driver.By.xpath(`//*[@aria-label='Select ${STORE_NAME} location']//button[text()='ORDER']`))
            .then((orderButton) => orderButton.click())
    )
    .then(() =>
        $browser
            .findElement($driver.By.xpath(`//a//*[contains(text(), '${PRODUCT_SELECTION_CATEGORY}')]`))
            .then((categoryOrderButton) => categoryOrderButton.click())
    )
    .then(() =>
        $browser.wait(
            () =>
                $browser
                    .findElement(
                        $driver.By.xpath(
                            `//*[@aria-label='Product Card']//*[text()='${PRODUCT_SELECTION_NAME}']//ancestor::div[@aria-label='Product Card']`
                        )
                    )
                    .then((productCard) =>
                        productCard.getText().then((productCardText) => {
                            console.log('Product Card Content: ', productCardText);
                            return /[0-9]+ calories/i.test(productCardText);
                        })
                    ),
            5000
        )
    )
    .then(() =>
        $browser
            .findElement(
                $driver.By.xpath(
                    `//*[@aria-label='Product Card']//*[text()='${PRODUCT_SELECTION_NAME}']//ancestor::div[@aria-label='Product Card']`
                )
            )
            .then((productCard) => productCard.click())
    )
    .then(() =>
        $browser
            .findElements($driver.By.xpath("//button[text()='Add to bag']"))
            .then((addToBagButtons) => addToBagButtons[0].click())
            .then(() =>
                $browser
                    .findElement($driver.By.xpath("//div[@aria-label='Open bag']"))
                    .then((bagButton) => bagButton.click())
                    .then(() =>
                        $browser.wait(
                            $driver.until.elementIsEnabled(
                                $browser.findElement($driver.By.xpath("//button[text()='CHECKOUT NOW']"))
                            ),
                            5000
                        )
                    )
                    .then(() => {
                        console.log('"Checkout Now" button is enabled. Hitting button...');
                        return $browser
                            .findElement($driver.By.xpath("//button[text()='CHECKOUT NOW']"))
                            .then((e) => e.click());
                    })
            )
    )
    .then(() => {
        const startTime = new Date();
        return $browser
            .wait(
                () => $browser.findElement($driver.By.xpath("//*[text()='Review Order']")),
                15000,
                'Checkout page load'
            )
            .then(() =>
                console.log('Checkout Page - Look for element END (success): ', (new Date() - startTime) / 1000)
            )
            .catch((e) => {
                console.log('Checkout Page - Look for element END (failure): ', (new Date() - startTime) / 1000);
                console.log('Error while waiting for Checkout page...', { e });
                return $browser
                    .findElement($driver.By.id('main'))
                    .getAttribute('innerHTML')
                    .then((html) => console.log({ html }))
                    .then(() => Promise.reject(e));
            });
    })
    .then(() =>
        $browser
            .findElement($driver.By.xpath("//div[@data-testid='Pickup Date']"))
            .then((pickUpDateSelector) =>
                pickUpDateSelector.click().then(() => {
                    orderDate = moment().add(3, 'days').format('MM/DD/YYYY');
                    return $browser
                        .findElement($driver.By.xpath(`//div[text()='${orderDate}']`))
                        .then((nextDayElement) => {
                            nextDayElement
                                .getText()
                                .then((text) => console.log('Selecting day from datePicker: ', text, { orderDate }))
                                .then(() => nextDayElement.click());
                        })
                        .then(() => $browser.findElement($driver.By.xpath("//div[@data-testid='Pickup Time']")))
                        .then((pickUpTimeSelector) => pickUpTimeSelector.click())
                        .then(() =>
                            $browser.findElement(
                                $driver.By.xpath(
                                    "//div[@data-testid='Pickup Time']//div[@role='menuitem' and contains(., '01:1')][2]"
                                )
                            )
                        )
                        .then((onePmTimeElement) => onePmTimeElement.click());
                })
            )
            .then(() =>
                $browser
                    .findElement($driver.By.name('firstName'))
                    .then((textbox) => textbox.sendKeys(CUSTOMER_FIRST_NAME))
                    .then(() =>
                        $browser
                            .findElement($driver.By.name('lastName'))
                            .then((textbox) => textbox.sendKeys(CUSTOMER_LAST_NAME))
                    )
                    .then(() =>
                        $browser
                            .findElement($driver.By.name('phone'))
                            .then((textbox) => textbox.sendKeys(CUSTOMER_PHONE))
                    )
                    .then(() =>
                        $browser
                            .findElement($driver.By.name('email'))
                            .then((textbox) => textbox.sendKeys(CUSTOMER_EMAIL))
                    )
                    .then(() =>
                        $browser
                            .findElement($driver.By.name('chFirstName'))
                            .then((textbox) => textbox.sendKeys(CUSTOMER_FIRST_NAME))
                    )
                    .then(() =>
                        $browser
                            .findElement($driver.By.name('chLastName'))
                            .then((textbox) => textbox.sendKeys(CUSTOMER_LAST_NAME))
                    )
                    .then(() =>
                        $browser
                            .findElement(
                                $driver.By.xpath(
                                    '//*[contains(text(), \'By clicking "PAY", you authorize the Arby’s location listed to charge your credit card for the full amount and you agree to Arby’s\')]'
                                )
                            )
                            .then((element) => element.click())
                    )
                    .then(() =>
                        $browser
                            .findElement($driver.By.xpath(`//*[@aria-label='Payment Info']//iframe`))
                            .then((iframe) => $browser.switchTo().frame(iframe))
                            .then(() =>
                                $browser
                                    .findElement($driver.By.name('CardNumber'))
                                    .then((textbox) => textbox.sendKeys(CUSTOMER_CC_NUMBER))
                            )
                            .then(() =>
                                $browser
                                    .findElement($driver.By.name('ExpirationDate'))
                                    .then((textbox) => textbox.sendKeys(CUSTOMER_CC_EXPIRATION))
                            )
                            .then(() =>
                                $browser
                                    .findElement($driver.By.name('SecurityCode'))
                                    .then((textbox) => textbox.sendKeys(CUSTOMER_CC_CVV))
                            )
                            .then(() =>
                                $browser
                                    .findElement($driver.By.name('PostalCode'))
                                    .then((textbox) => textbox.sendKeys(CUSTOMER_CC_ZIP, $driver.Key.TAB))
                            )
                    )
            )
            .then(() =>
                $browser.wait(
                    $driver.until.elementIsEnabled($browser.findElement($driver.By.xpath("//button[text()='Pay']"))),
                    5000
                )
            )
            .then(() => {
                console.log('"Pay" button is enabled. Hitting button...');
                return $browser.findElement($driver.By.xpath("//button[text()='Pay']")).then((e) => e.click());
            })
    )
    .then(() => $browser.sleep(2000))
    .then(() => console.log('Switching to default content...'))
    .then(() => $browser.switchTo().defaultContent())
    .then(() => $browser.sleep(2000))
    .then(() => console.log('Waiting for confirmation page...'))
    .then(() => {
        const startTime = new Date();
        return $browser
            .wait(
                $driver.until.elementLocated(
                    $driver.By.xpath(
                        "//*[text()='We are experiencing technical difficulties. Please try again later.']"
                    )
                ),
                5000,
                'Payment error'
            )
            .then(() =>
                console.log('Checkout Page - Look for element END (success): ', (new Date() - startTime) / 1000)
            )
            .catch((e) => {
                console.log('Checkout Page - Look for element END (failure): ', (new Date() - startTime) / 1000);
                console.log('Error while waiting for Checkout Page, payment error...', { e });
                return $browser
                    .findElement($driver.By.id('main'))
                    .getAttribute('innerHTML')
                    .then((html) => console.log({ html }))
                    .then(() => Promise.reject(e));
            });
    });
