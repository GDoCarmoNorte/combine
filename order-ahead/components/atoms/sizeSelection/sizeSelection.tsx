import React from 'react';
import classnames from 'classnames';

import styles from './sizeSelection.module.css';
import { ISizeSelection } from '../../../redux/types';
import { useProductItemGroup } from '../../../redux/hooks/domainMenu';
import { spaces2underscores } from '../../../lib/gtm';
import { BOGOS_LABELS_MAP } from './constants';
interface ISizeSelectionProps {
    selections?: ISizeSelection[];
    selectedProductId: string;
    disabled?: boolean;
    onSelect: (productId: string, size: string) => void;
    className?: string;
    itemClassName?: string;
}

export default function SizeSelection(props: ISizeSelectionProps): JSX.Element {
    const { disabled, selections, onSelect, selectedProductId, className, itemClassName } = props;

    const productItemGroup = useProductItemGroup(selectedProductId);

    const selectionName = selections?.find((s) => s.product.id === selectedProductId)?.sizeGroup.name;
    const bogoLabel = BOGOS_LABELS_MAP[productItemGroup?.name]?.(selectionName);

    const handleSizeSelection = (selection: ISizeSelection) =>
        disabled || selection.disabled ? undefined : () => onSelect(selection.product.id, selection?.sizeGroup?.name);

    if (!selections || selections.length <= 1) {
        return null;
    }

    const getGtmId = (selection) => {
        const productName = selection?.product?.name;
        const sizeName = selection?.sizeGroup?.name;
        return `sizeSelection_${spaces2underscores(productName)}_${spaces2underscores(sizeName)}`;
    };

    return (
        <div
            className={classnames(
                styles.container,
                { [styles.disabled]: disabled, [styles.initial]: bogoLabel },
                className
            )}
            role="radiogroup"
            aria-label="Select option"
        >
            {selections.map((selection, index) => {
                const isSelected = selection.product.id === selectedProductId;
                return (
                    <button
                        data-gtm-id={getGtmId(selection)}
                        role="radio"
                        aria-checked={isSelected}
                        className={classnames(
                            styles.selectionItem,
                            't-subheader-small',
                            {
                                [styles.selected]: isSelected,
                                [styles.disabled]: selection.disabled,
                            },
                            itemClassName
                        )}
                        onClick={handleSizeSelection(selection)}
                        key={`item${index}`}
                    >
                        <div>
                            {selection.isGroupedBySize && selection.sizeGroup?.name?.toLowerCase() !== 'none'
                                ? selection.sizeGroup.name
                                : selection.product?.name}
                            {isSelected && bogoLabel && <span className={styles.bogoFree}>{bogoLabel}</span>}
                        </div>
                    </button>
                );
            })}
        </div>
    );
}
