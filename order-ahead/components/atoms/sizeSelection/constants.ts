import { ItemGroupEnum } from '../../../redux/types';

export const BOGOS_LABELS_MAP = {
    [ItemGroupEnum.BOGO]: (selectionName: string) => `+ ${selectionName} Free`,
    [ItemGroupEnum.BOG6]: () => '+ 6 Free',
    [ItemGroupEnum.BOGO_50_OFF]: (selectionName: string) => `+ ${selectionName} @50% off`,
};
