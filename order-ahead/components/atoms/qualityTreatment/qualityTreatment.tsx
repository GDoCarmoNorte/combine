import React from 'react';
import { IQualityTreatmentProps } from './types';

export default function QualityTreatment(props: IQualityTreatmentProps): JSX.Element {
    return (
        <div className="m-quality-treatment">
            {props.showIcon && <div className="m-quality-treatment-icon" />}
            <div>
                {props.subheader?.length && <p className="m-quality-treatment-subheader">{props.subheader}</p>}
                <p className="m-quality-treatment-header">{props.header}</p>
            </div>
        </div>
    );
}
