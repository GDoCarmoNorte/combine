export interface IQualityTreatmentProps {
    showIcon?: boolean;
    header: string;
    subheader?: string;
}
