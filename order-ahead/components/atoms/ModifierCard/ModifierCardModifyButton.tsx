import React, { FC } from 'react';
import classNames from 'classnames';

import styles from './ModifierCard.module.css';
import { InspireLinkButton } from '../link';

import { TYPOGRAPHY_CLASS } from './constants';

export interface IModifierCardModifyButtonProps {
    modifiers?: string;
    onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
    selected: boolean;
}

const ModifierCardModifyButton: FC<IModifierCardModifyButtonProps> = ({ onClick, modifiers, selected }) => {
    return (
        <div className={styles.modifyContainer}>
            <InspireLinkButton aria-label="Modify" tabIndex={0} onClick={onClick} linkType="secondary">
                Modify
            </InspireLinkButton>

            {modifiers && selected && (
                <div
                    className={classNames('truncate', styles.modifications, TYPOGRAPHY_CLASS.MODIFIERS_TEXT)}
                    title={modifiers}
                >
                    {modifiers}
                </div>
            )}
        </div>
    );
};

export default ModifierCardModifyButton;
