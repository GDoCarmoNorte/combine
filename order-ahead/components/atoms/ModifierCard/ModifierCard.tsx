import React from 'react';
import classNames from 'classnames';

import { IProductFields, IProductModifierFields } from '../../../@generated/@types/contentful';

import ContentfulImage from '../ContentfulImage';
import { ISizeSelection, ProductIntensityEnum, ModifierCardSelectorType } from '../../../redux/types';
import { formatPrice } from '../../../lib/domainProduct';

import TextWithTrademark from '../textWithTrademark';
import TextWithDotSeparator from '../TextWithDotSeparator';
import { ProductItemCalories } from '../productItemPriceAndCalories';
import SizeSelection from '../sizeSelection';
import SauceIntensity from '../SauceIntensity/SauceIntensity';

import { CHECKBOX_SIZE, TYPOGRAPHY_CLASS } from './constants';

import styles from './ModifierCard.module.css';
import RadioButton from '../radioButton';
import Checkbox from '../checkbox';
import ModifierCardModifyButton from './ModifierCardModifyButton';
import QuantitySelector from '../quantitySelector';
import { InspireCmsEntry } from '../../../common/types';
import { isCaloriesDisplayEnabled } from '../../../lib/getFeatureFlags';

export interface IModifierCardProps {
    className?: string;
    selectorType: ModifierCardSelectorType;
    selectionType: 'none' | 'single' | 'multi';
    productName: string;
    quantity: number;
    calories?: number;
    price?: number;
    selected?: boolean;
    disabled?: boolean;
    showModify?: boolean;
    modifierItemId: string;
    onCardMinus?: () => void;
    onCardPlus?: () => void;
    onClick: () => void;
    intensity?: ProductIntensityEnum;
    contentfulProduct: InspireCmsEntry<IProductFields> | InspireCmsEntry<IProductModifierFields>;
    requireSelectionModifiersGroups?: string[];
    canDecrement?: boolean;
    canIncrement?: boolean;
    modifiers?: string;
    maxQuantityTooltipText?: string;
    selections?: ISizeSelection[];
    showPrice?: boolean; // weird prop
    onModifyClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    onSelectSize?: (itemId: string, size: string) => void;
}

export function ModifierCard(props: IModifierCardProps): JSX.Element {
    const {
        canDecrement = false,
        canIncrement = false,
        maxQuantityTooltipText,
        contentfulProduct,
        productName,
        modifierItemId,
        modifiers,
        onCardMinus,
        onCardPlus,
        onClick,
        onModifyClick,
        onSelectSize,
        quantity,
        calories,
        price,
        selections,
        showModify = false,
        showPrice = false,
        requireSelectionModifiersGroups = [],
        selectionType,
        selectorType,
        intensity,
        selected = false,
        disabled = false,
    } = props;

    const renderSelection = () => {
        if (selectionType === 'single') {
            return (
                <RadioButton
                    checked={selected}
                    inputProps={{ 'aria-label': `Select ${productName}` }}
                    value={modifierItemId}
                    tabIndex={-1}
                />
            );
        }

        if (selectionType === 'multi') {
            return (
                <Checkbox
                    ariaLabel={`Select ${productName}`}
                    className={styles.checkbox}
                    fieldName={modifierItemId}
                    selected={selected}
                    size={CHECKBOX_SIZE}
                    tabIndex={-1}
                />
            );
        }
    };

    const handleClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (!disabled) {
            onClick();

            if (requireSelectionModifiersGroups.length) {
                onModifyClick(e);
            }
        }
    };

    const handleModifyClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (!selected) {
            handleClick(e);
        }
        onModifyClick(e);
    };

    const handleKeyPress = (e: React.KeyboardEvent<HTMLDivElement>): void => {
        if (e.key === 'Enter') {
            handleClick(e);
        }
    };
    const activeElementsTabIndex = disabled ? -1 : 0;

    return (
        <div
            className={classNames(props.className, styles.cardContainer, {
                [styles.cardSelected]: selected,
                [styles.disabled]: disabled,
            })}
        >
            <div className={classNames(styles.cardWrapper)}>
                <div
                    tabIndex={activeElementsTabIndex}
                    className={styles.cardTop}
                    onClick={handleClick}
                    onKeyPress={handleKeyPress}
                >
                    {contentfulProduct ? (
                        <div className={styles.imageWrapper}>
                            <ContentfulImage maxWidth={105} asset={contentfulProduct.fields.image} />
                        </div>
                    ) : (
                        <div>
                            CONTENT CREATOR WARNING: Please create a product with id = default in Contentful for default
                            image
                        </div>
                    )}
                    <div className={styles.infoContainer}>
                        <div className={styles.textContainer}>
                            <TextWithTrademark tag="div" text={productName} className={TYPOGRAPHY_CLASS.NAME} />
                            {intensity && <SauceIntensity intensity={intensity} />}
                            {!!requireSelectionModifiersGroups.length && (
                                <div
                                    className={classNames('truncate-at-2', 't-paragraph-hint', styles.requireSelection)}
                                >{`Select ${requireSelectionModifiersGroups.join(', ')}`}</div>
                            )}
                            <div className={classNames(styles.priceAndCalories, 't-paragraph-hint')}>
                                <TextWithDotSeparator
                                    leftText={showPrice ? `+ ${formatPrice(price)}` : undefined}
                                    rightText={
                                        isCaloriesDisplayEnabled() ? <ProductItemCalories calories={calories} /> : ''
                                    }
                                />
                            </div>
                        </div>
                    </div>
                    {renderSelection()}
                </div>
                {selectorType === 'quantity' && selected ? (
                    <QuantitySelector
                        maxQuantityTooltipText={maxQuantityTooltipText}
                        onCardMinus={onCardMinus}
                        onCardPlus={onCardPlus}
                        quantity={quantity}
                        canDecrement={canDecrement}
                        canIncrement={canIncrement}
                        plusButtonAriaLabel={`Add one ${productName}`}
                        minusButtonAriaLabel={`Remove one ${productName}`}
                    />
                ) : null}
                {selectorType === 'size' && !!selections && (
                    <SizeSelection
                        selections={selections}
                        selectedProductId={selected ? modifierItemId : null}
                        onSelect={onSelectSize}
                        disabled={false}
                        className={styles.sizeSelection}
                        itemClassName={styles.itemSizeSelection}
                    />
                )}
            </div>
            {showModify && (
                <ModifierCardModifyButton onClick={handleModifyClick} modifiers={modifiers} selected={selected} />
            )}
        </div>
    );
}
