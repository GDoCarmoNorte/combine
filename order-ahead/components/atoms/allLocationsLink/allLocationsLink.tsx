import * as React from 'react';

import { InspireLink } from '../link';
import { isLocationPagesOn } from '../../../lib/getFeatureFlags';

interface IAllLocationsLinkProps {
    className?: string;
}

function AllLocationsLink({ className }: IAllLocationsLinkProps): JSX.Element {
    const locationsLink = isLocationPagesOn() ? '/locations/all/' : process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN;

    return locationsLink !== 'undefined' ? (
        <InspireLink type="secondary" link={locationsLink} newtab className={className}>
            All Locations
        </InspireLink>
    ) : null;
}
export default AllLocationsLink;
