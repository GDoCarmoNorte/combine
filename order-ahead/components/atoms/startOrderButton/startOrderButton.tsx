import React, { ReactEventHandler } from 'react';
import Link from 'next/link';
import Button from '@material-ui/core/Button';

import styles from './startOrderButton.module.css';
import classNames from 'classnames';
import useStartAnOrderLink from '../../../common/hooks/useStartAnOrderLink';
interface IStartOrderButtonProps {
    onClick?: ReactEventHandler;
    className?: string;
}

export default function StartOrderButton(props: IStartOrderButtonProps): JSX.Element {
    const buttonClasses = classNames(styles.startOrderButton, props.className);
    const startAnOrderLink = useStartAnOrderLink();

    // TODO refactor with InspireButton component
    return (
        <Link href={startAnOrderLink}>
            <Button
                id="start-order-btn"
                fullWidth
                variant="contained"
                classes={{ root: buttonClasses }}
                onClick={props.onClick}
                data-gtm-id="startOrder-CTA"
            >
                START AN ORDER
            </Button>
        </Link>
    );
}
