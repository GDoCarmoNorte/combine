import React, { FunctionComponent } from 'react';
import Link from 'next/link';
import classnames from 'classnames';

import {
    IDocumentLink,
    IExternalLink,
    IMenuCategoryLink,
    IPageLink,
    IPhoneNumberLink,
    IProduct,
} from '../../../@generated/@types/contentful';

import styles from './button.module.css';
import { getLinkDetails } from '../../../lib/link';
import useGlobalProps from '../../../redux/hooks/useGlobalProps';

// TODO: remove sizes from type
export type InspireButtonType = 'primary' | 'secondary' | 'large' | 'small';
type InspireButtonSizeType = 'large' | 'small';

export interface IInspireButtonProps {
    link?: IExternalLink | IPageLink | IMenuCategoryLink | IPhoneNumberLink | IProduct | IDocumentLink | string;
    text?: string;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    disabled?: boolean;
    className?: string;
    linkClassName?: string;
    type?: InspireButtonType;
    size?: InspireButtonSizeType;
    gtmId?: string;
    submit?: boolean;
    fullWidth?: boolean;
    ariaLabel?: string;
}

export const InspireButton: FunctionComponent<IInspireButtonProps> = (props) => {
    const {
        link,
        type = 'primary',
        size,
        text,
        onClick,
        children,
        disabled,
        className,
        linkClassName,
        gtmId,
        submit,
        fullWidth,
        ariaLabel,
    } = props;

    const { productDetailsPagePaths } = useGlobalProps();

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onClick && onClick(event);
    };

    if (children) {
        return (
            <button data-gtm-id={gtmId} onClick={handleClick}>
                {children}
            </button>
        );
    }

    const buttonClasses = classnames(
        styles.button,
        {
            [styles[type]]: !!type,
            [styles[size]]: !!size,
            [styles.fullWidth]: !!fullWidth,
        },
        className
    );

    if (!link) {
        return (
            <button
                type={submit ? 'submit' : 'button'}
                data-gtm-id={gtmId}
                disabled={disabled}
                className={buttonClasses}
                onClick={handleClick}
                tabIndex={0}
                aria-label={ariaLabel}
            >
                {text}
            </button>
        );
    }

    const { href, isExternal, isPhone, name } = getLinkDetails(link, {
        productDetailsPagePaths,
    });

    const linkClasses = classnames(buttonClasses, styles.link, linkClassName);

    return isExternal || isPhone ? (
        <a
            data-gtm-id={gtmId}
            target={isExternal ? '_blank' : undefined}
            rel="noreferrer"
            href={isPhone ? `tel:${href}` : href}
            className={linkClasses}
        >
            {text || name}
        </a>
    ) : (
        <Link href={href}>
            <a
                data-gtm-id={gtmId}
                className={classnames(linkClasses, {
                    [styles.disabled]: disabled,
                })}
            >
                {text || name}
            </a>
        </Link>
    );
};
