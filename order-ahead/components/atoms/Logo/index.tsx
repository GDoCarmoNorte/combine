import React from 'react';
import { useMediaQuery } from '@material-ui/core';

interface ILogoImageProps {
    urlLogo: string;
    className: string;
    urlLogoDesktop?: string;
}

const Logo = ({ urlLogo, urlLogoDesktop, className }: ILogoImageProps): JSX.Element => {
    const isDesktop = useMediaQuery('(min-width: 960px)');
    return (
        <picture>
            {isDesktop && urlLogoDesktop ? (
                <img className={className} src={urlLogoDesktop} alt="logo" />
            ) : (
                <img className={className} src={urlLogo} alt="logo" />
            )}
        </picture>
    );
};

export default Logo;
