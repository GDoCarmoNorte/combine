import React from 'react';
import classnames from 'classnames';
import AddIcon from '@material-ui/icons/Add';

import styles from './addNewCard.module.css';
import AddEditCard from '../../organisms/addEditCard/addEditCard';

interface IAddNewCard {
    text: string;
    className?: string;
    onAddNewCard: () => void;
    handleSetLoading: (boolean: boolean) => void;
    customerId: string;
    isOverlayOpen: boolean;
    closeOverlay: () => void;
    jwtToken: string;
    handleFetchPaymentMethods: () => void;
}

export const AddNewCard = (props: IAddNewCard): JSX.Element => {
    const {
        text,
        onAddNewCard,
        handleSetLoading,
        className,
        closeOverlay,
        jwtToken,
        handleFetchPaymentMethods,
    } = props;

    return (
        <>
            {props.isOverlayOpen && jwtToken ? (
                <div className={classnames(styles.overlayContainer, className)} data-testid={'AddEditComponent'}>
                    <AddEditCard
                        handleSetLoading={handleSetLoading}
                        customerId={props.customerId}
                        closeOverlay={closeOverlay}
                        jwtToken={jwtToken}
                        handleFetchPaymentMethods={handleFetchPaymentMethods}
                    />
                </div>
            ) : (
                <div className={classnames(styles.container, className)} data-testid={'addNewButton'}>
                    <button className={styles.button} onClick={onAddNewCard}>
                        <AddIcon className={styles.icon} />
                        <p className={classnames(styles.text, 't-paragraph-strong')}>{text}</p>
                    </button>
                </div>
            )}
        </>
    );
};
