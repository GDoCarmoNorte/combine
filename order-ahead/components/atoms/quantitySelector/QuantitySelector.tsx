import React, { useState, FC, useEffect, useRef } from 'react';

import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import classNames from 'classnames';

import InspireTooltip from '../Tooltip';
import styles from './quantitySelector.module.css';

interface ICardNumberInputProps {
    value: number;
}

function CardNumberInput({ value }: ICardNumberInputProps): JSX.Element {
    const [internalValue, setInternalValue] = useState(value);

    useEffect(() => {
        setInternalValue(value < 1 ? 1 : value);
    }, [value]);

    return (
        <input
            aria-label="quantity"
            title="quantity"
            className={classNames(styles.input, styles.quantity, 't-subheader-small')}
            value={internalValue}
            disabled
        />
    );
}

interface IQuantitySelectorProps {
    onCardMinus: () => void;
    onCardPlus: () => void;
    canDecrement?: boolean;
    canIncrement?: boolean;
    quantity: number;
    maxQuantityTooltipText?: string;
    className?: string;
    minusButtonAriaLabel?: string;
    plusButtonAriaLabel?: string;
}

const QuantitySelector: FC<IQuantitySelectorProps> = (props) => {
    const {
        canIncrement,
        canDecrement,
        onCardPlus,
        onCardMinus,
        maxQuantityTooltipText,
        quantity,
        className,
        minusButtonAriaLabel,
        plusButtonAriaLabel,
    } = props;

    const [shouldShowTooltip, setShouldShowTooltip] = useState<boolean>(false);
    const [timeoutId, setTimeoutId] = useState(null);

    const handleCardPlusClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (!canIncrement) {
            setShouldShowTooltip(true);
            clearTimeout(timeoutId);

            const newTimeout = setTimeout(() => {
                setShouldShowTooltip(false);
            }, 2000);

            setTimeoutId(newTimeout);
        }
        onCardPlus();
    };

    const handleCardPlusKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleCardPlusClick(e);
        }
    };

    const handleCardMinusClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        onCardMinus();
    };

    const handleCardMinusKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleCardMinusClick(e);
        }
    };
    const ref = useRef(null);

    return (
        <div className={classNames(className, styles.container)}>
            <div
                className={classNames(styles.input, styles.plusMinus, {
                    [styles.plusMinusDisabled]: !canDecrement,
                })}
                tabIndex={0}
                role="button"
                onClick={handleCardMinusClick}
                onKeyPress={handleCardMinusKeyPress}
                aria-label={minusButtonAriaLabel}
            >
                <RemoveIcon className={styles.icon} viewBox="5 11 14 2" />
            </div>
            <ClickAwayListener
                onClickAway={(e) => {
                    // if "plus" button is clicked, we don't hide tooltip
                    if (ref.current && ref.current.contains(e.target)) {
                        return;
                    }
                    setShouldShowTooltip(false);
                }}
            >
                <div className={styles.tooltipWrapper}>
                    <InspireTooltip
                        open={shouldShowTooltip}
                        placement="top"
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        theme="dark"
                        title={maxQuantityTooltipText}
                        arrow
                        PopperProps={{
                            disablePortal: true,
                            modifiers: {
                                flip: {
                                    enabled: false,
                                },
                                preventOverflow: {
                                    enabled: false,
                                },
                            },
                        }}
                    >
                        <div className={styles.quantityWrapper}>
                            <CardNumberInput value={quantity} />
                        </div>
                    </InspireTooltip>
                </div>
            </ClickAwayListener>
            <div
                ref={ref}
                className={classNames(styles.input, styles.plusMinus, {
                    [styles.plusMinusDisabled]: !canIncrement,
                })}
                tabIndex={0}
                role="button"
                onClick={handleCardPlusClick}
                onKeyPress={handleCardPlusKeyPress}
                aria-label={plusButtonAriaLabel}
            >
                <AddIcon className={styles.icon} viewBox="5 5 14 14" />
            </div>
        </div>
    );
};

export default QuantitySelector;
