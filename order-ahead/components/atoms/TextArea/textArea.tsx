import React from 'react';
import classnames from 'classnames';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';

import styles from './textArea.module.css';

interface IInspireTextAreaProps {
    ariaLabel?: string;
    className?: string;
    maxLength?: number;
}

const InspireTextArea = ({
    ariaLabel = 'text area',
    className,
    maxLength = 1024,
    ...props
}: IInspireTextAreaProps & TextFieldProps): JSX.Element => {
    const cssClasses = {
        notchedOutline: styles.textAreaBorder,
        input: styles.textAreaInput,
        ...props.InputProps?.classes,
    };

    return (
        <TextField
            className={classnames(className, styles.textArea)}
            variant="outlined"
            rows={5}
            fullWidth
            multiline
            inputProps={{ maxLength, 'aria-label': ariaLabel, ...props.inputProps }}
            InputProps={{ classes: { ...cssClasses }, ...props.InputProps }}
            {...props}
        />
    );
};

export default InspireTextArea;
