import React from 'react';
import styles from '../sectionCard.module.css';
import { ISectionCardTextContentProps } from './types';

const SectionCardTextContent = ({ gtmId, productCount, linkText }: ISectionCardTextContentProps): JSX.Element => {
    return (
        <>
            <span data-gtm-id={gtmId} className={styles.counter}>{`${productCount} items`}</span>
            <span data-gtm-id={gtmId} className={styles.link}>
                {linkText}
            </span>
        </>
    );
};

export default SectionCardTextContent;
