export interface ISectionCardTextContentProps {
    gtmId: string;
    productCount?: number;
    linkText?: string;
    description?: string;
}
