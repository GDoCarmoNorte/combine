import React, { FC } from 'react';
import classnames from 'classnames';

import { ProductIntensityEnum } from '../../../redux/types';
import BrandIcon from '../BrandIcon';

import styles from './SauceIntensity.module.css';

const flames = {
    [ProductIntensityEnum.MILD]: {
        classname: styles.mild,
        count: 1,
    },
    [ProductIntensityEnum.MEDIUM]: {
        classname: styles.medium,
        count: 2,
    },
    [ProductIntensityEnum.HOT]: {
        classname: styles.hot,
        count: 3,
    },
    [ProductIntensityEnum.WILD]: {
        classname: styles.wild,
        count: 4,
    },
};

interface ISauceIntensityProps {
    intensity: ProductIntensityEnum;
    intensityTextClassName?: string;
    intensityFlamesClassName?: string;
    containerClassName?: string;
}

const SauceIntensity: FC<ISauceIntensityProps> = ({
    intensity,
    intensityTextClassName,
    intensityFlamesClassName,
    containerClassName,
}) => {
    const flamesCount = flames[intensity].count;
    const flamesClassName = flames[intensity].classname;

    const flamesIcons = Array(flamesCount)
        .fill(null)
        .map((elem, index) => (
            <BrandIcon
                key={index + 1}
                className={classnames(styles.flame, flamesClassName, intensityFlamesClassName)}
                icon="flame"
            />
        ));

    return (
        <div className={classnames(styles.container, containerClassName)}>
            <span>{flamesIcons}</span>
            <span className={classnames(styles.intensityText, 't-paragraph-hint', intensityTextClassName)}>
                {intensity}
            </span>
        </div>
    );
};

export default SauceIntensity;
