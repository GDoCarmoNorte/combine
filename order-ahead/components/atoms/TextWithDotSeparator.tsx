import React, { FC, Fragment } from 'react';

const TextWithDotSeparator: FC<{
    leftText?: JSX.Element | string;
    rightText?: JSX.Element | string;
}> = ({ leftText, rightText }) => {
    if (leftText && rightText) {
        return (
            <Fragment>
                {leftText}&nbsp;&nbsp;•&nbsp;&nbsp;{rightText}
            </Fragment>
        );
    }

    return <Fragment>{leftText || rightText}</Fragment>;
};

export default TextWithDotSeparator;
