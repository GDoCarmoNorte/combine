import React from 'react';
import classnames from 'classnames';
import { useLogout } from '../../../common/hooks/useLogout';
import styles from './logoutButton.module.css';

interface ILogoutButton {
    className?: string;
}

const LogoutButton = (props: ILogoutButton): JSX.Element => {
    const { logoutAndClearCookies } = useLogout();
    const { className } = props;
    const handleLogout = (e) => {
        e.preventDefault();
        logoutAndClearCookies();
    };

    return (
        <button
            data-gtm-id="accountLogout"
            className={classnames(className, styles.logoutButton)}
            onClick={handleLogout}
        >
            Logout
        </button>
    );
};

export default LogoutButton;
