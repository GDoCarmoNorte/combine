import React, { FC } from 'react';
import classNames from 'classnames';
import styles from './verticalViewAllModifiersCard.module.css';
import commonStyles from '../common.module.css';

interface IVerticalViewAllModifiersCard {
    title: string;
    onClick: () => void;
    className?: string;
}

const VerticalViewAllModifiersCard: FC<IVerticalViewAllModifiersCard> = (props) => {
    const { title, onClick, className } = props;

    return (
        <button type="button" onClick={onClick} className={classNames(className, styles.container)}>
            <div className={styles.iconWrapper}>
                <img className={styles.icon} src="/brands/inspire/add-icon.svg" alt="" />
            </div>
            <div className={classNames(styles.text, commonStyles.productTitle)}>{title}</div>
        </button>
    );
};

export default VerticalViewAllModifiersCard;
