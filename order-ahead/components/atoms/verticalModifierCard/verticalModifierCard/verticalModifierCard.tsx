import React, { FC } from 'react';
import classNames from 'classnames';

import { IProductFields, IProductModifierFields } from '../../../../@generated/@types/contentful';

import ContentfulImage from '../../ContentfulImage';
import styles from './verticalModifierCard.module.css';
import SauceIntensity from '../../SauceIntensity/SauceIntensity';
import commonStyles from '../common.module.css';

import { ProductIntensityEnum, ModifierCardSelectorType } from '../../../../redux/types';
import { ProductItemPriceAndCalories } from '../../productItemPriceAndCalories';
import RadioButton from '../../radioButton';
import Checkbox from '../../checkbox/checkbox';
import { InspireLinkButton } from '../../link';

import TextWithTrademark from '../../textWithTrademark';
import QuantitySelector from '../../quantitySelector';
import { InspireCmsEntry } from '../../../../common/types';

export interface IVerticalModifierCardProps {
    className?: string;
    selectionType?: 'none' | 'single' | 'multi';
    selectorType: ModifierCardSelectorType;
    productName: string;
    quantity?: number;
    calories?: number;
    price?: number;
    selected?: boolean;
    disabled?: boolean;
    showModify?: boolean;
    modifierItemId: string;
    onCardMinus?: () => void;
    onCardPlus?: () => void;
    onClick: (e: React.MouseEvent<HTMLElement>) => void;
    onModifyClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
    intensity?: ProductIntensityEnum;
    contentfulProduct: InspireCmsEntry<IProductFields> | InspireCmsEntry<IProductModifierFields>;
    canIncrement?: boolean;
    canDecrement?: boolean;
    modifiersText?: string;
    maxQuantityTooltipText?: string;
    requireSelectionModifiersGroups?: string[];
}

const VerticalModifierCard: FC<IVerticalModifierCardProps> = (props: IVerticalModifierCardProps) => {
    const {
        className,
        modifierItemId,
        productName,
        calories,
        price,
        contentfulProduct,
        onClick,
        onCardMinus,
        onCardPlus,
        onModifyClick,
        selectionType = 'none',
        selectorType,
        selected = false,
        quantity,
        intensity,
        disabled = false,
        showModify = false,
        canIncrement = true,
        canDecrement = true,
        modifiersText,
        maxQuantityTooltipText,
        requireSelectionModifiersGroups = [],
    } = props;

    const renderSelection = () => {
        switch (selectionType) {
            case 'none':
                return null;
            case 'single':
                return (
                    <RadioButton
                        className={styles.radioButton}
                        checked={selected}
                        inputProps={{ 'aria-label': `Select ${productName}` }}
                        value={modifierItemId}
                        tabIndex={-1}
                    />
                );
            case 'multi':
                return (
                    <Checkbox
                        ariaLabel={`Select ${productName}`}
                        className={styles.checkbox}
                        fieldName={modifierItemId}
                        selected={selected}
                        tabIndex={-1}
                    />
                );
        }
    };

    const handleClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (!disabled) {
            onClick(e);

            if (requireSelectionModifiersGroups.length) {
                onModifyClick(e);
            }
        }
    };

    const handleModifyClick = (e) => {
        e.preventDefault();
        e.stopPropagation();

        if (!selected) {
            handleClick(e);
        }
        onModifyClick(e);
    };

    const handleKeyPress = (e: React.KeyboardEvent<HTMLDivElement>): void => {
        if (e.key === 'Enter') {
            handleClick(e);
        }
    };

    const activeElementsTabIndex = disabled ? -1 : 0;

    const hasContentfulProduct = !!contentfulProduct;
    const hasContentfulImage = hasContentfulProduct && !!contentfulProduct.fields.image;

    return (
        <div
            className={classNames(className, styles.container, { [styles.disabled]: disabled })}
            onClick={handleClick}
            onKeyPress={handleKeyPress}
            tabIndex={activeElementsTabIndex}
        >
            <div className={styles.contentWrapper}>
                {renderSelection()}
                {contentfulProduct ? (
                    <ContentfulImage asset={contentfulProduct.fields.image} maxWidth={130} className={styles.image} />
                ) : (
                    <div>
                        CONTENT CREATOR WARNING: Please create a product with id = default in Contentful for default
                        image
                    </div>
                )}
                <div
                    className={classNames(styles.textContentWrapper, {
                        [styles.textContentWrapperNoImage]: hasContentfulProduct && !hasContentfulImage,
                    })}
                >
                    <TextWithTrademark
                        tag="p"
                        text={productName}
                        className={classNames(styles.productName, commonStyles.productTitle)}
                    />
                    {!!requireSelectionModifiersGroups.length && (
                        <div
                            className={classNames('truncate-at-2', 't-paragraph-hint', styles.requireSelection)}
                        >{`Select ${requireSelectionModifiersGroups.join(', ')}`}</div>
                    )}
                    {modifiersText && selected && (
                        <span className={classNames('truncate-at-2', styles.priceAndCalories, 't-paragraph-hint')}>
                            {modifiersText}
                        </span>
                    )}
                    {intensity && <SauceIntensity intensity={intensity} />}
                    <ProductItemPriceAndCalories
                        calories={calories}
                        price={price}
                        className={classNames(styles.priceAndCalories, 't-paragraph-hint')}
                    />
                </div>
            </div>
            {selectorType === 'quantity' && selected ? (
                <QuantitySelector
                    className={styles.quantitySelectorWrapper}
                    quantity={quantity}
                    onCardMinus={onCardMinus}
                    onCardPlus={onCardPlus}
                    canDecrement={canDecrement}
                    canIncrement={canIncrement}
                    maxQuantityTooltipText={maxQuantityTooltipText}
                    plusButtonAriaLabel={`Add one ${productName}`}
                    minusButtonAriaLabel={`Remove one ${productName}`}
                />
            ) : null}
            {showModify && (
                <div className={styles.modifyButtonWrapper}>
                    <InspireLinkButton
                        onClick={handleModifyClick}
                        linkType="secondary"
                        tabIndex={activeElementsTabIndex}
                    >
                        Modify
                    </InspireLinkButton>
                </div>
            )}
        </div>
    );
};

export default VerticalModifierCard;
