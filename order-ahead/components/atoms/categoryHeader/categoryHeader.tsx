import React from 'react';
import { ICategoryHeaderProps } from './types';

export default function CategoryHeader(props: ICategoryHeaderProps): JSX.Element {
    return (
        <div className="m-category-header">
            {props.showBullet && <div className="m-category-header-bullet" />}
            <h2 className="m-category-header-text">{props.text}</h2>
            <div className="m-category-header-notation-text">{props.notationText}</div>
        </div>
    );
}
