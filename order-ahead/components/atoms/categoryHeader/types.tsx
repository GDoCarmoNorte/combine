export interface ICategoryHeaderProps {
    showBullet?: boolean;
    text: string;
    notationText?: string;
}
