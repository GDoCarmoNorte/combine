import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import RewardDetailsModal from '../../molecules/rewardDetailsModal';
import { format } from '../../../common/helpers/dateTime';
import { InspireButton } from '../button';
import { usePurchaseOffer } from '../../../common/hooks/usePurchaseOffer';

import recommendedCertificateStyles from './recommendedCertificate.module.css';
import styles from './../verticalProductCard/verticalProductCard.module.css';
import { CatalogRedeemedCertificateView } from '../../molecules/rewardDetailsModal/catalogRedeemedCertificateView/catalogRedeemedCertificateView';
import { useAccount } from '../../../redux/hooks';
import { useRouter } from 'next/router';
import { InspireLinkButton } from '../link';
import SuccessErrorModal from '../../molecules/successErrorModal';
import { TErrorCodeModel } from '../../../@generated/webExpApi';
import { Icons } from '../../molecules/successErrorModal/successErrorModal';
import {
    EMAIL_NOT_VERIFIED_MESSAGE,
    EMAIL_NOT_VERIFIED_TITLE,
    ERROR_MODAL_CLOSE_BUTTON_TEXT,
} from '../../../common/constants/rewards';

interface IRecommendedRewardsCertificateProps {
    imageUrl: string;
    title: string;
    points: number;
    containerClass?: string;
    isSideScroll: boolean;
    endDateTime: string;
    code: string;
    setPurchaseLoadingState: (state: boolean) => void;
}

const RecommendedCertificate = (props: IRecommendedRewardsCertificateProps): JSX.Element => {
    const { imageUrl, title, points, containerClass, isSideScroll, endDateTime, code, setPurchaseLoadingState } = props;

    const { purchaseOffer, cleanLastPurchaseData, loading, error, errorCode, cleanErrorCode } = usePurchaseOffer();
    const router = useRouter();

    useEffect(() => {
        setPurchaseLoadingState(loading);
    }, [loading, setPurchaseLoadingState]);

    const modalTitle = 'Certificate details';
    const modalButtonText = 'Order online';
    const formattedExpirationDate = format(new Date(endDateTime), 'MM/dd/yyyy');
    const expirationLabel = `Expires by ${formattedExpirationDate}.`;
    const { accountInfo } = useAccount();
    const memberNumber = accountInfo.phone;
    const productLink = '/menu';

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [openErrorSuccessModal, setOpenErrorSuccessModal] = useState<boolean>(false);

    const onOrderClick = () => {
        cleanLastPurchaseData();
        setIsModalOpen(false);
        router.push(productLink);
    };

    const isEmailNotVerifiedCode = errorCode && errorCode === TErrorCodeModel.EmailNotVerified;

    const onRedeemClick = async () => {
        await purchaseOffer(code);
    };

    const onModalClose = () => {
        setIsModalOpen(false);
        cleanLastPurchaseData();
    };

    const onErrorSuccessModalClose = () => {
        cleanErrorCode();
        setOpenErrorSuccessModal(false);
    };

    const renderButtons = () => {
        return (
            <InspireButton
                className={classnames(styles.button, recommendedCertificateStyles.modalBtn)}
                type="primary"
                text={modalButtonText}
                onClick={onOrderClick}
            />
        );
    };

    useEffect(() => {
        if (error) {
            setPurchaseLoadingState(false);
            setOpenErrorSuccessModal(true);
        }
    }, [error, setPurchaseLoadingState, setOpenErrorSuccessModal]);

    const certificateRedemptionLink = (
        <InspireLinkButton onClick={onRedeemClick} linkType={'secondary'}>
            redeem
        </InspireLinkButton>
    );

    const recommendedRewardsCertificateClassName = classnames(
        containerClass,
        styles.verticalProductCard,
        recommendedCertificateStyles.productCard,
        {
            [styles.verticalProductCardGrid]: !isSideScroll,
            [styles.verticalProductCardSideScroll]: isSideScroll,
        }
    );

    return (
        <>
            <div className={recommendedRewardsCertificateClassName}>
                <img src={imageUrl} alt={title} className={styles.itemImage} style={{ objectFit: 'contain' }} />
                <div className={styles.itemDescription}>
                    <div className={classnames('t-subheader-smaller', styles.itemName, 'truncate-at-2')}>{title}</div>
                    <div
                        className={classnames(
                            't-paragraph-hint',
                            styles.caloriesAndPrice,
                            recommendedCertificateStyles.points
                        )}
                    >
                        {points} points
                    </div>
                </div>
                <div className={classnames(styles.itemActions, recommendedCertificateStyles.buttonWrap)}>
                    {certificateRedemptionLink}
                </div>
            </div>
            <RewardDetailsModal
                open={isModalOpen}
                onClose={onModalClose}
                modalTitle={modalTitle}
                renderButtons={renderButtons}
                isLoading={loading}
            >
                <CatalogRedeemedCertificateView
                    imageUrl={imageUrl}
                    title={title}
                    expirationLabel={expirationLabel}
                    memberNumber={memberNumber}
                />
            </RewardDetailsModal>
            <SuccessErrorModal
                open={openErrorSuccessModal}
                onClose={onErrorSuccessModalClose}
                closeButtonText={ERROR_MODAL_CLOSE_BUTTON_TEXT}
                icon={Icons.WARNING}
                errorCode={errorCode}
                title={isEmailNotVerifiedCode ? EMAIL_NOT_VERIFIED_TITLE : null}
                description={isEmailNotVerifiedCode ? EMAIL_NOT_VERIFIED_MESSAGE : null}
                isSuccess={false}
                modalStyles={styles.successErrorModal}
                buttonRowStyles={styles.successErrorModalButtonRow}
                buttonStyles={styles.successErrorModalButton}
            />
        </>
    );
};

export default RecommendedCertificate;
