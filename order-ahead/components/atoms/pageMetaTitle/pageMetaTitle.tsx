import React, { FC } from 'react';
import classnames from 'classnames';

import styles from './pageMetaTitle.module.css';

interface IPageMetaTitle {
    text: string;
    tag?: keyof JSX.IntrinsicElements;
    titleClassName?: string;
    containerClassName?: string;
}

const PageMetaTitle: FC<IPageMetaTitle> = ({
    text,
    tag = 'h1',
    titleClassName = 't-header-h1',
    containerClassName,
}): JSX.Element => (
    <div className={classnames(containerClassName, styles.container)}>
        {React.createElement(
            tag,
            {
                className: classnames(titleClassName, styles.title),
            },
            text
        )}
    </div>
);

export default PageMetaTitle;
