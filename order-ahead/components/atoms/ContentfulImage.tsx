import React from 'react';
import { Asset } from 'contentful';
import { getImageSrcset, getImageUrl } from '../../common/helpers/contentfulImage';

interface IContentfulImageProps {
    asset: Asset;
    maxWidth?: number;
    className?: string;
    inlineStyles?: { [key: string]: string };
    gtmId?: string;
    useInitialFormat?: boolean;
    imageAlt?: string;
}

// TODO:  PLEASE IMPLEMENT AND TEST SRCSET
// const generateSrcset = (url: string, maxWidth = 1500) => {
//     if (maxWidth > 1500) return '';
//     let srcset = '';
//     let currentWidth = maxWidth;
//     while (currentWidth > 100) {
//         srcset = `${srcset},
// ${url}?fm=webp&w=${currentWidth} ${currentWidth}w
//         `;
//         currentWidth = currentWidth - 100;
//     }
//     return srcset;
// };

export default function ContentfulImage(props: IContentfulImageProps): JSX.Element | null {
    const { asset, maxWidth = 1500, className, inlineStyles, gtmId, imageAlt, useInitialFormat = false } = props;
    if (!asset?.fields?.file?.url) return null;
    // TODO:  PLEASE IMPLEMENT AND TEST SRCSET
    // const srcset = generateSrcset(asset.fields.file.url, 300);

    return (
        <picture data-gtm-id={gtmId} style={{ display: 'flex' /* fix spacing for picture tag */ }}>
            {!useInitialFormat && (
                <source data-gtm-id={gtmId} srcSet={getImageSrcset(asset, maxWidth)} type="image/webp" />
            )}
            <img
                data-gtm-id={gtmId}
                alt={imageAlt ?? (asset.fields.description || asset.fields.title)}
                src={getImageUrl(asset, maxWidth)}
                className={className}
                style={{ objectFit: 'contain', ...inlineStyles }}
            />
        </picture>
    );
}
