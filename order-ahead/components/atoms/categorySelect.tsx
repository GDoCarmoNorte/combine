import React from 'react';
import styles from './categorySelect.module.css';
import { useRouter } from 'next/router';
import { IMenuCategory, ITapListMenuCategory } from '../../@generated/@types/contentful';
import { Select, MenuItem } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import classnames from 'classnames';
import isTapListMenuCategory from '../../common/helpers/isTapListMenuCategory';
import { useUnavailableCategories } from '../../common/hooks/useUnavailableCategories';
import { isMenuCategoryUnavailable } from '../../common/helpers/menuCategoryHelper';
import { useDomainMenuCategories } from '../../redux/hooks/domainMenu';

interface ICategorySelectProps {
    categories: (IMenuCategory | ITapListMenuCategory)[];
    currentCategoryUrl: string;
}

export default function CategorySelect({ categories, currentCategoryUrl }: ICategorySelectProps): JSX.Element {
    const router = useRouter();
    const unavailableCategories = useUnavailableCategories();
    const domainMenuCategories = useDomainMenuCategories();

    return (
        <div aria-label="Category Select Section">
            <h1 className="visually-hidden">{currentCategoryUrl}</h1>
            <Select
                IconComponent={ExpandMoreIcon}
                value={currentCategoryUrl}
                onChange={(e) => {
                    router.push('/menu/[menuCategoryUrl]', `/menu/${e.target.value}`);
                }}
                disableUnderline={true}
                className={styles.mainText}
                classes={{
                    select: classnames(styles.selectHeader, 't-header-h1'),
                }}
                MenuProps={{
                    anchorOrigin: {
                        vertical: 'bottom',
                        horizontal: 'left',
                    },
                    transformOrigin: {
                        vertical: 'top',
                        horizontal: 'left',
                    },
                    getContentAnchorEl: null,
                }}
                inputProps={{
                    'aria-label': `Select Menu Category ${currentCategoryUrl}`,
                    style: { display: 'none' },
                }}
            >
                {categories
                    .filter(
                        (cat) =>
                            isTapListMenuCategory(cat) ||
                            !isMenuCategoryUnavailable(cat, unavailableCategories, domainMenuCategories)
                    )
                    .map((cat) => (
                        <MenuItem
                            key={cat.fields.link.fields.nameInUrl}
                            value={cat.fields.link.fields.nameInUrl}
                            className={styles.selectMenuItem}
                            classes={{
                                selected: styles.selectedItem,
                            }}
                        >
                            {cat.fields.categoryName}
                        </MenuItem>
                    ))}
            </Select>
        </div>
    );
}
