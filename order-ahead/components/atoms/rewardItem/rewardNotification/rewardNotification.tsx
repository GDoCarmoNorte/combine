import React, { ReactElement } from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import classnames from 'classnames';
import isMobileScreen from '../../../../lib/isMobileScreen';
import InspireTooltip from '../../Tooltip';
import Icon from '../../BrandIcon';
import styles from './rewardNotification.module.css';

interface RewardNotificationProps {
    isOpen: boolean;
    title: string;
    description: string;
    className?: string;
    children: ReactElement;
    onOpen: (state: boolean) => void;
}

const RewardNotification = (props: RewardNotificationProps): JSX.Element => {
    const { onOpen, isOpen, title, description, className, children } = props;
    const mobileScreen = isMobileScreen();

    return (
        <ClickAwayListener onClickAway={() => onOpen(false)}>
            <div>
                <InspireTooltip
                    tooltipClassName={className}
                    open={isOpen}
                    PopperProps={{
                        popperOptions: {
                            modifiers: {
                                flip: { enabled: false },
                            },
                        },
                    }}
                    placement={mobileScreen ? 'bottom' : 'left'}
                    theme="dark"
                    arrow
                    title={
                        <div className={styles.container}>
                            <Icon className={styles.icon} icon="brand-star" size="m" variant="colorful" />
                            <div className={styles.textBlock}>
                                <p className={classnames('t-paragraph-small-strong', styles.title)}>{title}</p>
                                <p className={classnames('t-paragraph-small', styles.description)}>{description}</p>
                            </div>
                        </div>
                    }
                >
                    {children}
                </InspireTooltip>
            </div>
        </ClickAwayListener>
    );
};

export default RewardNotification;
