import React from 'react';
import styles from './rewardItem.module.css';
import classnames from 'classnames';
import RewardNotification from './rewardNotification/rewardNotification';
import Icon from '../../atoms/BrandIcon';

interface IRewardItemProps {
    title: string;
    imageUrl: string;
    label: string;
    labelText?: string;
    activeCTAText?: string;
    defaultCTAText?: string;
    activeCTAGtmId?: string;
    defaultCTAGtmId?: string;
    onViewDetails?: () => void;
    notificationTitle?: string;
    notificationDescription?: string;
    onNotificationToggle?: () => void;
    onActiveCTAClick?: () => void;
    notificationOpen?: boolean;
    onNotificationClose?: (state: boolean) => void;
    hideCTAButton?: boolean;
    hasEnoughPoints?: boolean;
}

const RewardItem = (props: IRewardItemProps): JSX.Element => {
    const {
        title,
        imageUrl,
        label,
        labelText = '',
        defaultCTAText,
        activeCTAText,
        activeCTAGtmId,
        defaultCTAGtmId,
        onViewDetails,
        notificationTitle,
        notificationDescription,
        onNotificationToggle,
        onActiveCTAClick,
        notificationOpen,
        onNotificationClose,
        hideCTAButton,
        hasEnoughPoints = true,
    } = props;

    const renderCtaButton = () => {
        if (defaultCTAText) {
            return (
                <RewardNotification
                    isOpen={notificationOpen}
                    title={notificationTitle}
                    description={notificationDescription}
                    className={styles.tooltip}
                    onOpen={onNotificationClose}
                >
                    <button
                        data-gtm-id={defaultCTAGtmId}
                        className={classnames('t-subheader-smaller', styles.activeBtn, styles.button)}
                        onClick={onNotificationToggle}
                    >
                        {defaultCTAText}
                    </button>
                </RewardNotification>
            );
        }
        if (activeCTAText) {
            return (
                <button
                    data-gtm-id={activeCTAGtmId}
                    className={classnames(
                        't-subheader-smaller',
                        styles.textUnderLine,
                        styles.activateOrCancel,
                        styles.button
                    )}
                    onClick={onActiveCTAClick}
                >
                    {activeCTAText}
                </button>
            );
        }
    };

    return (
        <div className={styles.itemContainer}>
            <div className={styles.imageContainer}>
                <img src={imageUrl} className={styles.itemImage} alt="" />
            </div>
            <div className={styles.itemInfoRow}>
                <div className={styles.titleColumn}>
                    <span className={classnames('t-subheader-smaller', 'truncate', styles.itemTitle)}>{title}</span>
                    <span
                        className={classnames('t-paragraph-hint ', styles.itemDate, {
                            [styles.noEnoughPoints]: !hasEnoughPoints,
                        })}
                    >
                        {!hasEnoughPoints && <Icon className={styles.lockIcon} icon="info-lock" />}{' '}
                        {`${label} ${labelText}`}
                    </span>
                </div>
                <div className={styles.buttonsRow}>
                    {!hideCTAButton && (
                        <>
                            {renderCtaButton()}
                            <span className={styles.divider} />
                        </>
                    )}
                    <button
                        className={classnames('t-paragraph-hint', styles.textUnderLine, styles.button)}
                        onClick={onViewDetails}
                    >
                        View details
                    </button>
                </div>
            </div>
        </div>
    );
};

export default RewardItem;
