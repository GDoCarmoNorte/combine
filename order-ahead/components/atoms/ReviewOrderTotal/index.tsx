import React from 'react';
import classNames from 'classnames';

import styles from './index.module.css';
import InspireTooltip from '../Tooltip';
import { TEXT_DISCOUNT } from './constants';
import { FeeItemModel } from '../../../@generated/webExpApi/models/FeeItemModel';

interface FormattedFee extends Omit<FeeItemModel, 'amount'> {
    amount: string;
    name: string;
    legalText?: string;
}

interface IReviewOrderTotalProps {
    subtotal?: number | string;
    totalDiscount?: number | string;
    totalPrice?: number | string;
    totalTax?: number | string;
    certificateDiscount?: number | string;
    totalTaxLabel?: string;
    totalTip?: string;
    tooltipText?: string | JSX.Element;
    additionalFees?: number | string;
    formattedFees?: Array<FormattedFee>;
    isOverLimitForOrder?: boolean;
    tipPercentage?: number;
    cardNumber?: string;
    showCardNumber?: boolean;
    freeDelivery?: string;
}

const ReviewOrderTotal = (props: IReviewOrderTotalProps): JSX.Element => {
    const {
        subtotal,
        totalDiscount,
        totalPrice,
        totalTax,
        certificateDiscount,
        totalTaxLabel,
        isOverLimitForOrder,
        tooltipText,
        additionalFees,
        formattedFees,
        totalTip,
        tipPercentage,
        cardNumber,
        showCardNumber,
        freeDelivery,
    } = props;

    const maskedCardNumber = cardNumber && showCardNumber ? `${'*'.repeat(12)}${cardNumber}` : null;

    return (
        <div className={styles.reviewOrder}>
            {certificateDiscount && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, styles.greenColor, 't-paragraph-small')}>
                        Reward Certificate(s)
                    </div>
                    <div className={classNames(styles.totalItem, styles.greenColor, styles.price, 't-paragraph-small')}>
                        {certificateDiscount}
                    </div>
                </div>
            )}
            {subtotal && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, 't-paragraph-small')}>Subtotal</div>
                    <div className={styles.subtotalContainer}>
                        <InspireTooltip
                            tooltipClassName={styles.tooltip}
                            open={isOverLimitForOrder && !!tooltipText}
                            disableFocusListener
                            disableHoverListener
                            disableTouchListener
                            title={tooltipText || ''}
                            placement="right"
                            theme="primary"
                            arrow
                            disablePortal={true}
                        >
                            <div className={classNames(styles.totalItem, styles.price, 't-paragraph-small')}>
                                {subtotal}
                            </div>
                        </InspireTooltip>
                    </div>
                </div>
            )}
            {totalDiscount && (
                <div className={styles.reviewOrderItem}>
                    <div
                        className={classNames(
                            styles.totalItem,
                            styles.discount,
                            styles.discountFont,
                            't-paragraph-small'
                        )}
                    >
                        {TEXT_DISCOUNT}
                    </div>
                    <div
                        className={classNames(
                            styles.totalItem,
                            styles.discount,
                            styles.price,
                            styles.discountFont,
                            't-paragraph-small'
                        )}
                    >
                        -{totalDiscount}
                    </div>
                </div>
            )}
            {freeDelivery && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, styles.discount, 't-paragraph-small')}>
                        Delivery Fee
                    </div>
                    <div className={classNames(styles.totalItem, styles.discount, styles.price, 't-paragraph-small')}>
                        {freeDelivery}
                    </div>
                </div>
            )}
            {formattedFees &&
                formattedFees.map((fee, i) => {
                    const asterisksAmount =
                        fee.legalText && formattedFees.slice(0, i).filter((fee) => fee.legalText).length + 1;

                    return (
                        <div className={styles.reviewOrderItem} key={fee.type}>
                            <div className={classNames(styles.totalItem, 't-paragraph-small')}>
                                {fee.name} {'*'.repeat(asterisksAmount)}
                            </div>
                            <div className={classNames(styles.totalItem, styles.price, 't-paragraph-small')}>
                                {fee.amount}
                            </div>
                        </div>
                    );
                })}
            {additionalFees && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, 't-paragraph-small')}>Additional fees</div>
                    <div className={classNames(styles.totalItem, styles.price, 't-paragraph-small')}>
                        {additionalFees}
                    </div>
                </div>
            )}
            {totalTip && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, 't-paragraph-small')}>{`Tip${
                        tipPercentage ? ` (${tipPercentage}%)` : ''
                    }`}</div>
                    <div className={classNames(styles.totalItem, styles.price, 't-paragraph-small')}>{totalTip}</div>
                </div>
            )}
            {totalTax && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, 't-paragraph-small')}>{totalTaxLabel || 'Tax'}</div>
                    <div className={classNames(styles.totalItem, styles.price, 't-paragraph-small')}>{totalTax}</div>
                </div>
            )}
            {totalPrice && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, styles.totalLine, 't-paragraph-strong')}>Total</div>
                    <div className={classNames(styles.totalItem, styles.totalLine, styles.price, 't-paragraph-strong')}>
                        {totalPrice}
                    </div>
                </div>
            )}
            {formattedFees &&
                formattedFees
                    .filter((fee) => fee.legalText)
                    .map((fee, i) => (
                        <div className={classNames(styles.legal, 't-paragraph-hint')} key={fee.legalText}>
                            {'*'.repeat(i + 1)} {fee.legalText}
                        </div>
                    ))}
            {maskedCardNumber && (
                <div className={styles.reviewOrderItem}>
                    <div className={classNames(styles.totalItem, styles.cardNumber, 't-paragraph')}>Card Number</div>
                    <div className={classNames(styles.totalItem, styles.cardNumber, styles.price, 't-paragraph')}>
                        {maskedCardNumber}
                    </div>
                </div>
            )}
        </div>
    );
};

export default ReviewOrderTotal;
