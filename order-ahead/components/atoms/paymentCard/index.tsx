import classNames from 'classnames';
import React from 'react';
import { TAccountPaymentMethodTypeModel } from '../../../@generated/webExpApi';
import maskGiftCard from '../../../common/helpers/maskGiftCard';
import maskCreditCard from '../../../common/helpers/maskCreditCard';
import BrandIcon from '../BrandIcon';
import { Divider } from '../divider';
import styles from './index.module.css';

interface IGiftCard {
    title: string;
    accountBalance?: number;
    cardNumber: string;
    onRemove: (token: string) => void;
    className?: string;
}

export interface ICreditCard {
    cardType: TAccountPaymentMethodTypeModel | 'GIFT';
    expiredDate?: string;
    isExpired?: boolean;
    token?: string;
}

const PaymentCard = (props: IGiftCard & ICreditCard): JSX.Element => {
    const { cardType, accountBalance, cardNumber, title, onRemove, className, expiredDate, isExpired, token } = props;
    const maskedCard = cardType === 'GIFT' ? maskGiftCard(cardNumber) : maskCreditCard(cardNumber);
    const zeroBalance = accountBalance === 0;

    const displayCardImage = (cardType: ICreditCard['cardType']): string | null => {
        const cardImages = {
            GIFT: `/brands/inspire/giftCardImage.png`,
            MC: `/brands/inspire/masterCardImage.png`,
            VI: `/brands/inspire/visaCardImage.png`,
            AX: `/brands/inspire/americanExpress.png`,
            DS: `/brands/inspire/discoverCard.png`,
        };

        return cardImages[cardType] || null;
    };

    return (
        <div className={classNames(className, styles.container)}>
            <p className={classNames('t-link', styles.title)}>{title}</p>
            <div className={styles.wrapperBlock}>
                {displayCardImage(cardType) && (
                    <img className={styles.cardImage} src={displayCardImage(cardType)} alt="" />
                )}
                <span className={classNames('t-paragraph-small', styles.cardNumber)}>{maskedCard}</span>
                <Divider className={styles.divider} />
                {accountBalance !== undefined && (
                    <span
                        className={classNames('t-paragraph-small', styles.balanceDescription, {
                            [styles.zeroBalance]: zeroBalance,
                        })}
                    >
                        <span className={styles.balanceText}>Balance:</span> ${accountBalance}
                    </span>
                )}

                {expiredDate && (
                    <span
                        className={classNames('t-paragraph-small', styles.expiredBlock, {
                            [styles.isExpired]: isExpired,
                        })}
                    >
                        {isExpired ? 'Expired: ' : 'Expires: '}
                        <span className={classNames(styles.expiresDate, { [styles.isExpired]: isExpired })}>
                            {expiredDate}
                        </span>
                    </span>
                )}
            </div>

            <div className={styles.actionWrapper}>
                <div className={styles.rightActionWrapper}>
                    <div className={styles.btnWrapper}>
                        <BrandIcon className={styles.removeIcon} icon="action-delete" />
                        <span
                            role="button"
                            onClick={() => onRemove(token)}
                            className={classNames('t-paragraph-hint', styles.buttonText)}
                        >
                            Remove
                        </span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PaymentCard;
