import React from 'react';
import styles from './index.module.css';
import Link from 'next/link';
import { useBreadcrumbs } from '../../../common/hooks/useBreadcrumbs';
import classnames from 'classnames';

export interface IBreadcrumbsProps {
    paths?: Array<IBreadcrumbsPath>;
}

export interface IBreadcrumbsPath {
    title: string;
    href: string;
    as?: string;
}

export default function Breadcrumbs({ paths }: IBreadcrumbsProps): JSX.Element {
    const breadcrumbsPaths = useBreadcrumbs();

    const breadcrumbsPathForMapping = paths || breadcrumbsPaths.slice(0, -1);

    return (
        <span className={styles.menuSpan}>
            {breadcrumbsPathForMapping.map(({ title, ...rest }) => (
                <span key={title}>
                    <Link {...rest}>
                        <a className={styles.menuLink}>{title}</a>
                    </Link>
                    <span className={classnames('t-tag', styles.slash)}>&#47;</span>
                </span>
            ))}
        </span>
    );
}
