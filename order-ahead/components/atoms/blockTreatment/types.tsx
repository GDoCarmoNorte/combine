export interface IBlockTreatmentAltProps {
    title: string;
    subtitle: string;
}

export interface IBlockTreatmentProps extends IBlockTreatmentAltProps {
    size: 'sm' | 'md' | 'lg';
}
