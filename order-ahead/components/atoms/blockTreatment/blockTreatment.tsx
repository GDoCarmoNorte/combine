import React from 'react';
import classnames from 'classnames';
import { IBlockTreatmentProps, IBlockTreatmentAltProps } from './types';

export default function BlockTreatment(props: IBlockTreatmentProps): JSX.Element {
    const blockTreatmentClass = `m-block-treatment-${props.size}`;
    return (
        <div className={classnames('m-block-treatment', blockTreatmentClass)}>
            <p className="m-block-treatment-subtitle">{props.subtitle}</p>
            <h2 className="m-block-treatment-title">{props.title}</h2>
        </div>
    );
}

export function BlockTreatmentAlt(props: IBlockTreatmentAltProps): JSX.Element {
    const firstLetter = props.title.substr(0, 1);
    const headerText = props.title.substr(1).trim();

    return (
        <div className="m-block-treatment-alt">
            <h2 className="m-block-treatment-alt-first-letter">{firstLetter}</h2>
            <div>
                <h2 className="m-block-treatment-alt-title">{headerText}</h2>
                <p className="m-block-treatment-alt-subtitle">{props.subtitle}</p>
            </div>
        </div>
    );
}
