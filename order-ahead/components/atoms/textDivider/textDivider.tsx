import React, { FunctionComponent } from 'react';
import classnames from 'classnames';

import styles from './divider.module.css';

export interface IInspireDividerProps {
    text: string;
    className?: string;
    textClassName?: string;
}

export const TextDivider: FunctionComponent<IInspireDividerProps> = (props) => {
    const { text, className, textClassName } = props;

    return (
        <div className={classnames(styles.dividerLine, className)}>
            <span className={classnames('t-paragraph-small', styles.dividerText, textClassName)}>{text}</span>
        </div>
    );
};
