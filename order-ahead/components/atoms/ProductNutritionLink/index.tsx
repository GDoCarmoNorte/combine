import classNames from 'classnames';
import React from 'react';

import { InspireLink } from '../link';
import BrandIcon from '../BrandIcon';
import styles from './index.module.css';

import { IProductNutritionLinkFields } from '../../../@generated/@types/contentful';

export interface IProductNutritionLinkProps {
    productNutrition: IProductNutritionLinkFields | null;
}

function ProductNutritionLink({ productNutrition }: IProductNutritionLinkProps): JSX.Element {
    return (
        (productNutrition && (
            <InspireLink className={styles.nutritionLink} link={productNutrition.link} newtab>
                <div className={classNames('link-secondary-active', styles.nutritionLinkText)}>
                    {productNutrition.text || 'View full nutrition & allergen information'}
                </div>
                <BrandIcon className={styles.nutritionLinkIcon} icon="action-open-in-new" size="xs" />
            </InspireLink>
        )) || (
            <div className={classNames('t-paragraph-hint', styles.nutritionMissingText)}>
                Full nutrition details are not available for this item.
            </div>
        )
    );
}

export default ProductNutritionLink;
