import React from 'react';
import { Button } from '@material-ui/core';
import Close from '@material-ui/icons/Close';

import styles from './closeButton.module.css';

interface ICloseButtonProps {
    onClick: () => void;
}

const CloseButton = ({ onClick }: ICloseButtonProps): JSX.Element => {
    return (
        <Button
            fullWidth
            variant="contained"
            classes={{ root: styles.closeButton }}
            startIcon={<Close classes={{ root: styles.closeButtonIcon }} />}
            onClick={onClick}
        >
            Close
        </Button>
    );
};

export default CloseButton;
