import React from 'react';
import classnames from 'classnames';
import { IOfferModel } from '../../../@generated/webExpApi/models';
import styles from './dealItem.module.css';

export interface IDealItemProps {
    item: IOfferModel;
    onClick?: (id: string) => void;
    isUnavailable?: boolean;
}

const DealItem = (props: IDealItemProps): JSX.Element => {
    const { item, onClick, isUnavailable } = props;
    const { name, description, userOfferId, imageUrl } = item;

    const handleRedeem = () => {
        if (typeof onClick === 'function') {
            onClick(userOfferId);
        }
    };

    const styleDealItem = classnames(styles.itemContainer, { [styles.unavailable]: isUnavailable });

    return (
        <div className={styleDealItem}>
            <div className={styles.imgColumn}>
                {imageUrl && <img src={imageUrl} className={styles.dealImage} alt="Deal" />}
            </div>
            <div className={styles.detailsColumn}>
                <span className={classnames('t-subheader-small', styles.dealHeader)}>{name}</span>
                <span className={styles.descriptionHeader}>{description}</span>
                <span
                    role="link"
                    className={classnames('link-secondary-active', styles.buttonRemove)}
                    onClick={handleRedeem}
                    data-gtm-id={`cartRedeem-${name}`}
                >
                    Redeem
                </span>
            </div>
        </div>
    );
};

export default DealItem;
