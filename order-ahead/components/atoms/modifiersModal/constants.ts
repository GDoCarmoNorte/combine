export const MULTIBRAND_CONFIG = {
    FOOTER_ICON: 'animals-chicken',
    STICKY_MOBILE_FOOTER: false,
    SHOW_MODAL_CLOSE_BTN_TEXT: true,
};
