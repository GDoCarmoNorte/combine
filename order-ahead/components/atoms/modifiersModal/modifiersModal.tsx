import React, { FC } from 'react';
import classnames from 'classnames';
import { Modal } from '@material-ui/core';

import { InspireButton } from '../button';
import TextWithTrademark from '../textWithTrademark';
import Icon from '../BrandIcon';
import { MULTIBRAND_CONFIG } from './constants';
import styles from './modifiersModal.module.css';
import { InspireLinkButton } from '../link';

interface IModifiersModalProps {
    onClose: (e) => void;
    isOpen: boolean;
    title: string;
    subtitle: string;
    hintText?: string;
    footerContent?: React.ReactElement;
}

const ModifiersModal: FC<IModifiersModalProps> = (props) => {
    const { isOpen, onClose, children, title, subtitle, hintText, footerContent } = props;
    const {
        FOOTER_ICON: footerIcon,
        STICKY_MOBILE_FOOTER: isMobileFooterSticky,
        SHOW_MODAL_CLOSE_BTN_TEXT,
    } = MULTIBRAND_CONFIG;

    const defaultFooterContent = (
        <>
            {footerIcon && <Icon className={styles.footerIcon} icon={footerIcon} />}
            <div className={styles.footerButtons}>
                <InspireButton className={styles.footerButton} onClick={onClose} type="primary" text="Save & Close" />
            </div>
        </>
    );
    return (
        <Modal open={isOpen} onClose={onClose} onClick={(e) => e.stopPropagation()} className={styles.modal}>
            <div className={styles.container}>
                <div className={styles.containerContent}>
                    <div className={styles.header}>
                        <div className={styles.headerTop}>
                            <InspireLinkButton
                                linkType="primary"
                                onClick={onClose}
                                className={styles.modalCloseBtn}
                                aria-label="Save & Close"
                            >
                                {SHOW_MODAL_CLOSE_BTN_TEXT && (
                                    <span className={styles.modalCloseBtnText}>Save & Close</span>
                                )}
                                <Icon onClick={onClose} icon="action-close" size="m" className={styles.closeIcon} />
                            </InspireLinkButton>
                        </div>
                        <div className="m-block-treatment m-block-treatment-lg">
                            <h1 className={classnames('m-block-treatment-subtitle', 't-subheader', styles.subtitle)}>
                                {subtitle}
                            </h1>
                            <div className={styles.titleWrapper}>
                                <TextWithTrademark
                                    tag="h2"
                                    text={title}
                                    className={classnames('m-block-treatment-title', styles.title)}
                                />
                                {hintText && (
                                    <span className={classnames(styles.hintText, 't-paragraph-small')}>{hintText}</span>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className={styles.content}>{children}</div>
                    <div className={classnames(styles.footer, { [styles.sticky]: isMobileFooterSticky })}>
                        {footerContent || defaultFooterContent}
                    </div>
                </div>
            </div>
        </Modal>
    );
};

export default ModifiersModal;
