import React, { useMemo } from 'react';
import Link from 'next/link';

import { IMenuCategory, IProduct, ITapListMenuCategory } from '../../@generated/@types/contentful';
import ContentfulImage from './ContentfulImage';

import styles from './sectionCard.module.css';
import SectionCardTextContent from './SectionCardTextContent/SectionCardTextContent';
import isTapListMenuCategory from '../../common/helpers/isTapListMenuCategory';
import classNames from 'classnames';
import { spaces2underscores } from '../../lib/gtm';

const isProductVisible = (product: IProduct) => product.fields?.isVisible;

export default function SectionCard(props: {
    isLocationOrderAvailable: boolean;
    card: IMenuCategory | ITapListMenuCategory;
}): JSX.Element {
    const { card } = props;
    const {
        image,
        categoryName,
        categoryDescription,
        link: {
            fields: { nameInUrl },
        },
    } = card.fields;

    const linkText = props.isLocationOrderAvailable ? 'ORDER' : 'VIEW ALL';
    const productCount = useMemo(() => {
        if (!isTapListMenuCategory(card)) {
            const { products, subcategories } = card.fields;
            const productsCount = products?.filter(isProductVisible)?.length || 0;
            const productsInSubcategoriesCount =
                subcategories?.reduce(
                    (acc, subcategory) => (subcategory?.fields?.products?.filter(isProductVisible)?.length || 0) + acc,
                    0
                ) || 0;

            return productsCount + productsInSubcategoriesCount;
        }
    }, [card]);

    const gtmId = spaces2underscores(categoryName);

    const menuCategoryLinkHref = '/menu/[menuCategoryUrl]';
    const menuCategoryLinkAs = `/menu/${nameInUrl}`;

    return (
        <Link href={menuCategoryLinkHref} as={menuCategoryLinkAs}>
            <a data-gtm-id={gtmId} className={styles.menuCategoryCard} aria-label="Menu Category">
                <div data-gtm-id={gtmId} className={styles.imageSection}>
                    <ContentfulImage gtmId={gtmId} asset={image} className={styles.image} maxWidth={187} />
                </div>
                <div data-gtm-id={gtmId} className={styles.contentSection}>
                    <h3 data-gtm-id={gtmId} className={classNames('t-header-h2', 'truncate-at-3', styles.title)}>
                        {categoryName}
                    </h3>
                    <SectionCardTextContent
                        gtmId={gtmId}
                        productCount={productCount}
                        linkText={linkText}
                        description={categoryDescription}
                    />
                </div>
            </a>
        </Link>
    );
}
