const getDisplayCalories = (calories?: number): string =>
    Number.isFinite(calories) ? `${calories} calories` : 'Calorie info unavailable';

export default getDisplayCalories;
