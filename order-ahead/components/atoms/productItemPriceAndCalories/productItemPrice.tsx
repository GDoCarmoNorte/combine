import React from 'react';
import Loader from '../Loader';
import { formatPrice } from '../../../lib/domainProduct';

interface IProductPriceProps {
    price: number;
    gtmId?: string;
    isLoading?: boolean;
    className?: string;
}

const ProductItemPrice = ({ className, isLoading, price, gtmId }: IProductPriceProps): JSX.Element => {
    if (isLoading) {
        return (
            <span>
                <Loader size={10} />
            </span>
        );
    }

    return (
        <span className={className} data-gtm-id={gtmId}>
            {formatPrice(price)}
        </span>
    );
};

export default ProductItemPrice;
