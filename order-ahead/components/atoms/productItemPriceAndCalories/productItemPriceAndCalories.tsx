import React from 'react';
import Loader from '../Loader';
import TextWithDotSeparator from '../TextWithDotSeparator';
import { formatPrice } from '../../../lib/domainProduct';
import getDisplayCalories from './getDisplayCalories';
import classnames from 'classnames';
import styles from './productItemPriceAndCalories.module.css';
import { isCaloriesDisplayEnabled } from '../../../lib/getFeatureFlags';

interface IProductItemPriceAndCalories {
    price?: number;
    calories?: number;
    gtmId?: string;
    isLoading?: boolean;
    className?: string;
}

const ProductItemPriceAndCalories = ({
    isLoading,
    calories,
    gtmId,
    className,
    price,
}: IProductItemPriceAndCalories): JSX.Element => {
    const ariaLabelContent = isCaloriesDisplayEnabled() ? 'Product Price and Calories' : 'Product Price';

    if (isLoading) {
        return (
            <span className={classnames(className, styles.wrapper)} data-gtm-id={gtmId} aria-label={ariaLabelContent}>
                <TextWithDotSeparator leftText={<Loader size={10} />} rightText={<Loader size={10} />} />
            </span>
        );
    }

    return (
        <span className={classnames(className, styles.wrapper)} data-gtm-id={gtmId} aria-label={ariaLabelContent}>
            <TextWithDotSeparator
                leftText={Number.isFinite(price) ? formatPrice(price) : null}
                rightText={isCaloriesDisplayEnabled() ? getDisplayCalories(calories) : ''}
            />
        </span>
    );
};

export default ProductItemPriceAndCalories;
