import React from 'react';
import classnames from 'classnames';

import styles from './badge.module.css';

interface IInspireBadgeProps {
    className?: string;
    value: string;
    theme?: 'dark';
}

const InspireBadge = ({ className, value, theme = 'dark' }: IInspireBadgeProps): JSX.Element => {
    return <div className={classnames(styles[className], { [styles.dark]: theme === 'dark' })}>{value}</div>;
};

export default InspireBadge;
