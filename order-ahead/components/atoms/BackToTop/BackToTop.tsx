import React from 'react';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import styles from './BackToTop.module.css';

const BackToTop = (): JSX.Element => (
    <div className={styles.backToTopWrapper}>
        <a className={styles.backToTop} href="#">
            <ExpandLessIcon />
            Back to top
        </a>
    </div>
);

export default BackToTop;
