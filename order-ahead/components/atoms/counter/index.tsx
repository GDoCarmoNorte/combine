import React from 'react';
import styles from './index.module.css';
import classnames from 'classnames';

interface ICounter {
    value: number | string;
    ariaLabel?: string;
    counterClassName?: string;
}

const Counter = ({ value, counterClassName, ariaLabel, ...props }: ICounter): JSX.Element => {
    return (
        <span
            {...props}
            className={classnames('t-paragraph-hint-strong', styles.counter, {
                [counterClassName]: counterClassName,
            })}
            aria-label={ariaLabel}
        >
            {value}
        </span>
    );
};

export default Counter;
