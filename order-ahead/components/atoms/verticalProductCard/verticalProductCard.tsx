import React from 'react';
import classnames from 'classnames';

import { IProduct, ISodiumWarningFields } from '../../../@generated/@types/contentful';
import { useBag, useDomainMenu } from '../../../redux/hooks';
import {
    useProductIsCombo,
    useProductCategory,
    useDiscountPriceAndCalories,
    useProductItemGroup,
    useDefaultTallyItem,
    useTallyPriceAndCalories,
    useDomainProductWithRootIdByContentfulFields,
    useDomainProductByContentfulFields,
} from '../../../redux/hooks/domainMenu';

import ProductItemControls from '../../clientOnly/productItemControls';

import ContentfulImage from '../ContentfulImage';
import { ProductItemPriceAndCalories } from '../productItemPriceAndCalories';
import { useProductOrderAheadAvailability } from '../../../common/hooks/useProductOrderAheadAvailability';
import TextWithTrademark from '../textWithTrademark';
import { InspireLink } from '../link';
import { useProductIsSaleable } from '../../../common/hooks/useProductIsSaleable';

import styles from './verticalProductCard.module.css';
import InspireBadge from '../Badge';
import { useProductIsAvailable } from '../../../common/hooks/useProductIsAvailable';
import { InspireCmsEntry } from '../../../common/types';
import { BOGOS_LABELS_MAP } from './constants';
import { useTallyItemHasSodiumWarning } from '../../../common/hooks/useTallyItemHasSodiumWarning';
import getBrandInfo from '../../../lib/brandInfo';
interface ITopPicksItemProps {
    item: IProduct;
    recommendationId?: string;
    isSideScroll: boolean;
    category: string;
    className?: string;
    gtmId?: string;
    onModify?: () => void;
    containerClass?: string;
    addClass?: string;
    modifyClass?: string;
    crossSellGtmId?: string;
    sodiumWarning?: InspireCmsEntry<ISodiumWarningFields>;
    categoryId?: string;
}

const VerticalProductCard = (props: ITopPicksItemProps): JSX.Element => {
    const {
        item,
        recommendationId,
        isSideScroll,
        category,
        className,
        gtmId = 'productItem',
        onModify,
        containerClass = '',
        addClass = '',
        modifyClass = '',
        crossSellGtmId,
        sodiumWarning,
        categoryId,
    } = props;

    const { brandId } = getBrandInfo();

    const { fields } = item;
    const { name, image, showProductBadge } = fields;

    const getProduct =
        brandId === 'Bww' ? useDomainProductWithRootIdByContentfulFields : useDomainProductByContentfulFields;
    const product = getProduct(fields);

    const productId = product?.id;

    const productItemGroup = useProductItemGroup(productId);

    const bogoLabel = BOGOS_LABELS_MAP[productItemGroup?.name]?.();

    const defaultTallyItem = useDefaultTallyItem(productId);

    const productHasSodiumWarning = useTallyItemHasSodiumWarning(defaultTallyItem);
    const showSodiumWarning = !!sodiumWarning && productHasSodiumWarning;

    const productBadgeCategory = useProductCategory(productId);

    const productName = product && 'menuItemName' in product ? product['menuItemName'] : product?.name || name || '';

    const { price } = useDiscountPriceAndCalories(productId);

    const { calories } = useTallyPriceAndCalories(defaultTallyItem, false);

    const isCombo = useProductIsCombo(productId);

    const { isOrderAheadAvailable } = useProductOrderAheadAvailability(productId);

    const bag = useBag();

    const handleAddToBag = () => {
        bag.actions.addDefaultToBag({ productId, name: productName, category, recommendationId });
    };

    const handleLinkClick = () => {
        if (onModify) {
            onModify();
        }
    };

    const verticalProductCardClassName = classnames(containerClass, className, styles.verticalProductCard, {
        [styles.verticalProductCardGrid]: !isSideScroll,
        [styles.verticalProductCardSideScroll]: isSideScroll,
    });
    const { loading } = useDomainMenu();

    const crossSellOrRegGtmId = crossSellGtmId || gtmId;

    const { isSaleable } = useProductIsSaleable(productId);
    const { isAvailable } = useProductIsAvailable(productId, categoryId);

    const categoryUrl = category === 'recommended-items' ? '' : category;

    return (
        <div data-gtm-id={crossSellOrRegGtmId} className={verticalProductCardClassName} aria-label="Product Card">
            {showProductBadge && productBadgeCategory && productBadgeCategory.name && (
                <InspireBadge className="productBadge" value={productBadgeCategory.name} />
            )}
            {bogoLabel && <InspireBadge className="bogoBadge" value={bogoLabel} />}
            <InspireLink link={item} currentCategoryUrl={categoryUrl} onClick={handleLinkClick}>
                <ContentfulImage
                    asset={image}
                    maxWidth={200}
                    className={styles.itemImage}
                    gtmId={crossSellOrRegGtmId}
                />
                <div data-gtm-id={crossSellOrRegGtmId} className={styles.itemDescription}>
                    <TextWithTrademark
                        tag="span"
                        text={productName}
                        title={productName}
                        className={classnames(styles.itemName, 'truncate-at-3')}
                        data-gtm-id={crossSellOrRegGtmId}
                        afterContent={
                            showSodiumWarning && (
                                <img
                                    key="sodium"
                                    className={styles.sodiumWarningIcon}
                                    src={sodiumWarning.fields.icon.fields.icon.fields.file.url}
                                    alt="Sodium warning label"
                                />
                            )
                        }
                    />
                    <ProductItemPriceAndCalories
                        isLoading={loading}
                        gtmId={crossSellOrRegGtmId}
                        price={isSaleable && isOrderAheadAvailable ? price : null}
                        calories={calories}
                        className={classnames('t-paragraph-hint', styles.caloriesAndPrice)}
                    />
                </div>
            </InspireLink>
            <div data-gtm-id={crossSellOrRegGtmId} className={styles.itemActions}>
                <ProductItemControls
                    isOrderAheadAvailable={isOrderAheadAvailable}
                    productDetails={product}
                    product={item}
                    isSaleable={isSaleable}
                    isAvailable={isAvailable}
                    isCombo={isCombo}
                    onClick={handleAddToBag}
                    gtmId={crossSellGtmId || `${productId}|${productName}`}
                    buttonClassName={styles.productItemButton}
                    onModify={onModify}
                    addClass={addClass}
                    modifyClass={modifyClass}
                />
            </div>
        </div>
    );
};

export default VerticalProductCard;
