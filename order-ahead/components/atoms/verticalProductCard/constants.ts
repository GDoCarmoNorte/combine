import { ItemGroupEnum } from '../../../redux/types';

export const BOGOS_LABELS_MAP = {
    [ItemGroupEnum.BOGO]: () => `BOGO`,
    [ItemGroupEnum.BOG6]: () => 'BOGO 6 FREE',
    [ItemGroupEnum.BOGO_50_OFF]: () => `BOGO 50% OFF`,
};
