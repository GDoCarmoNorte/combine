import React from 'react';
import classNames from 'classnames';

import HScroller from '../HScroller';
import DealItem from '../dealItem';
import { isOfferModel } from '../../../redux/hooks/useRewards';
import { isAccountDealsPageOn } from '../../../lib/getFeatureFlags';

import styles from './userDeals.module.css';
import { Offers } from '../../../redux/rewards';
import { PayloadAction } from '@reduxjs/toolkit';
import { AddDealToBagPayload } from '../../../redux/bag';

export interface IUserDealsProps {
    offers: Offers;
    isCurrentLocationOAAvailable: boolean;
    onAddDealToBag: (payload: AddDealToBagPayload) => PayloadAction<AddDealToBagPayload>;
    dealId: string;
}

const UserDeals = ({ offers, isCurrentLocationOAAvailable, onAddDealToBag, dealId }: IUserDealsProps): JSX.Element => {
    if (!offers.length || !isAccountDealsPageOn()) {
        return null;
    }

    const handleDealItemClick = (id: string) => {
        onAddDealToBag({ id });
    };

    const renderItems = () => {
        return offers
            .filter(isOfferModel)
            .filter((item) => dealId !== item.userOfferId && !item.isRedeemableInStoreOnly)
            .map((offer) => (
                <DealItem
                    key={offer.userOfferId}
                    isUnavailable={!isCurrentLocationOAAvailable}
                    item={offer}
                    onClick={handleDealItemClick}
                />
            ));
    };

    return (
        <div className={styles.dealsContainer}>
            <h4 className={classNames('t-header-h3', styles.dealsHeading)}>redeem your deals</h4>
            <HScroller
                className={styles.carouselContainer}
                scrollStep={340}
                prevButtonClassName={styles.leftArrow}
                nextButtonClassName={styles.rightArrow}
                itemType="deal"
            >
                {renderItems()}
            </HScroller>
        </div>
    );
};

export default UserDeals;
