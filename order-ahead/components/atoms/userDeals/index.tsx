import React from 'react';
import { useBag, useOrderLocation, useRewards } from '../../../redux/hooks';

import UserDeals from './userDeals';

const UserDealsContainer = (): JSX.Element => {
    const { offers } = useRewards();
    const { actions, dealId } = useBag();
    const { isCurrentLocationOAAvailable } = useOrderLocation();

    return (
        <UserDeals
            offers={offers}
            isCurrentLocationOAAvailable={isCurrentLocationOAAvailable}
            onAddDealToBag={actions.addDealToBag}
            dealId={dealId}
        />
    );
};

export default UserDealsContainer;
