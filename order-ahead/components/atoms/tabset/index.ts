export { default as InspireTabSet } from './tabset';
export { default as InspireTab } from './tab';
export { default as InspireTabPanel } from './tabPanel';
