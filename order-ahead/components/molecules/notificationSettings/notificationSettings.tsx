import React, { useState } from 'react';

import classnames from 'classnames';
import styles from './notificationSettings.module.css';
import NotificationModal from '../notificationModal';
import Checkbox from '../../atoms/checkbox';
import { IMarketingModel } from '../../../@generated/webExpApi/models';
import RadioButton from '../../atoms/radioButton';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

interface INotificationSettings {
    marketing?: IMarketingModel[] | [];
    onChange: (marketing: IMarketingModel[] | []) => void;
}

export default function NotificationSettings(props: INotificationSettings): JSX.Element {
    const { marketing, onChange } = props;
    const isDealsSelected = marketing && marketing[0]?.type === 'EMAIL';
    const [selectedSetting, setSelectedSetting] = useState(null);

    const onClose = () => {
        setSelectedSetting(null);
    };

    const onConfirm = () => {
        onChange([]);
        setSelectedSetting(null);
    };

    const toggleCheckbox = (fieldName) => {
        if (isDealsSelected) {
            setSelectedSetting(fieldName);
        } else {
            const marketing = [
                {
                    type: 'EMAIL',
                },
            ];
            onChange(marketing);
        }
    };

    return (
        <div className={styles.settingsContainer}>
            <div className={styles.notificationsHeader}>
                <h2 className={classnames('t-header-h1', styles.notificationHeader)}>Notification Settings</h2>
            </div>
            <div className={styles.notificationItem}>
                <div className={styles.notificationItemNameColumn}>
                    <span className={classnames('t-subheader-small', styles.itemNameHeading)}>deals</span>
                    <span className={styles.itemNameSubHeading}>Notifications on new and expiring deals</span>
                </div>
                <div className={styles.notificationCheckboxColumn}>
                    <Checkbox
                        className={styles.notificationCheckbox}
                        fieldName="deals"
                        onClick={toggleCheckbox}
                        selected={!!isDealsSelected}
                        ariaLabel={isDealsSelected ? 'Selected Checkbox' : 'Non Selected Checkbox'}
                    />
                    <span className={styles.label}>Email</span>
                </div>
            </div>
            <NotificationModal
                open={selectedSetting !== null}
                onModalClose={onClose}
                title="unsubscribe?"
                message="We hate to see you go! Subscribe anytime again."
                confirmButtonProps={{
                    text: 'Submit',
                    onClick: onConfirm,
                }}
                rejectButtonProps={{
                    text: 'No, return to my accounts',
                    onClick: onClose,
                }}
            >
                <RadioGroup defaultValue="unsubscribe">
                    <FormControlLabel
                        className={styles.radioButtonLabel}
                        value="unsubscribe"
                        control={<RadioButton className={styles.radioButton} />}
                        label={<span className={styles.label}>{'Unsubscribe me from your email list'}</span>}
                    />
                </RadioGroup>
            </NotificationModal>
        </div>
    );
}
