import React, { FC } from 'react';
import styles from './notification.module.css';

const Notification: FC = ({ children }): JSX.Element => {
    return <div className={styles.notificationContainer}>{children}</div>;
};

export default Notification;
