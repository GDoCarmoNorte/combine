import React, { FC } from 'react';
import classnames from 'classnames';
import styles from '../rewardDetailsModal.module.css';

type ICatalogRedeemedCertificateViewProps = {
    title: string;
    imageUrl: string;
    memberNumber: string;
    expirationLabel: string;
};

export const CatalogRedeemedCertificateView: FC<ICatalogRedeemedCertificateViewProps> = ({
    title,
    imageUrl,
    memberNumber,
    expirationLabel,
}) => {
    return (
        <section className={styles.mainContent}>
            <div className={styles.contentWrapper}>
                <div className={styles.mainInfo}>
                    <h1 className={classnames('t-header-h1', styles.title)}>{title}</h1>
                    <span className={styles.displayPoints}>Reward has been redeem. Find it in your account</span>
                    <p className={classnames('t-paragraph-small', styles.date)}>{expirationLabel}</p>
                </div>
                <div className={styles.imageContainer}>
                    <img src={imageUrl} className={styles.itemImage} alt={title} />
                </div>
                <div className={classnames('t-paragraph-hint', styles.terms)}>
                    <span className={classnames('t-paragraph-hint-strong', styles.infoHeader)}>
                        Dine-In & Order Reward
                    </span>
                    <ol className={styles.infoList}>
                        <li>Get to Buffalo Wild Wings and order your reward.</li>
                        <li>Give the server your member number before paying.</li>
                    </ol>
                    <span className={classnames('t-paragraph-hint-strong', styles.infoHeader)}>
                        {`Member Number: ${memberNumber}`}
                    </span>
                    <p className={styles.infoText}>
                        {`Redemption includes one order of ${title}. This item may not be available for redemption in
                  all Buffalo Wild Wings Locations. Please check with your location for availability.`}
                    </p>
                </div>
            </div>
        </section>
    );
};
