import React, { FC } from 'react';
import Modal from '@material-ui/core/Modal';
import classnames from 'classnames';

import styles from './rewardDetailsModal.module.css';

import isSmallScreen from '../../../lib/isSmallScreen';
import isMobileScreen from '../../../lib/isMobileScreen';
import Icon from '../../atoms/BrandIcon';
import BrandLoader from '../../atoms/BrandLoader';

interface IDetailsModal {
    open: boolean;
    isLoading?: boolean;
    modalTitle: string;
    onClose?: () => void;
    renderButtons?: () => JSX.Element;
}

const RewardDetailsModal: FC<IDetailsModal> = ({
    open,
    modalTitle,
    isLoading = false,
    onClose,
    renderButtons = () => null,
    children,
}) => {
    const smallScreen = isSmallScreen();
    const mobileScreen = isMobileScreen();

    return (
        <Modal open={open} className={styles.modal} onClose={onClose}>
            <div className={styles.content}>
                <button aria-label="Close Icon" className={styles.closeButton} onClick={onClose}>
                    <Icon
                        icon="action-close"
                        size={smallScreen || mobileScreen ? 's' : 'm'}
                        className={styles.closeIcon}
                    />
                </button>
                <section className={styles.header}>
                    <h1 className={classnames('t-header-h1', styles.modalTitle)}>{modalTitle}</h1>
                </section>
                {children}
                <div className={styles.buttonsBlock}>{renderButtons()}</div>
                {isLoading && <BrandLoader className={styles.loader} />}
            </div>
        </Modal>
    );
};

export default RewardDetailsModal;
