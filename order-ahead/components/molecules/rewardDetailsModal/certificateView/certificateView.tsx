import React, { FC } from 'react';
import classnames from 'classnames';
import styles from '../rewardDetailsModal.module.css';

type ICertificateViewProps = {
    title: string;
    expirationLabel: string;
    imageUrl: string;
    isActive: boolean;
    isPendingCancellation?: boolean;
};

export const CertificateView: FC<ICertificateViewProps> = ({
    title,
    expirationLabel,
    imageUrl,
    isActive,
    isPendingCancellation,
}) => {
    return (
        <section className={styles.mainContent}>
            <div className={styles.contentWrapper}>
                <div className={styles.mainInfo}>
                    <h1 className={classnames('t-header-h1', styles.title)}>{title}</h1>
                    <div className={styles.tags}>
                        {isActive && <span className={classnames('t-tag', styles.tag)}>ACTIVE REWARD</span>}
                    </div>
                    {isPendingCancellation && (
                        <div className={styles.cancellationInfo}>
                            <p className={classnames('t-paragraph', styles.cancellationStatus)}>CANCELLATION PENDING</p>
                            <p className={classnames('t-paragraph-small', styles.cancellationTips)}>
                                This Reward Certificate is pending cancellation and cannot be used. Your point total
                                will be updated to reflect points refund within 24 hours.
                            </p>
                        </div>
                    )}
                    <p className={classnames('t-paragraph-small', styles.date)}>{expirationLabel}</p>
                </div>
                <div className={styles.imageContainer}>
                    <img src={imageUrl} className={styles.itemImage} alt={title} />
                </div>
                <div className={classnames('t-paragraph-hint', styles.terms)}>
                    <span className={classnames('t-paragraph-hint-strong', styles.infoHeader)}>
                        Dine-In & Order Reward
                    </span>
                    <ol className={styles.infoList}>
                        <li>Get to Buffalo Wild Wings and order your reward.</li>
                        <li>Give the server your member number before paying.</li>
                    </ol>
                    <p className={styles.infoText}>
                        {`Redemption includes one order of ${title}. This item may not be available for redemption in
                    all Buffalo Wild Wings Locations. Please check with your location for availability.`}
                    </p>
                </div>
            </div>
        </section>
    );
};
