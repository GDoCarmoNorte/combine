import React, { FC } from 'react';
import classnames from 'classnames';
import styles from '../rewardDetailsModal.module.css';

type IOfferViewProps = {
    title: string;
    dateLabel: string;
    dateText: string;
    imageUrl: string;
    terms: string;
    isActive: boolean;
};

export const OfferView: FC<IOfferViewProps> = ({ title, dateLabel, dateText, imageUrl, terms, isActive }) => {
    return (
        <section className={styles.mainContent}>
            <div className={styles.contentWrapper}>
                <div className={styles.mainInfo}>
                    <h1 className={classnames('t-header-h1', styles.title)}>{title}</h1>
                    <div className={styles.tags}>
                        {isActive && <span className={classnames('t-tag', styles.tag)}>ACTIVE OFFER</span>}
                    </div>
                    <p className={classnames('t-paragraph', styles.dateLabel)}>{dateLabel}</p>
                    <p className={classnames('t-paragraph-small', styles.date)}>{dateText}</p>
                </div>
                <div className={styles.imageContainer}>
                    <img src={imageUrl} className={styles.itemImage} alt={title} />
                </div>
                <div className={classnames('t-paragraph-hint', styles.terms)}>{terms}</div>
            </div>
        </section>
    );
};
