import React, { FC } from 'react';
import classnames from 'classnames';
import styles from '../rewardDetailsModal.module.css';

type ICatalogCertificateViewProps = {
    title: string;
    imageUrl: string;
    renderPointsLabel: () => JSX.Element;
};

export const CatalogCertificateView: FC<ICatalogCertificateViewProps> = ({ title, imageUrl, renderPointsLabel }) => {
    return (
        <section className={styles.mainContent}>
            <div className={styles.contentWrapper}>
                <div className={styles.mainInfo}>
                    <h1 className={classnames('t-header-h1', styles.catalogViewTitle)}>{title}</h1>
                    {renderPointsLabel()}
                </div>
                <div className={styles.imageContainer}>
                    <img src={imageUrl} className={styles.itemImage} alt={title} />
                </div>
                <div className={classnames('t-paragraph-hint', styles.terms)}>
                    {`Redemption includes one order of ${title}. This item may not be available for redemption in
                  all Buffalo Wild Wings Locations. Please check with your location for availability.`}
                </div>
            </div>
        </section>
    );
};
