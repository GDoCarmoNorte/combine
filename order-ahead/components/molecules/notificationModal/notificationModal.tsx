import React, { FC } from 'react';
import Modal from '@material-ui/core/Modal';
import classnames from 'classnames';

import styles from './notificationModal.module.css';

import { InspireButton, IInspireButtonProps } from '../../atoms/button';

import Icon from '../../atoms/BrandIcon';

interface INotificationModal {
    open: boolean;
    title: string;
    message: string;
    confirmButtonProps: IInspireButtonProps;
    rejectButtonProps: IInspireButtonProps;
    onModalClose?: () => void;
    modalId?: string;
}

const NotificationModal: FC<INotificationModal> = ({
    open,
    title,
    message,
    confirmButtonProps,
    rejectButtonProps,
    onModalClose,
    children,
}) => {
    return (
        <Modal open={open} className={styles.modal} onClose={onModalClose}>
            <div className={styles.content}>
                <Icon onClick={onModalClose} icon="action-close" size="m" className={styles.closeIcon} />
                <p className={classnames('t-subheader', styles.subheader)}>are you sure you want to</p>
                <p className={classnames(styles.header, 't-header-h1')}>{title}</p>
                <p className={classnames('t-paragraph', styles.message)}>{message}</p>
                {children}
                <div className={styles.buttons}>
                    <InspireButton className={styles.button} type="primary" {...rejectButtonProps} />
                    <InspireButton className={styles.button} type="primary" {...confirmButtonProps} />
                </div>
            </div>
        </Modal>
    );
};

export default NotificationModal;
