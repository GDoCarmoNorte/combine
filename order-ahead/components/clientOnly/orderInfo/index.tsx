import React from 'react';
import dynamic from 'next/dynamic';

import Loader from '../../atoms/Loader';

import styles from './orderInfo.module.css';

const OrderInfo = dynamic(import('./orderInfo'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => (
        <div className={styles.pickupTime}>
            <Loader size={10} />
        </div>
    ),
});

export default OrderInfo;
