import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

import { parseISO, format, isToday, utcToZonedTime } from '../../../common/helpers/dateTime';

import useSubmitOrder from '../../../redux/hooks/useSubmitOrder';
import InfoBanner from '../../../components/atoms/infoBanner';
import { useOrderLocation } from '../../../redux/hooks';
import { OrderLocationMethod } from '../../../redux/orderLocation';

const getPickupLabel = (timezone: string, rawPickupTime?: string): string => {
    if (rawPickupTime) {
        const pickupDate = utcToZonedTime(parseISO(rawPickupTime), timezone);
        const dayLabel = isToday(pickupDate) ? 'Today' : format(pickupDate, 'MM/dd/yyyy');

        return `${dayLabel} at ${format(pickupDate, 'hh:mm a')}`;
    }

    return '';
};

function OrderInfo({ method, backgroundColor }: { method: OrderLocationMethod; backgroundColor: string }): JSX.Element {
    const router = useRouter();
    const { currentLocation: location } = useOrderLocation();
    const orderId = parseInt(String(router.query.id));

    const { lastOrder } = useSubmitOrder();

    useEffect(() => {
        if (isNaN(orderId)) {
            router.replace('/');
            return;
        }
        // TODO: remove order retrieval error (https://inspirebrands.atlassian.net/browse/DBBP-47476)
    }, [lastOrder, router, orderId]);

    const rawPickupTime =
        lastOrder && lastOrder.orderId === String(orderId) ? lastOrder.fulfillment.time.toISOString() : null;

    const confirmationId = orderId;
    const pickupLabel = getPickupLabel(location.timezone, rawPickupTime);

    return (
        <>
            <InfoBanner
                title={`your order will be ready for ${method}`}
                mainText={pickupLabel}
                description={`Confirmation #${confirmationId}`}
                backgroundColor={backgroundColor}
            />
        </>
    );
}

export default OrderInfo;
