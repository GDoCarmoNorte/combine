import React from 'react';
import { DeleteAccountHeader } from '../header/header';
import styles from './layout.module.css';

interface DeleteAccountLayoutProps {
    buttons?: React.ReactNode[];
}

const DeleteAccountLayout: React.FC<DeleteAccountLayoutProps> = ({ children, buttons = [] }) => {
    return (
        <>
            <DeleteAccountHeader mainText={'Delete account'} />
            <div className={styles.container}>{children}</div>
            <div className={styles.buttons}>{buttons}</div>
        </>
    );
};

export default DeleteAccountLayout;
