import React, { useEffect } from 'react';
import styles from './deleteSuccess.module.css';
import { TITLE_TEXT, MAIN_TEXT, BUTTON_TEXT } from './constants';
import classnames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import { IDeleteSuccessProps } from './types';
import DeleteAccountLayout from '../layout/layout';
import { useAppDispatch } from '../../../../redux/store';
import { GTM_ACCOUNT_DELETED_SUBMITTED } from '../../../../common/services/gtmService/constants';
import {
    GtmCategory,
    GtmActionFields,
    AccountDeletedSubmittedData,
} from '../../../../common/services/gtmService/types';

export default function DeleteSuccess(props: IDeleteSuccessProps): JSX.Element {
    const { onFinish } = props;
    const dispatch = useAppDispatch();

    useEffect(() => {
        const payload = {
            category: GtmCategory.ACCOUNT,
            action: GtmActionFields.Delete,
            label: 'Confirmation',
        } as AccountDeletedSubmittedData;

        dispatch({ type: GTM_ACCOUNT_DELETED_SUBMITTED, payload });
    }, [dispatch]);

    return (
        <DeleteAccountLayout
            buttons={[
                <InspireButton
                    key={0}
                    onClick={onFinish}
                    type="primary"
                    text={BUTTON_TEXT}
                    className={styles.button}
                />,
            ]}
        >
            <div>
                <div className={'t-header-h2'}> {TITLE_TEXT} </div>

                {MAIN_TEXT.map((text, i) => (
                    <div className={classnames('t-paragraph', styles.main)} key={'mainText_' + i}>
                        {text}
                    </div>
                ))}
            </div>
        </DeleteAccountLayout>
    );
}
