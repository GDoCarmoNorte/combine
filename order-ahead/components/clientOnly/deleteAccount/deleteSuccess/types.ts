export interface IDeleteSuccessProps {
    onFinish(): void;
}
