export const TITLE_TEXT = 'Delete My Account';
export const MAIN_TEXT = [
    'This confirms that your account has been deleted.',
    'We appreciate your patronage and hope to see you again some day!',
];
export const BUTTON_TEXT = 'Finish';
