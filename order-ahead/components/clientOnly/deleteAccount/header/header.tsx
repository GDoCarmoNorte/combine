import React, { FC } from 'react';
import InfoBanner, { IInfoBanner } from '../../../atoms/infoBanner';
import styles from './header.module.css';

export const DeleteAccountHeader: FC<IInfoBanner> = (props) => {
    return <InfoBanner {...props} wrapperClassName={styles.banner} />;
};
