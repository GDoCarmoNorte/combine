import React from 'react';
import styles from './deletePermissions.module.css';
import {
    TITLE_TEXT,
    MAIN_TEXT,
    DELETE_PERSONAL_DATA_TEXT,
    DELETE_CONFIRMATION_TEXT,
    CANCEL_BUTTON_TEXT,
    SUBMIT_BUTTON_TEXT,
} from './constants';
import classnames from 'classnames';
import Checkbox from '../../../atoms/checkbox';
import { InspireButton } from '../../../atoms/button';

import { IDeletePermissionsProps } from './types';
import DeleteAccountLayout from '../layout/layout';

export default function DeletePermissions(props: IDeletePermissionsProps): JSX.Element {
    const {
        onToggleDeletePersonalData,
        onToggleDeleteConfirmation,
        onCancel,
        onSubmit,
        deletePersonalData,
        deleteConfirmation,
    } = props;

    return (
        <DeleteAccountLayout
            buttons={[
                <InspireButton
                    key={0}
                    onClick={onCancel}
                    type="secondary"
                    text={CANCEL_BUTTON_TEXT}
                    className={classnames(styles.button, styles.buttonSpacer)}
                />,
                <InspireButton
                    key={1}
                    onClick={onSubmit}
                    type="primary"
                    text={SUBMIT_BUTTON_TEXT}
                    className={styles.button}
                    disabled={!deleteConfirmation}
                />,
            ]}
        >
            <div>
                <div className={'t-header-h2'}> {TITLE_TEXT} </div>
                <div className={classnames('t-paragraph', styles.main)}> {MAIN_TEXT} </div>

                <div className={styles.checkBoxContainer}>
                    <input
                        className={styles.checkBox}
                        type="checkbox"
                        id="deletePersonalData"
                        checked={deletePersonalData}
                        onChange={onToggleDeletePersonalData}
                    />
                    <Checkbox
                        fieldName="deletePersonalData"
                        onClick={onToggleDeletePersonalData}
                        selected={deletePersonalData}
                        ariaLabel={DELETE_PERSONAL_DATA_TEXT}
                    />
                    <label className={classnames(styles.checkBoxLabel, 't-paragraph')} htmlFor="deletePersonalData">
                        {DELETE_PERSONAL_DATA_TEXT}
                    </label>
                </div>

                <div className={styles.checkBoxContainer}>
                    <input
                        className={styles.checkBox}
                        type="checkbox"
                        id="deleteConfirmation"
                        checked={deleteConfirmation}
                        onChange={onToggleDeleteConfirmation}
                    />
                    <Checkbox
                        fieldName="deleteConfirmation"
                        onClick={onToggleDeleteConfirmation}
                        selected={deleteConfirmation}
                        ariaLabel={DELETE_CONFIRMATION_TEXT}
                    />
                    <label className={classnames(styles.checkBoxLabel, 't-paragraph')} htmlFor="deleteConfirmation">
                        {DELETE_CONFIRMATION_TEXT}
                    </label>
                </div>
            </div>
        </DeleteAccountLayout>
    );
}
