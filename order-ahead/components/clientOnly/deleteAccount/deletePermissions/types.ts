export interface IDeletePermissionsProps {
    onToggleDeletePersonalData(): void;
    onToggleDeleteConfirmation(): void;
    onCancel(): void;
    onSubmit(): void;
    deletePersonalData: boolean;
    deleteConfirmation: boolean;
}
