export const TITLE_TEXT = 'Delete My Account';
export const MAIN_TEXT = `You have the right to request that Arby’s delete the personal information that Arby’s has collected from you, subject to certain exceptions as permitted by law.`;
export const DELETE_PERSONAL_DATA_TEXT = `Check here if you would like Arby’s to delete all of the personal information we have collection from you (subject to permitted exceptions).`;
export const DELETE_CONFIRMATION_TEXT = `I understand that by deleting my information, I will no longer be able to use my rewards and will forfeit any accrued points.`;
export const CANCEL_BUTTON_TEXT = 'Cancel';
export const SUBMIT_BUTTON_TEXT = 'Submit Request';
