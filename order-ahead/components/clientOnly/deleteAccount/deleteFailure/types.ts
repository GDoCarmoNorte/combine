export interface IDeleteFailureProps {
    onRetry(): void;
}
