import React from 'react';
import styles from './deleteFailure.module.css';
import { TITLE_TEXT, PRE_LINK_TEXT, LINK_TEXT, LINK, RETRY_BUTTON_TEXT } from './constants';
import classnames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import { IDeleteFailureProps } from './types';
import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';
import DeleteAccountLayout from '../layout/layout';

export default function DeleteFailure(props: IDeleteFailureProps): JSX.Element {
    const { onRetry } = props;

    return (
        <DeleteAccountLayout
            buttons={[
                <InspireButton
                    key={0}
                    onClick={onRetry}
                    type="primary"
                    text={RETRY_BUTTON_TEXT}
                    className={styles.button}
                />,
            ]}
        >
            <div>
                <div className={'t-header-h2'}> {TITLE_TEXT} </div>
                <div className={classnames('t-paragraph', styles.main)}>
                    {PRE_LINK_TEXT}
                    <InspireSimpleLink
                        type="primary"
                        link={LINK}
                        newtab
                        text={LINK_TEXT}
                        className={styles.inlineLink}
                    />
                </div>
            </div>
        </DeleteAccountLayout>
    );
}
