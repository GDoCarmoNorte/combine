import React from 'react';
import styles from './feedback.module.css';
import {
    TITLE_TEXT,
    MAIN_TEXT,
    FEEDBACK_QUESTION_TEXT,
    FEEDBACK_REASONS,
    CANCEL_BUTTON_TEXT,
    CONTINUE_BUTTON_TEXT,
    IS_QUESTION_TEXT_BOLD,
} from './constants';
import classnames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import DeleteAccountLayout from '../layout/layout';
import { useAppDispatch } from '../../../../redux/store';
import { GTM_ACCOUNT_DELETED_FEEDBACK } from '../../../../common/services/gtmService/constants';
import { GtmCategory, GtmActionFields, AccountDeletedFeedbackData } from '../../../../common/services/gtmService/types';
import RadioButton from '../../../atoms/radioButton';

export interface IFeedbackProps {
    onSetFeedback(feedback: string): void;
    onCancel(): void;
    onContinue(): void;
    feedback: string | null;
}

export default function Feedback(props: IFeedbackProps): JSX.Element {
    const dispatch = useAppDispatch();
    const { onSetFeedback, onCancel, onContinue, feedback } = props;

    const handleContinue = (): void => {
        onContinue();
        const payload = {
            category: GtmCategory.ACCOUNT,
            action: GtmActionFields.Delete,
            reason_1: feedback,
        } as AccountDeletedFeedbackData;

        dispatch({ type: GTM_ACCOUNT_DELETED_FEEDBACK, payload });
    };

    const buildRadioButtons = (): Array<JSX.Element> =>
        FEEDBACK_REASONS.map((reason, i) => {
            const radioBtnName = 'deleteAccountReasonRadioBtn_' + i;

            return (
                <div className={styles.radioBtnContainer} key={radioBtnName}>
                    <RadioButton
                        id={radioBtnName}
                        onClick={() => onSetFeedback(reason)}
                        checked={feedback === reason}
                        aria-label={reason}
                    />
                    <label className={classnames(styles.radioBtnLabel, 't-paragraph')} htmlFor={radioBtnName}>
                        {reason}
                    </label>
                </div>
            );
        });

    return (
        <DeleteAccountLayout
            buttons={[
                <InspireButton key={0} onClick={onCancel} type="secondary" text={CANCEL_BUTTON_TEXT} />,
                <InspireButton
                    key={1}
                    onClick={handleContinue}
                    type="primary"
                    text={CONTINUE_BUTTON_TEXT}
                    disabled={!feedback}
                />,
            ]}
        >
            <div>
                <div className={'t-header-h2'}> {TITLE_TEXT} </div>
                <div className={classnames('t-paragraph', styles.main)}> {MAIN_TEXT} </div>

                <div
                    className={classnames(
                        IS_QUESTION_TEXT_BOLD ? 't-paragraph-small-strong' : 't-paragraph-small',
                        styles.feedbackQuestionText
                    )}
                >
                    {FEEDBACK_QUESTION_TEXT}
                </div>
                {buildRadioButtons()}
            </div>
        </DeleteAccountLayout>
    );
}
