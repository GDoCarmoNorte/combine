export const TITLE_TEXT = 'Feedback';
export const MAIN_TEXT = 'Before you go, please tell us how we could improve.';
export const FEEDBACK_QUESTION_TEXT = 'Why are you deleting your account?';
export const FEEDBACK_REASONS: Array<string> = [
    'I don’t receive offers that are relevant to me',
    'I don’t receive enough offers',
    'I was looking for a Loyalty program',
    'I couldn’t find the food I wanted',
    'I didn’t like the food',
    'The food was too expensive',
    'I had a bad pickup experience',
    'Using the App was difficult',
    'Too many bugs or technical issues',
    'Customer Support was unsatisfactory',
];
export const CANCEL_BUTTON_TEXT = 'Cancel';
export const CONTINUE_BUTTON_TEXT = 'Continue';
export const IS_QUESTION_TEXT_BOLD = true;
