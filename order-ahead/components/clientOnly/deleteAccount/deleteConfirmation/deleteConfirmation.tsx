import React, { FC } from 'react';
import DeleteAccountLayout from '../layout/layout';
import classNames from 'classnames';
import styles from './deleteConfirmation.module.css';
import { DELETE_ACCOUNT_TEXT, REMOVE_HEADER_TRANSFORM } from './constants';
import { InspireButton } from '../../../atoms/button';

interface IDeleteConfirmationProps {
    onCancel: () => void;
    onDelete: () => void;
}

const DeleteConfirmation: FC<IDeleteConfirmationProps> = ({ onCancel, onDelete }) => {
    return (
        <DeleteAccountLayout
            buttons={[
                <InspireButton key={0} onClick={onCancel} type="secondary" text="Cancel" />,
                <InspireButton key={1} onClick={onDelete} type="primary" text={DELETE_ACCOUNT_TEXT} />,
            ]}
        >
            <div className={classNames('t-header-h2', { [styles.removeTextTransform]: REMOVE_HEADER_TRANSFORM })}>
                Are You Sure?
            </div>
            <p className={classNames('t-paragraph', styles.contentTopIndent)}>
                If you delete your account now, you will lose access to exclusive offers and deals.
            </p>
            <p className={classNames('t-paragraph', styles.contentTopIndent)}>
                If you’re ready to proceed, click “{DELETE_ACCOUNT_TEXT}” below.
            </p>
        </DeleteAccountLayout>
    );
};

export default DeleteConfirmation;
