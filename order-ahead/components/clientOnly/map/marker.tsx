import React from 'react';
import classnames from 'classnames';

import BrandIcon from '../../atoms/BrandIcon';
import getBrandInfo from '../../../lib/brandInfo';

import { PickupAddress } from '../../../redux/orderLocation';

import styles from './marker.module.css';

interface IMarkerProps {
    place: PickupAddress;
    onLocationSelect?: (place: PickupAddress) => void;
    selected: boolean;
    lat: number;
    lng: number;
    ['aria-label']?: string;
    listNumber?: number;
    isDeliveryMarker?: boolean;
}

const Marker = (props: IMarkerProps): JSX.Element => {
    const { selected, onLocationSelect, place, listNumber, isDeliveryMarker } = props;

    const { brandId } = getBrandInfo();
    const brandIdLowercase = brandId.toLowerCase();
    const imgSrc = `/brands/${brandIdLowercase}/map-pin${selected ? '-selected' : ''}.svg`;

    const handleClick = () => {
        if (typeof onLocationSelect === 'function') {
            onLocationSelect(place);
        }
    };

    return (
        <div
            className={classnames(styles.marker, { [styles.selected]: selected })}
            onClick={handleClick}
            aria-label={props['aria-label']}
        >
            {isDeliveryMarker && <BrandIcon className={styles.deliveryMarkerIcon} icon="order-delivery" />}
            {!isDeliveryMarker && listNumber && (
                <span className={classnames({ 't-header-h2': selected, 't-header-h3': !selected }, styles.listNumber)}>
                    {listNumber}
                </span>
            )}
            <img alt="map pin image" className={styles.markerImage} src={imgSrc} />
        </div>
    );
};
export default Marker;
