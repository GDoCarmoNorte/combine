import React, { useState, useEffect, useRef, useMemo } from 'react';
import GoogleMapReact from 'google-map-react';
import { useDispatch } from 'react-redux';
import getBrandInfo from '../../../lib/brandInfo';

import useWidth from '../../../common/hooks/useWidth';
import { PickupAddress } from '../../../redux/orderLocation';
import {
    GTM_MAP_CLICK,
    GTM_MAP_DOUBLE_CLICK,
    GTM_MAP_DRAG_START,
    GTM_MAP_DRAG,
    GTM_MAP_DRAG_END,
} from '../../../common/services/gtmService/constants';
import Marker from './marker';
import styles from './map.module.css';
import { isLocationsShowAsOrderedList } from '../../../lib/getFeatureFlags';

// Location list is on the map. Pan the map to left by locationListWidth so all locations are visible
const locationListWidth = 300;

// Toggle to pan the map to make selected location in center
const centerOnSelect = false;

// US
const defaultProps = {
    center: { lat: 37.65, lng: -99.38 },
    zoom: 4.9,
};
// Adjust based on break points
const zoomMap = {
    xs: 3.2,
    sm: 3.8,
    md: 4.9,
};

interface IInspireMapProps {
    isDelivery?: boolean;
    locations: PickupAddress[];
    place: PickupAddress;
    onLocationSelect?: (place: PickupAddress) => void;
}

const InspireMap = function (props: IInspireMapProps): JSX.Element {
    const { NEXT_PUBLIC_GOOGLE_MAP_API_KEY } = process.env;

    const { isDelivery, onLocationSelect, locations, place: selectedLocation } = props;
    const list = useMemo(() => locations || [], [locations]);

    const { brandId } = getBrandInfo();
    const brandIdLowercase = brandId.toLowerCase();

    const dispatch = useDispatch();
    const isDragging = useRef(false);
    const [error, setError] = useState(false);
    const [locationList, setLocationList] = useState([...list]);
    useEffect(() => {
        setLocationList([...list]);
    }, [list]);
    const [api, setAPI] = useState(null);
    const [map, setMap] = useState(null);
    const width = useWidth();
    const zoom = zoomMap[width] || defaultProps.zoom;

    const fitBounds = (map, maps) => {
        if (!map || !maps) return;

        const bounds = getMapBounds(maps, locationList);
        const padding = width === 'xs' ? 0 : locationListWidth;
        map.fitBounds(bounds, { right: padding });

        if (locationList.length === 1) {
            map.setZoom(15);
        }
    };

    const resetBounds = () => {
        if (locationList.length > 0) {
            fitBounds(map, api);
        }
    };

    const getMapBounds = (maps, places: PickupAddress[]) => {
        const bounds = new maps.LatLngBounds();
        places.forEach((place) => {
            bounds.extend(new maps.LatLng(selectLat(place), selectLng(place)));
        });
        return bounds;
    };

    const centerSelected = (place: PickupAddress) => {
        if (map && place) map.panTo(new api.LatLng(selectLat(place), selectLng(place)));
    };

    const apiIsLoaded = (map, maps, places) => {
        if (!map) {
            setError(true);
        } else {
            setError(false);
        }
        setAPI(maps);
        setMap(map);
        if (places.length === 0) return;

        fitBounds(map, api);
    };

    const selectLat = (place: PickupAddress): number => place.details.latitude;
    const selectLng = (place: PickupAddress): number => place.details.longitude;

    resetBounds();

    if (centerOnSelect && selectedLocation && locationList.length > 0) centerSelected(selectedLocation);

    const handleClick = () => {
        dispatch({ type: GTM_MAP_CLICK });
    };

    const handleDoubleClick = () => {
        dispatch({ type: GTM_MAP_DOUBLE_CLICK });
    };

    const handleDrag = () => {
        if (!isDragging.current) {
            dispatch({ type: GTM_MAP_DRAG_START });
            isDragging.current = true;
            dispatch({ type: GTM_MAP_DRAG });
        }
    };

    const handleDragEnd = () => {
        isDragging.current = false;
        dispatch({ type: GTM_MAP_DRAG_END });
    };

    if (error || !locationList || !locationList.length) {
        return (
            <>
                <div className={styles.mapPlaceHolder} role="img" aria-label="Map Placeholder" />
                <style jsx>
                    {`.${styles.mapPlaceHolder} {
                        background-image: url('/brands/${brandIdLowercase}/mapPlaceHolder.png');
                    }`}
                </style>
            </>
        );
    }

    return (
        <div onDoubleClick={handleDoubleClick} className={styles.mapWrapper} aria-label="locationsMap">
            <GoogleMapReact
                bootstrapURLKeys={{ key: NEXT_PUBLIC_GOOGLE_MAP_API_KEY }}
                defaultCenter={defaultProps.center}
                defaultZoom={defaultProps.zoom}
                zoom={zoom}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps, locationList)}
                options={createMapOptions}
                onClick={handleClick}
                onDrag={handleDrag}
                onDragEnd={handleDragEnd}
            >
                {(locationList || []).map((place, index) => (
                    <Marker
                        key={place.id}
                        lat={selectLat(place)}
                        lng={selectLng(place)}
                        place={place}
                        onLocationSelect={onLocationSelect}
                        selected={place === selectedLocation}
                        aria-label="locationMarker"
                        listNumber={isLocationsShowAsOrderedList() ? index + 1 : undefined}
                        isDeliveryMarker={isDelivery}
                    />
                ))}
            </GoogleMapReact>
        </div>
    );
};

function createMapOptions() {
    return {
        zoomControl: true,
        zoomControlOptions: {
            position: 6, //ControlPosition.LEFT_BOTTOM
        },
        fullscreenControl: false,
    };
}

export default InspireMap;
