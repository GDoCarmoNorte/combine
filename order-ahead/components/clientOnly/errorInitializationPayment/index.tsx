import React from 'react';
import classNames from 'classnames';
import { InspireButton } from '../../atoms/button';
import styles from './index.module.css';

interface IErrorInitializationPaymentProps {
    paymentError: { description?: string; header?: string; textError?: string };
    disabled?: boolean;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

export default function ErrorInitializationPayment(props: IErrorInitializationPaymentProps): JSX.Element {
    const {
        paymentError: { textError, header, description },
        disabled,
        onClick,
    } = props;

    return (
        <div role="banner" className={styles.errorBanner}>
            <div className={styles.centerSection}>
                <div className={classNames('t-subheader-small', styles.errorText)}>{textError}</div>
                <h1 className={classNames('t-header-h1', styles.headerText)}>{header}</h1>
                <div className={styles.mainText}>{description}</div>
                <div className={styles.buttonsBlock}>
                    <InspireButton
                        text={'Refresh'}
                        type={'primary'}
                        className={styles.refreshButton}
                        onClick={onClick}
                        disabled={disabled}
                    />
                </div>
            </div>
        </div>
    );
}
