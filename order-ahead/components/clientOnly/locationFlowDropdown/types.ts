import React from 'react';

export interface ILocationFlowDropdownProps {
    navRef?: React.MutableRefObject<HTMLElement>;
    titleTooltip?: string;
}

export interface ILocationFlowDropdownContentProps extends ILocationFlowDropdownProps {
    isLocationSelected?: boolean;
    isOAEnabled?: boolean;
}
