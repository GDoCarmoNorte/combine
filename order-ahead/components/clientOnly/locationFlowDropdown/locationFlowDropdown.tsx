import React from 'react';

import { useConfiguration, useOrderLocation } from '../../../redux/hooks';
import styles from './locationFlowDropdown.module.css';
import LocationFlowDropdownContent from './locationFlowDropdownContent';

import { OrderLocationMethod } from '../../../redux/orderLocation';
import { ILocationFlowDropdownProps } from './types';

const LocationFlowDropdown = (props: ILocationFlowDropdownProps): JSX.Element => {
    const { navRef, titleTooltip } = props;
    const { method } = useOrderLocation();
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    const isLocationSelected = method !== OrderLocationMethod.NOT_SELECTED;

    return (
        <div className={styles.dropdownWrapper}>
            <LocationFlowDropdownContent
                navRef={navRef}
                titleTooltip={titleTooltip}
                isLocationSelected={isLocationSelected}
                isOAEnabled={isOAEnabled}
            />
        </div>
    );
};

export default LocationFlowDropdown;
