import React from 'react';
import LocationSelect from '../../organisms/navigation/locationSelect';
import { InspireButton } from '../../atoms/button';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import NavigationTooltip from '../../organisms/navigation/navigationTooltip';
import { ILocationFlowDropdownContentProps } from './types';

import styles from './locationFlowDropdown.module.css';

const LocationFlowDropdownContent = (props: ILocationFlowDropdownContentProps): JSX.Element => {
    const { navRef, titleTooltip, isLocationSelected, isOAEnabled } = props;
    const isMobile = useMediaQuery('(max-width: 960px)');

    return (
        <>
            {!isLocationSelected ? (
                <div>
                    {isMobile && <LocationSelect navRef={navRef} titleTooltip={titleTooltip} />}
                    {!isMobile && (
                        <NavigationTooltip titleTooltip={titleTooltip}>
                            <div>
                                <InspireButton
                                    className={styles.startOrderButton}
                                    type="small"
                                    text={'start an order'}
                                    link="/locations"
                                    disabled={!isOAEnabled}
                                />
                            </div>
                        </NavigationTooltip>
                    )}
                </div>
            ) : (
                <LocationSelect navRef={navRef} />
            )}
        </>
    );
};

export default LocationFlowDropdownContent;
