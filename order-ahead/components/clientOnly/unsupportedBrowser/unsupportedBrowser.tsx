import React, { useEffect, useMemo } from 'react';
import { Entry } from 'contentful';
import Head from 'next/head';

import { IPageFields } from '../../../@generated/@types/contentful';

import { IGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import Footer from '../../../components/organisms/footer';
import BaseHeader from '../../../components/organisms/header/baseHeader';

import styles from './index.module.css';
import { PageContentWrapper } from '../../sections/PageContentWrapper';
import UAParser from 'ua-parser-js';
import logger from '../../../common/services/logger';
import classNames from 'classnames';
export interface IUnsupportedBrowserPage {
    page: Entry<IPageFields>;
    globalProps: IGlobalContentfulProps;
    isMobile: boolean;
}

export default function UnsupportedBrowser(props: IUnsupportedBrowserPage): JSX.Element {
    const { alertBanners, navigation, footer, userAccountMenu } = props.globalProps;

    const parser = useMemo(() => new UAParser(), []);

    const { name, version } = parser.getBrowser();
    const { type } = parser.getDevice();

    const setContinueAnyway = () => {
        sessionStorage.setItem('continueAnyway', 'true');
        const event = new Event('storage');
        window.dispatchEvent(event);
    };

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const { NEXT_PUBLIC_BRAND } = process.env;

    useEffect(() => {
        logger.logEvent('unsupported_browser', {
            appUA: parser.getUA(),
            appUAName: name,
            appUAVersion: version,
            appUAOS: parser.getOS(),
            appUAType: type ? type : 'desktop',
        });
    }, [parser, name, version, type]);

    const browsers = {
        chrome: {
            name: 'Google Chrome',
            icon: `/brands/${NEXT_PUBLIC_BRAND}/chrome.svg`,
            url: 'https://www.google.com/chrome/',
        },
        firefox: {
            name: 'Mozilla Firefox',
            icon: `/brands/${NEXT_PUBLIC_BRAND}/firefox.svg`,
            url: 'https://www.mozilla.org/',
        },
        safari: {
            name: 'Safari',
            icon: `/brands/${NEXT_PUBLIC_BRAND}/safari.svg`,
            url: 'https://www.apple.com/safari/',
        },
        opera: {
            name: 'Opera',
            icon: `/brands/${NEXT_PUBLIC_BRAND}/opera.svg`,
            url: 'https://www.opera.com/',
        },
        samsung: {
            name: 'Samsung',
            icon: `/brands/${NEXT_PUBLIC_BRAND}/samsung.svg`,
            url: 'https://www.samsung.com/us/support/owners/app/samsung-internet',
        },
    };

    const desktopBrowsersList = ['chrome', 'firefox', 'safari'];
    const mobileBrowsersList = ['chrome', 'safari'];

    const browsersList = props.isMobile ? mobileBrowsersList : desktopBrowsersList;

    const errorIcon = `/brands/${NEXT_PUBLIC_BRAND}/error.svg`;

    return (
        <>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <div className={styles.container}>
                <Head>
                    <title>Unsupported Browser</title>
                </Head>
                <PageContentWrapper>
                    <div className={styles.title}>
                        <img className={styles.errorIcon} src={errorIcon} />
                        <h2 className={classNames('t-header-h1', styles.header)}>Improve your experience</h2>
                        <p className="t-paragraph">
                            Your current browser or device cannot run this experience. To continue using our site,
                            please try one of the options below:
                        </p>
                    </div>
                    <div className={styles.browsers}>
                        {browsersList.map((browserId) => {
                            const { name, icon, url } = browsers[browserId];
                            return (
                                <a key={name} className={styles.browser} href={url} target="_blank" rel="noreferrer">
                                    <img src={icon} alt={name} />
                                    <span className="link-secondary-active">{name}</span>
                                </a>
                            );
                        })}
                    </div>

                    <p className={styles.continueAnyway}>
                        Seeing this message by mistake? Continue and{' '}
                        <span onClick={setContinueAnyway} className="link-secondary-active">
                            use your current browser.
                        </span>
                    </p>
                </PageContentWrapper>
            </div>
            {footer && <Footer footer={footer} />}
        </>
    );
}
