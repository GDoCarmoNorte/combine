import dynamic from 'next/dynamic';

const UnsupportedBrowser = dynamic(import('./unsupportedBrowser'), {
    ssr: false,
});

export default UnsupportedBrowser;
