import React, { useState, useLayoutEffect } from 'react';
import classnames from 'classnames';
import { useRouter } from 'next/router';
import throttle from 'lodash/throttle';
import { COOKIE_POLICY_PATHNAME } from './constants';

import styles from './index.module.css';
import { InspireButton } from '../../atoms/button';
import Link from 'next/link';

const NOT_SHOW_PAGES = ['checkout', 'confirmation', 'account/delete'];

export default function CookieBanner({ onClose }: { onClose: () => void }): JSX.Element {
    const [bottomOffset, setBottomOffset] = useState(0);

    const { pathname } = useRouter();

    const canShow = !NOT_SHOW_PAGES.includes(pathname.replace(/^\/?|\/?$/g, ''));

    const handleClose = () => {
        localStorage.setItem('acceptCookie', 'true');
        onClose();
    };

    useLayoutEffect(() => {
        const startOrderBtn = document.getElementById('start-order-btn');

        if (startOrderBtn) {
            const { height, bottom } = startOrderBtn.getBoundingClientRect();
            if (document.documentElement.clientHeight <= Math.round(bottom)) {
                setBottomOffset(height);
            }

            const onWindowScroll = throttle(() => {
                const { height, bottom } = startOrderBtn.getBoundingClientRect();
                if (document.documentElement.clientHeight <= Math.round(bottom)) {
                    setBottomOffset(height);
                } else {
                    setBottomOffset(0);
                }
            }, 150);

            window.addEventListener('scroll', onWindowScroll);

            return () => {
                window.removeEventListener('scroll', onWindowScroll);
            };
        } else {
            setBottomOffset(0);
        }
    }, [pathname]);

    return canShow ? (
        <div role="banner" className={styles.banner} style={{ bottom: `${bottomOffset}px` }}>
            <span className={classnames('t-paragraph-small', styles.text)}>
                This site uses cookies to improve our site and provide content and ads that might interest you. By using
                this site, you consent to our use of cookies as described in our{' '}
                <Link href={`/${COOKIE_POLICY_PATHNAME}`}>
                    <a className={classnames('link-secondary-active', styles.link)}>Cookie Policy</a>
                </Link>
                . For more about how we use your information, read our{' '}
                <Link href="/privacy-policy">
                    <a className={classnames('link-secondary-active', styles.link)}>Privacy Policy</a>
                </Link>
                .
            </span>

            <InspireButton
                className={styles.closeButton}
                text="CLOSE"
                type="small"
                onClick={handleClose}
                ariaLabel="Close"
            />
        </div>
    ) : null;
}
