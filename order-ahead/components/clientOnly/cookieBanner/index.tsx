import dynamic from 'next/dynamic';

const CookieBanner = dynamic(import('./cookieBanner'), {
    ssr: false,
});

export default CookieBanner;
