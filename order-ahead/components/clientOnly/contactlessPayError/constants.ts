import { TContactlessPayErrorTypes, IContactlessPayErrorText } from './types';

export const errorTexts: { [key in TContactlessPayErrorTypes]?: IContactlessPayErrorText } = {
    [TContactlessPayErrorTypes.DISABLED]: {
        header: 'Uh Oh',
        subheader: 'Service Not Available',
        description: 'Please Pay Using Terminal or Ask a Restaurant Staff',
    },
    [TContactlessPayErrorTypes.DEFAULT]: {
        header: 'Uh Oh',
        subheader: 'Order Error',
        description: `Sorry, we can't find your order. Retry your link or ask your server for assistance.`,
    },
};
