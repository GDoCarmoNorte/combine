export enum TContactlessPayErrorTypes {
    DISABLED = 'disabled',
    DEFAULT = 'default',
}

export interface IContactlessPayErrorText {
    header: string;
    subheader: string;
    description: string;
}

export interface IContactlessErrorProps {
    errorType: TContactlessPayErrorTypes;
}
