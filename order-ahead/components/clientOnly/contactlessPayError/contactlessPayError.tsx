import React from 'react';
import styles from './contactlessPayError.module.css';
import classNames from 'classnames';
import { TContactlessPayErrorTypes, IContactlessPayErrorText, IContactlessErrorProps } from './types';
import { errorTexts } from './constants';

export default function ContactlessError(props: IContactlessErrorProps): JSX.Element {
    const errorText: IContactlessPayErrorText =
        errorTexts[props.errorType] || errorTexts[TContactlessPayErrorTypes.DEFAULT];

    return (
        <div className={styles.container}>
            <div className={classNames('t-header-h1', styles.errorHeader)}>{errorText.header}</div>
            <div className={classNames('t-header-h3', styles.errorSubheader)}>{errorText.subheader}</div>
            <div className={classNames('t-subheader-small', styles.errorDescription)}>{errorText.description}</div>
        </div>
    );
}
