import React, { useEffect } from 'react';
import { FormikErrors, useFormikContext } from 'formik';

interface IConnectorProps<T> {
    onChange: (values: T, isValid?: boolean, errors?: FormikErrors<T>) => void;
}

export function createConnector<T>(): React.ComponentType<IConnectorProps<T>> {
    return (props: IConnectorProps<T>) => {
        const { onChange } = props;
        const { values, isValid, errors } = useFormikContext<T>();

        useEffect(() => {
            onChange(values, isValid, errors);
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [values, isValid]);

        return null;
    };
}
