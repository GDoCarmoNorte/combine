import React from 'react';
import classNames from 'classnames';
import { useRouter } from 'next/router';

import useOrderLocation from '../../../redux/hooks/useOrderLocation';
import { PickupAddress } from '../../../redux/orderLocation';
import { getLocationAddressString, getDeliveryAddressString } from '../../../lib/locations';
import { useSubmitOrder } from '../../../redux/hooks';
import { LocationTabPaths } from '../../organisms/locationsPageContent/locationsPageContentPickupAndDeliveryFlow/types';
import InspirePhoneLink from '../../atoms/phoneLink';
import { InspireLink, InspireLinkButton } from '../../atoms/link';

import { SHOW_ORDER_STATUS_BUTTON, TYPOGRAPHY_CLASS } from './constants';
import styles from './index.module.css';

interface ILocationInfoProps {
    viewOnly?: boolean;
    isPickUp?: boolean;
}

const getLocationUrl = (location: PickupAddress): string => {
    const { line1, line2, line3, city, stateProvinceCode: state, countryCode: country } =
        location?.contactDetails.address || {};
    const getAddressLine = (line: string): string => (line ? `${line},` : '');
    const addressLine = `${getAddressLine(line1)}${getAddressLine(line2)}${getAddressLine(line3)}`;

    return `https://www.google.com/maps/search/${location?.brandName},${addressLine}${city},${state},${country}`;
};

function LocationInfo(props: ILocationInfoProps): JSX.Element {
    const { viewOnly, isPickUp = true } = props;
    const router = useRouter();

    const { pickupAddress: pickupLocation, deliveryAddress } = useOrderLocation();
    const deliveryLocation = deliveryAddress?.deliveryLocation;
    const address = isPickUp ? getLocationAddressString(pickupLocation) : getDeliveryAddressString(deliveryLocation);
    const locationUrl = getLocationUrl(pickupLocation);
    const phone = pickupLocation?.contactDetails.phone;

    const { lastOrder } = useSubmitOrder();
    const orderTrackingLink = lastOrder?.fulfillment?.deliveryTrackingUrl;
    const titleText = isPickUp ? 'Pickup Location' : 'Delivery Address';
    const linkText = isPickUp ? 'Change Location' : 'Change Address';

    const pickupChangeLocationClick = () => {
        router.push({ pathname: '/locations' });
    };

    const deliveryChangeLocationClick = () => {
        router.push({
            pathname: '/locations',
            query: { t: LocationTabPaths.DELIVERY },
        });
    };

    const handleChangeLocationClick = isPickUp ? pickupChangeLocationClick : deliveryChangeLocationClick;

    return (
        <div className={styles.container} aria-label="Select Location Section">
            <div className={styles.infoBlock}>
                <p className={classNames('t-subheader-small', styles.heading)}>{titleText}</p>
                {isPickUp && (
                    <h3 className={TYPOGRAPHY_CLASS.NAME}>
                        {pickupLocation ? pickupLocation.displayName : 'Select Location'}
                    </h3>
                )}
                <p className={TYPOGRAPHY_CLASS.ADDRESS}>{address}</p>
                {isPickUp && phone && (
                    <InspirePhoneLink className={TYPOGRAPHY_CLASS.ADDRESS} variant="secondary" phone={phone} />
                )}
            </div>
            <div className={styles.actionBlock}>
                {viewOnly ? (
                    location && (
                        <div>
                            {isPickUp && (
                                <a
                                    href={locationUrl}
                                    className={classNames('link-secondary-active', styles.link)}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    Get Directions
                                </a>
                            )}

                            {!isPickUp && SHOW_ORDER_STATUS_BUTTON && orderTrackingLink && (
                                <InspireLink link={orderTrackingLink} type="primary" newtab>
                                    order status
                                </InspireLink>
                            )}
                        </div>
                    )
                ) : (
                    <InspireLinkButton onClick={handleChangeLocationClick} linkType="primary">
                        {linkText}
                    </InspireLinkButton>
                )}
            </div>
        </div>
    );
}

export default LocationInfo;
