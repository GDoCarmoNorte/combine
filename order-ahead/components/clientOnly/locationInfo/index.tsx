import React from 'react';
import dynamic from 'next/dynamic';

import Loader from '../../atoms/Loader';

import styles from './index.module.css';

const LocationInfo = dynamic(import('./locationInfo'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => (
        <div className={styles.container}>
            <Loader size={10} />
        </div>
    ),
});

export default LocationInfo;
