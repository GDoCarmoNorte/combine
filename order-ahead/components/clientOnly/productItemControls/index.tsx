import React from 'react';
import dynamic from 'next/dynamic';

import Loader from '../../atoms/Loader';

const ProductItemControls = dynamic(import('./productItemControls'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => (
        <span>
            <Loader size={20} />
        </span>
    ),
});

export default ProductItemControls;
