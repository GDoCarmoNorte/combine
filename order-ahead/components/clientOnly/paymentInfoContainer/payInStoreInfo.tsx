import React from 'react';
import classNames from 'classnames';
import { InspireButton } from '../../atoms/button';
import styles from './index.module.css';
import { IPaymentRequest } from './paymentInfo';
import { TInitialPaymentTypes } from './paymentTypeDropdown/types';

interface IPayInStoreInfo {
    onSubmit: (request: IPaymentRequest) => void;
    isActive: boolean;
}

const PayInStoreInfo = ({ onSubmit, isActive }: IPayInStoreInfo): JSX.Element => {
    const handlePayInStore = () => {
        return onSubmit({
            sessionKey: '',
            paymentKeys: [''],
            paymentType: TInitialPaymentTypes.PAY_IN_STORE,
        });
    };

    return (
        <>
            <p className={classNames('t-paragraph', styles.confirmText)}>
                You can now proceed to confirm your order, and pay the check at the front desk when you reach your
                restaurant.
            </p>
            <div>
                <InspireButton disabled={!isActive} text="Confirm" fullWidth onClick={handlePayInStore} />
            </div>
        </>
    );
};

export default PayInStoreInfo;
