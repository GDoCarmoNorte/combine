import React from 'react';
import Link from 'next/link';
import classNames from 'classnames';
import styles from './termsLabel.module.css';

const TermsLabel = ({ className, htmlFor }: { className: string; htmlFor: string }): JSX.Element => (
    <label htmlFor={htmlFor} className={classNames('t-paragraph-hint', className)}>
        By clicking &quot;PAY&quot;, you authorize the Arby’s location listed to charge your credit card for the full
        amount and you agree to Arby’s&nbsp;
        <Link href="/terms-of-use">
            <a target="_blank" className={styles.termsLink}>
                Terms &amp; Conditions
            </a>
        </Link>
        &nbsp;and&nbsp;
        <Link href="/privacy-policy">
            <a target="_blank" className={styles.termsLink}>
                Privacy Policy
            </a>
        </Link>
        .
    </label>
);

export default TermsLabel;
