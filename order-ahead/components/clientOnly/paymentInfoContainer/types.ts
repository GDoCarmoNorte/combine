import { FreedomPayDto } from '../../../common/hooks/useFreedomPay';
import { IPaymentRequest } from './paymentInfo';

export interface IGiftCard {
    sessionKey: string;
    paymentToken: FreedomPayDto;
    balance: number | null;
}

export interface IPaymentAndGiftCardInfoContainerProps {
    isActive?: boolean;
    onSubmit?: (request: IPaymentRequest, giftCards?: IGiftCard[]) => Promise<void>;
    onError?: (error: { message?: string; header?: string; description?: string; textError?: string }) => void;
    onFormStatusChange?: (isValid: boolean) => void;
    submitError?: Error;
    totalPrice?: number;
    inputLabelClass?: string;
}
