import React, { FC } from 'react';
import PaymentInfo from './paymentInfo';
import { IPaymentAndGiftCardInfoContainerProps } from './types';
import { TInitialPaymentTypes } from './paymentTypeDropdown/types';
import { usePayment } from '../../../common/hooks/usePayment';
import { FulfillmentTypeEnumModel } from '../../../@generated/webExpApi';

export interface IPaymentSectionProps extends IPaymentAndGiftCardInfoContainerProps {
    paymentType: TInitialPaymentTypes;
    fulfillmentType?: FulfillmentTypeEnumModel;
    locationId?: string;
}

const PaymentSection: FC<IPaymentSectionProps> = ({ paymentType, ...props }) => {
    const paymentPayload = usePayment(TInitialPaymentTypes.CREDIT_OR_DEBIT, props.locationId);
    const innnerHtml = paymentPayload.payment && { __html: paymentPayload.payment.iframeHtml };

    if (paymentType === TInitialPaymentTypes.CREDIT_OR_DEBIT) {
        return (
            <PaymentInfo {...props} paymentType={paymentType} paymentPayload={paymentPayload} innnerHtml={innnerHtml} />
        );
    }

    return null;
};

export default PaymentSection;
