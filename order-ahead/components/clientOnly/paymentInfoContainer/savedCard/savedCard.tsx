import React, { useState, useEffect } from 'react';

import CheckIcon from '@material-ui/icons/Check';
import TermsLabel from '../termsLabel';
import classNames from 'classnames';
import styles from './savedCard.module.css';
import { usePayment } from '../../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { IPaymentRequest } from '../paymentInfo';
import Loader from '../../../atoms/Loader';
import ErrorInitializationPayment from '../../errorInitializationPayment';
import { PAYMENT_ERROR_PAYLOAD } from '../constants';
import { CARD_ON_FILE } from '../paymentTypeDropdown/constants';
import { useCustomerPaymentMethod } from '../../../../common/hooks/useCustomerPaymentMethod';
import { getCreditCardIssuerFromFreedomPayType } from '../../../../common/helpers/creditCardIssuerMapper';

interface ISavedCardProps {
    token: string;
    onSubmit: (request: IPaymentRequest) => Promise<void>;
    isActive?: boolean;
}

const SavedCard = (props: ISavedCardProps): JSX.Element => {
    const { token, onSubmit, isActive } = props;
    const { paymentMethods } = useCustomerPaymentMethod();
    const paymentPayload = usePayment(CARD_ON_FILE);
    const { payment, error: paymentError, isLoading: isPaymentLoading, reset: resetPayment } = paymentPayload || {};
    const innnerHtml = paymentPayload.payment && { __html: paymentPayload.payment.iframeHtml };
    const {
        payment: paymentToken,
        reset: resetFreedomPay,
        isValid: isFreedomPayValid,
        errors: freedomPayErrors,
    } = useFreedomPay(CARD_ON_FILE);

    const freedomPayType = paymentMethods?.cREDITCARDS?.find((item) => item.token === token)?.type;
    const cardIssuer = getCreditCardIssuerFromFreedomPayType(freedomPayType);

    const [isTermsChecked, setIsTermsChecked] = useState<boolean>(false);

    const freedomPayEnabled = isFreedomPayValid && !(freedomPayErrors && freedomPayErrors.length > 0);
    const isPayButtonEnabled = freedomPayEnabled && isActive && isTermsChecked;

    const overlayClasses = classNames(styles.paymentOverlay, {
        [styles.paymentOverlayHidden]: isPayButtonEnabled,
    });

    const handleRefreshPaymentInfo = () => {
        resetPayment();
    };

    useEffect(() => {
        if (!paymentToken?.paymentType || paymentToken.paymentType !== CARD_ON_FILE) {
            return;
        }
        const keys = paymentToken && paymentToken.keys;

        if (keys && keys.length > 0 && onSubmit) {
            onSubmit({
                cardIssuer: cardIssuer,
                paymentType: CARD_ON_FILE,
                sessionKey: paymentPayload.payment.sessionKey,
                paymentKeys: keys,
                cardToken: token,
            }).catch(() => {
                resetFreedomPay();
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paymentToken]);

    if (!paymentError && !payment) {
        return (
            <div className={styles.paymentLoader}>
                <Loader size={20} />
            </div>
        );
    }

    if (paymentError) {
        return (
            <ErrorInitializationPayment
                paymentError={PAYMENT_ERROR_PAYLOAD}
                onClick={handleRefreshPaymentInfo}
                disabled={isPaymentLoading}
            />
        );
    }

    return (
        <div>
            <div className={styles.formWrapper}>
                <div className={styles.paymentContainer} dangerouslySetInnerHTML={innnerHtml} />
                <div className={overlayClasses} aria-label="Pay Button Overlay" aria-disabled={!isPayButtonEnabled} />
            </div>
            <div className={styles.termsContainer}>
                <input
                    className={styles.checkbox}
                    type="checkbox"
                    id="terms"
                    checked={isTermsChecked}
                    onChange={(e) => {
                        setIsTermsChecked(e.target.checked);
                    }}
                />
                <div
                    tabIndex={0}
                    className={styles.styledCheckbox}
                    onClick={() => {
                        setIsTermsChecked((prevState) => !prevState);
                    }}
                    onKeyPress={({ key }) => key === 'Enter' && setIsTermsChecked((prevState) => !prevState)}
                >
                    <CheckIcon className={styles.checkIcon} />
                </div>
                <TermsLabel htmlFor="terms" className={styles.terms} />
            </div>
        </div>
    );
};

export default SavedCard;
