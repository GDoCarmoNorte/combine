import React, { useEffect, useMemo, useState } from 'react';
import { Form, Formik } from 'formik';
import classNames from 'classnames';
import Checkbox from '../../atoms/checkbox';
import { UsePaymentHook } from '../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../common/hooks/useFreedomPay';
import styles from './index.module.css';
import FormikInput from '../../organisms/formikInput';
import { validateAlpha } from '../../../common/helpers/complexValidateHelper';
import { createConnector } from '../formik/connector';
import ErrorInitializationPayment from '../errorInitializationPayment';
import { experiencingTechnicalDifficultiesError } from '../../../common/services/createErrorWrapper';
import Loader from '../../atoms/Loader';
import { TInitialPaymentTypes, TPaymentMethodTypes } from './paymentTypeDropdown/types';
import TermsLabel from './termsLabel';
import { IPaymentAndGiftCardInfoContainerProps } from './types';
import { PAYMENT_ERROR_PAYLOAD } from './constants';
import logger from '../../../common/services/logger';

interface IPaymentInfo {
    chFirstName: string;
    chLastName: string;
}

export interface IPaymentRequest {
    chFirstName?: string;
    chLastName?: string;
    sessionKey: string;
    paymentKeys: string[];
    cardIssuer?: string;
    maskedCardNumber?: string;
    billingPostalCode?: string;
    paymentType?: TPaymentMethodTypes;
    cardToken?: string;
}

const initialValues: IPaymentInfo = {
    chFirstName: '',
    chLastName: '',
};

export interface IPaymentInfoProps extends IPaymentAndGiftCardInfoContainerProps {
    paymentPayload: UsePaymentHook;
    paymentType: TInitialPaymentTypes;
    fixedHeight?: boolean;
    innnerHtml: { __html: string };
    inputLabelClass?: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = (): void => {};
const minHeight = 450;
const maxHeight = 548;

const FormikConnector = createConnector<IPaymentInfo>();

const PaymentInfo = (props: IPaymentInfoProps): JSX.Element => {
    const {
        isActive,
        onSubmit,
        onError,
        onFormStatusChange,
        submitError,
        paymentPayload,
        paymentType,
        fixedHeight,
        innnerHtml,
        inputLabelClass,
    } = props;
    const [isValid, setIsValid] = useState<boolean>(isActive);
    const [paymentInfo, setPaymentInfo] = useState<IPaymentInfo>(null);

    const { payment, error: paymentError, reset: resetPayment, isLoading: isPaymentLoading } = paymentPayload || {};

    const {
        height,
        payment: paymentToken,
        errors: freedomPayErrors,
        isValid: isFreedomPayValid,
        reset: resetFreedomPay,
    } = useFreedomPay();
    const freedomPayEnabled = isFreedomPayValid && !(freedomPayErrors && freedomPayErrors.length > 0);

    const [isTermsChecked, setIsTermsChecked] = useState(false);
    const [iframeHeight, setIframeHeight] = useState(0);
    const paymentInfoEnabled = isActive && isValid && isTermsChecked;

    useEffect(() => {
        /**
         * There are times when there are multiple active hpc's.
         * Because all of these hpc's are getting messages on the window, there is no way to differentiate.
         * This function at least validates that a size change event matches this hpc type.
         */
        if (height >= minHeight && height <= maxHeight) {
            setIframeHeight(height);
        }
    }, [height]);

    useEffect(() => {
        if (!paymentToken?.paymentType || paymentToken.paymentType !== TInitialPaymentTypes.CREDIT_OR_DEBIT) {
            return;
        }
        const keys = paymentToken && paymentToken.keys;

        if (keys && keys.length > 0 && onSubmit && paymentInfoEnabled) {
            onSubmit({
                chFirstName: paymentInfo.chFirstName,
                chLastName: paymentInfo.chLastName,
                sessionKey: payment.sessionKey,
                paymentKeys: keys,
                cardIssuer: paymentToken.cardIssuer,
                maskedCardNumber: paymentToken.maskedCardNumber,
            }).catch(() => {
                resetFreedomPay();
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paymentToken]);

    useEffect(() => {
        if (onError) {
            onError(
                paymentError
                    ? PAYMENT_ERROR_PAYLOAD
                    : freedomPayErrors && freedomPayErrors.length > 0
                    ? {
                          message: experiencingTechnicalDifficultiesError,
                      }
                    : null
            );
        }
    }, [freedomPayErrors, paymentError, onError]);

    useEffect(() => {
        if (!paymentInfo) return;

        const paymentInfoValues = Object.values(paymentInfo);
        const isPaymentInfoFilled = paymentInfoValues.filter(Boolean).length === paymentInfoValues.length;

        onFormStatusChange && onFormStatusChange(isPaymentInfoFilled && isValid && isFreedomPayValid && isTermsChecked);
    }, [isValid, isFreedomPayValid, paymentInfo, isTermsChecked, onFormStatusChange]);

    const testFirstName = useMemo(() => validateAlpha('First Name'), []);
    const testLastName = useMemo(() => validateAlpha('Last Name'), []);

    const isPayButtonEnabled = (freedomPayEnabled || submitError) && paymentInfoEnabled;

    const overlayClasses = classNames(styles.paymentOverlay, {
        [styles.paymentOverlayHidden]: isPayButtonEnabled,
    });

    const inputLabelClasses = classNames(styles.inputLabel, {
        [inputLabelClass]: !!inputLabelClass,
    });

    const onFormChange = (values: IPaymentInfo, isValid: boolean) => {
        setIsValid(isValid);
        setPaymentInfo(values);
    };

    const handleRefreshPaymentInfo = () => {
        resetPayment();
    };

    const handleCheckBox = () => setIsTermsChecked((prevState) => !prevState);

    const formContainerClasses = classNames(styles.formContainer, {
        [styles.formContainerActive]: payment,
    });

    if (!paymentError && !payment) {
        return (
            <div className={styles.paymentLoader}>
                <Loader size={20} />
            </div>
        );
    }

    if (paymentError) {
        logger.logEvent('payment_iframe_not_displayed', { type: 'CREDIT_OR_DEBIT', message: PAYMENT_ERROR_PAYLOAD });
        return (
            <ErrorInitializationPayment
                paymentError={PAYMENT_ERROR_PAYLOAD}
                onClick={handleRefreshPaymentInfo}
                disabled={isPaymentLoading}
            />
        );
    }

    return (
        <div className={styles.paymentInfo} aria-label="Payment Info">
            {paymentType === TInitialPaymentTypes['CREDIT_OR_DEBIT'] && (
                <div className={styles.formWrapper}>
                    <Formik onSubmit={noop} initialValues={initialValues} validateOnMount>
                        {() => (
                            <Form className={formContainerClasses}>
                                <FormikInput
                                    name="chFirstName"
                                    label="Cardholder First Name"
                                    type="text"
                                    maxLength={70}
                                    placeholder="Enter Cardholder First Name"
                                    validate={testFirstName}
                                    inputClassName={styles.inputControl}
                                    labelClassName={inputLabelClasses}
                                />
                                <FormikInput
                                    name="chLastName"
                                    label="Cardholder Last Name"
                                    type="text"
                                    maxLength={70}
                                    placeholder="Enter Cardholder Last Name"
                                    validate={testLastName}
                                    inputClassName={styles.inputControl}
                                    labelClassName={inputLabelClasses}
                                />
                                <FormikConnector onChange={onFormChange} />
                            </Form>
                        )}
                    </Formik>
                    <div
                        className={styles.paymentContainer}
                        dangerouslySetInnerHTML={innnerHtml}
                        style={fixedHeight ? {} : { height: `${iframeHeight + 20}px` }} // 20px - error message's height
                    />
                    <div
                        className={overlayClasses}
                        aria-label="Pay Button Overlay"
                        aria-disabled={!isPayButtonEnabled}
                    />
                </div>
            )}
            <div className={styles.termsContainer}>
                <input
                    className={styles.checkbox}
                    type="checkbox"
                    id="terms"
                    checked={isTermsChecked}
                    onChange={(e) => {
                        setIsTermsChecked(e.target.checked);
                    }}
                />
                <Checkbox
                    fieldName="terms"
                    onClick={handleCheckBox}
                    onKeyUp={handleCheckBox}
                    selected={isTermsChecked}
                    ariaLabel="Terms and Conditions"
                />
                <TermsLabel htmlFor="terms" className={styles.terms} />
            </div>
        </div>
    );
};

export default PaymentInfo;
