import React from 'react';
import dynamic from 'next/dynamic';

export default dynamic(import('./paymentInfoContainer'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => <span>loading...</span>,
});
