import React, { useEffect } from 'react';
import classNames from 'classnames';

import { usePayment } from '../../../../common/hooks/usePayment';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { useOrderLocation } from '../../../../redux/hooks';

import styles from './giftCardSection.module.css';

import ErrorInitializationPayment from '../../errorInitializationPayment';
import { experiencingTechnicalDifficultiesError } from '../../../../common/services/createErrorWrapper';
import Loader from '../../../atoms/Loader';
import { TInitialPaymentTypes } from '../paymentTypeDropdown/types';
import { IPaymentRequest } from '../paymentInfo';
import useBwwCardBalance from '../../../../common/hooks/useBwwCardBalance';
import { PAYMENT_ERROR_PAYLOAD } from '../constants';
import { IGiftCard } from '../types';
import logger from '../../../../common/services/logger';
import { FulfillmentTypeEnumModel } from '../../../../@generated/webExpApi';

interface IPaymentInfoProps {
    isActive?: boolean;
    onSubmit?: (request: IPaymentRequest) => Promise<void>;
    onError?: (error: { message?: string; header?: string; description?: string; textError?: string }) => void;
    submitError?: Error;
    onApplyGiftCards: (giftCard: IGiftCard) => void;
    dineIn?: boolean;
    locationId?: string;
}

const GiftCardPaymentInfo = (props: IPaymentInfoProps): JSX.Element => {
    const { onError, submitError, onApplyGiftCards } = props;
    const isDineIn = !!props.dineIn;
    const fulfillmentType = isDineIn ? FulfillmentTypeEnumModel.Dinein : undefined;

    const { currentLocation } = useOrderLocation();
    const { payment, error: paymentError, reset: resetPayment, isLoading: isPaymentLoading } = usePayment(
        TInitialPaymentTypes.PLACEHOLDER,
        props.locationId,
        fulfillmentType
    );

    const { getBalance, balance, loading: isBalanceLoading, resetCard } = useBwwCardBalance();

    const innnerHtml = payment && { __html: payment.iframeHtml };

    const {
        payment: paymentToken,
        errors: freedomPayErrors,
        isValid: isFreedomPayValid,
        reset: resetFreedomPay,
    } = useFreedomPay(TInitialPaymentTypes.PLACEHOLDER);

    const freedomPayEnabled = isFreedomPayValid && !(freedomPayErrors && freedomPayErrors.length > 0);

    useEffect(() => {
        if (!paymentToken?.paymentType || paymentToken.paymentType !== TInitialPaymentTypes.GIFT_CARD) {
            return;
        }
        const keys = paymentToken && paymentToken.keys;

        if (keys && keys.length > 0) {
            getBalance({
                sessionKey: payment.sessionKey,
                paymentKey: keys[0],
                locationId: props.locationId || currentLocation.id,
                fulfillmentType: isDineIn ? 'DINEIN' : 'PICKUP',
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paymentToken]);

    useEffect(() => {
        if (onError) {
            onError(
                paymentError
                    ? PAYMENT_ERROR_PAYLOAD
                    : freedomPayErrors && freedomPayErrors.length > 0
                    ? {
                          message: experiencingTechnicalDifficultiesError,
                      }
                    : null
            );
        }
    }, [freedomPayErrors, paymentError, onError]);

    useEffect(() => {
        if (balance !== null && typeof balance !== 'undefined') {
            onApplyGiftCards({
                sessionKey: payment.sessionKey,
                paymentToken,
                balance,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [balance]);

    const isSaveButtonEnabled = freedomPayEnabled || submitError;

    const overlayClasses = classNames(styles.paymentOverlay, {
        [styles.paymentOverlayHidden]: isSaveButtonEnabled,
    });

    const handleRefreshPaymentInfo = async () => {
        await resetPayment();
        await resetCard();
        await resetFreedomPay();
    };

    if ((!paymentError && !payment) || isBalanceLoading || isPaymentLoading) {
        return (
            <div className={styles.paymentLoader}>
                <Loader size={20} />
            </div>
        );
    }

    if (paymentError) {
        logger.logEvent('payment_iframe_not_displayed', { type: 'GIFT_CARD', message: PAYMENT_ERROR_PAYLOAD });
        return (
            <ErrorInitializationPayment
                paymentError={PAYMENT_ERROR_PAYLOAD}
                onClick={handleRefreshPaymentInfo}
                disabled={isPaymentLoading}
            />
        );
    }

    return (
        innnerHtml && (
            <div className={styles.paymentInfo} aria-label="Payment Info">
                {
                    <div className={styles.formWrapper}>
                        <div className={styles.paymentContainer} dangerouslySetInnerHTML={innnerHtml}></div>
                        <div
                            className={overlayClasses}
                            aria-label="Save Button Overlay"
                            aria-disabled={!isSaveButtonEnabled}
                        ></div>
                    </div>
                }
            </div>
        )
    );
};

export default GiftCardPaymentInfo;
