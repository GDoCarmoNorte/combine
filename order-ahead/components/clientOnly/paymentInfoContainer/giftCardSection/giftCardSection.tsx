import React, { FC, useState } from 'react';
import Checkbox from '../../../atoms/checkbox';
import PaymentCard from '../../../atoms/paymentCard';
import GiftCardPaymentInfo from './giftCardPaymentInfo';

import styles from './giftCardSection.module.css';
import classnames from 'classnames';
import { IGiftCard, IPaymentAndGiftCardInfoContainerProps } from '../types';

interface IGiftCardSection extends IPaymentAndGiftCardInfoContainerProps {
    setGiftCards: (giftCards: IGiftCard[]) => void;
    giftCards: IGiftCard[];
    dineIn?: boolean;
    locationId?: string;
    totalToPay: number;
}

const GiftCardSection: FC<IGiftCardSection> = ({
    giftCards,
    setGiftCards,
    dineIn,
    locationId,
    totalToPay,
    ...props
}) => {
    const [addGiftCardSelected, setAddGiftCardSelected] = useState<boolean>(false);
    const [addAnotherGiftCardSelected, setAddAnotherGiftCardSelected] = useState<boolean>(false);

    const handleApplyGiftCard = (giftCard: IGiftCard) => {
        setGiftCards([...giftCards, giftCard]);
    };

    const handleRemoveGiftCard = (giftCard: IGiftCard) => {
        setAddAnotherGiftCardSelected(false);

        if (giftCards.length === 1) {
            setAddGiftCardSelected(false);
        }

        setGiftCards(
            giftCards.filter((card) => {
                const intersection = card.paymentToken.keys.filter((value) =>
                    giftCard.paymentToken.keys.includes(value)
                );
                return intersection.length === 0;
            })
        );
    };

    const handleClickAddGiftCard = () => {
        if (addGiftCardSelected && giftCards.length === 0) {
            setAddGiftCardSelected(false);
        } else {
            setAddGiftCardSelected(true);
        }
    };

    const handleClickAddAnotherGiftCard = () => {
        if (addAnotherGiftCardSelected && giftCards.length === 1) {
            setAddAnotherGiftCardSelected(false);
        } else {
            setAddAnotherGiftCardSelected(true);
        }
    };

    const renderMessage = () => {
        let messageText;
        const cardWordWithArticle = giftCards.length > 1 ? 'cards are' : 'card is';

        if (totalToPay > 0) {
            messageText = `Score! The total order price after applying gift ${cardWordWithArticle} ${totalToPay} $. Please complete the rest
            of the payment.`;
        }

        if (totalToPay === 0) {
            messageText = `Score! Your gift ${cardWordWithArticle} covering the bill. No additional payment needed.`;
        }

        return <p className={classnames('t-paragraph-strong', styles.message)}>{messageText}</p>;
    };

    return (
        <div className={styles.container}>
            <div className={classnames('t-paragraph', styles.addCheckBox)}>
                <button
                    onClick={handleClickAddGiftCard}
                    className={styles.addGiftCardButton}
                    aria-label="Pay with gift card"
                >
                    <Checkbox fieldName="pay-with-gift-card" selected={addGiftCardSelected} />
                    <div className={styles.addGiftCardLabels}>
                        <span>Pay with gift card</span>
                        <span className={styles.addGiftCardLabelSecondary}>Card must have 8-digit pin</span>
                    </div>
                </button>
                <span className={styles.addMax}>(Max of 2)</span>
            </div>
            {addGiftCardSelected && giftCards.length === 0 && (
                <GiftCardPaymentInfo
                    dineIn={!!dineIn}
                    locationId={locationId}
                    onApplyGiftCards={handleApplyGiftCard}
                    {...props}
                />
            )}
            {addGiftCardSelected && giftCards.length > 0 && (
                <div>
                    <PaymentCard
                        cardType={'GIFT'}
                        className={styles.appliedCard}
                        key={giftCards[0].paymentToken.keys[0]}
                        title={giftCards[0].paymentToken.cardIssuer}
                        cardNumber={giftCards[0].paymentToken.maskedCardNumber}
                        accountBalance={giftCards[0].balance}
                        onRemove={() => handleRemoveGiftCard(giftCards[0])}
                    />
                    {giftCards.length === 1 && renderMessage()}
                    {!(totalToPay === 0 && giftCards.length === 1) && (
                        <div className={classnames('t-paragraph', styles.addCheckBox, styles.anotherAddCheckBox)}>
                            <button
                                onClick={handleClickAddAnotherGiftCard}
                                className={styles.addGiftCardButton}
                                aria-label="Add another gift card"
                            >
                                <Checkbox fieldName="add-another-gift-card" selected={addAnotherGiftCardSelected} />
                                <div className={styles.addGiftCardLabels}>
                                    <span>Add another gift card</span>
                                    <span className={styles.addGiftCardLabelSecondary}>Card must have 8-digit pin</span>
                                </div>
                            </button>
                            <span className={styles.addMax}>(Max of 2)</span>
                        </div>
                    )}
                </div>
            )}
            {addAnotherGiftCardSelected && giftCards.length === 1 && (
                <GiftCardPaymentInfo
                    dineIn={!!dineIn}
                    locationId={locationId}
                    onApplyGiftCards={handleApplyGiftCard}
                    {...props}
                />
            )}
            {addAnotherGiftCardSelected && giftCards.length === 2 && (
                <>
                    <PaymentCard
                        cardType={'GIFT'}
                        className={styles.appliedCard}
                        key={giftCards[1].paymentToken.keys[0]}
                        title={giftCards[1].paymentToken.cardIssuer}
                        cardNumber={giftCards[1].paymentToken.maskedCardNumber}
                        accountBalance={giftCards[1].balance}
                        onRemove={() => handleRemoveGiftCard(giftCards[1])}
                    />
                    {renderMessage()}
                </>
            )}
        </div>
    );
};

export default GiftCardSection;
