import React, { useEffect } from 'react';

import Loader from '../../../atoms/Loader';
import { useApplePay } from '../../../../common/hooks/useApplePay';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { TInitialPaymentTypes } from '../paymentTypeDropdown/types';
import { useOrderLocation } from '../../../../redux/hooks';
import { getLocationById } from '../../../../common/services/locationService';
import { experiencingTechnicalDifficultiesError } from '../../../../common/services/createErrorWrapper';
import { PAYMENT_ERROR_PAYLOAD } from '../constants';
import { IPaymentRequest } from '../paymentInfo';

import styles from './applePaySection.module.css';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

interface IApplePayProps {
    setCompatibility: (isCompatible: boolean) => void;
    onSubmit?: (request: IPaymentRequest) => Promise<void>;
    onError?: (error: { message?: string; header?: string; description?: string; textError?: string }) => void;
    totalAmount: number;
    className?: string;
}

const ApplePaySection = (props: IApplePayProps): JSX.Element => {
    const { setCompatibility, onSubmit, onError, totalAmount, className } = props;

    const {
        configuration: { isApplePayEnabled },
    } = useConfiguration();
    const { currentLocation: pickupAddress } = useOrderLocation();
    const { isLoading, isCompatible, payment, error: paymentError, initPayment: initApplePay } = useApplePay();

    const { payment: paymentToken, errors: freedomPayErrors, reset: resetFreedomPay } = useFreedomPay(
        TInitialPaymentTypes.APPLE_PAY
    );

    useEffect(() => {
        if (initApplePay) {
            getLocationById({
                locationId: pickupAddress.id,
            }).then((location) => {
                initApplePay({
                    locationId: location.id,
                    totalAmount,
                    submitButtonColor: 'whiteWithOutline',
                    isAutoFinalizePayment: true,
                });
            });
        }
    }, [totalAmount, initApplePay, pickupAddress.id]);

    useEffect(() => {
        setCompatibility(isCompatible);
    }, [isCompatible, setCompatibility]);

    useEffect(() => {
        if (!paymentToken?.paymentType || paymentToken.paymentType !== TInitialPaymentTypes.APPLE_PAY) return;

        const keys = paymentToken && paymentToken.keys;
        if (keys && keys.length > 0 && onSubmit) {
            const cardHolderNameParts = paymentToken.cardHolderName?.split(' ') || [];
            const chFirstName = cardHolderNameParts.length > 0 ? cardHolderNameParts[0] : paymentToken.cardHolderName;
            const chLastName = cardHolderNameParts.length > 1 ? cardHolderNameParts[1] : paymentToken.cardHolderName;
            onSubmit({
                chFirstName,
                chLastName,
                sessionKey: payment.sessionKey,
                paymentKeys: keys,
                cardIssuer: paymentToken.cardIssuer,
                maskedCardNumber: paymentToken.maskedCardNumber,
                billingPostalCode: paymentToken.billingPostalCode,
                paymentType: paymentToken.paymentType,
            }).catch(() => {
                resetFreedomPay();
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paymentToken]);

    useEffect(() => {
        if (onError) {
            onError(
                paymentError
                    ? PAYMENT_ERROR_PAYLOAD
                    : freedomPayErrors && freedomPayErrors.length > 0
                    ? {
                          message: experiencingTechnicalDifficultiesError,
                      }
                    : null
            );
        }
    }, [freedomPayErrors, paymentError, onError]);

    if (!isApplePayEnabled || !isCompatible) {
        return null;
    }

    if ((!paymentError && !payment) || isLoading) {
        return (
            <div className={styles.paymentLoader}>
                <Loader size={20} />
            </div>
        );
    }

    return (
        <div className={className} aria-label="Apple pay">
            {payment && (
                <div className={styles.applePayContainer} dangerouslySetInnerHTML={{ __html: payment.iframeHtml }} />
            )}
        </div>
    );
};

export default ApplePaySection;
