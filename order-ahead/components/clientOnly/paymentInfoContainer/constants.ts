export const PAYMENT_ERROR_PAYLOAD = {
    textError: 'Connection error',
    header: 'Temporarily unavailable',
    description: 'The operation could not be completed. Please refresh and try again.',
};
