import React, { useEffect } from 'react';
import { useOrderLocation } from '../../../../redux/hooks';

import styles from './googlePaySection.module.css';
import Loader from '../../../atoms/Loader';
import { IPaymentRequest } from '../paymentInfo';
import { useGooglePayment } from '../../../../common/hooks/useGooglePayment';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import { TInitialPaymentTypes } from '../paymentTypeDropdown/types';
import { PAYMENT_ERROR_PAYLOAD } from '../constants';
import { experiencingTechnicalDifficultiesError } from '../../../../common/services/createErrorWrapper';
import { GoogleButtonColorEnumModel } from '../../../../@generated/webExpApi/models/GoogleButtonColorEnumModel';
import { GoogleButtonTypeEnumModel } from '../../../../@generated/webExpApi/models/GoogleButtonTypeEnumModel';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

interface IGooglePaymentInfoProps {
    totalAmount: number;
    onPaymentMethodSupportChange?: (isSupported: boolean) => void;
    onSubmit?: (request: IPaymentRequest) => Promise<void>;
    onError?: (error: { message?: string; header?: string; description?: string; textError?: string }) => void;
    submitError?: Error;
    className?: string;
}

const GooglePaySection = (props: IGooglePaymentInfoProps): JSX.Element => {
    const { onError, submitError, onSubmit, totalAmount, onPaymentMethodSupportChange, className } = props;

    const {
        configuration: { isGooglePayEnabled },
    } = useConfiguration();
    const { currentLocation: pickupAddress } = useOrderLocation();
    const {
        payment,
        isCompatible,
        error: paymentError,
        isLoading: isPaymentLoading,
        initPayment: initGooglePay,
    } = useGooglePayment();

    const { payment: paymentToken, errors: freedomPayErrors, reset: resetFreedomPay } = useFreedomPay(
        TInitialPaymentTypes.GOOGLE_PAY
    );

    const innnerHtml = payment && { __html: payment.iframeHtml };
    const freedomPayEnabled =
        !paymentToken ||
        paymentToken.paymentType !== TInitialPaymentTypes.GOOGLE_PAY ||
        (freedomPayErrors && freedomPayErrors.length > 0);

    useEffect(() => {
        if (initGooglePay) {
            initGooglePay({
                locationId: pickupAddress.id,
                totalAmount,
                submitButtonColor: GoogleButtonColorEnumModel.Black,
                submitButtonType: GoogleButtonTypeEnumModel.Buy,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [totalAmount, initGooglePay]);

    useEffect(() => {
        onPaymentMethodSupportChange && onPaymentMethodSupportChange(isCompatible);
    }, [isCompatible, onPaymentMethodSupportChange]);

    useEffect(() => {
        if (!paymentToken?.paymentType || paymentToken.paymentType !== TInitialPaymentTypes.GOOGLE_PAY) {
            return;
        }

        const keys = paymentToken && paymentToken.keys;
        if (keys && keys.length > 0 && onSubmit) {
            const cardHolderNameParts = paymentToken.cardHolderName?.split(' ') || [];
            const chFirstName = cardHolderNameParts.length > 0 ? cardHolderNameParts[0] : paymentToken.cardHolderName;
            const chLastName = cardHolderNameParts.length > 1 ? cardHolderNameParts[1] : paymentToken.cardHolderName;
            onSubmit({
                chFirstName,
                chLastName,
                sessionKey: payment.sessionKey,
                paymentKeys: keys,
                cardIssuer: paymentToken.cardIssuer,
                maskedCardNumber: paymentToken.maskedCardNumber,
                billingPostalCode: paymentToken.billingPostalCode,
                paymentType: paymentToken.paymentType,
            }).catch(() => {
                resetFreedomPay();
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paymentToken]);

    useEffect(() => {
        if (onError) {
            onError(
                paymentError
                    ? PAYMENT_ERROR_PAYLOAD
                    : freedomPayErrors && freedomPayErrors.length > 0
                    ? {
                          message: experiencingTechnicalDifficultiesError,
                      }
                    : null
            );
        }
    }, [freedomPayErrors, paymentError, onError]);

    if (!isGooglePayEnabled || !isCompatible || !innnerHtml || !(freedomPayEnabled || submitError)) {
        return null;
    }

    if ((!paymentError && !payment) || isPaymentLoading) {
        return (
            <div className={styles.paymentLoader}>
                <Loader size={20} />
            </div>
        );
    }

    return (
        <div className={className} aria-label="Google pay">
            <div className={styles.formWrapper}>
                <div className={styles.paymentContainer} dangerouslySetInnerHTML={innnerHtml}></div>
            </div>
        </div>
    );
};

export default GooglePaySection;
