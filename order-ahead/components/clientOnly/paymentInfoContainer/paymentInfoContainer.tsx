import React, { FC } from 'react';

import PaymentSection from './paymentSection';
import PaymentTypeSelectBox from './paymentTypeDropdown/paymentTypeDropdown';

import { TInitialPaymentTypes, IPaymentState } from './paymentTypeDropdown/types';
import { IPaymentAndGiftCardInfoContainerProps } from './types';

interface IPaymentInfoContainerProps extends IPaymentAndGiftCardInfoContainerProps {
    paymentInfo: IPaymentState;
    setPaymentInfo: (state: IPaymentState) => void;
    isUnavailableItems: boolean;
}

const PaymentInfoContainer: FC<IPaymentInfoContainerProps> = (props): JSX.Element => {
    const { paymentInfo, setPaymentInfo } = props;

    return (
        <>
            <PaymentTypeSelectBox onChange={setPaymentInfo} selectedMethod={paymentInfo} />
            {paymentInfo.type !== TInitialPaymentTypes.PLACEHOLDER && (
                <PaymentSection {...props} paymentType={paymentInfo.type as TInitialPaymentTypes} />
            )}
        </>
    );
};

export default PaymentInfoContainer;
