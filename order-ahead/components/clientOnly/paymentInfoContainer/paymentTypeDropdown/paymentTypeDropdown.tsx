import React, { useMemo } from 'react';

import {
    isPlaceholderPaymentOptionEnabled,
    isCreditOrDebitPayEnabled,
    isCardOnFilePayEnabled,
} from '../../../../lib/getFeatureFlags';

import { useOrderLocation, useTallyOrder } from '../../../../redux/hooks';
import { useCustomerPaymentMethod } from '../../../../common/hooks/useCustomerPaymentMethod';

import { TInitialPaymentTypes, IPaymentTypeDropDown, IPaymentMethod } from './types';
import { ALL_INITIAL_PAYMENT_TYPES, CARD_ON_FILE, CREDIT_CARD_IMG } from './constants';
import PaymentTypeDropdownContainer from './paymentTypeDropdownContainer';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

export default function PaymentTypeSelectBox(props: IPaymentTypeDropDown): JSX.Element {
    const { onChange, selectedMethod } = props;

    const {
        configuration: { isPayAtStoredEnabled },
    } = useConfiguration();
    const { paymentMethods, loading: loadingPaymentMethods } = useCustomerPaymentMethod();
    const { isPickUp, currentLocation } = useOrderLocation();
    const { tallyOrder } = useTallyOrder();

    const totalPrice = tallyOrder.total;

    const isLocationCanPayInStore = currentLocation?.additionalFeatures?.isPayAtStoreEnabled;
    const payAtStoreMaxAmount = isLocationCanPayInStore && currentLocation?.additionalFeatures?.payAtStoreMaxAmount;
    const limitTotalForPayAtStore = payAtStoreMaxAmount >= totalPrice;

    const paymentOptions = useMemo(() => {
        const methods: IPaymentMethod[] = [];

        if (isCreditOrDebitPayEnabled()) {
            methods.push(ALL_INITIAL_PAYMENT_TYPES[TInitialPaymentTypes.CREDIT_OR_DEBIT]);
        }

        if (isPayAtStoredEnabled && limitTotalForPayAtStore && isPickUp) {
            methods.push(ALL_INITIAL_PAYMENT_TYPES[TInitialPaymentTypes.PAY_IN_STORE]);
        }

        if (isPlaceholderPaymentOptionEnabled()) {
            methods.push(ALL_INITIAL_PAYMENT_TYPES[TInitialPaymentTypes.PLACEHOLDER]);
        }

        // add GOOGLE_PAY, VENMO and all that crap here

        if (isCardOnFilePayEnabled() && paymentMethods) {
            const creditCards = paymentMethods.cREDITCARDS;
            for (let i = 0; i < creditCards.length; i++) {
                const lastFourDigitsNormalize = creditCards[i].lastFourDigits.slice(-4);
                const cardName = creditCards[i].label || 'Credit Card';
                methods.unshift({
                    type: CARD_ON_FILE,
                    token: creditCards[i].token,
                    image: CREDIT_CARD_IMG,
                    text: `${cardName} ****${lastFourDigitsNormalize}`,
                });
            }
        }
        return methods;
    }, [limitTotalForPayAtStore, paymentMethods, isPickUp, isPayAtStoredEnabled]);

    return (
        <>
            <PaymentTypeDropdownContainer
                onChange={onChange}
                selectedMethod={selectedMethod}
                paymentOptions={paymentOptions}
                loadingPaymentMethods={loadingPaymentMethods}
            />
        </>
    );
}
