import React from 'react';
import classNames from 'classnames';

import MenuItem from '@material-ui/core/MenuItem';
import Dropdown from '../../../atoms/dropdown';
import Loader from '../../../atoms/Loader';

import styles from './paymentTypeDropdown.module.css';

import { IPaymentMethod, IPaymentTypeDropdownComponent } from './types';

export default function PaymentTypeDropdownContainer(props: IPaymentTypeDropdownComponent): JSX.Element {
    const { onChange, selectedMethod, paymentOptions, loadingPaymentMethods } = props;

    const handleOptionClick = (option: IPaymentMethod) => {
        onChange({
            type: option.type,
            token: option.token,
        });
    };

    const getSelectedOption = (): IPaymentMethod => {
        if (selectedMethod.token) return paymentOptions.filter((option) => option.token === selectedMethod.token)[0];
        return paymentOptions.filter((option) => option.type === selectedMethod.type)[0];
    };

    const renderOption = (option: IPaymentMethod): JSX.Element => {
        return (
            <span className={classNames('t-paragraph-small', styles.optionContainer)}>
                <span className={classNames(styles.optionIcon)}>
                    <img className={classNames(styles.optionIconImage)} src={option?.image} alt={'payment method'} />
                </span>
                <span>{option?.text}</span>
            </span>
        );
    };

    const renderListOptions = (): JSX.Element[] => {
        return paymentOptions.map((option) => {
            return (
                <MenuItem
                    tabIndex={0}
                    key={option.text}
                    className={classNames(styles.menuOption)}
                    onClick={() => {
                        handleOptionClick(option);
                    }}
                >
                    {renderOption(option)}
                </MenuItem>
            );
        });
    };

    const renderTargetLoader = () => (
        <div className={styles.loadingContainer}>
            <Loader size={28} />
        </div>
    );

    const renderTarget = (): JSX.Element => {
        if (loadingPaymentMethods) return renderTargetLoader();
        const selected = getSelectedOption();
        return renderOption(selected);
    };

    return (
        <>
            <div className={classNames(styles.dropdownTitle)}>PAYMENT METHOD</div>
            <Dropdown
                target={renderTarget()}
                disabled={loadingPaymentMethods}
                className={classNames(styles.selectDropdown)}
                buttonClassName={classNames(styles.selectionButton)}
                popperClass={classNames(styles.popperPositionOverride)}
                listClassName={classNames('t-paragraph-small', styles.optionsList)}
            >
                {renderListOptions()}
            </Dropdown>
        </>
    );
}
