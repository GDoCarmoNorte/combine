import React, { FC } from 'react';
import { DealsListItem } from '..';
import { IOfferModel } from '../../../../@generated/webExpApi';

import styles from './dealsList.module.css';

interface IDealsListProps {
    offers: IOfferModel[];
}

const DealsList: FC<IDealsListProps> = ({ offers }) => {
    return (
        <div className={styles.offers}>
            {offers.map((offer) => {
                return <DealsListItem key={offer.id} offer={offer} />;
            })}
        </div>
    );
};

export default DealsList;
