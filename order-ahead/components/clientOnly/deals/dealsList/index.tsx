import dynamic from 'next/dynamic';

const DealsList = dynamic(import('./dealsList'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default DealsList;
