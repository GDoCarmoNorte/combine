import React, { useCallback } from 'react';
import { useMediaQuery } from '@material-ui/core';
import { InspireButton } from '../../../atoms/button';
import styles from './deal.module.css';
import TextWithTrademark from '../../../atoms/textWithTrademark';
import InspireBadge from '../../../atoms/Badge';
import classNames from 'classnames';
import { Asset } from 'contentful';
import ContentfulImage from '../../../atoms/ContentfulImage';

interface IDealProps {
    title: string;
    label?: string;
    expires: string;
    description: string;
    primaryCtaText?: string;
    secondaryCtaText?: string;
    image: Asset;
    terms: string;
    type?: string;
    id?: string;
    showQr?: boolean;
    qrImage?: string;
    qrDescription?: string;
    onPrimaryCtaClick: (data) => void;
    onSecondaryCtaClick: () => void;
    disabled?: boolean;
    gtmId: string;
}

export default function Deal(props: IDealProps): JSX.Element {
    const {
        title,
        label,
        expires,
        description,
        disabled = false,
        primaryCtaText,
        secondaryCtaText,
        image,
        terms,
        type,
        id,
        showQr,
        qrImage,
        qrDescription,
        onPrimaryCtaClick,
        onSecondaryCtaClick,
        gtmId,
    } = props;
    const isSmallVariant = useMediaQuery('(max-width: 767px)');

    const renderImage = useCallback(() => {
        return (
            <div className={styles.imageContainer}>
                {showQr ? (
                    <img src={`data:image/png;base64,${qrImage}`} className={styles.image} alt="QR code" />
                ) : (
                    <ContentfulImage asset={image} className={styles.image} imageAlt="Deal" />
                )}
                {showQr && <div className={styles.qrDescription}>{qrDescription}</div>}
            </div>
        );
    }, [image, qrImage, showQr, qrDescription]);

    const handleClick = () => {
        onPrimaryCtaClick({ type, id });
    };

    const handleSecondaryCtaClick = () => {
        onSecondaryCtaClick();
    };

    return (
        <div
            className={classNames(styles.container, {
                [styles.containerQr]: showQr,
            })}
        >
            <div className={styles.banner}>
                <div className={styles.leftSection}>
                    <TextWithTrademark text={title} tag="h2" className={classNames('t-header-h1', styles.title)} />
                    {label && <InspireBadge className="dealBadge" value={label} />}
                    {isSmallVariant && renderImage()}
                    <span className={styles.expires}>{expires}</span>
                    <span className={styles.description}>{description}</span>
                    <div className={styles.buttonsBlock}>
                        {primaryCtaText && (
                            <InspireButton
                                text={primaryCtaText}
                                onClick={handleClick}
                                disabled={disabled}
                                gtmId={gtmId}
                            />
                        )}
                        {secondaryCtaText && (
                            <InspireButton
                                text={secondaryCtaText}
                                type="secondary"
                                disabled={disabled}
                                onClick={handleSecondaryCtaClick}
                            />
                        )}
                    </div>
                </div>
                {!isSmallVariant && <div className={styles.rightSection}>{renderImage()}</div>}
            </div>
            <div className={styles.termsAndConditions}>
                <p>Terms and conditions of the deal</p>
                <p>{terms}</p>
            </div>
        </div>
    );
}
