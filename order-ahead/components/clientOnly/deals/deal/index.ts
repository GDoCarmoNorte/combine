import dynamic from 'next/dynamic';

const Deal = dynamic(import('./deal'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default Deal;
