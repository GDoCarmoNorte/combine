import dynamic from 'next/dynamic';

const DealsListItem = dynamic(import('./dealsListItem'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default DealsListItem;
