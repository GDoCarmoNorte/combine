import React, { FC } from 'react';
import classnames from 'classnames';
import { isValid, format } from '../../../../common/helpers/dateTime';
import Link from 'next/link';

import { IOfferModel } from '../../../../@generated/webExpApi/models';

import TextWithTrademark from '../../../atoms/textWithTrademark';
import InspireBadge from '../../../atoms/Badge';
import InspireIcon from '../../../atoms/BrandIcon';
import { useBag } from '../../../../redux/hooks';

import styles from './dealsListItem.module.css';

import useDeal from '../../../../common/hooks/useDeal';
import ContentfulImage from '../../../atoms/ContentfulImage';

interface IDealsListItemProps {
    offer: IOfferModel;
}

const DealsListItem: FC<IDealsListItemProps> = (props) => {
    const { offer } = props;
    const { dealId: dealIdInBag } = useBag();
    const { name, userOfferId, endDateTime, isRedeemableInStoreOnly, isRedeemableOnlineOnly } = offer;
    const isInBag = userOfferId === dealIdInBag;

    const expiresDate = isValid(new Date(endDateTime)) ? format(new Date(endDateTime), 'MM/dd') : '';

    const { getDealImage } = useDeal();
    const imageUrl = getDealImage(offer?.id);

    return (
        <div className={styles.offer} data-testid="deals-list-item">
            {isRedeemableInStoreOnly && <InspireBadge className="productBadge" value="In Store Only" />}
            {isRedeemableOnlineOnly && <InspireBadge className="productBadge" value="Online Only" />}
            <ContentfulImage asset={imageUrl} className={styles.image} maxWidth={117} imageAlt="Deal" />
            <div className={styles.information}>
                <div className={styles.nameAndDate}>
                    <div className={styles.nameWrapper}>
                        <TextWithTrademark
                            tag="span"
                            text={name}
                            title={name}
                            className={classnames('t-subheader-small', styles.name, 'truncate-at-2')}
                        />
                        {isInBag && (
                            <div className={styles.inBagIcon} aria-label="Deal already in the bag">
                                <InspireIcon icon="navigation-bag" className={styles.bagIcon} />
                                <InspireIcon icon="action-check" className={styles.checkIcon} />
                            </div>
                        )}
                    </div>
                    {!!expiresDate && <span className="t-paragraph-hint">{`Expires ${expiresDate}`}</span>}
                </div>

                <Link href={`/account/deals/deal?id=${userOfferId}`}>
                    <a data-gtm-id="view-deal" className={classnames('link-primary-active')}>
                        view deal
                    </a>
                </Link>
            </div>
        </div>
    );
};

export default DealsListItem;
