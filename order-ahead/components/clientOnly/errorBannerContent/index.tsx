import dynamic from 'next/dynamic';

const ErrorBannerContent = dynamic(import('./errorBannerContent'), {
    ssr: false,
});

export default ErrorBannerContent;
