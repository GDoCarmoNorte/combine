import React from 'react';
import classNames from 'classnames';
import { IErrorBannerFields } from '../../../@generated/@types/contentful';

import { InspireButton } from '../../atoms/button';
import ContentfulImage from '../../atoms/ContentfulImage';
import { getGtmIdByName } from '../../../lib/gtm';

import styles from './errorBannerContent.module.css';

interface IErrorBannerContentProps {
    fields: IErrorBannerFields;
    enableGoBack?: boolean;
}

export default function ErrorBannerContent(props: IErrorBannerContentProps): JSX.Element {
    const { fields, enableGoBack } = props;

    const { mainText, mainImage, errorCodeText, headerText, homePageText } = fields;

    const gtmId = getGtmIdByName('errorBanner', mainText);

    return (
        <div data-gtm-id={gtmId} role="banner" className={styles.errorBanner}>
            <div data-gtm-id={gtmId} className={styles.leftSection}>
                <div className={classNames('t-subheader-small', styles.errorText)}>{errorCodeText}</div>
                <h1 data-gtm-id={gtmId} className={classNames('t-header-h1', styles.headerText)}>
                    {headerText}
                </h1>
                <p className="t-paragraph">{mainText}</p>
                <div className={styles.buttonsBlock}>
                    {enableGoBack && (
                        <InspireButton
                            text={'GO BACK'}
                            type={'secondary'}
                            gtmId={gtmId}
                            className={styles.goBackButton}
                            onClick={() => window.history.back()}
                        />
                    )}
                    <InspireButton
                        link={'/'}
                        text={homePageText}
                        type={'primary'}
                        gtmId={gtmId}
                        className={styles.homePageButton}
                    />
                </div>
            </div>
            <div data-gtm-id={gtmId} className={styles.rightSection}>
                <ContentfulImage className={styles.rightSectionImage} gtmId={gtmId} asset={mainImage} maxWidth={650} />
            </div>
        </div>
    );
}
