import dynamic from 'next/dynamic';

const CheckoutError = dynamic(import('./checkoutError'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default CheckoutError;
