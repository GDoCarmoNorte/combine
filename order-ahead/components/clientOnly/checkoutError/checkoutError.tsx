import React from 'react';

import Icon from '../../atoms/BrandIcon';

import styles from './index.module.css';

const DEFAULT_ERROR_TITLE = 'PAYMENT DECLINED';
const DEFAULT_ERROR_MESSAGE = 'Your payment was declined and your order was not submitted. Please try again.';

interface CheckoutErrorProps {
    error?: Error;
    title?: string;
    message?: string;
}

export default function CheckoutError(props: CheckoutErrorProps): JSX.Element {
    const { title, message, error } = props;

    return (
        <div className={styles.errorContainer}>
            <Icon icon="info-error" size="m" className={styles.icon} />
            <span className={styles.textContainer}>
                <span className={styles.title}>{`${title || DEFAULT_ERROR_TITLE}: `}</span>
                <span className={styles.text}>{message || error?.message || DEFAULT_ERROR_MESSAGE}</span>
            </span>
        </div>
    );
}
