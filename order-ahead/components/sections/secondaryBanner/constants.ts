export const TYPOGRAPHY_CLASS = {
    MAIN_TEXT: 't-subheader-universal',
    TITLE: 't-paragraph',
    DESCRIPTION: 't-paragraph-hint',
};
