import React from 'react';
import classNames from 'classnames';

import { ISecondaryBanner } from '../../../@generated/@types/contentful';

import { InspireButton } from '../../atoms/button';
import LegalMessage from '../../atoms/legalMessage';
import { getGtmIdByName } from '../../../lib/gtm';

import { ISectionComponentProps } from '..';

import styles from './secondaryBanner.module.css';
import ContentfulImage from '../../atoms/ContentfulImage';
import { TYPOGRAPHY_CLASS } from './constants';
import { getImageUrl } from '../../../common/helpers/contentfulImage';

interface ISecondaryBannerProps extends ISectionComponentProps {
    entry: ISecondaryBanner;
    classes?: { [key: string]: { [key: string]: string } };
}

export default function SecondaryBanner(props: ISecondaryBannerProps): JSX.Element {
    const fields = props.entry.fields;
    const classes = props.classes?.sectionBanner;

    if (!fields) return null;

    const {
        hasBorder,
        arrangeInline,
        icon,
        mainText,
        title,
        description,
        linkText,
        linkUrl,
        backgroundImage,
        mobileBackgroundImage,
        legalMessage,
    } = fields;

    const gtmId = getGtmIdByName('secondaryBanner', mainText);

    // default to inline alignment if arrangeInline is undefined
    let columnAligned = false;
    if (arrangeInline === false) {
        columnAligned = true;
    }

    const backgroundColor = fields.backgroundColor?.fields.hexColor;
    const mainTextColor = fields.mainTextColor?.fields.hexColor;
    const titleColor = fields.titleColor?.fields.hexColor;
    const descriptionColor = fields.descriptionColor?.fields.hexColor;
    const backgroundImageForMobile = mobileBackgroundImage || backgroundImage;

    const backgroundImageDesktopStyle = backgroundImage ? `url(${getImageUrl(backgroundImage)})` : 'none';
    const backgroundImageMobileStyle = backgroundImageForMobile
        ? `url(${getImageUrl(backgroundImageForMobile)})`
        : 'none';

    return (
        <div
            data-gtm-id={gtmId}
            role="banner"
            className={classNames(
                styles.secondaryBanner,
                {
                    [styles.hasBorder]: hasBorder,
                },
                classes?.container
            )}
        >
            <div
                className={classNames(
                    'wrapperConf',
                    styles.wrapper,
                    {
                        [styles.columnAligned]: columnAligned,
                    },
                    classes?.wrapper
                )}
            >
                {icon && (
                    <ContentfulImage
                        gtmId={gtmId}
                        className={classNames(styles.icon, classes?.icon)}
                        asset={icon}
                        imageAlt=""
                    />
                )}
                {mainText && (
                    <div
                        data-gtm-id={gtmId}
                        className={classNames(
                            'mainTextConf',
                            TYPOGRAPHY_CLASS.MAIN_TEXT,
                            styles.mainText,
                            classes?.mainText
                        )}
                    >
                        {mainText}
                    </div>
                )}
                <div data-gtm-id={gtmId} className={classNames(styles.descriptionBlock, classes?.descriptionBlock)}>
                    <div
                        data-gtm-id={gtmId}
                        className={classNames('titleConf', TYPOGRAPHY_CLASS.TITLE, styles.title, classes?.title)}
                    >
                        {title}
                    </div>
                    <div
                        data-gtm-id={gtmId}
                        className={classNames(
                            'descriptionConf',
                            TYPOGRAPHY_CLASS.DESCRIPTION,
                            styles.description,
                            classes?.description
                        )}
                    >
                        {description}{' '}
                        {legalMessage && (
                            <LegalMessage
                                className={classNames(styles.legalMessage, 'descriptionConf')}
                                legalMessage={legalMessage}
                            />
                        )}
                    </div>
                </div>
                {linkUrl && (
                    <InspireButton
                        gtmId={gtmId}
                        className={classNames(styles.secondaryButton, classes?.secondaryButton)}
                        link={linkUrl}
                        text={linkText}
                        type="secondary"
                    />
                )}
            </div>
            <style jsx>{`
                .wrapperConf {
                    background-color: ${backgroundColor ? `#${backgroundColor}` : 'transparent'};
                    background-image: ${backgroundImageDesktopStyle};
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center;
                }
                .mainTextConf {
                    color: ${mainTextColor ? `#${mainTextColor}` : 'var(--col--primary1)'};
                }
                .titleConf {
                    color: ${titleColor ? `#${titleColor}` : 'var(--col--dark)'};
                }
                .descriptionConf {
                    color: ${descriptionColor ? `#${descriptionColor}` : 'var(--col--dark)'};
                }
                @media (max-width: 1024px) {
                    .wrapperConf {
                        background-image: ${backgroundImageMobileStyle};
                    }
                }
            `}</style>
        </div>
    );
}
