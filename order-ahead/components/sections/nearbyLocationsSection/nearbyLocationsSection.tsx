import React, { FC, useEffect, useMemo } from 'react';
import { useRouter } from 'next/router';
import { ILocationByStateOrProvinceDetailsModel } from '../../../@generated/webExpApi';
import usePickupLocationsSearch from '../../../common/hooks/usePickupLocationsSearch';
import styles from './nearbyLocationsSection.module.css';
import LocationsListItem from '../../organisms/locationsPageContent/pickupSearchResults/locationsList/locationsListItem';
import { INearbyLocationsSection } from '../../../@generated/@types/contentful';
import { ISectionComponentProps } from '..';
import SectionHeader from '../../atoms/sectionHeader';
export interface INearbyLocationsSectionProps extends ISectionComponentProps {
    data?: {
        locationDetails?: ILocationByStateOrProvinceDetailsModel;
    };
    entry: INearbyLocationsSection;
}

const NearbyLocationsSection: FC<INearbyLocationsSectionProps> = (props) => {
    const locationDetails = props.data?.locationDetails;
    const { searchLocationsByCoordinates, locationsSearchResult } = usePickupLocationsSearch();
    const { header, locationsLimit } = props.entry.fields;
    const router = useRouter();

    const onOrderClick = () => {
        router.push(`/?locationId=${locationDetails?.id}`);
    };

    useEffect(() => {
        searchLocationsByCoordinates({
            lat: locationDetails?.details?.latitude,
            lng: locationDetails?.details?.longitude,
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const nearbyLocations = useMemo(() => {
        if (!locationDetails) {
            return [];
        }
        const list = (locationsSearchResult?.locations || []).filter((l) => l.id !== locationDetails?.id);
        return list.slice(0, parseInt(locationsLimit));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [locationDetails, locationsSearchResult]);

    return nearbyLocations && nearbyLocations.length > 0 ? (
        <div className={styles.wrapper}>
            <SectionHeader
                showBullet={true}
                text={header}
                tag={'h2'}
                className={styles.header}
                textClassName={'t-header-h2'}
            />
            <div className={styles.locationTilesWrapper}>
                {nearbyLocations.map((location) => {
                    return (
                        <div key={`Nearby-location_${location.id}`} className={styles.tileWrapper}>
                            <LocationsListItem
                                key={location.id}
                                location={location}
                                onLocationSet={() => onOrderClick()}
                                onLocationSelect={() => onOrderClick()}
                                selected={false}
                                isCurrentLocation={false}
                                listNumber={undefined}
                                hideStoreFeatures={true}
                                hideSelectStoreLink={true}
                            />
                        </div>
                    );
                })}
            </div>
        </div>
    ) : null;
};

export default NearbyLocationsSection;
