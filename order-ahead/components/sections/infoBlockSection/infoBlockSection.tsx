import React from 'react';
import classnames from 'classnames';

import { IInfoBlockSection } from '../../../@generated/@types/contentful';

import withSectionLayout from '../withSectionLayout';
import { InspireButton } from '../../atoms/button';
import { InspireLink } from '../../atoms/link';
import ContentfulImage from '../../atoms/ContentfulImage';

import { ISectionComponentProps } from '..';

import styles from './infoBlockSection.module.css';
import LegalMessage from '../../atoms/legalMessage';

interface IInfoBlockSectionProps extends ISectionComponentProps {
    entry: IInfoBlockSection;
    classes?: { [key: string]: { [key: string]: string } };
}

function InfoBlockSection(props: IInfoBlockSectionProps): JSX.Element {
    const { cards, name } = props.entry.fields;
    const isSingleCard = cards.length === 1;
    const isDoubleCard = cards.length === 2;
    const isTripleCard = cards.length === 3;
    const isMoreThreeCards = cards.length > 3;
    const classes = props.classes?.infoBlockSection;

    return (
        <div
            className={classnames(styles.sectionFlexContainer, {
                [styles.sectionMoreThree]: isMoreThreeCards,
                [styles.sectionUntitled]: !name,
            })}
        >
            {cards.map((card, i) => {
                const buttonText = card.fields?.buttonText;
                const link = card.fields?.link;

                const headerColor = card.fields?.headerColor?.fields?.hexColor;
                const subheaderColor = card.fields?.subheaderColor?.fields?.hexColor;
                const backgroundColor = card.fields?.backgroundColor?.fields?.hexColor;
                const bgImageUrl = card.fields?.backgroundImage?.fields?.file.url;
                const headerText = card.fields?.headerText;
                const subheaderText = card.fields?.subheaderText;
                const legalMessage = card.fields?.legalMessage;

                const headerClasses = classnames(styles.cardHeader, 't-header-card-title', `headerConfig${i}`, {
                    truncate: subheaderText,
                    'truncate-at-3': !subheaderText,
                });

                const subheaderClasses = classnames(
                    styles.cardSubheader,
                    't-paragraph',
                    `subheaderConfig${i}`,
                    classes?.cardSubheader
                );

                const CardLinkWrapper = ({ children, className }) => {
                    const wrapperClasses = classnames(
                        className,
                        styles.card,
                        {
                            [styles.cardWithButton]: buttonText && link,
                            [styles.single]: isSingleCard,
                            [styles.double]: isDoubleCard,
                            [styles.triple]: isTripleCard,
                            [styles.moreThree]: isMoreThreeCards,
                        },
                        classes?.cardLink
                    );

                    if (link) {
                        return (
                            <InspireLink className={wrapperClasses} link={link}>
                                {children}
                            </InspireLink>
                        );
                    }

                    return <div className={wrapperClasses}>{children}</div>;
                };

                return (
                    <CardLinkWrapper key={i} className={`backgroundCard${i}`}>
                        <ContentfulImage asset={card.fields?.icon} />
                        <div
                            className={classnames(styles.content, {
                                [styles.contentWithoutButton]: !buttonText || !link,
                            })}
                        >
                            <h3 className={classnames('t-header-h3', headerClasses)} title={headerText}>
                                {headerText}
                            </h3>
                            <div className={subheaderClasses} title={subheaderText}>
                                <div
                                    className={classnames({
                                        'truncate-at-2': headerText,
                                        'truncate-at-3': !headerText,
                                    })}
                                >
                                    {subheaderText}
                                </div>
                                {legalMessage && (
                                    <LegalMessage
                                        className={classnames(styles.legalMessage, subheaderClasses)}
                                        legalMessage={legalMessage}
                                    />
                                )}
                            </div>
                        </div>
                        {buttonText && link && (
                            <div className={styles.buttonSection} title={buttonText}>
                                <InspireButton
                                    className={classnames('truncate', styles.button)}
                                    text={buttonText}
                                    type="secondary"
                                />
                            </div>
                        )}
                        <style jsx>{`
                            .headerConfig${i} {
                                color: ${headerColor ? `#${headerColor}` : 'var(--col--light)'};
                            }
                            .subheaderConfig${i} {
                                color: ${subheaderColor ? `#${subheaderColor}` : 'var(--col--light)'};
                            }
                            .backgroundCard${i} {
                                background-color: ${backgroundColor ? `#${backgroundColor}` : 'var(--col--primary1)'};
                                ${bgImageUrl
                                    ? `background-image: url("${bgImageUrl}");
                                    background-repeat: no-repeat;
                                    background-size: cover;
                                    background-position: center;`
                                    : ``}
                            }
                        `}</style>
                    </CardLinkWrapper>
                );
            })}
        </div>
    );
}

export default withSectionLayout(InfoBlockSection, {
    showBullet: true,
    headerTag: 'h2',
    className: styles.sectionLayout,
    headerClassName: 't-header-h2',
});
