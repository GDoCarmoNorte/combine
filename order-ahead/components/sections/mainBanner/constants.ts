export const TYPOGRAPHY_CLASS = {
    TOP_TEXT: 't-subheader-hero',
    MAIN_TEXT: 't-header-hero',
    MAIN_TEXT_H2_SIZE: 't-header-h1',
};

export const IMAGES_CLASS = {
    maxWidth: 650,
    objectFit: 'contain',
};
