import React, { FC } from 'react';
import classNames from 'classnames';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import { IMainBanner, IMainBannerFields } from '../../../@generated/@types/contentful';

import { InspireButton, InspireButtonType } from '../../atoms/button';
import { InspireLink } from '../../atoms/link';
import ContentfulImage from '../../atoms/ContentfulImage';
import { getGtmIdByName } from '../../../lib/gtm';

import { ISectionComponentProps } from '..';

import styles from './mainBanner.module.css';
import { Asset } from 'contentful';
import TextWithTrademark from '../../atoms/textWithTrademark';
import VideoBlock from '../videoBlock';
import RichText from '../../atoms/richText';
import LegalMessage from '../../atoms/legalMessage';

import { UMLAUT_SYMBOLS_REGEX } from '../../../common/constants/umlautSymbols';
import { IMAGES_CLASS, TYPOGRAPHY_CLASS } from './constants';

const DEFAULT_MAIN_LINK_TYPE: InspireButtonType = 'large';

export const ActionsSection: FC<Pick<
    IMainBannerFields,
    | 'bottomText'
    | 'mainLinkHref'
    | 'mainLinkText'
    | 'mainLinkType'
    | 'mainText'
    | 'mainTextLink'
    | 'mainTextSize'
    | 'secondaryLink'
    | 'secondaryLinkHref'
    | 'topIcon'
    | 'topText'
    | 'centeredContent'
    | 'legalMessage'
>> = ({
    bottomText,
    mainLinkHref,
    mainLinkText,
    mainLinkType,
    mainText,
    mainTextLink,
    mainTextSize,
    secondaryLink,
    secondaryLinkHref,
    topIcon,
    topText,
    centeredContent,
    legalMessage,
}) => {
    const isSmallVariant = useMediaQuery('(max-width: 1024px)');
    const gtmId = getGtmIdByName('mainBanner', mainText);

    return (
        <div
            data-gtm-id={gtmId}
            className={classNames(styles.actionsSection, { [styles.centeredContentWrapper]: centeredContent })}
        >
            <div data-gtm-id={gtmId} className={styles.topIconBox}>
                {topIcon && (
                    <div data-gtm-id={gtmId} className={styles.topIconSection}>
                        <ContentfulImage asset={topIcon} className={styles.topIcon} />
                    </div>
                )}
                {topText && (
                    <TextWithTrademark
                        tag="span"
                        text={topText}
                        data-gtm-id={gtmId}
                        className={classNames(TYPOGRAPHY_CLASS.TOP_TEXT, 't-stylized', styles.topIconText, {
                            [styles.centerContent]: centeredContent,
                        })}
                    />
                )}
            </div>
            <MainTextSection
                mainText={mainText}
                gtmId={gtmId}
                mainTextLink={mainTextLink}
                mainTextSize={mainTextSize}
                centerContent={centeredContent}
            />
            <div className={styles.bottomText}>
                {bottomText && (
                    <RichText
                        className={classNames('t-paragraph', {
                            [styles.centerContent]: centeredContent,
                        })}
                        text={bottomText}
                        data-gtm-id={gtmId}
                    />
                )}
                {legalMessage && <LegalMessage className={styles.legalMessage} legalMessage={legalMessage} />}
            </div>
            <div
                className={classNames(styles.buttonsBlock, {
                    [styles.isDualButton]: secondaryLink && secondaryLinkHref,
                })}
            >
                {mainLinkText && mainLinkHref && (
                    <InspireButton
                        link={mainLinkHref}
                        text={mainLinkText}
                        type={isSmallVariant ? 'primary' : mainLinkType}
                        gtmId={getGtmIdByName('mainBanner', mainLinkText)}
                        className={classNames(styles.mainLink, 'truncate')}
                        linkClassName="truncate"
                    />
                )}
                {secondaryLink && secondaryLinkHref && (
                    <InspireButton
                        link={secondaryLinkHref}
                        text={secondaryLink}
                        type={isSmallVariant ? 'primary' : mainLinkType}
                        gtmId={getGtmIdByName('mainBanner', secondaryLink)}
                        className={classNames(styles.secondaryLink, 'truncate')}
                        linkClassName="truncate"
                    />
                )}
            </div>
        </div>
    );
};

const MainTextSection = ({
    mainTextLink,
    mainText,
    gtmId,
    mainTextSize,
    centerContent,
}: {
    mainText: string;
    mainTextLink?: IMainBannerFields['mainTextLink'];
    gtmId: string;
    mainTextSize: string;
    centerContent?: boolean;
}): JSX.Element => {
    const hasUmlautSymbols = UMLAUT_SYMBOLS_REGEX.test(mainText);

    const MainText = () => (
        <TextWithTrademark
            tag="h2"
            text={mainText}
            data-gtm-id={gtmId}
            className={classNames(styles.mainText, {
                [TYPOGRAPHY_CLASS.MAIN_TEXT_H2_SIZE]: mainTextSize === 'h2',
                [TYPOGRAPHY_CLASS.MAIN_TEXT]: mainTextSize !== 'h2',
                [styles.centerContent]: centerContent,
                [styles.mainTextUmlaut]: hasUmlautSymbols,
            })}
        />
    );

    return mainTextLink ? (
        <InspireLink link={mainTextLink}>
            <MainText />
        </InspireLink>
    ) : (
        <MainText />
    );
};

export const ImageSection: FC<{
    image: Asset;
    imageLink?: IMainBannerFields['leftImageLink'];
    gtmId: string;
    className?: string;
}> = ({ image, imageLink, gtmId, className }) => {
    const imageElement = (
        <ContentfulImage
            gtmId={gtmId}
            asset={image}
            maxWidth={IMAGES_CLASS.maxWidth}
            inlineStyles={{ objectFit: IMAGES_CLASS.objectFit }}
        />
    );

    return (
        <div data-gtm-id={gtmId} className={classNames(styles.imageSection, className)}>
            {imageLink ? (
                <InspireLink link={imageLink} className={styles.topLink}>
                    {imageElement}
                </InspireLink>
            ) : (
                imageElement
            )}
        </div>
    );
};

interface IMainBannerProps extends ISectionComponentProps {
    entry: IMainBanner;
}

export default function MainBanner(props: IMainBannerProps): JSX.Element {
    const fields = props.entry.fields;
    const isSmallVariant = useMediaQuery('(max-width: 1024px)');

    if (!fields) return null;

    const {
        backgroundColor,
        backgroundImage,
        bottomText,
        mainLinkHref,
        mainLinkText,
        mainText,
        mainTextLink,
        rightImage,
        rightImageLink,
        leftImage,
        leftImageLink,
        secondaryLink,
        secondaryLinkHref,
        topIcon,
        topText,
        mainTextSize,
        verticallyCentered,
        mainLinkType = DEFAULT_MAIN_LINK_TYPE,
        backgroundVideo,
        backgroundForMobile,
        centeredContent,
        overlayForBackgroundImage,
        legalMessage,
    } = fields;

    const bgImageUrl =
        backgroundForMobile && isSmallVariant
            ? backgroundForMobile?.fields.file?.url
            : backgroundImage?.fields.file?.url;

    const hasBgVideo = !!backgroundVideo;

    const gtmId = getGtmIdByName('mainBanner', mainText);

    return (
        <div className="mainBannerContainer">
            <div
                data-gtm-id={gtmId}
                role="banner"
                className={classNames(styles.mainBanner, {
                    mainBannerConf: !hasBgVideo,
                    [styles.mainBannerLeftImage]: !!leftImage,
                    [styles.mainBannerNoRightImage]: !rightImage,
                    [styles.mainBannerVerticallyCentered]: verticallyCentered,
                    [styles.mainBannerWithBgVideo]: hasBgVideo,
                    [props.className]: props.className,
                })}
            >
                {hasBgVideo && <VideoBlock entry={backgroundVideo} className={styles.bgVideo} width="0" height="0" />}
                {!centeredContent && leftImage && (
                    <ImageSection image={leftImage} imageLink={leftImageLink} gtmId={gtmId} />
                )}
                <ActionsSection
                    bottomText={bottomText}
                    mainLinkHref={mainLinkHref}
                    mainLinkText={mainLinkText}
                    mainLinkType={mainLinkType}
                    mainText={mainText}
                    mainTextLink={mainTextLink}
                    mainTextSize={mainTextSize}
                    secondaryLink={secondaryLink}
                    secondaryLinkHref={secondaryLinkHref}
                    topIcon={topIcon}
                    topText={topText}
                    centeredContent={centeredContent}
                    legalMessage={legalMessage}
                />
                {!centeredContent && rightImage && (
                    <ImageSection
                        className={styles.rightImageSection}
                        image={rightImage}
                        imageLink={rightImageLink}
                        gtmId={gtmId}
                    />
                )}
            </div>
            <style jsx>{`
                .mainBannerConf {
                    background-image: ${bgImageUrl ? `url("${bgImageUrl}")` : 'none'};
                    ${bgImageUrl
                        ? `background-image: url("${bgImageUrl}");
                            background-repeat: no-repeat;
                            background-size: cover;
                            background-position: center;
                            ${overlayForBackgroundImage ? `box-shadow: inset 0 0 0 100vh #${backgroundColor}` : ''}`
                        : ``}
                }
                .mainBannerContainer {
                    background-color: ${backgroundColor ? `#${backgroundColor}` : 'transparent'};
                }
            `}</style>
        </div>
    );
}
