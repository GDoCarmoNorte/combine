import React, { FC } from 'react';

import { ISectionComponentProps } from '..';
import { IImageBlockSection } from '../../../@generated/@types/contentful';
import styles from './imageBlock.module.css';
import { InspireButton } from '../../atoms/button';
import { InspireLink } from '../../atoms/link';
import classnames from 'classnames';
import ContentfulImage from '../../atoms/ContentfulImage';
import sectionIndentsStyles from '../sectionIndents.module.css';
import SectionHeader from '../../atoms/sectionHeader';
import { IMAGE_MAX_WIDTH } from './constants';
import LegalMessage from '../../atoms/legalMessage';
import { spaces2underscores } from '../../../lib/gtm';
import TextWithTrademark from '../../atoms/textWithTrademark';

interface IImageBlockProps extends ISectionComponentProps {
    entry: IImageBlockSection;
}

const ImageBlock: FC<IImageBlockProps> = ({ entry: { fields } }) => {
    if (!fields) return null;
    const backgroundColor = fields.backgroundColor?.fields.hexColor;
    const titleColor = fields.titleColor?.fields.hexColor;
    const descriptionColor = fields.descriptionColor?.fields.hexColor;

    const isSingleCard = fields.cards.length === 1;
    const isDoubleCard = fields.cards.length === 2;
    const isTripleCard = fields.cards.length === 3;

    return (
        <div className={classnames(sectionIndentsStyles.wrapper, styles.container)}>
            {fields?.name && (
                <SectionHeader
                    className={classnames(sectionIndentsStyles.wrapper, styles.header)}
                    textClassName="t-header-h2"
                    text={fields.name}
                />
            )}
            <div
                className={classnames(styles.wrapper, {
                    [styles.grid1]: isSingleCard,
                    [styles.grid2]: isDoubleCard,
                    [styles.grid3]: isTripleCard,
                })}
            >
                {fields?.cards?.map((card) => {
                    const {
                        fields: fieldsCards,
                        sys: { id },
                    } = card;
                    if (!fieldsCards) return null;
                    const { image, cta, description, title, subtitle, imageBlockCtaType, legalMessage } = fieldsCards;

                    const dataGtmId = `promoBanner-${spaces2underscores(title)}`;

                    const CardContent = (
                        <>
                            <ContentfulImage asset={image} className={styles.image} maxWidth={IMAGE_MAX_WIDTH} />
                            <div className={styles.textContent}>
                                {subtitle && (
                                    <p
                                        className={classnames('t-subheader-small', 'truncate', styles.subtitle)}
                                        title={subtitle}
                                    >
                                        {subtitle}
                                    </p>
                                )}
                                <TextWithTrademark
                                    tag={fields.name ? 'h4' : 'h3'}
                                    className={classnames('t-header-card-title', 'truncate', styles.title, {
                                        titleColor: !!titleColor,
                                    })}
                                    title={title}
                                    text={title}
                                />
                                <div>
                                    {description && (
                                        <p
                                            className={classnames('t-paragraph', 'truncate-at-2', styles.description, {
                                                descriptionColor: !!descriptionColor,
                                            })}
                                            title={description}
                                        >
                                            {description}
                                        </p>
                                    )}
                                    {legalMessage && (
                                        <LegalMessage
                                            className={classnames(styles.legalMessage, styles.description, {
                                                descriptionColor: !!descriptionColor,
                                            })}
                                            legalMessage={legalMessage}
                                        />
                                    )}
                                </div>
                                {cta && (
                                    <InspireButton
                                        type={imageBlockCtaType || 'primary'}
                                        text={cta.fields.name}
                                        className={classnames('truncate', styles.cta)}
                                        gtmId={dataGtmId}
                                    />
                                )}
                            </div>
                        </>
                    );

                    if (cta) {
                        return (
                            <InspireLink
                                link={cta.fields.link}
                                className={classnames(styles.wrapperLink, {
                                    [styles.wrapperGrid1]: isSingleCard,
                                    [styles.wrapperGrid2]: isDoubleCard,
                                    [styles.wrapperGrid3]: isTripleCard,
                                })}
                                backgroundColor={fields.backgroundColor}
                                key={id}
                                type={imageBlockCtaType || 'primary'}
                                gtmId={dataGtmId}
                            >
                                <div className={classnames(styles.card, { backgroundColor: !!backgroundColor })}>
                                    {CardContent}
                                </div>
                            </InspireLink>
                        );
                    }

                    return (
                        <div className={classnames(styles.card, { backgroundColor: !!backgroundColor })} key={id}>
                            {CardContent}
                        </div>
                    );
                })}
                <style jsx>{`
                    .backgroundColor {
                        background: #${backgroundColor};
                    }

                    .titleColor {
                        color: #${titleColor};
                    }

                    .descriptionColor {
                        color: #${descriptionColor};
                    }
                `}</style>
            </div>
        </div>
    );
};

export default ImageBlock;
