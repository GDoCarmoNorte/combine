import React from 'react';

import { IMenuCategory, IMenuCategorySection, ITapListMenuCategory } from '../../../@generated/@types/contentful';

import { useLocationOrderAheadAvailability } from '../../../common/hooks/useLocationOrderAheadAvailability';

import SectionCard from '../../atoms/sectionCard';
import { ISectionComponentProps } from '..';

import styles from './menuCategory.module.css';
import { useDomainMenu } from '../../../redux/hooks';
import isTapListMenuCategory from '../../../common/helpers/isTapListMenuCategory';
import { useUnavailableCategories } from '../../../common/hooks/useUnavailableCategories';
import { isMenuCategoryUnavailable } from '../../../common/helpers/menuCategoryHelper';
import { useDomainMenuCategories } from '../../../redux/hooks/domainMenu';

interface IMenuCategorySectionProps extends ISectionComponentProps {
    entry: IMenuCategorySection;
}

function MenuCategoryUI(props: IMenuCategorySectionProps): JSX.Element {
    const {
        actions: { getAvailableCategories },
    } = useDomainMenu();
    const domainMenuCategories = useDomainMenuCategories();

    const { backgroundPosition, backgroundBottomText, backgroundBottomTextSecondPart } = props.entry.fields;
    const { categories: cmsCategories } = props.entry.fields;
    const unavailableCategories = useUnavailableCategories();

    const isNotVisibleCategory = (category: IMenuCategory | ITapListMenuCategory): boolean =>
        !isTapListMenuCategory(category) &&
        isMenuCategoryUnavailable(category, unavailableCategories, domainMenuCategories);

    const availableCategories = getAvailableCategories(cmsCategories).filter(
        (category) => !isNotVisibleCategory(category)
    );

    const threeColumn = availableCategories.length % 3 === 0 ? styles.hideWhenWide : '';
    const needsFiller = availableCategories.length % 3 === 1 ? styles.showWhenWide : styles.hidden;
    const { isLocationOrderAheadAvailable } = useLocationOrderAheadAvailability();

    return (
        <div className={styles.sectionFlexContainer} aria-label="Category Menu Selector">
            {backgroundPosition === 'front' && (
                <div className={`${styles.menuCategoryCard} ${threeColumn} ${styles.backgroundBottomTextSection}`}>
                    {backgroundBottomText ? (
                        <div className={styles.backgroundBottomText}>
                            <div>{backgroundBottomText}</div>
                            <div>{backgroundBottomTextSecondPart}</div>
                        </div>
                    ) : null}
                </div>
            )}
            {availableCategories.map((c) => (
                <SectionCard isLocationOrderAvailable={isLocationOrderAheadAvailable} card={c} key={c.sys.id} />
            ))}

            <section className={`${styles.menuCategoryCard} ${needsFiller}`} />

            {backgroundPosition === 'back' && (
                <div className={`${styles.menuCategoryCard} ${threeColumn} ${styles.backgroundBottomTextSection}`}>
                    {backgroundBottomText ? (
                        <div className={styles.backgroundBottomText}>
                            <div>{backgroundBottomText}</div>
                            <div>{backgroundBottomTextSecondPart}</div>
                        </div>
                    ) : null}
                </div>
            )}
        </div>
    );
}

export default MenuCategoryUI;
