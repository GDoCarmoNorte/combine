import dynamic from 'next/dynamic';
import { locationSpecificMenuCategoriesEnabled } from '../../../lib/getFeatureFlags';
import withSectionLayout from '../withSectionLayout';

const isSSR = !locationSpecificMenuCategoriesEnabled();

const MenuCategoryUI = dynamic(import('./menuCategory'), {
    ssr: isSSR,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

const MenuCategorySection = withSectionLayout(MenuCategoryUI, { showBullet: true, headerTag: 'h2' });

export default MenuCategorySection;
