import React from 'react';
import classNames from 'classnames';
import Link from 'next/link';

import styles from './siteMapLinks.module.css';
import Icon from '../../atoms/BrandIcon';
import { ISiteMapLinkBlock } from '../../../common/helpers/siteMapHelper';

interface ISiteMapLinksProps {
    linkBlock: ISiteMapLinkBlock;
}

const SiteMapLinks = (props: ISiteMapLinksProps): JSX.Element => {
    const { linkBlock } = props;

    const isLinkBlock = linkBlock.name && linkBlock.nameInUrl;

    return (
        <div
            className={classNames({
                [styles.linkBlock]: linkBlock.name,
            })}
        >
            {isLinkBlock && (
                <Link href={linkBlock.nameInUrl}>
                    <a className={classNames(styles.linkBlockName, styles.link)}>{linkBlock.name}</a>
                </Link>
            )}

            {linkBlock.links?.length && (
                <ul className={styles.list}>
                    {linkBlock.links.map((link, index) => {
                        return (
                            <li key={`SiteMapLink-${index}-${link.name}`} className={styles.listItem}>
                                <Link href={link.nameInUrl}>
                                    <a target={link.isExternal ? '_blank' : undefined} className={styles.link}>
                                        {link.name}
                                    </a>
                                </Link>
                                {link.isExternal && <Icon className={styles.externalIcon} icon="action-open-in-new" />}
                            </li>
                        );
                    })}
                </ul>
            )}
        </div>
    );
};

export default SiteMapLinks;
