import React from 'react';
import classNames from 'classnames';
import Link from 'next/link';
import SiteMapLinks from './siteMapLinks';

import sectionIndentsStyles from '../sectionIndents.module.css';
import styles from './siteMapCategory.module.css';
import { ISiteMapCategory } from '../../../common/helpers/siteMapHelper';

const SiteMapCategory = (props: ISiteMapCategory): JSX.Element => {
    const { mainLink, name, linkBlocks } = props;

    return (
        <div className={classNames(sectionIndentsStyles.wrapper, styles.wrapper)}>
            <Link href={mainLink}>
                <a className={classNames(styles.mainLink, styles.link)}>{name}</a>
            </Link>
            <div
                className={classNames(styles.columns, {
                    [styles.multiColumns]: linkBlocks.length > 1 || linkBlocks[0].links.length > 4,
                    [styles.linkBlockColumns]: linkBlocks.length > 1,
                })}
            >
                {linkBlocks &&
                    linkBlocks.map((linkBlock, index) => (
                        <SiteMapLinks key={`SiteMapLinks-${index}-${linkBlock.name}`} linkBlock={linkBlock} />
                    ))}
            </div>
            <hr className={styles.hr} />
        </div>
    );
};

export default SiteMapCategory;
