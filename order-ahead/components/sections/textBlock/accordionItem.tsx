import React, { FC, useState } from 'react';
import classNames from 'classnames';
import { Collapse } from '@material-ui/core';

import { ITextBlockSectionAccordionItem } from '../../../@generated/@types/contentful';
import Icon from '../../atoms/BrandIcon';
import RichText from '../../atoms/richText';
import styles from './accordionItem.module.css';

interface IAccordionItemProps {
    entry: ITextBlockSectionAccordionItem;
    contentClassName?: string;
}

const AccordionItem: FC<IAccordionItemProps> = ({
    entry: {
        fields: { text, title, content },
    },
    contentClassName,
}) => {
    const [collapsed, setCollapsed] = useState(false);

    return (
        <div
            className={classNames(styles.wrapper, {
                [styles.collapsed]: collapsed,
            })}
        >
            <div className={styles.header} onClick={() => setCollapsed(!collapsed)}>
                <div className={styles.wrapperHeadings}>
                    <span
                        className={classNames('t-subheader', 'truncate', {
                            [styles.collapsed]: collapsed,
                        })}
                    >
                        {title}
                    </span>
                    {text && <span className={classNames('t-paragraph-hint', 'truncate', styles.text)}>{text}</span>}
                </div>
                <Icon className={styles.icon} icon={collapsed ? 'direction-up' : 'direction-down'} />
            </div>
            <Collapse in={collapsed}>
                <RichText className={classNames(styles.content, contentClassName)} text={content} />
            </Collapse>
        </div>
    );
};

export default AccordionItem;
