import React, { useCallback, useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import Head from 'next/head';

import AboutSection from '../../../components/sections/aboutSection/aboutSection';
import usePdp from '../../../redux/hooks/usePdp';
import useBag from '../../../redux/hooks/useBag';
import { useLocalization } from '../../../common/hooks/useLocalization';

import {
    useProductSizes,
    useDomainMenuSelectors,
    useNutrition,
    useDomainProductByContentfulFields,
} from '../../../redux/hooks/domainMenu';

import { InspireButton } from '../../../components/atoms/button';
import ModifyProductModal from '../../../components/organisms/modifyProductModal';
import { getUpgradeMealPath } from '../../../lib/domainProduct';

import Footer from '../../../components/organisms/footer';
import BaseHeader from '../../../components/organisms/header/baseHeader';
import {
    GTM_PRODUCT_VIEW,
    GTM_MODIFY_PRODUCT,
    GTM_COMBO_SELECTION,
} from '../../../common/services/gtmService/constants';
import ModifierComboGroupData from '../../../components/organisms/ModifierComboGroupData';
import ComboMainProductModifier from '../../../components/organisms/comboMainProductModifier';
import Breadcrumbs from '../../../components/atoms/Breadcrumbs';
import { applyModifiersToMeal } from '../../../lib/tallyItem';
import NotificationModal from '../../../components/molecules/notificationModal';
import useNavIntercept from '../../../redux/hooks/useNavIntercept';
import { setHasChanges } from '../../../redux/navIntercept';
import { useAppDispatch } from '../../../redux/store';
import getBrandInfo from '../../../lib/brandInfo';
import { FeatureFlagsContext } from '../../../redux/hooks/useFeatureFlags';
import { useDisplayModifierGroupsByProductId, useDisplayProduct } from '../../../redux/hooks/pdp';
import { getPdpPageTitle } from '../../../common/helpers/getPageTitle';
import { getPDPPageDescription } from '../../../common/helpers/getPageDescription';
import { useComboPriceIncreaseAnalytics } from '../../../common/hooks/pdpHooks';
import useGtmComboData from '../../../common/hooks/useGtmComboData';
import { ProductTypesEnum } from '../../../redux/types';
import { PageContentWrapper } from '../../../components/sections/PageContentWrapper';
import SchemaPdpAndPlp from '../../../common/helpers/schemaPdpAndPlp';
import { useProductIsSaleable } from '../../../common/hooks/useProductIsSaleable';
import { NOT_SALEABLE_BUTTON_LINK, NOT_SALEABLE_BUTTON_TEXT } from '../../../common/constants/product';

import styles from './productDetailsContainer.module.css';
import { IProductDetailsContainerProps } from './types';
import CaloriesLegalSection from '../../organisms/caloriesLegalSection/caloriesLegelSection';

const FloatingBanner = dynamic(import('../../../components/organisms/floatingBanner/floatingBanner'), {
    ssr: false,
    loading: () => null,
});

const ProductBanner = dynamic(import('../../../components/organisms/productBanner'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default function ProductDetailsContainer(props: IProductDetailsContainerProps): JSX.Element {
    const brandInfo = getBrandInfo();
    const {
        product: defaultProduct,
        currentCategory,
        productNutritionLink,
        canonicalPath,
        featureFlags,
        isPreviewMode,
        caloriesLegal,
    } = props;
    const { modifierItemsById, productsById } = props.globalProps;

    const pdp = usePdp();
    const { selectRelatedComboByMainProductIdAndSizeId, selectDefaultTallyItem } = useDomainMenuSelectors();
    const pdpTallyItem = pdp.pdpTallyItem;
    const { productId } = pdpTallyItem;

    const product = productsById[productId] || defaultProduct; // defaultProduct needs in case Product for some size is not specified in contentul
    const domainProduct = useDomainProductByContentfulFields(defaultProduct.fields);
    const { name } = product.fields;

    pdp.useInitTallyItem(domainProduct?.id);

    const { isSaleable } = useProductIsSaleable(productId);
    const { locationLinkText, isProductAvailable } = useLocalization(productId);
    const bag = useBag();

    const router = useRouter();

    const { path: makeItAMealPath, mealId } = getUpgradeMealPath(
        domainProduct,
        props.globalProps.productDetailsPagePaths
    );

    const { categoryName } = currentCategory?.fields || { categoryName: '' };

    const displayProduct = useDisplayProduct();

    const productIdForChildSize = pdpTallyItem?.childItems?.length && pdpTallyItem.childItems[0].productId; // TODO: Improve this
    const sizeSelections = useProductSizes(productId);
    const dispatch = useAppDispatch();

    const productInfo = {
        price: !locationLinkText && displayProduct.totalPrice ? displayProduct.totalPrice : null,
        calories: displayProduct?.calories,
    };

    useEffect(() => {
        dispatch({
            type: GTM_PRODUCT_VIEW,
            payload: {
                product,
                price: productInfo.price,
                category: categoryName,
            },
        });

        // reset state from bag on another page
        return () => {
            pdp.actions.resetPdpState();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const isCombo = displayProduct?.productType === ProductTypesEnum.Meal;
    const isPromo = displayProduct?.productType === ProductTypesEnum.Promo;

    const gtmComboData = useGtmComboData(pdpTallyItem, displayProduct);

    useEffect(() => {
        if (isCombo && pdpTallyItem.madeAMeal) {
            dispatch({ type: GTM_COMBO_SELECTION, payload: gtmComboData });

            pdp.actions.putTallyItem({
                pdpTallyItem,
                madeAMeal: false,
            });
        }
    }, [makeItAMealPath, pdpTallyItem, gtmComboData, dispatch, isCombo, pdp.actions]);

    useComboPriceIncreaseAnalytics(pdpTallyItem, displayProduct, router.asPath);

    const [modalOpened, setModalOpened] = useState(false);

    const handleMakeItAMealClick = () => {
        const mealPdpTallyItem = selectDefaultTallyItem(mealId);
        const updatedMealPdpTallyItem = applyModifiersToMeal(pdpTallyItem, mealPdpTallyItem);

        if (updatedMealPdpTallyItem) {
            pdp.actions.putTallyItem({
                pdpTallyItem: updatedMealPdpTallyItem,
                // mark as made a meal to trigger analytics
                madeAMeal: true,
            });
        }

        dispatch(setHasChanges(false)); // unsaved modifiers moved into meal

        router.push(makeItAMealPath);
    };

    const handleComboMainProductSizeSelect = (id: string) => {
        const oldPdpTallyItemQuantity = pdpTallyItem?.quantity || 1;
        const newComboItem = selectRelatedComboByMainProductIdAndSizeId(id, domainProduct.sizeGroupId);
        const newTallyItem = selectDefaultTallyItem(newComboItem.id);

        const newComboTallyItem = {
            ...newTallyItem,
            childItems: newTallyItem?.childItems?.map((el, i) => ({
                ...el,
                lineItemId: pdpTallyItem?.childItems[i]?.lineItemId,
            })),
        };

        pdp.actions.putTallyItem({
            pdpTallyItem: { ...newComboTallyItem, quantity: oldPdpTallyItemQuantity },
            lineItemId: pdpTallyItem.lineItemId,
        });
    };

    const handleAddToBag = () => {
        bag.actions.putToBag({ pdpTallyItem, category: categoryName, name });
        // After item is added to bag, return PDP to default state
        pdp.actions.resetPdpState();
    };

    const handleModifyProduct = useCallback(() => {
        dispatch({ type: GTM_MODIFY_PRODUCT });
        setModalOpened(true);
    }, [dispatch]);

    const handleCloseModal = useCallback((e) => {
        e.stopPropagation();
        setModalOpened(false);
    }, []);

    const { alertBanners, navigation, footer, userAccountMenu } = props.globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const isUpdating = !!pdpTallyItem && !!pdpTallyItem.lineItemId;
    const addToBagBtnText = isUpdating
        ? pdpTallyItem.quantity > 1
            ? `Update (${pdpTallyItem.quantity} items)`
            : 'Update'
        : 'Add to bag';

    const showMakeItAMealButton = isSaleable && !locationLinkText && makeItAMealPath && pdpTallyItem.quantity === 1;

    const hasUnsavedModifications = pdp.useHasUnsavedModifications();

    const { isPending, navigate } = useNavIntercept();

    useEffect(() => {
        dispatch(setHasChanges(hasUnsavedModifications));
    }, [hasUnsavedModifications, router.asPath, dispatch]);

    const onConfirm = () => navigate(false);
    const onDismiss = () => {
        pdp.actions.resetPdpState();
        navigate(true);
    };

    const mainSection = displayProduct.productSections.find(
        (productSection) => productSection.productSectionType === 'main'
    );

    const sectionIndex = 0;

    const modifierGroups = useDisplayModifierGroupsByProductId(
        displayProduct.productId,
        displayProduct?.productSections?.[0]?.productGroupId || domainProduct?.productGroupId,
        sectionIndex
    );

    const nutrition = useNutrition(productId);
    const dataSchema = SchemaPdpAndPlp(currentCategory, brandInfo, product, nutrition);

    const renderButtons = () => {
        if (!isSaleable) {
            return (
                <InspireButton
                    className={styles.addToBagButton}
                    link={NOT_SALEABLE_BUTTON_LINK}
                    type="primary"
                    text={NOT_SALEABLE_BUTTON_TEXT}
                />
            );
        }

        if (locationLinkText) {
            return (
                <InspireButton
                    className={styles.addToBagButton}
                    link={'/locations'}
                    type="primary"
                    text={locationLinkText}
                />
            );
        }

        return (
            <>
                {showMakeItAMealButton && (
                    <>
                        <InspireButton
                            className={styles.makeMealButton}
                            onClick={handleMakeItAMealClick}
                            type="primary"
                            text="Make it a meal"
                        />
                        <div className={styles.separator} />
                    </>
                )}
                <InspireButton
                    className={styles.addToBagButton}
                    onClick={handleAddToBag}
                    type="primary"
                    text={addToBagBtnText}
                />
            </>
        );
    };

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                isPreviewMode={isPreviewMode}
                userAccountMenu={userAccountMenu}
            />
            <div className="container">
                <Head>
                    <title>{getPdpPageTitle(brandInfo.brandName, product.fields.name)}</title>
                    <link rel="canonical" href={canonicalPath} />
                    <meta name="description" content={getPDPPageDescription(product)} />
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(dataSchema) }}
                    />
                </Head>
                <FloatingBanner
                    offset={400}
                    product={product}
                    onAddToBag={handleAddToBag}
                    productInfo={productInfo}
                    domainProduct={domainProduct}
                    addToBagBtnText={addToBagBtnText}
                />
                <div className={styles.breadcrumbs}>
                    <Breadcrumbs />
                </div>
                <PageContentWrapper>
                    <div>
                        <div key={5} className={styles.centerFlex}>
                            <ProductBanner
                                product={product}
                                displayName={displayProduct.displayName}
                                mainProductSectionName={displayProduct.mainProductSectionName}
                                productInfo={productInfo}
                                onAddToBag={handleAddToBag}
                                onModifyProduct={handleModifyProduct}
                                selections={sizeSelections}
                                makeItAMealPath={makeItAMealPath}
                                onMakeItAMeal={handleMakeItAMealClick}
                                onSizeSelect={(productId) => {
                                    pdp.actions.editTallyItemSize(productId);
                                }}
                                addToBagBtnText={addToBagBtnText}
                                showMakeItAMealButton={showMakeItAMealButton}
                                productType={displayProduct?.productType}
                                childProductId={productIdForChildSize}
                            />
                        </div>
                        {isSaleable && !locationLinkText && isProductAvailable && (
                            <div className={styles.modifiersWrapper} aria-label="Modifier Combo">
                                {isCombo ? (
                                    <ComboMainProductModifier
                                        section={mainSection}
                                        id={productIdForChildSize}
                                        onSizeSelect={handleComboMainProductSizeSelect}
                                    />
                                ) : null}
                                <ModifierComboGroupData
                                    displayProduct={displayProduct}
                                    modifierItemsById={modifierItemsById}
                                    productsById={productsById}
                                    isPromo={isPromo}
                                />
                            </div>
                        )}
                        <div className={styles.caloriesLegal}>
                            <CaloriesLegalSection prop={caloriesLegal}></CaloriesLegalSection>
                        </div>
                    </div>
                    <div>
                        <AboutSection
                            category={currentCategory}
                            isPromo={isPromo}
                            productNutritionLink={productNutritionLink}
                        />
                    </div>
                </PageContentWrapper>
                <NotificationModal
                    open={isPending}
                    onModalClose={onDismiss}
                    title="cancel your modifications?"
                    message="You haven’t added this item to your bag or saved your modifications. If you leave this page, your
                    most recent changes will be lost."
                    confirmButtonProps={{
                        text: 'Yes, cancel my changes',
                        onClick: onDismiss,
                    }}
                    rejectButtonProps={{
                        text: 'No, Return to modifications',
                        onClick: onConfirm,
                    }}
                />
                {pdpTallyItem && (
                    <ModifyProductModal
                        contentfulProduct={product}
                        displayProduct={displayProduct}
                        childProductId={productIdForChildSize}
                        open={modalOpened}
                        onClose={handleCloseModal}
                        modifierItemsById={modifierItemsById}
                        productsById={productsById}
                        sectionIndex={sectionIndex}
                        modifierGroups={modifierGroups}
                    />
                )}
                <div className={styles.buttonsBlock}>{renderButtons()}</div>
                {footer && <Footer footer={footer} />}
            </div>
        </FeatureFlagsContext.Provider>
    );
}
