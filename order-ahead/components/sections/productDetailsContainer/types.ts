import { Entry } from 'contentful';
import {
    IProductFields,
    IMenuCategory,
    IProductNutritionLinkFields,
    ICaloriesLegal,
} from '../../../@generated/@types/contentful';
import { IGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import { IFeatureFlags } from '../../../lib/getFeatureFlags';

export interface IProductDetailsContainerProps {
    product: Entry<IProductFields>;
    currentCategory: IMenuCategory;
    productNutritionLink: IProductNutritionLinkFields | null;
    canonicalPath: string;
    featureFlags: IFeatureFlags;
    isPreviewMode?: boolean;
    globalProps: IGlobalContentfulProps;
    caloriesLegal?: ICaloriesLegal;
}
