export const DUPLICATE_EMAIL_MESSAGE_TITLE = 'OOPS!';
export const DUPLICATE_EMAIL_MESSAGE_TEXT = `That email is already registered to receive emails from Arby’s. Because "past you" loved sandwiches just as much as "present you" does. If you're still having trouble, contact us.`;
export const DEFAULT_ERROR_MESSAGE_TEXT = 'Failed sign up to updates';
