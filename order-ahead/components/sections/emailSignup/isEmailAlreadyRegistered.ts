const isEmailAlreadyRegistered = (e?: { errors: [{ reasonCode: string }] }) => {
    return !!e?.errors?.find((it) => it.reasonCode === 'CDS_MKTG_PREF_OPTED_IN');
};

export default isEmailAlreadyRegistered;
