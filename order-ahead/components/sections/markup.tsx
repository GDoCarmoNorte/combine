import React from 'react';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';
import classnames from 'classnames';

import { IMarkup } from '../../@generated/@types/contentful';
import { ISectionComponentProps } from '.';

import sectionIndentsStyles from './sectionIndents.module.css';
import styles from './markup.module.css';
import useRichTextOptions from '../../common/hooks/useRichTextOptions';

interface IMarkupProps extends ISectionComponentProps {
    entry: IMarkup;
    className: string;
}

export default function Markup(props: IMarkupProps): JSX.Element {
    const { entry, className } = props;
    const richTextOptions = useRichTextOptions();

    return (
        <div
            className={classnames(sectionIndentsStyles.wrapper, styles.markup, {
                [className]: !!className,
            })}
            dangerouslySetInnerHTML={{ __html: documentToHtmlString(entry.fields.markup, richTextOptions) }}
        />
    );
}
