import React from 'react';
import classnames from 'classnames';
import { IAppLink, IExternalLink, IMiniBanners } from '../../../@generated/@types/contentful';
import SectionHeader from '../../atoms/sectionHeader';
import withSectionLayout from '../../sections/withSectionLayout';
import ContentfulImage from '../../atoms/ContentfulImage';
import { InspireButton } from '../../atoms/button';
import { InspireLink } from '../../atoms/link';
import { ISectionComponentProps } from '..';
import { mobileDeviceSpecificLinkFilter } from './mobileDeviceSpecificLinkFilter';
import styles from './miniBanners.module.css';

interface IMiniBannersProps extends ISectionComponentProps {
    entry: IMiniBanners;
}

const MiniBanners = (props: IMiniBannersProps): JSX.Element => {
    const fields = props.entry.fields;

    const { title, links: miniBannersItems } = fields;

    if (miniBannersItems.some((miniBannersItem) => !miniBannersItem.fields)) return null;

    const gtmId = 'miniBanners';

    return (
        <section className={styles.sectionFlexContainer} data-gtm-id={gtmId}>
            <SectionHeader text={title} tag="h2" textClassName={classnames('t-header-h2')} />
            <div data-gtm-id={gtmId} className={styles.miniBanners}>
                {miniBannersItems.map((miniBanner) => (
                    <div key={miniBanner.sys.id} data-gtm-id={gtmId} className={styles.miniBanner}>
                        <ContentfulImage maxWidth={397} asset={miniBanner.fields.image} />
                        <div className={styles.miniBannerContent}>
                            <h3 className={classnames('t-subheader', styles.secondaryTitle)}>
                                {miniBanner.fields.secondaryTitle}
                            </h3>
                            <h2 className={classnames('t-header-card-title', styles.title)}>
                                {miniBanner.fields.title}
                            </h2>
                            <p className={classnames('t-paragraph', styles.description)}>
                                {miniBanner.fields.description}
                            </p>
                            <div className={styles.buttonSection}>
                                {miniBanner.fields.links.filter(mobileDeviceSpecificLinkFilter).map((link) =>
                                    (link as IAppLink).fields?.image ? (
                                        <InspireLink link={(link as IAppLink).fields.url} newtab>
                                            <ContentfulImage asset={(link as IAppLink).fields.image} />
                                        </InspireLink>
                                    ) : (
                                        <InspireButton
                                            className="truncate"
                                            link={(link as IExternalLink).fields.nameInUrl}
                                            text={(link as IExternalLink).fields.name}
                                            type="primary"
                                        />
                                    )
                                )}
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default withSectionLayout(MiniBanners, { showBullet: true, headerTag: 'h2', className: styles.sectionLayout });
