import React from 'react';
import classnames from 'classnames';

import { ITopPicks } from '../../../@generated/@types/contentful';
import VerticalProductCard from '../../atoms/verticalProductCard';
import HScroller from '../../atoms/HScroller';
import { getGtmIdByName } from '../../../lib/gtm';
import { getImageUrl } from '../../../common/helpers/contentfulImage';

import ContentfulImage from '../../atoms/ContentfulImage';

import { ISectionComponentProps } from '..';

import styles from './topPicks.module.css';
import useTopPicksFilter from '../../../common/hooks/useTopPicksFilter';
interface ITopPicksProps extends ISectionComponentProps {
    entry: ITopPicks;
}

const TopPicks = (props: ITopPicksProps): JSX.Element => {
    const fields = props.entry.fields;
    const productsFilter = useTopPicksFilter();

    if (!(fields && fields.category)) return null;

    const {
        leftSideTopText: subtitle,
        leftSideMainText: title,
        leftSideImage,
        category,
        backgroundColor,
        textColor,
        backgroundImage,
        isSideScroll,
    } = fields;

    const {
        products,
        link: {
            fields: { nameInUrl },
        },
    } = category.fields;

    const availableProducts = products.filter(productsFilter);

    if (!availableProducts.length) return null;

    const gtmId = 'topPicks';

    return (
        <div className={styles.container}>
            <div className={classnames('bgBlock', styles.bgBlock)}></div>
            <div
                data-gtm-id={gtmId}
                className={classnames(styles.topPicks, {
                    [styles.withSubtitle]: !!subtitle,
                })}
                aria-label="Top Pick Products"
            >
                <div data-gtm-id={gtmId} className={classnames('content', styles.content)}>
                    <div data-gtm-id={gtmId} className={styles.wrapperHeadings}>
                        {subtitle && (
                            <div data-gtm-id={gtmId} className={styles.subtitle}>
                                {subtitle}
                            </div>
                        )}
                        <h2 data-gtm-id={gtmId} className={classnames('t-header-h1', styles.title)}>
                            {title}
                        </h2>
                        {leftSideImage && (
                            <ContentfulImage asset={leftSideImage} maxWidth={257} className={styles.leftSideImage} />
                        )}
                    </div>
                </div>
                <div data-gtm-id={gtmId} className={styles.topPicksContainer}>
                    <HScroller
                        listClassName={classnames(styles.items, {
                            [styles.itemsGrid]: !isSideScroll,
                        })}
                        className={classnames(styles.itemsWrapper, {
                            [styles.itemsWrapperGrid]: !isSideScroll,
                        })}
                        scrollStep={225}
                        prevButtonClassName={styles.prevButton}
                        nextButtonClassName={styles.nextButton}
                    >
                        {availableProducts.map((item, index) => (
                            <VerticalProductCard
                                isSideScroll={isSideScroll}
                                category={nameInUrl}
                                item={item}
                                key={item.sys.id}
                                className={classnames(styles.item, {
                                    [styles.itemGrid]: !isSideScroll,
                                })}
                                gtmId={getGtmIdByName(`topPicksItem-${index}`, item.fields?.name)}
                            />
                        ))}
                    </HScroller>
                </div>
            </div>
            <style jsx>{`
                .bgBlock {
                    background-image: ${backgroundImage ? `url(${getImageUrl(backgroundImage)})` : 'none'};
                    background-color: ${backgroundColor ? `#${backgroundColor}` : 'var(--col--primary1)'};
                }
                .content {
                    color: ${textColor ? `#${textColor}` : 'var(--col--light)'};
                }
            `}</style>
        </div>
    );
};

export default TopPicks;
