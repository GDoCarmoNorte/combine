import React from 'react';
import classnames from 'classnames';

import styles from './withSectionLayout.module.css';
import { ISectionComponent, ISectionComponentProps } from '.';
import SectionHeader, { ISectionHeaderProps } from '../atoms/sectionHeader';

interface IWithSectionLayoutParams {
    showBullet?: boolean;
    headerTag?: ISectionHeaderProps['tag'];
    className?: string;
    classes?: { [key: string]: { [key: string]: string } };
    headerClassName?: string;
}

export default function withSectionLayout(
    Component: ISectionComponent | React.ComponentClass<ISectionComponentProps>,
    { showBullet, headerTag, className, headerClassName }: IWithSectionLayoutParams = {}
): ISectionComponent {
    const Wrapper = (props: ISectionComponentProps) => {
        // @ts-ignore
        const { name, backgroundPosition, backgroundColor, backgroundText, backgroundBottomText } = props.entry.fields;
        // @ts-ignore
        const { backgroundImage, fullSizeBackgroundImage } = props.entry.fields;
        const classes = props.classes?.sectionLayout;
        const bgImageUrl = backgroundImage?.fields.file?.url;
        const bgHexColor = backgroundColor?.fields.hexColor;
        const isBackgroundImagePartial = !fullSizeBackgroundImage && bgImageUrl;

        const backgroundTileClasses = classnames(styles.backgroundTile, {
            'background-tile-color': !fullSizeBackgroundImage,
            'background-tile-image': isBackgroundImagePartial,
            [styles.top]: backgroundPosition === 'front',
            [styles.bottom]: backgroundPosition === 'back',
            [styles.hidden]: !backgroundPosition || backgroundPosition === 'hidden',
        });

        return (
            <>
                <div
                    className={classnames(styles.sectionWrapper, {
                        [className]: !!className,
                        [styles.sectionWrapperBackground]: fullSizeBackgroundImage,
                        'wrapper-with-background': fullSizeBackgroundImage,
                        [`wrapper-with-partial-background ${styles.partialBackground}`]: isBackgroundImagePartial,
                    })}
                >
                    <div
                        className={classnames(
                            styles.wrapperComponents,
                            {
                                [styles.withBackgroundText]: !!backgroundText,
                            },
                            classes?.wrapper
                        )}
                    >
                        <SectionHeader
                            showBullet={!!showBullet}
                            text={name}
                            tag={headerTag}
                            className={classnames({
                                [styles.backgroundPosition]: backgroundPosition === 'front',
                                [styles.headerLight]: fullSizeBackgroundImage && bgImageUrl,
                            })}
                            textClassName={classnames(
                                {
                                    ['t-header-h2']: isBackgroundImagePartial,
                                    [styles.sectionHeaderText]: isBackgroundImagePartial,
                                },
                                classes?.mainTitle,
                                headerClassName
                            )}
                        />
                        <div className={classnames('truncate', styles.backgroundText)}>{backgroundText}</div>
                    </div>
                    <div className={classnames(styles.sectionFlexContainer, classes?.mainContent)}>
                        <Component {...props} />
                    </div>
                    {backgroundBottomText && <div className={backgroundTileClasses} />}
                </div>
                <style jsx>{`
                    .background-tile-color {
                        background-color: ${backgroundColor && `#${backgroundColor.fields.hexColor}`};
                    }
                    .background-tile-image {
                        background-image: ${bgImageUrl ? `url("${bgImageUrl}")` : 'none'};
                    }
                    .wrapper-with-background {
                        background-color: ${bgHexColor ? `#${bgHexColor}` : 'transparent'};
                        background-image: ${bgImageUrl ? `url("${bgImageUrl}")` : 'none'};
                        background-repeat: ${bgImageUrl ? 'no-repeat' : 'initial'};
                        background-size: ${bgImageUrl ? 'cover' : 'initial'};
                    }
                    .wrapper-with-partial-background {
                        background-color: ${bgHexColor ? `#${bgHexColor}` : 'transparent'};
                        background-image: ${bgImageUrl ? `url("${bgImageUrl}")` : 'none'};
                        background-repeat: ${bgImageUrl ? 'no-repeat' : 'initial'};
                    }
                `}</style>
            </>
        );
    };

    Wrapper.displayName = `withSectionLayout(${Component.name || 'Unknown'})`;

    return Wrapper;
}
