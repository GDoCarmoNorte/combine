const aboutSectionData = {
    itemType: 'Sandwich',
    productDescription:
        'Take a bite of Arby’s most savory sandwich: slow-roasted chicken, crispy pepper bacon, Swiss cheese, and honey mustard. Then take another. You’re getting the hang of it.',
    includedItems: [
        'Roast Chicken',
        'Tomatoes',
        'Star Cut Bun',
        'Pepper Bacon',
        'Shredded Lettuce',
        'Swiss Cheese',
        'Honey Mustard Sauce',
    ],
};

export default aboutSectionData;
