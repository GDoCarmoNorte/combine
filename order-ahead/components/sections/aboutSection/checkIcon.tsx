import React from 'react';
import Icon from '../../atoms/BrandIcon';
import styles from './checkIcon.module.css';

const CheckIcon: React.FC = () => {
    return <Icon icon="action-check" size="s" className={styles.checkIcon} />;
};

export default CheckIcon;
