import React, { useCallback, useRef } from 'react';

import classNames from 'classnames';
import styles from './aboutSection.module.css';
import { IMenuCategory, IProductNutritionLinkFields } from '../../../@generated/@types/contentful';
import SectionHeader from '../../atoms/sectionHeader';
import Nutrition from '../../organisms/nutrition/nutrition';
import ContentfulImage from '../../atoms/ContentfulImage';
import CheckIcon from './checkIcon';
import { useAboutSection } from '../../../redux/hooks/pdp';
export interface IAboutSectionProps {
    category: IMenuCategory;
    isPromo?: boolean;
    productNutritionLink: IProductNutritionLinkFields | null;
}

export default function AboutSection({ category, isPromo, productNutritionLink }: IAboutSectionProps): JSX.Element {
    const aboutSectionInfo = useAboutSection();
    const fields = category?.fields;

    const containerRef = useRef<HTMLDivElement>();

    const handleHideNutrition = useCallback(() => {
        const el = containerRef.current;
        const rect = el.getBoundingClientRect();
        const stickyTop = document.getElementById('sticky-top');
        const floatingBanner = document.getElementById('floating-banner');

        const scrollTopValue =
            window.scrollY - (stickyTop?.offsetHeight || 0) - (floatingBanner?.offsetHeight || 0) + (rect?.top || 0);

        window.scroll({
            top: scrollTopValue,
            behavior: 'smooth',
        });
    }, []);

    if (!(fields && aboutSectionInfo)) {
        return null;
    }

    const { icon, taglineDescription, taglineHeading } = fields;
    const { description, whatsIncluded, productKind, nutritionInfo } = aboutSectionInfo;

    return (
        <div className={styles.aboutContainer} ref={containerRef}>
            <SectionHeader
                className={styles.header}
                textClassName="t-header-h2"
                tag="h2"
                text={`About the ${productKind}`}
            />
            <div className={styles.aboutBody}>
                <div className={styles.aboutSection}>
                    {whatsIncluded.length > 0 && !isPromo && (
                        <div className={styles.includedSection}>
                            <div className="t-subheader-universal-smaller">WHAT&apos;S INCLUDED</div>
                            <div className={styles.includedItems}>
                                {whatsIncluded?.length > 0 &&
                                    whatsIncluded.map((item, i) => (
                                        <div key={i} className={styles.includedItem}>
                                            <CheckIcon />
                                            <div
                                                className={classNames(
                                                    styles.includedItemText,
                                                    't-subheader-universal-smaller'
                                                )}
                                            >
                                                {item}
                                            </div>
                                        </div>
                                    ))}
                            </div>
                        </div>
                    )}
                    {description && (
                        <div className={classNames('t-paragraph', styles.productDescription)}>{description}</div>
                    )}
                    {nutritionInfo && (
                        <Nutrition
                            nutritionLink={productNutritionLink}
                            nutritionInfo={nutritionInfo}
                            isPromo={isPromo}
                            onHide={handleHideNutrition}
                        />
                    )}
                </div>
                <div className={styles.taglineSection}>
                    <ContentfulImage asset={icon} className={styles.icon} />
                    <div className={styles.taglineText}>
                        <div className={classNames(styles.taglineHeading, 't-subheader-small')}>{taglineHeading}</div>
                        <div className={classNames(styles.taglineDescription, 't-subheader')}>{taglineDescription}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
