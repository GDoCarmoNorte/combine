import React from 'react';
import { IErrorBanner } from '../../../@generated/@types/contentful';
import { ISectionComponentProps } from '..';

import ErrorBannerContent from '../../clientOnly/errorBannerContent';

interface IErrorBannerProps extends ISectionComponentProps {
    entry: IErrorBanner;
}

export default function ErrorBanner(props: IErrorBannerProps): JSX.Element {
    const fields = props.entry.fields;

    if (!fields) return null;

    return <ErrorBannerContent fields={props.entry.fields} enableGoBack></ErrorBannerContent>;
}
