import React, { useState } from 'react';

import classNames from 'classnames';
import styles from './withAccordion.module.css';

import { Collapse } from '@material-ui/core';
import Icon from '../../../atoms/BrandIcon';
import { IFrequentlyAskedQuestionsLineItemsFields } from '../../../../@generated/@types/contentful';
import useRichTextOptions from '../../../../common/hooks/useRichTextOptions';
import isSmallScreen from '../../../../lib/isSmallScreen';
import isMobileScreen from '../../../../lib/isMobileScreen';

import { documentToHtmlString } from '@contentful/rich-text-html-renderer';

export default function CollapseItem(props: IFrequentlyAskedQuestionsLineItemsFields): JSX.Element | null {
    const [collapsed, setCollapsed] = useState(false);
    const smallScreen = isSmallScreen();
    const mobileScreen = isMobileScreen();

    const richTextOptions = useRichTextOptions();

    const { title, items } = props;

    return items ? (
        <div
            className={classNames(styles.item, {
                [styles.line]: collapsed,
            })}
        >
            <div
                className={classNames(styles.itemSection, {
                    [styles.collapsedItemSection]: collapsed,
                })}
                key={title}
            >
                <span
                    className={classNames('t-subheader-universal', styles.heading, {
                        [styles.collapsedTitle]: collapsed,
                        't-subheader-universal-small': smallScreen || mobileScreen,
                    })}
                >
                    {title}
                </span>
                <Icon
                    className={styles.icon}
                    icon={collapsed ? 'direction-up' : 'direction-down'}
                    onClick={() => setCollapsed(!collapsed)}
                />
            </div>
            <Collapse in={collapsed}>
                <div className={styles.subItemsContainer}>
                    {items.map((item) => (
                        <div className={styles.subItem} key={item.sys.id}>
                            <span className={'t-paragraph-strong'}>{item.fields.question}</span>
                            <span
                                className={classNames('t-paragraph', styles.answer)}
                                dangerouslySetInnerHTML={{
                                    __html: documentToHtmlString(item.fields.answer, richTextOptions),
                                }}
                            />
                        </div>
                    ))}
                </div>
            </Collapse>
        </div>
    ) : null;
}
