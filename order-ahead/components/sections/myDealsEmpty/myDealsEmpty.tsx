import React, { FC } from 'react';
import { IMyDealsEmptyFields } from '../../../@generated/@types/contentful';
import ContentfulImage from '../../atoms/ContentfulImage';
import { InspireButton } from '../../atoms/button';
import styles from './myDealsEmpty.module.css';
import classNames from 'classnames';
interface IMyDealsEmptyProps {
    fields: IMyDealsEmptyFields;
}

const MyDealsEmpty: FC<IMyDealsEmptyProps> = ({ fields }) => {
    const { title, topImage, mainText, ctaText } = fields;

    return (
        <div className={styles.container} data-testid="my-deals-empty">
            <ContentfulImage className={styles.topImage} asset={topImage} maxWidth={200} />
            <h2 className={classNames(styles.title, 't-header-h3')}>{title}</h2>
            <p className={styles.mainText}>{mainText}</p>
            <InspireButton link="/menu" className={styles.cta} text={ctaText} />
        </div>
    );
};

export default MyDealsEmpty;
