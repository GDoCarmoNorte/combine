import React from 'react';
import classnames from 'classnames';
import { useOrderHistory } from '../../../redux/hooks';
import RecentOrdersHeader from './recentOrdersHeader';
import RecentOrdersCarousel from './recentOrdersCarousel';
import { IRecentOrdersSection } from '../../../@generated/@types/contentful';

import styles from './recentOrders.module.css';

interface IRecentOrdersProps {
    entry: IRecentOrdersSection;
}

const RecentOrders = (props: IRecentOrdersProps): JSX.Element => {
    const { orderHistoryFulfilled } = useOrderHistory();
    const backgroundColor = props.entry.fields.backgroundColor?.fields.hexColor;

    if (!orderHistoryFulfilled.length) return null;

    return (
        <>
            <section className={classnames(styles.container, 'backgroundColor')}>
                <RecentOrdersHeader
                    image={props.entry.fields.image}
                    mainText={props.entry.fields.mainText}
                    secondaryText={props.entry.fields.secondaryText}
                />
                <RecentOrdersCarousel orderHistory={orderHistoryFulfilled.slice(0, 3)} />
            </section>
            <style jsx>{`
                .backgroundColor {
                    background-color: ${backgroundColor ? `#${backgroundColor}` : 'inherit'};
                }
            `}</style>
        </>
    );
};

export default RecentOrders;
