import React, { FC } from 'react';
import styles from './recentOrderItem.module.css';
import { InspireButton } from '../../atoms/button';
import classnames from 'classnames';
import { IOrderModel } from '../../../@generated/webExpApi';
import { format } from '../../../common/helpers/dateTime';
import { useOrderLocation } from '../../../redux/hooks';
import useAddToBagFromOrderHistory from '../../../common/hooks/useAddToBagFromOrderHistory';
import { useOrderItem } from '../../../common/hooks/useOrderItem';

interface IRecentOrderItemProps {
    order: IOrderModel;
}

const fulfillmentTypes = { PickUp: 'Pickup', Delivery: 'Delivery' };

export const RecentOrderItem: FC<IRecentOrderItemProps> = ({ order }) => {
    const { products } = useOrderItem(order);
    const isPlural = order.products.length > 1;
    const countLabel = `${order.products.length} ${isPlural ? 'items' : 'item'}`;
    const orderLabel = products
        .map((product) => {
            const sizeLabel = product.sizeLabel ? `(${product.sizeLabel})` : '';
            return `${product.quantity}x ${product.domainProduct?.name} ${sizeLabel}`;
        })
        .join(', ');

    const { currentLocation, isCurrentLocationOAAvailable } = useOrderLocation();
    const { addFromOrderHistory } = useAddToBagFromOrderHistory(order.products);

    const ctaProps =
        currentLocation && isCurrentLocationOAAvailable
            ? { text: 'Add to bag', onClick: addFromOrderHistory }
            : { text: 'View details', link: '/account/orders' };

    return (
        <div className={styles.container}>
            <div className={styles.infoBlock}>
                <time className={classnames('t-tag', styles.date)}>{format(order.dateTime, 'MMM. dd, yyyy')}</time>
                <span className={classnames('t-paragraph-hint', styles.meta)}>
                    {fulfillmentTypes[order.fulfillment.type]} | {countLabel}
                </span>
            </div>
            <div className={classnames('t-paragraph-small', 'truncate', styles.orderBlock)}>{orderLabel}</div>
            <div className={styles.ctaBlock}>
                <InspireButton type="primary" size="small" {...ctaProps} />
            </div>
        </div>
    );
};
