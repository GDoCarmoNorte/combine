import React, { FC, useState } from 'react';
import classNames from 'classnames';

import { InspireButton } from '../../atoms/button';
import { ISingleImageWithOverlay } from '../../../@generated/@types/contentful';

import sectionIndentsStyles from '../sectionIndents.module.css';
import styles from './singleImageWithOverlay.module.css';
import ContentfulImage from '../../atoms/ContentfulImage';
import InspirePhoneLink from '../../atoms/phoneLink';
import TextWithTrademark from '../../atoms/textWithTrademark';

import { SHOULD_SHOW_TWO_BUTTONS, SHOULD_SHOW_INFO_BLOCK, TYPOGRAPHY_CLASS } from './constants';
import getBrandInfo from '../../../lib/brandInfo';
import { BlazingRewardsForm } from '../../organisms/blazingRewards/blazingRewardsForm';
import { BlazingRewardsButton } from '../../organisms/blazingRewards/blazingRewardsButton';

interface ISingleImageWithOverlayProps {
    entry: ISingleImageWithOverlay;
}

const SingleImageWithOverlay: FC<ISingleImageWithOverlayProps> = ({ entry: { fields } }) => {
    const [isBlazingRewardsFormOpen, setIsBlazingRewardsFormOpen] = useState(false);

    if (!fields) return null;

    const {
        header,
        contentHeader,
        contentDescription,
        textBlock1,
        textBlock1Value,
        textBlock2,
        textBlock2Value,
        buttonCta,
        secondaryButtonCta,
        phone,
        image,
        imageLeftAligned,
        showBlazingRewardsButton,
    } = fields;

    const sectionClasses = classNames(sectionIndentsStyles.wrapper, styles.section, {
        [styles.imageLeftAligned]: !!imageLeftAligned,
    });

    const shouldShowTextBlock1 = textBlock1 || textBlock1Value;
    const shouldShowTextBlock2 = textBlock2 || textBlock2Value;
    const isShowInfoBlock = SHOULD_SHOW_INFO_BLOCK && (shouldShowTextBlock1 || shouldShowTextBlock2);

    const isShowBlazingRewardsButton = getBrandInfo().brandId === 'Bww' && showBlazingRewardsButton;
    const isShowSecondaryButton =
        !isShowBlazingRewardsButton && secondaryButtonCta && (SHOULD_SHOW_TWO_BUTTONS || !buttonCta);

    return (
        <section className={sectionClasses}>
            {header && (
                <TextWithTrademark
                    text={header}
                    tag="h2"
                    className={classNames(TYPOGRAPHY_CLASS.HEADER, styles.header)}
                />
            )}
            <div className={styles.content}>
                <div className={styles.textSection}>
                    <TextWithTrademark
                        text={contentHeader}
                        tag={header ? 'h3' : 'h2'}
                        className={TYPOGRAPHY_CLASS.CONTENT_HEADER}
                    />
                    <TextWithTrademark
                        tag="p"
                        className={classNames(TYPOGRAPHY_CLASS.CONTENT_DESCRIPTION, styles.contentDescription)}
                        text={contentDescription}
                    />
                    {isShowInfoBlock && (
                        <div className={classNames(TYPOGRAPHY_CLASS.CONTENT_DESCRIPTION, styles.infoBlock)}>
                            {shouldShowTextBlock1 && (
                                <div>
                                    {textBlock1 && (
                                        <TextWithTrademark
                                            tag="span"
                                            text={textBlock1}
                                            className={classNames('t-subheader-small', styles.textBlock)}
                                        />
                                    )}
                                    <TextWithTrademark tag="span" text={textBlock1Value} />
                                </div>
                            )}
                            {shouldShowTextBlock2 && (
                                <div>
                                    {textBlock2 && (
                                        <TextWithTrademark
                                            tag="span"
                                            text={textBlock2}
                                            className={classNames('t-subheader-small', styles.textBlock)}
                                        />
                                    )}
                                    <TextWithTrademark tag="span" text={textBlock2Value} />
                                </div>
                            )}
                        </div>
                    )}
                    <div className={styles.bottomBlock}>
                        {buttonCta && <InspireButton className={styles.buttonCta} link={buttonCta} />}
                        {isShowSecondaryButton && (
                            <InspireButton className={styles.secondaryButtonCta} link={secondaryButtonCta} />
                        )}
                        {isShowBlazingRewardsButton && (
                            <BlazingRewardsButton
                                className={styles.secondaryButtonCta}
                                onClick={() => setIsBlazingRewardsFormOpen(true)}
                            />
                        )}
                        {phone && (
                            <div className={styles.phoneBlock}>
                                <InspirePhoneLink className={TYPOGRAPHY_CLASS.PHONE_NUMBER} phone={phone} />
                            </div>
                        )}
                    </div>
                </div>
                <div className={styles.imageSection}>
                    <ContentfulImage asset={image} maxWidth={660} />
                </div>
            </div>
            {isShowBlazingRewardsButton && (
                <BlazingRewardsForm
                    isOpen={isBlazingRewardsFormOpen}
                    onClose={() => {
                        setIsBlazingRewardsFormOpen(false);
                    }}
                />
            )}
        </section>
    );
};

export default SingleImageWithOverlay;
