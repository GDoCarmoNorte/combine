export const TITLE = 'Items unavailable in your location';
export const DESCRIPTION = 'Some items aren’t available at this new location.';
export const FOOTER_TEXT = 'Would you like to proceed to new location?';
