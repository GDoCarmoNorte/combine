import React, { FC } from 'react';
import classnames from 'classnames';
import { PageContentWrapper } from '../../sections/PageContentWrapper';
import RewardsCatalog from '../rewardsCatalog';
import { IFrequentlyAskedQuestions } from '../../../@generated/@types/contentful';
import Faq from '../../sections/faq';
import RecommendedCertificatesSection from '../recommendedCertificatesSection';
import styles from './rewardsRoster.module.css';
import { formatNumber } from '../../../lib/domainProduct';

interface RewardsRosterProps {
    faq: IFrequentlyAskedQuestions;
    isLoading: boolean;
    isError: boolean;
    points: number;
}

const RewardsRoster: FC<RewardsRosterProps> = ({ faq, isLoading, isError, points }) => {
    const getPointsText = () => {
        if (isLoading) {
            return `Checking points balance…`;
        }

        if (isError) {
            return `Points balance not available`;
        }

        return `${formatNumber(points)} pts available`;
    };

    return (
        <PageContentWrapper className={styles.wrapper} ariaLabel="Rewards Roster Page">
            <div className={styles.header}>
                <h1 className={classnames('t-header-h1', styles.headerText)}>rewards roster</h1>
                <span className={classnames('t-paragraph-small', styles.subHeaderText)}>{getPointsText()}</span>
            </div>
            <RecommendedCertificatesSection />
            <RewardsCatalog />
            <Faq entry={faq} />
        </PageContentWrapper>
    );
};

export default RewardsRoster;
