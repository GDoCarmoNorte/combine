import React, { useState } from 'react';
import HScroller from '../../atoms/HScroller';
import useRewards from '../../../redux/hooks/useRewards';
import RecommendedCertificate from '../../atoms/recommendedRewardsCertificate';
import CertificatesSectionLayout from '../withCertificatesSectionLayout';
import BrandLoader from '../../atoms/BrandLoader';
import styles from './recommendedCertificatesSection.module.css';

const layoutProps = {
    bgImageUrl: '/brands/bww/recommended-just-for-you.png',
    headingFirstLineText: 'recommended',
    headingSecondLineText: 'just',
    headingThirdLineText: 'for you',
};

const RecommendedCertificatesSection = (): JSX.Element => {
    const { rewardsRecommendations, isLoading } = useRewards();
    const [isPurchaseLoading, setIsPurchaseLoading] = useState(false);

    const isRewardsRecommendationsExist = rewardsRecommendations.length;

    if (isLoading) {
        return (
            <div className={styles.loader}>
                <BrandLoader />
            </div>
        );
    }

    if (!isRewardsRecommendationsExist) {
        return null;
    }

    return (
        <div className={styles.recommendedCertificatesSectionContainer}>
            {isPurchaseLoading && <BrandLoader className={styles.purchaseLoader} />}
            <CertificatesSectionLayout {...layoutProps}>
                <HScroller
                    scrollStep={340}
                    itemType="deal"
                    listClassName={styles.scrollerList}
                    className={styles.scroller}
                >
                    {rewardsRecommendations.map(({ title, id, imageUrl, points, endDateTime, code }) => {
                        return (
                            <RecommendedCertificate
                                key={id}
                                isSideScroll
                                title={title}
                                code={code}
                                points={points}
                                imageUrl={imageUrl}
                                endDateTime={endDateTime}
                                setPurchaseLoadingState={setIsPurchaseLoading}
                            />
                        );
                    })}
                </HScroller>
            </CertificatesSectionLayout>
        </div>
    );
};

export default RecommendedCertificatesSection;
