import React from 'react';
import classNames from 'classnames';

import styles from './signInBanner.module.css';
import getBrandInfo from '../../../../lib/brandInfo';
import { InspireLink } from '../../../atoms/link';

const { brandId } = getBrandInfo();
const brandIdLowercase = brandId.toLowerCase();

const SignInBanner = (): JSX.Element => {
    const signUpLink = '/create-account?redirect=account/rewards';

    return (
        <div className={styles.banner}>
            <img className={styles.logo} src={`/brands/${brandIdLowercase}/star.svg`} alt="icon" />
            <div className={styles.messageContainer}>
                <div className={classNames('t-subheader-small', styles.message)}>
                    sign up and earn POINTS on future orders
                </div>
                <InspireLink link={signUpLink} className={classNames('t-subheader-smaller', styles.link)}>
                    Sign up now
                </InspireLink>
            </div>
        </div>
    );
};

export default SignInBanner;
