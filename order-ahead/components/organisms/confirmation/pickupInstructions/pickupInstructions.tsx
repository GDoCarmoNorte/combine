import React from 'react';

import styles from './pickupInstructions.module.css';
import { IPickupInstructionsItem } from '../../../../@generated/@types/contentful';

interface PickupInstructionsProps {
    pickupInstructions: IPickupInstructionsItem[] | undefined;
}

function PickupInstructions(props: PickupInstructionsProps): JSX.Element {
    const items = props.pickupInstructions || [];
    return (
        <div className={styles.container}>
            {items.map((item) => (
                <div key={item?.fields.instruction} className={styles.itemWrapper}>
                    <p className={styles.text}>{item?.fields.instruction}</p>
                </div>
            ))}
        </div>
    );
}

export default PickupInstructions;
