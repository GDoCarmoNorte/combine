import React, { FC } from 'react';
import classNames from 'classnames';
import Link from 'next/link';
import { Entry } from 'contentful';

import {
    ICheckoutLegalFields,
    IColor,
    IPickupInstructionsItem,
    ISecondaryBanner,
    ISecondaryBannerFields,
} from '../../../@generated/@types/contentful';
import LocationInfo from '../../../components/clientOnly/locationInfo';
import OrderInfo from '../../../components/clientOnly/orderInfo';
import ReviewOrder from '../../../components/organisms/checkout/reviewOrder';
import PickupInstructions from '../../../components/organisms/confirmation/pickupInstructions/pickupInstructions';
import SecondaryBanner from '../../../components/sections/secondaryBanner';
import { ORDER_INFO_BACKGROUND_COLOR, SHOW_SIGN_UP_SECTION } from './constants';

import { OrderLocationMethod } from '../../../redux/orderLocation';
import { IProductItemById } from '../../../common/services/globalContentfulProps';

import styles from './confirmation.module.css';
import { TallyResponseModel } from '../../../@generated/webExpApi';
import SignInBanner from './signInBanner/signInBanner';
import { useAuth0 } from '@auth0/auth0-react';
import { isSingUpBannerOnConfirmationEnabled } from '../../../lib/getFeatureFlags';

interface IConfirmationProps {
    method: OrderLocationMethod;
    pickupInstructions: IPickupInstructionsItem[];
    deliveryInfo: {
        deliveryOption: string;
        specialIstructions: string;
    };
    tallyOrder: TallyResponseModel;
    productsById: IProductItemById;
    backgroundColor: IColor;
    secondaryBanner: Entry<ISecondaryBannerFields>;
    isPickUp: boolean;
    isAuthenticated: boolean;
    tipsAmount?: number;
    checkoutLegal?: ICheckoutLegalFields;
}

const Confirmation: FC<IConfirmationProps> = (props) => {
    const {
        method,
        pickupInstructions,
        deliveryInfo,
        tallyOrder,
        productsById,
        backgroundColor,
        secondaryBanner,
        isPickUp,
        tipsAmount,
        checkoutLegal,
    } = props;

    const { isAuthenticated } = useAuth0();

    return (
        <div className={styles.container}>
            <div className={styles.orderHeader}>
                <OrderInfo method={method} backgroundColor={ORDER_INFO_BACKGROUND_COLOR} />
            </div>
            <div className={styles.orderForm}>
                <LocationInfo viewOnly isPickUp={isPickUp} />
                <div className={styles.formFields}>
                    {isPickUp && pickupInstructions && (
                        <section className={styles.pickupInstructionsSection}>
                            <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Pickup Instructions</h4>
                            <PickupInstructions pickupInstructions={pickupInstructions} />
                        </section>
                    )}
                    {!isPickUp && deliveryInfo && (
                        <section className={styles.deliveryInfoSection}>
                            <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Delivery Info</h4>

                            <p className="t-paragraph-small">
                                <span className="t-paragraph-small-strong">Delivery option: </span>
                                {deliveryInfo?.deliveryOption}
                            </p>
                            <p className="t-paragraph-small">
                                <span className="t-paragraph-small-strong">Special instructions: </span>
                                {deliveryInfo?.specialIstructions}
                            </p>
                        </section>
                    )}
                    {!isAuthenticated && isSingUpBannerOnConfirmationEnabled() && <SignInBanner />}
                    <section className={styles.reviewOrderSection}>
                        <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Order Summary</h4>
                        <ReviewOrder
                            tipsAmount={tipsAmount}
                            withTips={false}
                            order={tallyOrder}
                            productsById={productsById}
                            checkoutLegal={checkoutLegal}
                        />
                    </section>
                    {SHOW_SIGN_UP_SECTION && (
                        <section className={styles.actionSection}>
                            <Link href="/deals">
                                <a className={styles.link} target="_blank" rel="noreferrer">
                                    Sign-up for the Arby’s email program
                                </a>
                            </Link>
                        </section>
                    )}
                </div>
            </div>
            <div className={classNames(styles.bannerContainer, 'bannerContainerBackground')}>
                {secondaryBanner && <SecondaryBanner entry={secondaryBanner as ISecondaryBanner} />}
            </div>
            <style jsx>{`
                .bannerContainerBackground {
                    background-color: ${backgroundColor ? backgroundColor : 'var(--col--light)'};
                }
            `}</style>
        </div>
    );
};

export default Confirmation;
