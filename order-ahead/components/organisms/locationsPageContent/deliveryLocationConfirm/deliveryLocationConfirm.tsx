import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import { Formik, Form } from 'formik';

import FormikInput from '../../formikInput';
import { InspireButton } from '../../../atoms/button';
import StatesDropdown from './statesDropdown';

import { IDeliveryLocationAddressModel } from '../../../../@generated/webExpApi/models';
import { IDeliveryLocation } from '../../../../common/services/locationService/types';

import styles from './deliveryLocationConfirm.module.css';

interface IDeliveryLocationConfirmProps {
    location: IDeliveryLocationAddressModel;
    onFormSubmit: (IDeliveryLocation) => Promise<void>;
}

const ADDRESS_TEXT_MAX_LENGTH = 50;
const CITY_TEXT_MAX_LENGTH = 25;
const POSTAL_CODE_MAX_LENGTH = 10;

const DeliveryLocationConfirm = (props: IDeliveryLocationConfirmProps): JSX.Element => {
    const { location, onFormSubmit } = props;
    const [formData, setFormData] = useState<IDeliveryLocation | null>(null);

    useEffect(() => {
        setFormData({
            addressLine1: location.line1,
            addressLine2: '',
            businessName: '',
            city: location.city,
            state: location.stateProvinceCode,
            zipCode: location.postalCode,
        });
    }, [location]);

    const formSubmit = async (formData) => {
        await onFormSubmit(formData);
    };

    return (
        <div className={styles.confirmWrapper}>
            <h2 className={classnames('t-header-h3', styles.title)}>Confirm address</h2>
            {formData && (
                <Formik initialValues={formData} onSubmit={formSubmit}>
                    {({ isSubmitting }) => (
                        <Form>
                            <FormikInput
                                required
                                disabled
                                name="addressLine1"
                                label="Address Line 1"
                                labelClassName={classnames(styles.label, styles.required)}
                                type="text"
                                maxLength={ADDRESS_TEXT_MAX_LENGTH}
                                placeholder="Address Line 1"
                            />
                            <FormikInput
                                name="addressLine2"
                                label="Address Line 2"
                                labelClassName={styles.label}
                                type="text"
                                maxLength={ADDRESS_TEXT_MAX_LENGTH}
                                placeholder="Address Line 2"
                            />
                            <FormikInput
                                name="businessName"
                                label="Business Name"
                                labelClassName={styles.label}
                                type="text"
                                maxLength={ADDRESS_TEXT_MAX_LENGTH}
                                placeholder="Business Name"
                            />
                            <FormikInput
                                required
                                disabled
                                name="city"
                                label="City"
                                labelClassName={classnames(styles.label, styles.required)}
                                type="text"
                                maxLength={CITY_TEXT_MAX_LENGTH}
                                placeholder="City"
                            />
                            <div className={styles.stateAndZipContainer}>
                                <div className={styles.stateDropdown}>
                                    <StatesDropdown />
                                </div>
                                <FormikInput
                                    required
                                    disabled
                                    name="zipCode"
                                    label="Zip Code"
                                    labelClassName={classnames(styles.label, styles.required)}
                                    type="text"
                                    maxLength={POSTAL_CODE_MAX_LENGTH}
                                    placeholder="Zip Code"
                                    className={styles.zipCodeInput}
                                />
                            </div>
                            <InspireButton
                                className={styles.submitButton}
                                type="primary"
                                text="Confirm Address"
                                fullWidth
                                submit
                                disabled={isSubmitting}
                                gtmId={'CTA-Delivery-Confirm_Address'}
                            />
                        </Form>
                    )}
                </Formik>
            )}
        </div>
    );
};

export default DeliveryLocationConfirm;
