import React from 'react';
import classnames from 'classnames';
import { useFormikContext } from 'formik';
import MenuItem from '@material-ui/core/MenuItem';
import Dropdown from '../../../../atoms/dropdown';

import { IDeliveryLocation } from '../../../../../common/services/locationService/types';
import { stateCodes } from './constants';

import styles from './statesDropdown.module.css';

const StatesDropdown = (): JSX.Element => {
    const { values, setFieldValue } = useFormikContext<IDeliveryLocation>();
    const { state: selectedState } = values;
    const target = selectedState && stateCodes.indexOf(selectedState) >= 0 ? selectedState : 'Select State';

    return (
        <Dropdown
            labelClassName={classnames(styles.label, styles.required)}
            listClassName={styles.dropdown}
            buttonClassName={styles.input}
            target={target}
            label="State"
            popperPlacement="top"
            disabled
        >
            {stateCodes.map((state) => {
                const isSelected = selectedState === state;
                return (
                    <MenuItem
                        key={state}
                        selected={isSelected}
                        onClick={() => setFieldValue('state', state)}
                        disableRipple
                    >
                        <span className="truncate">{state}</span>
                    </MenuItem>
                );
            })}
        </Dropdown>
    );
};

export default StatesDropdown;
