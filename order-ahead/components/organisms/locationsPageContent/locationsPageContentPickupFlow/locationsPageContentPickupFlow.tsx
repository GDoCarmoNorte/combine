import React, { FC } from 'react';

import Map from '../../../clientOnly/map';
import PickupSearchResults from '../pickupSearchResults';
import { ILocationNotFoundFields, ILocationFailedFields } from '../../../../@generated/@types/contentful';
import { Entry } from 'contentful';

import styles from './locationsPageContentPickupFlow.module.css';
import AllLocationsLink from '../../../atoms/allLocationsLink';

import LocationsPageLayout from '../locationsPageLayout';
import Input from '../../../atoms/Input';
import PickupSearchInputIcon from '../pickupSearchInputIcon';
import usePickupFlow from '../../../../common/hooks/usePickupFlow';
import { LoadingStatusEnum } from '../../../../common/types';
import { locationTitle } from './constants';
import { useConfiguration } from '../../../../redux/hooks';

interface LocationsPageContentPickupFlowProps {
    sections: {
        locationNotFound: Entry<ILocationNotFoundFields>;
        locationFailed: Entry<ILocationFailedFields>;
    };
}

const LocationsPageContentPickupFlow: FC<LocationsPageContentPickupFlowProps> = (props) => {
    const {
        sections: { locationNotFound, locationFailed },
    } = props;

    const {
        selectedLocation,
        currentLocation,
        locationsList,
        isAbleToFetchMoreLocations,
        locationsSearchStatus,
        moreLocationsFetchStatus,
        locationQuery,
        onMapLocationSelect,
        onViewMoreLocationsClick,
        onSearchClick,
        onSearchInputChange,
        onSearchInputKeyPress,
        onLocationSelect,
        onLocationSet,
    } = usePickupFlow();

    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    return (
        <LocationsPageLayout
            renderMap={() => (
                <Map locations={locationsList} onLocationSelect={onMapLocationSelect} place={selectedLocation} />
            )}
            renderSearchPanel={() => (
                <div>
                    <div className={styles.container}>
                        <div className={styles.topSection}>
                            <div className={`${styles.title} t-subheader`}>{locationTitle}</div>
                            <AllLocationsLink />
                        </div>
                        <Input
                            searchIconAriaLabel="Search Location"
                            searchInputAriaLabel="Search Location Input"
                            defaultValue={locationQuery}
                            onKeyPress={onSearchInputKeyPress}
                            onChange={onSearchInputChange}
                            onClick={onSearchClick}
                            type="search"
                            placeholder="Search by Address, City and State or Zip Code"
                            leftIcon={<PickupSearchInputIcon />}
                        />
                    </div>
                    <PickupSearchResults
                        selectedLocation={selectedLocation}
                        onLocationSet={onLocationSet}
                        locationsList={locationsList}
                        onLocationSelect={onLocationSelect}
                        currentLocation={currentLocation}
                        locationsSearchStatus={locationsSearchStatus}
                        locationFailedMessage={locationFailed}
                        locationNotFoundMessage={locationNotFound}
                        onViewMoreButtonClick={onViewMoreLocationsClick}
                        moreLocationsLoading={moreLocationsFetchStatus === LoadingStatusEnum.Loading}
                        showViewMoreButton={isAbleToFetchMoreLocations}
                        isOAEnabled={isOAEnabled}
                    />
                    <div className={styles.allLocationsLink}>
                        <AllLocationsLink />
                    </div>
                </div>
            )}
        />
    );
};

export default LocationsPageContentPickupFlow;
