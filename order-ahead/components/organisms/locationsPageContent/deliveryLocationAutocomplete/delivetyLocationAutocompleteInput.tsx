import React from 'react';
import classnames from 'classnames';

import SearchIcon from '@material-ui/icons/Search';
import { IconButton, Input } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import Icon from '../../../atoms/BrandIcon';

import { AutocompleteRenderInputParams } from '@material-ui/lab/Autocomplete';

import styles from '../../../atoms/Input/input.module.css';

interface IDeliveryLocationAutocompleteInputProps {
    params: AutocompleteRenderInputParams;
    disabled?: boolean;
    onClick: () => void;
    searchAddressLength?: number;
}

const DeliveryLocationAutocompleteInput = (props: IDeliveryLocationAutocompleteInputProps): JSX.Element => {
    const { params, disabled, onClick, searchAddressLength } = props;

    return (
        <div className={styles.searchInputContainer}>
            <div className={styles.searchInputRightBlock}>
                <Icon size="s" icon="nav-location" />
                <Input
                    disableUnderline
                    ref={params.InputProps.ref}
                    classes={{
                        root: classnames(styles.input, styles.searchInput, styles.withIcon),
                    }}
                    inputProps={{
                        ...params.inputProps,
                        'aria-label': 'Delivery Address Input',
                    }}
                    disabled={disabled}
                    placeholder="Enter Delivery Address"
                />
            </div>
            <IconButton aria-label={`${searchAddressLength ? 'Clear' : 'Enter'} delivery address`} onClick={onClick}>
                {searchAddressLength ? (
                    <ClearIcon classes={{ root: styles.searchIcon }} />
                ) : (
                    <SearchIcon classes={{ root: styles.searchIcon }} />
                )}
            </IconButton>
        </div>
    );
};

export default DeliveryLocationAutocompleteInput;
