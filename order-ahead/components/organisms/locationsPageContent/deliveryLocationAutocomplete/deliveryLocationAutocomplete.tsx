import React, { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import { injectScriptOnce } from '../../../../lib/injectScriptOnce';

import Autocomplete from '@material-ui/lab/Autocomplete';
import BrandLoader from '../../../atoms/BrandLoader';
import DeliveryLocationAutocompleteInput from './delivetyLocationAutocompleteInput';

import styles from './deliveryLocationAutocomplete.module.css';
import { useConfiguration } from '../../../../redux/hooks';

interface ILocationAutocompleteProps {
    onLocationSelect: (value: google.maps.places.AutocompletePrediction) => void;
}

const DeliveryLocationAutocomplete = ({ onLocationSelect }: ILocationAutocompleteProps): JSX.Element => {
    const { NEXT_PUBLIC_GOOGLE_MAP_API_KEY } = process.env;

    const [initializing, setInitializingStatus] = useState<boolean>(true);
    const [loading, setLoading] = useState<boolean>(false);
    const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);

    const [searchAddress, setSearchAddress] = useState('');
    const router = useRouter();
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();
    const defaultSearchAddress: string = (router.query.dq as string) || '';
    const tabChangeTrigger = router.query.c;

    useEffect(() => {
        setSearchAddress(defaultSearchAddress);
    }, [defaultSearchAddress, tabChangeTrigger]);

    const [searchAddressTrigger, setSearchAddressTrigger] = useState(0);
    const [addressPredictions, setAddressPredictions] = useState([]);
    const placesAPIRef = useRef(null);

    useEffect(() => {
        const googleMapScriptSrc = `https://maps.googleapis.com/maps/api/js?key=${NEXT_PUBLIC_GOOGLE_MAP_API_KEY}&libraries=places`;
        injectScriptOnce(googleMapScriptSrc).then(() => {
            placesAPIRef.current = {
                autocompleteService: new google.maps.places.AutocompleteService(),
                sessionToken: new google.maps.places.AutocompleteSessionToken(),
            };
            setInitializingStatus(false);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleChange = (_, value) => {
        setSearchAddress(value);
    };

    const handleClick = () => {
        if (searchAddress?.length) {
            setAddressPredictions([]);
            setDropdownOpen(false);
            setSearchAddress('');
        } else {
            setSearchAddressTrigger(Date.now());
        }
    };

    useEffect(() => {
        if (initializing) return;

        if (!searchAddress.length) {
            setAddressPredictions([]);
            setDropdownOpen(false);
            return;
        }

        setDropdownOpen(true);
        setLoading(true);
        const { autocompleteService, sessionToken } = placesAPIRef.current;

        const autocompleteRequest = {
            input: searchAddress,
            sessionToken,
            types: ['address'],
            componentRestrictions: { country: 'us' },
        };

        autocompleteService.getPlacePredictions(autocompleteRequest, handleAutocomplete);
    }, [searchAddress, searchAddressTrigger, initializing]);
    const handleAutocomplete = (
        predictions: google.maps.places.AutocompletePrediction[] | null,
        status: google.maps.places.PlacesServiceStatus
    ) => {
        const filterPredictions = status === google.maps.places.PlacesServiceStatus.OK ? [...predictions] : [];
        setAddressPredictions(filterPredictions);
        setLoading(false);
    };

    const handleLocationSelect = (e, value, reason) => {
        // for our Autocomplete component only other possible reason is 'clear'
        if (reason !== 'select-option') return;
        onLocationSelect(value);
    };

    const getOptionLabel = (prediction) =>
        prediction.terms
            .slice(0, -1)
            .map(({ value }) => value)
            .join(', ');

    // filterOptions prop is fixing material-ui Autocomplete internal bug
    return (
        <Autocomplete
            id="deliveryAutocompleteInput"
            handleHomeEndKeys
            freeSolo
            autoHighlight
            loading={loading}
            disabled={initializing || !isOAEnabled}
            clearOnBlur={false}
            open={dropdownOpen}
            onChange={handleLocationSelect}
            onOpen={() => setDropdownOpen(true)}
            onClose={() => setDropdownOpen(false)}
            options={addressPredictions}
            getOptionLabel={getOptionLabel}
            filterOptions={(o) => o}
            classes={{
                listbox: styles.dropdownList,
                option: styles.dropdownItem,
            }}
            inputValue={searchAddress}
            onInputChange={handleChange}
            renderInput={(params) => (
                <DeliveryLocationAutocompleteInput
                    params={params}
                    disabled={initializing || !isOAEnabled}
                    onClick={handleClick}
                    searchAddressLength={searchAddress?.length}
                />
            )}
            PaperComponent={({ children }) => {
                if (loading) {
                    return (
                        <div className={styles.dropdownLoadingContainer}>
                            <BrandLoader />
                        </div>
                    );
                }
                if (!addressPredictions.length) {
                    return (
                        <div className={styles.dropdownContainer}>
                            <p className={styles.dropdownItem}>No suggested locations available</p>
                        </div>
                    );
                }
                return <div className={styles.dropdownContainer}>{children}</div>;
            }}
            PopperComponent={({ children }) => <div className={styles.dropdownWrapper}>{children}</div>}
        />
    );
};

export default DeliveryLocationAutocomplete;
