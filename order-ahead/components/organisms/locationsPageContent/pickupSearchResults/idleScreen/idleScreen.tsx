import React, { FC } from 'react';
import getBrandInfo from '../../../../../lib/brandInfo';
import AllLocationsLink from '../../../../atoms/allLocationsLink';
import NotFound from '../../../../molecules/notFound';

import styles from './idleScreen.module.css';

interface IIdleScreenProps {
    showAllLocationsLink?: boolean;
}

const IdleScreen: FC<IIdleScreenProps> = ({ showAllLocationsLink }) => {
    const { brandId } = getBrandInfo();
    const brandIdLowercase = brandId.toLowerCase();

    return (
        <div className={styles.idleScreen}>
            <NotFound
                heading="find a Location"
                body="Enter your location to find your local store."
                className={styles.notFound}
                icon={<img src={`/brands/${brandIdLowercase}/logo.svg`} className={styles.icon} alt="" />}
                headingTag="h2"
            />
            {showAllLocationsLink && <AllLocationsLink className={styles.allLocationsLink} />}
        </div>
    );
};

export default IdleScreen;
