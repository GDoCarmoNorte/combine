import React, { FC } from 'react';
import BrandLoader from '../../../../atoms/BrandLoader';
import styles from './loadingScreen.module.css';

const LoadingScreen: FC = () => {
    return (
        <div className={styles.loadingSection}>
            <BrandLoader />
        </div>
    );
};

export default LoadingScreen;
