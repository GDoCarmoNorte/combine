import { Asset } from 'contentful';
import React, { FC } from 'react';
import AllLocationsLink from '../../../../atoms/allLocationsLink';
import NotFound from '../../../../molecules/notFound';
import styles from './notFoundScreen.module.css';

interface INotFoundScreen {
    header: string;
    body: string;
    icon: Asset | JSX.Element;
    showAllLocationsLink?: boolean;
}

const NotFoundScreen: FC<INotFoundScreen> = ({ header, body, icon, showAllLocationsLink = false }) => {
    return (
        <div className={styles.notFoundWrapper}>
            <NotFound heading={header} body={body} icon={icon} className={styles.notFound} />
            {showAllLocationsLink && <AllLocationsLink className={styles.allLocationsLink} />}
        </div>
    );
};

export default NotFoundScreen;
