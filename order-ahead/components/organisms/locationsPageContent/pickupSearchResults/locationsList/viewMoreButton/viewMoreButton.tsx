import React, { FC } from 'react';
import Loader from '../../../../../atoms/Loader';
import Icon from '../../../../../atoms/BrandIcon';
import styles from './viewMoreButton.module.css';
import { InspireLinkButton } from '../../../../../atoms/link';

interface IViewMoreButtonProps {
    onClick: () => void;
    isLoading?: boolean;
}

const ViewMoreButton: FC<IViewMoreButtonProps> = ({ onClick, isLoading = false }) => {
    return (
        <InspireLinkButton className={styles.button} onClick={onClick} disabled={isLoading} linkType="secondary">
            <Icon icon="action-add" className={styles.addIcon} />
            <span className={styles.buttonText}>View more locations</span>
            {isLoading && <Loader className={styles.loader} size={20} />}
        </InspireLinkButton>
    );
};

export default ViewMoreButton;
