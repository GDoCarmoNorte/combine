import React, { FC } from 'react';
import TextWithDotSeparator from '../../../../../atoms/TextWithDotSeparator';
import { ILocationsListItemLayout } from './types';

import styles from './locationsListItemLayout.module.css';

const LocationsListItemLayout: FC<ILocationsListItemLayout> = ({
    title,
    primaryCta,
    secondaryCta,
    address,
    openingHours,
    distance,
    storeId,
    services,
}) => {
    return (
        <div className={styles.wrapper}>
            <div className={styles.leftColumn}>
                <div className={styles.title}>{title}</div>
                <div className={styles.address}>{address}</div>
                <div className={styles.distanceAndOpeningHours}>
                    <TextWithDotSeparator leftText={distance} rightText={storeId} />
                    <div>{openingHours}</div>
                </div>
                <div>{services}</div>
                <div className={styles.secondaryCta}>{secondaryCta}</div>
            </div>
            <div>{primaryCta}</div>
        </div>
    );
};

export default LocationsListItemLayout;
