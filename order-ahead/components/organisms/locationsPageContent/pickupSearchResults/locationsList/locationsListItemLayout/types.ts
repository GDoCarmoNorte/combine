export interface ILocationsListItemLayout {
    title: JSX.Element;
    primaryCta: JSX.Element;
    secondaryCta: JSX.Element;
    address: JSX.Element;
    openingHours: JSX.Element;
    distance: JSX.Element;
    storeId: JSX.Element;
    services: JSX.Element;
}
