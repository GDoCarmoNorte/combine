import React, { useRef, useEffect, SyntheticEvent } from 'react';
import classnames from 'classnames';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { PickupAddress } from '../../../../../../redux/orderLocation';
import { InspireButton } from '../../../../../atoms/button';
import InspireChip from '../../../../../atoms/Chip';
import {
    getLocationAddressString,
    getOpeningHoursInfoString,
    isLocationOrderAheadAvailable,
    isLocationClosed,
} from '../../../../../../lib/locations';
import {
    GTM_START_PICKUP_ORDER,
    GTM_MAKE_MY_STORE,
    GTM_ONLINE_ORDER_COMING_SOON,
} from '../../../../../../common/services/gtmService/constants';

import styles from './locationsListItem.module.css';
import getLocationDetailsPageUrl from '../../../../../../common/helpers/getLocationDetailsPageUrl';
import { InspireLink, InspireLinkButton } from '../../../../../atoms/link';
import LocationsListItemLayout from '../locationsListItemLayout';
import { ADDRESS_LINE_CLASSNAME, SHOW_UNAVAILABLE, UNAVAILABLE_MESSAGE } from './constants';
import { IServicesInfo, TServiceTypeText } from './types';
import { useConfiguration } from '../../../../../../redux/hooks';

interface ILocationsListItemProps {
    location: PickupAddress;
    onLocationSet: (place: PickupAddress) => void;
    onLocationSelect: (place: PickupAddress) => void;
    selected: boolean;
    isCurrentLocation: boolean;
    listNumber?: number;
    hideStoreFeatures?: boolean;
    hideSelectStoreLink?: boolean;
}

const LocationsListItem = (props: ILocationsListItemProps): JSX.Element => {
    const {
        location,
        onLocationSet,
        onLocationSelect,
        selected,
        isCurrentLocation,
        listNumber,
        hideStoreFeatures,
        hideSelectStoreLink,
    } = props;
    const { displayName, id: locationId, distance } = location;
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();
    const address = getLocationAddressString(location);
    const isOnlineOrderAvailable = isLocationOrderAheadAvailable(location, isOAEnabled);
    const isClosed = isLocationClosed(location);

    const elementRef = useRef<HTMLDivElement>(null);
    const router = useRouter();
    const dispatch = useDispatch();

    useEffect(() => {
        if (selected) {
            const parentElement = elementRef?.current?.parentNode as HTMLElement | null;
            if (parentElement && elementRef?.current?.offsetTop >= 0) {
                parentElement.scrollTop = elementRef.current.offsetTop;
            }
        }
    }, [selected]);

    const locationOnlineOrderSet = (e: SyntheticEvent) => {
        dispatch({ type: GTM_ONLINE_ORDER_COMING_SOON, payload: { id: location.id, name: location.displayName } });
        locationSet(e);
    };

    const locationSet = (e: SyntheticEvent) => {
        e.stopPropagation();
        onLocationSet(location);
    };

    // TODO update GTM_MAKE_MY_STORE for delivery flow cover @ga-31325
    const handleMakeMyStoreClick = (e: SyntheticEvent) => {
        dispatch({ type: GTM_MAKE_MY_STORE, payload: { id: location.id, name: location.displayName } });
        locationSet(e);
    };

    const handleClickOrder = (e: SyntheticEvent) => {
        dispatch({ type: GTM_START_PICKUP_ORDER });
        dispatch({ type: GTM_MAKE_MY_STORE, payload: { id: location.id, name: location.displayName } });
        locationSet(e);
        router.push('/menu');
    };

    const handleClickViewMenu = (e: SyntheticEvent) => {
        locationSet(e);
        router.push('/menu');
    };

    const handleSelectLocation = () => {
        onLocationSelect(location);
    };

    const handleSelectLocationKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === 'Enter') {
            handleSelectLocation();
        }
    };

    const detailsPageUrl = getLocationDetailsPageUrl(location);

    const renderPrimaryCta = () => {
        if (isClosed || !isOnlineOrderAvailable) {
            return (
                <InspireButton
                    className={classnames(styles.viewMenuButton)}
                    onClick={handleClickViewMenu}
                    text="VIEW MENU"
                    type="secondary"
                    size="small"
                />
            );
        }

        return (
            <InspireButton
                className={classnames(styles.orderButton)}
                onClick={handleClickOrder}
                text="ORDER"
                type="small"
            />
        );
    };

    const renderSecondaryCta = () => {
        if (hideSelectStoreLink) {
            return null;
        }

        if (isCurrentLocation) {
            return <InspireChip label="MY STORE" />;
        }
        if (isOnlineOrderAvailable) {
            return (
                <InspireLinkButton linkType="secondary" onClick={handleMakeMyStoreClick}>
                    MAKE MY STORE
                </InspireLinkButton>
            );
        }
        return (
            <InspireLink link="/menu" onClick={locationOnlineOrderSet} type="secondary">
                ONLINE ORDERING COMING SOON
            </InspireLink>
        );
    };

    const renderTitle = () => {
        return (
            <h2
                className={classnames('truncate-at-3', 't-subheader-smaller', styles.title, {
                    [styles.withLink]: detailsPageUrl,
                })}
                title={displayName}
            >
                {listNumber && <span>{listNumber}. </span>}
                {detailsPageUrl ? (
                    <InspireLink
                        link={detailsPageUrl}
                        newtab={false}
                        onClick={(e) => {
                            e.stopPropagation();
                        }}
                        className={styles.detailsPageLink}
                    >
                        {displayName}
                    </InspireLink>
                ) : (
                    displayName
                )}
            </h2>
        );
    };

    const renderServices = () => {
        if (!hideStoreFeatures && Array.isArray(location.services) && location.services.length != 0) {
            const servicesInfo: IServicesInfo[] = location.services
                .filter((service) => service.type)
                .map((service) => ({
                    type: service.type,
                    text: TServiceTypeText[service.type],
                }));

            return (
                <div className={styles.servicesList}>
                    {servicesInfo.map((service) => {
                        return (
                            <div className={styles.serviceInfo} data-testid="serviceInfo" key={service.type}>
                                {service.text}
                            </div>
                        );
                    })}
                </div>
            );
        }
    };
    const openingHoursInfoString = getOpeningHoursInfoString(location);
    return (
        <div
            className={classnames(styles.container, { [styles.selected]: selected })}
            ref={elementRef}
            role="button"
            aria-label={`Select ${displayName} location`}
            onClick={handleSelectLocation}
            onKeyPress={handleSelectLocationKeyPress}
        >
            <LocationsListItemLayout
                title={renderTitle()}
                address={
                    <p title={address} className={classnames(ADDRESS_LINE_CLASSNAME, styles.address)}>
                        {address}
                    </p>
                }
                primaryCta={renderPrimaryCta()}
                secondaryCta={renderSecondaryCta()}
                openingHours={
                    location.status &&
                    (isClosed ? (
                        <span className={styles.openingHours}>Closed</span>
                    ) : (
                        <span className={styles.openingHours}>
                            {openingHoursInfoString || (SHOW_UNAVAILABLE ? UNAVAILABLE_MESSAGE : null)}
                        </span>
                    ))
                }
                distance={
                    <span className={styles.distance}>{`${Math.round(distance.amount || 0)} ${distance.unit}`}</span>
                }
                storeId={<span className={styles.storeId}>{`Store ID: ${locationId}`}</span>}
                services={renderServices()}
            />
        </div>
    );
};

export default LocationsListItem;
