export const ADDRESS_LINE_CLASSNAME = 'truncate t-paragraph-small';
export const UNAVAILABLE_MESSAGE = 'Restaurant hours currently unavailable';
export const SHOW_UNAVAILABLE = true;
