import React from 'react';
import styles from './locationsList.module.css';

import LocationsListItem from './locationsListItem';
import ViewMoreButton from './viewMoreButton';
import { PickupAddress } from '../../../../../redux/orderLocation';
import AllLocationsLink from '../../../../atoms/allLocationsLink';
import { isLocationsShowAsOrderedList } from '../../../../../lib/getFeatureFlags';

interface ILocationsListProps {
    locationsList: PickupAddress[];
    currentLocation?: PickupAddress;
    onLocationSet: (place: PickupAddress) => void;
    onLocationSelect: (place: PickupAddress) => void;
    selectedLocation?: PickupAddress;
    onViewMoreButtonClick: () => void;
    showViewMoreButton?: boolean;
    moreLocationsLoading?: boolean;
    showAllLocationsLink?: boolean;
}

const LocationsList = (props: ILocationsListProps): JSX.Element => {
    const {
        locationsList,
        currentLocation,
        onLocationSet,
        onLocationSelect,
        selectedLocation,
        onViewMoreButtonClick,
        showViewMoreButton = false,
        moreLocationsLoading = false,
        showAllLocationsLink = false,
    } = props;

    return (
        <div className={styles.container}>
            {locationsList.map((location, index) => {
                const isCurrentLocation = currentLocation?.id === location.id;
                return (
                    <LocationsListItem
                        key={location.id}
                        location={location}
                        onLocationSet={onLocationSet}
                        onLocationSelect={onLocationSelect}
                        selected={selectedLocation?.id === location.id}
                        isCurrentLocation={isCurrentLocation}
                        listNumber={isLocationsShowAsOrderedList() ? index + 1 : undefined}
                    />
                );
            })}
            {showViewMoreButton && <ViewMoreButton onClick={onViewMoreButtonClick} isLoading={moreLocationsLoading} />}
            {showAllLocationsLink && (
                <div className={styles.allLocationsLinkWrapper}>
                    <AllLocationsLink />
                </div>
            )}
        </div>
    );
};

export default LocationsList;
