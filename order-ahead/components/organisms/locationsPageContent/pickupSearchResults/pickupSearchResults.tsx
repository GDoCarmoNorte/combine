import React from 'react';
import { PickupAddress } from '../../../../redux/orderLocation';
import { ILocationNotFoundFields, ILocationFailedFields } from '../../../../@generated/@types/contentful';
import { Entry } from 'contentful';
import { LoadingStatusEnum } from '../../../../common/types';
import IdleScreen from './idleScreen';
import LoadingScreen from './loadingScreen';
import NotFoundScreen from './notFoundScreen';
import LocationsList from './locationsList';
import NotOrderAheadScreen from './notOrderAheadScreen';

interface ILocationsListProps {
    locationsList?: PickupAddress[];
    selectedLocation?: PickupAddress;
    onLocationSet: (place: PickupAddress) => void;
    onLocationSelect: (place: PickupAddress) => void;
    currentLocation: PickupAddress;
    locationsSearchStatus: LoadingStatusEnum;
    locationFailedMessage: Entry<ILocationFailedFields>;
    locationNotFoundMessage: Entry<ILocationNotFoundFields>;
    onViewMoreButtonClick: () => void;
    showViewMoreButton?: boolean;
    moreLocationsLoading?: boolean;
    showAllLocationsLink?: boolean;
    showIdleScreen?: boolean;
    isOAEnabled?: boolean;
}

const PickupSearchResults = (props: ILocationsListProps): JSX.Element => {
    const {
        locationsList,
        currentLocation,
        onLocationSet,
        onLocationSelect,
        selectedLocation,
        locationsSearchStatus,
        onViewMoreButtonClick,
        showViewMoreButton = false,
        moreLocationsLoading = false,
        locationFailedMessage,
        locationNotFoundMessage,
        showAllLocationsLink,
        showIdleScreen,
        isOAEnabled,
    } = props;

    if (!isOAEnabled) return <NotOrderAheadScreen />;

    switch (locationsSearchStatus) {
        case LoadingStatusEnum.Idle:
            if (showIdleScreen) {
                return <IdleScreen showAllLocationsLink={showAllLocationsLink} />;
            } else {
                return null;
            }
        case LoadingStatusEnum.Loading:
            return <LoadingScreen />;
        case LoadingStatusEnum.Error: {
            const {
                fields: { header, body, icon },
            } = locationFailedMessage;

            return (
                <NotFoundScreen header={header} body={body} icon={icon} showAllLocationsLink={showAllLocationsLink} />
            );
        }
    }

    if (!locationsList?.length) {
        const {
            fields: { header, body, icon },
        } = locationNotFoundMessage;

        return <NotFoundScreen header={header} body={body} icon={icon} showAllLocationsLink={showAllLocationsLink} />;
    }

    return (
        <LocationsList
            locationsList={locationsList}
            currentLocation={currentLocation}
            onLocationSet={onLocationSet}
            onLocationSelect={onLocationSelect}
            selectedLocation={selectedLocation}
            onViewMoreButtonClick={onViewMoreButtonClick}
            showViewMoreButton={showViewMoreButton}
            moreLocationsLoading={moreLocationsLoading}
            showAllLocationsLink={showAllLocationsLink}
        />
    );
};

export default PickupSearchResults;
