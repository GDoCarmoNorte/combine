import React from 'react';
import getBrandInfo from '../../../../../lib/brandInfo';
import NotFound from '../../../../molecules/notFound';
import styles from '../idleScreen/idleScreen.module.css';
import { ORDER_AHEAD_NOT_AVAIABLE_MESSAGE } from '../../../../../common/constants/orderAhead';

const NotOrderAheadScreen = () => {
    const { brandId } = getBrandInfo();
    const brandIdLowercase = brandId.toLowerCase();

    return (
        <div className={styles.idleScreen}>
            <NotFound
                body={ORDER_AHEAD_NOT_AVAIABLE_MESSAGE}
                className={styles.notFound}
                icon={<img src={`/brands/${brandIdLowercase}/warning.svg`} className={styles.icon} alt="" />}
                headingTag="h2"
            />
        </div>
    );
};

export default NotOrderAheadScreen;
