import React, { FC, ReactElement } from 'react';
import getBrandInfo from '../../../../lib/brandInfo';
import { PageContentWrapper } from '../../../sections/PageContentWrapper';
import styles from './locationsPageLayout.module.css';

interface LocationsPageLayoutProps {
    renderMap: () => ReactElement;
    renderSearchPanel: () => ReactElement;
}

const { brandName } = getBrandInfo();
const LocationsPageLayout: FC<LocationsPageLayoutProps> = ({ renderMap, renderSearchPanel }) => {
    return (
        <PageContentWrapper>
            <h1 className="visually-hidden">{`find an ${brandName}`}</h1>
            <div className={styles.locationsMapWrapper}>{renderMap()}</div>
            <div className={styles.locationsSearchWrapper}>{renderSearchPanel()}</div>
        </PageContentWrapper>
    );
};

export default LocationsPageLayout;
