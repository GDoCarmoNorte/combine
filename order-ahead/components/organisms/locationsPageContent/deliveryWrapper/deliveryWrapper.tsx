import React, { useState } from 'react';
import { useRouter } from 'next/router';

import { IDropOffModel, IPickupModel } from '../../../../@generated/webExpApi/models';

import DeliveryLocationSearch from '../deliveryLocationSearch';
import DeliveryLocationConfirm from '../deliveryLocationConfirm';
import DeliveryOutOfBounds from '../deliveryLocationSearch/deliveryOutOfBounds/deliveryOutOfBounds';
import { useDeliveryAddress } from '../../../../common/hooks/useDeliveryAddress';

import styles from './deliveryWrapper.module.css';

interface IDeliveryWrapperProps {
    setActiveTab: (number: number) => void;
    selectLocation: (location: IDropOffModel) => void;
}

const DeliveryWrapper = (props: IDeliveryWrapperProps): JSX.Element => {
    const { setActiveTab, selectLocation } = props;
    const [pickUpLocation, setPickUpLocation] = useState<IPickupModel>(null);
    const [dropOffLocation, setDropOffLocation] = useState<IDropOffModel>(null);
    const router = useRouter();
    const { setDeliveryAddress } = useDeliveryAddress();

    const onDeliveryLocationSelect = (dropOff, pickUp) => {
        selectLocation(dropOff);
        setPickUpLocation(pickUp);
        setDropOffLocation(dropOff);
    };

    const onDeliveryLocationConfirm = async (formData) => {
        await setDeliveryAddress({ pickUpLocation, deliveryLocation: formData });
        router.push('/menu');
    };

    const switchToPickupTab = () => setActiveTab(0);

    return (
        <div className={styles.wrapper}>
            {!dropOffLocation ? (
                <DeliveryLocationSearch
                    onDeliveryLocationSelect={onDeliveryLocationSelect}
                    outOfTheBoundsComponent={<DeliveryOutOfBounds switchToPickupTab={switchToPickupTab} />}
                />
            ) : (
                <DeliveryLocationConfirm onFormSubmit={onDeliveryLocationConfirm} location={dropOffLocation.address} />
            )}
        </div>
    );
};

export default DeliveryWrapper;
