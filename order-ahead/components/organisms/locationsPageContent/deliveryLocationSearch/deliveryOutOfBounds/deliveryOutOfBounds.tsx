import React from 'react';
import classnames from 'classnames';

import { InspireButton } from '../../../../atoms/button';
import { TextDivider } from '../../../../atoms/textDivider';
import BrandIcon from '../../../../atoms/BrandIcon';

import styles from './deliveryOutOfBounds.module.css';

interface IDeliveryOutOfBoundsProps {
    switchToPickupTab: () => void;
}

const DeliveryOutOfBounds = (props: IDeliveryOutOfBoundsProps): JSX.Element => {
    const { switchToPickupTab } = props;

    const outOfBoundsText = "It looks like your address isn't in our delivery area. Try entering a new address above.";

    return (
        <div className={styles.container}>
            <span className={styles.iconContainer}>
                <BrandIcon className={styles.carIcon} icon="order-delivery-small" />
                <BrandIcon className={styles.restrictIcon} icon="order-not" size="xl" variant="colorful" />
            </span>
            <div className={styles.content}>
                <h3 className={classnames('t-header-card-title', styles.title)}>Out of bounds</h3>
                <p className={classnames('t-paragraph', styles.text)}>{outOfBoundsText}</p>
            </div>
            <TextDivider text="or" />
            <InspireButton
                onClick={switchToPickupTab}
                className={styles.pickupInsteadButton}
                type="primary"
                text="Pickup instead"
                fullWidth
                gtmId={'CTA-Delivery-Pickup_Instead'}
            />
        </div>
    );
};

export default DeliveryOutOfBounds;
