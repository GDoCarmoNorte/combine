import React, { useState } from 'react';
import { getDeliveryLocationByPlaceId } from '../../../../common/services/deliveryLocation';

import DeliveryLocationAutocomplete from '../deliveryLocationAutocomplete';
import BrandLoader from '../../../atoms/BrandLoader';

import styles from './deliveryLocationSearch.module.css';

interface IDeliveryLocationSearchProps {
    onDeliveryLocationSelect: (IDropOffModel, IPickupModel) => void;
    outOfTheBoundsComponent: JSX.Element;
}

const DeliveryLocationSearch = (props: IDeliveryLocationSearchProps): JSX.Element => {
    const { onDeliveryLocationSelect, outOfTheBoundsComponent } = props;
    const [outOfBounds, setOutOfBounds] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);

    const onLocationSelect = (selectedLocation: google.maps.places.AutocompletePrediction) => {
        setLoading(true);
        getDeliveryLocationByPlaceId(selectedLocation.place_id).then(({ dropOff, pickUp, isInDeliveryZone }) => {
            setLoading(false);
            if (!isInDeliveryZone) return setOutOfBounds(true);

            setOutOfBounds(false);
            onDeliveryLocationSelect(dropOff, pickUp);
        });
    };

    return (
        <>
            <div className={styles.searchContainer} id="deliveryAutocompleteInput-label">
                <DeliveryLocationAutocomplete onLocationSelect={onLocationSelect} />
            </div>
            {loading && (
                <div className={styles.loadingContainer}>
                    <BrandLoader />
                </div>
            )}
            {!loading && outOfBounds && outOfTheBoundsComponent}
        </>
    );
};

export default DeliveryLocationSearch;
