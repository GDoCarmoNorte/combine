export enum LocationTabPaths {
    PICKUP = 'pk',
    DELIVERY = 'dl',
}

export enum LocationTabIndex {
    'pk' = 0,
    'dl' = 1,
}
