import React, { useState, useEffect, FC, FormEvent } from 'react';
import { Entry } from 'contentful';
import { useRouter } from 'next/router';

import Map from '../../../clientOnly/map';
import PickupSearchResults from '../pickupSearchResults';
import DeliveryWrapper from '../deliveryWrapper';

import { ILocationNotFoundFields, ILocationFailedFields } from '../../../../@generated/@types/contentful';

import LocationsPageLayout from '../locationsPageLayout';
import { InspireTabSet, InspireTab, InspireTabPanel } from '../../../atoms/tabset';
import classNames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import Input from '../../../atoms/Input';
import Icon from '../../../atoms/BrandIcon';
import styles from './locationsPageContentPickupAndDeliveryFlow.module.css';
import usePickupFlow from '../../../../common/hooks/usePickupFlow';
import { LoadingStatusEnum } from '../../../../common/types';
import PickupSearchInputIcon from '../pickupSearchInputIcon';
import { LocationTabIndex, LocationTabPaths } from './types';
import { useConfiguration } from '../../../../redux/hooks';

interface LocationsPageContentPickupAndDeliveryFlowProps {
    sections: {
        locationNotFound: Entry<ILocationNotFoundFields>;
        locationFailed: Entry<ILocationFailedFields>;
    };
}

const LocationsPageContentPickupAndDeliveryFlow: FC<LocationsPageContentPickupAndDeliveryFlowProps> = (props) => {
    const {
        sections: { locationNotFound, locationFailed },
    } = props;

    const [deliveryLocation, setDeliveryLocation] = useState(null);

    const [activeTab, setActiveTab] = useState<number>(0);
    const router = useRouter();

    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    const defaultActiveTab: string = (router.query.t as string) || LocationTabPaths.PICKUP;

    useEffect(() => {
        const defaultActiveTabIndex: number = LocationTabIndex[defaultActiveTab];
        setActiveTab(defaultActiveTabIndex);
    }, [defaultActiveTab]);

    const handleChangeTab = (_: FormEvent<HTMLButtonElement>, newValue?: number): void => {
        const tabUrl = newValue ? LocationTabPaths.DELIVERY : LocationTabPaths.PICKUP;
        router.push({ pathname: '/locations', query: { t: tabUrl } }, null, { shallow: true });
    };

    const {
        selectedLocation,
        currentLocation,
        locationsList: pickupLocationsList,
        isAbleToFetchMoreLocations,
        locationsSearchStatus,
        moreLocationsFetchStatus,
        locationQuery,
        onMapLocationSelect,
        onViewMoreLocationsClick,
        onSearchClick,
        onSearchInputChange,
        onSearchInputKeyPress,
        onLocationSelect,
        onLocationSet,
        onUseMyLocationClick,
    } = usePickupFlow();

    const renderMap = () => {
        if (activeTab) {
            const deliveryLocationInsideList = deliveryLocation ? [deliveryLocation] : [];
            return <Map locations={deliveryLocationInsideList} place={deliveryLocation} isDelivery />;
        }

        return <Map locations={pickupLocationsList} place={selectedLocation} onLocationSelect={onMapLocationSelect} />;
    };

    return (
        <LocationsPageLayout
            renderMap={renderMap}
            renderSearchPanel={() => (
                <div className={styles.wrapper}>
                    <InspireTabSet
                        value={activeTab}
                        variant="fullWidth"
                        onChange={handleChangeTab}
                        className={styles.tabs}
                        disabled={!isOAEnabled}
                    >
                        <InspireTab
                            label="Pickup"
                            icon={<Icon icon="order-pickup" size="xs" />}
                            id={`locations-tab-0`}
                            aria-controls={`locations-tabpanel-0`}
                            className={styles.tab}
                            disabled={!isOAEnabled}
                        />
                        <InspireTab
                            label="Delivery"
                            icon={<Icon icon="order-delivery" size="xs" />}
                            id={`locations-tab-1`}
                            aria-controls={`locations-tabpanel-1`}
                            className={styles.tab}
                            disabled={!isOAEnabled}
                        />
                    </InspireTabSet>
                    <InspireTabPanel
                        value={activeTab}
                        index={0}
                        id={`locations-tabpanel-0`}
                        aria-labelledby={`locations-tab-0`}
                        className={classNames(styles.tabPanel, styles.pickup)}
                    >
                        <div className={styles.pickupSearch}>
                            <Input
                                searchIconAriaLabel="Search Location"
                                searchInputAriaLabel="Search Location Input"
                                defaultValue={locationQuery}
                                onKeyPress={onSearchInputKeyPress}
                                onChange={onSearchInputChange}
                                onClick={onSearchClick}
                                type="search"
                                placeholder="Search by zip or city and state"
                                leftIcon={<PickupSearchInputIcon />}
                                disabled={!isOAEnabled}
                            />
                            <div
                                className={classNames(styles.useMyLocationWrapper, {
                                    [styles.hide]: locationsSearchStatus !== LoadingStatusEnum.Idle,
                                })}
                            >
                                <div className={styles.separator}>
                                    <span>or</span>
                                </div>
                                <InspireButton
                                    className={styles.useMyLocationBtn}
                                    type="primary"
                                    text="use my location"
                                    onClick={onUseMyLocationClick}
                                    disabled={!isOAEnabled}
                                />
                            </div>
                        </div>
                        <PickupSearchResults
                            selectedLocation={selectedLocation}
                            onLocationSet={onLocationSet}
                            locationsList={pickupLocationsList}
                            onLocationSelect={onLocationSelect}
                            currentLocation={currentLocation}
                            locationsSearchStatus={locationsSearchStatus}
                            locationFailedMessage={locationFailed}
                            locationNotFoundMessage={locationNotFound}
                            onViewMoreButtonClick={onViewMoreLocationsClick}
                            moreLocationsLoading={moreLocationsFetchStatus === LoadingStatusEnum.Loading}
                            showViewMoreButton={isAbleToFetchMoreLocations}
                            showAllLocationsLink
                            showIdleScreen
                            isOAEnabled={isOAEnabled}
                        />
                    </InspireTabPanel>
                    <InspireTabPanel
                        value={activeTab}
                        index={1}
                        id={`locations-tabpanel-1`}
                        aria-labelledby={`locations-tab-1`}
                        className={classNames(styles.tabPanel, styles.delivery)}
                    >
                        <DeliveryWrapper selectLocation={setDeliveryLocation} setActiveTab={setActiveTab} />
                    </InspireTabPanel>
                </div>
            )}
        />
    );
};

export default LocationsPageContentPickupAndDeliveryFlow;
