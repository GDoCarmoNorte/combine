import React, { FC } from 'react';
import Icon from '../../../atoms/BrandIcon';

import styles from './pickupSearchInputIcon.module.css';

const PickupSearchInputIcon: FC = () => {
    return <Icon size="s" icon="action-location-filled" className={styles.icon} />;
};

export default PickupSearchInputIcon;
