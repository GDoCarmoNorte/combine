import React, { useState, useMemo } from 'react';

import {
    IDefaultModifier,
    IDisplayFullProduct,
    IDisplayProduct,
    ISelectedModifier,
    ProductIntensityEnum,
    ProductTypesEnum,
    ModifierCardSelectorType,
} from '../../../redux/types';
import {
    getFormattedModifications,
    getModifierCardType,
    getPriceAndCaloriesWithAddedRemovedModifiers,
} from '../../../lib/domainProduct';
import { useProductOrderAheadAvailability } from '../../../common/hooks/useProductOrderAheadAvailability';
import { IModifierItemById, IProductItemById } from '../../../common/services/globalContentfulProps';

import ModifyProductModal from '../../organisms/modifyProductModal';
import { useDisplayModifierGroupsByProductId } from '../../../redux/hooks/pdp';
import { useDomainProduct } from '../../../redux/hooks/domainMenu';
import ModifierCard from '../../atoms/ModifierCard';

export interface IModifierSelectedParams {
    id: string;
    quantity: number;
    displayProduct: IDisplayProduct;
}

export interface IModifierCardContainerProps {
    modifierItemsById: IModifierItemById | IProductItemById;
    productsById?: IProductItemById;
    selectorType: ModifierCardSelectorType;
    selectedValue?: string | number;
    canIncrement?: boolean;
    canDecrement?: boolean;
    onChange: (params: IModifierSelectedParams) => void;
    categoryName?: string;
    hideOrderAheadUnavailable?: boolean;
    displayProduct: IDisplayProduct;
    isPromo?: boolean;
    sectionIndex: number;
    addedModifiers?: ISelectedModifier[];
    removedDefaultModifiers?: IDefaultModifier[];
    intensity?: ProductIntensityEnum;
    disabled?: boolean;
    selectionType: 'single' | 'multi';
}

export function ModifierCardContainer(props: IModifierCardContainerProps): JSX.Element {
    const {
        selectorType,
        onChange,
        modifierItemsById, // TODO: replace in favor of displayProduct
        canDecrement,
        categoryName,
        canIncrement,
        hideOrderAheadUnavailable = false,
        displayProduct,
        isPromo,
        productsById,
        sectionIndex,
        addedModifiers,
        removedDefaultModifiers,
        intensity,
        disabled,
        selectionType,
    } = props;
    const { displayProductDetails } = displayProduct;

    const handleSetSelectedItem = (itemId: string) => {
        onChange({
            id: itemId,
            quantity: 1,
            displayProduct: displayProduct,
        });
    };

    const {
        price,
        calories,
        quantity,
        maxQuantity,
        defaultQuantity,
        productId: modifierItemId,
        selections,
    } = displayProductDetails;

    const [modalOpened, setModalOpen] = useState<boolean>(false);

    const { isOrderAheadAvailable } = useProductOrderAheadAvailability(modifierItemId);

    const domainProduct = useDomainProduct(modifierItemId);
    const adjustedSelectorType = getModifierCardType(domainProduct, selectorType);

    const modifierGroups = useDisplayModifierGroupsByProductId(
        modifierItemId,
        domainProduct?.productGroupId,
        sectionIndex
    );
    const modifiers = useMemo(() => {
        if (addedModifiers && removedDefaultModifiers) {
            return getFormattedModifications(addedModifiers, removedDefaultModifiers);
        }

        return null;
    }, [addedModifiers, removedDefaultModifiers]);
    if (hideOrderAheadUnavailable && !isOrderAheadAvailable) return null;

    const contentfulProduct =
        (isPromo ? productsById[modifierItemId] : modifierItemsById[modifierItemId]) || modifierItemsById['default'];

    const { resultPrice, resultCalories } = getPriceAndCaloriesWithAddedRemovedModifiers(
        addedModifiers,
        removedDefaultModifiers,
        price,
        calories,
        quantity
    );

    const showPrice = resultPrice && resultPrice > 0 && (!defaultQuantity || quantity > defaultQuantity) && !isPromo;

    const handleClick = () => {
        onChange({
            id: modifierItemId,
            quantity: quantity > 0 ? 0 : 1,
            displayProduct,
        });
    };

    const handleCloseModal = (e) => {
        e.stopPropagation();
        setModalOpen(false);
    };

    const handleOpenModal = (e) => {
        e.stopPropagation();

        if (!quantity) {
            handleClick();
        }

        setModalOpen(true);
    };

    const handleCardMinus = () => {
        onChange({ id: modifierItemId, quantity: quantity - 1, displayProduct });
    };

    const handleCardPlus = () => {
        onChange({ id: modifierItemId, quantity: quantity + 1, displayProduct });
    };

    const showModify = !!modifierGroups?.length && !!productsById;

    return (
        <>
            <ModifierCard
                canDecrement={canDecrement}
                canIncrement={canIncrement}
                contentfulProduct={contentfulProduct}
                productName={displayProduct?.displayName || ''}
                modifierItemId={modifierItemId}
                modifiers={modifiers}
                onCardMinus={handleCardMinus}
                onCardPlus={handleCardPlus}
                onClick={handleClick}
                onModifyClick={handleOpenModal}
                onSelectSize={handleSetSelectedItem}
                quantity={quantity}
                calories={resultCalories}
                price={resultPrice}
                selections={selections}
                selectionType={selectionType}
                showModify={showModify}
                showPrice={showPrice}
                selectorType={adjustedSelectorType}
                selected={quantity > 0}
                intensity={intensity}
                disabled={disabled}
                maxQuantityTooltipText={`Maximum of ${maxQuantity} ${categoryName}`}
            />
            {showModify && quantity > 0 && (
                <ModifyProductModal
                    contentfulProduct={productsById[modifierItemId]}
                    displayProduct={
                        {
                            ...displayProduct.displayProductDetails,
                            productType: ProductTypesEnum.Single,
                        } as IDisplayFullProduct
                    }
                    childProductId={modifierItemId}
                    sectionIndex={sectionIndex}
                    open={modalOpened}
                    onClose={handleCloseModal}
                    modifierItemsById={modifierItemsById}
                    productsById={productsById}
                    modifierGroups={modifierGroups}
                />
            )}
        </>
    );
}
