import React, { useMemo } from 'react';
import { useDispatch } from 'react-redux';

import { IProductFields, IProductModifierFields } from '../../../@generated/@types/contentful';

import { IDisplayModifierGroup, IDisplayProduct, ModifierCardSelectorType } from '../../../redux/types';
import {
    getDisplayModifierGroupDefaultModifiersCount,
    getFormattedModifications,
    getModifierCardType,
    getTallyModifierGroupModifiersCount,
} from '../../../lib/domainProduct';

import { useBwwDisplayModifierGroup, useBwwDisplayModifierGroupsForModifier } from '../../../redux/hooks/pdp';
import { useDefaultModifiers, useDomainProduct, useSelectedModifiers } from '../../../redux/hooks/domainMenu';
import { useGlobalProps, usePdp } from '../../../redux/hooks';
import { NO_SAUCE_CATEGORY_ID } from '../../../common/constants/product';
import { IModifierItemById, IProductItemById } from '../../../common/services/globalContentfulProps';
import { getChangedModifiers } from '../../../common/helpers/getChangedModifiers';
import { GTM_MODIFIER_SECTION } from '../../../common/services/gtmService/constants';
import { ModifierSelectionAction } from '../../../common/services/gtmService/types';
import { InspireCmsEntry } from '../../../common/types';

interface IBwwModifierCardContainerProps {
    modifierGroupId: string;
    displayProduct: IDisplayProduct;
    selectionType: 'single' | 'multi';
    children: (renderProps: IRenderProps) => React.ReactElement;
    parentModifierId?: string;
    parentModifierGroupId?: string;
}

interface IRenderProps {
    contentfulProduct: InspireCmsEntry<IProductFields> | InspireCmsEntry<IProductModifierFields>;
    selectorType: ModifierCardSelectorType;
    onClick: () => void;
    onPlus: () => void;
    onMinus: () => void;
    isSelected: boolean;
    isDisabled: boolean;
    productName: string;
    showModify: boolean;
    canIncrement: boolean;
    canDecrement: boolean;
    maxQuantityTooltipText: string;
    productsById: IProductItemById;
    modifierItemsById: IModifierItemById;
    modifierGroups: IDisplayModifierGroup[];
    modifiersText?: string;
    showPrice: boolean;
    requireSelectionModifiersGroups: string[];
}

export function BwwModifierCardContainer(props: IBwwModifierCardContainerProps): JSX.Element {
    const { modifierItemsById, productsById } = useGlobalProps();
    const { displayProduct, selectionType, modifierGroupId, children, parentModifierId, parentModifierGroupId } = props;

    const { displayProductDetails } = displayProduct;

    const { quantity, maxQuantity, productId: modifierItemId, defaultQuantity } = displayProductDetails;

    const domainProduct = useDomainProduct(modifierItemId);

    const selectorType = maxQuantity > 1 ? 'quantity' : 'default';
    const adjustedSelectorType = getModifierCardType(domainProduct, selectorType);

    const parentGroup = useBwwDisplayModifierGroup(modifierGroupId, parentModifierGroupId, parentModifierId);

    const contentfulProduct = modifierItemsById[modifierItemId] || modifierItemsById['default'];

    const totalCount = parentGroup?.modifiers?.reduce((res, modifier) => {
        return res + modifier.displayProductDetails.quantity || 0;
    }, 0);

    const isMaxAmountReached = totalCount === parentGroup?.maxQuantity;
    const shouldDisableUnselected = isMaxAmountReached && selectionType === 'multi';
    const isMinAmountReached = totalCount === parentGroup?.minQuantity;

    const isSelected = quantity > 0;

    const isNoModifiersItem = domainProduct.categoryIds?.some((it) => it === NO_SAUCE_CATEGORY_ID);
    const isDisabled = shouldDisableUnselected && !isSelected && !isNoModifiersItem;

    const productName = displayProduct.displayName || contentfulProduct?.fields?.name;
    const maxQuantityTooltipText = `Maximum of ${parentGroup?.maxQuantity} ${parentGroup?.displayName || ''}`;

    const modifierGroups = useBwwDisplayModifierGroupsForModifier(modifierGroupId, modifierItemId);
    const showModify = !!modifierGroups?.length;

    const canIncrement = !(isMaxAmountReached || quantity === maxQuantity);
    const canDecrement = !isMinAmountReached;

    const needToPreventClick = selectionType === 'single' && isSelected;

    const {
        pdpTallyItem,
        actions: { onModifierChange },
    } = usePdp();

    const dispatch = useDispatch();

    const handleClick = () => {
        if (!needToPreventClick) {
            onModifierChange({
                modifierGroupId,
                modifierId: modifierItemId,
                parentModifierGroupId,
                parentModifierId,
                quantity: quantity === 0 ? 1 : 0,
            });
        }
    };

    const handleCardMinus = () => {
        if (!isDisabled && canDecrement) {
            dispatch({
                type: GTM_MODIFIER_SECTION,
                payload: {
                    action: ModifierSelectionAction.REMOVE,
                    label: productName,
                },
            });

            onModifierChange({
                modifierGroupId,
                modifierId: modifierItemId,
                parentModifierGroupId,
                parentModifierId,
                quantity: quantity - 1,
            });
        }
    };

    const handleCardPlus = () => {
        if (!isDisabled && canIncrement) {
            dispatch({
                type: GTM_MODIFIER_SECTION,
                payload: {
                    action: ModifierSelectionAction.ADD,
                    label: productName,
                },
            });
            onModifierChange({
                modifierGroupId,
                modifierId: modifierItemId,
                parentModifierGroupId,
                parentModifierId,
                quantity: quantity + 1,
            });
        }
    };

    const tallyItemGroup = pdpTallyItem?.modifierGroups
        ?.find((group) => {
            return group.productId === modifierGroupId;
        })
        ?.modifiers?.find((modifier) => modifier.productId === modifierItemId);

    const selectedModifiers = useSelectedModifiers(tallyItemGroup);
    const defaultModifiers = useDefaultModifiers(tallyItemGroup?.productId);

    const selectRequireSelectionModifiersGroups = () => {
        const requireSelection = [];

        modifierGroups?.forEach((mg) => {
            const tallyGroup = tallyItemGroup?.modifierGroups?.find((el) => el.productId === mg.modifierGroupId);

            if (tallyGroup) {
                if (mg.minQuantity > getTallyModifierGroupModifiersCount(tallyGroup)) {
                    requireSelection.push(mg.displayName);
                }
            } else if (mg.minQuantity > getDisplayModifierGroupDefaultModifiersCount(mg)) {
                requireSelection.push(mg.displayName);
            }
        });

        return requireSelection;
    };

    const requireSelectionModifiersGroups = selectRequireSelectionModifiersGroups();

    const { addedModifiers, removedDefaultModifiers } = useMemo(
        () => getChangedModifiers(selectedModifiers, defaultModifiers),
        [selectedModifiers, defaultModifiers]
    );

    const modifiersText = getFormattedModifications(addedModifiers, removedDefaultModifiers);

    // reuse arbys logic
    const showPrice =
        displayProductDetails.price &&
        displayProductDetails.price > 0 &&
        (!defaultQuantity || quantity > defaultQuantity);

    return children({
        contentfulProduct,
        selectorType: adjustedSelectorType,
        onClick: handleClick,
        onPlus: handleCardPlus,
        onMinus: handleCardMinus,
        showModify,
        isSelected,
        isDisabled,
        canDecrement,
        canIncrement,
        productName,
        maxQuantityTooltipText,
        productsById,
        modifierItemsById,
        modifierGroups,
        modifiersText,
        showPrice,
        requireSelectionModifiersGroups,
    });
}
