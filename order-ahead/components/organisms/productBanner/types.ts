import { ICaloriesLegal, IProductFields } from '../../../@generated/@types/contentful';

import { ISizeSelection } from '../../../redux/types';
import { IPriceAndCalories } from '../../../lib/tallyItem';
import { InspireCmsEntry } from '../../../common/types';

export interface IProductBannerProps {
    productInfo: IPriceAndCalories;
    product: InspireCmsEntry<IProductFields>;
    selections?: ISizeSelection[];
    makeItAMealPath?: string;
    onMakeItAMeal?: () => void;
    onSizeSelect?: (productId: string) => void;
    onAddToBag: () => void;
    onModifyProduct: () => void;
    displayName: string;
    mainProductSectionName: string;
    addToBagBtnText: string;
    showMakeItAMealButton?: boolean;
    productType: string;
    childProductId?: string;
    caloriesLegal?: ICaloriesLegal;
}
