import React, { useMemo } from 'react';
import classNames from 'classnames';

import { IProductBannerProps } from './types';
import { InspireButton } from '../../atoms/button';
import {
    useIsProductEditable,
    useDefaultModifiers,
    useSelectedModifiers,
    useSelectedSideAndDrinks,
    useDomainMenuSelectors,
} from '../../../redux/hooks/domainMenu';
import { useLocalization } from '../../../common/hooks/useLocalization';
import usePdp from '../../../redux/hooks/usePdp';
import { getChangedModifiers } from '../../../common/helpers/getChangedModifiers';

import styles from './productBanner.module.css';
import ContentfulImage from '../../atoms/ContentfulImage';
import TextWithTrademark from '../../atoms/textWithTrademark';
import { ProductItemPriceAndCalories } from '../../atoms/productItemPriceAndCalories';
import { ProductTypesEnum } from '../../../redux/types';
import { getFormattedModifications } from '../../../lib/domainProduct';
import SizeSelection from '../../atoms/sizeSelection';
import { useProductIsSaleable } from '../../../common/hooks/useProductIsSaleable';
import {
    GTM_NOT_SALEABLE_DESCRIPTION,
    GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
    NOT_SALEABLE_BUTTON_LINK,
    NOT_SALEABLE_BUTTON_TEXT,
    NOT_SALEABLE_DESCRIPTION,
} from '../../../common/constants/product';
import { useSodiumWarning } from '../../../common/hooks/useSodiumWarning';
import { useTallyItemHasSodiumWarning } from '../../../common/hooks/useTallyItemHasSodiumWarning';
import { useSodiumLegalWarning } from '../../../common/hooks/useSodiumLegalWarning';
import { SodiumWarningAreaEnumModel } from '../../../@generated/webExpApi';
import { GtmErrorEvent } from '../../../common/services/gtmService/types';
import { useDispatch } from 'react-redux';
import { GTM_ERROR_EVENT } from '../../../common/services/gtmService/constants';

export default function ProductBanner(props: IProductBannerProps): JSX.Element {
    const {
        product: contentfulProduct,
        productInfo,
        onAddToBag,
        onModifyProduct,
        selections,
        onMakeItAMeal,
        onSizeSelect,
        addToBagBtnText,
        displayName,
        mainProductSectionName,
        showMakeItAMealButton,
        productType,
        childProductId,
    } = props;
    const { pdpTallyItem } = usePdp();
    const isPromo = productType === ProductTypesEnum.Promo;
    let isMealWithMultiSizeSandwich = false;

    const { productId } = pdpTallyItem;

    const isProductEditable = useIsProductEditable(productId);
    const { locationLinkText, descriptionText, isLocationOrderAheadAvailable } = useLocalization(productId);

    const selectedModifiers = useSelectedModifiers(pdpTallyItem);
    const defaultModifiers = useDefaultModifiers(productId);

    const { addedModifiers, removedDefaultModifiers, modifiersIsChanged } = useMemo(
        () => getChangedModifiers(selectedModifiers, defaultModifiers),
        [selectedModifiers, defaultModifiers]
    );

    const sodiumWarning = useSodiumWarning();
    const sodiumLegalWarning = useSodiumLegalWarning();
    const isNycArea = sodiumLegalWarning?.additionalProperties.area === SodiumWarningAreaEnumModel.Nyc;
    const pdpHasSodiumWarning = useTallyItemHasSodiumWarning(pdpTallyItem);
    const showSodiumWarning = !!sodiumWarning && pdpHasSodiumWarning;

    const selectedSides = useSelectedSideAndDrinks(pdpTallyItem);
    const { selectProductById, selectProductSizes } = useDomainMenuSelectors();
    const dispatch = useDispatch();

    if (childProductId) {
        const childDomainProduct = selectProductById(childProductId);
        const childProductSizes = selectProductSizes(childProductId);
        isMealWithMultiSizeSandwich =
            productType === ProductTypesEnum.Meal &&
            mainProductSectionName === 'Sandwich' &&
            childProductSizes.length > 1;

        isMealWithMultiSizeSandwich && selectedSides.unshift(childDomainProduct.name);
    }

    const { isSaleable } = useProductIsSaleable(productId);

    if (isSaleable === false || locationLinkText) {
        const gtmErrorEventNotAvailable = {
            ErrorCategory: GTM_UNAVAILABLE_AT_LOCATION_CATEGORY,
            ErrorDescription: isSaleable === false ? GTM_NOT_SALEABLE_DESCRIPTION : descriptionText,
        } as GtmErrorEvent;
        dispatch({ type: GTM_ERROR_EVENT, payload: gtmErrorEventNotAvailable });
    }

    const showModifiers = isSaleable && !locationLinkText && isProductEditable;

    const modifiers = useMemo(() => getFormattedModifications(addedModifiers, removedDefaultModifiers), [
        addedModifiers,
        removedDefaultModifiers,
    ]);

    const handleMakeItAMealClick = () => {
        onMakeItAMeal();
    };

    const handleAddToBagClick = () => {
        onAddToBag();
    };

    const handleModifyProductKeyPress = (e: React.KeyboardEvent<HTMLAnchorElement>): void => {
        if (e.key === 'Enter') {
            e.preventDefault();
            onModifyProduct();
        }
    };

    const handleModifyProductClick = () => {
        onModifyProduct();
    };

    const renderButtonsBlock = () => {
        let content: JSX.Element;

        if (isSaleable === false) {
            content = (
                <div>
                    <p
                        className={classNames('t-paragraph', styles.notSaleableDescription)}
                        dangerouslySetInnerHTML={{ __html: NOT_SALEABLE_DESCRIPTION }}
                    />
                    <InspireButton link={NOT_SALEABLE_BUTTON_LINK} type="primary" text={NOT_SALEABLE_BUTTON_TEXT} />
                </div>
            );
        } else if (locationLinkText) {
            content = (
                <div>
                    <p
                        className={classNames('t-paragraph', styles.localizationDescription)}
                        dangerouslySetInnerHTML={{ __html: descriptionText }}
                    />
                    <InspireButton link={'/locations'} type="primary" text={locationLinkText} />
                </div>
            );
        } else {
            content = (
                <>
                    <InspireButton onClick={handleAddToBagClick} type="primary" text={addToBagBtnText} />
                    {isProductEditable && !isPromo && (
                        <a
                            className={styles.modifyLink}
                            tabIndex={0}
                            onKeyPress={handleModifyProductKeyPress}
                            onClick={handleModifyProductClick}
                            role="link"
                        >
                            Modify {mainProductSectionName}
                        </a>
                    )}
                </>
            );
        }

        return (
            <div className={styles.buttonBlock}>
                {showMakeItAMealButton && (
                    <InspireButton
                        className={styles.makeItAMeal}
                        onClick={handleMakeItAMealClick}
                        type="primary"
                        text="Make it a meal"
                    />
                )}
                {content}
            </div>
        );
    };

    const renderMobileButtonsBlock = () => {
        let content: JSX.Element;

        if (isSaleable === false) {
            content = (
                <p
                    className={classNames('t-paragraph', styles.notSaleableDescription)}
                    dangerouslySetInnerHTML={{ __html: NOT_SALEABLE_DESCRIPTION }}
                />
            );
        } else if (locationLinkText) {
            content = (
                <p
                    className={classNames('t-paragraph', styles.localizationDescription)}
                    dangerouslySetInnerHTML={{ __html: descriptionText }}
                />
            );
        } else {
            content = isProductEditable && !isPromo && (
                <a href="#" tabIndex={0} className={styles.modifyLink} onClick={onModifyProduct}>
                    Modify {mainProductSectionName}
                </a>
            );
        }

        return <div className={styles.mobileButtonBlock}>{content}</div>;
    };

    return (
        <div className={styles.container} aria-label="Product Main Section">
            <div className={styles.bannerImageContainer}>
                <ContentfulImage asset={contentfulProduct?.fields.image} />
            </div>
            <div className={styles.descriptionContainer}>
                <TextWithTrademark
                    tag="h1"
                    text={displayName || contentfulProduct.fields.name}
                    className={classNames('t-header-hero', styles.menuItemName)}
                    afterContent={
                        showSodiumWarning &&
                        isNycArea && (
                            <img
                                alt="Sodium warning label"
                                className={styles.sodiumWarningIcon}
                                src={sodiumWarning.fields.icon.fields.icon.fields.file.url}
                            />
                        )
                    }
                />
                {showSodiumWarning && !isNycArea && (
                    <img
                        alt="Sodium warning label"
                        className={classNames(styles.sodiumWarningIcon, styles.sodiumWarningPhillyIcon)}
                        src={sodiumWarning.fields.icon.fields.icon.fields.file.url}
                    />
                )}
                {isSaleable && !locationLinkText && !isPromo && (
                    <SizeSelection
                        key={locationLinkText}
                        selections={selections}
                        selectedProductId={productId}
                        onSelect={onSizeSelect}
                        disabled={!isLocationOrderAheadAvailable}
                    />
                )}
                {showModifiers && (
                    <div
                        className={classNames('t-paragraph-hint-strong', styles.modifiersContainer, {
                            [styles.withoutSizeButtons]: selections && selections?.length < 2,
                        })}
                        aria-label="Current Sides and Modifications"
                    >
                        {selectedSides.length ? (
                            <div>
                                {isPromo || isMealWithMultiSizeSandwich ? 'Selections: ' : 'Sides: '}
                                <TextWithTrademark tag="span" text={selectedSides.join(', ')} />
                            </div>
                        ) : null}
                        {!isPromo && modifiersIsChanged && (
                            <div className={classNames('truncate', styles.modifications)} title={modifiers}>
                                {'Modifications: '}
                                {modifiers}
                            </div>
                        )}
                    </div>
                )}

                <ProductItemPriceAndCalories
                    className={classNames(styles.priceContainer, 't-paragraph-hint')}
                    price={isSaleable ? productInfo.price : null}
                    calories={productInfo.calories}
                />
                {renderButtonsBlock()}
            </div>
            {renderMobileButtonsBlock()}
        </div>
    );
}
