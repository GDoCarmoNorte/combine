import React from 'react';
import RichText from '../../atoms/richText';
import styles from './caloriesLegalSection.module.css';
import { BLOCKS } from '@contentful/rich-text-types';

const CaloriesLegalSection = (props) => {
    const text = props.prop?.fields?.message;
    const renderNode = {
        [BLOCKS.PARAGRAPH]: (node, next) => `<p class='${styles.caloriesParagraph}'>${next(node.content)}</p>`,
    };
    return (
        <div className={styles.caloriesContainer}>
            <RichText text={text} renderNode={renderNode}></RichText>
        </div>
    );
};

export default CaloriesLegalSection;
