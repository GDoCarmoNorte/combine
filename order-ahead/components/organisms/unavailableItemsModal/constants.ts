export const DESCRIPTION_FOR_INVALID_TIME =
    'These products are only available during a specific period of time. Update your chosen time to order these products or remove unavailable products.';
export const DESCRIPTION_FOR_UNAVAILABLE_ITEMS = 'A modification or side in your bag is no longer available.';
export const TITLE = 'change in your bag';
export const FOOTER_TEXT = 'One or more of the items above require modification before you can proceed.';
