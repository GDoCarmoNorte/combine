import React, { useEffect } from 'react';
import classNames from 'classnames';

import { InspireButton } from '../../atoms/button';
import UnavailableItem from './unavailableItem';
import Loader from '../../atoms/Loader';
import { IUnavailableItem } from '../../../common/hooks/useUnavailableItems';

import { DESCRIPTION_FOR_INVALID_TIME, DESCRIPTION_FOR_UNAVAILABLE_ITEMS, TITLE, FOOTER_TEXT } from './constants';
import styles from './unavailableItemsModal.module.css';
import { useDispatch } from 'react-redux';
import { GTM_ERROR_EVENT } from '../../../common/services/gtmService/constants';
import { GtmErrorCategory, GtmErrorEvent } from '../../../common/services/gtmService/types';
import { TallyProductModel } from '../../../@generated/webExpApi/models';
import { UnavailableModal } from '../../molecules/unavailableModal/unavailableModal';

interface UnavailableItemsModal {
    unavailableItems: IUnavailableItem[];
    open: boolean;
    onClose: () => void;
    onRemove: (lineItemId: number) => void;
    onModifiersRemove: (
        entry: TallyProductModel,
        unavailableModifiers: string[],
        unavailableSubModifiers: string[]
    ) => void;
    onModifyClick: (lineItemId: number) => void;
    loading?: boolean;
    isNotValidTime: boolean;
}

export default function UnavailableItemsModal(props: UnavailableItemsModal): JSX.Element {
    const {
        open,
        onClose,
        unavailableItems,
        onRemove,
        loading,
        isNotValidTime,
        onModifiersRemove,
        onModifyClick,
    } = props;

    const dispatch = useDispatch();

    const description = isNotValidTime ? DESCRIPTION_FOR_INVALID_TIME : DESCRIPTION_FOR_UNAVAILABLE_ITEMS;

    useEffect(() => {
        const payload = {
            ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_ITEMS,
            ErrorDescription: description,
        } as GtmErrorEvent;
        dispatch({ type: GTM_ERROR_EVENT, payload });
    }, [description, dispatch]);

    if (!unavailableItems?.length) return null;

    const hasUnavailableItems = unavailableItems.some((unavailableItem) => !unavailableItem.isTouched);

    const renderButtons = () => (
        <InspireButton
            onClick={onClose}
            type="primary"
            text="Proceed to checkout"
            disabled={hasUnavailableItems}
            className={styles.button}
            fullWidth
        />
    );

    const renderLoader = () => (
        <div className={classNames({ [styles.loading]: loading }, styles.loadingWrapper)}>
            <Loader size={30} className={styles.loader} />
        </div>
    );

    return (
        <UnavailableModal
            open={open}
            onClose={onClose}
            title={TITLE}
            footerText={FOOTER_TEXT}
            description={description}
            renderLoader={renderLoader}
            renderButtons={renderButtons}
        >
            {unavailableItems?.map(
                ({
                    productId,
                    lineItemId,
                    modifierIds,
                    subModifierIds,
                    isWholeProductUnavailable,
                    isDefaultModifiersUnavailable,
                    isTouched,
                }) => (
                    <UnavailableItem
                        key={productId}
                        lineItemId={lineItemId}
                        unavailableProductId={productId}
                        unavailableModifierIds={modifierIds}
                        unavailableSubModifierIds={subModifierIds}
                        isWholeProductUnavailable={isWholeProductUnavailable}
                        isDefaultModifiersUnavailable={isDefaultModifiersUnavailable}
                        isTouched={isTouched}
                        onRemove={onRemove}
                        onModifiersRemove={onModifiersRemove}
                        onModifyClick={onModifyClick}
                    />
                )
            )}
        </UnavailableModal>
    );
}
