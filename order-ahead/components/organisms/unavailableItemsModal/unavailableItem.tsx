import React from 'react';
import classNames from 'classnames';

import { IProductFields, IProductModifierFields } from '../../../@generated/@types/contentful';
import { TallyProductModel } from '../../../@generated/webExpApi';

import ContentfulImage from '../../atoms/ContentfulImage';
import { useBag, useGlobalProps } from '../../../redux/hooks';
import { useDomainProduct } from '../../../redux/hooks/domainMenu';
import TextWithTrademark from '../../atoms/textWithTrademark';
import { InspireLinkButton } from '../../atoms/link';
import { IDomainProductItem } from '../../../redux/types';
import { InspireCmsEntry } from '../../../common/types';

import styles from './unavailableItem.module.css';

type Controls = {
    ariaLabel: string;
    onClick: () => void;
    text: string;
    title?: string;
}[];

enum ControlText {
    MODIFY = 'MODIFY',
    REMOVE = 'REMOVE',
    REMOVED = 'REMOVED',
    REMOVED_UNAVAILABLE_ITEMS = 'REMOVED UNAVAILABLE ITEMS',
}

enum ControlLabel {
    MODIFY_ITEM = 'modify bag item',
    REMOVE_ITEM = 'remove bag item',
    MODIFY_MODIFIER = 'modify unavailable modifier',
    REMOVE_MODIFIER = 'remove unavailable modifier',
}

interface UnavailableItem {
    lineItemId: number;
    unavailableProductId: string;
    unavailableModifierIds: string[];
    unavailableSubModifierIds: string[];
    isWholeProductUnavailable?: boolean;
    isDefaultModifiersUnavailable?: boolean;
    isTouched?: boolean;
    onRemove: (lineItemId: number) => void;
    onModifiersRemove: (
        entry: TallyProductModel,
        unavailableModifiers: string[],
        unavailableSubModifiers: string[]
    ) => void;
    onModifyClick: (lineItemId: number) => void;
}

const getModifierQuantity = (bagEntry: TallyProductModel, modifierId: string): number => {
    const modifierQuantity = bagEntry.modifierGroups?.reduce(
        (quantity, modifierGroup) =>
            quantity +
            modifierGroup.modifiers.reduce(
                (quantity, modifier) => (modifier.productId === modifierId ? quantity + modifier.quantity : quantity),
                0
            ),
        0
    );

    return modifierQuantity;
};

const getSubModifierQuantity = (bagEntry: TallyProductModel, subModifierId: string): number => {
    const subModifierQuantity = bagEntry.modifierGroups.reduce(
        (quantity, modifierGroup) =>
            quantity +
            modifierGroup.modifiers.reduce(
                (quantity, modifier) => quantity + (getModifierQuantity(modifier, subModifierId) || 0),
                0
            ),
        0
    );

    return subModifierQuantity;
};

const getProductName = (
    domainProduct?: IDomainProductItem,
    contentfulProduct?: InspireCmsEntry<IProductFields> | InspireCmsEntry<IProductModifierFields>
) => domainProduct?.name || contentfulProduct?.fields?.name || '';

const UnavailableModifierInfo = ({
    bagEntry,
    modifierId,
    subModifierId,
}: {
    bagEntry: TallyProductModel;
    modifierId?: string;
    subModifierId?: string;
}) => {
    const domainProduct = useDomainProduct(modifierId || subModifierId);
    const { modifierItemsById } = useGlobalProps();
    const contentfulProduct = modifierItemsById[modifierId || subModifierId];
    const productName = getProductName(domainProduct, contentfulProduct);

    const getQuantity = () => {
        if (modifierId) {
            return getModifierQuantity(bagEntry, modifierId);
        }
        return getSubModifierQuantity(bagEntry, subModifierId);
    };
    const quantity = getQuantity();

    return (
        <div>
            <span className={classNames('t-paragraph-hint', styles.notAvailableModifier)}>
                {productName} {quantity > 1 && `x${quantity}`}
            </span>
            <span className={classNames('t-paragraph-hint', styles.notAvailable)}>Not Available</span>
        </div>
    );
};

export default function UnavailableItem(props: UnavailableItem): JSX.Element {
    const {
        lineItemId,
        unavailableProductId,
        unavailableModifierIds,
        unavailableSubModifierIds,
        isWholeProductUnavailable,
        isDefaultModifiersUnavailable,
        isTouched,
        onRemove,
        onModifiersRemove,
        onModifyClick,
    } = props;

    const domainProduct = useDomainProduct(unavailableProductId);
    const { productsById } = useGlobalProps();
    const {
        bagEntries,
        actions: { setLastEdit },
    } = useBag();

    const bagEntry = bagEntries.find((i) => unavailableProductId === i.productId && lineItemId === i.lineItemId);
    const contentfulProduct = productsById[unavailableProductId];

    const handleRemoveClick = () => {
        onRemove(bagEntry.lineItemId);
    };

    const handleModifiersRemoveClick = () => {
        onModifiersRemove(bagEntry, unavailableModifierIds, unavailableSubModifierIds);
        setLastEdit(Date.now());
    };
    const handleModifyClick = () => {
        onModifyClick(bagEntry.lineItemId);
    };

    const productName = getProductName(domainProduct, contentfulProduct);

    const isBagItemUnavailable = isWholeProductUnavailable || isDefaultModifiersUnavailable;
    const isDefaultModifiersUnavailableUntouched =
        !isTouched && !isWholeProductUnavailable && isDefaultModifiersUnavailable;

    const removeTextTouched = isBagItemUnavailable ? ControlText.REMOVED : ControlText.REMOVED_UNAVAILABLE_ITEMS;
    const removeLabel = isBagItemUnavailable ? ControlLabel.REMOVE_ITEM : ControlLabel.REMOVE_MODIFIER;

    const controlsConfig: Controls = [
        ...(isDefaultModifiersUnavailableUntouched || (!isBagItemUnavailable && !isTouched)
            ? [
                  {
                      ariaLabel: isBagItemUnavailable ? ControlLabel.MODIFY_ITEM : ControlLabel.MODIFY_MODIFIER,
                      onClick: handleModifyClick,
                      text: ControlText.MODIFY,
                  },
              ]
            : []),
        {
            ariaLabel: removeLabel,
            onClick: isBagItemUnavailable ? handleRemoveClick : handleModifiersRemoveClick,
            title: removeLabel,
            text: isTouched ? removeTextTouched : ControlText.REMOVE,
        },
    ];

    return (
        <div className={classNames({ [styles.isDisabled]: isTouched }, styles.container)}>
            <ContentfulImage className={styles.bagItemImage} asset={contentfulProduct?.fields?.image} maxWidth={132} />
            <div className={styles.itemInfo}>
                <TextWithTrademark
                    tag="span"
                    text={`${(bagEntry?.quantity || 0) > 1 ? `${bagEntry.quantity}X ` : ''}${productName}`}
                    className={classNames('t-subheader-smaller', 'truncate-at-3')}
                />
                {isWholeProductUnavailable ? (
                    <div className={classNames('t-paragraph-hint', styles.notAvailable)}>Not Available</div>
                ) : (
                    [
                        ...unavailableModifierIds.map((unavailableModifierId) => (
                            <UnavailableModifierInfo
                                key={unavailableModifierId}
                                bagEntry={bagEntry}
                                modifierId={unavailableModifierId}
                            />
                        )),
                        ...unavailableSubModifierIds.map((unavailableSubModifierId) => (
                            <UnavailableModifierInfo
                                key={unavailableSubModifierId}
                                bagEntry={bagEntry}
                                subModifierId={unavailableSubModifierId}
                            />
                        )),
                    ]
                )}

                <div className={styles.unavailableItemLinks}>
                    {controlsConfig.map((control, index: number) => (
                        <InspireLinkButton
                            key={index}
                            aria-label={control.ariaLabel}
                            linkType="secondary"
                            className={styles.button}
                            onClick={control.onClick}
                            title={control.title}
                        >
                            {control.text}
                        </InspireLinkButton>
                    ))}
                </div>
            </div>
            <div className={styles.disableWrapper} />
        </div>
    );
}
