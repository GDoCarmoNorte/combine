import React, { useState } from 'react';
import classnames from 'classnames';
import { useCustomerPaymentMethod } from '../../../common/hooks/useCustomerPaymentMethod';
import BrandLoader from '.././../atoms/BrandLoader';
import PaymentCard from '../../atoms/paymentCard';
import { AddNewCard } from '../../atoms/addNewCard';
import PaymentType from './components/paymentType';
import PaymentModal from './components/paymentModal';

import styles from './paymentMethods.module.css';

export default function PaymentMethods({
    customerId,
    jwtToken,
}: {
    customerId: string;
    jwtToken: string;
}): JSX.Element {
    const [openModal, setOpenModal] = useState(false);
    const [creditCardtoken, setCreditCardToken] = useState<string | null>(null);
    const [isOverlayOpen, setIsOverlayOpen] = useState(false);

    const {
        paymentMethods,
        loading,
        handleSetLoading,
        handleDeletePaymentMethods,
        handleFetchPaymentMethods,
    } = useCustomerPaymentMethod();

    const handleOpenModal = () => setOpenModal(true);
    const handleCloseModal = () => setOpenModal(false);

    const handleDeletePaymentCard = (creditCardToken: string) => {
        setCreditCardToken(creditCardToken);
        handleOpenModal();
    };

    const handleConfirmDelete = () => {
        handleDeletePaymentMethods(creditCardtoken);
        handleCloseModal();
        setCreditCardToken(null);
    };

    const creditCardList = paymentMethods?.cREDITCARDS;

    const renderCreditCard =
        creditCardList &&
        creditCardList.map(
            (creditCard, index) =>
                index < 2 && (
                    <PaymentCard
                        className={styles.creditCard}
                        cardType={creditCard.type}
                        key={creditCard.token}
                        title={creditCard.label}
                        cardNumber={creditCard.lastFourDigits}
                        expiredDate={creditCard.expirationDate}
                        isExpired={creditCard.isExpired}
                        onRemove={handleDeletePaymentCard}
                        token={creditCard.token}
                    />
                )
        );

    return (
        <section className={styles.container}>
            <h2 className={classnames(styles.title, 't-header-h2')}>{'Payment Methods'}</h2>
            <PaymentType image={`/brands/inspire/creditCardIcon.svg`} text={'Credit Cards'} />

            {loading && <BrandLoader className={styles.loader} />}
            <div className={classnames(styles.gridWrapper)}>
                {renderCreditCard}
                {creditCardList && creditCardList.length <= 1 && (
                    <>
                        <AddNewCard
                            className={styles.creditCard}
                            text={'Add new credit card'}
                            onAddNewCard={() => setIsOverlayOpen(true)}
                            customerId={customerId}
                            isOverlayOpen={isOverlayOpen}
                            closeOverlay={() => setIsOverlayOpen(false)}
                            jwtToken={jwtToken}
                            handleFetchPaymentMethods={handleFetchPaymentMethods}
                            handleSetLoading={handleSetLoading}
                        />
                    </>
                )}
            </div>

            <PaymentModal
                open={openModal}
                onClose={handleCloseModal}
                title="Are you sure you want to delete this payment method?"
                confirmButtonProps={{
                    text: 'Yes, delete',
                    onClick: handleConfirmDelete,
                    className: classnames(styles.button),
                }}
                rejectButtonProps={{
                    text: 'No, leave it',
                    onClick: handleCloseModal,
                    className: styles.button,
                    type: 'secondary',
                }}
            />
        </section>
    );
}
