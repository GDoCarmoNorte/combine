import React from 'react';
import classnames from 'classnames';
import styles from '../paymentMethods.module.css';

interface IPaymentType {
    text: string;
    image: string;
}

export default function PaymentType(props: IPaymentType): JSX.Element {
    const { image, text } = props;
    return (
        <div className={styles.paymentType}>
            <img src={image} alt="" />
            <p className={classnames('t-paragraph', styles.paymentTypeText)}>{text}</p>
        </div>
    );
}
