import React from 'react';
import classNames from 'classnames';

import { Modal } from '@material-ui/core';
import Icon from '../../../../atoms/BrandIcon';
import { InspireButton, IInspireButtonProps } from '../../../../atoms/button';
import { Optional } from '../../../../../common/types';

import styles from './paymentModal.module.css';

interface IPaymentModal {
    open: boolean;
    title?: string;
    confirmButtonProps: Optional<IInspireButtonProps, 'type'>;
    rejectButtonProps: Optional<IInspireButtonProps, 'type'>;
    onClose: () => void;
    children?: JSX.Element;
}

export default function PaymentModal(props: IPaymentModal): JSX.Element {
    const { open, onClose, title, confirmButtonProps = {}, rejectButtonProps = {}, children } = props;

    return (
        <Modal open={open} onClose={onClose} className={styles.modal}>
            <div className={styles.modalContainer}>
                <Icon onClick={onClose} className={styles.modalIconClose} size="xs" icon="action-close" role="button" />

                {title && <h2 className={classNames(styles.modalTitle, 't-subheader-smaller')}>{title}</h2>}

                {children && <div>{children}</div>}

                <div className={styles.modalButtons}>
                    <InspireButton {...rejectButtonProps} />
                    <InspireButton {...confirmButtonProps} />
                </div>
            </div>
        </Modal>
    );
}
