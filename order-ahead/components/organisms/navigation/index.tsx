import React, { useState, useCallback, useRef } from 'react';
import Link from 'next/link';

import { INavigationFields, IUserAccountMenuFields } from '../../../@generated/@types/contentful';

import Icon from '../../atoms/BrandIcon';
import NavigationLink from './navigationLink';

import { useBag, useConfiguration } from '../../../redux/hooks';
import NavigationMenu from './navigationMenu';
import useWindowResize from '../../../common/hooks/useWindowResize';

import styles from './index.module.css';
import dynamic from 'next/dynamic';
import SignIn from './signIn';
import { useFeatureFlags } from '../../../redux/hooks/useFeatureFlags';
import { logoDesktop } from './constants';
import Logo from '../../atoms/Logo';
import { getImageUrl } from '../../../common/helpers/contentfulImage';
import { InspireCmsEntry } from '../../../common/types';
import { InspireButton } from '../../atoms/button';
import useStartAnOrderLink from '../../../common/hooks/useStartAnOrderLink';

const Bag = dynamic(import('./bag'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

const LocationFlowDropdown = dynamic(import('../../clientOnly/locationFlowDropdown'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

export default function Navigation(props: {
    navigation: InspireCmsEntry<INavigationFields>;
    userAccountMenu: InspireCmsEntry<IUserAccountMenuFields>;
    titleTooltip?: string;
}): JSX.Element {
    const { navigation, titleTooltip, userAccountMenu } = props;
    const { featureFlags } = useFeatureFlags();
    const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
    const navRef = useRef<HTMLElement>(null);
    const startAnOrderLink = useStartAnOrderLink();
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    const handleMenuOpen = useCallback(() => {
        setIsMenuOpen(true);
    }, []);
    const handleMenuClose = useCallback(() => {
        setIsMenuOpen(false);
    }, []);

    useWindowResize(handleMenuClose);
    const bag = useBag();

    const handleKeyPress = (e: React.KeyboardEvent<HTMLElement>) => {
        if (e.key === 'Enter') {
            bag.actions.toggleIsOpen({ isOpen: !bag.isOpen });
        }
    };

    const asset = getImageUrl(navigation?.fields?.logo, 1500);

    return (
        <>
            <nav className={styles.navigation} data-gtm-id="main-nav" ref={navRef}>
                <div className={styles.wrapper}>
                    <Icon
                        className={`${styles.icon} ${styles.menuIcon}`}
                        icon="navigation-hamburger"
                        onClick={handleMenuOpen}
                    />
                    <div className={styles.centerContainer}>
                        <div className={styles.logoContainer}>
                            <Link href="/">
                                <a className={styles.logo} data-gtm-id="main-nav-home">
                                    <Logo urlLogo={asset} urlLogoDesktop={logoDesktop} className={styles.logoImage} />
                                </a>
                            </Link>
                        </div>
                        <div className={styles.linkContainer}>
                            {navigation?.fields?.links?.map((link) => (
                                <NavigationLink key={link.sys.id} link={link} />
                            ))}
                        </div>
                        {featureFlags.account && <SignIn navRef={navRef} userAccountMenu={userAccountMenu} />}
                    </div>
                    <LocationFlowDropdown navRef={navRef} titleTooltip={titleTooltip} />
                    <div className={styles.iconsContainer}>
                        <a
                            aria-label="bag toggler"
                            tabIndex={0}
                            onClick={() => {
                                bag.actions.toggleIsOpen({ isOpen: !bag.isOpen });
                            }}
                            onKeyPress={handleKeyPress}
                        >
                            <Bag />
                        </a>
                    </div>
                </div>
                <div className={styles.startOrderBtnWrapper}>
                    <InspireButton
                        className={styles.startOrderBtn}
                        type="small"
                        text="start an order"
                        link={startAnOrderLink}
                        disabled={!isOAEnabled}
                    />
                </div>
            </nav>
            <NavigationMenu open={isMenuOpen} links={navigation?.fields?.links} onClose={handleMenuClose} />
        </>
    );
}
