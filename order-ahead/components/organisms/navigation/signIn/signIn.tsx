import React, { useMemo, useRef, useState, useEffect } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { useMediaQuery } from '@material-ui/core';
import classnames from 'classnames';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { IUserAccountMenuFields } from '../../../../@generated/@types/contentful';
import Icon from '../../../../components/atoms/BrandIcon';
import Loader from '../../../atoms/Loader';
import useAccount from '../../../../redux/hooks/useAccount';
import NavigationDropdown from '../navigationDropdown/navigationDropdown';
import CloseButton from '../../../atoms/CloseButton/closeButton';
import LogoutButton from '../../../atoms/logoutButton';
import { DEALS, REWARDS } from '../../../../common/constants/account';
import styles from './signIn.module.css';
import Counter from '../../../atoms/counter';
import useRewards from '../../../../redux/hooks/useRewards';
import { TOfferStatusModel } from '../../../../@generated/webExpApi';
import { getAccountNavLinks } from '../../../../common/helpers/accountHelper';
import useNotifications from '../../../../redux/hooks/useNotifications';
import { useDispatch } from 'react-redux';
import { GTM_USER_ID } from '../../../../common/services/gtmService/constants';
import { useLoyalty, useSelectedSell } from '../../../../redux/hooks';
import { InspireCmsEntry } from '../../../../common/types';
import { isRewardsOn } from '../../../../lib/getFeatureFlags';
import SuccessErrorModal from '../../../molecules/successErrorModal';
import { useConfiguration } from '../../../../common/hooks/useConfiguration';

const SignIn = (props: {
    navRef?: React.MutableRefObject<HTMLElement>;
    userAccountMenu: InspireCmsEntry<IUserAccountMenuFields>;
}): JSX.Element => {
    const { configuration } = useConfiguration();
    const { loginWithRedirect, isAuthenticated, user, isLoading } = useAuth0();
    const isDesktopVariant = useMediaQuery('(min-width: 960px)');
    const signInTextRef = useRef<HTMLSpanElement>(null);
    const { pathname, query, push } = useRouter();
    const [isOpenVerificationModal, setOpenVerificationModal] = useState(true);

    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const chevronIcon = isDropdownOpen ? 'direction-up' : 'direction-down';
    const { account } = useAccount();
    const dispatch = useDispatch();
    const userAgent = typeof window !== 'undefined' && navigator?.userAgent;
    const { correlationId } = useSelectedSell();

    useEffect(() => {
        if (account || correlationId) {
            dispatch({
                type: GTM_USER_ID,
                payload: {
                    userID: account ? account.idpCustomerId : correlationId,
                    isLoggedIn: isAuthenticated,
                    userAgent,
                },
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account, correlationId]);

    const { offers, totalCount: rewardsCount, rewardsActivityHistory } = useRewards();
    const { loyalty, error: loyaltyError } = useLoyalty();

    const isShowRewardsInfo = account && !loyaltyError && isRewardsOn();

    const rewardsInfo = useMemo(() => {
        const infoItems: string[] = [];

        if (loyalty?.pointsBalance) {
            infoItems.push(`${loyalty?.pointsBalance} pts`);
        }

        if (rewardsCount) {
            infoItems.push(`${rewardsCount} ${rewardsCount > 1 ? 'rewards' : 'reward'}`);
        }

        return infoItems.join('  |  ');
    }, [rewardsCount, loyalty?.pointsBalance]);

    const SignInIcon = () => (
        <div className={styles.signInIconWrapper}>
            <Icon icon="navigation-user" size="m" className={styles.signInIcon}>
                {!!(loyalty?.pointsBalance || rewardsCount) && <div className={styles.iconDot} />}
            </Icon>
        </div>
    );

    const {
        actions: { enqueueError },
    } = useNotifications();

    const navigationLinks = useMemo(
        () =>
            getAccountNavLinks(props.userAccountMenu, rewardsActivityHistory.length, configuration).map((item) => {
                if (item.type === DEALS) {
                    return {
                        ...item,
                        count: offers.filter((offer) => offer.status === TOfferStatusModel.Open).length,
                    };
                }

                if (item.type === REWARDS) {
                    return {
                        ...item,
                        count: rewardsCount,
                    };
                }

                return item;
            }),
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [offers]
    );

    const isAccountLoading = isLoading || (user && !account);
    if (isAccountLoading) {
        return <Loader size={20} className={styles.signInLoading} />;
    }
    const handleSignInButtonClick = () => {
        if (!isAuthenticated) {
            try {
                loginWithRedirect({ appState: { target: window.location.pathname } });
            } catch ({ message }) {
                enqueueError({ message });
            }
            return;
        }
        return setIsDropdownOpen(!isDropdownOpen);
    };

    const handleNavigationDropdownClose = () => setIsDropdownOpen(false);

    const signInTextClassNames = classnames('truncate', styles.signInText, {
        [styles.notAuthenticated]: !account,
        [styles.isAuthenticated]: account,
    });

    const getSignInText = (name: string) => `Hi ${name}`;
    const onCloseEmailVerificationModal = () => {
        setOpenVerificationModal(false);
    };
    const redirectRewardsPage = () => {
        push(`${location.origin}/rewards`);
        setOpenVerificationModal(false);
    };
    const hasQueryEmailVerification = query && Object.keys(query).includes('isEmailVerification');

    return (
        <div className={styles.signInContainer}>
            <div
                tabIndex={0}
                className={styles.signInButton}
                onKeyPress={handleSignInButtonClick}
                onClick={handleSignInButtonClick}
                data-gtm-id="topNav_signin"
            >
                <SignInIcon />
                <div className={styles.accountInfoContaner}>
                    <>
                        <span className={signInTextClassNames} ref={signInTextRef}>
                            {account && user ? getSignInText(account.firstName) : 'Sign In'}
                        </span>
                        {account && <Icon className={styles.chevronIcon} icon={chevronIcon} size="tiny" />}
                    </>
                    {isShowRewardsInfo && isDesktopVariant && (
                        <div className={classnames('t-paragraph-hint', styles.rewardsInfo)}>{rewardsInfo}</div>
                    )}
                </div>
            </div>
            <div className={styles.divider} />
            {account && (
                <NavigationDropdown
                    open={isDropdownOpen}
                    onClose={handleNavigationDropdownClose}
                    fullScreen
                    className={styles.dropdownContent}
                    controlRef={signInTextRef}
                    navRef={props.navRef}
                >
                    <div className={styles.menuContent}>
                        {!isDesktopVariant && (
                            <>
                                <CloseButton onClick={handleNavigationDropdownClose} />
                                <div className={styles.dropdownSignInSection}>
                                    <div className={styles.wrapperDropdownSignIn}>
                                        <div>
                                            <SignInIcon />
                                        </div>
                                        <span className={classnames(styles.signInText, styles.dropdownSignInText)}>
                                            <span>{getSignInText(account.firstName)}</span>{' '}
                                            {isShowRewardsInfo && (
                                                <span
                                                    className={classnames(
                                                        't-paragraph-small',
                                                        styles.rewardsInfoMobile
                                                    )}
                                                >
                                                    {rewardsInfo}
                                                </span>
                                            )}
                                        </span>
                                    </div>
                                </div>
                            </>
                        )}
                        {navigationLinks.map(({ link, name, count, trackingId }) => (
                            <div key={name} className={styles.itemContainer}>
                                <Link key={name} href={link}>
                                    <a
                                        key={name}
                                        className={classnames(styles.accountMenulink, {
                                            't-paragraph-small-strong': isDesktopVariant,
                                            't-subheader-small': !isDesktopVariant,
                                        })}
                                        tabIndex={0}
                                        onClick={
                                            pathname === link && !isDesktopVariant && handleNavigationDropdownClose
                                        }
                                        {...(trackingId && { 'data-gtm-id': trackingId })}
                                    >
                                        {name}
                                        {!!count && (
                                            <Counter
                                                value={count}
                                                counterClassName={styles.counter}
                                                ariaLabel={`${name.toLowerCase()} counter`}
                                            />
                                        )}
                                    </a>
                                </Link>
                            </div>
                        ))}
                        <div className={styles.horizontalDivider} />
                        <LogoutButton className={styles.logoutButton} />
                    </div>
                </NavigationDropdown>
            )}
            {hasQueryEmailVerification && (
                <SuccessErrorModal
                    open={isOpenVerificationModal}
                    isSuccess={!!query?.isEmailVerification}
                    onButtonClick={query?.isEmailVerification && redirectRewardsPage}
                    onClose={onCloseEmailVerificationModal}
                    description={
                        query?.isEmailVerification
                            ? 'You are able to redeem certificates for getting free products'
                            : 'Please try again later'
                    }
                    title={query?.isEmailVerification ? 'Account was verified!' : 'Account was not verified'}
                    closeButtonText={query?.isEmailVerification && 'Go to rewards roster'}
                />
            )}
        </div>
    );
};

export default SignIn;
