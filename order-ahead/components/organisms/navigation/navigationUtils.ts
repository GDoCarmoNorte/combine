import { useLocationOrderAheadAvailability } from '../../../common/hooks/useLocationOrderAheadAvailability';

export const CheckLocation = (): boolean => {
    const { isLocationOrderAheadAvailable } = useLocationOrderAheadAvailability();
    return isLocationOrderAheadAvailable;
};
