import React, { ReactEventHandler } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import classnames from 'classnames';

import {
    IDocumentLink,
    IExternalLink,
    IMenuCategoryLink,
    IPageLink,
    IPhoneNumberLink,
    IProduct,
} from '../../../@generated/@types/contentful';

import isRelativeLink from '../../../lib/isRelativeLink';

import styles from './index.module.css';
import { getLinkDetails } from '../../../lib/link';
import { useGlobalProps } from '../../../redux/hooks';

interface INavigationLinkProps {
    link: IExternalLink | IMenuCategoryLink | IPageLink | IProduct | IPhoneNumberLink | IDocumentLink;
    onClick?: ReactEventHandler;
}

const NavigationLink = (props: INavigationLinkProps): JSX.Element => {
    const { link, onClick } = props;
    const router = useRouter();
    const globalProps = useGlobalProps();

    if (!link.fields) return null;
    const { href, name } = getLinkDetails(link, { productDetailsPagePaths: globalProps?.productDetailsPagePaths });

    const linkStyle = classnames(styles.link, {
        [styles.linkActive]: href.split('/')[1] === router.asPath.split('/')[1],
    });

    // TODO: replace with other solution
    if (isRelativeLink(href)) {
        return (
            <Link key={href} href={href}>
                <a
                    className={linkStyle}
                    onClick={onClick}
                    data-gtm-id={`main-nav-${name?.replace(/\s+/g, '-').toLowerCase()}`}
                >
                    <span className={styles.linkLabel}>{name?.toUpperCase()}</span>
                </a>
            </Link>
        );
    }
    return (
        <a
            key={href}
            href={href}
            className={styles.link}
            onClick={onClick}
            data-gtm-id={`main-nav-${name?.toLowerCase()}`}
        >
            <span className={styles.linkLabel}>{name?.toUpperCase()}</span>
        </a>
    );
};

export default NavigationLink;
