import React, { SyntheticEvent } from 'react';
import classnames from 'classnames';

import InspirePhoneLink from '../../../../../../atoms/phoneLink';
import InspireTooltip from '../../../../../../atoms/Tooltip';
import { InspireLink, InspireLinkButton } from '../../../../../../atoms/link';
import { InspireButton } from '../../../../../../atoms/button';
import TimeSlots from '../../../../../../organisms/timeSlots';
import PickupLocationFind from '../../../pickupLocationFind';

import styles from './locationMethodTab.module.css';

import { UsePickupTabHook } from '../../../../../../../common/hooks/usePickupTab';
import { OrderLocationMethod } from '../../../../../../../redux/orderLocation';
import { locationTimeSlotsEnabled } from '../../../../../../../lib/getFeatureFlags';
import { SHOW_UNAVAILABLE, UNAVAILABLE_MESSAGE } from './constants';

interface ILocationMethodTabProps extends UsePickupTabHook {
    method: OrderLocationMethod;
    handleStartOrderClick: () => void;
    handleChangeLocationClick: (e: SyntheticEvent) => void;
    closeDropdown: () => void;
}

const LocationMethodTab = (props: ILocationMethodTabProps): JSX.Element => {
    const {
        method,
        title,
        displayName,
        detailsPageUrl,
        address,
        phone,
        storeStatus,
        changeLocationText,
        ctaTooltip,
        ctaText,
        handleStartOrderClick,
        handleChangeLocationClick,
        closeDropdown,
    } = props;

    if (!address) {
        return (
            <div className={styles.methodTabWrapper}>
                <PickupLocationFind method={method} closeDropdown={closeDropdown} />
            </div>
        );
    }

    const isLocationTimeSlotsFeatureEnabled = locationTimeSlotsEnabled();

    return (
        <div className={styles.methodTabWrapper}>
            <div className={styles.backgroundLine} />
            <div className={styles.detailsContainer}>
                <div className={classnames('t-subheader-small', styles.title)}>{title}</div>
                {displayName && (
                    <div
                        title={displayName}
                        className={classnames(
                            't-subheader-smaller',
                            {
                                [styles.withLink]: detailsPageUrl,
                                'truncate-at-2': !detailsPageUrl,
                            },
                            styles.displayName
                        )}
                    >
                        {detailsPageUrl ? (
                            <InspireLink
                                link={detailsPageUrl}
                                newtab={false}
                                onClick={(e) => {
                                    e.stopPropagation();
                                }}
                                className={classnames({ 'truncate-at-2': detailsPageUrl })}
                            >
                                {displayName}
                            </InspireLink>
                        ) : (
                            displayName
                        )}
                    </div>
                )}
                <p title={address} className={classnames('truncate', 't-paragraph-small', styles.address)}>
                    {address}
                </p>
                {phone && <InspirePhoneLink className={styles.phoneLink} variant="secondary" phone={phone} />}
                {/* TODO: Show after fix DBBP-12223
                    <span>{!!roundedDistance && `${roundedDistance} mi`}</span>
                    {!!roundedDistance && hours && <span>&nbsp; • &nbsp;</span>}
                */}
                <p className={classnames('t-paragraph-hint', styles.statusText)} title={'Store Status and Hours'}>
                    {storeStatus || (SHOW_UNAVAILABLE ? UNAVAILABLE_MESSAGE : null)}
                </p>
                <InspireLinkButton
                    className={styles.changeLocationButton}
                    onClick={handleChangeLocationClick}
                    linkType="secondary"
                >
                    {changeLocationText}
                </InspireLinkButton>
            </div>
            {isLocationTimeSlotsFeatureEnabled && <TimeSlots method={method} />}
            {ctaTooltip ? (
                <InspireTooltip title={ctaTooltip} placement="top" theme="light" arrow>
                    <div className={styles.startOrderCtaWrapper}>
                        <InspireButton type="primary" text={ctaText} onClick={handleStartOrderClick} fullWidth />
                    </div>
                </InspireTooltip>
            ) : (
                <div className={styles.startOrderCtaWrapper}>
                    <InspireButton type="primary" text={ctaText} onClick={handleStartOrderClick} fullWidth />
                </div>
            )}
        </div>
    );
};

export default LocationMethodTab;
