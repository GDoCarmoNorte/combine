import React, { useEffect, useState } from 'react';

import InspireTooltip from '../../../atoms/Tooltip';
import Icon from '../../../atoms/BrandIcon';
import Counter from '../../../atoms/counter';

import { useBag } from '../../../../redux/hooks';

import styles from './bag.module.css';
import navStyles from '../index.module.css';
import { TOOLTIP_THEME } from './constants';
import useBagAvailableItemsCounter from '../../../../common/hooks/useBagAvailableItemsCounter';

const BagToolTipText = () => {
    const { entryCreated, entryUpdated, entryCreatedWithDeal, entryUpdatedWithDeal } = useBag();
    const [tooltipTitle, setTooltipTitle] = useState('');

    useEffect(() => {
        let title = tooltipTitle;
        if (entryCreated) {
            title = 'Item added to bag';
        }

        if (entryCreatedWithDeal) {
            title = 'Deal added to bag';
        }

        if (entryUpdatedWithDeal) {
            title = 'Deal updated';
        }

        if (entryUpdated) {
            title = 'Item updated';
        }

        setTooltipTitle(title);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [entryCreated, entryUpdated, entryCreatedWithDeal, entryUpdatedWithDeal]);

    return (
        <div>
            <Icon icon="action-check" variant="colorful" className={styles.checkIconBagTooltip} />
            {tooltipTitle}
        </div>
    );
};

const Bag = (): JSX.Element => {
    const {
        bagEntriesCount,
        tooltipOpen,
        entryUpdated,
        actions: { updateTooltip },
        isOpen,
        dealId,
    } = useBag();

    const [timer, setTimer] = useState(null);

    useEffect(() => {
        clearTimeout(timer);
        if (isOpen) {
            updateTooltip({ tooltipIsOpen: false });
        } else {
            setTimer(
                setTimeout(() => {
                    updateTooltip({ tooltipIsOpen: false });
                }, 2000)
            );
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [bagEntriesCount, isOpen, entryUpdated, dealId]);

    const bagAvailableCount = useBagAvailableItemsCounter();
    const bagItemsCount = bagAvailableCount < 10 ? bagAvailableCount : '9+';

    return (
        <InspireTooltip
            tooltipClassName={styles.tooltip}
            open={tooltipOpen}
            disableFocusListener
            disableHoverListener
            disableTouchListener
            title={<BagToolTipText />}
            placement="bottom"
            theme={TOOLTIP_THEME}
            arrow
        >
            <div className={`${navStyles.icon} ${styles.bagIcon}`} role="button" aria-label="Open bag">
                <Icon icon="navigation-bag" className="light" />
                {bagItemsCount > 0 && (
                    <Counter ariaLabel="bag counter" counterClassName={styles.bagCounter} value={bagItemsCount} />
                )}
            </div>
        </InspireTooltip>
    );
};

export default Bag;
