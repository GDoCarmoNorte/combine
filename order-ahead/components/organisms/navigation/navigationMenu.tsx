import React from 'react';
import { Modal, Button } from '@material-ui/core';

import Icon from '../../atoms/BrandIcon';
import StartOrderButton from '../../atoms/startOrderButton/startOrderButton';
import CloseButton from '../../atoms/CloseButton/closeButton';

import styles from './index.module.css';
import {
    IDocumentLink,
    IExternalLink,
    IMenuCategoryLink,
    IPageLink,
    IPhoneNumberLink,
    IProduct,
} from '../../../@generated/@types/contentful';
import NavigationLink from './navigationLink';
import { useBag } from '../../../redux/hooks';
import { CheckLocation } from './navigationUtils';

interface INavigationMenuProps {
    open?: boolean;
    links?: (IExternalLink | IMenuCategoryLink | IPageLink | IProduct | IPhoneNumberLink | IDocumentLink)[];
    onClose?: () => void;
}

const NavigationMenu = (props: INavigationMenuProps): JSX.Element => {
    const { actions } = useBag();
    const toggleBagOpen = () => actions.toggleIsOpen({ isOpen: true });
    const location = CheckLocation();

    return (
        <Modal open={props.open} hideBackdrop>
            <div className={styles.navigationMenu}>
                <CloseButton onClick={props.onClose} />
                <div className={styles.menuLinkContainer}>
                    {props.links &&
                        props.links.map((link) => (
                            <NavigationLink key={link.sys.id} link={link} onClick={props.onClose} />
                        ))}
                </div>
                {location && (
                    <>
                        <div className={styles.menuActionContainer}>
                            <Button classes={{ root: styles.menuActionButton }} onClick={toggleBagOpen}>
                                <Icon icon="navigation-bag" className={styles.menuActionBagIcon} />
                                Shopping Bag
                            </Button>
                        </div>
                        <StartOrderButton className={styles.startOrderButton} onClick={props.onClose} />
                    </>
                )}
            </div>
        </Modal>
    );
};

export default NavigationMenu;
