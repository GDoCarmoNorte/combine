import React from 'react';
import styles from './certificatesSectionLayout.module.css';
import classnames from 'classnames';

export interface ICertificatesSectionLayoutProps {
    bgImageUrl: string;
    headingFirstLineText: string;
    headingSecondLineText: string;
    headingThirdLineText: string;
    children: JSX.Element;
}

const CertificatesSectionLayout = ({
    bgImageUrl,
    headingFirstLineText,
    headingSecondLineText,
    headingThirdLineText,
    children,
}: ICertificatesSectionLayoutProps): JSX.Element => {
    return (
        <>
            <div className={classnames('wrapper-with-background-image', styles.certificatesSectionLayout)}>
                <div className={styles.titleBlock}>
                    <h2 className={classnames('t-header-h1', styles.sectionHeading)}>
                        <span className="t-subheader">{headingFirstLineText}</span>
                        <span className={classnames('m-block-treatment', 'm-block-treatment-md')}>
                            <span className="m-block-treatment-title">
                                {headingSecondLineText}
                                {<br />} {headingThirdLineText}
                            </span>
                        </span>
                    </h2>
                </div>

                {children}
            </div>
            <style jsx>{`
                .wrapper-with-background-image {
                    background-image: ${bgImageUrl
                        ? `url("${bgImageUrl}"), linear-gradient(#e3e1db 70%, #fff 70%)`
                        : 'linear-gradient(#e3e1db 70%, #fff 70%)'};
                }
            `}</style>
        </>
    );
};

export default CertificatesSectionLayout;
