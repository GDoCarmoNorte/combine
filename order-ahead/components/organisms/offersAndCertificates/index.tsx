import React, { useState } from 'react';
import styles from './index.module.css';
import classnames from 'classnames';
import OfferItem from './offerItem/offerItem';
import CertificateItem from './certificateItem/certificateItem';
import useRewards from '../../../redux/hooks/useRewards';
import BrandLoader from '../../atoms/BrandLoader';

const OffersAndCertificates = (): JSX.Element => {
    const { certificatesSortedByStatus, offers } = useRewards();
    const [isLoading, setIsLoading] = useState(false);
    if (!offers.length && !certificatesSortedByStatus.length) return null;

    return (
        <div className={styles.container}>
            <h2 className={classnames('t-header-h2', styles.blockTitle)}>My Rewards</h2>
            {offers.map((offer) => (
                <OfferItem key={offer.id} offer={offer} onLoadingToggle={setIsLoading} />
            ))}
            {certificatesSortedByStatus.map((certificate) => (
                <CertificateItem key={certificate.number} certificate={certificate} onLoadingToggle={setIsLoading} />
            ))}
            {isLoading && <BrandLoader className={styles.loader} />}
        </div>
    );
};

export default OffersAndCertificates;
