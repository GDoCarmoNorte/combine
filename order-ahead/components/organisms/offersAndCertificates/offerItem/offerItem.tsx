import React, { useState } from 'react';
import { format } from '../../../../common/helpers/dateTime';
import RewardItem from '../../../atoms/rewardItem';
import RewardDetailsModal from '../../../molecules/rewardDetailsModal';
import { useRewardsService } from '../../../../common/hooks/useRewardsService';
import {
    IActivateOfferModelModelOperationEnum,
    IInactiveOfferModel,
    TRewardOfferStatusModel,
    IOfferModel,
} from '../../../../@generated/webExpApi';
import { useNotifications, useRewards } from '../../../../redux/hooks';

import { InspireButton } from '../../../atoms/button';
import { OfferView } from '../../../molecules/rewardDetailsModal/offerView/offerView';
import styles from '../../../molecules/rewardDetailsModal/rewardDetailsModal.module.css';
import { useSafeSetTimeout } from '../../../../common/hooks/useSafeSetTimeout';
import { spaces2underscores } from '../../../../lib/gtm';
import { useGtmErrorEvent } from '../../../../common/hooks/useGtmErrorEvent';
import { GtmErrorCategory } from '../../../../common/services/gtmService/types';

interface IOfferItemProps {
    offer: IInactiveOfferModel | IOfferModel;
    onLoadingToggle: (state: boolean) => void;
}

const OfferItem = (props: IOfferItemProps): JSX.Element => {
    const {
        offer: { name, startDateTime, endDateTime, terms, imageUrl, id, code, status },
        onLoadingToggle,
    } = props;

    const rewardsService = useRewardsService();
    const safeSetTimeout = useSafeSetTimeout();
    const {
        actions: { setRewardStatus },
    } = useRewards();
    const { pushGtmErrorEvent } = useGtmErrorEvent();
    const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
    const [isModalLoading, setIsModalLoading] = useState<boolean>(false);
    const [notificationIsOpen, setNotificationIsOpen] = useState<boolean>(false);
    const dateLabel = 'Limited Time Offer';
    const date = `${format(new Date(startDateTime), 'M/d')}-${format(new Date(endDateTime), 'M/d/yy')}`;
    const dateText = `valid ${date}`;
    const dateTextForModal = `Valid between ${date}`;
    const isActiveOffer = status === TRewardOfferStatusModel.Active;
    const activeCTAText = !isActiveOffer && 'Activate Offer';
    const defaultCTAText = isActiveOffer && 'Active Offer';
    const nameNoSpaces = spaces2underscores(name);
    const activeCTAGtmId = `CTA-RewardsDeals-Activate_Offer_Confirm-${nameNoSpaces}`;
    const defaultCTAGtmId = `CTA-RewardsDeals-Activate_Offer-${nameNoSpaces}`;
    const notificationTitle = 'Your Offer is currently ACTIVE';
    const notificationDescription = 'Click view details to know more about your offer.';
    const notificationDelay = 5000;

    const {
        actions: { enqueueError },
    } = useNotifications();

    const onNotificationToggle = () => {
        setNotificationIsOpen(true);

        safeSetTimeout(() => setNotificationIsOpen(false), notificationDelay);
    };

    const onActivate = async () => {
        try {
            await rewardsService.activateOffer({
                rewardId: id,
                code,
                operation: IActivateOfferModelModelOperationEnum.Activate,
            });
            setRewardStatus({ rewardId: id, status: TRewardOfferStatusModel.Active });
        } catch {
            const message = "We couldn't activate your offer. Please try again later.";
            enqueueError({ message });
            pushGtmErrorEvent({
                ErrorCategory: GtmErrorCategory.REWARDS,
                ErrorDescription: message,
            });
        }
    };

    const onActivateFromModal = async () => {
        setIsModalLoading(true);
        try {
            await onActivate();
        } finally {
            setIsModalLoading(false);
        }
    };

    const onActivateFromListing = async () => {
        onLoadingToggle(true);
        await onActivate();
        setNotificationIsOpen(true);
        onLoadingToggle(false);

        safeSetTimeout(() => setNotificationIsOpen(false), notificationDelay);
    };

    const onModalOpen = () => {
        setIsOpenModal(true);
    };

    const onModalClose = () => {
        setIsOpenModal(false);
    };

    const renderButtons = () => {
        if (isActiveOffer) {
            return (
                <InspireButton
                    ariaLabel={'back button'}
                    className={styles.button}
                    onClick={onModalClose}
                    type="primary"
                    gtmId={`CTA-RewardsDeals-Back_To_Rewards-${nameNoSpaces}`}
                    text="BACK TO REWARDS"
                />
            );
        }
        return (
            <InspireButton
                ariaLabel={'activate button'}
                className={styles.button}
                onClick={onActivateFromModal}
                type="primary"
                gtmId={`CTA-RewardsDeals-Activate_Offer_Confirm-${nameNoSpaces}`}
                text="ACTIVATE OFFER"
            />
        );
    };

    return (
        <>
            <RewardItem
                title={name}
                imageUrl={imageUrl}
                label={dateLabel}
                labelText={dateText}
                activeCTAText={activeCTAText}
                defaultCTAText={defaultCTAText}
                activeCTAGtmId={activeCTAGtmId}
                defaultCTAGtmId={defaultCTAGtmId}
                onViewDetails={onModalOpen}
                notificationTitle={notificationTitle}
                notificationDescription={notificationDescription}
                onActiveCTAClick={onActivateFromListing}
                onNotificationToggle={onNotificationToggle}
                onNotificationClose={setNotificationIsOpen}
                notificationOpen={notificationIsOpen}
            />
            <RewardDetailsModal
                open={isOpenModal}
                modalTitle="OFFER DETAILS"
                onClose={onModalClose}
                isLoading={isModalLoading}
                renderButtons={renderButtons}
            >
                <OfferView
                    title={name}
                    dateLabel={dateLabel}
                    dateText={dateTextForModal}
                    imageUrl={imageUrl}
                    terms={terms}
                    isActive={isActiveOffer}
                />
            </RewardDetailsModal>
        </>
    );
};

export default OfferItem;
