import React, { useState } from 'react';
import { format } from '../../../../common/helpers/dateTime';
import classnames from 'classnames';
import RewardItem from '../../../atoms/rewardItem';
import RewardDetailsModal from '../../../molecules/rewardDetailsModal';
import { InspireButton } from '../../../atoms/button';
import { ICertificateModel, TCertificateStatusModel } from '../../../../@generated/webExpApi';
import { useRewardsService } from '../../../../common/hooks/useRewardsService';
import { useNotifications, useRewards } from '../../../../redux/hooks';
import { useSafeSetTimeout } from '../../../../common/hooks/useSafeSetTimeout';

import { CertificateView } from '../../../molecules/rewardDetailsModal/certificateView/certificateView';
import styles from '../index.module.css';
import { spaces2underscores } from '../../../../lib/gtm';
import { useGtmErrorEvent } from '../../../../common/hooks/useGtmErrorEvent';
import { GtmErrorCategory } from '../../../../common/services/gtmService/types';

interface ICertificateProps {
    certificate: ICertificateModel;
    onLoadingToggle?: (state: boolean) => void;
}

const useCancelCertificate = (certificateId: string) => {
    const rewardsService = useRewardsService();
    const { actions: rewardsActions } = useRewards();
    const { actions: notificationsActions } = useNotifications();
    const { pushGtmErrorEvent } = useGtmErrorEvent();
    const cancelCertificate = async () => {
        try {
            await rewardsService.cancelCertificate({
                rewardId: certificateId,
            });
            rewardsActions.setRewardStatus({
                rewardId: certificateId,
                status: TCertificateStatusModel.PendingCancellation,
            });
        } catch {
            const message = "We couldn't cancel your reward. Please try again later.";
            notificationsActions.enqueueError({ message });
            pushGtmErrorEvent({
                ErrorCategory: GtmErrorCategory.REWARDS,
                ErrorDescription: message,
            });
        }
    };

    return { cancelCertificate };
};

const CertificateItem = (props: ICertificateProps): JSX.Element => {
    const {
        certificate: { number, title, expirationDateTime, imageUrl, priceInPoints, status },
        onLoadingToggle = () => null,
    } = props;
    const formattedExpirationDate = format(new Date(expirationDateTime), 'MM/dd/yyyy');
    const expirationLabel = `Expires ${formattedExpirationDate}.`;
    const isActive = status === TCertificateStatusModel.Active;
    const isPendingCancellation = status === TCertificateStatusModel.PendingCancellation;
    const isFree = priceInPoints === 0;
    const canRefund = !isFree && !isPendingCancellation;
    const activeCTAText = canRefund && 'Refund Points';
    const defaultCTAText = isPendingCancellation && 'Refund Pending';
    const nameNoSpaces = spaces2underscores(title);
    const gtmCTARefundPoints = `CTA-RewardsDeals-Refund_Points-${nameNoSpaces}`;
    const gtmCTAOnlineOrder = `CTA-RewardsDeals-Order_Online-${nameNoSpaces}`;
    const [modalIsLoading, setModalIsLoading] = useState(false);
    const productLink = '/menu';
    const notificationDelay = 5000;

    const { cancelCertificate } = useCancelCertificate(number);
    const safeSetTimeout = useSafeSetTimeout();

    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isNotificationOpen, setIsNotificationOpen] = useState(false);

    const onNotificationToggle = () => {
        setIsNotificationOpen(true);

        safeSetTimeout(() => setIsNotificationOpen(false), notificationDelay);
    };

    const openDetailsModal = () => {
        setIsModalOpen(true);
    };

    const closeDetailsModal = () => {
        setIsModalOpen(false);
    };

    const handleCancelCertificateFromListView = async () => {
        onLoadingToggle(true);
        await cancelCertificate();
        onLoadingToggle(false);
        setIsNotificationOpen(true);

        safeSetTimeout(() => setIsNotificationOpen(false), notificationDelay);
    };

    const handleCancelCertificateFromDetailsView = async () => {
        setModalIsLoading(true);
        await cancelCertificate();
        setModalIsLoading(false);
    };

    const renderButtons = () => {
        const primaryButtonLabel = isPendingCancellation ? 'BACK TO REWARDS' : 'ORDER ONLINE';
        const primaryButtonLink = isPendingCancellation ? undefined : productLink;
        const primaryButtonClickHandler = isPendingCancellation ? () => setIsModalOpen(false) : undefined;

        const primaryButtonProps = {
            text: primaryButtonLabel,
            link: primaryButtonLink,
            onClick: primaryButtonClickHandler,
            ...(!isPendingCancellation && { gtmId: gtmCTAOnlineOrder }),
        };

        return (
            <>
                {canRefund && (
                    <InspireButton
                        className={classnames(styles.button, styles.secondary)}
                        type="secondary"
                        text="REFUND POINTS"
                        gtmId={gtmCTARefundPoints}
                        onClick={handleCancelCertificateFromDetailsView}
                    />
                )}
                <InspireButton className={styles.button} {...primaryButtonProps} />
            </>
        );
    };

    return (
        <>
            <RewardItem
                title={title}
                imageUrl={imageUrl}
                label={expirationLabel}
                activeCTAText={activeCTAText}
                onViewDetails={openDetailsModal}
                defaultCTAText={defaultCTAText}
                activeCTAGtmId={gtmCTARefundPoints}
                notificationOpen={isNotificationOpen}
                notificationTitle="Your Reward is currently PENDING"
                notificationDescription="Your point total will be updated to reflect points refund within 24 hours"
                onNotificationToggle={onNotificationToggle}
                onNotificationClose={setIsNotificationOpen}
                onActiveCTAClick={handleCancelCertificateFromListView}
            />
            <RewardDetailsModal
                open={isModalOpen}
                onClose={closeDetailsModal}
                modalTitle="Reward Details"
                renderButtons={renderButtons}
                isLoading={modalIsLoading}
            >
                <CertificateView
                    isPendingCancellation={isPendingCancellation}
                    isActive={isActive}
                    imageUrl={imageUrl}
                    title={title}
                    expirationLabel={expirationLabel}
                />
            </RewardDetailsModal>
        </>
    );
};

export default CertificateItem;
