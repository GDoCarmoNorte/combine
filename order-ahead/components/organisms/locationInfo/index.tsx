import React from 'react';

import styles from './locationInfo.module.css';
import { LocationHours } from './locationHours';
import { Entry } from 'contentful';
import { LocationServices } from './locationServices';
import { ILocationByStateOrProvinceDetailsModel } from '../../../@generated/webExpApi';
import { IImageAssetFields } from '../../../@generated/@types/contentful';
import sectionIndentsStyles from '../../sections/sectionIndents.module.css';

export interface ILocationInfoProps {
    locationDetails: ILocationByStateOrProvinceDetailsModel;
    image?: Entry<IImageAssetFields>;
}

export const LocationInfo = ({
    locationDetails: { services, hoursByDay, timezone },
    image,
}: ILocationInfoProps): JSX.Element => {
    const imageUrl = image?.fields.image?.fields.file.url;
    return (
        <div className={sectionIndentsStyles.wrapper}>
            <div className={styles.container}>
                <div className={styles.locationInfoCard}>
                    <LocationHours hoursByDay={hoursByDay} timezone={timezone} />
                    <LocationServices services={services} />
                </div>
                {imageUrl && <div style={{ backgroundImage: `url(${imageUrl})` }} className={styles.locationImage} />}
            </div>
        </div>
    );
};
