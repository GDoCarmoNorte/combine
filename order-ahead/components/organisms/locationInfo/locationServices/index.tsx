import React from 'react';
import styles from './locationServices.module.css';
import { IServiceTypeModel } from '../../../../@generated/webExpApi';
import { LocationServicesModel } from '../../../../common/constants/locations';
import classnames from 'classnames';

export interface ILocationServicesProps {
    services: Array<IServiceTypeModel>;
}

export const LocationServices = ({ services }: ILocationServicesProps): JSX.Element => (
    <div className={styles.locationServices}>
        <h2 className={classnames('t-header-h3', styles.locationHeader)}>Services</h2>

        {services ? (
            <ul className={styles.servicesList}>
                {services.map((service, index) => (
                    <li className={styles.servicesItem} key={`${index} ${service}`}>
                        <p className={classnames('t-subheader-universal-smaller', styles.servicesTitle)}>
                            {LocationServicesModel[service]}
                        </p>
                    </li>
                ))}
            </ul>
        ) : null}
    </div>
);
