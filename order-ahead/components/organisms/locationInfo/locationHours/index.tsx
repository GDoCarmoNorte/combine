import React, { useMemo } from 'react';
import classnames from 'classnames';

import styles from './locationHours.module.css';
import { convertHours } from '../../../../common/helpers/convertHours';
import { LocationsDaysModel } from '../../../../common/constants/locations';
import { ILocationServiceHoursModel } from '../../../../@generated/webExpApi';
import { getStoreStatus } from '../../../../common/helpers/getStoreStatus';

export interface ILocationHoursProps {
    hoursByDay: { [key: string]: ILocationServiceHoursModel };
    timezone: string;
}

export const LocationHours = ({ hoursByDay, timezone }: ILocationHoursProps): JSX.Element => {
    const today = useMemo(() => Object.keys(LocationsDaysModel)[new Date().getDay()], []);
    const { statusText, isClosingSoon } = useMemo(() => getStoreStatus(hoursByDay, timezone), [hoursByDay, timezone]);

    return (
        <div className={styles.locationHours}>
            <h2 className={classnames(styles.locationHeader, 't-header-h3')}>Store Hours</h2>
            <p
                className={classnames(
                    styles.storeStatus,
                    't-paragraph-hint',
                    isClosingSoon && styles.storeStatusClosingSoon
                )}
            >
                {statusText}
            </p>

            {hoursByDay ? (
                <ul className={styles.hoursDetailsList}>
                    {Object.entries(hoursByDay).map(([day, timeData], index) => {
                        const { start, end } = timeData;
                        const hoursDetailsItemStyles =
                            day === today
                                ? classnames(styles.hoursDetailsItem, styles.hoursDetailsItemSelected)
                                : styles.hoursDetailsItem;
                        const convertedStart = convertHours(start);
                        const convertedEnd = convertHours(end);

                        return (
                            <li className={hoursDetailsItemStyles} key={`${day} ${index}`}>
                                <p className={classnames('t-subheader-small', styles.day)}>{LocationsDaysModel[day]}</p>
                                <p className={styles.time}>
                                    {convertedStart} - {convertedEnd}
                                </p>
                            </li>
                        );
                    })}
                </ul>
            ) : null}
        </div>
    );
};
