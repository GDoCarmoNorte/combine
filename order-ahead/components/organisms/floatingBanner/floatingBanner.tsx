import React, { useEffect, useState } from 'react';
import classnames from 'classnames';

import { FloatingBannerProps } from './types';
import styles from './floatingBanner.module.css';
import { InspireButton } from '../../atoms/button';
import { useLocalization } from '../../../common/hooks/useLocalization';
import ContentfulImage from '../../atoms/ContentfulImage';
import TextWithTrademark from '../../atoms/textWithTrademark';
import { ProductItemPriceAndCalories } from '../../atoms/productItemPriceAndCalories';
import { IMAGE_MAX_WIDTH } from './constants';
import { useProductIsSaleable } from '../../../common/hooks/useProductIsSaleable';
import { NOT_SALEABLE_BUTTON_TEXT, NOT_SALEABLE_BUTTON_LINK } from '../../../common/constants/product';
import { useProductIsAvailable } from '../../../common/hooks/useProductIsAvailable';
import { useConfiguration } from '../../../redux/hooks';

function FloatingBanner(props: FloatingBannerProps): JSX.Element {
    const [showBanner, setShowBanner] = useState(props.showInitially || false);
    const { offset, onAddToBag, product, productInfo, domainProduct, addToBagBtnText } = props;
    const productId = domainProduct?.id;

    const { locationLinkText } = useLocalization(productId);
    const { isSaleable } = useProductIsSaleable(productId);
    const { isAvailable } = useProductIsAvailable(productId);
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    useEffect(() => {
        window &&
            window.document.addEventListener('scroll', () => {
                window.scrollY >= offset ? setShowBanner(true) : setShowBanner(false);
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const renderButtons = () => {
        if (!isSaleable || !isAvailable || !isOAEnabled) {
            return (
                <InspireButton
                    disabled={!isOAEnabled}
                    link={NOT_SALEABLE_BUTTON_LINK}
                    type="primary"
                    text={NOT_SALEABLE_BUTTON_TEXT}
                />
            );
        }

        if (locationLinkText) return <InspireButton link={'/locations'} type="primary" text={locationLinkText} />;

        return (
            <InspireButton
                onClick={onAddToBag}
                type="primary"
                text={addToBagBtnText}
                disabled={props.addToBagBtnDisabled}
            />
        );
    };

    return (
        showBanner && (
            <div className={styles.container} id="floating-banner">
                <div className={styles.content}>
                    <div className={styles.bannerImageContainer}>
                        <ContentfulImage maxWidth={IMAGE_MAX_WIDTH} asset={product.fields.image} />
                    </div>
                    <div className={styles.descriptionContainer}>
                        <TextWithTrademark
                            tag="p"
                            text={productInfo.name || domainProduct?.name || product.fields.name}
                            className={classnames('t-subheader', styles.bannerHeader)}
                        />
                        <div className={styles.priceContainer}>
                            <ProductItemPriceAndCalories price={productInfo.price} calories={productInfo.calories} />
                        </div>
                    </div>
                    {renderButtons()}
                </div>
            </div>
        )
    );
}

export default FloatingBanner;
