import { ItemModel } from '../../../@generated/webExpApi';
import { IProductFields } from '../../../@generated/@types/contentful';

import { IPriceAndCalories } from '../../../lib/tallyItem';
import { InspireCmsEntry } from '../../../common/types';

export interface FloatingBannerProps {
    showInitially?: boolean;
    offset: number;
    product: InspireCmsEntry<IProductFields>;
    productInfo: IPriceAndCalories;
    modifications?: string[];
    onAddToBag: () => void;
    domainProduct: ItemModel;
    addToBagBtnText: string;
    addToBagBtnDisabled?: boolean;
}
