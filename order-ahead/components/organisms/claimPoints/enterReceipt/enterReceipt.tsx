import React, { useMemo } from 'react';
import { Form, Formik } from 'formik';
import styles from './enterReceipt.module.css';
import classnames from 'classnames';
import FormikInput from '../../formikInput';
import { format } from '../../../../common/helpers/dateTime';
import { IClaimMissingPointsFromClientModel } from '../../../../@generated/webExpApi/models';
import { InspireButton } from '../../../atoms/button';
import {
    validateCheckNumber,
    validateStoreNumber,
    validateSubTotal,
} from '../../../../common/helpers/complexValidateHelper';
import Icon from '../../../atoms/BrandIcon';
import { STORE_NUMBER_LABEL, TOTAL_LABEL, CHECK_NUMBER_LABEL, DATE_LABEL } from './constants';
import { validateReceiptDate } from '../../../../common/helpers/validateDateHelper';

interface IEnterReceiptProps {
    isLoading: boolean;
    onSubmit: (options: IClaimMissingPointsFromClientModel, resetAndValidateForm: () => void) => void;
}

const EnterReceipt = (props: IEnterReceiptProps): JSX.Element => {
    const { isLoading } = props;
    const initialValue = {
        storeNumber: '',
        date: '',
        checkNumber: '',
        subTotal: '',
    };

    const testStoreNumber = useMemo(() => validateStoreNumber(STORE_NUMBER_LABEL), []);
    const testCheckNumber = useMemo(() => validateCheckNumber(CHECK_NUMBER_LABEL), []);
    const testDate = useMemo(() => validateReceiptDate(DATE_LABEL), []);
    const testTotal = useMemo(() => validateSubTotal(TOTAL_LABEL), []);

    const handleSubmit = ({ storeNumber, date, checkNumber, subTotal }, { resetForm, validateForm }) => {
        const resetAndValidateForm = () => {
            resetForm();
            validateForm();
        };

        props.onSubmit(
            {
                locationId: storeNumber,
                checkNumber,
                subTotal: Number(subTotal),
                date: format(new Date(date), 'yyyy-MM-dd'),
            },
            resetAndValidateForm
        );
    };

    const handleChangeOnlyDigit = ({ event, setFieldValue, fieldName, regex }) => {
        event.preventDefault();
        const { value } = event.target;

        if (regex.test(value.toString())) {
            setFieldValue(fieldName, value);
        }
    };

    const storeNumberMask = [/\d/, /\d/, /\d/, /\d/];
    const dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
    return (
        <>
            <span className={classnames('t-subheader-small', styles.enterReceiptHeader)}>
                Enter Receipt <Icon icon="info-question" size="m" className={styles.questionIcon} />{' '}
            </span>
            <div className={styles.enterReceiptForm}>
                <Formik initialValues={initialValue} onSubmit={handleSubmit} validateOnMount enableReinitialize>
                    {({ isValid, setFieldValue }) => (
                        <Form className={styles.form}>
                            <div className={styles.inputsRow}>
                                <FormikInput
                                    label={STORE_NUMBER_LABEL}
                                    mask={storeNumberMask}
                                    placeholder="Enter store number"
                                    className={styles.input}
                                    labelClassName={styles.inputLabel}
                                    required
                                    type="phone"
                                    name="storeNumber"
                                    validate={testStoreNumber}
                                />
                                <FormikInput
                                    mask={dateMask}
                                    label={DATE_LABEL}
                                    placeholder="mm/dd/yyyy"
                                    required
                                    className={styles.shortInput}
                                    labelClassName={styles.inputLabel}
                                    name="date"
                                    validate={testDate}
                                />
                            </div>
                            <div className={styles.inputsRow}>
                                <FormikInput
                                    label={CHECK_NUMBER_LABEL}
                                    placeholder="Enter check number"
                                    className={styles.input}
                                    labelClassName={styles.inputLabel}
                                    required
                                    type="phone"
                                    name="checkNumber"
                                    validate={testCheckNumber}
                                    onChange={(event) => {
                                        handleChangeOnlyDigit({
                                            event,
                                            setFieldValue,
                                            fieldName: 'checkNumber',
                                            regex: /^[0-9]*$/,
                                        });
                                    }}
                                />
                                <FormikInput
                                    placeholder="Enter Sub total or total"
                                    label={TOTAL_LABEL}
                                    className={styles.input}
                                    labelClassName={styles.inputLabel}
                                    required
                                    type="phone"
                                    name="subTotal"
                                    validate={testTotal}
                                    onChange={(event) => {
                                        handleChangeOnlyDigit({
                                            event,
                                            setFieldValue,
                                            fieldName: 'subTotal',
                                            regex: /^[0-9]*\.?[0-9]*$/,
                                        });
                                    }}
                                />
                            </div>
                            <InspireButton
                                disabled={!isValid || isLoading}
                                className={styles.submitReceipt}
                                submit
                                text="Submit"
                            />
                        </Form>
                    )}
                </Formik>
            </div>
        </>
    );
};

export default EnterReceipt;
