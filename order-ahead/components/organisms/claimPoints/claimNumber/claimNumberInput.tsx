import React, { useMemo, useState } from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import styles from './claimNumberInput.module.css';
import classnames from 'classnames';
import { IClaimMissingPointsFromClientModel } from '../../../../@generated/webExpApi/models';
import FormikInput from '../../formikInput';
import { InspireButton } from '../../../atoms/button';
import InspireTooltip from '../../../atoms/Tooltip';
import { Formik, Form } from 'formik';
import { validateClaimNumber } from '../../../../common/helpers/complexValidateHelper';
import Icon from '../../../atoms/BrandIcon';
interface IClaimNumberProps {
    isLoading: boolean;
    onSubmit: (options: IClaimMissingPointsFromClientModel, resetAndValidateForm: () => void) => void;
}

const ClaimNumberInput = (props: IClaimNumberProps): JSX.Element => {
    const [showTooltip, setShowTooltip] = useState<boolean>(false);

    const { onSubmit, isLoading } = props;
    const initialValue = {
        claimNumber: '',
    };
    const label = 'CLAIM NUMBER';
    const testClaimNumberInput = useMemo(() => validateClaimNumber(label), [label]);
    const handleSubmit = (values, { resetForm, validateForm }) => {
        const resetAndValidateForm = () => {
            resetForm();
            validateForm();
        };
        onSubmit(values, resetAndValidateForm);
    };

    const handleToggleTooltip = () => setShowTooltip((state) => !state);

    const handleCloseTooltip = () => setShowTooltip(false);

    return (
        <>
            <ClickAwayListener onClickAway={handleCloseTooltip}>
                <div>
                    <span className={classnames('t-subheader-small', styles.claimNumberHeader)}>
                        Enter Claim Number
                    </span>
                    <InspireTooltip
                        open={showTooltip}
                        tooltipClassName={styles.questionTooltip}
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        disablePortal
                        placement="bottom"
                        theme="dark"
                        title={'Your claim number is at the very bottom of your receipt, under the QR code.'}
                        arrow
                    >
                        <div className={styles.tooltipWrapper}>
                            <Icon
                                onClick={handleToggleTooltip}
                                icon="info-question"
                                size="m"
                                className={styles.questionIcon}
                            />
                        </div>
                    </InspireTooltip>
                </div>
            </ClickAwayListener>
            <div className={styles.inputRow}>
                <Formik onSubmit={handleSubmit} initialValues={initialValue} validateOnMount enableReinitialize>
                    {({ isValid, setFieldValue }) => (
                        <Form className={styles.claimNumberForm}>
                            <FormikInput
                                labelClassName={styles.claimNumberLabel}
                                label="Claim Number"
                                name="claimNumber"
                                onChange={(e) => {
                                    e.preventDefault();
                                    const { value } = e.target;
                                    const claimNumberRegex = /^[\d-]+$/;
                                    if (!value || claimNumberRegex.test(value.toString())) {
                                        setFieldValue('claimNumber', value);
                                    }
                                }}
                                className={styles.claimNumberInput}
                                validate={testClaimNumberInput}
                                placeholder="xxxx-xxxx-xxxx-xxxx"
                            />
                            <InspireButton
                                submit
                                disabled={!isValid || isLoading}
                                text="Submit"
                                className={styles.submitButton}
                            />
                        </Form>
                    )}
                </Formik>
            </div>
        </>
    );
};

export default ClaimNumberInput;
