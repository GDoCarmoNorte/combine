import React, { useState } from 'react';
import styles from './claimPoints.module.css';
import { IClaimMissingPointsFromClientModel } from '../../../@generated/webExpApi/models';
import classnames from 'classnames';
import ClaimNumberInput from './claimNumber/claimNumberInput';
import EnterReceipt from './enterReceipt/enterReceipt';
import Icon from '../../atoms/BrandIcon';
import SuccessErrorModal from '../../molecules/successErrorModal';
import { useClaimPoints } from '../../../common/hooks/useClaimPoints';

const SUCCESSFULLY_CLAIM_POINTS_MESSAGE =
    'You have successfully submitted your receipt and your points will be added to your account shorty!';

const ClaimPoints = (): JSX.Element => {
    const [collapsed, setCollapsed] = useState<boolean>(false);
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const { hasError, isLoading, errorMessage, errorCode, onClaimPoints } = useClaimPoints();

    const onModalClose = () => {
        setIsModalOpen(false);
    };

    const toggleCollapsed = () => {
        setCollapsed((state) => !state);
    };

    const onClaimPointsPressed = (options: IClaimMissingPointsFromClientModel, resetAndValidateForm: () => void) => {
        onClaimPoints(options, resetAndValidateForm).finally(() => {
            setIsModalOpen(true);
        });
    };

    return (
        <div className={styles.container}>
            <span className={classnames('t-header-h2', styles.claimPointsHeader)}>earn more points</span>
            <p className={classnames('t-paragraph-small', styles.claimNumbersInfo)}>
                Enter claim number or receipt information within 14 days of the original purchase to get points towards
                rewards or open the app to scan receipts instead.
            </p>
            <ClaimNumberInput isLoading={isLoading} onSubmit={onClaimPointsPressed} />
            {collapsed && <EnterReceipt isLoading={isLoading} onSubmit={onClaimPointsPressed} />}
            <span className={styles.collapseRow} onClick={toggleCollapsed}>
                <Icon className={styles.icon} icon={collapsed ? 'direction-up' : 'direction-down'} />
                <span className={styles.underline}>Enter Receipt</span>
            </span>
            <SuccessErrorModal
                open={isModalOpen}
                onClose={onModalClose}
                isSuccess={!hasError}
                description={hasError ? errorMessage : SUCCESSFULLY_CLAIM_POINTS_MESSAGE}
                errorCode={errorCode}
            />
        </div>
    );
};

export default ClaimPoints;
