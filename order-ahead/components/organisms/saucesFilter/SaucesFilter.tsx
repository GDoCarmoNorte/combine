import React, { FC } from 'react';
import classnames from 'classnames';
import Chip from '../../atoms/Chip';
import { ProductIntensityEnum } from '../../../redux/types';
import SauceIntensity from '../../atoms/SauceIntensity/SauceIntensity';

import styles from './SaucesFilter.module.css';

interface ISaucesFilterProps {
    onChangeFilter: (filters: ProductIntensityEnum[]) => void;
    selectedFilters: ProductIntensityEnum[];
}

const SaucesFilter: FC<ISaucesFilterProps> = ({ onChangeFilter, selectedFilters }) => {
    const handleClickFilterItem = (intensity: ProductIntensityEnum) => {
        const newFilters = selectedFilters.includes(intensity)
            ? selectedFilters.filter((value) => value !== intensity)
            : [...selectedFilters, intensity];

        onChangeFilter(newFilters);
    };

    return (
        <div className={styles.container}>
            {Object.values(ProductIntensityEnum).map((intensity) => {
                const selected = selectedFilters.includes(intensity);

                return (
                    <Chip
                        key={intensity}
                        className={styles.chip}
                        component="button"
                        onClick={() => handleClickFilterItem(intensity)}
                        size="medium"
                        clickable
                        selected={selected}
                        variant="outlined"
                        label={
                            <SauceIntensity
                                containerClassName={styles.sauseIntensity}
                                intensityFlamesClassName={classnames(styles.intensityFlames, {
                                    [styles.selectedIntensityFlames]: selected,
                                })}
                                intensityTextClassName={styles.intensityText}
                                intensity={intensity}
                            />
                        }
                    />
                );
            })}
        </div>
    );
};

export default SaucesFilter;
