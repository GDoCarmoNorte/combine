import React from 'react';
import { useGlobalProps } from '../../../redux/hooks';
import { useMainComboDisplayProductByProductId } from '../../../redux/hooks/pdp';
import { IDisplayProductSection } from '../../../redux/types';
import CategoryHeader from '../../atoms/categoryHeader/categoryHeader';
import { ModifierCardContainer, IModifierSelectedParams } from '../modifierCardContainer/modifierCardContainer';

import styles from './comboMainProductModifier.module.css';

interface IComboMainProductModifierProps {
    id: string;
    section: IDisplayProductSection;
    onSizeSelect: (id: string) => void;
}

export default function ComboMainProductModifier({
    id,
    section,
    onSizeSelect,
}: IComboMainProductModifierProps): JSX.Element | null {
    const globalProps = useGlobalProps();
    const displayProduct = useMainComboDisplayProductByProductId(id);
    const sizeSelections = displayProduct?.displayProductDetails?.selections;

    if (!(section.productSectionDisplayName === 'Sandwich' && sizeSelections?.length > 1)) {
        return null;
    }

    const handleSizeSelect = (mod: IModifierSelectedParams) => {
        onSizeSelect(mod.id);
    };

    return (
        <>
            <CategoryHeader text={`${section.productSectionDisplayName} SIZE`} showBullet />
            <div className={styles.modifierCardSection}>
                <div className={styles.modifierCardWrapper} aria-label={`${section.productSectionDisplayName} Section`}>
                    <ModifierCardContainer
                        categoryName={'displayName'}
                        modifierItemsById={globalProps.productsById}
                        selectorType="size"
                        displayProduct={displayProduct}
                        onChange={handleSizeSelect}
                        sectionIndex={0}
                        selectionType="multi"
                    />
                </div>
            </div>
        </>
    );
}
