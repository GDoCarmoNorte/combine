import React from 'react';
import classnames from 'classnames';
import { useOrderHistory } from '../../../redux/hooks';
import BrandLoader from '../../atoms/BrandLoader';
import { IProductItemById } from '../../../common/services/globalContentfulProps';
import { OrderHistoryItem } from './orderHistoryItem/orderHistoryItem';
import { OrderHistoryEmpty } from './orderHistoryEmpty/orderHistoryEmpty';
import styles from './orderHistory.module.css';

interface OrderHistoryProps {
    productsById: IProductItemById;
}

export const OrderHistory = ({ productsById }: OrderHistoryProps): JSX.Element => {
    const { orderHistory, isLoading } = useOrderHistory();

    if (isLoading) {
        return <BrandLoader className={styles.loader} />;
    }

    return (
        <div className={styles.container}>
            <h2 className={classnames('t-header-h2', styles.blockTitle)}>orders</h2>
            {!orderHistory.length ? (
                <OrderHistoryEmpty />
            ) : (
                <section className={classnames(styles.container)}>
                    {orderHistory.map((item) => (
                        <OrderHistoryItem key={item.id} orderItem={item} productsById={productsById} />
                    ))}
                </section>
            )}
        </div>
    );
};
