import React from 'react';
import { format } from '../../../../common/helpers/dateTime';
import classnames from 'classnames';
import {
    ILocationWithDetailsModel,
    IOrderModel,
    TallyFulfillmentTypeModel,
    TallyProductModel,
} from '../../../../@generated/webExpApi';
import { getLocationAddressString } from '../../../../lib/locations';
import { Divider } from '../../../atoms/divider';
import { InspireLink } from '../../../atoms/link';
import { InspireButton } from '../../../atoms/button';
import { IProductItemById } from '../../../../common/services/globalContentfulProps';
import { ReviewOrderItem } from '../../checkout/reviewOrder';
import styles from './orderHistoryItem.module.css';
import { useOrderLocation } from '../../../../redux/hooks';
import { useDomainProducts } from '../../../../redux/hooks/domainMenu';
import { OrderLocationMethod } from '../../../../redux/orderLocation';
import useAddToBagFromOrderHistory from '../../../../common/hooks/useAddToBagFromOrderHistory';

interface IOrderHistoryItem {
    orderItem: IOrderModel;
    productsById: IProductItemById;
}

export const OrderHistoryItem = ({ orderItem, productsById }: IOrderHistoryItem): JSX.Element => {
    const VISIBLE_PRODUCTS_COUNT = 3;
    const products = orderItem.products.slice(0, VISIBLE_PRODUCTS_COUNT);
    const remainingProductsCount =
        orderItem.products.length > VISIBLE_PRODUCTS_COUNT ? orderItem.products.length - VISIBLE_PRODUCTS_COUNT : 0;
    const isPlural = remainingProductsCount > 1;
    const isDeliveryItem = orderItem.fulfillment.type === TallyFulfillmentTypeModel.Delivery;
    const locationUrl = orderItem.fulfillment.location?.url;

    const deliveryTypeString = isDeliveryItem ? 'DELIVERY STATUS' : 'PICKUP LOCATION';
    const locationNameString = isDeliveryItem ? '' : orderItem.fulfillment.location?.displayName;
    const address = isDeliveryItem
        ? orderItem.fulfillment.contactDetails?.deliveryAddress
        : orderItem.fulfillment.location;
    const addressString = isDeliveryItem
        ? 'Your order will be delivered to the address as stated. The address is hidden for security purposes.'
        : getLocationAddressString(address as ILocationWithDetailsModel);

    const { method, isCurrentLocationOAAvailable } = useOrderLocation();

    const productsIds = orderItem.products.map((product) => product.id);
    const domainProducts = useDomainProducts(productsIds);

    const isAddToBagAvailable =
        isCurrentLocationOAAvailable &&
        method !== OrderLocationMethod.NOT_SELECTED &&
        productsIds.some((productId) => domainProducts[productId]);

    const { addFromOrderHistory: handleAddToBag, modifierGroups } = useAddToBagFromOrderHistory(products);

    const { currentLocation } = useOrderLocation();

    const ctaProps = currentLocation
        ? {
              text: 'add to bag',
              onClick: handleAddToBag,
              disabled: !isAddToBagAvailable,
          }
        : {
              text: 'Select a location',
              link: '/locations',
          };

    return (
        <div className={styles.container}>
            <div className={styles.locationContainer}>
                <p className={classnames(styles.date, 't-paragraph-small')}>
                    {format(orderItem.dateTime, 'MMM. dd, yyyy')}
                </p>
                <p className={classnames(styles.subhead, 't-subheader-small')}>{deliveryTypeString}</p>
                {isDeliveryItem ? (
                    <p className={classnames(styles.location, 't-header-h3')}>{locationNameString}</p>
                ) : (
                    <InspireLink
                        link={!locationUrl ? `/locations/${locationUrl}` : '/locations/all'}
                        newtab
                        onClick={(e) => {
                            e.stopPropagation();
                        }}
                        className={classnames(styles.locationLink, 't-header-h3')}
                    >
                        {locationNameString}
                    </InspireLink>
                )}
                <p className="t-paragraph-small">{String(addressString)}</p>
            </div>
            <Divider className={styles.verticalDivider} orientation="vertical" />
            <Divider className={styles.horizontalDivider} orientation="horizontal" />
            <div className={styles.orderContainer}>
                <div className={styles.reviewOrder}>
                    {products.map((product, i) => (
                        <ReviewOrderItem
                            key={i + product.id}
                            tallyItem={
                                {
                                    ...product,
                                    productId: product.id,
                                    modifierGroups: modifierGroups[i],
                                } as TallyProductModel
                            }
                            productsById={productsById}
                            hidePrice
                            showImage
                        />
                    ))}
                </div>
                {remainingProductsCount > 0 && (
                    <p className={classnames(styles.more, 't-paragraph-hint')}>
                        ( {remainingProductsCount} more {isPlural ? 'items' : 'item'} )
                    </p>
                )}
            </div>
            <Divider className={styles.verticalDivider} orientation="vertical" />
            <Divider className={styles.horizontalDivider} orientation="horizontal" />{' '}
            <div className={styles.actionsContainer}>
                <InspireButton type="small" {...ctaProps} />
            </div>
        </div>
    );
};
