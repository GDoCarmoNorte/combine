import React from 'react';
import classnames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import BrandIcon from '../../../atoms/BrandIcon';
import styles from './orderHistoryEmpty.module.css';

export const OrderHistoryEmpty = (): JSX.Element => {
    const emptyOrdersText = 'Aren’t you hungry? You haven’t ordered anything.';

    return (
        <div className={styles.container}>
            <span className={styles.iconContainer}>
                <BrandIcon className={styles.orderIcon} icon="order-pickup-small" />
                <BrandIcon className={styles.restrictIcon} icon="order-not" size="xl" variant="colorful" />
            </span>
            <div className={styles.content}>
                <h3 className={classnames('t-header-card-title', styles.title)}>no recent orders</h3>
                <p className={classnames('t-paragraph', styles.text)}>{emptyOrdersText}</p>
            </div>
            <div className={styles.buttonWrapper}>
                <InspireButton link="/menu" type="primary" text="see our menu" />
            </div>
        </div>
    );
};
