import React, { FC } from 'react';
import { useAccount } from '../../../redux/hooks';
import { IInspireButtonProps, InspireButton } from '../../atoms/button';

export const BlazingRewardsButton: FC<IInspireButtonProps> = (props) => {
    const account = useAccount()?.account;

    return <>{account ? <InspireButton text={'blazin’ rewards®'} {...props} /> : null}</>;
};
