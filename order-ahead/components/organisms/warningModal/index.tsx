import React from 'react';
import classnames from 'classnames';
import { Modal } from '@material-ui/core';
import { IInspireButtonProps, InspireButton } from '../../atoms/button';
import styles from './index.module.css';

export interface IWarningModalDescription {
    title: string;
    message?: string;
    textButton?: string;
}

interface IWarningModal {
    description: IWarningModalDescription;
    open: boolean;
    onModalClose?: () => void;
    primaryButton: IInspireButtonProps;
    secondaryButton?: IInspireButtonProps;
}

const WarningModal = (props: IWarningModal) => {
    const { description, open, primaryButton, secondaryButton, onModalClose } = props;

    const { onClick: onPrimaryClick, text: textPrimaryButton } = primaryButton;

    const { message, title } = description;

    return (
        <Modal open={open} onBackdropClick={onModalClose}>
            <div className={styles.container}>
                <div className={styles.content}>
                    <img className={styles.icon} src={`/brands/bww/warning.svg`} alt="" />
                    <div className={classnames('t-header-h3', styles.title)}>{title}</div>
                    {message && <div className={classnames('t-paragraph', styles.message)}>{description.message}</div>}
                </div>
                <div className={styles.btnBlock}>
                    {secondaryButton?.text && (
                        <InspireButton
                            className={styles.button}
                            onClick={secondaryButton?.onClick}
                            type="secondary"
                            fullWidth
                            text={secondaryButton?.text}
                        />
                    )}
                    <InspireButton
                        className={styles.button}
                        onClick={onPrimaryClick}
                        fullWidth
                        text={textPrimaryButton}
                    />
                </div>
            </div>
        </Modal>
    );
};

export default WarningModal;
