import React from 'react';
import classNames from 'classnames';

import styles from './termsAndConditionsLinks.module.css';
import Link from 'next/link';

const TermsAndConditionsLinks = (): JSX.Element => {
    return (
        <div className={classNames('t-paragraph-hint', styles.termsAndConditionsInfoBlock)}>
            <p>
                I have read and agree to the{' '}
                <Link href={'/terms-of-use'}>
                    <a
                        className={classNames('t-paragraph-hint', 'link-secondary-active')}
                        target={'_blank'}
                        rel="noreferrer"
                    >
                        terms and conditions
                    </a>
                </Link>
                . Information will be used as described here and in the{' '}
                <Link href={'/privacy-policy'}>
                    <a
                        className={classNames('t-paragraph-hint', 'link-secondary-active')}
                        target={'_blank'}
                        rel="noreferrer"
                    >
                        privacy policy
                    </a>
                </Link>
                .
            </p>
        </div>
    );
};

export default TermsAndConditionsLinks;
