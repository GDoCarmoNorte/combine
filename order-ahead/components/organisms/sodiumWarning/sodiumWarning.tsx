import React from 'react';
import classNames from 'classnames';

import styles from './sodiumWarning.module.css';
import { ISodiumWarningFields } from '../../../@generated/@types/contentful';
import RichText from '../../atoms/richText';
import { Block, INLINES } from '@contentful/rich-text-types';
import { InspireCmsEntry } from '../../../common/types';

interface ISodiumWarning {
    sodiumWarning: InspireCmsEntry<ISodiumWarningFields>;
}

const renderSodiumWarning = (node: Block) => {
    const imgSrc = node.data?.target?.fields?.icon?.fields?.file?.url;
    return `<img src="${imgSrc}" alt="Sodium warning label"/>`;
};

const renderNode = {
    [INLINES.EMBEDDED_ENTRY]: renderSodiumWarning,
};

export default function SodiumWarning(props: ISodiumWarning): JSX.Element {
    const { sodiumWarning } = props;
    const text = sodiumWarning?.fields?.message;

    return <RichText text={text} renderNode={renderNode} className={classNames('t-paragraph', styles.container)} />;
}
