import React, { FC, useEffect, useMemo, useState } from 'react';
import { sha256 } from 'js-sha256';
import styles from './playPageLayout.module.css';
import BrandLoader from '../../atoms/BrandLoader';
import { useAuth0 } from '@auth0/auth0-react';
import { useCheckin } from '../../../common/hooks/useCheckin';
import { useAccount, useNotifications, useOrderHistory } from '../../../redux/hooks';
import { ITransformedGetCustomerAccountResponseForClientModel } from '../../../@generated/webExpApi';
import { getCoordinates } from '../../../common/helpers/getCoordinates';
import { setCookieValue } from '../../../common/helpers/cookieHelper';
import { LocationWithDetailsModel } from '../../../common/services/locationService/types';
import { getLocationById } from '../../../common/services/locationService';
import {
    HASHED_BLAZIN_REWARDS_COOKIE_NAME,
    LMS_PROFILE_ID_TYPE,
    MESSAGE_EVENT,
    RAW_BLAZIN_REWARDS_COOKIE_NAME,
} from './constants';
import { ICookieData, MessageCommand } from './types';

const PlayPageLayout: FC = () => {
    const { loginWithRedirect, isAuthenticated } = useAuth0();
    const { orderHistory, isLoading: isOrderHistoryLoading } = useOrderHistory();
    const { account } = useAccount();
    const {
        actions: { enqueueError },
    } = useNotifications();
    const { checkin, payload: checkinData } = useCheckin();

    const [isLoading, setLoading] = useState<boolean>(true);
    const [userLatitude, setUserLatitude] = useState<number>();
    const [userLongitude, setUserLongitude] = useState<number>();
    const [cookieData, setCookieData] = useState<ICookieData>();
    const [getCoordinatesInProgress, setCoordinatesInProgress] = useState<boolean>(true);
    const [checkinInProgress, setCheckinInProgress] = useState<boolean>(isAuthenticated);
    const [location, setLocation] = useState<LocationWithDetailsModel>();

    useEffect(() => {
        retrieveUserCoordinates();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const playPageUrl = useMemo(() => {
        return `${process.env.NEXT_PUBLIC_PLAY_PAGE_URL}?checkedIn=${!!checkinData}&storeId=${
            location?.id || ''
        }&storeState=${location?.contactDetails.address.stateProvinceCode || ''}&storeZip=${
            location?.contactDetails.address.postalCode || ''
        }`;
    }, [checkinData, location]);

    useEffect(() => {
        if (checkinData?.storeId) {
            (async () => {
                const location = await getLocationById({
                    locationId: checkinData.storeId.toString(),
                });
                if (location) {
                    setLocation(location);
                }
            })();
        }
    }, [checkinData]);

    useEffect(() => {
        if (isAuthenticated && account && !getCoordinatesInProgress) {
            checkinUser();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isAuthenticated, account, getCoordinatesInProgress]);

    useEffect(() => {
        if (!getCoordinatesInProgress && !isOrderHistoryLoading) {
            const cookieData = getCookieData(account);
            setCookieData(cookieData);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [getCoordinatesInProgress, isOrderHistoryLoading, account]);

    useEffect(() => {
        window.addEventListener(MESSAGE_EVENT, messageHandler, false);

        return () => {
            window.removeEventListener(MESSAGE_EVENT, messageHandler);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoading]);

    useEffect(() => {
        if (cookieData) {
            const cookieString = JSON.stringify(cookieData);
            const hashable = cookieString + process.env.NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY;
            const hashedCookieString = sha256(hashable);
            setCookieValue(RAW_BLAZIN_REWARDS_COOKIE_NAME, cookieString, 365);
            setCookieValue(HASHED_BLAZIN_REWARDS_COOKIE_NAME, hashedCookieString, 365);
        }
    }, [cookieData]);

    const messageHandler = (event: MessageEvent): void => {
        const MESSAGE_ORIGIN = process.env.NEXT_PUBLIC_PLAY_PAGE_URL;
        if (event.origin === MESSAGE_ORIGIN) {
            const { command } = event.data;

            switch (command) {
                case MessageCommand.Checkin:
                    checkinUser();
                    break;
                case MessageCommand.Login:
                    handleLoginCommand();
                    break;
                default:
                    console.warn('Unexpected command received: ', command);
            }
        }
    };

    const onIframeLoad = () => {
        setLoading(false);
    };

    const checkinUser = async () => {
        if (isAuthenticated) {
            setCheckinInProgress(true);
            checkin().finally(() => setCheckinInProgress(false));
        }
    };

    const retrieveUserCoordinates = async () => {
        try {
            const {
                coords: { latitude, longitude },
            } = await getCoordinates();
            setUserLatitude(latitude);
            setUserLongitude(longitude);
        } catch (e) {
            enqueueError({
                message:
                    'Location services are disabled for your device. Please go to your app settings and enable location service access for the app to use this feature.',
            });
        } finally {
            setCoordinatesInProgress(false);
        }
    };

    const handleLoginCommand = () => {
        if (!isAuthenticated) {
            try {
                loginWithRedirect({ appState: { target: window.location.pathname } });
            } catch ({ message }) {
                enqueueError({ message });
            }
        }
    };

    const getCookieData = (account: ITransformedGetCustomerAccountResponseForClientModel) => {
        const profileId = account?.references?.find((r) => r.type === LMS_PROFILE_ID_TYPE)?.id || '';
        const lastOrder = orderHistory?.length ? orderHistory[0] : null;
        return {
            profileId,
            email: account?.email || '',
            firstName: account?.firstName || '',
            lastName: account?.lastName || '',
            userZipcode: account?.preferences?.postalCode || '',
            phoneNumber: account?.phones && account.phones.length > 0 ? account.phones[0].number : '',
            eCommerceDate: lastOrder?.dateTime?.toISOString() || '',
            eCommerceStoreId: lastOrder?.fulfillment?.location?.id || '',
            latitude: userLatitude,
            longitude: userLongitude,
            appIdentifier: '',
            GTMContainerId: '', //TODO: populate when created by SEO team
            deviceType: 'web',
        };
    };

    return (
        <div className={styles.container}>
            {(isLoading || checkinInProgress || !cookieData) && <BrandLoader className={styles.loader} />}
            {!checkinInProgress && cookieData && (
                <iframe
                    aria-label="Play"
                    src={playPageUrl}
                    onLoad={() => onIframeLoad()}
                    height={'600'}
                    width={'100%'}
                />
            )}
        </div>
    );
};

export default PlayPageLayout;
