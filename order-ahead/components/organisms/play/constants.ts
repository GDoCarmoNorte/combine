export const MESSAGE_EVENT = 'message';
export const RAW_BLAZIN_REWARDS_COOKIE_NAME = 'blazin_rewards';
export const HASHED_BLAZIN_REWARDS_COOKIE_NAME = 'blazin_hash';
export const LMS_PROFILE_ID_TYPE = 'LMS';
