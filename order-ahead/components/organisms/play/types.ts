export interface ICookieData {
    profileId: string;
    email: string;
    firstName: string;
    lastName: string;
    userZipcode: string;
    phoneNumber: string;
    eCommerceDate?: string;
    eCommerceStoreId?: string;
    latitude: number;
    longitude: number;
    appIdentifier?: string;
    GTMContainerId: string;
    deviceType: string;
}

export enum MessageCommand {
    Login = 'userLogin',
    Checkin = 'userCheckin',
}
