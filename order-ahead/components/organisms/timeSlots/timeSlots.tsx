import React, { FC, useEffect, useState, useMemo } from 'react';
import styles from './timeSlots.module.css';
import classnames from 'classnames';
import DropdownComponent from '../checkout/pickupDeliveryInfo/dropdown';
import { useBag, useOrderLocation } from '../../../redux/hooks';
import { IAvailableTimeModel, TServiceTypeModel } from '../../../@generated/webExpApi';
import { isSameDay, isAfter, utcToZonedTime, formatTZ } from '../../../common/helpers/dateTime';
import { resolveOpeningHours } from '../../../lib/locations';
import { addASAPFieldToRange } from '../../../common/helpers/checkoutHelpers';
import { OrderLocationMethod } from '../../../redux/orderLocation';
import { capitalize } from 'lodash';
import { getHoursWithTimezone } from '../../../lib/locations/resolveOpeningHours';
import { transformHoursByDay } from '../../../common/helpers/getStoreStatus';
import { useLocationTimeZone } from '../../../common/hooks/useLocationTimeZone';
import { useBagAnalytics } from '../../../common/hooks/useBagAnalytics';

export interface ILocationTimeSlotsProps {
    method: OrderLocationMethod;
}

const todayLabel = 'Today';
const asapValue = 'asap';
const dayFormat = 'MM/dd/yyyy';

const TimeSlots: FC<ILocationTimeSlotsProps> = (props): JSX.Element => {
    const { method } = props;
    const {
        pickupTime,
        pickupTimeType,
        deliveryTime,
        deliveryTimeType,
        actions: { setPickupTime },
    } = useBag();

    const time = method === OrderLocationMethod.DELIVERY ? deliveryTime : pickupTime;
    const timeType = method === OrderLocationMethod.DELIVERY ? deliveryTimeType : pickupTimeType;

    const { pushGtmChangeOrderDate } = useBagAnalytics();

    const timeDropdownLabel = `${capitalize(method)} time`;
    const dateDropdownLabel = `${capitalize(method)} date`;

    const getFormattedDay = (date: Date) => formatTZ(date, dayFormat, { timeZone: locationTimeZone });

    const { pickupLocationTimeSlots, deliveryLocationTimeSlots, deliveryAddress, pickupAddress } = useOrderLocation();

    const pickUpLocation = deliveryAddress?.pickUpLocation;

    const locationTimeZone = useLocationTimeZone();

    const availableTimeSlots = useMemo(
        () => (method === OrderLocationMethod.DELIVERY ? deliveryLocationTimeSlots : pickupLocationTimeSlots),
        [deliveryLocationTimeSlots, pickupLocationTimeSlots, method]
    );
    const storeIsOpenNow = useMemo(() => {
        if (method === OrderLocationMethod.DELIVERY && !pickUpLocation?.hoursByDay) {
            return false;
        }

        const resolvedOpeningHours =
            method === OrderLocationMethod.DELIVERY
                ? getHoursWithTimezone(transformHoursByDay(pickUpLocation.hoursByDay), locationTimeZone)
                : resolveOpeningHours(pickupAddress, TServiceTypeModel.OrderAhead);

        return !!resolvedOpeningHours && resolvedOpeningHours.isOpen;
    }, [pickUpLocation, pickupAddress, method, locationTimeZone]);

    const timeIntervalsNotSetForDelivery =
        method === OrderLocationMethod.DELIVERY &&
        availableTimeSlots &&
        availableTimeSlots.delivery &&
        availableTimeSlots.delivery.byDay &&
        Object.keys(availableTimeSlots.delivery.byDay).length === 0;

    const [availableTimesByDay, setAvailableTimesByDay] = useState<{ [key: string]: Array<IAvailableTimeModel> }>(null);
    const [timeSlotsDay, setTimeSlotsDay] = useState<{ day: string; timeRanges: IAvailableTimeModel[] }>(null);
    const [availableDays, setAvailableDays] = useState<{ value: string; label: string; hint?: string }[]>(null);
    const [availableTimes, setAvailableTimes] = useState<{ value: string; label: string; hint?: string }[]>(null);

    const formatDates = (date: string) => {
        //date has format yyyy-MM-dd
        if (date) {
            const splitDate = date.split('-');
            return splitDate[1] + '/' + splitDate[2] + '/' + splitDate[0];
        }
        return null;
    };

    const filterAvailableTimes = (): void => {
        const availableTimes = (method === OrderLocationMethod.DELIVERY
            ? availableTimeSlots.delivery
            : availableTimeSlots.pickup
        )?.byDay;
        if (!availableTimes) return;

        const timeWeek = Object.keys(availableTimes).reduce((acc, key) => {
            return { ...acc, [formatDates(key)]: availableTimes[key] };
        }, {});

        const currentTime = utcToZonedTime(new Date(Date.now()), locationTimeZone);
        const day = getFormattedDay(currentTime);

        if (timeWeek[day]) {
            const times = timeWeek[day]?.filter((time) =>
                isAfter(utcToZonedTime(new Date(time.utc), locationTimeZone), currentTime)
            );
            timeWeek[day] = times;
        }
        setAvailableTimesByDay(timeWeek);
    };

    useEffect(() => {
        const currentTime = utcToZonedTime(new Date(Date.now()), locationTimeZone);
        if (!isAfter(utcToZonedTime(new Date(time), locationTimeZone), currentTime)) {
            setPickupTime({
                time: null,
                asap: true,
                method,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [time]);

    useEffect(() => {
        if (!availableTimeSlots) return;
        filterAvailableTimes();
        const interval = setInterval(filterAvailableTimes, 1000);
        return () => clearInterval(interval);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [availableTimeSlots]);

    useEffect(() => {
        if (!availableTimesByDay) return;
        const now = new Date(Date.now());
        const nowTimezoned = formatTZ(utcToZonedTime(now, locationTimeZone), 'yyyy-MM-dd');
        const availableDays = Object.keys(availableTimesByDay).map((day) => {
            const label = day === formatDates(nowTimezoned) ? `${todayLabel} (${day})` : day;
            return { value: day, label };
        });

        setAvailableDays(availableDays);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [availableTimesByDay]);

    useEffect(() => {
        if (!timeSlotsDay || !timeSlotsDay.timeRanges) return;

        let availableTimeRanges = timeSlotsDay.timeRanges?.map((time) => ({
            value: time.utc.toString(),
            label: time.display.toLowerCase(),
        }));

        if (storeIsOpenNow) {
            availableTimeRanges = addASAPFieldToRange({
                timeRange: availableTimeRanges,
                selectedDay: timeSlotsDay?.timeRanges[0].utc.toString(),
                timezone: locationTimeZone,
                prepTime: null,
                asapWithoutBrackets: true,
                isAdditionalASAP: method === OrderLocationMethod.DELIVERY,
            });
        }

        setAvailableTimes(availableTimeRanges);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [timeSlotsDay]);

    useEffect(() => {
        if (!availableTimesByDay) return;

        if (timeType !== asapValue && time) {
            const selectedDay = Object.keys(availableTimesByDay).find((day) => {
                const pickupTimeWithTimezone = utcToZonedTime(new Date(time), locationTimeZone);
                return isSameDay(new Date(day), pickupTimeWithTimezone);
            });

            if (!selectedDay) return;

            setTimeSlotsDay({ day: selectedDay, timeRanges: availableTimesByDay[selectedDay] });
        } else {
            setTimeSlotsDay(getFirstAvailableDay());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [availableTimesByDay, time]);

    const getFirstAvailableDay = () => {
        const firstDay = Object.keys(availableTimesByDay)[0];

        return { day: firstDay, timeRanges: availableTimesByDay[firstDay] };
    };

    const handlePickupDayChange = (value: string) => {
        const newTimeSlotsDayTimes = availableTimesByDay[value];
        setTimeSlotsDay({ day: value, timeRanges: newTimeSlotsDayTimes });
        const isFirstDaySelected = availableDays[0].value === value;

        setPickupTime({
            time: isFirstDaySelected && storeIsOpenNow ? null : `${newTimeSlotsDayTimes[0].utc}`,
            asap: isFirstDaySelected && storeIsOpenNow,
            method,
        });

        const selectedDaySlot = new Date(newTimeSlotsDayTimes[0].utc).toISOString();
        pushGtmChangeOrderDate(method, selectedDaySlot);
    };

    const handleOrderTimeChange = (value: string) => {
        (value === availableTimes[0].value && getFormattedDay(new Date(value)) === availableDays[0].value) ||
        value === null
            ? setPickupTime({ asap: true, time: null, method })
            : setPickupTime({ time: value, asap: false, method });

        const selectedTimeSlot = new Date(value).toISOString();
        pushGtmChangeOrderDate(method, selectedTimeSlot);
    };

    if ((!availableDays || !availableTimes) && !timeIntervalsNotSetForDelivery) return null;

    return (
        <>
            <div className={styles.wrapper}>
                <DropdownComponent
                    value={timeIntervalsNotSetForDelivery ? 'Unavailable' : timeSlotsDay?.day}
                    label={dateDropdownLabel}
                    labelClassName={styles.label}
                    buttonClassName={styles.buttonLabel}
                    listClassName={classnames('t-paragraph', styles.list)}
                    selectedItemClassName={styles.selectedItem}
                    name="day"
                    placeholder={dateDropdownLabel}
                    options={availableDays ?? []}
                    onChange={handlePickupDayChange}
                    disabled={false}
                    readOnly={timeIntervalsNotSetForDelivery}
                    readOnlyErrorState={true}
                />
                <DropdownComponent
                    value={timeIntervalsNotSetForDelivery ? 'Unavailable' : timeType === asapValue ? asapValue : time}
                    label={timeDropdownLabel}
                    labelClassName={styles.label}
                    buttonClassName={styles.buttonLabel}
                    listClassName={classnames('t-paragraph', styles.list)}
                    selectedItemClassName={styles.selectedItem}
                    name="time"
                    placeholder={
                        timeIntervalsNotSetForDelivery ? '' : `${availableTimes[0]?.label || timeDropdownLabel}`
                    }
                    options={availableTimes ?? []}
                    onChange={handleOrderTimeChange}
                    disabled={false}
                    readOnly={timeIntervalsNotSetForDelivery}
                    readOnlyErrorState={true}
                />
            </div>
            {timeIntervalsNotSetForDelivery && (
                <p
                    data-testid={'selectPickupMessage'}
                    className={classnames('t-paragraph', styles.deliveryTimesErrors)}
                >
                    Select either pickup or a different location
                </p>
            )}
        </>
    );
};

export default TimeSlots;
