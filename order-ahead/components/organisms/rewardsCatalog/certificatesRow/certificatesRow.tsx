import React, { useState } from 'react';
import { TCatalogCertificateModel } from '../../../../@generated/webExpApi/models';
import Collapse from '@material-ui/core/Collapse';
import isSmallScreen from '../../../../lib/isSmallScreen';
import isMobileScreen from '../../../../lib/isMobileScreen';
import classnames from 'classnames';
import Icon from '../../../atoms/BrandIcon';
import useLoyalty from '../../../../redux/hooks/useLoyalty';
import CatalogItem from '../catalogItem/catalogItem';
import BrandLoader from '../../../atoms/BrandLoader';
import styles from './certificatesRow.module.css';

interface ICertificatesRow {
    title: string;
    subTitle?: string;
    items: TCatalogCertificateModel[];
}

const CertificatesRow = (props: ICertificatesRow): JSX.Element => {
    const { title, subTitle, items } = props;
    const [collapsed, setCollapsed] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const { loyalty } = useLoyalty();
    const { pointsBalance } = loyalty;
    const smallScreen = isSmallScreen();
    const mobileScreen = isMobileScreen();

    if (!items.length) return null;

    return (
        <div
            className={classnames(styles.collapseContainer, {
                [styles.line]: collapsed,
            })}
        >
            <div
                className={classnames(styles.row, {
                    [styles.collapsedRow]: collapsed,
                })}
                role="button"
                aria-label="Collapse Row"
                onClick={() => setCollapsed((collapsed) => !collapsed)}
            >
                <div className={styles.headingRow}>
                    <span
                        className={classnames('t-subheader-universal', styles.heading, {
                            [styles.collapsedTitle]: collapsed,
                            't-subheader-universal-small': smallScreen || mobileScreen,
                        })}
                    >
                        {title}
                    </span>
                    {subTitle && <span className={classnames('t-paragraph-hint', styles.subTitle)}>{subTitle}</span>}
                </div>
                <Icon className={styles.icon} icon={collapsed ? 'direction-up' : 'direction-down'} />
            </div>
            <Collapse in={collapsed}>
                <div className={classnames(styles.collapseBody, { [styles.disabled]: loading })}>
                    {items.map((item) => {
                        const { id, title, imageUrl, points, code } = item;
                        const hasEnoughPoints = pointsBalance - points >= 0;
                        const label = !hasEnoughPoints ? `Redeem with ${points} pts` : `${points} pts`;
                        return (
                            <CatalogItem
                                key={id}
                                code={code}
                                title={title}
                                imageUrl={imageUrl}
                                label={label}
                                pointsBalance={pointsBalance}
                                points={points}
                                hasEnoughPoints={hasEnoughPoints}
                                activeCTAText="Redeem"
                                setLoading={setLoading}
                            />
                        );
                    })}
                    {loading && <BrandLoader className={styles.loader} />}
                </div>
            </Collapse>
        </div>
    );
};

export default CertificatesRow;
