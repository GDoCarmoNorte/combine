import React from 'react';
import { useRewards } from '../../../redux/hooks';
import SectionHeader from '../../atoms/sectionHeader';
import styles from './rewardsCatalog.module.css';
import CertificatesRow from './certificatesRow/certificatesRow';
import BrandLoader from '../../atoms/BrandLoader';
import isEmpty from 'lodash/isEmpty';

const RewardsCatalog = (): JSX.Element => {
    const { isCatalogLoading, rewardsCatalog } = useRewards();
    if (isCatalogLoading) {
        return <BrandLoader className={styles.catalogLoader} />;
    }

    if (!isCatalogLoading && isEmpty(rewardsCatalog)) {
        return null;
    }

    return (
        <div className={styles.rewardsCatalogWrapper}>
            <SectionHeader text="Rewards Certificates" className={styles.title} tag="h2" textClassName="t-header-h2" />
            {Object.entries(rewardsCatalog).map(([code, item]) => {
                const { totalCount, certificates, description } = item;

                return (
                    <CertificatesRow
                        key={code}
                        title={description}
                        items={certificates}
                        subTitle={`${totalCount} certificates`}
                    />
                );
            })}
        </div>
    );
};

export default RewardsCatalog;
