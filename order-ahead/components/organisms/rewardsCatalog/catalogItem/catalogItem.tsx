import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import { format } from '../../../../common/helpers/dateTime';
import RewardItem from '../../../atoms/rewardItem';
import { InspireButton } from '../../../atoms/button';
import RewardDetailsModal from '../../../molecules/rewardDetailsModal';
import { CatalogCertificateView } from '../../../molecules/rewardDetailsModal/catalogCertificateView/catalogCertificateView';
import { usePurchaseOffer } from '../../../../common/hooks/usePurchaseOffer';
import { useAccount, useRewards } from '../../../../redux/hooks';
import { useRouter } from 'next/router';
import { ICertificateModel, TErrorCodeModel } from '../../../../@generated/webExpApi';
import { CatalogRedeemedCertificateView } from '../../../molecules/rewardDetailsModal/catalogRedeemedCertificateView/catalogRedeemedCertificateView';
import styles from '../../../molecules/rewardDetailsModal/rewardDetailsModal.module.css';
import { spaces2underscores } from '../../../../lib/gtm';
import SuccessErrorModal from '../../../molecules/successErrorModal';
import { Icons } from '../../../molecules/successErrorModal/successErrorModal';
import {
    EMAIL_NOT_VERIFIED_MESSAGE,
    EMAIL_NOT_VERIFIED_TITLE,
    ERROR_MODAL_CLOSE_BUTTON_TEXT,
} from '../../../../common/constants/rewards';

interface ICatalogItemProps {
    title: string;
    code: string;
    imageUrl: string;
    label: string;
    points: number;
    pointsBalance: number;
    hasEnoughPoints: boolean;
    activeCTAText: string;
    setLoading: (state: boolean) => void;
}

const CatalogItem = (props: ICatalogItemProps): JSX.Element => {
    const { title, code, imageUrl, label, points, pointsBalance, hasEnoughPoints, activeCTAText, setLoading } = props;

    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const {
        loading,
        purchaseOffer,
        lastPurchaseData,
        cleanLastPurchaseData,
        error,
        errorCode,
        cleanErrorCode,
    } = usePurchaseOffer();
    const { getCertificateById } = useRewards();
    const { accountInfo } = useAccount();
    const router = useRouter();
    const productLink = '/menu';
    const memberNumber = accountInfo.phone;

    let certificate: ICertificateModel = null;
    const nameNoSpaces = spaces2underscores(title);
    const GtmId = `CTA-RewardsDeals-Redeem_Certificate-${nameNoSpaces}`;
    const [openErrorSuccessModal, setOpenErrorSuccessModal] = useState<boolean>(false);

    if (lastPurchaseData) {
        certificate = getCertificateById(lastPurchaseData.certificateNumber);
    }

    useEffect(() => {
        if (isModalOpen) {
            setLoading(false);
        } else {
            setLoading(loading);
        }
    }, [loading, isModalOpen, setLoading]);

    useEffect(() => {
        if (loading || error || certificate?.code !== code) return;
        setIsModalOpen(true);
    }, [certificate, code, error, getCertificateById, loading]);

    useEffect(() => {
        if (error) {
            setLoading(false);
            setOpenErrorSuccessModal(true);
        }
    }, [error, setLoading]);

    const isEmailNotVerifiedCode = () => {
        return errorCode && errorCode === TErrorCodeModel.EmailNotVerified;
    };

    const onOrderClick = () => {
        cleanLastPurchaseData();
        setIsModalOpen(false);
        router.push(productLink);
    };

    const onRedeemClick = async () => {
        await purchaseOffer(code);
    };

    const onModalOpen = () => {
        setIsModalOpen(true);
    };

    const onModalClose = () => {
        setIsModalOpen(false);
        cleanLastPurchaseData();
    };

    const onErrorSuccessModalClose = () => {
        cleanErrorCode();
        setOpenErrorSuccessModal(false);
    };

    const renderButtons = () => {
        if (certificate) {
            return (
                <InspireButton className={styles.button} type="primary" text="order online" onClick={onOrderClick} />
            );
        }

        if (hasEnoughPoints) {
            return (
                <InspireButton
                    className={classnames(styles.button, { [styles.disabled]: loading })}
                    gtmId={GtmId}
                    type="primary"
                    text="redeem certificate"
                    onClick={onRedeemClick}
                />
            );
        }

        return <InspireButton className={styles.button} type="primary" text="back to rewards" onClick={onModalClose} />;
    };

    const renderPointsLabel = () => {
        const neededPoints = points - pointsBalance;

        if (hasEnoughPoints) {
            return (
                <span
                    className={styles.displayPoints}
                >{`You’re eligible to redeem this reward with ${points} points`}</span>
            );
        }

        return (
            <span
                className={classnames('t-paragraph-hint-strong', styles.errorDisplayPoints)}
            >{`${points} pts - you’re ${neededPoints} points away`}</span>
        );
    };

    const renderModalView = () => {
        if (certificate || (!!lastPurchaseData && !loading)) {
            const expirationLabel = certificate
                ? `Expires by ${format(new Date(certificate?.expirationDateTime), 'MM/dd/yyyy')}.`
                : '';

            return (
                <CatalogRedeemedCertificateView
                    imageUrl={imageUrl}
                    title={title}
                    memberNumber={memberNumber}
                    expirationLabel={expirationLabel}
                />
            );
        }

        return <CatalogCertificateView imageUrl={imageUrl} title={title} renderPointsLabel={renderPointsLabel} />;
    };

    return (
        <>
            <RewardItem
                title={title}
                imageUrl={imageUrl}
                label={label}
                hasEnoughPoints={hasEnoughPoints}
                activeCTAText={activeCTAText}
                activeCTAGtmId={GtmId}
                onActiveCTAClick={onRedeemClick}
                onViewDetails={onModalOpen}
                hideCTAButton={!hasEnoughPoints}
            />
            <RewardDetailsModal
                open={isModalOpen}
                onClose={onModalClose}
                modalTitle="Reward Details"
                renderButtons={renderButtons}
                isLoading={loading}
            >
                {renderModalView()}
            </RewardDetailsModal>
            <SuccessErrorModal
                open={openErrorSuccessModal}
                onClose={onErrorSuccessModalClose}
                closeButtonText={ERROR_MODAL_CLOSE_BUTTON_TEXT}
                icon={Icons.WARNING}
                errorCode={errorCode}
                title={isEmailNotVerifiedCode() ? EMAIL_NOT_VERIFIED_TITLE : null}
                description={isEmailNotVerifiedCode() ? EMAIL_NOT_VERIFIED_MESSAGE : null}
                isSuccess={false}
                modalStyles={styles.successErrorModalMobileViewStyleOverride}
                buttonRowStyles={styles.successErrorModalRowButtonStyleOverride}
                buttonStyles={styles.successErrorModalButtonStyleOverride}
            />
        </>
    );
};

export default React.memo(CatalogItem);
