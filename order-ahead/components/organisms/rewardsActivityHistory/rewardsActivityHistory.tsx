import React, { useState } from 'react';
import classnames from 'classnames';
import { format } from '../../../common/helpers/dateTime';
import { useRewards } from '../../../redux/hooks';
import BrandLoader from '../../atoms/BrandLoader';
import { RewardsActivityHistoryItem } from './rewardsActivityHistoryItem/rewardsActivityHistoryItem';
import { TActivityTypeModel } from '../../../@generated/webExpApi/models';
import { InspireLink, InspireLinkButton } from '../../atoms/link';
import Icon from '../../atoms/BrandIcon';
import styles from './rewardsActivityHistory.module.css';

interface RewardsActivityHistoryProps {
    isActivityPage?: boolean;
}

export const RewardsActivityHistory = ({ isActivityPage }: RewardsActivityHistoryProps): JSX.Element => {
    const countItemsForActivityPage = 10;
    const countItemsForAccountRewardsPage = 5;
    const paginationStep = 10;

    const { rewardsActivityHistory, rewardsActivityHistoryLoading } = useRewards();
    const countOfActivities = rewardsActivityHistory.length;
    const defaultShowItem = isActivityPage ? countItemsForActivityPage : countItemsForAccountRewardsPage;
    const [showItems, setShowItems] = useState<number>(defaultShowItem);

    const isLastItem = showItems >= countOfActivities;
    const shouldShowViewAllLink = countOfActivities > countItemsForAccountRewardsPage && !isLastItem;
    const shouldShowMoreLink = countOfActivities > countItemsForActivityPage && !isLastItem && isActivityPage;
    const displayedHistory = shouldShowViewAllLink
        ? rewardsActivityHistory.slice(0, showItems)
        : rewardsActivityHistory;

    const typeMap = {
        [TActivityTypeModel.Adjustment]: 'Adjustment',
        [TActivityTypeModel.BaseEarn]: 'Eat & Earn',
        [TActivityTypeModel.CancelledCertificate]: 'Canceled Certificate',
        [TActivityTypeModel.CertificateRedemption]: 'Redeemed Certificate',
        [TActivityTypeModel.Bonus]: 'Bonus',
    };

    const renderLink = () => {
        if (shouldShowMoreLink) {
            return (
                <InspireLinkButton
                    linkType="secondary"
                    className={styles.link}
                    onClick={() => setShowItems((state) => state + paginationStep)}
                >
                    <Icon className={styles.arrowIcon} icon="direction-down" size="xs" />
                    <span className={styles.linkText}>More</span>
                </InspireLinkButton>
            );
        }
        if (shouldShowViewAllLink) {
            return (
                <InspireLink className={styles.link} type="secondary" link={'/account/activity'}>
                    <Icon className={styles.arrowIcon} icon="direction-down" size="xs" />
                    <span className={styles.linkText}>View All</span>
                </InspireLink>
            );
        }
        return null;
    };

    if (rewardsActivityHistoryLoading) {
        return <BrandLoader className={styles.loader} />;
    }
    if (!(countOfActivities || rewardsActivityHistoryLoading)) return null;

    return (
        <section className={classnames(styles.container, { [styles.containerInPage]: isActivityPage })}>
            <h2 className={classnames('t-header-h2', styles.blockTitle)}>Points Activity</h2>
            {displayedHistory.map(
                ({ dateTime, type, description, points, offersApplied, transaction, certificatesApplied }) => {
                    const date = new Date(dateTime);
                    const dateText = format(date, 'MM/dd/yy');
                    const isFullView = type === TActivityTypeModel.BaseEarn;
                    const itemType = typeMap[type];
                    return (
                        <RewardsActivityHistoryItem
                            key={date.toISOString()}
                            type={itemType}
                            points={points}
                            date={dateText}
                            isFullView={isFullView}
                            transactionNumber={transaction?.number}
                            offers={offersApplied}
                            products={transaction?.products}
                            total={transaction?.total}
                            totalEligible={transaction?.totalEligible}
                            certificates={certificatesApplied}
                            itemDescription={description}
                        />
                    );
                }
            )}
            {renderLink()}
        </section>
    );
};
