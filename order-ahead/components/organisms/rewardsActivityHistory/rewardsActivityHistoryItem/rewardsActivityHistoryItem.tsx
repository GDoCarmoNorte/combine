import React, { Fragment, useState } from 'react';
import classnames from 'classnames';
import Collapse from '@material-ui/core/Collapse';
import {
    IActivityHistoryTransactionProductsModel,
    ICertificateAppliedModel,
    IOfferAppliedModel,
} from '../../../../@generated/webExpApi';
import styles from './rewardsActivityHistoryItem.module.css';

interface IRewardsActivityHistoryItem {
    type: string;
    date: string;
    itemDescription?: string;
    points: number;
    isFullView?: boolean;
    transactionNumber?: string;
    offers: IOfferAppliedModel[];
    certificates: ICertificateAppliedModel[];
    products?: IActivityHistoryTransactionProductsModel[];
    total?: number;
    totalEligible?: number;
}

export const RewardsActivityHistoryItem = (props: IRewardsActivityHistoryItem): JSX.Element => {
    const {
        type,
        date,
        itemDescription,
        points,
        isFullView,
        transactionNumber,
        offers,
        certificates,
        products,
        total,
        totalEligible,
    } = props;
    const [expanded, setExpanded] = useState(false);

    const renderProductItem = (description: string, price: number): JSX.Element => (
        <div className={classnames('t-paragraph-hint-strong', styles.productContainer)}>
            <p className={styles.itemLabel}>{description}</p>
            <p className={styles.priceLabel}>${price}</p>
        </div>
    );
    const renderModifierItems = (modifiers: IActivityHistoryTransactionProductsModel[]): JSX.Element[] => {
        if (!modifiers.length) return null;
        return modifiers.map(
            ({ lineItemId, description, price, modifiers }): JSX.Element => {
                if (!price) return null;
                return (
                    <Fragment key={lineItemId}>
                        {renderProductItem(description, price)}
                        {renderModifierItems(modifiers)}
                    </Fragment>
                );
            }
        );
    };

    if (isFullView) {
        return (
            <div className={styles.expandedContainer}>
                <div className={classnames(styles.itemContainer, styles.expandedItemContainer)}>
                    <div className={styles.infoBlock}>
                        <span className={classnames('t-subheader-smaller', styles.date)}>{date}</span>
                        <span
                            role="button"
                            onClick={() => setExpanded((state) => !state)}
                            className={classnames('t-paragraph-hint-strong', styles.expandedType)}
                        >
                            {type}
                        </span>
                    </div>
                    <div className={classnames('t-subheader-smaller')}>{`${points} Points`}</div>
                </div>
                <Collapse in={expanded}>
                    <div className={styles.expandedBlock}>
                        <section className={styles.heading}>
                            <h3 className={classnames('t-header-h3', styles.headingPoints)}>{`${points} POINTS`}</h3>
                            <p className={classnames('t-paragraph-hint', styles.headingLabel)}>FROM THIS OFFER</p>
                        </section>
                        <section className={styles.transactionBlock}>
                            <div className={classnames('t-paragraph-hint', styles.transactionInfo)}>
                                <p>TRANSACTION NUMBER: {transactionNumber}</p>
                                <p className={styles.typeAndPoints}>{`${type}: ${points} POINTS`}</p>
                                {offers.map(({ code, name }) => (
                                    <p key={code}>{name}</p>
                                ))}
                            </div>
                            <div className={styles.transactionProducts}>
                                <div className={classnames('t-paragraph-hint', styles.productLabelContainer)}>
                                    <p className={styles.itemLabel}>ITEM</p>
                                    <p className={styles.priceLabel}>PRICE</p>
                                </div>
                                {products.map(({ lineItemId, description, price, modifiers }) => (
                                    <Fragment key={lineItemId}>
                                        {renderProductItem(description, price)}
                                        {renderModifierItems(modifiers)}
                                    </Fragment>
                                ))}
                            </div>
                            <div className={styles.transactionTotals}>
                                <div className={classnames('t-paragraph-hint', styles.totalLabelContainer)}>
                                    <p className={styles.itemLabel}>TOTAL SPEND</p>
                                    <p className={styles.priceLabel}>{total}</p>
                                </div>
                                <div className={classnames('t-paragraph-hint-strong', styles.productContainer)}>
                                    <p className={styles.itemLabel}>POINTS ELIGIBLE SPEND</p>
                                    <p className={styles.priceLabel}>{totalEligible}</p>
                                </div>
                            </div>
                        </section>
                        <section className={styles.rewardsBlock}>
                            {certificates.length > 0 && (
                                <p className={classnames('t-paragraph-hint', styles.rewardsLabel)}>REWARD APPLIED</p>
                            )}
                            {certificates.map(({ title, number }) => (
                                <p key={number} className={classnames('t-paragraph-hint-strong', styles.rewardsTitle)}>
                                    {title}
                                </p>
                            ))}
                        </section>
                    </div>
                </Collapse>
            </div>
        );
    }

    return (
        <div className={styles.itemContainer}>
            <div className={styles.infoBlock}>
                <span className={classnames('t-subheader-smaller', styles.date)}>{date}</span>
                <span className={classnames('t-paragraph-hint ', styles.type)}>
                    {type} - {itemDescription}
                </span>
            </div>
            <div className={classnames('t-subheader-smaller', styles.points)}>{`${points} Points`}</div>
        </div>
    );
};
