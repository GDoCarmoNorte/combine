import React, { memo } from 'react';
import AlertBanners from '../../alertBanners/alertBanners';
import Navigation from '../../navigation';
import {
    IAlertBannerFields,
    INavigationFields,
    IUserAccountMenuFields,
} from '../../../../@generated/@types/contentful';
import SkipLink from '../../../atoms/skipLink';
import { InspireCmsEntryCollection, InspireCmsEntry } from '../../../../common/types';

interface HeaderProps {
    alertBanners?: InspireCmsEntryCollection<IAlertBannerFields>;
    webNavigation?: InspireCmsEntry<INavigationFields>;
    userAccountMenu?: InspireCmsEntry<IUserAccountMenuFields>;
    isPreviewMode?: boolean;
    titleTooltip?: string;
}

function BaseHeader({
    alertBanners,
    webNavigation,
    userAccountMenu,
    isPreviewMode,
    titleTooltip,
}: HeaderProps): JSX.Element {
    return (
        <>
            <SkipLink />
            <div className="sticky-top" id="sticky-top">
                {alertBanners && <AlertBanners alertBanners={alertBanners} isPreviewMode={isPreviewMode} />}
                {webNavigation && (
                    <Navigation
                        navigation={webNavigation}
                        userAccountMenu={userAccountMenu}
                        titleTooltip={titleTooltip}
                    />
                )}
            </div>
        </>
    );
}

export default memo(BaseHeader);
