import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { IAlertBannerFields, INavigationFields } from '../../../../@generated/@types/contentful';
import classnames from 'classnames';

import Icon from '../../../atoms/BrandIcon';

import AlertBanners from '../../alertBanners/alertBanners';
import SkipLink from '../../../atoms/skipLink';
import Logo from '../../../atoms/Logo';
import { getImageUrl } from '../../../../common/helpers/contentfulImage';
import { logoDesktop } from './constants';
import styles from './index.module.css';
import { InspireCmsEntryCollection } from '../../../../common/types';
import { useMediaQuery } from '@material-ui/core';

interface IPaymentHeaderProps {
    alertBanners?: InspireCmsEntryCollection<IAlertBannerFields>;
    navigation: InspireCmsEntryCollection<INavigationFields>;
    isPaymentProcessingShouldBeShown: boolean;
}

export default function PaymentHeader(props: IPaymentHeaderProps): JSX.Element {
    const { alertBanners, navigation, isPaymentProcessingShouldBeShown } = props;
    const router = useRouter();
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const asset = getImageUrl(webNavigation?.fields?.logo, 1500);
    const isMobile = useMediaQuery('(max-width: 960px)');
    const shouldNotShowBackBtn = isMobile && isPaymentProcessingShouldBeShown;

    const handleGoBack = (e) => {
        e.preventDefault();

        router.back();
    };

    return (
        <>
            <SkipLink />
            <div className="sticky-top">
                {alertBanners && <AlertBanners alertBanners={alertBanners} />}
                <nav className={styles.navigation}>
                    <div className={styles.logoContainer}>
                        <Link href="/">
                            <a className={styles.logo}>
                                <Logo urlLogo={asset} urlLogoDesktop={logoDesktop} className={styles.logoImage} />
                            </a>
                        </Link>
                    </div>
                    {shouldNotShowBackBtn ? null : (
                        <a
                            tabIndex={0}
                            onClick={handleGoBack}
                            onKeyPress={handleGoBack}
                            className={styles.backContainer}
                        >
                            <Icon icon="direction-left" size="xs" className={styles.icon} />
                            <span className={classnames('link-primary-active', styles.back)}>Back</span>
                        </a>
                    )}
                </nav>
            </div>
        </>
    );
}
