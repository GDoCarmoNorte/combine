import React from 'react';
import Link from 'next/link';
import { INavigationFields } from '../../../../@generated/@types/contentful';
import Logo from '../../../atoms/Logo';
import { getImageUrl } from '../../../../common/helpers/contentfulImage';
import LocationBlock from '../../navigation/locationSelect/locationBlock';
import { InspireCmsEntryCollection } from '../../../../common/types';

import styles from './index.module.css';

interface IPaymentHeaderProps {
    navigation: InspireCmsEntryCollection<INavigationFields>;
    locationName?: string;
}

export default function ContactLessHeader(props: IPaymentHeaderProps): JSX.Element {
    const { navigation, locationName } = props;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const asset = getImageUrl(webNavigation?.fields?.logo, 1500);

    return (
        <div className="sticky-top" aria-label="Contactless Header">
            <nav className={styles.navigation}>
                <div className={styles.logoContainer}>
                    <Link href="/">
                        <a className={styles.logo}>
                            <Logo urlLogo={asset} className={styles.logoImage} />
                        </a>
                    </Link>
                </div>
                {locationName && (
                    <div className={styles.headerLocation}>
                        <LocationBlock storeStatusText="Location" storeDisplayName={locationName} mobileOnly />
                    </div>
                )}
            </nav>
        </div>
    );
}
