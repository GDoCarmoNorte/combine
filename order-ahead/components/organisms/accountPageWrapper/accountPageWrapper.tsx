import React, { useMemo } from 'react';
import Head from 'next/head';
import classnames from 'classnames';

import { IGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import Footer from '../footer';
import AccountNavigation from '../accountNavigation';
import BaseHeader from '../header/baseHeader';

import styles from './accountPageWrapper.module.css';
import { useRouter } from 'next/router';

import { PageContentWrapper } from '../../sections/PageContentWrapper';
import { DEALS, REWARDS } from '../../../common/constants/account';
import useRewards from '../../../redux/hooks/useRewards';
import { TOfferStatusModel } from '../../../@generated/webExpApi';
import { getAccountNavLinks } from '../../../common/helpers/accountHelper';
import { IAccountHeader } from '../../../@generated/@types/contentful';
import AccountHeader from '../accountHeader';
import { useConfiguration } from '../../../common/hooks/useConfiguration';

export interface IAccountPage {
    globalProps: IGlobalContentfulProps;
    children: React.ReactElement | React.ReactElement[];
    containerClassName?: string;
    pageWrapperClassName?: string;
    titleTooltip?: string;
    accountHeader: IAccountHeader;
}

export default function AccountPageWrapper(props: IAccountPage): JSX.Element {
    const { alertBanners, navigation, footer, userAccountMenu } = props.globalProps;
    const { accountHeader } = props;
    const { configuration } = useConfiguration();

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const router = useRouter();

    const { offers, totalCount, rewardsActivityHistory } = useRewards();

    const navigationLinks = useMemo(
        () =>
            getAccountNavLinks(userAccountMenu, rewardsActivityHistory.length, configuration).map((item) => {
                if (item.type === DEALS) {
                    return {
                        ...item,
                        count: offers.filter((offer) => offer.status === TOfferStatusModel.Open).length,
                    };
                }

                if (item.type === REWARDS) {
                    return {
                        ...item,
                        count: totalCount,
                    };
                }

                return item;
            }),
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [offers]
    );

    return (
        <>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                userAccountMenu={userAccountMenu}
                titleTooltip={props.titleTooltip}
            />
            <AccountHeader accountHeader={accountHeader} />
            <div className={styles.accountNavigation}>
                <AccountNavigation links={navigationLinks} currentPath={router.pathname} />
            </div>
            <div className={classnames(styles.container, props.containerClassName)}>
                <Head>
                    {/* TODO: title and description should be configured via contentful */}
                    <title>Account</title>
                    <meta name="description" content={'account description'} />
                </Head>
                <PageContentWrapper className={props.pageWrapperClassName} ariaLabel="Account Page Content">
                    {props.children}
                </PageContentWrapper>
            </div>
            <div id="my-teams-portal" className={styles.myTeamsPortal} />
            {footer && <Footer footer={footer} />}
        </>
    );
}
