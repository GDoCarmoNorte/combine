import React, { useState } from 'react';
import styles from './paymentInfoContainer.module.css';
import { IPaymentRequest } from '../../../clientOnly/paymentInfoContainer/paymentInfo';
import { IGiftCard } from '../../../clientOnly/paymentInfoContainer/types';
import {
    IPaymentMethod,
    IPaymentState,
    TInitialPaymentTypes,
} from '../../../clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import PaymentTypeDropdownContainer from '../../../clientOnly/paymentInfoContainer/paymentTypeDropdown/paymentTypeDropdownContainer';
import { ALL_INITIAL_PAYMENT_TYPES } from '../../../clientOnly/paymentInfoContainer/paymentTypeDropdown/constants';
import PaymentSection from '../../../clientOnly/paymentInfoContainer/paymentSection';
import GiftCardSection from '../../../clientOnly/paymentInfoContainer/giftCardSection/giftCardSection';
import { isGiftCardPayEnabled } from '../../../../lib/getFeatureFlags';
import { InspireButton } from '../../../atoms/button';
import { ButtonTypeEnumModel, FulfillmentTypeEnumModel } from '../../../../@generated/webExpApi';

interface IPaymentInfoProps {
    locationId: string;
    totalAmount: number;
    onSubmit?: (request: IPaymentRequest, giftCards?: IGiftCard[]) => Promise<void>;
    onError?: (error: { message?: string; header?: string; description?: string; textError?: string }) => void;
    onFormStatusChange?: (isValid: boolean) => void;
    submitError?: Error;
    fulfillmentType?: FulfillmentTypeEnumModel;
    isActive?: boolean;
}

const PaymentInfoContainer: React.FC<IPaymentInfoProps> = ({
    locationId,
    totalAmount,
    onSubmit,
    onError,
    onFormStatusChange,
    submitError,
    fulfillmentType,
    isActive = true,
}) => {
    const [paymentInfo, setPaymentInfo] = useState<IPaymentState>({
        type: TInitialPaymentTypes.CREDIT_OR_DEBIT,
    });
    const supportedPaymentMethods: IPaymentMethod[] = [ALL_INITIAL_PAYMENT_TYPES[TInitialPaymentTypes.CREDIT_OR_DEBIT]];

    const [giftCards, setGiftCards] = useState<IGiftCard[]>([]);
    const totalGiftCardBalance = giftCards.reduce((total: number, curr: IGiftCard) => total + (curr.balance || 0), 0);
    const totalToPay =
        totalGiftCardBalance >= totalAmount ? 0 : parseFloat((totalAmount - totalGiftCardBalance).toFixed(2));
    const isPaymentRequired = totalToPay > 0;

    const handleSubmitGiftCardsOnlyPay = () => {
        // GTM fire event gift cards pay
        return onSubmit(undefined, giftCards);
    };

    const handleSubmitPay = (request: IPaymentRequest) => {
        // if(giftCards.length) => GTM fire event combined pay
        // else => GTM fire event credit card pay
        return onSubmit(request, giftCards);
    };

    return (
        <>
            {isGiftCardPayEnabled() && (
                <GiftCardSection
                    setGiftCards={setGiftCards}
                    giftCards={giftCards}
                    dineIn
                    locationId={locationId}
                    totalToPay={totalToPay}
                />
            )}
            {isPaymentRequired ? (
                <>
                    <PaymentTypeDropdownContainer
                        onChange={setPaymentInfo}
                        selectedMethod={paymentInfo}
                        paymentOptions={supportedPaymentMethods}
                    />
                    {paymentInfo.type === TInitialPaymentTypes.CREDIT_OR_DEBIT && (
                        <PaymentSection
                            isActive={isActive}
                            onSubmit={handleSubmitPay}
                            onError={onError}
                            onFormStatusChange={onFormStatusChange}
                            submitError={submitError}
                            totalPrice={totalToPay}
                            paymentType={TInitialPaymentTypes.CREDIT_OR_DEBIT}
                            inputLabelClass={styles.customInputLabel}
                            locationId={locationId}
                            fulfillmentType={fulfillmentType}
                        />
                    )}
                </>
            ) : (
                <InspireButton
                    className={styles.buttonPay}
                    onClick={handleSubmitGiftCardsOnlyPay}
                    text={ButtonTypeEnumModel.Pay}
                    fullWidth
                />
            )}
        </>
    );
};

export default PaymentInfoContainer;
