import React, { useEffect, useMemo, useState } from 'react';
import { useRouter } from 'next/router';
import classNames from 'classnames';
import { IGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import DiningInfo from './diningInfo';
import ContactlessHeader from '../../../components/organisms/header/contactlessHeader';
import { PageContentWrapper } from '../../sections/PageContentWrapper';
import { InspireButton } from '../../atoms/button';
import { FulfillmentTypeEnumModel, TErrorCodeModel } from '../../../@generated/webExpApi';
import OrderSummary from './orderSummary';
import PaymentInfoContainer from './paymentInfoContainer';
import styles from './contactlessPay.module.css';
import { IPaymentRequest } from '../../clientOnly/paymentInfoContainer/paymentInfo';
import { isFraudCheckEnabled } from '../../../lib/getFeatureFlags';
import { injectScript } from '../../../lib/injectScriptOnce';
import InfoBanner from '../../atoms/infoBanner';
import { ORDER_INFO_BACKGROUND_COLOR } from '../confirmation/constants';
import { IGiftCard } from '../../clientOnly/paymentInfoContainer/types';
import { useDineInSubmitOrder } from '../../../redux/hooks/useDineInSubmitOrder';
import ContactlessPayError from '../../clientOnly/contactlessPayError';
import WarningModal from '../warningModal';
import { DEFAULT, errorOrderMap, warningDescription, errorPaymentMap } from './constantsContactlessPay';
import { useDineInOrder } from '../../../redux/hooks/useDineInOrder';
import PaymentProcessing from '../checkout/paymentProcessing/paymentProcessing';
import Loader from '../../atoms/Loader';

interface IContactlessPayProps extends IGlobalContentfulProps {
    isConfirmationPage?: boolean;
}

export enum statusApplyingTips {
    apply = 'apply',
    noapply = 'noapply',
    initial = 'initial',
}

const ContactlessPay = (props: IContactlessPayProps): JSX.Element => {
    const { navigation, isConfirmationPage } = props;
    const [kountSessionId, setKountSessionId] = useState<string>();
    const [tipsAmount, setTipsAmount] = useState(0);
    const { query, replace } = useRouter();
    const [open, setOpen] = useState(false);
    const [paymentError, setPaymentError] = useState(null);
    const [isSubmitPayment, setIsSubmitPayment] = useState(false);

    const [showWarningModal, setShowWarningModal] = useState(false);

    const [applyTips, setApplyTips] = useState(statusApplyingTips.initial);
    const [currentTipsAmount, setCurrentTipsAmount] = useState('');

    const { locationId, orderId } = query;

    const { getOrder, loading, orderLocationId, error: orderError, order } = useDineInOrder();

    const totalAmount = useMemo(() => {
        return parseFloat(((order?.subTotalAfterDiscounts || 0) + (order?.tax || 0) + (tipsAmount || 0)).toFixed(2));
    }, [order, tipsAmount]);

    const { submitOrder, loading: isSubmitOrderLoading } = useDineInSubmitOrder();

    const isPaymentRequired = totalAmount !== 0;

    useEffect(() => {
        if (isFraudCheckEnabled()) {
            const dataCollectorScriptSrc = `${process.env.NEXT_PUBLIC_KOUNT_DATA_COLLECTOR_URL}${process.env.NEXT_PUBLIC_KOUNT_MERCHANT_ID}`;
            injectScript(dataCollectorScriptSrc).then(() => {
                window.ka?.sessionId && setKountSessionId(window.ka.sessionId);
            });
        }
    }, []);

    useEffect(() => {
        if (typeof locationId === 'string' && typeof orderId === 'string') {
            getOrder({ orderId, locationId });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [locationId, orderId]);

    useEffect(() => {
        if (showWarningModal) {
            setApplyTips(statusApplyingTips.initial);
        }
    }, [showWarningModal]);

    const redirectUrl = useMemo(() => {
        if (!isConfirmationPage && order?.isOpen === false) {
            return `/contactless-pay/confirmation?locationId=${locationId}&orderId=${orderId}`;
        } else if (isConfirmationPage && order?.isOpen) {
            return `/contactless-pay/?locationId=${locationId}&orderId=${orderId}`;
        }
        return null;
    }, [isConfirmationPage, locationId, order?.isOpen, orderId]);

    useEffect(() => {
        if (redirectUrl) {
            replace(redirectUrl);
        }
    }, [redirectUrl, replace]);

    const handleSubmit = async (paymentRequest: IPaymentRequest, giftCards?: IGiftCard[]): Promise<void> => {
        const orderPayload = {
            orderInfo: {
                ...order,
                orderId: orderId as string,
            },
            paymentRequest,
            deviceId: kountSessionId,
            tipAmount: tipsAmount,
            giftCards: giftCards,
        };
        setIsSubmitPayment(true);
        setPaymentError(null);

        try {
            await submitOrder(orderPayload);
            replace(`/contactless-pay/confirmation?locationId=${locationId}&orderId=${orderId}`);
        } catch (e) {
            setPaymentError(e);
            setOpen(true);
            setIsSubmitPayment(false);
        }
    };

    const getOrderError = errorOrderMap[orderError?.code] || errorOrderMap[DEFAULT];

    if (orderError) return <ContactlessPayError errorType={getOrderError.errorType} />;

    const error = errorPaymentMap[paymentError?.code] || errorPaymentMap[DEFAULT];

    const handleCloseErrorModal = () => {
        setOpen(false);

        if (paymentError?.code === TErrorCodeModel.CheckAmountMismatch) {
            getOrder({ orderId, locationId });
        }
    };

    const handleCloseWarningModal = () => setShowWarningModal(false);

    const handleApplyTips = () => {
        setApplyTips(statusApplyingTips.apply);
        handleCloseWarningModal();
    };

    const handleNoApplyTips = () => {
        setApplyTips(statusApplyingTips.noapply);
        handleCloseWarningModal();
    };

    const headerOverlayClasses = classNames(styles.headerOverlay, {
        [styles.headerOverlayHidden]: !isSubmitPayment,
    });

    const isLoading = loading || redirectUrl;

    return (
        <>
            <div className={styles.paymentHeaderContainer}>
                <ContactlessHeader locationName={!isLoading && order?.location?.displayName} navigation={navigation} />
                <div className={headerOverlayClasses} />
            </div>
            <div
                className={classNames(styles.container, {
                    [styles.containerHidden]: !isSubmitPayment,
                })}
            >
                {isSubmitPayment && <PaymentProcessing />}
            </div>
            <PageContentWrapper
                className={classNames(styles.container, {
                    [styles.containerHidden]: isSubmitPayment,
                })}
                ariaLabel="Contactless Page"
            >
                {order && !isLoading && (
                    <>
                        {isConfirmationPage && (
                            <div className={styles.orderHeader}>
                                <InfoBanner
                                    title="payment received"
                                    mainText="thank you"
                                    description={`Confirmation #${orderId}`} // TODO orderId or confirmationId
                                    backgroundColor={ORDER_INFO_BACKGROUND_COLOR}
                                />
                            </div>
                        )}
                        <div className={styles.contactlessPayForm}>
                            <div className={styles.formFields}>
                                <section aria-label="Dining Info Section" className={styles.section}>
                                    <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Dining info</h4>
                                    <DiningInfo
                                        server={order.serverName}
                                        table={order.table}
                                        guests={order.guestsCount}
                                        orderType="Dine In"
                                        // @TODO: orderNumber is temporary posOrderId
                                        orderNumber={order.posOrderId}
                                        dateTime={order.dateTime}
                                    />
                                </section>
                                <section aria-label="Review Order Section" className={styles.section}>
                                    <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>
                                        Order summary
                                    </h4>
                                    <OrderSummary
                                        order={order}
                                        setTipsAmount={setTipsAmount}
                                        tipsAmount={tipsAmount}
                                        showTips={!isConfirmationPage && isPaymentRequired}
                                        showCardNumber={isConfirmationPage}
                                        isPaymentRequired={isPaymentRequired}
                                        showWarningModal={showWarningModal}
                                        setShowWarningModal={setShowWarningModal}
                                        applyTips={applyTips}
                                        setCurrentTipsAmount={setCurrentTipsAmount}
                                    />
                                </section>
                                {!isConfirmationPage && isPaymentRequired && (
                                    <section className={styles.section}>
                                        <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>
                                            Payment info
                                        </h4>
                                        <PaymentInfoContainer
                                            locationId={orderLocationId}
                                            fulfillmentType={FulfillmentTypeEnumModel.Dinein}
                                            totalAmount={totalAmount}
                                            onSubmit={handleSubmit}
                                            isActive={!isSubmitOrderLoading}
                                        />
                                    </section>
                                )}
                            </div>
                            {!isPaymentRequired && (
                                <section className={classNames(styles.section, styles.noPaymentRequired)}>
                                    <InspireButton text="No payment required" fullWidth disabled />
                                </section>
                            )}
                        </div>

                        {isConfirmationPage && (
                            <div className={classNames('t-subheader-small', styles.confirmationFooter)}>
                                thank you and we’ll see you next time
                            </div>
                        )}
                    </>
                )}
                {isLoading && (
                    <div className={styles.loader}>
                        <Loader size={20} />
                    </div>
                )}
                {open && (
                    <WarningModal
                        description={error}
                        open={open}
                        onModalClose={handleCloseErrorModal}
                        primaryButton={{
                            text: error.textButton,
                            onClick: handleCloseErrorModal,
                        }}
                    />
                )}
                {showWarningModal && (
                    <WarningModal
                        description={warningDescription(currentTipsAmount)}
                        open={showWarningModal}
                        primaryButton={{
                            text: 'Yes',
                            onClick: handleApplyTips,
                        }}
                        secondaryButton={{
                            text: 'cancel',
                            onClick: handleNoApplyTips,
                        }}
                    />
                )}
            </PageContentWrapper>
        </>
    );
};

export default ContactlessPay;
