import { TErrorCodeModel } from '../../../@generated/webExpApi/models/TErrorCodeModel';
import { TContactlessPayErrorTypes } from '../../clientOnly/contactlessPayError/types';
import { IWarningModalDescription } from '../warningModal';

const PAYMENT_ERROR = 'PAYMENT ERROR';

enum TErrorButton {
    BACK_TO_PAYMENT = 'back to payment',
    SEE_UPDATED_CHECK = 'see updated check',
}

export const DEFAULT = 'DEFAULT';

export const errorOrderMap: { [key in TErrorCodeModel | string]: { errorType: TContactlessPayErrorTypes } } = {
    [TErrorCodeModel.CheckNotFound]: { errorType: TContactlessPayErrorTypes.DEFAULT },
    [DEFAULT]: { errorType: TContactlessPayErrorTypes.DISABLED },
};

export const errorPaymentMap: { [key in TErrorCodeModel | string]: IWarningModalDescription } = {
    [TErrorCodeModel.Generic]: {
        title: PAYMENT_ERROR,
        message: 'Sorry, an unexpected error occurred. Please resubmit your payment.',
        textButton: TErrorButton.BACK_TO_PAYMENT,
    },
    [TErrorCodeModel.CheckAmountMismatch]: {
        title: 'Your check has been updated',
        textButton: TErrorButton.SEE_UPDATED_CHECK,
    },
    [TErrorCodeModel.Validation]: {
        title: PAYMENT_ERROR,
        message: 'Your payment has not been processed. Please try again later.',
        textButton: TErrorButton.BACK_TO_PAYMENT,
    },
    [DEFAULT]: {
        title: PAYMENT_ERROR,
        message: 'We are experiencing technical difficulties. Please try again or ask your server for assistance.',
        textButton: TErrorButton.BACK_TO_PAYMENT,
    },
};

export const warningDescription = (tips: string): IWarningModalDescription => ({
    title: 'tip amount is more than subtotal',
    message: `Your tip is $${tips}. Is this correct?`,
});
