import React, { useState } from 'react';
import ReviewOrderTotal from '../../../atoms/ReviewOrderTotal';
import { formatPrice } from '../../../../lib/domainProduct';
import OrderItem from './orderItem';
import { IDineInOrderDetailsModel } from '../../../../@generated/webExpApi';
import styles from './orderSummary.module.css';
import TipsBlock from '../../checkout/tipsBlock/tipsBlock';
import { statusApplyingTips } from '../contactlessPay';

interface IOrderSummaryProps {
    order?: IDineInOrderDetailsModel;
    setTipsAmount: (tips: number) => void;
    tipsAmount?: number;
    showTips?: boolean;
    showCardNumber?: boolean;
    isPaymentRequired?: boolean;
    showWarningModal?: boolean;
    setShowWarningModal?: (showWarningModal: boolean) => void;
    applyTips?: statusApplyingTips;
    setApplyTips?: (applyTips: statusApplyingTips) => void;
    setCurrentTipsAmount?: (amount: string) => void;
}

const OrderSummary: React.FC<IOrderSummaryProps> = ({
    order,
    showTips,
    showCardNumber,
    tipsAmount,
    isPaymentRequired,
    setTipsAmount,
    showWarningModal,
    setShowWarningModal,
    applyTips,
    setCurrentTipsAmount,
}) => {
    const [tipsPercentage, setTipsPercentage] = useState(0);

    return (
        <div>
            <div className={styles.itemWrapper}>
                {order.products.map((product, i) => (
                    <OrderItem key={i} product={product} />
                ))}
            </div>
            {showTips && (
                <div className={styles.tipsBlock}>
                    <TipsBlock
                        subtotal={order.subTotalAfterDiscounts}
                        setTipsAmount={setTipsAmount}
                        setTipsPercentage={setTipsPercentage}
                        tipsAmount={tipsAmount}
                        isContactless={true}
                        showWarningModal={showWarningModal}
                        setShowWarningModal={setShowWarningModal}
                        applyTips={applyTips}
                        setCurrentTipsAmount={setCurrentTipsAmount}
                    />
                </div>
            )}
            <div className={styles.orderTotal}>
                <ReviewOrderTotal
                    certificateDiscount={!isPaymentRequired ? formatPrice(-order.certificateDiscount) : null}
                    subtotal={formatPrice(order.subTotalAfterDiscounts)}
                    totalTax={formatPrice(order.tax)}
                    totalTip={isPaymentRequired ? formatPrice(tipsAmount) : null}
                    totalPrice={formatPrice(order.subTotalAfterDiscounts + order.tax + tipsAmount)}
                    tipPercentage={isPaymentRequired ? tipsPercentage : null}
                    cardNumber={order.payments?.[0]?.lastFourDigits}
                    showCardNumber={showCardNumber}
                />
            </div>
        </div>
    );
};

export default OrderSummary;
