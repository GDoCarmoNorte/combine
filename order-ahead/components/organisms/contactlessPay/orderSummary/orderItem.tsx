import React from 'react';
import classNames from 'classnames';
import TextWithTrademark from '../../../atoms/textWithTrademark';
import { IDineInOrderDetailsItemModel } from '../../../../@generated/webExpApi';
import { formatPrice } from '../../../../lib/domainProduct';
import styles from './orderItem.module.css';

interface IProps {
    product: IDineInOrderDetailsItemModel;
}

const OrderItem: React.FC<IProps> = ({ product }) => {
    return (
        <div className={classNames(styles.wrapper, 't-product-title')}>
            <div className={styles.quantity}>{product.quantity}x</div>
            <TextWithTrademark tag="div" text={product?.description} />
            <div className={styles.price}>{formatPrice(product.price)}</div>
        </div>
    );
};

export default OrderItem;
