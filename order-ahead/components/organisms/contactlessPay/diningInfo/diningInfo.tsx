import React from 'react';
import { formatDate, formatHours } from '../../../../common/helpers/dateTime';

import styles from './diningInfo.module.css';

interface IProps {
    server: string;
    table: string;
    guests: number;
    orderType: string;
    orderNumber: string | number;
    dateTime: string;
}

const DiningInfo: React.FC<IProps> = ({ server, table, guests, orderType, orderNumber, dateTime }) => {
    return (
        <div className={styles.diningInfo}>
            <div>
                <div>
                    <span className="t-paragraph">Server: </span>
                    <span className="t-paragraph-strong">{server}</span>
                </div>
                <div>
                    <span className="t-paragraph">Table: </span>
                    <span className="t-paragraph-strong">{table}</span>
                </div>
                <div>
                    <span className="t-paragraph">Guests: </span>
                    <span className="t-paragraph-strong">{guests}</span>
                </div>
                <div>
                    <span className="t-paragraph">Order type: </span>
                    <span className="t-paragraph-strong">{orderType}</span>
                </div>
                <div>
                    <span className="t-paragraph">Order number: </span>
                    <span className="t-paragraph-strong">{orderNumber}</span>
                </div>
            </div>
            <div className={styles.dateSection}>
                <div className="t-paragraph">{formatDate(dateTime)}</div>
                <div className="t-paragraph">{formatHours(dateTime)}</div>
            </div>
        </div>
    );
};

export default DiningInfo;
