import React from 'react';
import BrandLoader from '../../../atoms/BrandLoader';
import styles from './paymentProcessing.module.css';
import { Typography } from '@material-ui/core';
import classNames from 'classnames';

interface IPaymentProcessingProps {
    hideHeader?: boolean;
}

export default function PaymentProcessing(props: IPaymentProcessingProps): JSX.Element {
    const { hideHeader } = props;
    return (
        <div className={styles.container}>
            <BrandLoader className={styles.loader} />
            <div className={styles.textContainer}>
                {!hideHeader && (
                    <Typography className={classNames('t-header-h1', styles.subHeader)}>
                        Payment processing...
                    </Typography>
                )}
                <Typography className={classNames('t-paragraph', styles.description)}>
                    Please do not close this tab or refresh your browser.
                </Typography>
            </div>
        </div>
    );
}
