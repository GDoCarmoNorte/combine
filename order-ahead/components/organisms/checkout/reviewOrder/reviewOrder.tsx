import React from 'react';

import ReviewOrderItem from './reviewOrderItem';
import ReviewOrderTotal from '../../../atoms/ReviewOrderTotal';
import { IProductItemById } from '../../../../common/services/globalContentfulProps';

import styles from './reviewOrder.module.css';
import { formatPrice } from '../../../../lib/domainProduct';
import { useTallyItemsWithPricesAndCalories } from '../../../../redux/hooks/domainMenu';
import { TallyResponseModel } from '../../../../@generated/webExpApi';
import TipsBlock from '../tipsBlock/tipsBlock';
import { capitalizeString } from '../../../../common/helpers/capitalizeString';
import { ICheckoutLegalFields } from '../../../../@generated/@types/contentful';

const orderTypeLegalMessageMap = {
    DELIVERY: 'deliveryFeeLegalMessage',
    SERVICE: 'serviceFeeLegalMessage',
    TAKEOUT: 'takeoutServiceFeeLegalMessage',
};

interface IReviewOrderProps extends React.HTMLAttributes<HTMLDivElement> {
    order?: TallyResponseModel;
    productsById: IProductItemById;
    withTips: boolean;
    tipsAmount: number;
    setTipsAmount?: (tips: number) => void;
    checkoutLegal?: ICheckoutLegalFields;
}

const ReviewOrder = (props: IReviewOrderProps): JSX.Element => {
    const { order, productsById, withTips, tipsAmount, setTipsAmount, checkoutLegal, ...rest } = props;

    const tallyItemsWithPrices = useTallyItemsWithPricesAndCalories(order?.products || [], false);

    const discount = tallyItemsWithPrices.reduce((acc, curr) => {
        return curr.priceType ? (curr.productData.price - curr.price) * curr.quantity + acc : acc;
    }, 0);

    const offerDiscount =
        order?.discounts?.discountDetails?.reduce((acc, curr) => {
            return (
                acc + curr?.appliedItems?.reduce((sum, item) => sum + item.amount * item.quantity, 0) ||
                acc + curr.amount
            );
        }, 0) || 0;
    const freeDeliveryDiscount = order?.discounts?.discountDetails?.find((discount) =>
        discount?.name?.match(/free delivery/i)
    );

    const totalDiscount = discount + offerDiscount - (freeDeliveryDiscount?.amount || 0);
    const subTotalBeforeDiscounts = order?.subTotalBeforeDiscounts;
    const formattedDiscount = !!totalDiscount && formatPrice(totalDiscount);
    const formattedFees = order?.fees
        ? order.fees
              .filter((fee) => !(fee.type === 'DELIVERY' && freeDeliveryDiscount))
              .map((fee) => ({
                  ...fee,
                  amount: formatPrice(fee.amount),
                  name: capitalizeString(fee.name || fee.type),
                  legalText: checkoutLegal?.[orderTypeLegalMessageMap[fee.type]],
              }))
        : [];

    const totalTip = tipsAmount || withTips ? formatPrice(tipsAmount) : '';
    const totalPrice = formatPrice(order?.total + tipsAmount);

    return (
        order && (
            <div {...rest}>
                <div className={styles.reviewOrder}>
                    {order.products.map((tallyItem, i) => (
                        <ReviewOrderItem
                            key={i + tallyItem.productId}
                            tallyItem={tallyItem}
                            productsById={productsById}
                        />
                    ))}
                </div>
                {withTips && (
                    <div className={styles.tipsBlock}>
                        <TipsBlock
                            subtotal={order.subTotalBeforeDiscounts}
                            setTipsAmount={setTipsAmount}
                            tipsAmount={tipsAmount}
                        />
                    </div>
                )}
                <div className={styles.reviewOrderTotal} aria-label="Order Total Section">
                    <ReviewOrderTotal
                        subtotal={formatPrice(subTotalBeforeDiscounts)}
                        totalDiscount={formattedDiscount}
                        totalPrice={totalPrice}
                        totalTax={formatPrice(order.tax)}
                        totalTip={totalTip}
                        formattedFees={formattedFees}
                        freeDelivery={freeDeliveryDiscount && formatPrice(0)}
                    />
                </div>
            </div>
        )
    );
};

export default ReviewOrder;
