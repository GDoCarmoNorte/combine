import React from 'react';
import classNames from 'classnames';

import { useDomainMenuSelectors, useDomainProduct } from '../../../../redux/hooks/domainMenu';
import TextWithTrademark from '../../../atoms/textWithTrademark';
import { formatPrice } from '../../../../lib/domainProduct';
import { useTallyModifiers } from '../../../../common/hooks/useTallyModifiers';
import ListModifiers from '../../../atoms/ListModifiers';

import styles from './reviewOrder.module.css';
import { getBagItemSizeLabel } from '../../../../common/helpers/bagHelper';
import { TallyProductModel } from '../../../../@generated/webExpApi';

interface IReviewOrderComboItemProps {
    comboItem: TallyProductModel;
}

export const ReviewOrderComboItem = ({ comboItem }: IReviewOrderComboItemProps): JSX.Element => {
    const productDetails = useDomainProduct(comboItem.productId);
    const { removedDefaultModifiers, addedModifiers } = useTallyModifiers(comboItem);

    const { selectProductSize } = useDomainMenuSelectors();
    const productSize = getBagItemSizeLabel(selectProductSize(comboItem.productId));
    const sizeLabel = productSize ? ` (${productSize})` : '';
    const productName = `${productDetails?.name}${sizeLabel}`;

    return (
        <>
            <div className={styles.detailsItem} aria-label="Order Details Item">
                <div className={classNames(styles.detailsCell, 't-paragraph-hint')}>
                    <TextWithTrademark tag="span" text={productName} className={styles.product} />
                </div>
                <div className={classNames(styles.detailsCell, styles.price, 't-paragraph-hint')}>
                    {comboItem.price > 0 ? `${formatPrice(comboItem.price)}` : ''}
                </div>
            </div>
            {(removedDefaultModifiers?.length > 0 || addedModifiers?.length > 0) && (
                <ListModifiers removedModifiers={removedDefaultModifiers} addedModifiers={addedModifiers} />
            )}
        </>
    );
};

interface IReviewOrderComboItemListProps {
    comboItems?: TallyProductModel[];
}

export const ReviewOrderComboItemList = ({ comboItems }: IReviewOrderComboItemListProps): JSX.Element => {
    return (
        comboItems && (
            <>
                {comboItems.map((comboItem) => (
                    <ReviewOrderComboItem key={comboItem.productId} comboItem={comboItem} />
                ))}
            </>
        )
    );
};
