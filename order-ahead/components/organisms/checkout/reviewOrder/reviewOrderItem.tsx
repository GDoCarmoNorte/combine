import React from 'react';
import classNames from 'classnames';

import ContentfulImage from '../../../atoms/ContentfulImage';

import styles from './reviewOrder.module.css';

import {
    useDomainMenuSelectors,
    useDomainProduct,
    useProductItemGroup,
    useTallyPriceAndCalories,
} from '../../../../redux/hooks/domainMenu';
import { formatPrice, getRelatedProduct } from '../../../../lib/domainProduct';
import { IProductItemById } from '../../../../common/services/globalContentfulProps';

import { ReviewOrderComboItemList } from './reviewOrderComboItem';
import TextWithTrademark from '../../../atoms/textWithTrademark';
import { useTallyModifiers } from '../../../../common/hooks/useTallyModifiers';
import ListModifiers from '../../../atoms/ListModifiers';
import { getBagItemSizeLabel } from '../../../../common/helpers/bagHelper';
import { SHOW_ORDER_PRODUCTS_IMAGES } from './constants';
import { TallyProductModel } from '../../../../@generated/webExpApi';
import { ItemGroupEnum } from '../../../../redux/types';

interface IReviewOrderItem {
    productsById: IProductItemById;
    tallyItem: TallyProductModel;
    hidePrice?: boolean;
    showImage?: boolean;
}

const ReviewOrderItem = (props: IReviewOrderItem): JSX.Element => {
    const { tallyItem, productsById, hidePrice, showImage } = props;

    const domainProduct = useDomainProduct(tallyItem.productId);
    const productItemGroup = useProductItemGroup(tallyItem.productId);
    const contentfulProduct = productsById[tallyItem.productId] || getRelatedProduct(domainProduct, productsById);

    const offerDiscount = tallyItem.discounts
        ? tallyItem?.discounts.reduce((sum, discount) => {
              return sum + discount.amount * discount.quantity;
          }, 0)
        : 0;

    const { totalPrice: totallPriceForSingleProduct } = useTallyPriceAndCalories(tallyItem);
    const productPrice = totallPriceForSingleProduct * tallyItem.quantity;

    const productDiscount = domainProduct?.price?.currentPrice
        ? (domainProduct?.price?.currentPrice - (tallyItem.price || 0)) * tallyItem.quantity
        : 0;
    const offerDiscountPrice = offerDiscount
        ? formatPrice(productPrice - offerDiscount)
        : formatPrice(productPrice + productDiscount);

    const { removedDefaultModifiers, addedModifiers } = useTallyModifiers(tallyItem);

    const { selectProductSize } = useDomainMenuSelectors();

    const sizeLabel = !domainProduct?.hasChildItems ? getBagItemSizeLabel(selectProductSize(domainProduct?.id)) : '';

    const isBogoOrBogo50 =
        productItemGroup?.name === ItemGroupEnum.BOGO || productItemGroup?.name === ItemGroupEnum.BOGO_50_OFF;

    const orderItemSizeLabel = sizeLabel
        ? isBogoOrBogo50
            ? `(${Number(sizeLabel) * 2} Total)`
            : `(${sizeLabel})`
        : '';
    const orderItemLabel = `${domainProduct?.name || tallyItem.description} ${orderItemSizeLabel}`;

    return (
        <div className={styles.reviewOrderItem}>
            {(SHOW_ORDER_PRODUCTS_IMAGES || showImage) && (
                <div className={classNames(styles.reviewOrderCell, styles.iconCell)}>
                    <ContentfulImage
                        className={styles.reviewOrderIcon}
                        asset={contentfulProduct?.fields?.image}
                        maxWidth={92}
                    />
                </div>
            )}
            <div className={classNames(styles.reviewOrderCell, styles.quantityCell, 't-subheader-small')}>
                {`${tallyItem.quantity}x`}
            </div>
            <div className={styles.reviewOrderCell}>
                <div className={styles.details}>
                    <div className={styles.detailsItem} aria-label="Order Item">
                        <TextWithTrademark
                            tag="div"
                            text={orderItemLabel}
                            className={classNames(styles.detailsCell, 't-subheader-small')}
                        />
                        {!hidePrice && (
                            <div className={classNames(styles.detailsCell, styles.price, 't-subheader-small')}>
                                {offerDiscountPrice}
                            </div>
                        )}
                    </div>
                    {!hidePrice && offerDiscount > 0 && (
                        <div className={classNames(styles.price, 't-paragraph-hint', styles.lineThrough)}>
                            {formatPrice(productPrice + productDiscount)}
                        </div>
                    )}

                    {tallyItem?.childItems?.length ? (
                        <ReviewOrderComboItemList comboItems={tallyItem.childItems} />
                    ) : (
                        <ListModifiers
                            removedModifiers={removedDefaultModifiers}
                            addedModifiers={addedModifiers}
                            hidePrice={hidePrice}
                        />
                    )}
                    {!hidePrice && offerDiscount > 0 && (
                        <div className={styles.detailsItem}>
                            <div className={classNames(styles.discount, 't-paragraph-hint-strong')}>
                                Offers/Discounts
                            </div>
                            <div className={classNames(styles.discount, styles.price, 't-paragraph-hint-strong')}>
                                -{formatPrice(offerDiscount)}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default ReviewOrderItem;
