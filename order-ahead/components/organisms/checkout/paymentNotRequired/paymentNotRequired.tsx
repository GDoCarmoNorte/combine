import React, { useEffect } from 'react';
import { IPaymentRequest } from '../../../clientOnly/paymentInfoContainer/paymentInfo';
import styles from './paymentNotRequired.module.css';
import { Typography } from '@material-ui/core';
import classNames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import {
    IPaymentState,
    TInitialPaymentTypes,
} from '../../../clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

interface IPaymentNotRequiredProps {
    onSubmit: (request: IPaymentRequest) => Promise<void>;
    isActive: boolean;
    setPaymentInfo: (paymentInfo: IPaymentState) => void;
    isPickUp: boolean;
}

const paymentInfo: IPaymentState = {
    type: TInitialPaymentTypes.NO_PAYMENT,
};

function PaymentNotRequired(props: IPaymentNotRequiredProps): JSX.Element {
    const { onSubmit, isActive, setPaymentInfo, isPickUp } = props;

    useEffect(() => {
        setPaymentInfo(paymentInfo);
    }, [setPaymentInfo]);

    const onClick = async () => {
        onSubmit({
            chFirstName: '',
            chLastName: '',
            sessionKey: null,
            paymentKeys: null,
            cardIssuer: null,
            maskedCardNumber: null,
        });
    };

    return (
        <div className={styles.container}>
            <div className={styles.textContainer}>
                <Typography className={classNames('t-header-h3', styles.subHeader)}>
                    NO PAYMENT REQUIRED THIS TIME
                </Typography>
                <Typography className={classNames('t-paragraph', styles.description)}>
                    {isPickUp
                        ? 'Your order total is $0, so proceed to submit the order and pickup at your location.'
                        : 'Your order total is $0, so proceed to submit the order.'}
                </Typography>
            </div>
            <InspireButton
                data-testid={'paymentNotRequiredSubmitButton'}
                disabled={!isActive}
                onClick={onClick}
                text={'SUBMIT'}
                className={styles.submitButton}
            />
        </div>
    );
}

export default PaymentNotRequired;
