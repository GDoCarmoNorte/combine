import React, { useMemo } from 'react';
import { Formik, Form, Field } from 'formik';
import classNames from 'classnames';
import Link from 'next/link';
import Checkbox from '../../../atoms/checkbox';

import FormikInput from '../../formikInput';
import { validateAlpha, validatePhone, validateEmail } from '../../../../common/helpers/complexValidateHelper';
import { createConnector } from '../../../clientOnly/formik/connector';
import { AccountInfo } from '../../../../redux/account';
import { formatAccountName } from '../../../../common/helpers/formatData';
import { TYPOGRAPHY_CLASS } from './constants';
import styles from './customerInfo.module.css';
import { isMarketingPreferencesSignupEnabled } from '../../../../lib/getFeatureFlags';

export interface ICustomerInfo {
    firstName: string;
    lastName: string;
    phone: string;
    email: string;
    isSignUpChecked?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
const noop = (): void => {};

const FormikConnector = createConnector<ICustomerInfo>();

interface ICustomerInfoProps {
    onChange?: (values: ICustomerInfo, isValid: boolean) => void;
    accountInfo?: AccountInfo;
}

const CustomerInfo = (props: ICustomerInfoProps): JSX.Element => {
    const { onChange, accountInfo } = props;

    const MAX_LENGTH_NAME = 70;

    const initialValues: ICustomerInfo = {
        firstName: formatAccountName(accountInfo?.firstName, MAX_LENGTH_NAME) || '',
        lastName: formatAccountName(accountInfo?.lastName, MAX_LENGTH_NAME) || '',
        phone: accountInfo?.phone?.replace(/\D/g, '') || '',
        email: accountInfo?.email || '',
        isSignUpChecked: true,
    };

    const testFirstName = useMemo(() => validateAlpha('First Name'), []);
    const testLastName = useMemo(() => validateAlpha('Last Name'), []);
    const testPhone = useMemo(() => validatePhone('Phone Number'), []);
    const testEmail = useMemo(() => validateEmail('Email'), []);

    const onFormChange = (values: ICustomerInfo, isValid: boolean): void => {
        if (onChange) {
            onChange(values, isValid);
        }
    };

    return (
        <Formik onSubmit={noop} initialValues={initialValues} validateOnMount enableReinitialize>
            {({ setFieldValue, values }) => {
                const handleCheckBox = () => setFieldValue('isSignUpChecked', !values.isSignUpChecked);
                return (
                    <>
                        <Form>
                            <FormikInput
                                name="firstName"
                                label="First Name"
                                labelClassName={styles.label}
                                type="text"
                                maxLength={MAX_LENGTH_NAME}
                                placeholder="First Name"
                                validate={testFirstName}
                                required
                            />
                            <FormikInput
                                name="lastName"
                                label="Last Name"
                                labelClassName={styles.label}
                                type="text"
                                maxLength={MAX_LENGTH_NAME}
                                placeholder="Last Name"
                                validate={testLastName}
                                required
                            />
                            <FormikInput
                                name="phone"
                                label="Phone Number"
                                labelClassName={styles.label}
                                type="tel"
                                maxLength={10}
                                placeholder="Phone Number"
                                validate={testPhone}
                                required
                            />
                            <FormikInput
                                name="email"
                                label="Email"
                                labelClassName={styles.label}
                                type="email"
                                placeholder="Email"
                                validate={testEmail}
                                maxLength={256}
                                required
                            />
                            <Field name="isSignUpChecked" type="hidden" />
                            <FormikConnector onChange={onFormChange} />
                        </Form>

                        {isMarketingPreferencesSignupEnabled() && (
                            <div className={styles.signUpContainer}>
                                <input
                                    className={styles.checkbox}
                                    type="checkbox"
                                    id="signUp"
                                    checked={values.isSignUpChecked}
                                    onChange={(e) => setFieldValue('isSignUpChecked', e.target.checked)}
                                />
                                <Checkbox
                                    fieldName="recieve-offers-and-updates"
                                    onClick={handleCheckBox}
                                    onKeyUp={handleCheckBox}
                                    selected={values.isSignUpChecked}
                                    ariaLabel="Receive tasty offers and updates"
                                />
                                <label
                                    htmlFor="signUp"
                                    className={classNames(styles.signUp, TYPOGRAPHY_CLASS.SIGN_UP_LABEL)}
                                >
                                    Yes! Sign me up to receive tasty offers and updates. Please see our{' '}
                                    <Link href="/privacy-policy">
                                        <a className={classNames(styles.signUpLink, TYPOGRAPHY_CLASS.SIGN_UP_LINK)}>
                                            privacy policy
                                        </a>
                                    </Link>{' '}
                                    for details.
                                </label>
                            </div>
                        )}
                    </>
                );
            }}
        </Formik>
    );
};

export default CustomerInfo;
