import React, { useEffect, useState } from 'react';
import classnames from 'classnames';
import CustomTipsButton from './customTipsButton/customTipsButton';
import { useTips } from '../../../../common/hooks/useTips';
import styles from './tipsBlock.module.css';
import { useOrderLocation } from '../../../../redux/hooks';
import { statusApplyingTips } from '../../contactlessPay/contactlessPay';
import { useAppDispatch } from '../../../../redux/store';
import { GTM_CHECKOUT_TIP_SELECTION_EVENT } from '../../../../common/services/gtmService/constants';

export enum CustomTipsStatus {
    inactive = 'inactive',
    active = 'active',
    success = 'success',
}
interface TipsBlockProps {
    subtotal: number;
    setTipsAmount: (amount: number) => void;
    tipsAmount: number;
    isContactless?: boolean;
    setTipsPercentage?: (percentage: number) => void;
    showWarningModal?: boolean;
    setShowWarningModal?: (showWarningModal: boolean) => void;
    applyTips?: statusApplyingTips;
    setCurrentTipsAmount?: (amount: string) => void;
}

const TipsBlock: React.FC<TipsBlockProps> = ({
    subtotal,
    setTipsAmount,
    tipsAmount,
    isContactless,
    setTipsPercentage,
    showWarningModal,
    setShowWarningModal,
    applyTips,
    setCurrentTipsAmount,
}): JSX.Element => {
    const { isPickUp } = useOrderLocation();

    const [customTipsStatus, setCustomTipsStatus] = useState<CustomTipsStatus>(CustomTipsStatus.inactive);
    const [customTipsValue, setCustomTipsValue] = useState<string>('');
    const dispatch = useAppDispatch();

    const { tipsItems, titleText, isError, errorMessage, defaultTipsId } = useTips(
        subtotal,
        customTipsValue,
        isPickUp,
        isContactless
    );

    const isCheckoutError = isError && !isContactless;
    const isContactlessError = isError && isContactless;

    const [activeTipsId, setActiveTipsId] = useState<number>(defaultTipsId);

    const setTipAmountWithTracking = (tipAmount: number) => {
        setTipsAmount(tipAmount);
        if (!isContactless) {
            dispatch({ type: GTM_CHECKOUT_TIP_SELECTION_EVENT, payload: { tipAmount } });
        }
    };
    useEffect(() => {
        setTipAmountWithTracking(tipsItems[defaultTipsId].amount);
        setTipsPercentage && setTipsPercentage(tipsItems[defaultTipsId].percentage);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [defaultTipsId]);

    useEffect(() => {
        if (isContactlessError) {
            setShowWarningModal(true);
        }
    }, [isContactlessError, setShowWarningModal]);

    useEffect(() => {
        if (applyTips === statusApplyingTips.apply) {
            appliedTips();
        }

        if (applyTips === statusApplyingTips.noapply) {
            onActivateCustomTips();
            setCustomTipsValue('');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [applyTips]);

    const onActivateCustomTips = () => {
        setCustomTipsStatus(CustomTipsStatus.active);
        setActiveTipsId(null);
    };

    const appliedTips = () => {
        setCustomTipsValue((value) => parseFloat(value).toString());
        setCustomTipsStatus(CustomTipsStatus.success);
        setActiveTipsId(null);
        setTipAmountWithTracking(parseFloat(customTipsValue));
        setTipsPercentage && setTipsPercentage(0);
    };

    const onCustomTipsBlur = () => {
        const lastTipsId = tipsItems.findIndex((item) => item.amount === tipsAmount);
        const isEmptyCustomTips = customTipsValue.length === 0;

        if (isEmptyCustomTips && lastTipsId === -1) {
            setCustomTipsStatus(CustomTipsStatus.inactive);
            setTipAmountWithTracking(tipsItems[defaultTipsId].amount);
            setActiveTipsId(defaultTipsId);
        } else if (isEmptyCustomTips) {
            setCustomTipsStatus(CustomTipsStatus.inactive);
            setActiveTipsId(lastTipsId);
        } else if (isCheckoutError) {
            setCustomTipsStatus(CustomTipsStatus.inactive);
            setTipAmountWithTracking(tipsItems[2].amount);
            setActiveTipsId(2);
            setCustomTipsValue('');
        } else if (!showWarningModal) {
            appliedTips();
        }
    };

    const onCustomTipsChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value.replace('$', '');
        if (isContactless) setCurrentTipsAmount(value);
        setCustomTipsValue(value);
    };

    const onCustomTipsKeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
        event.currentTarget.blur();
    };

    const getButtonText = (amount, percentage): JSX.Element => {
        if (percentage) {
            return (
                <>
                    <p className={classnames('t-subheader-small', styles.tipsAmountText)}>${amount}</p>
                    <p className={classnames('t-paragraph-hint', styles.tipsPercentageText)}>{percentage}%</p>
                </>
            );
        }
        return <div className={classnames('t-subheader-small', styles.tipsAmountText)}>${amount}</div>;
    };

    return (
        <div className={styles.tipsSelectors}>
            <p className={classnames('t-paragraph-small-strong')}>{titleText}</p>
            <div className={styles.tipsContainer}>
                <div className={classnames(styles.tipsButtonsBlock, { [styles.errorBorder]: isCheckoutError })}>
                    {tipsItems.map(({ amount, percentage }, index) => {
                        return (
                            <button
                                className={classnames(styles.tipsButton, {
                                    [styles.tipsActiveButton]: activeTipsId === index,
                                })}
                                onClick={() => {
                                    setActiveTipsId(index);
                                    setTipAmountWithTracking(amount);
                                    setTipsPercentage && setTipsPercentage(percentage);
                                    setCustomTipsStatus(CustomTipsStatus.inactive);
                                    setCustomTipsValue('');
                                }}
                                key={amount}
                            >
                                {getButtonText(amount, percentage)}
                            </button>
                        );
                    })}
                    <CustomTipsButton
                        onChange={onCustomTipsChange}
                        onClick={onActivateCustomTips}
                        onBlur={onCustomTipsBlur}
                        onKeyUp={onCustomTipsKeyUp}
                        status={customTipsStatus}
                        value={customTipsValue}
                        isPickUp={isPickUp}
                        isContactless={isContactless}
                    />
                </div>
                {isCheckoutError && <p className={classnames('t-paragraph-hint', styles.errorText)}>{errorMessage}</p>}
            </div>
        </div>
    );
};

export default TipsBlock;
