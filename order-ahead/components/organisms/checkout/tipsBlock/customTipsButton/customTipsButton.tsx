import React, { FC } from 'react';
import classnames from 'classnames';
import { CustomTipsStatus } from '../tipsBlock';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import styles from './customTipsButton.module.css';

interface CustomTipsButtonProps {
    onKeyUp: (e: React.KeyboardEvent<HTMLInputElement>) => void;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    onClick: () => void;
    onBlur: () => void;
    status: CustomTipsStatus;
    value: string;
    isPickUp: boolean;
    isContactless?: boolean;
}

const CustomTipsButton: FC<CustomTipsButtonProps> = ({
    onChange,
    onClick,
    onBlur,
    onKeyUp,
    value,
    status,
    isPickUp,
    isContactless,
}) => {
    const getNumberMaskOptions = () => {
        const defaultOptions = {
            prefix: '$',
            suffix: '',
            allowDecimal: false,
            integerLimit: 2,
            allowNegative: false,
            allowLeadingZeroes: false,
        };

        if (isPickUp || isContactless) {
            return {
                ...defaultOptions,
                allowDecimal: true,
                decimalSymbol: '.',
                decimalLimit: 2,
            };
        }

        return defaultOptions;
    };
    const currencyMask = createNumberMask(getNumberMaskOptions());

    const onInputKeyUp = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (onKeyUp && event.key === 'Enter') {
            onKeyUp(event);
        }
    };

    const renderInactiveCustomButton = () => (
        <button onClick={onClick} className={classnames('t-subheader-small', styles.customTipsButton)}>
            CUSTOM
        </button>
    );

    const renderActiveCustomButton = () => (
        <button className={classnames(styles.customTipsButton, styles.activeCustomTipsButton)}>
            <p className={styles.title}>Custom</p>
            <MaskedInput
                mask={currencyMask}
                className={classnames('t-paragraph-hint', styles.customTipsInput, {
                    [styles.activeCustomTipsInput]: !!value,
                })}
                autoFocus={true}
                placeholder={'$'}
                value={value}
                type="text"
                onChange={onChange}
                onBlur={onBlur}
                onKeyUp={onInputKeyUp}
            />
        </button>
    );

    const renderSuccessCustomButton = () => (
        <button onClick={onClick} className={classnames(styles.customTipsButton, styles.activeCustomTipsButton)}>
            <p className={styles.title}>Custom</p>
            <p className={classnames('t-subheader-small', styles.customTipsAmountText)}>${value}</p>
        </button>
    );

    const statusMap = {
        [CustomTipsStatus.inactive]: renderInactiveCustomButton(),
        [CustomTipsStatus.active]: renderActiveCustomButton(),
        [CustomTipsStatus.success]: renderSuccessCustomButton(),
    };

    return statusMap[status];
};

export default CustomTipsButton;
