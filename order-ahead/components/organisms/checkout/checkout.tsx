import React, { useState, useMemo, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import classNames from 'classnames';

import { ItemModel, TallyProductModel } from '../../../@generated/webExpApi';

import { IGlobalContentfulProps } from '../../../common/services/globalContentfulProps';

import useSubmitOrder, { IOrderPayload } from '../../../redux/hooks/useSubmitOrder';

import LocationInfo from '../../clientOnly/locationInfo';
import PickupDeliveryInfo, { IDeliveryInfo } from './pickupDeliveryInfo';
import CustomerInfo, { ICustomerInfo } from '../../../components/organisms/checkout/customerInfo';
import ReviewOrder from '../../../components/organisms/checkout/reviewOrder';

import PaymentInfoContainer from '../../../components/clientOnly/paymentInfoContainer';
import CheckoutError from '../../clientOnly/checkoutError';

import PaymentHeader from '../../../components/organisms/header/paymentHeader';

import styles from './checkout.module.css';

import { IPaymentRequest } from '../../clientOnly/paymentInfoContainer/paymentInfo';
import useTallyOrder from '../../../redux/hooks/useTallyOrder';
import useAccount from '../../../redux/hooks/useAccount';
import useBag from '../../../redux/hooks/useBag';
import useNotifications from '../../../redux/hooks/useNotifications';
import {
    GTM_CHECKOUT_CUSTOMER_INFO,
    GTM_CHECKOUT_PAYMENT,
    GTM_ERROR_EVENT,
    GTM_TRANSACTION_COMPLETE,
} from '../../../common/services/gtmService/constants';
import { actions as orderActions } from '../../../redux/submitOrder';
import { useOrderLocation, useSelectedSell, usePdp } from '../../../redux/hooks';
import { useDomainProducts } from '../../../redux/hooks/domainMenu';
import { PageContentWrapper } from '../../sections/PageContentWrapper';

import { ICheckoutHeader, ICheckoutLegalFields } from '../../../@generated/@types/contentful';
import { IPaymentState, TInitialPaymentTypes } from '../../clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import { getInitialPaymentType } from './constants';
import PaymentNotRequired from './paymentNotRequired/paymentNotRequired';
import { IGiftCard } from '../../clientOnly/paymentInfoContainer/types';
import UnavailableItemsModal from '../unavailableItemsModal';
import { injectScript } from '../../../lib/injectScriptOnce';
import { isFraudCheckEnabled } from '../../../lib/getFeatureFlags';
import { useNotValidBagProductsByTime } from '../../../common/hooks/useNotValidBagProductsByTime';
import { useAppSelector } from '../../../redux/store';
import { selectProducts } from '../../../redux/selectors/domainMenu';
import { useUnavailableItems } from '../../../common/hooks/useUnavailableItems';
import { getProductPath } from '../../../lib/domainProduct';
import { IProductDetailsPagePath } from '../../../lib/contentfulDelivery';
import { GtmErrorCategory } from '../../../common/services/gtmService/types';
import { minSubtotalValueForPickUpTipping } from './tipsBlock/tipsConstants';
import PaymentProcessing from './paymentProcessing/paymentProcessing';
import { removeUnavailableModifiers } from '../../../common/helpers/bagHelper';
import logger from '../../../common/services/logger';
import { useAuth0 } from '@auth0/auth0-react';
import { useConfiguration } from '../../../common/hooks/useConfiguration';
import { useSignUp } from '../../../common/hooks/useSignUp';

interface ICheckoutItem {
    entry: TallyProductModel;
    product: ItemModel;
    markedAsRemoved: boolean;
}

interface ICheckoutProps extends IGlobalContentfulProps {
    handleOrderTimeChange: (payload: { time?: string; asap?: boolean }) => void;
    tallyLoading: boolean;
    pickupInstructions?: string[];
    checkoutHeader: ICheckoutHeader;
    onBagChange: () => void;
    productDetailsPagePaths: IProductDetailsPagePath[];
    checkoutLegal?: ICheckoutLegalFields;
}

const Checkout = (props: ICheckoutProps): JSX.Element => {
    const {
        alertBanners,
        navigation,
        productsById,
        handleOrderTimeChange,
        tallyLoading,
        checkoutHeader,
        onBagChange,
        productDetailsPagePaths,
        checkoutLegal,
    } = props;
    const dispatch = useDispatch();
    const router = useRouter();

    const { configuration } = useConfiguration();

    const initialPaymentType = getInitialPaymentType(configuration);

    const productsByProductId = useAppSelector(selectProducts);

    const {
        bagEntries,
        orderTime,
        orderTimeType,
        entriesMarkedAsRemoved,
        lastRemovedLineItemId,
        lastEdit,
        actions: {
            removeAllFromBag,
            toggleIsOpen,
            removeFromBag,
            clearLastRemovedLineItemId,
            clearLastEdit,
            editBagLineItem,
        },
    } = useBag();

    const { isPickUp, deliveryAddress, isCurrentLocationOAAvailable, currentLocation } = useOrderLocation();

    const {
        actions: { enqueueError },
    } = useNotifications();

    const {
        actions: { setCorrelationId },
    } = useSelectedSell();

    const { account, accountInfo } = useAccount();

    const { tallyOrder, error: tallyError } = useTallyOrder();
    const {
        error: submitError,
        isLoading,
        lastOrder,
        submitOrder,
        resetSubmitOrder,
        isShowAlertModal,
        hideAlertModal,
        setUnavailableItems,
        unavailableItems: unavailableIds,
    } = useSubmitOrder();

    const notValidProductsByTime = useNotValidBagProductsByTime();
    const { unavailableItems, setTouched } = useUnavailableItems(unavailableIds);
    const pdp = usePdp();
    const { isAuthenticated } = useAuth0();
    const { signUpMarketingPreferences } = useSignUp();

    useEffect(() => {
        if (isShowAlertModal && (lastRemovedLineItemId || lastEdit)) {
            onBagChange();
        }
    }, [bagEntries.length, isShowAlertModal, lastRemovedLineItemId, lastEdit, onBagChange]);

    const [isActive, setIsActive] = useState<boolean>(false);
    const [customerInfo, setCustomerInfo] = useState<ICustomerInfo>(null);
    const [deliveryInfo, setDeliveryInfo] = useState<IDeliveryInfo>(null);
    const [tipsAmount, setTipsAmount] = useState(0);

    const [isGTMCheckout2Fired, setIsGTMCheckout2Fired] = useState<boolean>(false);
    const [isGTMCheckout3Fired, setIsGTMCheckout3Fired] = useState<boolean>(false);
    const [kountSessionId, setKountSessionId] = useState<string>();
    const [paymentInfo, setPaymentInfo] = useState<IPaymentState>({
        type: initialPaymentType,
    });
    const [isSubmitInProcess, setIsSubmitInProcess] = useState<boolean>(false);
    const [isPayAtStoreSelected, setIsPayAtStoreSelected] = useState<boolean>(false);

    useEffect(() => {
        if (isFraudCheckEnabled()) {
            const dataCollectorScriptSrc = `${process.env.NEXT_PUBLIC_KOUNT_DATA_COLLECTOR_URL}${process.env.NEXT_PUBLIC_KOUNT_MERCHANT_ID}`;
            injectScript(dataCollectorScriptSrc).then(() => {
                window.ka?.sessionId && setKountSessionId(window.ka.sessionId);
            });
        }
    }, []);

    useEffect(() => {
        if (submitError || !!unavailableItems.length) {
            setIsSubmitInProcess(false);
        }
    }, [submitError, unavailableItems]);

    useEffect(() => {
        if (isLoading) {
            setIsSubmitInProcess(true);
        }
    }, [isLoading]);

    const redirect = () => router.replace('/');

    const handleChange = (values: ICustomerInfo, isValid: boolean): void => {
        setIsActive(isValid);
        setCustomerInfo(values);

        const arrayOfValues = Object.values(values);
        const isValuesFilled = arrayOfValues.filter(Boolean).length === arrayOfValues.length;

        if (isValuesFilled && !isGTMCheckout2Fired) {
            setIsGTMCheckout2Fired(true);
            dispatch({
                type: GTM_CHECKOUT_CUSTOMER_INFO,
                payload: { tallyOrder, bagEntries, step: 2, action: 'Checkout' },
            });
        }
    };

    const onDeliveryInfoChange = (values: IDeliveryInfo): void => {
        setDeliveryInfo(values);
    };

    const orderTimeASAP = orderTimeType === 'asap';
    const isPaymentActive =
        isActive &&
        !(isLoading || lastOrder) &&
        !(tallyLoading || tallyError) &&
        (orderTime || orderTimeASAP) &&
        (!isLoading || submitError);
    const orderProductsIds = tallyOrder?.products?.map((tallyItem) => tallyItem.productId) || [];

    const domainProducts = useDomainProducts(orderProductsIds);
    const handleSubmit = async (paymentRequest: IPaymentRequest, giftCards?: IGiftCard[]): Promise<void> => {
        if (!isPaymentActive) return;

        resetSubmitOrder();
        setIsPayAtStoreSelected(
            paymentRequest?.paymentType && paymentRequest?.paymentType === TInitialPaymentTypes.PAY_IN_STORE
        );

        const getOrderTime = (): Date | null => {
            if (isPickUp) return orderTimeASAP ? null : new Date(orderTime);

            return orderTimeASAP ? tallyOrder.fulfillment.time : new Date(orderTime);
        };

        const submitOrderPayload: IOrderPayload = {
            tallyOrder,
            customerInfo,
            customerId: account?.idpCustomerId,
            paymentRequest,
            paymentInfo,
            giftCards,
            location: currentLocation,
            orderTime: getOrderTime(),
            instructions: isPickUp ? props.pickupInstructions : Array.of(deliveryInfo?.instructions),
            driverTip: (!isPickUp && tipsAmount) || 0,
            serverTip: (isPickUp && tipsAmount) || 0,
            deviceIdentifier: kountSessionId,
        };

        const deliveryLocation = deliveryAddress?.deliveryLocation;

        if (deliveryLocation && !isPickUp) {
            const copyDeliveryLocation = { ...deliveryLocation };
            copyDeliveryLocation.addressLine2 = deliveryInfo.place;
            submitOrderPayload.deliveryLocation = copyDeliveryLocation;
        }

        const checkoutItems: ICheckoutItem[] = bagEntries
            .map((entry) => ({
                entry,
                product: productsByProductId[entry.productId],
                markedAsRemoved: entriesMarkedAsRemoved.includes(entry.lineItemId),
            }))
            .filter(
                (item) =>
                    item.product &&
                    item.product.availability?.isAvailable &&
                    item.product.isSaleable &&
                    !item.markedAsRemoved
            );

        const noCheckoutEntries = checkoutItems.length < 1;
        const isOrderValid = !(!isCurrentLocationOAAvailable || noCheckoutEntries);

        if (isOrderValid) {
            return submitOrder(submitOrderPayload)
                .then((action) => {
                    if (action.type === orderActions.rejected.type) {
                        setIsSubmitInProcess(false);
                    }

                    if (action.type === orderActions.fulfilled.type) {
                        if (customerInfo.isSignUpChecked) {
                            const zip = currentLocation.contactDetails.address.postalCode;
                            const payload = {
                                firstName: customerInfo.firstName,
                                lastName: customerInfo.lastName,
                                email: customerInfo.email,
                                storeId: currentLocation.id,
                                storeName: currentLocation.displayName,
                                postalCode: zip.slice(0, 5),
                            };
                            signUpMarketingPreferences(payload);
                        }
                        router.push(`/confirmation?id=${action.payload.response.orderId}`);
                        dispatch({
                            type: GTM_CHECKOUT_PAYMENT,
                            payload: { tallyOrder, bagEntries, step: 4, action: 'Payment' },
                        });
                        return action.payload;
                    }
                })
                .then((res) => {
                    if (!res) return;

                    logger.logEvent('order_placed', {
                        type: res.response.fulfillment.type,
                        isGuestUser: !isAuthenticated,
                        amount: orderTotal,
                    });

                    dispatch({
                        type: GTM_TRANSACTION_COMPLETE,
                        payload: {
                            tallyOrder,
                            orderId: res.response.orderId,
                            domainProducts,
                            productsById,
                            bagEntries,
                        },
                    });

                    // clearBag();
                    // Remove submitted items and reset pickup time instead of bag clear as unavailable items could be there
                    removeAllFromBag({
                        lineItemIdList: checkoutItems.map((item) => item.entry.lineItemId),
                    });

                    // set NEW Correlation ID for Suggested Sell Recommended Items
                    setCorrelationId();
                });
        } else {
            redirect().then(() => {
                toggleIsOpen({ isOpen: true });
            });
        }
    };

    const handlePaymentFormStatusChange = (isValid: boolean) => {
        if (isValid && !isGTMCheckout3Fired) {
            setIsGTMCheckout3Fired(true);
            dispatch({
                type: GTM_CHECKOUT_CUSTOMER_INFO,
                payload: { tallyOrder, bagEntries, step: 3, action: 'Checkout' },
            });
        }
    };

    const [paymentError, setPaymentError] = useState<{
        message?: string;
        title?: string;
        description?: string;
        header?: string;
        textError?: string;
    }>(null);

    const handlePaymentError = (
        error: { message?: string; title?: string; header?: string; description?: string; textError?: string } | null
    ): void => {
        if (error && !error.textError) {
            enqueueError({ message: error.message, title: error.title });

            const payload = {
                ErrorCategory: GtmErrorCategory.CHECKOUT_PAYMENT,
                ErrorDescription: error.message,
            };

            dispatch({ type: GTM_ERROR_EVENT, payload });
        }

        setPaymentError(error ? error : null);
    };

    const [pickupTimeError, setOrderTimeError] = useState<{ message?: string; title?: string }>(null);
    const handleOrderTimeError = (error: { message?: string; title?: string }): void => {
        setOrderTimeError(error ? error : null);
    };

    const handleUnavailableItemsModalClose = () => {
        if (notValidProductsByTime.length) {
            onBagChange();
        } else {
            clearLastRemovedLineItemId();
            clearLastEdit();
        }
        hideAlertModal();
    };

    const handleUnavailableItemRemove = (lineItemId: number) => {
        removeFromBag({ lineItemId });
        setTouched(lineItemId);
    };

    const handleUnavailableModifierRemove = (
        entry: TallyProductModel,
        unavailableModifiers: string[],
        unavailableSubModifiers: string[]
    ) => {
        const updatedBagItem = removeUnavailableModifiers(entry, unavailableModifiers, unavailableSubModifiers);
        editBagLineItem({
            bagEntry: updatedBagItem,
        });

        setTouched(entry.lineItemId);
    };

    const handleModifyClick = (lineItemId: number) => {
        const bagEntry = bagEntries.find((bagEntry) => bagEntry.lineItemId === lineItemId);

        pdp.actions.putTallyItem({
            lineItemId,
            pdpTallyItem: bagEntry,
        });

        const entryPath = getProductPath(domainProducts[bagEntry.productId], productDetailsPagePaths);

        if (entryPath && router) {
            router.push(entryPath.href, entryPath.as);
        }
    };

    const showOnCheckOutAlertBanners = {
        ...alertBanners,
        items: alertBanners.items.filter((alertBanner) => alertBanner.fields.showOnCheckout),
    };
    const displayError = paymentError || pickupTimeError;

    const checkoutHeaderFields = checkoutHeader?.fields;
    const { backgroundColor, backgroundImage } = checkoutHeaderFields || {};
    const bgImageUrl = backgroundImage?.fields?.file?.url;
    const bgColorHex = backgroundColor?.fields?.hexColor;

    const pickupDeliveryTitle = isPickUp ? 'Pickup' : 'Delivery';

    const isPaymentRequired = !(
        tallyOrder &&
        tallyOrder.products.length > 0 &&
        tallyOrder.fulfillment &&
        tallyOrder.discountsAmount > 0 &&
        tallyOrder.total === 0
    );

    const isTippingEnabled = useMemo(() => {
        return (
            configuration.isTippingEnabled &&
            currentLocation.isTippingEnabled &&
            paymentInfo.type !== TInitialPaymentTypes['PAY_IN_STORE'] &&
            isPaymentRequired &&
            !(isPickUp && tallyOrder.subTotalBeforeDiscounts <= minSubtotalValueForPickUpTipping)
        );
    }, [currentLocation, paymentInfo, isPaymentRequired, isPickUp, tallyOrder, configuration.isTippingEnabled]);

    useEffect(() => {
        if (!isTippingEnabled) {
            setTipsAmount(0);
        }
    }, [isTippingEnabled]);

    const orderTotal = useMemo(() => {
        return tallyOrder?.total && parseFloat((tallyOrder.total + (tipsAmount || 0)).toFixed(2));
    }, [tallyOrder, tipsAmount]);

    const headerOverlayClasses = classNames(styles.headerOverlay, {
        [styles.headerOverlayHidden]: !isSubmitInProcess,
    });

    return (
        <>
            <div className={styles.paymentHeaderContainer}>
                <PaymentHeader
                    alertBanners={showOnCheckOutAlertBanners}
                    navigation={navigation}
                    isPaymentProcessingShouldBeShown={isSubmitInProcess}
                />
                <div className={headerOverlayClasses} />
            </div>
            {displayError?.message && <CheckoutError message={displayError.message} />}
            <div
                className={classNames({
                    [styles.paymentProcesssingContainerHidden]: !isSubmitInProcess,
                })}
            >
                <PaymentProcessing hideHeader={isPayAtStoreSelected} />
            </div>
            <PageContentWrapper
                className={classNames(styles.container, { [styles.containerHidden]: isSubmitInProcess })}
            >
                <div className={classNames(styles.background, 'bannerBackground')}>
                    <style jsx>{`
                        .bannerBackground {
                            background-color: ${bgColorHex ? `#${bgColorHex}` : 'transparent'};
                            ${bgImageUrl
                                ? `background-image: url("${bgImageUrl}");
                                background-repeat: no-repeat;
                                background-size: cover;
                                background-position: center;`
                                : ``}
                        }
                    `}</style>
                </div>
                <div className={styles.checkoutForm}>
                    <LocationInfo isPickUp={isPickUp} />
                    <div className={styles.formFields}>
                        <section aria-label={`${pickupDeliveryTitle} Info Section`}>
                            <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>
                                {pickupDeliveryTitle} Info
                            </h4>
                            <PickupDeliveryInfo
                                onChange={(payload: { time?: string; asap?: boolean }) => {
                                    setUnavailableItems(notValidProductsByTime);
                                    handleOrderTimeChange(payload);
                                    handleOrderTimeError(null);
                                }}
                                onInvalidTimeUpdate={handleOrderTimeError}
                                loading={tallyLoading}
                                onDeliveryInfoChange={onDeliveryInfoChange}
                                fulfilmentTime={tallyOrder.fulfillment.time}
                            />
                        </section>
                        <section>
                            <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Customer Info</h4>
                            <CustomerInfo
                                data-testid="customer-info"
                                onChange={handleChange}
                                accountInfo={accountInfo}
                            />
                        </section>
                        <section
                            className={classNames({ [styles.loading]: tallyLoading })}
                            aria-label="Review Order Section"
                        >
                            <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Review Order</h4>
                            <ReviewOrder
                                tipsAmount={tipsAmount}
                                setTipsAmount={setTipsAmount}
                                withTips={isTippingEnabled}
                                order={tallyOrder}
                                productsById={productsById}
                                checkoutLegal={checkoutLegal}
                            />
                        </section>
                        <section>
                            <h4 className={classNames('t-header-h3', styles.formSectionHeader)}>Payment info</h4>
                            {!isPaymentRequired ? (
                                <PaymentNotRequired
                                    data-testid="payment-not-required"
                                    onSubmit={handleSubmit}
                                    isActive={!!isPaymentActive}
                                    setPaymentInfo={setPaymentInfo}
                                    isPickUp={isPickUp}
                                />
                            ) : (
                                <PaymentInfoContainer
                                    data-testid="payment-info"
                                    paymentInfo={paymentInfo}
                                    setPaymentInfo={setPaymentInfo}
                                    isActive={!!isPaymentActive}
                                    onSubmit={handleSubmit}
                                    onError={handlePaymentError}
                                    onFormStatusChange={handlePaymentFormStatusChange}
                                    submitError={submitError}
                                    totalPrice={orderTotal}
                                    isUnavailableItems={!!unavailableItems.length}
                                />
                            )}
                        </section>
                    </div>
                </div>
            </PageContentWrapper>
            <UnavailableItemsModal
                unavailableItems={unavailableItems}
                onClose={handleUnavailableItemsModalClose}
                onRemove={handleUnavailableItemRemove}
                onModifiersRemove={handleUnavailableModifierRemove}
                onModifyClick={handleModifyClick}
                open={isShowAlertModal}
                loading={tallyLoading}
                isNotValidTime={!!notValidProductsByTime.length}
            />
        </>
    );
};

export default Checkout;
