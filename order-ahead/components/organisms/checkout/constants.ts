import { ConfigurationValues } from '../../../redux/configuration';
import { TInitialPaymentTypes } from '../../clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

// Disabled eslint check since configuration parameter is used in injected versions for other brands (see bww.constants);
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function getInitialPaymentType(configuration?: ConfigurationValues) {
    return TInitialPaymentTypes.CREDIT_OR_DEBIT;
}
