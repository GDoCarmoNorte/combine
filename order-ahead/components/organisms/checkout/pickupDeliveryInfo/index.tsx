import React, { useCallback, useEffect, useState, useMemo } from 'react';
import {
    differenceInMinutes,
    isSameDay,
    parseISO,
    isAfter,
    zonedTimeToUtc,
    utcToZonedTime,
} from '../../../../common/helpers/dateTime';

import { LocationPickupAndDeliveryResponseModel, TServiceTypeModel } from '../../../../@generated/webExpApi';
import Dropdown from './dropdown';
import { useBag, useOrderLocation } from '../../../../redux/hooks';
import { resolveOpeningHours } from '../../../../lib/locations';
import { getHoursWithTimezone } from '../../../../lib/locations/resolveOpeningHours';
import { transformHoursByDay } from '../../../../common/helpers/getStoreStatus';
import {
    addASAPFieldToRange,
    enhanceDaysWithLabel,
    enhanceTimesWithLabel,
} from '../../../../common/helpers/checkoutHelpers';
import { useLocationTimeZone } from '../../../../common/hooks/useLocationTimeZone';

import { Form, Formik } from 'formik';
import FormikInput from '../../formikInput';
import { createConnector } from '../../../clientOnly/formik/connector';

import styles from './index.module.css';
import { WorkingHours } from '../../../../redux/bag';
import { OrderLocationMethod } from '../../../../redux/orderLocation';

export interface IDeliveryInfo {
    place: string;
    instructions: string;
}

const FormikConnector = createConnector<IDeliveryInfo>();
const noop = (): void => null;

interface IPickupInfoProps {
    onChange: (payload: { time?: string; asap?: boolean }) => void;
    onInvalidTimeUpdate: (error: { title: string; message: string }) => void;
    loading: boolean;
    onDeliveryInfoChange: (values: IDeliveryInfo) => void;
    fulfilmentTime?: Date;
}

function PickupDeliveryInfo(props: IPickupInfoProps): JSX.Element {
    const { onChange, onInvalidTimeUpdate, onDeliveryInfoChange, loading } = props;
    const {
        orderTime,
        orderTimeType,
        pickupTimeValues,
        pickupTimeIsValid,
        actions: { setPickupTime, initPickupTimeValues },
    } = useBag();
    const {
        isPickUp,
        deliveryLocationTimeSlots,
        method,
        deliveryAddress,
        currentLocation: location,
    } = useOrderLocation();
    const pickupDeliveryLabel = isPickUp ? 'Pickup' : 'Delivery';
    const initialDeliveryPlace = deliveryAddress?.deliveryLocation?.addressLine2;

    const getDeliveryTimeValues = (availableTimeSlots: LocationPickupAndDeliveryResponseModel): WorkingHours => {
        return Object.entries(availableTimeSlots.delivery.byDay).map(([_, timeRanges]) => ({
            day: `${timeRanges[0].utc}`,
            timeRange: timeRanges.map((time) => `${time.utc}`),
        }));
    };

    const pickUpLocation = deliveryAddress?.pickUpLocation;

    const locationTimeZone = useLocationTimeZone();

    const storeIsOpenNow = useMemo(() => {
        if (method === OrderLocationMethod.DELIVERY && !pickUpLocation?.hoursByDay) {
            return false;
        }

        const resolvedOpeningHours =
            method === OrderLocationMethod.DELIVERY
                ? getHoursWithTimezone(transformHoursByDay(pickUpLocation.hoursByDay), locationTimeZone)
                : resolveOpeningHours(location, TServiceTypeModel.OrderAhead);

        return !!resolvedOpeningHours && resolvedOpeningHours.isOpen;
    }, [pickUpLocation, location, locationTimeZone, method]);

    const timeValues = isPickUp ? pickupTimeValues : getDeliveryTimeValues(deliveryLocationTimeSlots);

    const [pickupDay, setPickupDay] = useState<{ day: string; timeRange: string[] }>(null);

    const [initValuesSet, setInitValuesSet] = useState(false);
    const [initValuesChanged, setInitValuesChanged] = useState(false);

    const [fieldLevelError, setFieldLevelError] = useState<string>(null);

    const openingHours = resolveOpeningHours(location, TServiceTypeModel.Pickup);

    useEffect(() => {
        if (!timeValues) return;

        if (orderTimeType === 'asap' && !initValuesSet) {
            setPickupDay(timeValues[0]);
            setInitValuesSet(true);
        }

        if (orderTime && !initValuesSet) {
            if (!pickupTimeIsValid) {
                const pickupDay = timeValues.reduce((acc, item) => {
                    if (isSameDay(parseISO(item.day), parseISO(orderTime))) {
                        return item.timeRange.find((time) => {
                            if (time === orderTime) return true;
                            if (isAfter(parseISO(time), parseISO(orderTime))) return true;
                        });
                    }
                    return acc;
                }, '');

                pickupDay
                    ? setPickupTime({ time: pickupDay, method })
                    : setPickupTime({ time: timeValues[0].timeRange[0], method });
                setInitValuesSet(true);
                return;
            }

            setInitValuesSet(true);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, orderTimeType, timeValues, pickupTimeIsValid]);

    useEffect(() => {
        if (!timeValues || !orderTime) return;

        const pickupDay = timeValues.find((day) => {
            const selectedDayWithTimezone = utcToZonedTime(new Date(day.day), location.timezone);
            const pickupTimeWithTimezone = utcToZonedTime(new Date(orderTime), location.timezone);
            return isSameDay(selectedDayWithTimezone, pickupTimeWithTimezone);
        });

        if (!pickupDay || (initValuesSet && !isPickUp)) return;

        setPickupDay(pickupDay);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, timeValues, isPickUp]);

    const onChangeCallback = useCallback(() => {
        if (!timeValues) return;
        if (!initValuesSet) return;
        if (!(orderTime || orderTimeType === 'asap')) return;

        if (!initValuesChanged) {
            setInitValuesChanged(true);
            return;
        }

        if (typeof onChange === 'function') {
            if (orderTimeType === 'asap') {
                onChange({ asap: true });
            } else if (orderTimeType === 'future') {
                const localStoreTime = zonedTimeToUtc(orderTime, openingHours.storeTimezone);

                onChange({ time: localStoreTime.toISOString() });
            }
        }

        setFieldLevelError(null);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, orderTimeType, initValuesSet]);

    useEffect(onChangeCallback, [orderTime, orderTimeType, initValuesSet, onChangeCallback]);

    const recheckTime = () => {
        if (!initValuesSet || !timeValues) return;

        const closestPickupTime = timeValues[0].timeRange[0];
        const shouldUpdatePickupTimeValues =
            differenceInMinutes(parseISO(closestPickupTime), new Date(Date.now())) < 15;

        if (shouldUpdatePickupTimeValues) {
            initPickupTimeValues({ openedTimeRanges: openingHours.openedTimeRanges, timezone: openingHours.closeTime });

            if (orderTimeType === 'asap') return;

            onInvalidTimeUpdate({
                title: `${pickupDeliveryLabel} Time Update`,
                message: 'Selected date and time must be valid',
            });

            setFieldLevelError(`Scheduled ${pickupDeliveryLabel} time is invalid. Please review your selection`);
        }
    };

    useEffect(() => {
        const interval = setInterval(recheckTime, 1000);
        return () => clearInterval(interval);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [orderTime, orderTimeType, timeValues, initValuesSet]);

    const handlePickupDayChange = (value: string) => {
        const pickupDay = timeValues.find((day) => day.day === value);
        setPickupDay(pickupDay);
        setPickupTime({ time: null, method });
    };
    const handleOrderTimeChange = (value: string) => {
        if (value === 'asap') {
            setPickupTime({ asap: true, time: null, method });
        } else {
            setPickupTime({ time: value, asap: false, method });
        }
    };

    if (!initValuesSet || !timeValues || !pickupDay) return null;

    let timeOptions = enhanceTimesWithLabel(pickupDay.timeRange, openingHours.storeTimezone);

    if (storeIsOpenNow) {
        timeOptions = addASAPFieldToRange({
            timeRange: enhanceTimesWithLabel(pickupDay.timeRange, openingHours.storeTimezone),
            selectedDay: pickupDay.day,
            timezone: openingHours.storeTimezone,
            prepTime: location.additionalFeatures?.prepTime,
            isAdditionalASAP: false,
        });
    }

    return (
        <>
            <div className={styles.container}>
                <Dropdown
                    value={pickupDay.day}
                    label={`${pickupDeliveryLabel} Date`}
                    labelClassName={styles.label}
                    name="day"
                    placeholder={`${pickupDeliveryLabel} day`}
                    options={enhanceDaysWithLabel(
                        timeValues.map((days) => days.day),
                        openingHours.storeTimezone
                    )}
                    onChange={handlePickupDayChange}
                    disabled={loading}
                />
                <Dropdown
                    value={orderTimeType === 'asap' ? timeOptions[0]?.value : orderTime}
                    label={`${pickupDeliveryLabel} Time`}
                    labelClassName={styles.label}
                    name="time"
                    placeholder={timeOptions[0]?.label || `${pickupDeliveryLabel} time`}
                    options={timeOptions}
                    onChange={handleOrderTimeChange}
                    disabled={loading}
                    error={!orderTime && !(orderTimeType === 'asap') && 'Select time'}
                />
                {loading && <p className={styles.info}>Refreshing order data</p>}
                {fieldLevelError && <p className={styles.info}>{fieldLevelError}</p>}
            </div>
            {!isPickUp && (
                <Formik
                    onSubmit={noop}
                    initialValues={{ place: initialDeliveryPlace, instructions: '' }}
                    enableReinitialize
                >
                    <Form>
                        <FormikInput
                            name="place"
                            label="Delivery Place"
                            type="text"
                            placeholder="APT/Suite #"
                            optional
                        />
                        <FormikInput
                            name="instructions"
                            label="Delivery Instructions"
                            type="text"
                            placeholder="Enter instructions"
                            optional
                        />
                        <FormikConnector onChange={onDeliveryInfoChange} />
                    </Form>
                </Formik>
            )}
        </>
    );
}

export default PickupDeliveryInfo;
