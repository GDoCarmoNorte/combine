import React, { FC, useCallback, useState, useMemo } from 'react';

import styles from './allModifiersModal.module.css';
import { IDisplayModifierGroup, IDisplayProduct, ProductIntensityEnum } from '../../../redux/types';

import ModifiersModal from '../../atoms/modifiersModal';
import { groupBy } from 'lodash';
import classNames from 'classnames';
import SaucesFilter from '../saucesFilter/SaucesFilter';
import { Divider } from '../../atoms/divider';
import { isItemSelected } from './useModifiersSelection';
import ModalFooterContent from './modalFooterContent';
import { BwwModifierCardContainer } from '../modifierCardContainer/bwwModifierCardContainer';
import ModifierCard from '../../atoms/ModifierCard';

interface IModifierToDisplay {
    title: string;
    count: number;
    modifiers: IDisplayProduct[];
}

const RECOMMENDED_GROUP_TITLE = 'Recommended';

const modifiersSortOrder: string[] = [
    RECOMMENDED_GROUP_TITLE,
    ProductIntensityEnum.MILD,
    ProductIntensityEnum.MEDIUM,
    ProductIntensityEnum.HOT,
    ProductIntensityEnum.WILD,
    null,
];

export const sortModifiers = (a: IModifierToDisplay, b: IModifierToDisplay): number =>
    modifiersSortOrder.indexOf(a.title) - modifiersSortOrder.indexOf(b.title);

interface IAllSaucesModifiersModalProps {
    isOpen?: boolean;
    onClose: () => void;
    modifierGroup: IDisplayModifierGroup;
    selectionType: 'multi' | 'single';
    limitText: string;
    selectedModifiersText?: string;
}

const AllSaucesModifiersModal: FC<IAllSaucesModifiersModalProps> = (props) => {
    const { isOpen = false, onClose, modifierGroup, selectionType, limitText, selectedModifiersText } = props;
    const [selectedFilters, setSelectedFilters] = useState([]);

    const handleChangeFilter = useCallback((selectedFilters) => {
        setSelectedFilters(selectedFilters);
    }, []);

    const modifiersGroupedByIntensity = groupBy(modifierGroup.modifiers, (it) => it.displayProductDetails.intensity);
    const intensityGroups = Object.entries(modifiersGroupedByIntensity).map(([title, modifiers]) => ({
        title: title === 'null' ? null : title,
        count: modifiers.length,
        modifiers,
    }));

    const recommendedModifiers = modifierGroup.modifiers.filter((it) => it.displayProductDetails.isRecommended);

    const allGroups = recommendedModifiers.length
        ? intensityGroups.concat({
              title: RECOMMENDED_GROUP_TITLE,
              count: null,
              modifiers: recommendedModifiers,
          })
        : intensityGroups;

    const groupedModifiersToDisplay = allGroups
        .filter(({ title }) => !selectedFilters.length || selectedFilters.includes(title))
        .sort(sortModifiers);

    const isSaveAndCloseDisabled = useMemo(() => modifierGroup.modifiers.filter(isItemSelected).length === 0, [
        modifierGroup,
    ]);

    return (
        <ModifiersModal
            isOpen={isOpen}
            subtitle="Choose"
            title={modifierGroup.displayName}
            onClose={onClose}
            hintText={limitText}
            footerContent={
                <ModalFooterContent
                    selections={selectedModifiersText}
                    onSaveAndCloseClick={onClose}
                    isSaveAndCloseDisabled={isSaveAndCloseDisabled}
                />
            }
        >
            <div className={classNames(styles.modalContent, styles.sauces)}>
                <div>
                    <SaucesFilter selectedFilters={selectedFilters} onChangeFilter={handleChangeFilter} />
                    <Divider orientation="horizontal" className={styles.filterDivider} />
                    {groupedModifiersToDisplay.map((group) => {
                        return (
                            <div className={styles.groupWrapper} key={group.title}>
                                {group.title ? (
                                    <div className={styles.titleWrapper}>
                                        <h3
                                            aria-label={`${group.title} category`}
                                            className={classNames(styles.title, 't-header-h3')}
                                        >
                                            {group.title}
                                        </h3>
                                        {group.count && (
                                            <span className={classNames(styles.count, 't-paragraph-small')}>
                                                ({group.count})
                                            </span>
                                        )}
                                    </div>
                                ) : (
                                    <Divider
                                        aria-label="uncategorised"
                                        orientation="horizontal"
                                        className={styles.noSauceDivider}
                                    />
                                )}
                                <div className={styles.group}>
                                    {group.modifiers.map((item) => {
                                        const {
                                            quantity,
                                            productId,
                                            intensity,
                                            price,
                                            calories,
                                        } = item.displayProductDetails;

                                        return (
                                            <BwwModifierCardContainer
                                                key={productId}
                                                modifierGroupId={modifierGroup.modifierGroupId}
                                                selectionType={selectionType}
                                                displayProduct={item}
                                            >
                                                {({
                                                    contentfulProduct,
                                                    selectorType,
                                                    showModify,
                                                    onClick,
                                                    onPlus,
                                                    onMinus,
                                                    isSelected,
                                                    isDisabled,
                                                    canDecrement,
                                                    canIncrement,
                                                    productName,
                                                    maxQuantityTooltipText,
                                                    showPrice,
                                                    requireSelectionModifiersGroups,
                                                }) => {
                                                    return (
                                                        <div className={styles.modifierCard}>
                                                            <ModifierCard
                                                                canDecrement={canDecrement}
                                                                canIncrement={canIncrement}
                                                                contentfulProduct={contentfulProduct}
                                                                productName={productName}
                                                                modifierItemId={productId}
                                                                onCardMinus={onMinus}
                                                                onCardPlus={onPlus}
                                                                onClick={onClick}
                                                                quantity={quantity}
                                                                calories={calories}
                                                                price={price}
                                                                selectionType={selectionType}
                                                                showModify={showModify}
                                                                selectorType={selectorType}
                                                                selected={isSelected}
                                                                disabled={isDisabled}
                                                                maxQuantityTooltipText={maxQuantityTooltipText}
                                                                intensity={intensity}
                                                                showPrice={showPrice}
                                                                requireSelectionModifiersGroups={
                                                                    requireSelectionModifiersGroups
                                                                }
                                                            />
                                                        </div>
                                                    );
                                                }}
                                            </BwwModifierCardContainer>
                                        );
                                    })}
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </ModifiersModal>
    );
};

export default AllSaucesModifiersModal;
