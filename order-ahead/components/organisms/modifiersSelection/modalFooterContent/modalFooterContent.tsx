import React, { FC } from 'react';

import styles from './modalFooterContent.module.css';

import classNames from 'classnames';
import { InspireButton } from '../../../atoms/button';

interface IModalFooterContentProps {
    selections?: string;
    onSaveAndCloseClick: (e) => void;
    isSaveAndCloseDisabled?: boolean;
}

const ModalFooterContent: FC<IModalFooterContentProps> = ({
    selections,
    onSaveAndCloseClick,
    isSaveAndCloseDisabled,
}) => {
    return (
        <div className={styles.footerContent}>
            {selections && (
                <div className={classNames(styles.selections, 't-paragraph-small')}>
                    <span className={styles.selectionsTitle}>Selections: </span>
                    {selections}
                </div>
            )}
            <div className={styles.footerButtons}>
                <InspireButton
                    className={styles.footerButton}
                    onClick={onSaveAndCloseClick}
                    disabled={isSaveAndCloseDisabled}
                    type="primary"
                    text="Save & Close"
                />
            </div>
        </div>
    );
};

export default ModalFooterContent;
