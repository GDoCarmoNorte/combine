import React, { FC } from 'react';
import classNames from 'classnames';
import styles from './sauceModifiersSelection.module.css';
import useModifiersSelection from './useModifiersSelection';
import { useBwwSaucesDisplayModifierGroup } from '../../../redux/hooks/pdp';
import AllSaucesModifiersModal from './allSaucesModifiersModal';
import Checkbox from '../../atoms/checkbox';
import { usePdp } from '../../../redux/hooks';
import SelectionHeader from './selectionHeader';
import ProductsList from './productsList';

interface ISauceModifiersSelectionProps {
    className?: string;
    title: string;
    productGroupId: string;
    productId: string;
}

const SauceModifiersSelection: FC<ISauceModifiersSelectionProps> = ({
    className,
    title,
    productGroupId,
    productId,
}) => {
    const modifierGroup = useBwwSaucesDisplayModifierGroup(productGroupId);
    const {
        limitText,
        modifiersToDisplay,
        selectionType,
        shouldShowViewAllBtn,
        onViewAll,
        onCloseModal,
        isModalOpened,
        selectedModifiersText,
        isMinimumAmountReached,
    } = useModifiersSelection(modifierGroup, productId);

    const {
        actions: { onSauceOnSideChange },
    } = usePdp();

    const onCheckboxClick = () => {
        if (!modifierGroup.isOnSideOptionDisabled) {
            onSauceOnSideChange({ productGroupId });
        }
    };

    return (
        <div className={classNames(className, styles.container)}>
            <SelectionHeader
                title={title}
                limitText={limitText}
                onViewAll={onViewAll}
                shouldShowViewAllBtn={shouldShowViewAllBtn}
                className={styles.seletionHeader}
                shouldShowRequired={!isMinimumAmountReached}
            />
            <div className={styles.productList}>
                <ProductsList
                    modifiers={modifiersToDisplay}
                    onViewAllClick={onViewAll}
                    shouldShowViewAllBtn={shouldShowViewAllBtn}
                    selectionType={selectionType}
                    viewAllBtnTitle={`View All ${title}`}
                    modifierGroupId={modifierGroup.modifierGroupId}
                />
                {modifierGroup.hasOnSideOption && (
                    <div
                        className={classNames(styles.sauceOnSide, {
                            [styles.disabled]: modifierGroup.isOnSideOptionDisabled,
                        })}
                    >
                        <Checkbox
                            className={styles.checkbox}
                            fieldName="sauce-on-side"
                            onClick={onCheckboxClick}
                            selected={modifierGroup.isOnSideChecked}
                            ariaLabel="Sauce on the side"
                        />
                        <span className={styles.checkboxLabel} onClick={onCheckboxClick}>
                            Sauce on the side
                        </span>
                    </div>
                )}
            </div>
            <AllSaucesModifiersModal
                isOpen={isModalOpened}
                modifierGroup={modifierGroup}
                onClose={onCloseModal}
                selectionType={selectionType}
                limitText={limitText}
                selectedModifiersText={selectedModifiersText}
            />
        </div>
    );
};

export default SauceModifiersSelection;
