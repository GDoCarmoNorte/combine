import React, { FC } from 'react';
import { InspireLinkButton } from '../../../atoms/link';
import classNames from 'classnames';
import styles from './selectionHeader.module.css';

interface ISelectionTitleProps {
    title: string;
    limitText?: string;
    shouldShowViewAllBtn?: boolean;
    onViewAll: () => void;
    className?: string;
    shouldShowRequired?: boolean;
}

const SelectionHeader: FC<ISelectionTitleProps> = ({
    title,
    limitText,
    onViewAll,
    shouldShowViewAllBtn,
    className,
    shouldShowRequired,
}) => {
    return (
        <div className={classNames(className, styles.headerWrapper)}>
            <div className={classNames(styles.titleWrapper)}>
                <h2 className={classNames(styles.title, 't-header-card-title')}>{title}</h2>
                {limitText && <span className={classNames(styles.count, 't-paragraph-hint')}>{limitText}</span>}
                {shouldShowViewAllBtn && (
                    <InspireLinkButton className={styles.viewAllButton} linkType="secondary" onClick={onViewAll}>
                        View All
                    </InspireLinkButton>
                )}
            </div>
            {shouldShowRequired && <span className={classNames(styles.requiredLabel)}>REQUIRED</span>}
        </div>
    );
};

export default SelectionHeader;
