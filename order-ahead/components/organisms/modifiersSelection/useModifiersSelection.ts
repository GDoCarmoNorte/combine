import { useCallback, useEffect, useLayoutEffect, useMemo, useState } from 'react';
import { IDisplayModifierGroup, IDisplayProduct } from '../../../redux/types';
import { useMediaQuery } from '@material-ui/core';
import { usePrevious } from '../../../common/hooks/usePreviousState';

const MAX_CARDS_ROW_NUMBER_MOBILE = 6;
const MAX_CARDS_ROW_NUMBER_DESKTOP = 4;
const MAX_ROWS_NUMBER_DESKTOP = 2;

interface IUseModifiersSelection {
    selectionType: 'multi' | 'single';
    shouldShowViewAllBtn: boolean;
    limitText: string;
    modifiersToDisplay: IDisplayProduct[];
    isModalOpened: boolean;
    selectedModifiersText?: string;
    onViewAll: () => void;
    onCloseModal: () => void;
    isMinimumAmountReached: boolean;
}

const getItemId = (item: IDisplayProduct) => item.displayProductDetails.productId;
export const isItemSelected = (item: IDisplayProduct): boolean => !!item.displayProductDetails.quantity;
const isItemNotSelected = (item: IDisplayProduct) => !isItemSelected(item);

const getModifiersByIds = (modifiersIds: string[], allModifiers: IDisplayProduct[]) => {
    return modifiersIds.map((item) => allModifiers.find((it) => getItemId(it) === item));
};

export const getInitialModifiersToDisplayIds = (allModifiers: IDisplayProduct[], length: number): string[] => {
    if (allModifiers.length <= length) {
        return allModifiers.map(getItemId);
    }
    const currentVisible = allModifiers.slice(0, length);

    return getNewModifiersToDisplayIds(allModifiers, currentVisible.map(getItemId));
};

export const getNewModifiersToDisplayIds = (
    allModifiers: IDisplayProduct[],
    currentModifiersToDisplayIds: string[]
): string[] => {
    const allSelectedModifiersIds = allModifiers.filter(isItemSelected).map(getItemId);
    const invisibleSelectedModifiersIdsQueue = allSelectedModifiersIds.filter(
        (it) => currentModifiersToDisplayIds.indexOf(it) === -1
    );
    if (!invisibleSelectedModifiersIdsQueue.length) {
        return currentModifiersToDisplayIds;
    }

    const modifiersToDisplay = getModifiersByIds(currentModifiersToDisplayIds, allModifiers);

    return currentModifiersToDisplayIds.map((id, index) => {
        const item = modifiersToDisplay[index];

        if (isItemNotSelected(item) && invisibleSelectedModifiersIdsQueue.length) {
            return invisibleSelectedModifiersIdsQueue.shift();
        }

        return id;
    });
};

const useModifiersSelection = (modifierGroup: IDisplayModifierGroup, productId: string): IUseModifiersSelection => {
    const { maxQuantity, modifiers, minQuantity } = modifierGroup;
    const selectionType = maxQuantity > 1 ? 'multi' : 'single';
    const allModifiersNumber = modifierGroup.modifiers.length;

    const limitText = useMemo(() => {
        if (maxQuantity === 1) {
            return '(Select one)';
        }

        if (selectionType === 'multi') {
            return `(Up to ${maxQuantity})`;
        }

        return null;
    }, [maxQuantity, selectionType]);

    const isSmallVariant = useMediaQuery('(max-width: 767px)');

    const modifiersNumberToDisplay = useMemo(() => {
        if (isSmallVariant) {
            if (allModifiersNumber <= MAX_CARDS_ROW_NUMBER_MOBILE) {
                return modifiers.length;
            }

            return MAX_CARDS_ROW_NUMBER_MOBILE - 1;
        }

        if (maxQuantity < MAX_CARDS_ROW_NUMBER_DESKTOP) {
            return MAX_CARDS_ROW_NUMBER_DESKTOP - 1;
        }

        return MAX_CARDS_ROW_NUMBER_DESKTOP * MAX_ROWS_NUMBER_DESKTOP - 1;
    }, [modifiers, isSmallVariant, allModifiersNumber, maxQuantity]);

    const [isModalOpened, setIsModalOpened] = useState(false);

    const [modifiersToDisplayIds, setModifiersToDisplayIds] = useState<string[]>(() =>
        getInitialModifiersToDisplayIds(modifiers, modifiersNumberToDisplay)
    );

    const onViewAll = useCallback(() => {
        setIsModalOpened(true);
    }, []);

    const modifiersToDisplay = getModifiersByIds(modifiersToDisplayIds, modifiers).filter(Boolean);

    // re-initialize modifiers to display if modifiers are changing
    useLayoutEffect(() => {
        const isSomeItemIsNotPresent = modifiersToDisplayIds.some(
            (id) => !modifiers.find((it) => getItemId(it) === id)
        );

        if (isSomeItemIsNotPresent) {
            const initialModifiersToDisplayIds = getInitialModifiersToDisplayIds(modifiers, modifiersNumberToDisplay);

            setModifiersToDisplayIds(initialModifiersToDisplayIds);
        }
    }, [modifiers, modifiersToDisplayIds, modifiersNumberToDisplay]);

    const onCloseModal = () => {
        const newModifiersToDisplauIds = getNewModifiersToDisplayIds(modifiers, modifiersToDisplayIds);

        setModifiersToDisplayIds(newModifiersToDisplauIds);
        setIsModalOpened(false);
    };

    const prevModifiersNumberToDisplay = usePrevious(modifiersNumberToDisplay);
    const prevProductId = usePrevious(productId);

    const initialModifiersToDisplayIds = getInitialModifiersToDisplayIds(modifiers, modifiersNumberToDisplay);

    useEffect(() => {
        if (modifiersNumberToDisplay !== prevModifiersNumberToDisplay) {
            setModifiersToDisplayIds(initialModifiersToDisplayIds);
        }
    }, [modifiersNumberToDisplay, initialModifiersToDisplayIds, prevModifiersNumberToDisplay, modifiers]);

    // re-initialize modifiers to display on product size change
    useEffect(() => {
        if (productId !== prevProductId) {
            setModifiersToDisplayIds(initialModifiersToDisplayIds);
        }
    }, [productId, initialModifiersToDisplayIds, prevProductId]);

    const selectedModifiers = modifierGroup.modifiers.filter(isItemSelected);

    const selectedModifiersText = selectedModifiers.map((it) => it.displayName).join(', ') || null;

    const isMinimumAmountReached = minQuantity <= selectedModifiers.length;

    return {
        selectionType,
        shouldShowViewAllBtn: allModifiersNumber > modifiersNumberToDisplay,
        limitText,
        modifiersToDisplay,
        selectedModifiersText,
        isModalOpened,
        onViewAll,
        onCloseModal,
        isMinimumAmountReached,
    };
};

export default useModifiersSelection;
