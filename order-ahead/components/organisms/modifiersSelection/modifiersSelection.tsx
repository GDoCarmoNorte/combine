import React, { FC } from 'react';
import classNames from 'classnames';
import styles from './modifiersSelection.module.css';
import useModifiersSelection from './useModifiersSelection';
import { useBwwDisplayModifierGroup } from '../../../redux/hooks/pdp';
import AllModifiersModal from './allModifiersModal';
import SelectionHeader from './selectionHeader';
import ProductsList from './productsList';

interface IModifiersSelectionProps {
    className?: string;
    title: string;
    productGroupId: string;
    productId: string;
}

const ModifiersSelection: FC<IModifiersSelectionProps> = ({ className, title, productGroupId, productId }) => {
    const modifierGroup = useBwwDisplayModifierGroup(productGroupId);

    const {
        limitText,
        modifiersToDisplay,
        selectionType,
        shouldShowViewAllBtn,
        onViewAll,
        onCloseModal,
        isModalOpened,
        selectedModifiersText,
        isMinimumAmountReached,
    } = useModifiersSelection(modifierGroup, productId);

    return (
        <>
            {!!modifierGroup?.modifiers?.length && (
                <div className={classNames(className, styles.container)}>
                    <SelectionHeader
                        title={title}
                        limitText={limitText}
                        onViewAll={onViewAll}
                        shouldShowViewAllBtn={shouldShowViewAllBtn}
                        className={styles.seletionHeader}
                        shouldShowRequired={!isMinimumAmountReached}
                    />
                    <ProductsList
                        modifierGroupId={modifierGroup.modifierGroupId}
                        modifiers={modifiersToDisplay}
                        onViewAllClick={onViewAll}
                        shouldShowViewAllBtn={shouldShowViewAllBtn}
                        selectionType={selectionType}
                        viewAllBtnTitle={`View All ${title}`}
                    />
                    <AllModifiersModal
                        isOpen={isModalOpened}
                        modifierGroup={modifierGroup}
                        onClose={onCloseModal}
                        selectionType={selectionType}
                        limitText={limitText}
                        selectedModifiersText={selectedModifiersText}
                    />
                </div>
            )}
        </>
    );
};

export default ModifiersSelection;
