import React, { FC, useCallback, useState } from 'react';
import { VerticalModifierCard, VerticalViewAllModifiersCard } from '../../../atoms/verticalModifierCard';

import styles from './productsList.module.css';
import { IDisplayProduct } from '../../../../redux/types';
import { BwwModifierCardContainer } from '../../modifierCardContainer/bwwModifierCardContainer';
import NestedModifiersModal from '../../nestedModifiersModal/nestedModifiersModal';

interface IProductsListProps {
    shouldShowViewAllBtn: boolean;
    viewAllBtnTitle: string;
    modifierGroupId: string;
    modifiers: IDisplayProduct[];
    onViewAllClick: () => void;
    selectionType: 'multi' | 'single';
}
const ProductsList: FC<IProductsListProps> = ({
    modifiers,
    modifierGroupId,
    selectionType,
    onViewAllClick,
    shouldShowViewAllBtn,
    viewAllBtnTitle,
}) => {
    const [modalOpened, setModalOpen] = useState<boolean>(false);
    const handleOpenModal = useCallback((e) => {
        e.stopPropagation();
        setModalOpen(true);
    }, []);

    const handleCloseModal = (e) => {
        e.stopPropagation();
        setModalOpen(false);
    };

    return (
        <div className={styles.productListWrapper}>
            {modifiers.map((item) => {
                const { productId } = item.displayProductDetails;
                return (
                    <BwwModifierCardContainer
                        key={productId}
                        modifierGroupId={modifierGroupId}
                        selectionType={selectionType}
                        displayProduct={item}
                    >
                        {({
                            contentfulProduct,
                            selectorType,
                            showModify,
                            onClick,
                            onPlus,
                            onMinus,
                            isSelected,
                            isDisabled,
                            canDecrement,
                            canIncrement,
                            productName,
                            modifierGroups,
                            modifiersText,
                            maxQuantityTooltipText,
                            showPrice,
                            requireSelectionModifiersGroups,
                        }) => (
                            <>
                                <VerticalModifierCard
                                    className={styles.productCard}
                                    selectionType={selectionType}
                                    selectorType={selectorType}
                                    productName={productName}
                                    quantity={item.displayProductDetails.quantity}
                                    calories={item.displayProductDetails.calories}
                                    price={showPrice ? item.displayProductDetails.price : undefined}
                                    selected={isSelected}
                                    disabled={isDisabled}
                                    showModify={showModify}
                                    modifierItemId={item.displayProductDetails.productId}
                                    onCardPlus={onPlus}
                                    onCardMinus={onMinus}
                                    onClick={onClick}
                                    onModifyClick={handleOpenModal}
                                    intensity={item.displayProductDetails.intensity}
                                    contentfulProduct={contentfulProduct}
                                    canIncrement={canIncrement}
                                    canDecrement={canDecrement}
                                    modifiersText={modifiersText}
                                    maxQuantityTooltipText={maxQuantityTooltipText}
                                    requireSelectionModifiersGroups={requireSelectionModifiersGroups}
                                />
                                {showModify && item.displayProductDetails.quantity > 0 && (
                                    <NestedModifiersModal
                                        isOpen={modalOpened}
                                        onClose={handleCloseModal}
                                        modifier={item}
                                        modifierGroupId={modifierGroupId}
                                        modifierGroups={modifierGroups}
                                        requireSelectionModifiersGroups={requireSelectionModifiersGroups}
                                    />
                                )}
                            </>
                        )}
                    </BwwModifierCardContainer>
                );
            })}

            {shouldShowViewAllBtn && (
                <VerticalViewAllModifiersCard
                    key="view-all"
                    title={viewAllBtnTitle}
                    onClick={onViewAllClick}
                    className={styles.viewAllCard}
                />
            )}
        </div>
    );
};

export default ProductsList;
