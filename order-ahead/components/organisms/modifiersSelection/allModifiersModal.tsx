import React, { FC, useCallback, useState } from 'react';

import styles from './allModifiersModal.module.css';
import { IDisplayModifierGroup } from '../../../redux/types';

import ModifiersModal from '../../atoms/modifiersModal';
import classNames from 'classnames';
import ModalFooterContent from './modalFooterContent';
import { BwwModifierCardContainer } from '../modifierCardContainer/bwwModifierCardContainer';
import ModifierCard from '../../atoms/ModifierCard';
import NestedModifiersModal from '../nestedModifiersModal/nestedModifiersModal';

interface IAllModifiersModalProps {
    isOpen: boolean;
    onClose: () => void;
    modifierGroup: IDisplayModifierGroup;
    selectionType: 'multi' | 'single';
    limitText: string;
    selectedModifiersText?: string;
}

const AllModifiersModal: FC<IAllModifiersModalProps> = (props) => {
    const { isOpen, onClose, modifierGroup, selectionType, limitText, selectedModifiersText } = props;

    const [modalOpened, setModalOpen] = useState<boolean>(false);
    const handleOpenModal = useCallback((e) => {
        e.stopPropagation();
        setModalOpen(true);
    }, []);

    const handleCloseModal = (e) => {
        e.stopPropagation();
        setModalOpen(false);
    };

    return (
        <ModifiersModal
            isOpen={isOpen}
            subtitle="Modify"
            title={modifierGroup.displayName}
            hintText={limitText}
            onClose={onClose}
            footerContent={<ModalFooterContent selections={selectedModifiersText} onSaveAndCloseClick={onClose} />}
        >
            <div className={classNames(styles.modalContent)}>
                {modifierGroup.modifiers.map((item) => {
                    const { quantity, productId, price, calories } = item.displayProductDetails;

                    return (
                        <BwwModifierCardContainer
                            key={productId}
                            modifierGroupId={modifierGroup.modifierGroupId}
                            selectionType={selectionType}
                            displayProduct={item}
                        >
                            {({
                                contentfulProduct,
                                selectorType,
                                showModify,
                                onClick,
                                onPlus,
                                onMinus,
                                isSelected,
                                isDisabled,
                                canDecrement,
                                canIncrement,
                                productName,
                                maxQuantityTooltipText,
                                modifierGroups,
                                modifiersText,
                                showPrice,
                                requireSelectionModifiersGroups,
                            }) => {
                                return (
                                    <div className={styles.modifierCard}>
                                        <ModifierCard
                                            canDecrement={canDecrement}
                                            canIncrement={canIncrement}
                                            contentfulProduct={contentfulProduct}
                                            productName={productName}
                                            modifierItemId={productId}
                                            onCardMinus={onMinus}
                                            onCardPlus={onPlus}
                                            onClick={onClick}
                                            onModifyClick={handleOpenModal}
                                            quantity={quantity}
                                            calories={calories}
                                            price={price}
                                            selectionType={selectionType}
                                            showModify={showModify}
                                            selectorType={selectorType}
                                            selected={isSelected}
                                            disabled={isDisabled}
                                            maxQuantityTooltipText={maxQuantityTooltipText}
                                            showPrice={showPrice}
                                            modifiers={modifiersText}
                                            requireSelectionModifiersGroups={requireSelectionModifiersGroups}
                                        />
                                        {showModify && item.displayProductDetails.quantity > 0 && (
                                            <NestedModifiersModal
                                                isOpen={modalOpened}
                                                onClose={handleCloseModal}
                                                modifier={item}
                                                modifierGroupId={modifierGroup.modifierGroupId}
                                                modifierGroups={modifierGroups}
                                                requireSelectionModifiersGroups={requireSelectionModifiersGroups}
                                            />
                                        )}
                                    </div>
                                );
                            }}
                        </BwwModifierCardContainer>
                    );
                })}
            </div>
        </ModifiersModal>
    );
};

export default AllModifiersModal;
