import React from 'react';
import classnames from 'classnames';

import styles from './teamsBlock.module.css';
import { ISurveyModel } from '../../../../@generated/webExpApi';
import { InspireLink } from '../../../atoms/link';
import { formatSurveyName } from '../../../../common/helpers/survey';

interface IMyTeamsBlockProps {
    myTeamsSurvey: ISurveyModel;
}

function MyTeamsBlock({ myTeamsSurvey }: IMyTeamsBlockProps) {
    if (!myTeamsSurvey.questions.length) return null;

    return (
        <section className={styles.section} id="add-teams">
            <h2 className={classnames('t-header-h2', styles.heading)}>ADD TEAMS</h2>
            <div className={styles.wrapper}>
                {myTeamsSurvey.questions.map((question) => (
                    <div className={classnames(styles.itemContainer)} key={question.id}>
                        <div>
                            <span role="button" className={classnames('t-product-title', styles.teamName)}>
                                {formatSurveyName(question.text)}
                            </span>
                        </div>
                        <InspireLink link={`/account/my-teams/?team=${question.id}`} type="primary">
                            SELECT TEAM
                        </InspireLink>
                    </div>
                ))}
            </div>
        </section>
    );
}

export default MyTeamsBlock;
