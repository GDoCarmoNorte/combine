import React, { useState } from 'react';
import classnames from 'classnames';

import { ISurveyModel, ISurveyRespondModel } from '../../../../@generated/webExpApi';
import { TEAM_NOT_SELECTED_ID } from '../constants';
import { formatSurveyName, getDefaultSurveyRespond } from '../../../../common/helpers/survey';
import { TypeSurveyEnum } from '../../../../common/hooks/useSurvey';

import Icon from '../../../atoms/BrandIcon';
import TeamsBanner from '../teamsBanner';
import { InspireLinkButton, InspireLink } from '../../../atoms/link';

import styles from './myTeamsBlock.module.css';

interface IMyTeamsBlockProps {
    myTeamsSurvey: ISurveyModel;
    updateTeamsSurvey: (payload: {
        surveyId: string;
        surveyRespond: ISurveyRespondModel[];
        typeSurvey: TypeSurveyEnum;
    }) => void;
}

function MyTeamsBlock({ myTeamsSurvey, updateTeamsSurvey }: IMyTeamsBlockProps) {
    const myReplies = myTeamsSurvey.replies.filter((reply) => reply.id !== TEAM_NOT_SELECTED_ID);
    const [viewAll, setViewAll] = useState(false);

    const handleRemove = (replyId: string, replyQuestionId: string) => {
        const surveyRespond: ISurveyRespondModel[] = myReplies
            .filter((reply) => !(reply.questionId === replyQuestionId && reply.id === replyId))
            .map((reply) => ({
                questionId: reply.questionId,
                answerId: reply.id,
            }));

        updateTeamsSurvey({
            surveyId: myTeamsSurvey.id,
            surveyRespond: [...surveyRespond, ...getDefaultSurveyRespond(myTeamsSurvey, surveyRespond)],
            typeSurvey: TypeSurveyEnum.DELETE,
        });
    };

    if (!myReplies.length) return <TeamsBanner />;

    const replies = viewAll ? myReplies : myReplies.slice(0, 6);

    return (
        <section>
            <div className={styles.headerWrapper}>
                <h2 className={classnames('t-header-h2', styles.header)}>MY TEAMS</h2>
                <InspireLink link="#add-teams" type="secondary">
                    ADD TEAMS
                </InspireLink>
            </div>
            <div className={styles.wrapper}>
                {replies.map((reply) => (
                    <div className={styles.itemContainer} key={`${reply.id}${reply.questionId}`}>
                        <span className="t-subheader-small">{reply.text}</span>
                        <span className="t-paragraph-small">{formatSurveyName(reply.questionText)}</span>
                        <InspireLinkButton
                            aria-label="remove my team"
                            linkType="secondary"
                            className={styles.button}
                            onClick={() => handleRemove(reply.id, reply.questionId)}
                        >
                            REMOVE
                        </InspireLinkButton>
                    </div>
                ))}
            </div>
            {!viewAll && myReplies.length > 6 && (
                <div className={styles.collapse}>
                    <Icon className={styles.icon} icon="direction-down" />
                    <InspireLinkButton
                        aria-label="view all my teams"
                        linkType="secondary"
                        className={styles.button}
                        onClick={() => setViewAll(true)}
                    >
                        VIEW ALL
                    </InspireLinkButton>
                </div>
            )}
        </section>
    );
}

export default MyTeamsBlock;
