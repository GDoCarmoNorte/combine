import React from 'react';
import classnames from 'classnames';
import styles from './teamsBanner.module.css';

const TeamsBanner = () => {
    return (
        <div className={styles.container}>
            <img src={'/brands/bww/Brand.svg'} alt="" />
            <div className={styles.textWrapper}>
                <h3 className={classnames(styles.bannerTitle, 't-header-card-title')}>
                    Tell us your teams for 50 points
                </h3>
                <p className={classnames(styles.bannerDescription, 't-paragraph')}>
                    Add teams, get points. It’s that easy.
                </p>
            </div>
        </div>
    );
};

export default TeamsBanner;
