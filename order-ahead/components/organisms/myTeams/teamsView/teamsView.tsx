import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import classnames from 'classnames';

import { ISurveyRespondModel } from '../../../../@generated/webExpApi';
import useSurvey from '../../../../common/hooks/useSurvey';
import { formatSurveyName, getDefaultSurveyRespond } from '../../../../common/helpers/survey';
import { TEAM_NOT_SELECTED_ID } from '../constants';

import BrandLoader from '../../../atoms/BrandLoader';
import BrandIcon from '../../../../components/atoms/BrandIcon';
import { InspireLink } from '../../../atoms/link';
import Checkbox from '../../../atoms/checkbox';
import TeamsSavePanel from './TeamsSavePanel';

import styles from './teamsView.module.css';

interface ITeamsViewProps {
    teamId: string;
}

export interface ISelectedTeams {
    id?: string;
    questionId?: string;
    questionText?: string;
    text?: string;
}

function TeamsView({ teamId }: ITeamsViewProps) {
    const isMobile = useMediaQuery('(max-width: 960px)');
    const {
        myTeams,
        isLoading,
        actions: { updateSurveys },
    } = useSurvey();

    const router = useRouter();
    const [selectedTeams, setSelectedTeams] = useState<ISelectedTeams[]>([]);
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        if (myTeams) {
            const preSelectedTeams =
                myTeams?.replies?.filter(
                    ({ questionId, id }) => questionId === teamId && id !== TEAM_NOT_SELECTED_ID
                ) || [];
            setSelectedTeams(preSelectedTeams);
        }
    }, [myTeams, teamId]);

    const sportsCategory = myTeams?.questions.find(({ id }) => id === teamId);
    const myReplies = myTeams?.replies.filter(({ questionId }) => questionId !== teamId);

    if (!sportsCategory) return null;
    if (isLoading) return <BrandLoader className={styles.loader} />;

    const { answers: teamsList, text: categoryText } = sportsCategory;

    const isSelectedTeam = (team: ISelectedTeams): boolean => selectedTeams?.some(({ id }) => id === team.id);

    const handleClick = (team: ISelectedTeams): void => {
        if (isSelectedTeam(team)) {
            setSelectedTeams((selectedTeams) => selectedTeams.filter(({ id }) => id !== team.id));
        } else {
            setSelectedTeams((selectedTeams) => [...selectedTeams, { ...team, questionId: sportsCategory.id }]);
        }
    };

    const handleSaveTeams = () => {
        const surveyRespond: ISurveyRespondModel[] = selectedTeams?.map(({ questionId, id }) => ({
            questionId,
            answerId: id,
        }));

        setLoading(true);
        const changedSurveyRespond = [
            ...surveyRespond,
            ...myReplies.map(({ questionId, id }) => ({ questionId, answerId: id })),
        ];
        updateSurveys({
            surveyId: myTeams.id,
            surveyRespond: [...changedSurveyRespond, ...getDefaultSurveyRespond(myTeams, changedSurveyRespond)],
        }).finally(() => {
            setLoading(false);
            router.push('/account/my-teams');
        });
    };

    return (
        <section
            className={classnames(styles.section, {
                [styles.selectedTeams]: !!selectedTeams.length,
            })}
        >
            <BrandIcon size="xs" className={styles.backIcon} icon="direction-left" />
            <InspireLink link={`/account/my-teams/`} type="secondary">
                back to add teams
            </InspireLink>
            <h2 className={classnames('t-header-h2')}>{formatSurveyName(sportsCategory.text)}</h2>
            <div className={styles.wrapper}>
                {teamsList
                    .filter((team) => !team.isDefault)
                    .map((team) => (
                        <div
                            className={classnames(styles.teamContainer, {
                                [styles.selected]: isSelectedTeam(team),
                            })}
                            key={team.id}
                            onClick={() => handleClick(team)}
                            data-testid={team.text}
                        >
                            <div>
                                <span role="button" className={classnames('t-product-title', styles.teamName)}>
                                    {team.text}
                                </span>
                            </div>
                            <Checkbox
                                ariaLabel={`Select ${team.text}`}
                                selected={isSelectedTeam(team)}
                                fieldName={team.id}
                                size={isMobile ? 'standard' : 'large'}
                                tabIndex={-1}
                            />
                        </div>
                    ))}
            </div>
            {!!selectedTeams.length && (
                <TeamsSavePanel
                    onSaveTeams={handleSaveTeams}
                    selectedTeams={selectedTeams}
                    categoryText={categoryText}
                    loading={loading}
                />
            )}
        </section>
    );
}

export default TeamsView;
