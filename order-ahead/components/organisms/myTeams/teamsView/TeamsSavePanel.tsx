import React from 'react';
import { createPortal } from 'react-dom';
import { useMediaQuery } from '@material-ui/core';
import classnames from 'classnames';
import { InspireButton } from '../../../atoms/button';
import TextWithTrademark from '../../../atoms/textWithTrademark';
import { ISelectedTeams } from './teamsView';
import styles from './teamsSavePanel.module.css';
import Loader from '../../../atoms/Loader';

interface ISpecialPanel {
    onSaveTeams: () => void;
    selectedTeams: Array<ISelectedTeams>;
    categoryText: string;
    loading: boolean;
}

const TeamsSavePanel = ({ onSaveTeams, selectedTeams, categoryText, loading }: ISpecialPanel): JSX.Element => {
    const isMobile = useMediaQuery('(max-width: 768px)');
    const myTeamsPortal = document.getElementById('my-teams-portal');
    return createPortal(
        <div className={styles.panelWrapper}>
            <div className={styles.descriptionContainer}>
                <TextWithTrademark
                    tag="p"
                    text={isMobile ? categoryText : categoryText.replace(':', '')}
                    className={classnames('t-subheader', styles.bannerHeader)}
                />

                <p className={classnames('t-paragraph-hint', styles.subText)}>
                    {selectedTeams.map(({ text, id }, index) => (
                        <span key={id}>{index !== selectedTeams.length - 1 ? `${text}, ` : `${text}`}</span>
                    ))}
                </p>
            </div>
            <div className={styles.buttonWrapper}>
                <InspireButton
                    className={classnames(styles.buttonSave, {
                        [styles.buttonLoading]: loading,
                    })}
                    onClick={onSaveTeams}
                    disabled={loading}
                    type="primary"
                    text="save teams"
                />
                {loading && <Loader size={20} className={styles.signInLoading} />}
            </div>
        </div>,
        myTeamsPortal
    );
};

export default TeamsSavePanel;
