import React from 'react';
import MyTeamsBlock from './myTeamsBlock';
import AddTeamsBlock from './teamsBlock';
import useSurvey from '../../../common/hooks/useSurvey';
import BrandLoader from '../../atoms/BrandLoader';

const MyTeams = () => {
    const { myTeams, isLoading, actions } = useSurvey();

    if (isLoading) {
        return <BrandLoader />;
    }

    return (
        <>
            <MyTeamsBlock myTeamsSurvey={myTeams} updateTeamsSurvey={actions.updateSurveys} />
            <AddTeamsBlock myTeamsSurvey={myTeams} />
        </>
    );
};

export default MyTeams;
