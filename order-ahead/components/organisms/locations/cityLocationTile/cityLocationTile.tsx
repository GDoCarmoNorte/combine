import React, { FC, useMemo } from 'react';
import styles from './cityLocationTile.module.css';
import Icon from '../../../atoms/BrandIcon';
import { InspireButton } from '../../../atoms/button';
import { InspireLink } from '../../../atoms/link';
import { ILocationByStateOrProvinceDetailsModel } from '../../../../@generated/webExpApi';
import classnames from 'classnames';
import { useRouter } from 'next/router';
import { getStoreStatus } from '../../../../common/helpers/getStoreStatus';
import { formatPhoneNumber } from '../../../../lib/formatPhoneNumber';
import { getLocationById } from '../../../../common/services/locationService';
import { UNAVAILABLE_MESSAGE, SHOW_UNAVAILABLE } from './constants';
import { useDomainMenu, useOrderLocation, useTallyOrder } from '../../../../redux/hooks';
import { useLocationUnavailableError } from '../../../../common/hooks/useLocationUnavailableError';

export interface ICityLocationTileProps {
    location: ILocationByStateOrProvinceDetailsModel;
}

const CityLocationTile: FC<ICityLocationTileProps> = (props): JSX.Element => {
    const {
        id,
        displayName,
        url,
        addressMapLink,
        contactDetails: {
            address: { line, cityName, stateProvinceCode, postalCode },
            phone,
        },
        timezone,
        hoursByDay,
        isClosed,
        isOnlineOrderAvailable,
    } = props.location;

    const router = useRouter();
    const {
        actions: { getDomainMenu },
    } = useDomainMenu();

    const {
        actions: { setPickupLocation },
    } = useOrderLocation();
    const { setUnavailableTallyItems } = useTallyOrder();

    const { pushLocationUnavailableError } = useLocationUnavailableError();

    const status = useMemo(() => getStoreStatus(hoursByDay, timezone), [hoursByDay, timezone]);
    const formattedPhone = useMemo(() => formatPhoneNumber(phone || ''), [phone]);

    const getStatus = () => {
        if (status) {
            return (
                <p
                    className={classnames(
                        styles.storeStatus,
                        't-paragraph-hint',
                        status.isClosingSoon && styles.storeStatusClosingSoon
                    )}
                >
                    {status.statusText}
                </p>
            );
        }

        if (SHOW_UNAVAILABLE) {
            return <p className={classnames(styles.storeStatus, 't-paragraph-hint')}>{UNAVAILABLE_MESSAGE}</p>;
        }

        return null;
    };

    const handleClickViewMenu = async () => {
        const location = await getLocationById({
            locationId: id,
        });

        if (location && !location.isDigitallyEnabled) {
            pushLocationUnavailableError(location);
        }

        if (location) {
            setPickupLocation(location);
            getDomainMenu(location);
            setUnavailableTallyItems([]);
            router.push('/menu');
        }
    };

    const renderPrimaryCta = () => {
        if (isClosed || !isOnlineOrderAvailable) {
            return <InspireButton onClick={handleClickViewMenu} text="VIEW MENU" type="secondary" size="small" />;
        }

        return <InspireButton link={`/?locationId=${id}`} text="ORDER" type="small" />;
    };

    const statusView = getStatus();
    return (
        <div className={styles.wrapper}>
            <div className={styles.leftColumn}>
                <p className={styles.title}>
                    <InspireLink
                        link={`/locations/${url}`}
                        newtab={false}
                        onClick={(e) => {
                            e.stopPropagation();
                        }}
                        className={classnames('t-subheader', styles.titleLink)}
                    >
                        {displayName}
                    </InspireLink>
                </p>
                <InspireLink
                    link={addressMapLink}
                    newtab={true}
                    onClick={(e) => {
                        e.stopPropagation();
                    }}
                    className={classnames('t-paragraph-small', styles.adsressLink)}
                >
                    <p className={styles.address}>{line}</p>
                    <p className={styles.address}>{`${cityName}, ${stateProvinceCode} ${postalCode}`}</p>
                </InspireLink>
                <p className={classnames('t-paragraph-hint', styles.storeId)}>{`Store ID: ${id}`}</p>
                {statusView}
                {formattedPhone && (
                    <InspireLink link={`tel:${formattedPhone}`} className={styles.phoneLink}>
                        <Icon className={styles.icon} size="xs" icon="action-phone" variant="colorful" />
                        <p className={classnames(styles.phoneLinkTitle, 't-paragraph-small')}>{formattedPhone}</p>
                    </InspireLink>
                )}
            </div>
            <div className={styles.rightColumn}>{renderPrimaryCta()}</div>
        </div>
    );
};

export default CityLocationTile;
