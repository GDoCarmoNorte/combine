import React, { FC } from 'react';
import { Entry } from 'contentful';
import { ILocationByStateOrProvinceDetailsModel } from '../../../../@generated/webExpApi';
import getBrandInfo from '../../../../lib/brandInfo';
import { PageContentWrapper } from '../../../sections/PageContentWrapper';
import styles from './locationDetailsPageLayout.module.css';
import PageSections, { IPageSections } from '../../../sections';
import { IImageAssetFields, INotificationBannerFields } from '../../../../@generated/@types/contentful';
import NotificationBanner from '../../notificationBanner/notificationBanner';
import { LocationHeader } from '../../locationHeader';
import { LocationInfo } from '../../locationInfo';
import { getLocationNewsSectionsToExpand } from '../../../../common/helpers/locationHelper';

export interface ILocationDetailsPageLayoutProps {
    locationDetails: ILocationByStateOrProvinceDetailsModel;
    sections?: IPageSections;
    description?: string;
}

const LocationDetailsPageLayout: FC<ILocationDetailsPageLayoutProps> = (props) => {
    const { brandName } = getBrandInfo();
    const { sections, locationDetails, description } = props;
    const notificationBanner = sections?.find((s) => s.sys.contentType.sys.id === 'notificationBanner');
    const locationInfoImage = sections?.find((s) => s.sys.contentType.sys.id === 'imageAsset');
    const expandedSections = getLocationNewsSectionsToExpand(sections);

    return (
        <PageContentWrapper>
            <h1 className="visually-hidden">{`find an ${brandName}`}</h1>
            {notificationBanner && (
                <NotificationBanner entry={notificationBanner as Entry<INotificationBannerFields>} />
            )}
            <div>
                <LocationHeader locationDetails={locationDetails} />
                <LocationInfo
                    locationDetails={locationDetails}
                    image={locationInfoImage ? (locationInfoImage as Entry<IImageAssetFields>) : undefined}
                />
                {sections?.length > 0 && (
                    <PageSections
                        pageSections={sections}
                        classes={{
                            sectionLayout: {
                                mainTitle: styles.sectionLayoutMainTitle,
                            },
                            sectionBanner: {
                                wrapper: styles.sectionBannerWrapper,
                                icon: styles.sectionBannerIcon,
                                mainText: styles.sectionBannerMainText,
                                descriptionBlock: styles.sectionBannerDescriptionBlock,
                                title: styles.sectionBannerTitle,
                                description: styles.sectionBannerDescription,
                                secondaryButton: styles.sectionBannerSecondaryButton,
                            },
                        }}
                        data={{ locationDetails, description, expandedSections }}
                    />
                )}
            </div>
        </PageContentWrapper>
    );
};

export default LocationDetailsPageLayout;
