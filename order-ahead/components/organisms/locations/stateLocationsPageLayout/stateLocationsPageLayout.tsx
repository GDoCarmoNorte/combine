import React, { FC } from 'react';
import { PageContentWrapper } from '../../../sections/PageContentWrapper';
import StateLocationsTile from '../stateLocationsTile';
import { ILocationsByStateOrProvinceModel } from '../../../../@generated/webExpApi';
import { Entry } from 'contentful';
import NotificationBanner from '../../notificationBanner/notificationBanner';
import styles from './stateLocationsPageLayout.module.css';
import classNames from 'classnames';
import { IPageSections } from '../../../sections';
import { INotificationBannerFields } from '../../../../@generated/@types/contentful';
import Breadcrumbs from '../../../atoms/Breadcrumbs';
import PageMetaTitle from '../../../../components/atoms/pageMetaTitle';

export interface IStateLocationsPageLayoutProps {
    stateLocationDetails: ILocationsByStateOrProvinceModel;
    countryCode: string;
    sections?: IPageSections;
    pageMetaTitle: string;
}

const StateLocationsPageLayout: FC<IStateLocationsPageLayoutProps> = ({
    countryCode,
    stateLocationDetails,
    sections,
    pageMetaTitle,
}) => {
    const breadcrumbsPaths = [
        {
            title: 'locations',
            href: '/locations',
        },
        {
            title: 'all locations',
            href: '/locations/all',
        },
    ];

    const notificationBanner = sections?.find((s) => s.sys.contentType.sys.id === 'notificationBanner');

    return (
        <PageContentWrapper>
            {notificationBanner && (
                <NotificationBanner entry={notificationBanner as Entry<INotificationBannerFields>} />
            )}
            <div className={classNames(styles.wrapper)}>
                <Breadcrumbs paths={breadcrumbsPaths} />
                <PageMetaTitle text={pageMetaTitle} />
                <StateLocationsTile countryCode={countryCode} stateLocationDetails={stateLocationDetails} />
            </div>
        </PageContentWrapper>
    );
};

export default StateLocationsPageLayout;
