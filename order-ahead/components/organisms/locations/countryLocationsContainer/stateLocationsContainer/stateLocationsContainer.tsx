import Link from 'next/link';
import React, { FC } from 'react';
import { ILocationsByStateOrProvinceModel } from '../../../../../@generated/webExpApi';
import CityLocationsContainer from './cityLocationsContainer';

import styles from './stateLocationsContainer.module.css';
import { getGtmIdByName } from '../../../../../lib/gtm';
import Accordion from '../../../../atoms/accordion';
import classNames from 'classnames';

export interface IStateLocationsContainerProps {
    stateLocationDetails: ILocationsByStateOrProvinceModel;
    countryCode: string;
    selectedStateOrProvinceCode?: string;
}

const StateLocationsContainer: FC<IStateLocationsContainerProps> = (props) => {
    const { stateLocationDetails, countryCode, selectedStateOrProvinceCode } = props;
    const stateLocationsLinkHref = '/locations/[countryCode]/[stateOrProvinceCode]';
    const stateLocationsLinkAs = `/locations/${countryCode}/${stateLocationDetails.code}`.toLowerCase();
    const gtmId = getGtmIdByName('locationsPage', stateLocationDetails.name);

    const renderAccordionHeaderContent = (isExpanded, renderButton, onClickHandler) => {
        const header = stateLocationDetails.name;
        return (
            <div
                className={classNames(styles.accordion, {
                    [styles.expandedAccordion]: isExpanded,
                })}
                onClick={onClickHandler}
            >
                <div className={styles.stateDetailsWrapper}>
                    <Link href={stateLocationsLinkHref} as={stateLocationsLinkAs}>
                        <a
                            data-gtm-id={gtmId}
                            className={classNames('t-header-h3', styles.title)}
                            aria-label={`${header} location`}
                        >
                            {header}
                        </a>
                    </Link>

                    <div
                        className={classNames('t-paragraph-hint', styles.details)}
                    >{`${stateLocationDetails.count.toLocaleString()} ${
                        stateLocationDetails.count === 1 ? 'location' : 'locations'
                    }`}</div>
                </div>
                {renderButton()}
            </div>
        );
    };

    const renderAccordionContent = (isExpanded) => {
        return (
            <div
                className={classNames(styles.cityLocationsWrapper, {
                    [styles.expanded]: isExpanded,
                    [styles.collapsed]: !isExpanded,
                })}
            >
                {stateLocationDetails.cities &&
                    stateLocationDetails.cities.map((cityLocationDetails) => {
                        return (
                            <CityLocationsContainer
                                key={cityLocationDetails.displayName}
                                cityLocationDetails={cityLocationDetails}
                                countryCode={countryCode}
                                stateCode={stateLocationDetails.code}
                            />
                        );
                    })}
            </div>
        );
    };

    return (
        <Accordion
            renderHeader={(isExpanded, renderButton, onClickHandler) =>
                renderAccordionHeaderContent(isExpanded, renderButton, onClickHandler)
            }
            renderContent={(isExpanded) => renderAccordionContent(isExpanded)}
            containerClassName={styles.wrapper}
            isExpandedByDefault={
                selectedStateOrProvinceCode &&
                selectedStateOrProvinceCode.toUpperCase() === stateLocationDetails.code.toUpperCase()
            }
            iconClassName={styles.arrowIcon}
            title={stateLocationDetails.name}
        />
    );
};

export default StateLocationsContainer;
