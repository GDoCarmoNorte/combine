import Link from 'next/link';
import React, { FC } from 'react';
import { ILocationsByCityModel } from '../../../../../../@generated/webExpApi';
import { getGtmIdByName } from '../../../../../../lib/gtm';

import styles from './cityLocationsContainer.module.css';

export interface ICityLocationsContainerProps {
    cityLocationDetails: ILocationsByCityModel;
    stateCode: string;
    countryCode: string;
}

const CityLocationsContainer: FC<ICityLocationsContainerProps> = (props) => {
    const { cityLocationDetails, stateCode, countryCode } = props;
    const cityLocationsLinkHref = '/locations/[countryCode]/[stateOrProvinceCode]/[displayCityName]';
    const cityLocationsLinkAs = `/locations/${countryCode}/${stateCode}/${cityLocationDetails.displayName}`.toLowerCase();
    const gtmId = getGtmIdByName('locationsPage', cityLocationDetails.name);

    return (
        <div className={styles.wrapper}>
            <Link href={cityLocationsLinkHref} as={cityLocationsLinkAs}>
                <a data-gtm-id={gtmId} className={styles.link} aria-label={`${cityLocationDetails.name} location`}>
                    {cityLocationDetails.name}
                </a>
            </Link>
        </div>
    );
};

export default CityLocationsContainer;
