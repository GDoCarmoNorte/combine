import React, { FC } from 'react';
import { ILocationsByCountryModel } from '../../../../@generated/webExpApi';

import styles from './countryLocationsContainer.module.css';
import StateLocationsContainer from './stateLocationsContainer';

export interface ICountryLocationsContainerProps {
    locationDetails: ILocationsByCountryModel;
    selectedStateOrProvinceCode?: string;
}

const CountryLocationsContainer: FC<ICountryLocationsContainerProps> = (props) => {
    const { locationDetails, selectedStateOrProvinceCode } = props;

    return (
        <div className={styles.wrapper}>
            <div className={styles.countryDetailsWrapper}>
                <div className={styles.title}>{locationDetails.countryName}</div>
                <div className={styles.details}>{`${locationDetails.count.toLocaleString()} ${
                    locationDetails.count === 1 ? 'location' : 'locations'
                }`}</div>
            </div>
            {locationDetails.statesOrProvinces &&
                locationDetails.statesOrProvinces.map((stateLocationDetails) => {
                    return (
                        <StateLocationsContainer
                            key={stateLocationDetails.code}
                            stateLocationDetails={stateLocationDetails}
                            countryCode={locationDetails.countryCode}
                            selectedStateOrProvinceCode={selectedStateOrProvinceCode}
                        />
                    );
                })}
        </div>
    );
};

export default CountryLocationsContainer;
