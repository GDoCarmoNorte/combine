import React, { FC } from 'react';
import { INotificationBannerFields } from '../../../../@generated/@types/contentful';
import { ICountryLocationsResponseModel, ILocationsByCountryModel } from '../../../../@generated/webExpApi';
import getBrandInfo from '../../../../lib/brandInfo';
import { IPageSections } from '../../../sections';
import { PageContentWrapper } from '../../../sections/PageContentWrapper';
import CountryLocationsContainer from '../countryLocationsContainer';
import styles from './allLocationsPageLayout.module.css';
import { Entry } from 'contentful';
import NotificationBanner from '../../notificationBanner/notificationBanner';
import Breadcrumbs from '../../../atoms/Breadcrumbs';
import PageMetaTitle from '../../../../components/atoms/pageMetaTitle';

export interface IAllLocationsContainerProps {
    allLocationDetails: ICountryLocationsResponseModel;
    stateOrProvinceCode?: string;
    sections?: IPageSections;
    pageMetaTitle: string;
}

const AllLocationsPageLayout: FC<IAllLocationsContainerProps> = (props) => {
    const { brandName } = getBrandInfo();
    const { allLocationDetails, stateOrProvinceCode, sections, pageMetaTitle } = props;
    const notificationBanner = sections?.find((s) => s.sys.contentType.sys.id === 'notificationBanner');

    const breadcrumbsPaths = [
        {
            title: 'locations',
            href: '/locations',
        },
    ];

    return (
        <PageContentWrapper>
            <h1 className="visually-hidden">{`find an ${brandName}`}</h1>
            {notificationBanner && (
                <NotificationBanner entry={notificationBanner as Entry<INotificationBannerFields>} />
            )}
            <div className={styles.locationsWrapper}>
                <Breadcrumbs paths={breadcrumbsPaths} />
                <div className={styles.title}>{`${allLocationDetails.totalCount.toLocaleString()} locations`}</div>
                {allLocationDetails.locationsByCountry.map((countryLocations: ILocationsByCountryModel) => {
                    return (
                        <CountryLocationsContainer
                            key={countryLocations.countryCode}
                            locationDetails={countryLocations}
                            selectedStateOrProvinceCode={stateOrProvinceCode}
                        />
                    );
                })}
                <PageMetaTitle
                    tag="h2"
                    titleClassName="t-header-h2"
                    containerClassName={styles.titleContainer}
                    text={pageMetaTitle}
                />
            </div>
        </PageContentWrapper>
    );
};

export default AllLocationsPageLayout;
