import { LocationWithDetailsModel } from '../../../../common/services/locationService/types';

export interface ICountryLocations {
    countryCode: string;
    countryName: string;
    locations: LocationWithDetailsModel[];
}
