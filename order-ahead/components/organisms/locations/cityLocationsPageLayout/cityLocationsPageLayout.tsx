import React, { FC, useMemo } from 'react';
import { PageContentWrapper } from '../../../sections/PageContentWrapper';
import CityLocationTile from '../cityLocationTile';
import styles from './cityLocationsPageLayout.module.css';
import { ILocationsByCityModel } from '../../../../@generated/webExpApi';
import classnames from 'classnames';
import { Entry } from 'contentful';
import NotificationBanner from '../../notificationBanner/notificationBanner';
import { IPageSections } from '../../../sections';
import { INotificationBannerFields } from '../../../../@generated/@types/contentful';
import Breadcrumbs from '../../../atoms/Breadcrumbs';
import PageMetaTitle from '../../../../components/atoms/pageMetaTitle';
import { IS_META_IN_H1 } from './constants';

export interface IBreadcrumbsData {
    countryCode: string;
    stateOrProvinceCode: string;
    stateOrProvince: string;
}

export interface ICityLocationsPageLayoutProps {
    cityLocations: ILocationsByCityModel;
    sections?: IPageSections;
    breadcrumbsData: IBreadcrumbsData;
    pageMetaTitle: string;
}

const CityLocationsPageLayout: FC<ICityLocationsPageLayoutProps> = ({
    cityLocations: { name, count, locations },
    sections,
    breadcrumbsData: { countryCode, stateOrProvinceCode, stateOrProvince },
    pageMetaTitle,
}) => {
    const breadcrumbsPaths = useMemo(
        () => [
            {
                title: 'locations',
                href: '/locations',
            },
            {
                title: 'all locations',
                href: '/locations/all',
            },
            {
                title: stateOrProvince,
                href: `/locations/${countryCode}/${stateOrProvinceCode}`,
            },
        ],
        [countryCode, stateOrProvinceCode, stateOrProvince]
    );

    const notificationBanner = sections?.find((s) => s.sys.contentType.sys.id === 'notificationBanner');

    const locationsCountString = `${count} ${name} ${count === 1 ? 'location' : 'locations'}`;
    const h1String = IS_META_IN_H1 ? pageMetaTitle : locationsCountString;
    const h2String = IS_META_IN_H1 ? locationsCountString : pageMetaTitle;

    return (
        <PageContentWrapper>
            {notificationBanner && (
                <NotificationBanner entry={notificationBanner as Entry<INotificationBannerFields>} />
            )}
            <div className={styles.wrapper}>
                <div className={classnames(styles.headerGroup)}>
                    <Breadcrumbs paths={breadcrumbsPaths} />
                    <h1 className={classnames('t-header-h1', styles.title)}>{h1String}</h1>
                </div>
                <div className={styles.tilesWrapper}>
                    {locations.map((location) => {
                        return <CityLocationTile key={location.id} location={location} />;
                    })}
                </div>
                <PageMetaTitle text={h2String} tag="h2" titleClassName="t-header-h2" />
            </div>
        </PageContentWrapper>
    );
};

export default CityLocationsPageLayout;
