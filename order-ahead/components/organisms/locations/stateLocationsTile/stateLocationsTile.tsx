import React, { FC } from 'react';
import { ILocationsByStateOrProvinceModel } from '../../../../@generated/webExpApi';
import CityLocationsContainer from '../countryLocationsContainer/stateLocationsContainer/cityLocationsContainer';

import styles from './stateLocationsTile.module.css';
import classNames from 'classnames';

export interface IStateLocationsContainerProps {
    stateLocationDetails: ILocationsByStateOrProvinceModel;
    countryCode: string;
}

const StateLocationsTile: FC<IStateLocationsContainerProps> = (props) => {
    const { stateLocationDetails, countryCode } = props;

    return (
        <>
            <div className={classNames(styles.citesContainer)}>
                {stateLocationDetails.cities &&
                    stateLocationDetails.cities.map((cityLocationDetails) => {
                        return (
                            <CityLocationsContainer
                                key={cityLocationDetails.displayName}
                                cityLocationDetails={cityLocationDetails}
                                countryCode={countryCode}
                                stateCode={stateLocationDetails.code}
                            />
                        );
                    })}
            </div>
            <h2 className={classNames(styles.header, 't-header-h2')}>
                {`${stateLocationDetails.count.toLocaleString()} ${stateLocationDetails.name} ${
                    stateLocationDetails.count === 1 ? 'location' : 'locations'
                }`}
            </h2>
        </>
    );
};

export default StateLocationsTile;
