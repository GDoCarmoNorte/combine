import React, { useMemo, useCallback, useState } from 'react';

import {
    IDefaultModifier,
    IDisplayFullProduct,
    IDisplayProduct,
    IExtraItemModel,
    ISelectedModifier,
    ProductTypesEnum,
} from '../../../redux/types';
import { useGlobalProps, usePdp } from '../../../redux/hooks';

import ModifierCard from '../../atoms/ModifierCard';
import ModifyProductModal from '../modifyProductModal';
import { useBwwDisplayModifierGroupsForExtraItem } from '../../../redux/hooks/pdp';
import { getFormattedModifications } from '../../../lib/domainProduct';

export interface IModifierSelectedParams {
    id: string;
    quantity: number;
    displayProduct: IDisplayProduct;
}

export interface IExtrasCardContainerProps {
    item: IExtraItemModel;
    sectionIndex: number;
    addedModifiers?: ISelectedModifier[];
    removedDefaultModifiers?: IDefaultModifier[];
}

export function ExtrasCardContainer(props: IExtrasCardContainerProps): JSX.Element {
    const { item, sectionIndex, addedModifiers, removedDefaultModifiers } = props;
    const pdp = usePdp();
    const { setSelectedExtraChild, unselectExtraChild } = pdp.useExtraChild(sectionIndex);
    const { modifierItemsById, productsById } = useGlobalProps();

    const modifierGroups = useBwwDisplayModifierGroupsForExtraItem(item.id, sectionIndex);

    const contentfulProduct = modifierItemsById[item.id] || modifierItemsById['default'];

    const showModify = !!modifierGroups?.length && !!item.quantity;
    const showPrice = !!item.resultPrice;

    const [modalOpened, setModalOpen] = useState<boolean>(false);

    const cardType = useMemo(() => {
        if (item.quantity && item.sizeSelections?.length > 1) {
            return 'size';
        }
        return 'default';
    }, [item]);

    const handleClick = useCallback(() => {
        if (!item.quantity) {
            setSelectedExtraChild(item.id);
        } else {
            unselectExtraChild();
        }
    }, [setSelectedExtraChild, unselectExtraChild, item]);

    const handleOpenModal = useCallback((e) => {
        e.stopPropagation();
        setModalOpen(true);
    }, []);

    const handleCloseModal = (e) => {
        e.stopPropagation();
        setModalOpen(false);
    };

    const handleSetSelectedItem = useCallback(
        (itemId: string, size: string) => {
            setSelectedExtraChild(itemId, size);
        },
        [setSelectedExtraChild]
    );

    const modifiers = useMemo(() => {
        if (addedModifiers && removedDefaultModifiers) {
            return getFormattedModifications(addedModifiers, removedDefaultModifiers);
        }

        return null;
    }, [addedModifiers, removedDefaultModifiers]);

    return (
        <>
            <ModifierCard
                contentfulProduct={contentfulProduct}
                productName={item.name || ''}
                modifierItemId={item.id}
                onClick={handleClick}
                onModifyClick={handleOpenModal}
                onSelectSize={handleSetSelectedItem}
                quantity={item.quantity}
                selected={item.quantity > 0}
                calories={item.resultCalories}
                price={item.resultPrice}
                selections={item.sizeSelections}
                selectionType="single"
                showModify={showModify}
                showPrice={showPrice}
                selectorType={cardType}
                modifiers={modifiers}
            />
            {showModify && (
                <ModifyProductModal
                    contentfulProduct={productsById[item.id]}
                    displayProduct={
                        {
                            productId: item.id,
                            calories: item.resultCalories,
                            price: item.resultPrice,
                            displayName: item.name,
                            productType: ProductTypesEnum.Single,
                        } as IDisplayFullProduct
                    }
                    childProductId={item.id}
                    sectionIndex={sectionIndex}
                    open={modalOpened}
                    onClose={handleCloseModal}
                    modifierItemsById={modifierItemsById}
                    productsById={productsById}
                    modifierGroups={modifierGroups}
                />
            )}
        </>
    );
}
