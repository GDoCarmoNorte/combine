import { IProductFields } from '../../../@generated/@types/contentful';
import { IModifierItemById, IProductItemById } from '../../../common/services/globalContentfulProps';
import { IDisplayFullProduct, IDisplayModifierGroup } from '../../../redux/types';
import { InspireCmsEntry } from '../../../common/types';

export interface IModifyProductModalProps {
    contentfulProduct: InspireCmsEntry<IProductFields>;
    displayProduct: IDisplayFullProduct;
    childProductId?: string;
    open: boolean;
    onClose: (e: React.MouseEvent | React.KeyboardEvent) => void;
    modifierItemsById: IModifierItemById | IProductItemById;
    productsById: IProductItemById;
    sectionIndex: number;
    modifierGroups: IDisplayModifierGroup[];
}
