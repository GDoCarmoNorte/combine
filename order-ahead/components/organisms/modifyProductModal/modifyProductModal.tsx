import React from 'react';
import { useDispatch } from 'react-redux';

import { IModifyProductModalProps } from './types';
import { ModifierGroupController } from '../ModifierGroupController';
import { IDisplayProduct } from '../../../redux/types';
import { GTM_MODIFIER_CUSTOMIZATION, GTM_MODIFY_PRODUCT } from '../../../common/services/gtmService/constants';

import { GtmModifierCustomizationCategory } from '../../../common/services/gtmService/types';
import ModifiersModal from '../../atoms/modifiersModal';
import styles from './modifyProductModal.module.css';

export default function ModifyProductModal(props: IModifyProductModalProps): JSX.Element {
    const {
        contentfulProduct,
        open,
        onClose,
        modifierItemsById,
        productsById,
        childProductId,
        displayProduct,
        sectionIndex,
        modifierGroups,
    } = props;
    const dispatch = useDispatch();

    if (!modifierGroups) {
        return null;
    }

    const productName = displayProduct.displayName || contentfulProduct.fields.name;

    const handleSaveModificationsClick = (e) => {
        dispatch({ type: GTM_MODIFY_PRODUCT });
        onClose(e);
    };

    const dispatchModifierCustomization = (modifier: IDisplayProduct, newQuantity: number, prevQuantity: number) => {
        if (newQuantity === prevQuantity) return;

        const category =
            newQuantity < prevQuantity
                ? GtmModifierCustomizationCategory.removal
                : GtmModifierCustomizationCategory.addition;

        dispatch({
            type: GTM_MODIFIER_CUSTOMIZATION,
            payload: {
                category,
                productName,
                modifierName: modifier.displayName,
            },
        });
    };

    return (
        <ModifiersModal
            isOpen={open}
            onClose={handleSaveModificationsClick}
            subtitle={
                displayProduct?.mainProductSectionName ? `modify ${displayProduct.mainProductSectionName}` : 'modify'
            }
            title={productName}
        >
            <div className={styles.content}>
                {modifierGroups.map((mg) => {
                    return (
                        <ModifierGroupController
                            displayModifierGroup={mg}
                            key={mg.displayName}
                            modifierItemsById={modifierItemsById}
                            productsById={productsById}
                            childProductId={childProductId}
                            onModifierQuantityChange={dispatchModifierCustomization}
                            sectionIndex={sectionIndex}
                        />
                    );
                })}
            </div>
        </ModifiersModal>
    );
}
