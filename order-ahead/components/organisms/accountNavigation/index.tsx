import React, { useEffect, useState } from 'react';
import styles from './index.module.css';

import Link from 'next/link';
import classNames from 'classnames';
import LogoutButton from '../../atoms/logoutButton';
import Counter from '../../atoms/counter';

interface AccountMenu {
    links: NavItem[];
    currentPath: string;
}

interface NavItem {
    name: string;
    link: string;
    exact?: boolean;
    count?: number;
    trackingId?: string;
}

export default function AccountNavigation(props: AccountMenu): JSX.Element {
    const [scrollContainer, setScrollContainer] = useState(null);
    const [scrollTarget, setScrollTarget] = useState(null);

    const containerRefCallback = (node) => setScrollContainer(node);
    const targetRefCallback = (node) => setScrollTarget(node);
    const CONTAINER_PADDING = 20;

    // useLayoutEffect doesn't work properly - element's sizes are not correct. Probably due to styles are not appied yet.
    useEffect(() => {
        if (!scrollContainer || !scrollTarget) {
            return;
        }
        const { width: containerWidth } = scrollContainer.getBoundingClientRect();
        const { right: targetRight } = scrollTarget.getBoundingClientRect();

        const scrollLeft = targetRight - containerWidth + CONTAINER_PADDING;
        if (scrollLeft > 0) {
            scrollContainer.scrollLeft = scrollLeft;
        }
    }, [scrollContainer, scrollTarget]);

    const renderNavItem = (navItem, index) => {
        const isExactPath = props.currentPath === navItem.link;

        const isDealPage = !navItem.exact && new RegExp(navItem.link).test(props.currentPath);

        return (
            <div key={index} className={styles.linkContainer}>
                {isExactPath && (
                    <span className={classNames('t-subheader-small', styles.linkTextSelected)} ref={targetRefCallback}>
                        {navItem.name.toUpperCase()}
                    </span>
                )}
                {((!isExactPath && navItem.exact) || (!isExactPath && !navItem.exact && !isDealPage)) && (
                    <Link href={navItem.link}>
                        <a
                            {...(navItem.trackingId && { 'data-gtm-id': navItem.trackingId })}
                            className={classNames('t-subheader-small', styles.linkText)}
                        >
                            {navItem.name.toUpperCase()}
                        </a>
                    </Link>
                )}
                {!isExactPath && !navItem.exact && isDealPage && (
                    <Link href={navItem.link}>
                        <a className={classNames('t-subheader-small', styles.linkTextSelected)}>
                            {navItem.name.toUpperCase()}
                        </a>
                    </Link>
                )}
                {!!navItem.count && (
                    <Counter
                        value={navItem.count}
                        counterClassName={styles.counter}
                        ariaLabel={`${navItem.name.toLowerCase} counter`}
                    />
                )}
            </div>
        );
    };
    return (
        <div className={styles.menuContainer} ref={containerRefCallback}>
            {props.links.map(renderNavItem)}
            <div className={styles.linkContainer}>
                <LogoutButton className={classNames('t-paragraph', styles.logoutButton)} />
            </div>
        </div>
    );
}
