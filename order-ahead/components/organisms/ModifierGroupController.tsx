import React from 'react';
import { ModifierCardContainer } from './modifierCardContainer/modifierCardContainer';
import { IDisplayModifierGroup, IDisplayProduct, ModifierCardSelectorType } from '../../redux/types';
import styles from './modifierGroupController.module.css';
import { IModifierItemById, IProductItemById } from '../../common/services/globalContentfulProps';
import classNames from 'classnames';
import useCardClick from '../../common/hooks/useModifierCardClick';

export interface IModifierGroupControllerProps {
    className?: string;
    modifierItemsById: IModifierItemById | IProductItemById;
    productsById: IProductItemById;
    childProductId?: string;
    displayModifierGroup?: IDisplayModifierGroup;
    onModifierQuantityChange?: (modifier: IDisplayProduct, newQuantity: number, prevQuantity: number) => void;
    sectionIndex: number;
}

export function ModifierGroupController(props: IModifierGroupControllerProps): JSX.Element | null {
    const {
        className,
        modifierItemsById,
        productsById,
        childProductId,
        displayModifierGroup,
        onModifierQuantityChange,
        sectionIndex,
    } = props;
    const { modifiers, maxQuantity, displayName } = displayModifierGroup;
    const type: ModifierCardSelectorType = maxQuantity > 1 ? 'quantity' : 'default';

    const onCardClick = useCardClick({
        modifierGroup: displayModifierGroup,
        childProductId,
        sectionIndex,
        onModifierQuantityChange,
    });

    return (
        <div>
            <div className={styles.modifierGroupHeader}>
                <h3 className={classNames(styles.modifierHeader, 't-header-h3')}>
                    {displayModifierGroup?.displayName}
                </h3>
            </div>
            <div className={`${styles.modifierGroupContainer} ${className}`} role="list">
                {modifiers.map((displayProduct, i) => {
                    const { displayProductDetails } = displayProduct;

                    return (
                        <div
                            key={i}
                            className={styles.modifierCard}
                            role="listitem"
                            aria-label={displayProduct?.displayName || ''}
                        >
                            <ModifierCardContainer
                                categoryName={displayName}
                                modifierItemsById={modifierItemsById}
                                productsById={productsById}
                                selectorType={displayProductDetails.selections ? 'size' : type}
                                displayProduct={displayProduct}
                                canIncrement={displayProductDetails.quantity < displayProductDetails.maxQuantity}
                                canDecrement={displayProductDetails.quantity > displayProductDetails.minQuantity}
                                onChange={onCardClick}
                                sectionIndex={sectionIndex}
                                selectionType="multi"
                            />
                        </div>
                    );
                })}
            </div>
        </div>
    );
}
