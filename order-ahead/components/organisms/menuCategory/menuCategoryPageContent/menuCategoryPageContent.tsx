import React, { useEffect, FC } from 'react';
import { useDispatch } from 'react-redux';
import Head from 'next/head';
import { Entry, EntryCollection } from 'contentful';
import { ICaloriesLegal, IMenuCategory, IMenuSubcategoryFields } from '../../../../@generated/@types/contentful';
import VerticalProductCard from '../../../../components/atoms/verticalProductCard';
import SectionHeader from '../../../../components/atoms/sectionHeader';
import CategorySelect from '../../../../components/atoms/categorySelect';
import styles from '../../../../pages/menu/[menuCategoryUrl]/index.module.css';
import { GTM_MENU_IMPRESSION } from '../../../../common/services/gtmService/constants';
import Breadcrumbs from '../../../../components/atoms/Breadcrumbs';
import useGtmImpression from '../../../../common/hooks/useGtmImpression';
import getBrandInfo from '../../../../lib/brandInfo';
import PageSections from '../../../../components/sections';
import { PageContentWrapper } from '../../../../components/sections/PageContentWrapper';
import schemaPdpAndPlp from '../../../../common/helpers/schemaPdpAndPlp';
import {
    useDomainMenuCategories,
    useDomainProductByContentfulFields,
    useNutrition,
} from '../../../../redux/hooks/domainMenu';
import { useDomainMenu } from '../../../../redux/hooks';
import SodiumWarning from '../../../../components/organisms/sodiumWarning';
import { Divider } from '../../../../components/atoms/divider';
import { useMenuCategoryHasSodiumWarning } from '../../../../common/hooks/useMenuCategoryHasSodiumWarning';
import { useSodiumWarning } from '../../../../common/hooks/useSodiumWarning';
import isTapListMenuCategory from '../../../../common/helpers/isTapListMenuCategory';
import BackToTop from '../../../atoms/BackToTop';
import useMenuCategoryProductsFilter from '../../../../common/hooks/useMenuCategoryProductsFilter';
import CaloriesLegalSection from '../../caloriesLegalSection/caloriesLegelSection';
import BrandLoader from '../../../atoms/BrandLoader';
import {
    getContentfulMenuCategoryIdsByFields,
    getDomainMenuCategoryIdByIds,
} from '../../../../common/helpers/menuCategoryHelper';

interface IMenuCategoryPageContentProps {
    menuCategory: IMenuCategory;
    menuCategories: IMenuCategory[];
    subcategorys: EntryCollection<IMenuSubcategoryFields>;
    caloriesLegal?: ICaloriesLegal;
    areSectionsloading?: boolean;
}

function getFullSubcategory(
    sysId: string,
    subcategories: EntryCollection<IMenuSubcategoryFields>
): Entry<IMenuSubcategoryFields> | undefined {
    return subcategories?.items.find((subcategory) => subcategory.sys.id === sysId);
}

const MenuCategoryPageContent: FC<IMenuCategoryPageContentProps> = (props) => {
    const {
        actions: { getAvailableCategories },
    } = useDomainMenu();
    const domainMenuCategories = useDomainMenuCategories();
    const brandInfo = getBrandInfo();
    const { menuCategory, menuCategories: cmsCategories, areSectionsloading, caloriesLegal } = props;

    const productsFilter = useMenuCategoryProductsFilter();

    const availableCategories = getAvailableCategories(cmsCategories);

    const categoryIsAvailable = availableCategories.some(
        (category) =>
            !isTapListMenuCategory(category) &&
            category.fields.link.fields.nameInUrl === menuCategory.fields.link.fields.nameInUrl
    );

    const impression = useGtmImpression(menuCategory);
    const dispatch = useDispatch();
    const defaultProduct = menuCategory?.fields.products?.[0];
    const productId = useDomainProductByContentfulFields(defaultProduct.fields)?.id;
    const nutrition = useNutrition(productId);
    const dataSchema = schemaPdpAndPlp(menuCategory, brandInfo, defaultProduct, nutrition);

    const sodiumWarning = useSodiumWarning();
    const isMenuCategoryHasSodiumWarning = useMenuCategoryHasSodiumWarning(menuCategory);
    const showSodiumWarning = isMenuCategoryHasSodiumWarning && !!sodiumWarning;

    useEffect(() => {
        if (impression && categoryIsAvailable) {
            dispatch({
                type: GTM_MENU_IMPRESSION,
                payload: {
                    products: impression,
                    category: menuCategory.fields.categoryName,
                },
            });
        }
    }, [impression, categoryIsAvailable, dispatch, menuCategory.fields.categoryName]);
    if (!categoryIsAvailable) return null;

    const {
        sections,
        link: {
            fields: { nameInUrl },
        },
    } = menuCategory.fields;

    const categoryId = getDomainMenuCategoryIdByIds(
        getContentfulMenuCategoryIdsByFields(menuCategory.fields),
        domainMenuCategories
    );

    return (
        <div>
            <Head>
                <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(dataSchema) }} />
            </Head>
            <div className={styles.container}>
                <Breadcrumbs />
                <PageContentWrapper>
                    <CategorySelect categories={availableCategories} currentCategoryUrl={nameInUrl} />

                    {/* No Subcategory Products */}
                    <div className={styles.sectionItemsContainer}>
                        {menuCategory.fields.products?.filter(productsFilter).map((product) => (
                            <VerticalProductCard
                                key={product.sys.id}
                                category={nameInUrl}
                                item={product}
                                isSideScroll={false}
                                sodiumWarning={sodiumWarning}
                                categoryId={categoryId}
                            />
                        ))}
                    </div>

                    {/* Subcategory Products */}
                    <div>
                        {menuCategory.fields.subcategories &&
                            menuCategory.fields.subcategories.map((subcategory) => {
                                const currentSubcategory = getFullSubcategory(subcategory.sys.id, props.subcategorys);
                                if (!currentSubcategory) return null;
                                return (
                                    <div key={subcategory.sys.id}>
                                        <SectionHeader text={currentSubcategory.fields.name} />

                                        <div className={styles.sectionItemsContainer}>
                                            {currentSubcategory.fields.products
                                                ?.filter(productsFilter)
                                                .map((product) => (
                                                    <VerticalProductCard
                                                        key={product.sys.id}
                                                        category={nameInUrl}
                                                        item={product}
                                                        isSideScroll={false}
                                                        sodiumWarning={sodiumWarning}
                                                    />
                                                ))}
                                        </div>
                                    </div>
                                );
                            })}
                    </div>
                </PageContentWrapper>
            </div>
            {areSectionsloading && <BrandLoader className={styles.loader} />}
            <BackToTop />
            {caloriesLegal && (
                <div className={styles.caloriesWrapper}>
                    <CaloriesLegalSection prop={caloriesLegal}></CaloriesLegalSection>
                </div>
            )}
            {!areSectionsloading && sections?.length > 0 && <PageSections pageSections={sections} />}
            {showSodiumWarning && (
                <div className={styles.sodiumWrapper}>
                    <Divider orientation="horizontal" className={styles.sodiumDivider} />
                    <SodiumWarning sodiumWarning={sodiumWarning} />
                </div>
            )}
        </div>
    );
};

export default MenuCategoryPageContent;
