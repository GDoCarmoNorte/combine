import React, { FC } from 'react';
import { IMenuCategory, ITapListMenuCategory } from '../../../../@generated/@types/contentful';
import CategorySelect from '../../../../components/atoms/categorySelect';
import styles from '../../../../pages/menu/[menuCategoryUrl]/index.module.css';
import Breadcrumbs from '../../../../components/atoms/Breadcrumbs';
import PageSections from '../../../../components/sections';
import { PageContentWrapper } from '../../../../components/sections/PageContentWrapper';
import { useDomainMenu } from '../../../../redux/hooks';
import BrandLoader from '../../../atoms/BrandLoader';

interface ITapListMenuCategoryPageContentProps {
    tapListMenuCategory: ITapListMenuCategory;
    menuCategories: IMenuCategory[];
    areSectionsloading?: boolean;
}

const TapListMenuCategoryPageContent: FC<ITapListMenuCategoryPageContentProps> = ({
    tapListMenuCategory,
    menuCategories,
    areSectionsloading,
}) => {
    const {
        fields: {
            sections,
            link: {
                fields: { nameInUrl },
            },
        },
    } = tapListMenuCategory;

    const {
        actions: { getAvailableCategories },
    } = useDomainMenu();

    const availableCategories = getAvailableCategories(menuCategories);

    return (
        <PageContentWrapper>
            <div className={styles.container}>
                <Breadcrumbs />
                <CategorySelect categories={availableCategories} currentCategoryUrl={nameInUrl} />
            </div>
            {areSectionsloading && <BrandLoader className={styles.loader} />}
            {!areSectionsloading && sections?.length > 0 && <PageSections pageSections={sections} />}
        </PageContentWrapper>
    );
};

export default TapListMenuCategoryPageContent;
