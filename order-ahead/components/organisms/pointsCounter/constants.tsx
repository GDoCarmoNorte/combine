import React from 'react';

export const CHECKIN_SUCCESS_TEXT = (
    <>
        Your Check-in is currently PENDING
        <br />
        Points will be deposited within 24 hours
    </>
);

export const CHECKIN_LOCATION_SERVICES_DISABLED_TEXT = (
    <>
        Location services are disabled for your device.
        <br />
        Please go to your settings and enable location service access to use this feature.
    </>
);
export const CHECKIN_NO_LOCATIONS_AROUND_TEXT = (
    <>
        Unable to check-in at this location.
        <br />
        Please try again later.
    </>
);

export const CHECKIN_ERROR_TEXT = (
    <>
        Unable to check-in at this time.
        <br />
        Please try again later.
    </>
);
