import React from 'react';
import styles from './pointsCounter.module.css';
import { InspireButton } from '../../atoms/button';
import BrandLoader from '../../atoms/BrandLoader';
import { useRouter } from 'next/router';
import classnames from 'classnames';
import getBrandInfo from '../../../lib/brandInfo';
import { format } from '../../../common/helpers/dateTime';
import { formatNumber } from '../../../lib/domainProduct';
import useLoyalty from '../../../redux/hooks/useLoyalty';
import { useCheckin } from '../../../common/hooks/useCheckin';
import { CheckinResult } from './checkinResult';
import InspireTooltip from '../../atoms/Tooltip/tooltip';
import BrandIcon from '../../atoms/BrandIcon';

const PointsCounter = (): JSX.Element => {
    const { brandId } = getBrandInfo();
    const brandIdLowercase = brandId.toLowerCase();
    const { loyalty, isLoading } = useLoyalty();
    const { pointsExpiringDate, pointsBalance } = loyalty;
    const router = useRouter();
    const { checkin, isShowTooltip, ...checkinData } = useCheckin();

    const onFindRewardsClicked = (pointsBalance: number) => {
        return pointsBalance ? router.push('/rewards') : router.push('/menu');
    };

    const buttonText = !pointsBalance ? 'order now' : 'find rewards';

    if (isLoading) {
        return <BrandLoader className={styles.counterLoader} />;
    }

    const descriptionWithoutPoints =
        'Earn points by ordering online, scanning receipts, customizing your profile, and checking-in at your local restaurant. Use ‘em towards food redemption and more.';
    const descriptionWithPoints =
        'You earned points, now use ‘em towards food and more. Find rewards to see what items you can get for free. Check-in to keep on earning.';

    return (
        <div className={styles.counterContainer} aria-label="Points Counter">
            <InspireTooltip
                open={isShowTooltip}
                placement="bottom"
                tooltipClassName={styles.checkinTooltip}
                disableFocusListener
                disableHoverListener
                disableTouchListener
                theme="dark"
                title={
                    <div className={'t-paragraph-strong'}>
                        <BrandIcon icon="action-check" /> You’ve earned 10 points
                    </div>
                }
                PopperProps={{
                    disablePortal: true,
                    modifiers: {
                        flip: {
                            enabled: false,
                        },
                        preventOverflow: {
                            enabled: false,
                        },
                    },
                }}
            >
                <div className={styles.imageContainer}>
                    <img src={`/brands/${brandIdLowercase}/Rewards_Logo.svg`} alt="" />
                </div>
            </InspireTooltip>

            <div className={styles.pointsContainer}>
                <div
                    className={classnames(styles.pointsRow, {
                        [styles.emptyPoints]: !pointsBalance,
                    })}
                >
                    <span className={styles.pointsAmount}>{pointsBalance ? formatNumber(pointsBalance) : 0}</span>
                    <div className={styles.pointsTextColumn}>
                        <span className={styles.pointsTitle}>points</span>
                        <span className={styles.pointsInMyAccount}>in my account</span>
                    </div>
                </div>
                <div className={styles.pointsDescriptionContainer}>
                    {pointsExpiringDate && (
                        <span className={classnames('t-paragraph-small', styles.pointsExpirationDate)}>
                            Points expire: {format(new Date(pointsExpiringDate), 'MM/dd/yyyy')}
                        </span>
                    )}
                    <p className={classnames('t-paragraph-small', styles.pointsDescription)}>
                        {pointsBalance ? descriptionWithPoints : descriptionWithoutPoints}
                    </p>
                </div>
                <div className={styles.pointsButtonsRow}>
                    <InspireButton
                        text={buttonText}
                        className={styles.findRewardsButton}
                        onClick={() => onFindRewardsClicked(pointsBalance)}
                    />
                    <InspireButton text="check-in" className={styles.checkInBtn} onClick={checkin} />
                </div>
                <CheckinResult {...checkinData} />
            </div>
        </div>
    );
};

export default PointsCounter;
