import classNames from 'classnames';
import React, { FC } from 'react';
import { ICheckinModel, TErrorCodeModel } from '../../../@generated/webExpApi';
import { LOCATION_SERVICES_DISABLED_ERROR_CODE } from '../../../common/helpers/getCoordinates';
import { ICheckinError } from '../../../common/hooks/useCheckin';
import BrandIcon from '../../atoms/BrandIcon';
import BrandLoader from '../../atoms/BrandLoader';
import {
    CHECKIN_ERROR_TEXT,
    CHECKIN_LOCATION_SERVICES_DISABLED_TEXT,
    CHECKIN_NO_LOCATIONS_AROUND_TEXT,
    CHECKIN_SUCCESS_TEXT,
} from './constants';
import styles from './pointsCounter.module.css';

const formatCheckinErrorMessage = (error: ICheckinError) => {
    const errorMap = {
        [LOCATION_SERVICES_DISABLED_ERROR_CODE]: CHECKIN_LOCATION_SERVICES_DISABLED_TEXT,
        [TErrorCodeModel.NoLocationsAround]: CHECKIN_NO_LOCATIONS_AROUND_TEXT,
    };

    return errorMap[error.code] || CHECKIN_ERROR_TEXT;
};

interface ICheckinResultProps {
    loading: boolean;
    error: ICheckinError;
    payload: ICheckinModel;
}

export const CheckinResult: FC<ICheckinResultProps> = ({ loading, error, payload }) => {
    const render = () => {
        if (loading) {
            return (
                <div className={classNames(styles.checkinContainer, styles.checkinLoader)}>
                    <BrandLoader />
                </div>
            );
        }

        if (error || payload) {
            return (
                <div className={styles.checkinContainer}>
                    <div className={styles.checkinIcon}>
                        {error ? (
                            <BrandIcon icon="info-error" className={styles.checkinErrorIcon} />
                        ) : (
                            <BrandIcon icon="action-calendar" className={styles.checkinCalendarIcon} />
                        )}
                    </div>
                    <p className="t-paragraph">{payload ? CHECKIN_SUCCESS_TEXT : formatCheckinErrorMessage(error)}</p>
                </div>
            );
        }

        return <></>;
    };

    return render();
};
