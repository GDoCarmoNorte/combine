import React, { FC } from 'react';
import Accordion from '../../atoms/accordion';
import styles from './wingTypeSelection.module.css';
import classnames from 'classnames';
import { useBwwDisplayModifierGroup } from '../../../redux/hooks/pdp';
import { usePdp } from '../../../redux/hooks';
import { formatPrice } from '../../../lib/domainProduct';

const MIXED = 'Mixed';

interface WingTypeSelectionProps {
    productGroupId: string;
}

export const WingTypeSelection: FC<WingTypeSelectionProps> = ({ productGroupId }) => {
    const modifierGroup = useBwwDisplayModifierGroup(productGroupId);
    const modifiers = modifierGroup?.modifiers || [];
    const {
        actions: { onModifierChange },
    } = usePdp();

    const isShow = modifiers.length > 0;
    const selected = modifiers.find((item) => item.displayProductDetails.quantity > 0);
    const subTitle = selected
        ? `${selected.displayName} (+${formatPrice(selected.displayProductDetails.price)})`
        : MIXED;

    const handleModifierClick = (id: string) => {
        onModifierChange({
            modifierGroupId: productGroupId,
            modifierId: id,
            quantity: 1,
        });
    };

    return (
        isShow && (
            <Accordion
                renderHeader={(isExpanded, renderButton, onClickHandler) => (
                    <div
                        className={classnames(styles.accordionHeader, { [styles.headerExpanded]: isExpanded })}
                        onClick={onClickHandler}
                    >
                        <div>
                            <span className={'t-header-h3'}>TYPE</span>
                            <span className={classnames('t-paragraph-hint', styles.subTitle)}>{subTitle}</span>
                        </div>
                        {renderButton()}
                    </div>
                )}
                renderContent={(isExpanded) => (
                    <div
                        role="radiogroup"
                        aria-label="Select option"
                        className={classnames(styles.buttons, {
                            [styles.expanded]: isExpanded,
                            [styles.collapsed]: !isExpanded,
                        })}
                    >
                        <button
                            role="radio"
                            aria-checked={!selected}
                            onClick={() => handleModifierClick('')}
                            className={classnames('t-subheader-small', {
                                [styles.selected]: !selected,
                            })}
                        >
                            {MIXED}
                        </button>

                        {modifiers.map((item, index) => {
                            const isSelected = item === selected;
                            const { displayName, displayProductDetails } = item;
                            return (
                                <button
                                    role="radio"
                                    aria-checked={isSelected}
                                    onClick={() => handleModifierClick(displayProductDetails.productId)}
                                    key={index}
                                    className={classnames('t-subheader-small', {
                                        [styles.selected]: isSelected,
                                    })}
                                >
                                    <div>
                                        <div>{displayName}</div>
                                        <div className={classnames('t-paragraph-hint', styles.price)}>
                                            {formatPrice(displayProductDetails.price)}
                                        </div>
                                    </div>
                                </button>
                            );
                        })}
                    </div>
                )}
                containerClassName={styles.wrapper}
                isExpandedByDefault={false}
                title={'Wings Type'}
            ></Accordion>
        )
    );
};
