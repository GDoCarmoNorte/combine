import React, { FC } from 'react';
import classNames from 'classnames';
import { IDisplayModifierGroup, IDisplayProduct } from '../../../redux/types';
import ModifierCard from '../../atoms/ModifierCard';
import ModifiersModal from '../../atoms/modifiersModal';
import { BwwModifierCardContainer } from '../modifierCardContainer/bwwModifierCardContainer';
import ModalFooterContent from '../modifiersSelection/modalFooterContent';

import styles from '../modifiersSelection/allModifiersModal.module.css';

interface INestedModifiersModalProps {
    isOpen?: boolean;
    onClose: (e: React.MouseEvent<HTMLButtonElement>) => void;
    modifier: IDisplayProduct;
    modifierGroups: IDisplayModifierGroup[];
    modifierGroupId: string;
    requireSelectionModifiersGroups?: string[];
}

const NestedModifiersModal: FC<INestedModifiersModalProps> = (props) => {
    const {
        isOpen = false,
        onClose,
        modifier: parentModifier,
        modifierGroups,
        modifierGroupId,
        requireSelectionModifiersGroups = [],
    } = props;

    return (
        <ModifiersModal
            isOpen={isOpen}
            subtitle="Modify"
            title={parentModifier.displayName}
            onClose={onClose}
            footerContent={
                <ModalFooterContent
                    onSaveAndCloseClick={onClose}
                    isSaveAndCloseDisabled={!!requireSelectionModifiersGroups.length}
                />
            }
        >
            <div className={styles.modalContent}>
                {modifierGroups.map((group) => {
                    return (
                        <div className={styles.groupWrapper} key={group.modifierGroupId}>
                            <div className={styles.titleWrapper}>
                                <h3
                                    aria-label={`${group.displayName} category`}
                                    className={classNames(styles.title, 't-header-h3')}
                                >
                                    {group.displayName}
                                </h3>
                            </div>
                            <div className={styles.group}>
                                <div>
                                    {group.modifiers.map((modifier) => {
                                        const selectionType = group.maxQuantity > 1 ? 'multi' : 'single';

                                        return (
                                            <BwwModifierCardContainer
                                                key={modifier.displayProductDetails.productId}
                                                modifierGroupId={group.modifierGroupId}
                                                selectionType={selectionType}
                                                displayProduct={modifier}
                                                parentModifierId={parentModifier.displayProductDetails.productId}
                                                parentModifierGroupId={modifierGroupId}
                                            >
                                                {({
                                                    contentfulProduct,
                                                    selectorType,
                                                    showModify,
                                                    onClick,
                                                    onPlus,
                                                    onMinus,
                                                    isSelected,
                                                    isDisabled,
                                                    canDecrement,
                                                    canIncrement,
                                                    productName,
                                                    maxQuantityTooltipText,
                                                    showPrice,
                                                }) => {
                                                    return (
                                                        <div className={styles.modifierCard}>
                                                            <ModifierCard
                                                                canDecrement={canDecrement}
                                                                canIncrement={canIncrement}
                                                                contentfulProduct={contentfulProduct}
                                                                productName={productName}
                                                                modifierItemId={
                                                                    modifier.displayProductDetails.productId
                                                                }
                                                                onCardMinus={onMinus}
                                                                onCardPlus={onPlus}
                                                                onClick={onClick}
                                                                quantity={modifier.displayProductDetails.quantity}
                                                                calories={modifier.displayProductDetails.calories}
                                                                price={modifier.displayProductDetails.price}
                                                                selectionType={selectionType}
                                                                showModify={showModify}
                                                                selectorType={selectorType}
                                                                selected={isSelected}
                                                                disabled={isDisabled}
                                                                maxQuantityTooltipText={maxQuantityTooltipText}
                                                                showPrice={showPrice}
                                                            />
                                                        </div>
                                                    );
                                                }}
                                            </BwwModifierCardContainer>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </ModifiersModal>
    );
};

export default NestedModifiersModal;
