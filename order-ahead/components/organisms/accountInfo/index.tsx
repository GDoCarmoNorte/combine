import React from 'react';
import { Formik, Form } from 'formik';
import classnames from 'classnames';
import { conformToMask } from 'react-text-mask';

import { InspireButton } from '../../atoms/button';
import styles from './index.module.css';
import InspireTooltip from '../../atoms/Tooltip';
import Icon from '../../atoms/BrandIcon';
import PhoneInput from './components/phoneInput';
import EmailInput from './components/emailInput';
import NameInput from './components/nameInput';
import BirthDayInput from './components/birthDayInput';

interface IFormData {
    firstName: string;
    lastName: string;
    phone?: string;
}
interface IAccountInfo {
    firstName: string;
    lastName: string;
    phone?: string;
    email: string;
    birthDate?: string;
    pending: boolean;
    onSubmit: (data: IFormData) => void;
    onChangePasswordClick: () => void;
    tooltipIsOpen: boolean;
}

export const phoneNumberMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

const SuccessfulMessage = () => {
    return (
        <>
            <Icon icon="action-check" size="xs" className={styles.checkIcon} />
            <span> Your information has been updated</span>
        </>
    );
};

const AccountInfo = (props: IAccountInfo): JSX.Element => {
    const {
        onSubmit,
        onChangePasswordClick,
        pending,
        phone = '',
        birthDate = '',
        tooltipIsOpen,
        firstName = '',
        lastName = '',
        email = '',
    } = props;

    const { conformedValue } = conformToMask(phone, phoneNumberMask, { guide: true });

    const initial = {
        firstName,
        lastName,
        email,
        phone: conformedValue,
        birthDate,
    };

    const isBirthDateDisabled = !!birthDate;

    return (
        <section className={styles.container}>
            <div className={styles.wrapper}>
                <div className={classnames(styles.container)}>
                    <Formik onSubmit={onSubmit} initialValues={initial} validateOnMount enableReinitialize>
                        {({ isValid, values, initialValues }) => (
                            <Form className={styles.formContainer}>
                                <h2 className={classnames(styles.title, 't-header-h2')}>{'account info'}</h2>
                                <div className={styles.formSection}>
                                    <NameInput
                                        placeHolder="Enter First Name"
                                        label="First Name"
                                        maxLength={256}
                                        type="text"
                                        name="firstName"
                                    />
                                    <NameInput
                                        placeHolder="Enter Last Name"
                                        label="Last Name"
                                        maxLength={256}
                                        type="text"
                                        name="lastName"
                                    />
                                </div>
                                <div className={styles.formSection}>
                                    <PhoneInput />
                                    <EmailInput />
                                </div>
                                <div className={styles.formSection}>
                                    <BirthDayInput disabled={isBirthDateDisabled} />
                                </div>
                                <div className={styles.formSection}>
                                    <a
                                        className={classnames('link-secondary-active', styles.changePasswordLink)}
                                        onClick={onChangePasswordClick}
                                        aria-label="Change Password"
                                    >
                                        change password
                                    </a>
                                </div>
                                <div className={styles.buttonContainer}>
                                    <InspireTooltip
                                        open={tooltipIsOpen}
                                        placement="top"
                                        disableFocusListener
                                        disableHoverListener
                                        disableTouchListener
                                        theme="dark"
                                        title={<SuccessfulMessage />}
                                        tooltipClassName={styles.tooltip}
                                    >
                                        <div className={styles.submitButton}>
                                            <InspireButton
                                                submit
                                                disabled={
                                                    !isValid ||
                                                    pending ||
                                                    JSON.stringify(values) === JSON.stringify(initialValues)
                                                }
                                                text="save"
                                                fullWidth
                                            />
                                        </div>
                                    </InspireTooltip>
                                </div>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </section>
    );
};

export default AccountInfo;
