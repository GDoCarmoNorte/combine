import React, { useMemo } from 'react';
import styles from '../index.module.css';
import { validateRequireDateOfBirth } from '../../../../common/helpers/complexValidateHelper';
import { autoCorrectedDatePipe } from '../../../../common/helpers/autoCorrectedDatePipe';
import classnames from 'classnames';
import FormikInput from '../../formikInput';
import { WITH_YEAR, BIRTHDAY_INPUT_LABEL } from '../accountInfo.constants';

interface IBirthDayInput {
    disabled?: boolean;
}

const BirthDayInput = (props: IBirthDayInput): JSX.Element => {
    const { disabled } = props;
    const testDateOfBirth = useMemo(() => validateRequireDateOfBirth('Date Of Birth', {}, WITH_YEAR), []);
    const inputMask = WITH_YEAR
        ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
        : [/\d/, /\d/, '/', /\d/, /\d/];
    return (
        <FormikInput
            name="birthDate"
            label={`${BIRTHDAY_INPUT_LABEL}`}
            type="text"
            placeholder={WITH_YEAR ? 'MM/DD/YYYY' : 'MM/DD'}
            mask={inputMask}
            pipe={autoCorrectedDatePipe}
            className={styles.inputContainer}
            inputClassName={classnames(styles.input, {
                [styles.inputDisabled]: disabled,
            })}
            errorClassName={styles.inputError}
            labelClassName={styles.inputLabel}
            disabled={!!disabled}
            validate={testDateOfBirth}
            required
        />
    );
};

export default BirthDayInput;
