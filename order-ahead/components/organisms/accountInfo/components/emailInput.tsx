import React from 'react';
import FormikInput from '../../formikInput';
import styles from '../index.module.css';
import classnames from 'classnames';

const EmailInput = (): JSX.Element => {
    return (
        <FormikInput
            name="email"
            label="Email *"
            type="email"
            placeholder="Enter Email"
            className={styles.inputContainer}
            inputClassName={classnames(styles.input, styles.inputDisabled)}
            errorClassName={styles.inputError}
            labelClassName={styles.inputLabel}
            maxLength={256}
            disabled
        />
    );
};

export default EmailInput;
