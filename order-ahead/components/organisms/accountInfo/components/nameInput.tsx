import React, { useMemo } from 'react';
import styles from '../index.module.css';
import FormikInput from '../../formikInput';
import { validateAlpha } from '../../../../common/helpers/complexValidateHelper';

interface INameInput {
    type: string;
    placeHolder: string;
    label: string;
    maxLength: number;
    name: string;
}

const NameInput = (props: INameInput): JSX.Element => {
    const { type, placeHolder, label, maxLength, name } = props;
    const testNameInput = useMemo(() => validateAlpha(label, maxLength), [label, maxLength]);
    return (
        <FormikInput
            name={name}
            label={label}
            type={type}
            placeholder={placeHolder}
            className={styles.inputContainer}
            inputClassName={styles.input}
            errorClassName={styles.inputError}
            labelClassName={styles.inputLabel}
            maxLength={maxLength}
            validate={testNameInput}
            required
        />
    );
};

export default NameInput;
