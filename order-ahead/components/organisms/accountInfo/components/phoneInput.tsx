import React, { useMemo } from 'react';
import styles from '../index.module.css';
import { phoneNumberMask } from '../index';
import { validatePhoneWithMask } from '../../../../common/helpers/complexValidateHelper';
import FormikInput from '../../formikInput';
import { PHONE_INPUT_LABEL } from '../accountInfo.constants';

const PhoneInput = (): JSX.Element => {
    const testPhoneField = useMemo(
        () =>
            validatePhoneWithMask(PHONE_INPUT_LABEL, {
                require: 'Phone number is incomplete',
                invalid: 'Incorrect phone number',
            }),
        []
    );

    return (
        <FormikInput
            name="phone"
            label={PHONE_INPUT_LABEL}
            type="tel"
            placeholder="(   )   -     "
            mask={phoneNumberMask}
            className={styles.inputContainer}
            inputClassName={styles.input}
            errorClassName={styles.inputError}
            labelClassName={styles.inputLabel}
            validate={testPhoneField}
            required
        />
    );
};

export default PhoneInput;
