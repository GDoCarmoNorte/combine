import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import classNames from 'classnames';
import { styled } from '@material-ui/styles';

import styles from './alertBanners.module.css';
import {
    IDocumentLink,
    IExternalLink,
    IMenuCategoryLink,
    IPageLink,
    IPhoneNumberLink,
    IProduct,
} from '../../../@generated/@types/contentful';
import { InspireLink } from '../../atoms/link';

interface IAlertBanner {
    link?: IDocumentLink | IExternalLink | IMenuCategoryLink | IPageLink | IPhoneNumberLink | IProduct;
    text: string;
    backgroundColor?: string;
    textColor?: string;
    isDismissible: boolean;
    showOnCheckout?: boolean;
    bannerId: string;
    onCloseClick: (bannerId: string) => void;
}

export default function AlertBanner(props: IAlertBanner): JSX.Element {
    const { link, text, backgroundColor, textColor, isDismissible, bannerId, onCloseClick } = props;

    // TODO: change this to theming solution when it is ready
    const StyledCloseIcon = styled(CloseIcon)({
        color: textColor ? textColor : 'var(--col--light)',
        width: '22px',
        height: '22px',
    });

    const onCloseKeyPress = (e: React.KeyboardEvent<SVGElement>, bannerId: string) => {
        if (e.key === 'Enter') {
            onCloseClick(bannerId);
        }
    };

    return (
        <div role="banner" className={classNames(`alertBannerConf-${bannerId}`, styles.alertBanner)}>
            {link ? (
                <InspireLink link={link}>
                    <div className={classNames(`textColorConf-${bannerId}`, styles.linkText, 'link-secondary-active')}>
                        {text}
                    </div>
                </InspireLink>
            ) : (
                <div className={classNames(`textColorConf-${bannerId}`, styles.linkText, 'link-secondary-active')}>
                    {text}
                </div>
            )}

            {isDismissible && (
                <StyledCloseIcon
                    tabIndex={0}
                    onClick={() => onCloseClick(bannerId)}
                    onKeyPress={(e) => onCloseKeyPress(e, bannerId)}
                    className={styles.closeIcon}
                    focusable
                    aria-label="Close"
                    aria-hidden="false"
                />
            )}
            {/* TODO: change this to theming solution when it is ready */}
            <style jsx>{`
                .alertBannerConf-${bannerId} {
                    background-color: ${backgroundColor ? backgroundColor : 'var(--col--primary1)'};
                }
                .textColorConf-${bannerId} {
                    color: ${textColor ? textColor : 'var(--col--light)'};
                }
            `}</style>
        </div>
    );
}
