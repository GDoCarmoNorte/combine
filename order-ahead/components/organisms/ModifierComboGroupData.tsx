import React, { useCallback, useState } from 'react';
import classnames from 'classnames';
import { IDisplayFullProduct, IDisplayProductSection, ProductTypesEnum } from '../../redux/types';
import { usePdp } from '../../redux/hooks';
import { IModifierItemById, IProductItemById } from '../../common/services/globalContentfulProps';
import CategoryHeader from '../atoms/categoryHeader';
import { ModifierCardContainer } from './modifierCardContainer/modifierCardContainer';
import styles from './ModifierComboGroupData.module.css';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { useChangedModifiersForChildItem, useDisplayModifierGroupsByProductId } from '../../redux/hooks/pdp';

interface IModifierComboGroupControllerProps {
    modifierItemsById: IModifierItemById | IProductItemById;
    productsById: IProductItemById;
    displayProduct: IDisplayFullProduct;
    isPromo: boolean;
}

interface ISideOrDrinkSectionProps {
    section: IDisplayProductSection;
    modifierItemsById: IModifierItemById | IProductItemById;
    productsById: IProductItemById;
    productId?: string;
    sectionIndex: number;
    productType: string;
    isPromo?: boolean;
}

function SideOrDrinkSection(props: ISideOrDrinkSectionProps): JSX.Element {
    const { section, modifierItemsById, productsById, sectionIndex, productId, isPromo } = props;
    const pdp = usePdp();
    const { setSelectedChildProduct } = pdp.useProductChild(sectionIndex);
    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const modifierGroups = useDisplayModifierGroupsByProductId(productId, section.productGroupId, sectionIndex);

    const { addedModifiers, removedDefaultModifiers } = useChangedModifiersForChildItem(sectionIndex);

    return (
        <>
            <CategoryHeader
                text={`${!isPromo ? 'Your ' : ''}${section.productSectionDisplayName}`}
                notationText={'(Select one)'}
                showBullet
            />
            <div
                className={styles.modifierCardContainer}
                aria-label={`${section.productSectionDisplayName} Section`}
                role="list"
            >
                {modifierGroups.map((group) => (
                    <SideOrDrinkSubsection
                        key={group.displayName}
                        subheaderSection={{
                            subheader: modifierGroups.length > 1 && !isPromo ? group.displayName : '',
                            displayProducts: group.modifiers,
                        }}
                        modifierItemsById={modifierItemsById}
                        setSelectedChildProduct={setSelectedChildProduct}
                        isCollapsed={isCollapsed}
                        productsById={productsById}
                        isPromo={isPromo}
                        sectionIndex={sectionIndex}
                        addedModifiers={addedModifiers}
                        removedDefaultModifiers={removedDefaultModifiers}
                    />
                ))}
                <div className={styles.accordionHeader} onClick={() => setIsCollapsed(!isCollapsed)}>
                    {isCollapsed ? <ExpandMoreIcon viewBox="6 8.59 12 7" /> : <ExpandLessIcon viewBox="6 8.59 12 7" />}
                    {isCollapsed ? 'Modify' : 'Collapse'} {section.productSectionDisplayName}
                </div>
            </div>
        </>
    );
}

function SideOrDrinkSubsection({
    subheaderSection,
    modifierItemsById,
    productsById,
    setSelectedChildProduct,
    isCollapsed,
    isPromo,
    sectionIndex,
    addedModifiers,
    removedDefaultModifiers,
}) {
    const setSelectedChildProductCallback = useCallback(
        ({ id }) => {
            setSelectedChildProduct(id);
        },
        [setSelectedChildProduct]
    );

    return (
        <>
            {subheaderSection.subheader && (
                <h3
                    className={classnames('t-header-h2', styles.sectionSubHeader, {
                        [styles.sectionSubHeaderSelected]: isCollapsed,
                    })}
                >
                    {subheaderSection.subheader}
                </h3>
            )}
            {subheaderSection.displayProducts.map((displayProduct, index) => {
                const { displayProductDetails } = displayProduct;
                const isSelected = !!displayProductDetails.quantity;

                const wrapperClasses = classnames(
                    {
                        [styles.modifierCardWrapperNotSelected]: !isSelected && isCollapsed,
                    },
                    styles.modifierCardWrapper
                );

                return (
                    <div
                        key={index}
                        className={wrapperClasses}
                        role="listitem"
                        aria-label={displayProduct?.displayName || ''}
                    >
                        <ModifierCardContainer
                            hideOrderAheadUnavailable={true}
                            modifierItemsById={modifierItemsById}
                            productsById={productsById}
                            selectorType="default"
                            canIncrement={false}
                            canDecrement={false}
                            onChange={setSelectedChildProductCallback}
                            displayProduct={displayProduct}
                            isPromo={isPromo}
                            sectionIndex={sectionIndex}
                            addedModifiers={isSelected ? addedModifiers : null}
                            removedDefaultModifiers={isSelected ? removedDefaultModifiers : null}
                            selectionType="multi"
                        />
                    </div>
                );
            })}
        </>
    );
}

export default function ModifierComboGroupData({
    displayProduct,
    modifierItemsById,
    productsById,
    isPromo,
}: IModifierComboGroupControllerProps): JSX.Element | null {
    return (
        <>
            {displayProduct.productSections
                .filter((section) => section.productSectionType !== 'main')
                .map((section, index) => {
                    const sectionIndex = displayProduct.productType === ProductTypesEnum.Promo ? index : index + 1;
                    return (
                        <SideOrDrinkSection
                            key={section.productSectionDisplayName}
                            productType={displayProduct.productType}
                            sectionIndex={sectionIndex}
                            productId={displayProduct.mainProductId}
                            section={section}
                            modifierItemsById={modifierItemsById}
                            productsById={productsById}
                            isPromo={isPromo}
                        />
                    );
                })}
        </>
    );
}
