import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
import classnames from 'classnames';
import Icon from '../../atoms/BrandIcon';
import styles from './notifications.module.css';
import { NotificationType } from '../../../common/constants/notificationType';

interface INotification {
    id?: number;
    title: string;
    message: string;
    type?: NotificationType;
    onCloseClick?: (id: number) => void;
}

export default function Notification(props: INotification): JSX.Element {
    const { id, title, message, type, onCloseClick } = props;

    const iconClasses = {
        root: styles.closeIcon,
    };

    if (type === NotificationType.SUCCESS) {
        return (
            <div role="alert" className={classnames(styles.notification, styles.successNotification)}>
                <div className={styles.notificationContent}>
                    <Icon className={styles.notificationIcon} icon="action-check" />
                    <span className={classnames('t-subheader-smaller', styles.title)}>{title}:</span>
                    <span className={classnames('t-subheader-smaller', styles.message)}>{message}</span>
                </div>
                <CloseIcon onClick={() => onCloseClick(id)} classes={iconClasses} aria-label="close" />
            </div>
        );
    }

    return (
        <div role="alert" className={classnames(styles.notification, styles.errorNotification)}>
            <div className={styles.notificationContent}>
                <Icon className={styles.notificationIcon} icon="info-error" variant="light" />
                <span className={classnames('t-subheader-smaller', styles.title)}>{title}:</span>
                <span className={classnames('t-subheader-smaller', styles.message)}>{message}</span>
            </div>
            <CloseIcon onClick={() => onCloseClick(id)} classes={iconClasses} aria-label="close" />
        </div>
    );
}
