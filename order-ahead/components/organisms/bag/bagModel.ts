import { Dictionary } from '@reduxjs/toolkit';

import { ItemModel, TallyProductModel } from '../../../@generated/webExpApi';
import { IProductFields } from '../../../@generated/@types/contentful';

import { formatPrice } from '../../../lib/domainProduct';
import { DiscountStringMap, getDiscountType } from '../../../common/helpers/discountHelper';
import { LocationWithDetailsModel } from '../../../common/services/locationService/types';
import { isAvailableProductByTally } from '../../../common/helpers/bagHelper';
import { InspireCmsEntry } from '../../../common/types';
import { IUnavailableItem } from '../../../common/hooks/useUnavailableItems';

export interface IBagRenderItem {
    entry: TallyProductModel;
    isAvailable: boolean;
    markedAsRemoved: boolean;
    contentfulProduct: InspireCmsEntry<IProductFields>;
    entryPath?: { href: string; as: string };
    discountBanner?: string;
}

export const getDiscountBanner = (
    entry: TallyProductModel,
    location: LocationWithDetailsModel,
    domainProduct: ItemModel,
    isPromo: boolean
): string | undefined => {
    const discountType = getDiscountType(domainProduct, location);

    if (discountType) {
        return DiscountStringMap[discountType];
    }

    if (isPromo) {
        return `${domainProduct.itemModifierGroups?.length} FOR ${formatPrice(entry.price)}`;
    }

    return undefined;
};

export const getCheckoutEntries = (
    renderItems: IBagRenderItem[],
    unavailableTallyItems: IUnavailableItem[]
): TallyProductModel[] =>
    renderItems
        .filter(
            (item) =>
                item.isAvailable && !item.markedAsRemoved && isAvailableProductByTally(item, unavailableTallyItems)
        )
        .map((item) => item.entry);

export const hasCheckoutEntries = (renderItems: IBagRenderItem[], unavailableTallyItems: IUnavailableItem[]): boolean =>
    renderItems.filter(
        (item) => item.isAvailable && !item.markedAsRemoved && isAvailableProductByTally(item, unavailableTallyItems)
    ).length > 0;

export const sortRenderItems = (
    renderItems: IBagRenderItem[],
    domainProducts: Dictionary<ItemModel>
): IBagRenderItem[] => {
    return renderItems.sort((left, right) => {
        const leftProductName = domainProducts[left.entry.productId]?.name?.toLowerCase();
        const rightProductName = domainProducts[right.entry.productId]?.name?.toLowerCase();

        if (leftProductName < rightProductName) return -1;
        if (leftProductName > rightProductName) return 1;

        return 0;
    });
};
