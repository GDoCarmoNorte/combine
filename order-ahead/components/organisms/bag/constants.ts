export const typeButton = 'secondary';

export const TYPOGRAPHY_CLASS = {
    NAME: 't-subheader-small',
};

export const PLUS_MINUS_ICONS_ACTIVE_VARIANT = 'colorful';

export const SHOW_NUMBER_ITEMS = true;
