import React, { useEffect, useState } from 'react';

import { InspireButton } from '../../atoms/button';

import styles from './index.module.css';
import InspireTooltip from '../../atoms/Tooltip';
import { useRouter } from 'next/router';
import { GTM_ADD_MORE_ITEMS } from '../../../common/services/gtmService/constants';
import { useAppDispatch } from '../../../redux/store';
import { requestNavIntercept } from '../../../redux/navIntercept';
import useBag from '../../../redux/hooks/useBag';
import { useConfiguration } from '../../../redux/hooks';
import { IBagRenderItem } from './bagModel';
import { LocationWithDetailsModel } from '../../../common/services/locationService/types';
import { typeButton } from './constants';

interface IBagButtonsProps {
    isLoading: boolean;
    isCheckoutDisabledByTally: boolean;
    location: LocationWithDetailsModel;
    renderItems: IBagRenderItem[];
    toggleBag: () => void;
    onCheckoutClick: () => void;
    tooltipText?: string;
    onTooltipDismiss?: () => void;
    tooltipDismiss?: boolean;
    noCheckoutEntries: boolean;
    isOverLimitForOrder?: boolean;
    disabled?: boolean;
}

const BagButtons = (props: IBagButtonsProps): JSX.Element => {
    const {
        isLoading,
        isCheckoutDisabledByTally,
        location,
        toggleBag,
        renderItems,
        onCheckoutClick,
        tooltipText,
        onTooltipDismiss,
        tooltipDismiss,
        noCheckoutEntries,
        isOverLimitForOrder,
        disabled,
    } = props;

    const allItemsRemoved = renderItems.every((item) => item.markedAsRemoved);
    const isBagEmpty = !renderItems.length || allItemsRemoved;

    if (isBagEmpty) return <CallToActionButton disabled={disabled} isLocation={!location} toggleBag={toggleBag} />;

    return (
        <Buttons
            location={location}
            isLoading={isLoading}
            isCheckoutDisabledByTally={isCheckoutDisabledByTally}
            toggleBag={toggleBag}
            renderItems={renderItems}
            onCheckoutClick={onCheckoutClick}
            tooltipText={tooltipText}
            onTooltipDismiss={onTooltipDismiss}
            tooltipDismiss={tooltipDismiss}
            noCheckoutEntries={noCheckoutEntries}
            isOverLimitForOrder={isOverLimitForOrder}
        />
    );
};

interface IButtonsProps {
    isLoading: boolean;
    isCheckoutDisabledByTally: boolean;
    location: LocationWithDetailsModel;
    renderItems: IBagRenderItem[];
    onCheckoutClick: () => void;
    toggleBag: () => void;
    tooltipText?: string;
    onTooltipDismiss?: () => void;
    tooltipDismiss?: boolean;
    noCheckoutEntries: boolean;
    isOverLimitForOrder?: boolean;
}

const Buttons = (props: IButtonsProps): JSX.Element => {
    const {
        onCheckoutClick,
        toggleBag,
        isLoading,
        isCheckoutDisabledByTally,
        location,
        tooltipText,
        onTooltipDismiss,
        tooltipDismiss,
        noCheckoutEntries,
        isOverLimitForOrder,
        renderItems,
    } = props;

    const [tooltipVisible, setTooltipVisible] = useState(false);

    const bag = useBag();
    const router = useRouter();
    const dispatch = useAppDispatch();
    const checkoutButtonText = 'CHECKOUT NOW';
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    const isButtonsDisabled = isLoading || isOverLimitForOrder || !!tooltipText || !isOAEnabled;
    const allItemsIsUnavailable = renderItems.every((item) => !item.isAvailable);
    const isCallToActionLocation = !location || allItemsIsUnavailable;

    const handleAddMoreItemsClick = () => {
        dispatch(requestNavIntercept()).then(({ payload: result }) => {
            if (result) {
                dispatch({ type: GTM_ADD_MORE_ITEMS });
                toggleBag();
                router.push('/menu');
            } else {
                bag.actions.toggleIsOpen({ isOpen: false });
            }
        });
    };

    useEffect(() => {
        setTooltipVisible(!!tooltipText);

        if (!tooltipDismiss) return;

        const timeout = setTimeout(() => {
            setTooltipVisible(false);
            typeof onTooltipDismiss === 'function' && onTooltipDismiss();
        }, 3000);
        return () => {
            timeout && clearTimeout(timeout);
        };
    }, [tooltipText, onTooltipDismiss, tooltipDismiss]);

    return (
        <InspireTooltip
            tooltipClassName={styles.tooltip}
            open={tooltipVisible}
            disableFocusListener
            disableHoverListener
            disableTouchListener
            title={tooltipText || ''}
            placement="top"
            theme="primary"
            arrow
        >
            {!noCheckoutEntries || isCheckoutDisabledByTally ? (
                <div className={styles.buttons}>
                    <InspireButton
                        className={styles.bagButton}
                        onClick={handleAddMoreItemsClick}
                        disabled={isButtonsDisabled}
                        type={typeButton}
                        text="ADD MORE ITEMS"
                    />
                    <InspireButton
                        className={styles.bagButton}
                        onClick={onCheckoutClick}
                        type="primary"
                        text={checkoutButtonText}
                        disabled={isButtonsDisabled || isCheckoutDisabledByTally}
                    />
                </div>
            ) : (
                <div className={styles.callToActionButtonsContainer}>
                    <CallToActionButton
                        isLocation={isCallToActionLocation}
                        isChangeLocation={allItemsIsUnavailable}
                        toggleBag={toggleBag}
                        disabled={!isOAEnabled}
                    />
                </div>
            )}
        </InspireTooltip>
    );
};

interface ICallToActionButtonProps {
    toggleBag: () => void;
    isLocation: boolean;
    isChangeLocation?: boolean;
    disabled?: boolean;
}

const CallToActionButton = ({
    isLocation,
    toggleBag,
    isChangeLocation = false,
    disabled,
}: ICallToActionButtonProps) => {
    const router = useRouter();
    const callToActionLabel = isLocation ? 'SELECT A LOCATION' : 'START AN ORDER';

    const handleCallToActionClick = () => {
        router.push(`/${isLocation ? 'locations' : 'menu'}`);
        toggleBag();
    };

    return (
        <div className={`${styles.buttons} ${styles.callToActionButtons}`}>
            <InspireButton
                className={`${styles.bagButton} ${styles.callToActionButton}`}
                type="primary"
                onClick={handleCallToActionClick}
                text={isChangeLocation ? 'CHANGE LOCATION' : callToActionLabel}
                disabled={disabled}
            />
        </div>
    );
};

export default BagButtons;
