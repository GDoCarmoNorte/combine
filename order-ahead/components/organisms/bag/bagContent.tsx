import React, { useEffect } from 'react';
import classnames from 'classnames';
import { useAppDispatch } from '../../../redux/store';
import { GTM_ERROR_EVENT } from '../../../common/services/gtmService/constants';
import { GtmErrorCategory, GtmErrorEvent } from '../../../common/services/gtmService/types';

import Loader from '../../atoms/Loader';
import ReviewOrderTotal from '../../atoms/ReviewOrderTotal';

import BagItem from './item';

import styles from './index.module.css';
import itemStyles from './item.module.css';

import { IBagRenderItem, sortRenderItems } from './bagModel';
import useAccount from '../../../redux/hooks/useAccount';
import { useDomainProducts } from '../../../redux/hooks/domainMenu';
import UserDeals from '../../atoms/userDeals';
import { useBag, useOrderLocation, useSelectedSell } from '../../../redux/hooks';
import BagDealItem from './bagDealItem';
import RecommendedProducts from '../recommendedProducts';

import { InspireLinkButton } from '../../atoms/link';
import { OrderLocationMethod } from '../../../redux/orderLocation';
import { TallyProductModel } from '../../../@generated/webExpApi';
import { IUnavailableItem } from '../../../common/hooks/useUnavailableItems';
import {
    isItemUnavailableByModifiers,
    isAvailableProductByTally,
    isWholeItemUnavailable,
} from '../../../common/helpers/bagHelper';

export interface IBagContentProps {
    totalPrice: string;
    isMenuLoading: boolean;
    renderItems: IBagRenderItem[];
    onRemoveUnavailable: (unavailableEntries: TallyProductModel[]) => void;
    totalDiscount: number | string;
    subTotal: string;
    tooltipText?: string | JSX.Element;
    isOverLimitForOrder?: boolean;
    unavailableTallyItems: IUnavailableItem[];
}

const BagContent = ({
    totalPrice,
    isMenuLoading,
    renderItems,
    onRemoveUnavailable,
    totalDiscount,
    subTotal,
    tooltipText,
    isOverLimitForOrder,
    unavailableTallyItems,
}: IBagContentProps): JSX.Element => {
    const { account } = useAccount();
    const { recommendedProductIds } = useSelectedSell();
    const { dealId, actions } = useBag();
    const dispatch = useAppDispatch();
    const { isCurrentLocationOAAvailable: isOnlineOrderAvailable, method } = useOrderLocation();

    const availableItems = renderItems.filter(
        (item) => isOnlineOrderAvailable && item.isAvailable && isAvailableProductByTally(item, unavailableTallyItems)
    );

    const itemsWithUnavailableModifiers = renderItems.filter((item) =>
        isItemUnavailableByModifiers(item, unavailableTallyItems)
    );

    let unavailableItems = renderItems.filter(
        (item) => !isOnlineOrderAvailable || !item.isAvailable || isWholeItemUnavailable(item, unavailableTallyItems)
    );

    unavailableItems = sortRenderItems(
        unavailableItems,
        useDomainProducts(unavailableItems.map((item) => item.entry.productId))
    );

    const handleRemoveAll = () => {
        onRemoveUnavailable([...unavailableItems, ...itemsWithUnavailableModifiers]?.map((item) => item.entry));
        actions.removeDealFromBag();
    };

    useEffect(() => {
        if (unavailableItems?.length) {
            const payload = {
                ErrorCategory: GtmErrorCategory.BAG_ERROR,
                ErrorDescription: 'These items are currently unavailable at this location',
            } as GtmErrorEvent;

            dispatch({ type: GTM_ERROR_EVENT, payload });
        }
    }, [unavailableItems, dispatch]);

    return isMenuLoading ? (
        <span className={styles.loader}>
            <Loader size={50} />
        </span>
    ) : (
        <div className={styles.bagItems}>
            <h3 className={classnames('t-header-card-title', styles.summaryHeader)}>Order Summary</h3>
            <div role="list" aria-label="Bag items" className={styles.bagItemsList}>
                {availableItems?.map((item) => (
                    <BagItem
                        entry={item.entry}
                        entryPath={item.entryPath}
                        key={item.entry.lineItemId}
                        markedAsRemoved={item.markedAsRemoved}
                        contentfulProduct={item.contentfulProduct}
                        discountBanner={item.discountBanner}
                        tabIndex={0}
                    />
                ))}
                {dealId && isOnlineOrderAvailable && <BagDealItem dealId={dealId} />}
            </div>
            {unavailableItems?.length + itemsWithUnavailableModifiers.length > 0 && (
                <div className={styles.bagItemsList}>
                    <div className={styles.unavailableBlockControls}>
                        <h4
                            className={classnames(
                                't-header-card-title',
                                styles.summaryHeader,
                                styles.unavailableHeader
                            )}
                        >
                            Unavailable
                        </h4>
                        <InspireLinkButton
                            linkType="secondary"
                            className={styles.unavailableRemove}
                            onClick={handleRemoveAll}
                        >
                            REMOVE ALL
                        </InspireLinkButton>
                    </div>
                    {itemsWithUnavailableModifiers.map((item) => {
                        const unavailableTallyItem = unavailableTallyItems.find(
                            ({ productId, lineItemId }) =>
                                productId === item.entry.productId && lineItemId === item.entry.lineItemId
                        );
                        const { modifierIds, subModifierIds, isDefaultModifiersUnavailable } = unavailableTallyItem;

                        return (
                            <BagItem
                                entry={item.entry}
                                entryPath={item.entryPath}
                                key={item.entry.lineItemId}
                                markedAsRemoved={item.markedAsRemoved}
                                contentfulProduct={item.contentfulProduct}
                                unavailableModifiers={modifierIds}
                                isDefaultModifiersUnavailable={isDefaultModifiersUnavailable}
                                unavailableSubModifiers={subModifierIds}
                            />
                        );
                    })}
                    {unavailableItems?.length ? (
                        <div className={classnames('t-paragraph-hint', styles.unavailableHint)}>
                            These items are currently unavailable at this location.
                        </div>
                    ) : null}
                    <div className={itemStyles.unavailable}>
                        {unavailableItems?.map((item) => (
                            <BagItem
                                entry={item.entry}
                                entryPath={item.entryPath}
                                key={item.entry.lineItemId}
                                markedAsRemoved={item.markedAsRemoved}
                                contentfulProduct={item.contentfulProduct}
                                isUnavailableItem={true}
                            />
                        ))}
                        {dealId && !isOnlineOrderAvailable && <BagDealItem dealId={dealId} />}
                    </div>
                </div>
            )}
            {recommendedProductIds.length > 0 && <RecommendedProducts />}
            {account && <UserDeals />}
            <div className={styles.reviewTotal}>
                <ReviewOrderTotal
                    totalTax="TBD"
                    subtotal={subTotal}
                    totalPrice={totalPrice}
                    totalTaxLabel="Estimated Tax"
                    totalDiscount={totalDiscount}
                    tooltipText={tooltipText}
                    additionalFees={method === OrderLocationMethod.DELIVERY ? 'TBD' : ''}
                    isOverLimitForOrder={isOverLimitForOrder}
                />
            </div>
        </div>
    );
};

export default BagContent;
