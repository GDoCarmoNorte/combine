import React, { Fragment, useMemo } from 'react';
import { useRouter } from 'next/router';
import classnames from 'classnames';

import { ItemModel, TallyProductModel } from '../../../@generated/webExpApi';
import { IProductFields } from '../../../@generated/@types/contentful';
import {
    useDomainMenuSelectors,
    useDomainProduct,
    useDomainProducts,
    useProductItemGroup,
    useIsProductEditable,
    useSelectedModifiers,
    useTallyPriceAndCalories,
} from '../../../redux/hooks/domainMenu';
import ContentfulImage from '../../atoms/ContentfulImage';

import styles from './item.module.css';

import ListModifiers from '../../atoms/ListModifiers';
import Icon from '../../atoms/BrandIcon';
import { useBag, usePdp } from '../../../redux/hooks';
import { formatPrice } from '../../../lib/domainProduct';
import InspireBadge from '../../atoms/Badge';

import {
    GTM_MODIFY_PRODUCT,
    GTM_QTY_DECREASE,
    GTM_QTY_INCREASE,
    GTM_REMOVE_FROM_CART,
    GTM_RESTORE_ITEM_TO_CART,
} from '../../../common/services/gtmService/constants';
import TextWithTrademark from '../../atoms/textWithTrademark';
import { getBagItemSizeLabel, removeUnavailableModifiers } from '../../../common/helpers/bagHelper';
import { useAppDispatch } from '../../../redux/store';
import { requestNavIntercept } from '../../../redux/navIntercept';
import { useTallyModifiers } from '../../../common/hooks/useTallyModifiers';
import { useTallyItemIsPromo } from '../../../redux/hooks/useTally';
import { useProductHasOtherSizes } from '../../../common/hooks/useProductHasOtherSizes';
import { MAX_PRODUCT_QUANTITY, MIN_PRODUCT_QUANTITY } from '../../../common/constants/bag';

import { getChangedModifiers } from '../../../common/helpers/getChangedModifiers';
import { PLUS_MINUS_ICONS_ACTIVE_VARIANT, TYPOGRAPHY_CLASS } from './constants';
import { InspireLinkButton } from '../../atoms/link';
import NonexistentItem from './nonexistentItem/nonexistentItem';
import getBrandInfo from '../../../lib/brandInfo';
import { ItemGroupEnum, ModifierGroupType } from '../../../redux/types';

import { InspireCmsEntry } from '../../../common/types';

interface IBagItemProps {
    entry: TallyProductModel;
    markedAsRemoved: boolean;
    contentfulProduct: InspireCmsEntry<IProductFields>;
    entryPath?: { href: string; as: string };
    discountBanner?: string;
    tabIndex?: number;
    unavailableModifiers?: string[];
    isDefaultModifiersUnavailable?: boolean;
    unavailableSubModifiers?: string[];
    isUnavailableItem?: boolean;
}

interface IBagItemBlockProps {
    entry: TallyProductModel;
    contentfulProduct: InspireCmsEntry<IProductFields>;
    entryPath?: { href: string; as: string };
    domainProduct: ItemModel;
    discountBanner?: string;
    tabIndex?: number;
    unavailableModifiers?: string[];
    isDefaultModifiersUnavailable?: boolean;
    unavailableSubModifiers?: string[];
    isUnavailableItem: boolean;
}

interface IRemovedItemProps {
    entry: TallyProductModel;
    domainProduct: ItemModel;
}

interface IBagItemDetailsProps {
    entry: TallyProductModel;
    entryPrice: number;
    domainProduct: ItemModel;
    unavailableModifiers?: string[];
    isDefaultModifiersUnavailable?: boolean;
    unavailableSubModifiers?: string[];
    isUnavailableItem: boolean;
}

export interface ITallyItemProduct {
    product: ItemModel;
    item: TallyProductModel;
}

export interface IBagDealItem {
    name: string;
    id: string;
    endDate?: Date;

    onRemove(): void;
}

function BagItem(props: IBagItemProps): JSX.Element {
    const {
        entry,
        entryPath,
        markedAsRemoved,
        contentfulProduct,
        discountBanner,
        tabIndex,
        unavailableModifiers,
        unavailableSubModifiers,
        isUnavailableItem = false,
        isDefaultModifiersUnavailable = false,
    } = props;
    const domainProduct = useDomainProduct(entry.productId);

    if (!domainProduct) {
        return <NonexistentItem contentfulProduct={contentfulProduct} quantity={entry.quantity} />;
    }

    return (
        <div className={styles.bagItemContainer} role="listitem" aria-label={domainProduct.name}>
            {!markedAsRemoved && (
                <BagItemBlock
                    entry={entry}
                    entryPath={entryPath}
                    contentfulProduct={contentfulProduct}
                    discountBanner={discountBanner}
                    domainProduct={domainProduct}
                    tabIndex={tabIndex}
                    unavailableModifiers={unavailableModifiers}
                    isDefaultModifiersUnavailable={isDefaultModifiersUnavailable}
                    unavailableSubModifiers={unavailableSubModifiers}
                    isUnavailableItem={isUnavailableItem}
                />
            )}
            {markedAsRemoved && <RemovedItem entry={entry} domainProduct={domainProduct} />}
        </div>
    );
}

function BagItemBlock(props: IBagItemBlockProps): JSX.Element {
    const {
        entry,
        entryPath,
        contentfulProduct,
        discountBanner,
        domainProduct,
        tabIndex,
        unavailableModifiers,
        isDefaultModifiersUnavailable,
        unavailableSubModifiers,
        isUnavailableItem,
    } = props;

    const router = useRouter();
    const dispatch = useAppDispatch();

    const bag = useBag();
    const pdp = usePdp();

    const { totalPrice: totallPriceForSingleProduct } = useTallyPriceAndCalories(entry);

    const price = totallPriceForSingleProduct * entry.quantity;

    const isEditable = useIsProductEditable(entry.productId);
    const productHasOtherSizes = useProductHasOtherSizes(entry.productId);

    const shouldShowModifyLink = (isEditable || productHasOtherSizes) && !isUnavailableItem;

    const hasUnsavedModifications = pdp.useHasUnsavedModifications();

    const canIncrement = useMemo(() => {
        if (entry.quantity >= MAX_PRODUCT_QUANTITY) {
            return false;
        }
        return true;
    }, [entry.quantity]);

    const canDecrement = useMemo(() => {
        if (entry.quantity <= MIN_PRODUCT_QUANTITY) {
            return false;
        }
        return true;
    }, [entry.quantity]);

    const incrementCount = () => {
        if (!canIncrement) return;

        const entryIndex = bag.bagEntries.findIndex((e) => e.lineItemId === entry.lineItemId);

        if (entryIndex > -1) {
            bag.actions.updateBagItemCount({ bagEntryIndex: entryIndex, value: 1 });
            pdp.actions.editTallyItemCount({
                pageProductId: domainProduct?.id,
                lineItemId: entry.lineItemId,
                value: 1,
            });
            dispatch({ type: GTM_QTY_INCREASE, payload: entry });
        }
    };

    const decrementCount = () => {
        if (!canDecrement) return;

        const entryIndex = bag.bagEntries.findIndex((e) => e.lineItemId === entry.lineItemId);

        if (entryIndex > -1) {
            bag.actions.updateBagItemCount({ bagEntryIndex: entryIndex, value: -1 });
            pdp.actions.editTallyItemCount({
                pageProductId: domainProduct?.id,
                lineItemId: entry.lineItemId,
                value: -1,
            });
            dispatch({ type: GTM_QTY_DECREASE, payload: entry });
        }
    };

    const selectedModifiers = useSelectedModifiers(entry);

    const hasUnavailableModifiersOrSubModifiers = !!(unavailableModifiers?.length || unavailableSubModifiers?.length);

    const hasUnavailableNotDefaultModifiersOrSubModifiers =
        hasUnavailableModifiersOrSubModifiers && !isDefaultModifiersUnavailable;

    const markAsRemoved = () => {
        if (hasUnsavedModifications) {
            dispatch(requestNavIntercept()).then(({ payload: result }) => {
                if (result) {
                    bag.actions.markAsRemoved({ lineItemId: entry.lineItemId });
                } else {
                    bag.actions.toggleIsOpen({ isOpen: false });
                }
            });
        } else {
            const sauce = selectedModifiers.reduce((acc, modifier) => {
                if (modifier.metadata?.MODIFIER_GROUP_TYPE !== ModifierGroupType.SAUCES) return acc;
                const numberSauces = Object.keys(acc).length;
                return {
                    ...acc,
                    [`sauce${numberSauces + 1}`]: modifier.name || 'none',
                };
            }, {});

            /*
            / Behavior for unavailable items
            / Remove bag item or modifier
            */
            if (isUnavailableItem || isDefaultModifiersUnavailable) {
                bag.actions.removeFromBag({ lineItemId: entry.lineItemId });
                return;
            }

            if (hasUnavailableModifiersOrSubModifiers) {
                const updatedBagItem = removeUnavailableModifiers(entry, unavailableModifiers, unavailableSubModifiers);
                bag.actions.editBagLineItem({
                    bagEntry: updatedBagItem,
                });
                /*
                / Behavior by default
                / Mark as removed
                */
            } else {
                bag.actions.markAsRemoved({ lineItemId: entry.lineItemId });
                dispatch({ type: GTM_REMOVE_FROM_CART, payload: { ...entry, sauce } });
            }
        }
    };

    const removeTitle = hasUnavailableNotDefaultModifiersOrSubModifiers
        ? 'remove unavailable modifier'
        : 'remove product';

    const onModifyClick = (e) => {
        e.preventDefault();

        dispatch(requestNavIntercept()).then(({ payload: result }) => {
            if (result) {
                bag.actions.toggleIsOpen({ isOpen: false });
                pdp.actions.putTallyItem({
                    lineItemId: entry.lineItemId,
                    pdpTallyItem: entry,
                });
                dispatch({ type: GTM_MODIFY_PRODUCT });
                if (entryPath && router) {
                    router.push(entryPath.href, entryPath.as);
                }
            } else {
                bag.actions.toggleIsOpen({ isOpen: false });
            }
        });
    };

    const handleEnterKeyDown = (callback: (e: React.KeyboardEvent<HTMLElement>) => void) => (
        e: React.KeyboardEvent<HTMLElement>
    ) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            callback(e);
        }
    };

    return (
        <>
            <div
                className={classnames(styles.bagItem, {
                    [styles.unavailableItem]:
                        unavailableModifiers?.length > 0 || unavailableSubModifiers?.length > 0 || isUnavailableItem,
                })}
            >
                {discountBanner && <InspireBadge className="inspireBadge" value={discountBanner} />}

                <ContentfulImage
                    className={styles.bagItemImage}
                    asset={contentfulProduct?.fields?.image}
                    maxWidth={92}
                />
                <div className={styles.bagItemInfo}>
                    {entry.childItems ? (
                        <ComboBagItemDetail
                            entry={entry}
                            entryPrice={price}
                            domainProduct={domainProduct}
                            unavailableModifiers={unavailableModifiers}
                            unavailableSubModifiers={unavailableSubModifiers}
                            isUnavailableItem={isUnavailableItem}
                        />
                    ) : (
                        <RegularBagItemDetail
                            entry={entry}
                            entryPrice={price}
                            domainProduct={domainProduct}
                            unavailableModifiers={unavailableModifiers}
                            unavailableSubModifiers={unavailableSubModifiers}
                            isUnavailableItem={isUnavailableItem}
                        />
                    )}
                    <div className={classnames(styles.bagItemLinks, { [styles.noBorder]: !shouldShowModifyLink })}>
                        {domainProduct && shouldShowModifyLink && (
                            <InspireLinkButton
                                linkType="secondary"
                                className={styles.bagItemLabel}
                                onClick={onModifyClick}
                                onKeyDown={handleEnterKeyDown(onModifyClick)}
                            >
                                MODIFY
                            </InspireLinkButton>
                        )}
                        <InspireLinkButton
                            aria-label="remove bag item"
                            linkType="secondary"
                            className={styles.bagItemLabel}
                            onClick={markAsRemoved}
                            onKeyDown={handleEnterKeyDown(markAsRemoved)}
                            title={removeTitle}
                        >
                            REMOVE
                        </InspireLinkButton>
                    </div>
                </div>
            </div>
            {!(unavailableModifiers || unavailableSubModifiers || isUnavailableItem) ? (
                <div className={styles.quantityControls}>
                    <div
                        tabIndex={tabIndex}
                        aria-label="reduce quantity"
                        className={classnames(styles.quantityControl, styles.bagItemLabel, {
                            [styles.activeControl]: canDecrement,
                        })}
                        onClick={decrementCount}
                        onKeyDown={handleEnterKeyDown(decrementCount)}
                        role="button"
                    >
                        <Icon
                            icon="action-subtract"
                            size="s"
                            variant={canDecrement ? PLUS_MINUS_ICONS_ACTIVE_VARIANT : 'gray4'}
                        />
                    </div>

                    <div
                        aria-label="quantity"
                        className={classnames('t-subheader-small', styles.quantityControl, styles.bagItemLabel)}
                    >
                        {entry.quantity}
                    </div>
                    <div
                        tabIndex={tabIndex}
                        aria-label="increase quantity"
                        className={classnames(styles.quantityControl, styles.bagItemLabel, {
                            [styles.activeControl]: canIncrement,
                        })}
                        onClick={incrementCount}
                        onKeyDown={handleEnterKeyDown(incrementCount)}
                        role="button"
                    >
                        <Icon
                            icon="action-add"
                            size="s"
                            variant={canIncrement ? PLUS_MINUS_ICONS_ACTIVE_VARIANT : 'gray4'}
                        />
                    </div>
                </div>
            ) : null}
        </>
    );
}

function RegularBagItemDetail(props: IBagItemDetailsProps): JSX.Element {
    const {
        entry,
        entryPrice,
        domainProduct,
        unavailableModifiers,
        unavailableSubModifiers,
        isUnavailableItem,
    } = props;
    const { brandId } = getBrandInfo();
    const { selectProductSize } = useDomainMenuSelectors();
    const isUnavailableInfo = !!unavailableModifiers || !!unavailableSubModifiers || isUnavailableItem;

    const productSize = selectProductSize(domainProduct.id);
    const sizeLabel = getBagItemSizeLabel(productSize);
    const { addedModifiers, removedDefaultModifiers, defaultModifiers, defaultSubModifiersData } = useTallyModifiers(
        entry
    );
    const productItemGroup = useProductItemGroup(domainProduct.id);

    const productName = useMemo(() => {
        if (productItemGroup?.name === ItemGroupEnum.BOGO || productItemGroup?.name === ItemGroupEnum.BOGO_50_OFF) {
            return `${domainProduct.name} (${Number(productSize) * 2} Total)`;
        }
        if (productItemGroup?.name === ItemGroupEnum.BOGO_50_OFF) {
            return `Buy ${productSize} ${domainProduct.name} get ${productSize} 50% off (${
                Number(productSize) * 2
            } Total)`;
        }
        if (sizeLabel) {
            return ['Arbys', 'Bww'].includes(brandId)
                ? `${domainProduct.name} (${sizeLabel})`
                : `${sizeLabel} ${domainProduct.name}`;
        }
        return domainProduct.name;
    }, [domainProduct.name, sizeLabel, productItemGroup, productSize, brandId]);

    return (
        <>
            <div className={styles.bagItemHeader}>
                <TextWithTrademark
                    tag="span"
                    className={classnames(TYPOGRAPHY_CLASS.NAME, styles.bagItemLabel)}
                    text={`${entry.quantity > 1 && isUnavailableInfo ? entry.quantity + 'X ' : ''}${productName}`}
                />
                {!isUnavailableInfo && (
                    <span className={classnames(TYPOGRAPHY_CLASS.NAME, styles.bagItemPrice)}>
                        {formatPrice(entryPrice)}
                    </span>
                )}
            </div>
            <ListModifiers
                removedModifiers={removedDefaultModifiers}
                addedModifiers={addedModifiers}
                defaultModifiers={defaultModifiers}
                defaultSubModifiersData={defaultSubModifiersData}
                unavailableModifiers={unavailableModifiers}
                unavailableSubModifiers={unavailableSubModifiers}
            />
        </>
    );
}

function ComboBagItemDetail(props: IBagItemDetailsProps): JSX.Element {
    const {
        entry,
        entryPrice,
        domainProduct,
        unavailableModifiers,
        unavailableSubModifiers,
        isUnavailableItem,
    } = props;
    const isUnavailableInfo = !!unavailableModifiers || !!unavailableSubModifiers || isUnavailableItem;
    const domainProducts = useDomainProducts(entry.childItems?.map((m) => m?.productId));
    const childItems: ITallyItemProduct[] = entry.childItems
        ?.map((item) => ({
            item,
            product: domainProducts[item.productId],
        }))
        .filter((item) => !!item.product);

    const isPromo = useTallyItemIsPromo(entry);
    // Logic below to ensure only the $2 for 6 Promo displays modifiers for now
    const isPromoThatDiscludesModifiers = isPromo && !domainProduct.name.toLowerCase().startsWith('2 for $6');

    const { selectProductSize, selectDefaultModifiers, selectSelectedModifiers } = useDomainMenuSelectors();

    return (
        <>
            <div className={styles.bagItemHeader}>
                <TextWithTrademark
                    tag="span"
                    text={`${entry.quantity > 1 && isUnavailableInfo ? entry.quantity + 'X ' : ''}${
                        domainProduct.name
                    }`}
                    className={classnames(TYPOGRAPHY_CLASS.NAME, styles.bagItemLabel)}
                />
                {!isUnavailableInfo && (
                    <span className={classnames(TYPOGRAPHY_CLASS.NAME, styles.bagItemPrice)}>
                        {formatPrice(entryPrice)}
                    </span>
                )}
            </div>
            <div className={styles.bagItemModifiers}>
                {childItems.map(({ item, product }) => {
                    const selectedModifiers = selectSelectedModifiers(item);
                    const defaultModifiers = selectDefaultModifiers(product.id);

                    const { addedModifiers, removedDefaultModifiers, modifiersIsChanged } = getChangedModifiers(
                        selectedModifiers,
                        defaultModifiers
                    );

                    const tallyItemPrice = item.price || 0;
                    const sizeLabel = getBagItemSizeLabel(selectProductSize(product.id));

                    return (
                        <Fragment key={product.id}>
                            <div className={styles.bagItemModifier}>
                                <TextWithTrademark
                                    tag="span"
                                    className={classnames('t-paragraph-hint', styles.product, styles.bagItemLabel)}
                                    text={`${product.name} ${sizeLabel && `(${sizeLabel})`}`}
                                />
                                {tallyItemPrice !== 0 && (
                                    <span
                                        className={classnames('t-paragraph-hint', styles.product, styles.bagItemPrice)}
                                    >
                                        {formatPrice(tallyItemPrice)}
                                    </span>
                                )}
                            </div>
                            {!isPromoThatDiscludesModifiers && modifiersIsChanged && (
                                <ListModifiers
                                    removedModifiers={removedDefaultModifiers}
                                    addedModifiers={addedModifiers}
                                    defaultModifiers={defaultModifiers}
                                    unavailableModifiers={unavailableModifiers}
                                    unavailableSubModifiers={unavailableSubModifiers}
                                />
                            )}
                        </Fragment>
                    );
                })}
            </div>
        </>
    );
}

function RemovedItem(props: IRemovedItemProps): JSX.Element {
    const { entry, domainProduct } = props;
    const dispatch = useAppDispatch();

    const bag = useBag();
    const { selectProductSize } = useDomainMenuSelectors();

    const markAsRemoved = () => {
        bag.actions.markAsRemoved({ lineItemId: entry.lineItemId });
        dispatch({
            type: GTM_RESTORE_ITEM_TO_CART,
            payload: {
                ...entry,
                reAddCart: true,
            },
        });
    };

    const handleKeyPress = (e: React.KeyboardEvent<HTMLElement>) => {
        if (e.key === 'Enter') {
            markAsRemoved();
        }
    };

    const sizeLabel = getBagItemSizeLabel(selectProductSize(domainProduct.id));

    return (
        <div className={styles.removedItemContainer}>
            <div className={styles.removedItemProduct}>
                <div className={styles.removedItemProductWrapper}>
                    {entry.childItems ? (
                        <TextWithTrademark tag="span" className="t-subheader-small" text={`${domainProduct.name}`} />
                    ) : (
                        <TextWithTrademark
                            tag="span"
                            className="t-subheader-small"
                            text={`${domainProduct.name} ${sizeLabel && `(${sizeLabel})`}`}
                        />
                    )}
                    <span className="t-subheader-small">&nbsp;was removed!</span>
                </div>
            </div>
            <div className={styles.removedItemAction} onClick={markAsRemoved}>
                <p className={styles.removedItemActionLine} tabIndex={0} onKeyDown={handleKeyPress}>
                    <Icon icon="action-add" size="xs" />
                    <InspireLinkButton linkType="secondary">ADD THIS ITEM BACK TO YOUR BAG</InspireLinkButton>
                </p>
            </div>
        </div>
    );
}

export default BagItem;
