import React from 'react';
import { IProductFields } from '../../../../@generated/@types/contentful';
import { InspireCmsEntry } from '../../../../common/types';

interface NonexistentItemProps {
    contentfulProduct: InspireCmsEntry<IProductFields>;
    quantity: number;
}

const NonexistentItem: React.FC<NonexistentItemProps> = (): null => null;

export default NonexistentItem;
