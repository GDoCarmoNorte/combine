import React, { useCallback, useEffect, useRef } from 'react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { Modal } from '@material-ui/core';
import classnames from 'classnames';
import { parseISO, isAfter } from '../../../common/helpers/dateTime';

import { TallyProductModel, TServiceTypeModel } from '../../../@generated/webExpApi';

import {
    useAccount,
    useBag,
    useConfiguration,
    useDomainMenu,
    useGlobalProps,
    useOrderLocation,
    usePageLoader,
    useSelectedSell,
} from '../../../redux/hooks';
import { formatPrice } from '../../../lib/domainProduct';
import Icon from '../../atoms/BrandIcon';
import { useDomainProducts, useTallyItemsWithPricesAndCalories } from '../../../redux/hooks/domainMenu';
import NotFound from '../../molecules/notFound';
import useTallyOrder from '../../../redux/hooks/useTallyOrder';
import { actions } from '../../../redux/tallyOrder';
import { GtmErrorCategory, GtmErrorEvent } from '../../../common/services/gtmService/types';
import { GTM_CHECKOUT_VIEW, GTM_USER_ID, GTM_ERROR_EVENT } from '../../../common/services/gtmService/constants';
import {
    getTallyBagEntries,
    getTallyBagProductIds,
    isAvailableProductByTally,
    isWholeItemUnavailable,
    isWholeItemUnavailableByModifiers,
} from '../../../common/helpers/bagHelper';
import { useAppDispatch } from '../../../redux/store';
import { requestNavIntercept } from '../../../redux/navIntercept';
import BagContent from './bagContent';
import { getCheckoutEntries, hasCheckoutEntries } from './bagModel';
import { NetworkErrorMessage } from '../../../common/services/createErrorWrapper';
import { resolveOpeningHours } from '../../../lib/locations';
import { usePrevious } from '../../../common/hooks/usePreviousState';
import isMobileScreen from '../../../lib/isMobileScreen';
import isSmallScreen from '../../../lib/isSmallScreen';
import { isRewardsOn, isShoppingBagLocationSelectComponentEnabled } from '../../../lib/getFeatureFlags';
import LocationSelect from '../navigation/locationSelect';

import styles from './index.module.css';
import { ITEMS_NOT_AVAILABLE_FOR_LOCATION, ORDER_NOT_AVAILABLE_FOR_LOCATION } from '../../../common/constants/bag';
import BagLoyaltyBanner from './bagLoyaltyBanner';
import useBagAvailableItemsCounter from '../../../common/hooks/useBagAvailableItemsCounter';
import { SHOW_NUMBER_ITEMS } from './constants';
import { useUnavailableItems } from '../../../common/hooks/useUnavailableItems';
import { useBagRenderItems } from '../../../common/hooks/useBagRenderItems';

const BagButtons = dynamic(import('./bagButtons'), {
    ssr: false,
    // eslint-disable-next-line react/display-name
    loading: () => null,
});

enum TooltipTypes {
    ORDER_NOT_AVAILABLE_FOR_LOCATION,
    ITEMS_NOT_AVAILABLE_FOR_LOCATION,
}

function Bag(): JSX.Element {
    const bag = useBag();
    const router = useRouter();
    const dispatch = useAppDispatch();
    const globalProps = useGlobalProps();
    const { isLoading, submitTallyOrder, error, unavailableItems, setUnavailableTallyItems } = useTallyOrder();
    const allItemsIds = getTallyBagProductIds(bag.bagEntries);
    const domainProducts = useDomainProducts(allItemsIds);
    const { account } = useAccount();
    const smallScreen = isSmallScreen();
    const mobileScreen = isMobileScreen();
    const suggestedSell = useSelectedSell();
    const ref = useRef<HTMLDivElement>(null);

    const isLocationSelectComponentEnabled = isShoppingBagLocationSelectComponentEnabled();
    const bagAvailableCount = useBagAvailableItemsCounter();
    const {
        currentLocation: location,
        isCurrentLocationOAAvailable,
        isPickUp,
        method,
        pickupAddress,
        deliveryAddress,
    } = useOrderLocation();

    const { unavailableItems: unavailableTallyItems } = useUnavailableItems(unavailableItems);

    const {
        configuration: { isOAEnabled },
    } = useConfiguration();

    useEffect(() => {
        bag.bagEntries.forEach((item) => {
            if (!(globalProps && globalProps.productsById && globalProps.productsById[item.productId])) {
                bag.actions.removeFromBag({ lineItemId: item.lineItemId });
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    // eslint-disable-next-line testing-library/render-result-naming-convention
    const renderItems = useBagRenderItems();

    const isCheckoutDisabledByTally = renderItems.some(
        (item) => !isAvailableProductByTally(item, unavailableTallyItems)
    );

    const numberOfUnavailableItems = renderItems
        .filter(
            (item) =>
                !item.isAvailable ||
                isWholeItemUnavailableByModifiers(item, unavailableTallyItems) ||
                isWholeItemUnavailable(item, unavailableTallyItems)
        )
        .reduce((acc, curr) => acc + curr.entry.quantity, 0);

    const checkoutEntries = getCheckoutEntries(renderItems, unavailableTallyItems);

    const {
        error: menuError,
        loading: isMenuLoading,
        actions: { getDomainMenu },
    } = useDomainMenu();

    const bagItemPrices = useTallyItemsWithPricesAndCalories(checkoutEntries) || [];

    const bagEmpty = bag.bagEntries.length === 0;
    const { image, description, header } = globalProps.emptyShoppingBag.fields;

    const totalPrice = bagItemPrices.reduce((acc, curr) => acc + curr.productData.price * curr.quantity, 0);
    const subTotal = bagItemPrices.reduce((acc, curr) => acc + curr.productData.totalPrice * curr.quantity, 0);

    const maxOrderAmount = location?.additionalFeatures?.maxOrderAmount;
    const isOverLimitForOrder = subTotal >= maxOrderAmount;
    const totalDiscount = formatPrice(subTotal) !== formatPrice(totalPrice) && formatPrice(subTotal - totalPrice);

    const toggleBag = () => bag.actions.toggleIsOpen({ isOpen: !bag.isOpen });

    const {
        actions: { showLoader, hideLoader },
    } = usePageLoader();

    useEffect(() => {
        if (checkoutEntries.length > 0 && bag.isOpen && suggestedSell.correlationId) {
            suggestedSell.actions.fetchRecommendedProducts({
                locationId: location.id,
                correlationId: suggestedSell.correlationId,
                productIds: checkoutEntries.reduce(
                    (productIdList, current) => [...productIdList, ...Array(current.quantity).fill(current.productId)],
                    []
                ),
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [bag.isOpen, suggestedSell.correlationId, checkoutEntries.length, bag.bagEntriesCount]);

    useEffect(() => {
        if (isMenuLoading === false && !menuError) {
            dispatch(bag.actions.updateBagItems());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isMenuLoading]);

    const resolvedOpeningHours = resolveOpeningHours(location, TServiceTypeModel.OrderAhead);
    const storeIsOpenNow = !!resolvedOpeningHours && resolvedOpeningHours.isOpen;

    const {
        actions: { setPickupTime, initPickupTimeValues, resetPickupTime, resetDeliveryTime },
        pickupTimeValues,
        orderTime,
        orderTimeType,
    } = bag;

    const { id: pickupAddressId } = pickupAddress || {};
    const { id: deliveryAddressId } = deliveryAddress?.locationDetails || {};

    const prevPickupAddressId = usePrevious(pickupAddressId);
    const prevDeliveryAddressId = usePrevious(deliveryAddressId);
    const userAgent = typeof window !== 'undefined' && navigator?.userAgent;

    useEffect(() => {
        const openingHours = pickupAddress && resolveOpeningHours(pickupAddress, TServiceTypeModel.Pickup);

        if (prevPickupAddressId && prevPickupAddressId !== pickupAddressId) {
            resetPickupTime();
        }

        if (openingHours) {
            initPickupTimeValues({
                openedTimeRanges: openingHours.openedTimeRanges,
                timezone: openingHours.storeTimezone,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pickupAddressId]);

    useEffect(() => {
        if (prevDeliveryAddressId && prevDeliveryAddressId !== deliveryAddressId) {
            resetDeliveryTime();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [deliveryAddressId]);

    const isInitialRender = useRef(true);

    useEffect(() => {
        if (!isInitialRender.current) {
            bag.actions.toggleIsOpen({ isOpen: false });
        } else {
            isInitialRender.current = false;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [router.pathname]);

    const onCheckoutClick = useCallback(() => {
        let time = orderTime && new Date(orderTime);

        // if store close and pickup time not choose before
        if (!storeIsOpenNow && !orderTime && pickupTimeValues) {
            const firstAvailableDate = pickupTimeValues[0].timeRange[0];

            time = new Date(firstAvailableDate);
            setPickupTime({ time: firstAvailableDate, method });
        }

        const currentDate = new Date(Date.now());
        //if previously selected future time passed for Delivery
        if (!isPickUp && orderTime && orderTimeType === 'future' && isAfter(currentDate, parseISO(orderTime))) {
            time = null;
        }

        dispatch(requestNavIntercept()).then(({ payload: result }) => {
            if (result) {
                bag.actions.clearLastRemovedLineItemId();
                showLoader();
                submitTallyOrder(
                    getTallyBagEntries(checkoutEntries, domainProducts, location, time),
                    time,
                    bag?.dealId,
                    account?.idpCustomerId
                )
                    .then((action) => {
                        switch (action.type) {
                            case actions.fulfilled.type: {
                                toggleBag();
                                router.push('/checkout');
                                dispatch({ type: GTM_CHECKOUT_VIEW, payload: bag.bagEntries });
                                dispatch({
                                    type: GTM_USER_ID,
                                    payload: {
                                        userID: account ? account?.idpCustomerId : suggestedSell.correlationId,
                                        isLoggedIn: !!account,
                                        userAgent,
                                    },
                                });
                                const onRouterComplete = () => {
                                    hideLoader();
                                    router.events.off('routeChangeComplete', onRouterComplete);
                                };

                                const onRouterError = () => {
                                    hideLoader();
                                    router.events.off('routeChangeError', onRouterError);
                                };

                                router.events.on('routeChangeComplete', onRouterComplete);
                                router.events.on('routeChangeError', onRouterError);
                                setUnavailableTallyItems([]);
                                break;
                            }
                            case actions.invalid.type: {
                                if (action.payload.message !== NetworkErrorMessage) {
                                    getDomainMenu(location);
                                }
                                hideLoader();
                                break;
                            }
                            case actions.setUnavailableItems.type: {
                                hideLoader();
                                break;
                            }
                            default:
                                break;
                        }
                    })
                    .catch(() => {
                        hideLoader();
                    });
            } else {
                bag.actions.toggleIsOpen({ isOpen: false });
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [bag, orderTime]);

    const onRemoveUnavailable = (unavailableEntries: TallyProductModel[]) => {
        bag.actions.removeAllFromBag({ lineItemIdList: unavailableEntries.map((entry) => entry.lineItemId) });
    };

    const noCheckoutEntries = !isMenuLoading && hasCheckoutEntries(renderItems, unavailableTallyItems) === false;
    const isOrderAheadAvailable = isCurrentLocationOAAvailable;

    const getTooltipType = (): TooltipTypes | null => {
        if (error) {
            if (!isOrderAheadAvailable) {
                return TooltipTypes.ORDER_NOT_AVAILABLE_FOR_LOCATION;
            }
            if (noCheckoutEntries) {
                return TooltipTypes.ITEMS_NOT_AVAILABLE_FOR_LOCATION;
            }
            return null;
        }

        return null;
    };

    const tooltipMap = {
        [TooltipTypes.ORDER_NOT_AVAILABLE_FOR_LOCATION]: {
            message: ORDER_NOT_AVAILABLE_FOR_LOCATION,
            onDismiss: null,
            dismiss: false,
        },
        [TooltipTypes.ITEMS_NOT_AVAILABLE_FOR_LOCATION]: {
            message: ITEMS_NOT_AVAILABLE_FOR_LOCATION,
            onDismiss: null,
            dismiss: false,
        },
    };

    const tooltip = tooltipMap[getTooltipType()];

    useEffect(() => {
        if (isOverLimitForOrder) {
            const payload = {
                ErrorCategory: GtmErrorCategory.BAG_ERROR_MAX_TOTAL,
                ErrorDescription: `Online orders over ${maxOrderAmount} cannot be placed. Please call the restaurant to help fulfill your order: ${location.contactDetails.phone}`,
            } as GtmErrorEvent;

            dispatch({ type: GTM_ERROR_EVENT, payload });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isOverLimitForOrder, maxOrderAmount]);

    return (
        <div className={styles.container}>
            <Modal className={styles.modal} open={bag.isOpen} onClose={toggleBag} aria-label="Bag">
                <div className={styles.content}>
                    <button className={classnames('t-subheader-small', styles.closeButton)} onClick={toggleBag}>
                        <Icon
                            icon="action-close"
                            size={smallScreen || mobileScreen ? 's' : 'm'}
                            className={styles.closeButtonIcon}
                        />
                        CLOSE BAG
                    </button>
                    {isLocationSelectComponentEnabled && (
                        <div className={styles.locationSelectWrapper} ref={ref}>
                            <LocationSelect
                                className={styles.locationSelect}
                                infoClassName={styles.locationSelectInfo}
                                locationBlockClassName={styles.locationSelectLocationBlock}
                                iconClassName={styles.locationSelectIcon}
                                chevronIconClassName={styles.locationSelectChevronIcon}
                                containerClassName={styles.locationSelectContainer}
                                locationDropdownClassName={styles.bagNavigationDropdownContent}
                                locationDropdownModalStyle={{}}
                                locationDropdownModalClassName={styles.bagNavigationDropdownModal}
                                disablePortal={false}
                                navRef={ref}
                            />
                        </div>
                    )}
                    {!!numberOfUnavailableItems && !isMenuLoading && (
                        <div className={classnames('t-paragraph-small-strong', styles.unavailableNotification)}>
                            {`You have ${numberOfUnavailableItems} unavailable item${
                                numberOfUnavailableItems > 1 ? 's' : ''
                            } in your bag.`}
                        </div>
                    )}
                    <div
                        className={classnames(styles.bag, {
                            [styles.bagEmpty]: bagEmpty,
                        })}
                    >
                        {!isMenuLoading && (
                            <div className={styles.headerContainer}>
                                <div className={styles.yourBagHeaderContainer}>
                                    <h2 className={classnames('t-header-h1', styles.header)}>MY BAG</h2>
                                    {SHOW_NUMBER_ITEMS && bagAvailableCount > 0 && (
                                        <span
                                            aria-label="bag item count"
                                            className={styles.itemCount}
                                        >{`(${bagAvailableCount} ITEM${bagAvailableCount === 1 ? '' : 'S'})`}</span>
                                    )}
                                </div>
                                {!bagEmpty && (
                                    <div className={styles.total}>
                                        <span className={classnames('t-paragraph-hint', styles.totalHint)}>TOTAL</span>
                                        <span
                                            aria-label="bag total price"
                                            className={classnames('t-subheader-universal', styles.priceHint)}
                                        >
                                            {formatPrice(totalPrice)}
                                        </span>
                                    </div>
                                )}
                            </div>
                        )}
                        {isRewardsOn() && <BagLoyaltyBanner />}
                        {bagEmpty && (
                            <div className={styles.bagItemsEmpty}>
                                <NotFound icon={image} heading={header} body={description} />
                            </div>
                        )}
                        {!bagEmpty && (
                            <BagContent
                                isMenuLoading={isMenuLoading}
                                totalPrice={formatPrice(totalPrice)}
                                renderItems={renderItems}
                                onRemoveUnavailable={onRemoveUnavailable}
                                subTotal={formatPrice(subTotal)}
                                totalDiscount={totalDiscount}
                                tooltipText={
                                    <span>
                                        Online orders over ${maxOrderAmount} cannot be placed. Please call the
                                        restaurant to help fulfill your order: &nbsp;
                                        <a className={styles.phoneLink} href={`tel:${location.contactDetails.phone}`}>
                                            {location.contactDetails.phone}
                                        </a>
                                    </span>
                                }
                                isOverLimitForOrder={isOverLimitForOrder}
                                unavailableTallyItems={unavailableTallyItems}
                            />
                        )}
                    </div>
                    <BagButtons
                        location={location}
                        isLoading={isLoading || isMenuLoading}
                        renderItems={renderItems}
                        toggleBag={toggleBag}
                        onCheckoutClick={onCheckoutClick}
                        noCheckoutEntries={noCheckoutEntries}
                        tooltipText={tooltip?.message}
                        onTooltipDismiss={tooltip?.onDismiss}
                        tooltipDismiss={tooltip?.dismiss}
                        isOverLimitForOrder={isOverLimitForOrder}
                        isCheckoutDisabledByTally={isCheckoutDisabledByTally}
                        disabled={!isOAEnabled}
                    />
                </div>
            </Modal>
        </div>
    );
}

export default Bag;
