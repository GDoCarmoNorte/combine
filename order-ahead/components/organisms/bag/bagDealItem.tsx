import React from 'react';
import classnames from 'classnames';
import { isValid, format } from '../../../common/helpers/dateTime';

import { useBag, useRewards } from '../../../redux/hooks';

import styles from './item.module.css';

interface IDealItemProps {
    dealId: string;
}

function BagDealItem(props: IDealItemProps): JSX.Element {
    const { dealId } = props;

    const { getOfferById } = useRewards();
    const bag = useBag();
    const offer = getOfferById(dealId);

    if (!offer) return null;

    const expiresDate = isValid(new Date(offer?.endDateTime)) ? format(new Date(offer?.endDateTime), 'MM/dd') : '';

    const handleRemoveDeal = () => {
        bag.actions.removeDealFromBag();
    };

    return (
        <div className={styles.productDealContainer}>
            <div className={styles.imgColumn}>
                <img src={offer.imageUrl} className={styles.dealImage} alt="Deal" />
            </div>
            <div className={styles.dealInfoContainer}>
                <span className={classnames('t-subheader-small', 'truncate', styles.offerHeader)}>{offer.name}</span>
                <span className={styles.offerDateHeader}>{`Expires ${expiresDate}`}</span>
                <div className={styles.offerButtonSection}>
                    <span
                        className={classnames('link-secondary-active', styles.offerButtonRemove)}
                        onClick={handleRemoveDeal}
                    >
                        Remove
                    </span>
                </div>
            </div>
        </div>
    );
}

export default BagDealItem;
