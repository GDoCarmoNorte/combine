import React, { useState, FormEvent, useCallback } from 'react';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import classNames from 'classnames';

import styles from './nutrition.module.css';

import { InspireTabSet, InspireTab, InspireTabPanel } from '../../atoms/tabset';
import TextWithTrademark from '../../atoms/textWithTrademark';
import ProductNutritionLink from '../../atoms/ProductNutritionLink';
import { INutritionInfo, INutritionInfoSizeSelection } from '../../../redux/types';
import { IProductNutritionLinkFields } from '../../../@generated/@types/contentful';

interface INutritionProductContentProps {
    sizeSelection: INutritionInfoSizeSelection;
}

function NutritionProductContent(props: INutritionProductContentProps): JSX.Element {
    const { nutritionalFacts, allergicInformation } = props.sizeSelection;

    return (
        <div className={styles.nutrition}>
            {nutritionalFacts && (
                <div className={styles.nutritionTable}>
                    {nutritionalFacts.map((field) => (
                        <div className={classNames(styles.nutritionRow, 't-paragraph')} key={field.label}>
                            <span>{field.label}</span>
                            <span>{field.weight?.value}</span>
                        </div>
                    ))}
                </div>
            )}
            {allergicInformation && (
                <div className={styles.contains}>
                    <span>{allergicInformation}</span>
                </div>
            )}
        </div>
    );
}

const NutritionProductWithSizing = (props: { sizeSelections: INutritionInfoSizeSelection[] }): JSX.Element => {
    const [activeTab, setActiveTab] = useState<number>(0);
    const { sizeSelections } = props;

    const handleChangeTab = (event: FormEvent<HTMLButtonElement>, newValue?: number): void => {
        setActiveTab(newValue);
    };

    return (
        <div>
            <InspireTabSet value={activeTab} onChange={handleChangeTab}>
                {sizeSelections.map((item, index) => {
                    return (
                        item.name && (
                            <InspireTab
                                key={index}
                                label={item.name}
                                id={`size-tab-${index}`}
                                aria-controls={`size-tabpanel-${index}`}
                            />
                        )
                    );
                })}
            </InspireTabSet>
            {sizeSelections.map((item, index) => (
                <InspireTabPanel
                    value={activeTab}
                    index={index}
                    key={index}
                    id={`size-tabpanel-${index}`}
                    aria-labelledby={`size-tab-${index}`}
                >
                    <NutritionProductContent key={item.name} sizeSelection={item} />
                </InspireTabPanel>
            ))}
        </div>
    );
};

function NutritionProduct(props: { sizeSelections: INutritionInfo['sizeSelections'] }): JSX.Element {
    const { sizeSelections } = props;

    if (!sizeSelections?.length) {
        return null;
    }

    return sizeSelections.length > 1 ? (
        <NutritionProductWithSizing sizeSelections={sizeSelections} />
    ) : (
        <NutritionProductContent sizeSelection={sizeSelections[0]} />
    );
}

interface IDropdownProps {
    nutritionInfo: INutritionInfo;
}

function NutritionDropdown({ nutritionInfo }: IDropdownProps) {
    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);
    const { displayName, sizeSelections } = nutritionInfo;

    const toggleDropdown = (): void => {
        setIsCollapsed(!isCollapsed);
    };

    const toggleDropdownKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === 'Enter') {
            toggleDropdown();
        }
    };

    return (
        <div className={classNames(styles.nutritionDropdown, !isCollapsed ? styles.activeDropdown : undefined)}>
            <div
                onClick={toggleDropdown}
                tabIndex={0}
                onKeyPress={toggleDropdownKeyPress}
                className={styles.nutritionDropdownHeaderWrapper}
            >
                <div className={styles.nutritionDropdownHeader} tabIndex={-1}>
                    <TextWithTrademark
                        tag="div"
                        className={classNames(
                            { [styles.expandedHeader]: !isCollapsed },
                            styles.nutritionDropdownHeaderText,
                            't-subheader-small'
                        )}
                        text={displayName}
                    />
                    <div className={styles.nutritionDropdownChevron}>
                        {isCollapsed ? <ExpandMoreIcon /> : <ExpandLessIcon />}
                    </div>
                </div>
            </div>
            {!isCollapsed && <NutritionProduct sizeSelections={sizeSelections} />}
        </div>
    );
}

export default function Nutrition(props: {
    nutritionInfo: INutritionInfo[];
    isPromo?: boolean;
    onHide?: () => void;
    nutritionLink: IProductNutritionLinkFields | null;
}): JSX.Element {
    const { nutritionInfo, isPromo, onHide, nutritionLink } = props;

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);

    const toggleAccordion = useCallback(() => {
        setIsCollapsed(!isCollapsed);
    }, [isCollapsed]);

    const handleHide = useCallback(() => {
        toggleAccordion();
        if (onHide) {
            onHide();
        }
    }, [onHide, toggleAccordion]);

    const toggleAccordionKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            toggleAccordion();
        }
    };

    return (
        <div className={styles.container} aria-label="Nutrition Information">
            {isCollapsed ? (
                <div
                    className={classNames(styles.nutritionLink, 'link-secondary-active')}
                    tabIndex={0}
                    onClick={toggleAccordion}
                    onKeyPress={toggleAccordionKeyPress}
                >
                    <ExpandMoreIcon />
                    <div className="t-link">View nutrition information</div>
                </div>
            ) : (
                <>
                    {nutritionInfo.length === 1 && !isPromo ? (
                        // NOT A COMBO
                        <NutritionProduct sizeSelections={nutritionInfo[0].sizeSelections} />
                    ) : (
                        // COMBO
                        <div className={styles.nutritionDropdownSection}>
                            {nutritionInfo.map((product) => (
                                <NutritionDropdown key={product.displayName} nutritionInfo={product} />
                            ))}
                        </div>
                    )}
                    <ProductNutritionLink productNutrition={nutritionLink} />
                    <div
                        tabIndex={0}
                        onClick={handleHide}
                        onKeyPress={toggleAccordionKeyPress}
                        className={classNames(styles.nutritionLink, 'link-secondary-active')}
                    >
                        <ExpandLessIcon />
                        <div className="t-link">Hide nutrition information</div>
                    </div>
                </>
            )}
        </div>
    );
}
