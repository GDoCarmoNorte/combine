import React from 'react';
import styles from './index.module.css';
import { IAccountHeader } from '../../../@generated/@types/contentful';
import classnames from 'classnames';

interface IAccountHeaderProps {
    accountHeader: IAccountHeader;
}

const AccountHeader = (props: IAccountHeaderProps): JSX.Element => {
    const { accountHeader } = props;
    const {
        fields: { backgroundColor, backgroundImage },
    } = accountHeader;
    const bgImageUrl = backgroundImage?.fields?.file?.url;
    const bgColorHex = backgroundColor?.fields?.hexColor;
    return (
        <div className={classnames('accountHeader', styles.accountHeader)}>
            <h1 className={classnames('t-header-h1', styles.accountHeaderTitle)}>my account</h1>
            <style jsx>{`
                .accountHeader {
                    background-color: ${bgColorHex ? `#${bgColorHex}` : 'transparent'};
                    ${bgImageUrl
                        ? `background-image: url("${bgImageUrl}");
                                background-repeat: no-repeat;
                                background-size: 100% 100%;
                                background-position: center;`
                        : ``}
                }
            `}</style>
        </div>
    );
};

export default AccountHeader;
