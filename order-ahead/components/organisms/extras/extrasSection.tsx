import React, { useState } from 'react';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import classNames from 'classnames';

import styles from './extras.module.css';

import TextWithTrademark from '../../atoms/textWithTrademark';
import { IExtrasSectionProps } from './types';
import { ExtrasCardContainer } from '../extrasCardContainer/extrasCardContainer';
import { useChangedModifiersForChildItem } from '../../../redux/hooks/pdp';

export default function ExtrasSection(props: IExtrasSectionProps): JSX.Element {
    const { productGroup, sectionIndex } = props;

    const [isCollapsed, setIsCollapsed] = useState<boolean>(true);

    const { addedModifiers, removedDefaultModifiers } = useChangedModifiersForChildItem(sectionIndex, true);

    const toggleDropdown = (): void => {
        setIsCollapsed(!isCollapsed);
    };

    const toggleDropdownKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (e.key === 'Enter') {
            toggleDropdown();
        }
    };

    return (
        <>
            <div className={styles.extrasDropdown}>
                <div
                    onClick={toggleDropdown}
                    tabIndex={0}
                    onKeyPress={toggleDropdownKeyPress}
                    className={styles.extrasDropdownHeaderWrapper}
                >
                    <div className={styles.extrasDropdownHeader} tabIndex={-1}>
                        <TextWithTrademark
                            tag="div"
                            className={classNames(styles.extrasDropdownHeaderText, 't-subheader-small')}
                            text={productGroup.displayName}
                        />
                        <div className={styles.extrasDropdownChevron}>
                            {isCollapsed ? <ExpandMoreIcon /> : <ExpandLessIcon />}
                        </div>
                    </div>
                </div>
            </div>
            {!isCollapsed && (
                <div className={styles.extrasCardsWrapper}>
                    {productGroup.products.map((item) => (
                        <div key={item.id}>
                            <ExtrasCardContainer
                                key={item.id}
                                item={item}
                                sectionIndex={sectionIndex}
                                addedModifiers={addedModifiers}
                                removedDefaultModifiers={removedDefaultModifiers}
                            />
                        </div>
                    ))}
                </div>
            )}
        </>
    );
}
