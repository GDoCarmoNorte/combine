import { IExtraProductGroup } from '../../../redux/types';

export interface IExtrasSectionProps {
    productGroup: IExtraProductGroup;
    sectionIndex: number;
}
