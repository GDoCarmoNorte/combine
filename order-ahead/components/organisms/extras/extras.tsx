import React, { FC } from 'react';

import styles from './extras.module.css';

import { useExtraProducts } from '../../../redux/hooks/pdp';
import ExtrasSection from './extrasSection';
import SectionHeader from '../../atoms/sectionHeader';

interface IExtrasProps {
    className?: string;
}

const Extras: FC<IExtrasProps> = ({ className }) => {
    const sections = useExtraProducts();

    if (!sections?.length) return null;

    return (
        <div className={className}>
            <SectionHeader text="Add extras" tag="h2" textClassName="t-header-h2" />
            <div className={styles.container} aria-label="Add Extras">
                <div className={styles.extrasDropdownSection}>
                    {sections.map((section, index) => (
                        <ExtrasSection key={section.displayName} sectionIndex={index} productGroup={section} />
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Extras;
