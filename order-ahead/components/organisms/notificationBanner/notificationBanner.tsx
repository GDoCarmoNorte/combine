import React from 'react';
import classNames from 'classnames';

import styles from './notificationBanner.module.css';
import { Entry } from 'contentful';
import { INotificationBannerFields } from '../../../@generated/@types/contentful';
import { getGtmIdByName } from '../../../lib/gtm';
import { getImageUrl } from '../../../common/helpers/contentfulImage';

interface INotificationBannerProps {
    entry: Entry<INotificationBannerFields>;
}

export default function NotificationBanner(props: INotificationBannerProps): JSX.Element {
    const fields = props.entry.fields;
    if (!fields) return null;

    const { text, icon, backgroundColor } = fields;
    const bgColorHex = backgroundColor?.fields?.hexColor;
    const gtmId = getGtmIdByName('notificationBanner', text);

    return (
        <div data-gtm-id={gtmId} role="banner" className={classNames(styles.container, 'backgroundBanner')}>
            <div className={classNames(styles.wrapper)} style={{ backgroundImage: `url(${getImageUrl(icon)})` }}>
                {<p className={classNames('t-paragraph-hint-strong', styles.message)}>{text}</p>}
            </div>
            <style jsx>{`
                .backgroundBanner {
                    background-color: ${bgColorHex ? `#${bgColorHex}` : 'var(--col--gray1)'};
                }
            `}</style>
        </div>
    );
}
