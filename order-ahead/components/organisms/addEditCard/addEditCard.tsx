import React, { useEffect } from 'react';
import { useWallet } from '../../../common/hooks/useWallet';
import AddEditFormWithFreedomPay from './components/addEditFormWithFreedomPay';
import useAddCardToWallet from '../../../common/hooks/useAddCardToWallet';
import { IAddCreditCardRequestModel } from '../../../@generated/webExpApi';
import { useNotifications } from '../../../redux/hooks';

const AddEditCard = ({
    customerId,
    closeOverlay,
    jwtToken,
    handleFetchPaymentMethods,
    handleSetLoading,
}: {
    customerId: string;
    closeOverlay: () => void;
    jwtToken: string;
    handleFetchPaymentMethods: () => void;
    handleSetLoading: (boolean: boolean) => void;
}): JSX.Element => {
    const { wallet } = useWallet();
    const innerHtml = wallet && { __html: wallet.iframeHtml };
    const { addCardToWallet, isLoading, error } = useAddCardToWallet();
    const {
        actions: { enqueueError },
    } = useNotifications();

    const addCard = (formData: IAddCreditCardRequestModel, reset: () => void) => {
        const request = { ...formData, ...{ customerId } };
        handleSetLoading(true);
        addCardToWallet(request, jwtToken)
            .then(() => {
                closeOverlay();
                handleFetchPaymentMethods();
            })
            .catch(() => {
                reset();
            });
    };

    useEffect(() => {
        error && enqueueError({ message: error.message });
    }, [error, enqueueError]);

    return (
        <>
            <AddEditFormWithFreedomPay
                innerHtml={innerHtml}
                wallet={wallet}
                addCard={addCard}
                closeOverlay={closeOverlay}
                isLoading={isLoading}
            />
        </>
    );
};

export default AddEditCard;
