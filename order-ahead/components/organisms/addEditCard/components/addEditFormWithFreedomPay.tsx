import React, { useEffect, useState } from 'react';
import styles from './addEditFormWithFreedomPay.module.css';
import { Card, CardContent, CardHeader, Typography } from '@material-ui/core';
import FormikInput from '../../formikInput';
import { Form, Formik } from 'formik';
import { createConnector } from '../../../clientOnly/formik/connector';
import { noop } from 'lodash';
import { useFreedomPay } from '../../../../common/hooks/useFreedomPay';
import classNames from 'classnames';
import { IAddCreditCardRequestModel, IPaymentInitResponseModel } from '../../../../@generated/webExpApi';
import BrandLoader from '../../../atoms/BrandLoader';
import { validateRequire } from '../../../../common/helpers/complexValidateHelper';
import { getCreditCardIssuerPrefixFromFreedomPayName } from '../../../../common/helpers/creditCardIssuerMapper';
import Icon from '../../../atoms/BrandIcon';

interface IAddEditCard {
    nameCard: string;
    chFirstName: string;
    chLastName: string;
}

interface IAddEditForWithFreedomPay {
    innerHtml: { __html: string };
    wallet: IPaymentInitResponseModel;
    addCard: (formData: IAddCreditCardRequestModel, reset: () => void) => void;
    closeOverlay: () => void;
    isLoading: boolean;
}

const FormikConnector = createConnector<IAddEditCard>();

const validateFirstName = validateRequire('First Name');
const validateLastName = validateRequire('Last Name');
const AddEditFormWithFreedomPay = ({
    innerHtml,
    wallet,
    addCard,
    closeOverlay,
    isLoading,
}: IAddEditForWithFreedomPay): JSX.Element => {
    const initial = {
        nameCard: '',
        chFirstName: '',
        chLastName: '',
    } as IAddEditCard;

    const [paymentInfo, setPaymentInfo] = useState<IAddEditCard>({} as IAddEditCard);
    const [formFieldsIsValid, setFormFieldsIsValid] = useState<boolean>(false);

    const onFormChange = (values: IAddEditCard, isValid: boolean) => {
        setFormFieldsIsValid(isValid);
        setPaymentInfo(values);
    };

    const { payment: paymentToken, errors: freedomPayErrors, isValid: isFreedomPayValid, reset } = useFreedomPay();

    const freedomPayEnabled = isFreedomPayValid && (!paymentToken || (freedomPayErrors && freedomPayErrors.length > 0));
    const isPayButtonEnabled = freedomPayEnabled && formFieldsIsValid;

    useEffect(() => {
        const keys = paymentToken && paymentToken.keys;
        if (keys && keys.length > 0 && paymentToken.cardIssuer) {
            const type = getCreditCardIssuerPrefixFromFreedomPayName(paymentToken.cardIssuer);
            const requestForm = {
                label: paymentInfo.nameCard,
                isPreferred: false,
                sessionKey: wallet.sessionKey,
                paymentKey: keys[0],
                type,
                cardHolderName: paymentInfo.chFirstName + ' ' + paymentInfo.chLastName,
            } as IAddCreditCardRequestModel;
            addCard(requestForm, reset);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [paymentToken]);

    const overlayClasses = classNames(styles.paymentOverlay, {
        [styles.paymentOverlayHidden]: isPayButtonEnabled && !isLoading,
    });

    const overlayButtonRef = React.useRef<HTMLDivElement>(null);
    const outsideRef = React.useRef<HTMLDivElement>(null);

    const focusOverlayButton = () => {
        outsideRef.current.focus();
    };

    const clickOverlayButton = () => {
        focusOverlayButton();
        overlayButtonRef.current.click();
    };

    return (
        <Card className={styles.cardContainer} data-testid={'addCardContainer'}>
            <CardHeader
                action={
                    <Icon
                        id={'close-button'}
                        aria-label={'Close Button'}
                        onClick={closeOverlay}
                        className={styles.iconClose}
                        icon="action-close"
                        role="button"
                    />
                }
                title={<Typography className={styles.overlayTitle}>New Payment Method</Typography>}
            />

            <CardContent>
                <div className={styles.container}>
                    <div className={styles.formWrapper}>
                        <Formik initialValues={initial} onSubmit={noop} validateOnMount>
                            {() => (
                                <Form>
                                    <FormikInput
                                        label={'Name Card'}
                                        optional
                                        name={'nameCard'}
                                        labelClassName={classNames(styles.fieldLabel, styles.optionalField)}
                                        inputClassName={styles.fieldInput}
                                        placeholder="Name this card"
                                    />
                                    <div className={styles.formBackground}>
                                        <div className={styles.formSpacing} style={{ padding: '1rem 0 0.5rem 0' }}>
                                            <FormikInput
                                                label={'Card Holder First Name'}
                                                name={'chFirstName'}
                                                labelClassName={styles.fieldLabel}
                                                inputClassName={styles.fieldInput}
                                                placeholder="First Name"
                                                validate={validateFirstName}
                                            />
                                            <FormikInput
                                                label={'Card Holder Last Name'}
                                                name={'chLastName'}
                                                labelClassName={styles.fieldLabel}
                                                inputClassName={styles.fieldInput}
                                                placeholder="Last Name"
                                                validate={validateLastName}
                                            />
                                        </div>
                                    </div>
                                    <FormikConnector onChange={onFormChange} />
                                </Form>
                            )}
                        </Formik>
                        <div className={styles.paymentContainer} dangerouslySetInnerHTML={innerHtml} />
                        <div
                            ref={overlayButtonRef}
                            className={overlayClasses}
                            onMouseOver={focusOverlayButton}
                            onTouchStart={clickOverlayButton}
                            aria-label="Save button disable overlay"
                            aria-disabled={!isPayButtonEnabled}
                        />
                        {/* added to catch focus and revalidate FreedomPay form when hovering over or touching the overlay button*/}
                        <div tabIndex={0} ref={outsideRef} />
                        {isLoading && <BrandLoader className={styles.brandLoader} />}
                    </div>
                </div>
            </CardContent>
        </Card>
    );
};

export default AddEditFormWithFreedomPay;
