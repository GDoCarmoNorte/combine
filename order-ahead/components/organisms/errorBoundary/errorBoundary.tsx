import React, { ErrorInfo, ReactNode } from 'react';
import logger from '../../../common/services/logger';

import styles from './errorBoundary.module.css';

interface ErrorBoundaryProps {
    children: ReactNode;
}

interface ErrorBoundaryState {
    hasError: boolean;
    error: string;
}
export default class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
    constructor(props) {
        super(props);
        this.state = { hasError: false, error: '' };
    }

    static getDerivedStateFromError(error: string): ErrorBoundaryState {
        return { hasError: true, error: error.toString() };
    }

    componentDidCatch(error: Error, info: ErrorInfo): void {
        logger.logError(error, { ...info });
        logger.logEvent('error_boundary', {
            error,
            ...info,
        });
    }

    render() {
        if (this.state.hasError) {
            return (
                <div className={styles.container}>
                    <h4 className="t-header-h3">Something went wrong.</h4>
                    <p>We are working on a solution to fix this problem.</p>
                </div>
            );
        }

        return this.props.children;
    }
}
