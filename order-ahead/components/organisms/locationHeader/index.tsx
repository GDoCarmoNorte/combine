import React, { useCallback, useMemo } from 'react';
import styles from './locationHeader.module.css';
import Icon from '../../atoms/BrandIcon';
import { InspireButton } from '../../atoms/button';
import { ILocationByStateOrProvinceDetailsModel } from '../../../@generated/webExpApi';
import { useRouter } from 'next/router';
import { formatPhoneNumber } from '../../../lib/formatPhoneNumber';
import { getLocationAddressDetailsString } from '../../../lib/locations/getLocationAddressString';
import { InspireLink } from '../../atoms/link';
import directionsIcon from '../../../public/brands/inspire/directions.svg';
import Breadcrumbs from '../../atoms/Breadcrumbs';
import { getLocationPath } from '../../../common/helpers/locationHelper';
import classNames from 'classnames';
import { getLocationById } from '../../../common/services/locationService';
import { useDomainMenu, useOrderLocation, useTallyOrder } from '../../../redux/hooks';
import sectionIndentsStyles from '../../sections/sectionIndents.module.css';
import { useLocationUnavailableError } from '../../../common/hooks/useLocationUnavailableError';

export interface ILocationHeaderProps {
    locationDetails: ILocationByStateOrProvinceDetailsModel;
}

export const LocationHeader = ({ locationDetails }: ILocationHeaderProps): JSX.Element => {
    const router = useRouter();
    const {
        actions: { getDomainMenu },
    } = useDomainMenu();

    const {
        actions: { setPickupLocation },
    } = useOrderLocation();
    const { setUnavailableTallyItems } = useTallyOrder();

    const { pushLocationUnavailableError } = useLocationUnavailableError();

    const {
        displayName,
        contactDetails: { address, phone },
        id,
        addressMapLink,
        url,
        isClosed,
        isOnlineOrderAvailable,
    } = locationDetails;

    const formattedPhone = useMemo(() => formatPhoneNumber(phone || ''), [phone]);
    const formattedAddress = useMemo(() => getLocationAddressDetailsString(address), [address]);
    const currentLocationPath = useMemo(() => getLocationPath(url), [url]);
    const breadcrumbsPaths = useMemo(
        () => [
            {
                title: 'locations',
                href: '/locations',
            },
            {
                title: 'all locations',
                href: '/locations/all',
            },
            {
                title: address.cityName || '',
                href: `/locations/${currentLocationPath}`,
            },
        ],
        [address, currentLocationPath]
    );

    const handleClickViewMenu = useCallback(async () => {
        const location = await getLocationById({
            locationId: id,
        });

        if (location && !location.isDigitallyEnabled) {
            pushLocationUnavailableError(location);
        }
        if (location) {
            setPickupLocation(location);
            getDomainMenu(location);
            setUnavailableTallyItems([]);
            router.push('/menu');
        }
    }, [setUnavailableTallyItems, getDomainMenu, id, router, setPickupLocation, pushLocationUnavailableError]);

    const renderPrimaryCta = () => {
        if (isClosed || !isOnlineOrderAvailable) {
            return (
                <InspireButton
                    className={styles.orderButton}
                    onClick={handleClickViewMenu}
                    text="VIEW MENU"
                    type="secondary"
                />
            );
        }

        return <InspireButton className={styles.orderButton} link={`/?locationId=${id}`} text="ORDER" type="primary" />;
    };

    return (
        <div className={sectionIndentsStyles.wrapper}>
            <div className={styles.container}>
                <Breadcrumbs paths={breadcrumbsPaths} />
                <div className={styles.mainContent}>
                    <div className={styles.infoBlock}>
                        <h2 className={classNames('t-header-h1', styles.title)}>{displayName}</h2>
                        <p className={styles.subtitle}>{formattedAddress}</p>
                        <div className={styles.details}>
                            <div className={styles.bottomBlock}>
                                {formattedPhone && (
                                    <InspireLink link={`tel:${formattedPhone}`} className={styles.link}>
                                        <Icon
                                            className={styles.icon}
                                            size="xs"
                                            icon="action-phone"
                                            variant="colorful"
                                        />
                                        <p className={styles.linkTitle}>{formattedPhone}</p>
                                    </InspireLink>
                                )}
                                {addressMapLink && (
                                    <InspireLink link={addressMapLink} className={styles.link} newtab={true}>
                                        <img className={styles.icon} src={directionsIcon} alt="" />
                                        <p className={styles.linkTitle}>directions</p>
                                    </InspireLink>
                                )}
                            </div>
                            <p className={styles.storeId}>Store ID: {id}</p>
                        </div>
                    </div>
                    <div className={styles.orderBlock}>{renderPrimaryCta()}</div>
                </div>
            </div>
        </div>
    );
};
