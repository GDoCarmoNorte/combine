import React, { useCallback, useState } from 'react';
import { useField, FieldConfig } from 'formik';
import classNames from 'classnames';
import MaskedInput, { MaskedInputProps } from 'react-text-mask';

import styles from './index.module.css';

interface IFormikPasswordInput extends FieldConfig<string> {
    className?: string;
    defaultValue?: string;
    description?: string;
    errorClassName?: string;
    iconClassName?: string;
    inputClassName?: string;
    inputContainerClassName?: string;
    label: string;
    labelClassName?: string;
    mask?: Array<RegExp | string>;
    max?: number;
    maxLength?: number;
    onBlur?: (event: React.FocusEvent<HTMLInputElement>) => void;
    onChange?: (event: React.FocusEvent<HTMLInputElement>) => void;
    onKeyPress?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
    pipe?: MaskedInputProps['pipe'];
    placeholder?: string;
    required?: boolean;
    showHideButton?: boolean;
    showErrorImmediately?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const Input = ({ mask, pipe, ...props }: any): JSX.Element => {
    return mask ? <MaskedInput {...props} keepCharPositions mask={mask} pipe={pipe} /> : <input {...props} />;
};

const FormikPasswordInput = ({
    className,
    description,
    errorClassName,
    iconClassName,
    inputClassName,
    inputContainerClassName,
    label,
    labelClassName,
    onKeyPress,
    placeholder,
    required,
    showErrorImmediately,
    showHideButton = true,
    validate,
    ...props
}: IFormikPasswordInput): JSX.Element => {
    const [field, meta] = useField<string>({ validate, ...props });
    const [showPassword, setShowPassword] = useState(false);

    const groupClasses = classNames(styles.inputGroup, {
        [styles.invalid]: meta.touched && meta.error,
        [className]: !!className,
    });

    const handleKeyPress = useCallback(
        (e) => {
            if (typeof onKeyPress === 'function') {
                onKeyPress(e);
            }
        },
        [onKeyPress]
    );

    return (
        <div className={groupClasses}>
            <label
                className={classNames(styles.inputLabel, {
                    [labelClassName]: !!labelClassName,
                })}
                htmlFor={field.name}
            >
                {label}
                {required && <span className={styles.inputLabelStar}> *</span>}
            </label>

            <div
                className={classNames(styles.inputContainer, {
                    [inputContainerClassName]: !!inputContainerClassName,
                })}
            >
                <Input
                    aria-required={required}
                    id={field.name}
                    {...field}
                    {...props}
                    placeholder={placeholder}
                    className={classNames(styles.inputControl, {
                        [inputClassName]: !!inputClassName,
                    })}
                    onKeyPress={handleKeyPress}
                    type={showPassword ? 'text' : 'password'}
                />

                {showHideButton && (
                    <button
                        aria-label={showPassword ? 'Hide Password' : 'Show Password'}
                        className={classNames(styles.icon, {
                            [iconClassName]: !!iconClassName,
                            'brand-icon': true,
                            'info-visible': !showPassword,
                            'info-invisible': showPassword,
                        })}
                        onClick={() => {
                            setShowPassword(!showPassword);
                        }}
                        type="button"
                    />
                )}
            </div>

            {(meta.touched || (showErrorImmediately && meta.value)) && meta.error && (
                <div
                    className={classNames(styles.inputError, {
                        [errorClassName]: !!errorClassName,
                    })}
                >
                    {meta.error}
                </div>
            )}
            {description && <div className={classNames('t-paragraph-hint', styles.description)}>{description}</div>}
        </div>
    );
};

export default FormikPasswordInput;
