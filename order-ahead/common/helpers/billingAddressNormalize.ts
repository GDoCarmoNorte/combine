interface BillingAddressDataNormalized {
    cardHolderName?: string;
    billingPostalCode?: string;
}

export const defaultNormalizer = (billingAddress): BillingAddressDataNormalized => {
    return {
        ...(billingAddress?.Value?.postalCode && { billingPostalCode: billingAddress.Value.postalCode }),
        ...(billingAddress?.Value?.name && { cardHolderName: billingAddress.Value.name }),
    };
};

export const applePayNormalizer = (billingAddress): BillingAddressDataNormalized => {
    const billingAddressData = billingAddress?.Value ? JSON.parse(billingAddress.Value) : {};
    return {
        ...(billingAddressData.postalCode && { billingPostalCode: billingAddressData.postalCode }),
        ...(billingAddressData.givenName &&
            billingAddressData.familyName && {
                cardHolderName: `${billingAddressData.givenName} ${billingAddressData.familyName}`,
            }),
    };
};
