import {
    TallyDiscountDetailsTypeModel,
    TallyDiscountTypeModel,
    TallyFulfillmentTypeModel,
    TallyProductModel,
    TallyRequestDeliveryAddressModel,
    TallyRequestModel,
} from '../../@generated/webExpApi';
import { IDeliveryLocation } from '../services/locationService/types';

export const addLineItemId = (product, index): TallyProductModel => {
    const entryLineItemId = index + 1;
    const childItems =
        Array.isArray(product.childItems) &&
        product.childItems.map((entry, index) => addLineItemId(entry, entryLineItemId * 100 + index));

    return {
        ...product,
        lineItemId: entryLineItemId,
        ...(childItems && { childItems }),
    };
};

interface ICreateTallyRequestOptions {
    products: TallyProductModel[];
    locationId: string;
    fulfillmentType: TallyFulfillmentTypeModel;
    orderTime?: Date;
    dealId?: string;
    customerId?: string;
    ssCorrelationId?: string;
    deliveryLocation?: IDeliveryLocation;
}

export const getDeliveryAddress = (deliveryLocation: IDeliveryLocation): TallyRequestDeliveryAddressModel => {
    const { addressLine1, addressLine2, city, state, zipCode } = deliveryLocation;
    const countryCode = 'US';
    return {
        line1: addressLine1,
        line2: addressLine2,
        city: city,
        stateProvinceCode: state,
        postalCode: zipCode,
        countryCode,
    };
};

export const createTallyRequest = ({
    products,
    locationId,
    fulfillmentType,
    orderTime,
    dealId,
    customerId,
    ssCorrelationId,
    deliveryLocation,
}: ICreateTallyRequestOptions): TallyRequestModel => {
    const tallyRequest: TallyRequestModel = {
        locationId,
        fulfillmentType,
        ...(orderTime && { requestedDateTime: orderTime }),
        isAsap: !orderTime,
        products,
        ssCorrelationId,
    };

    if (dealId) {
        tallyRequest.discounts = {
            discountType: TallyDiscountTypeModel.Provided,
            discountDetails: [
                {
                    code: dealId,
                    type: TallyDiscountDetailsTypeModel.OfferCode,
                },
            ],
        };
    }
    if (customerId) {
        tallyRequest.customerId = customerId;
    }
    if (deliveryLocation) {
        tallyRequest.deliveryAddress = getDeliveryAddress(deliveryLocation);
    }

    return tallyRequest;
};
