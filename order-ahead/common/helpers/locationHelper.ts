import { ILocationByStateOrProvinceDetailsModel, IServiceTypeModel } from '../../@generated/webExpApi';
import { IPageSections } from '../../components/sections';

export const getLocationDescription = (
    location: ILocationByStateOrProvinceDetailsModel,
    descriptions: { [key: string]: string }
): string | undefined => {
    if (!descriptions) {
        return undefined;
    }

    let description = descriptions[IServiceTypeModel.GeneralLocation];
    const locationServices = location.services;
    if (!locationServices) {
        return description;
    }

    location.services.forEach((service) => {
        const locationSpecificDescription = descriptions[service];
        if (locationSpecificDescription) {
            description = locationSpecificDescription;
        }
    });

    return description;
};

export const getLocationNewsSectionsToExpand = (sections?: IPageSections): string[] => {
    const expandedSections: string[] = [];
    const locationNewsSections = sections?.filter((s) => s.sys.contentType.sys.id === 'locationNewsSection');
    if (!locationNewsSections || locationNewsSections.length > 1) {
        return expandedSections;
    }

    return [locationNewsSections[0]?.sys.id];
};

export const getLocationPath = (url: string): string => {
    if (!url) {
        return '';
    }

    const paths = url.split('/');

    return `${paths[0]}/${paths[1]}/${paths[2]}`;
};
