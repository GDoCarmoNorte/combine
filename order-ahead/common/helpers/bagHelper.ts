import { Dictionary } from '@reduxjs/toolkit';

import { ItemModel, TallyModifierGroupModel, TallyModifierModel, TallyProductModel } from '../../@generated/webExpApi';

import { PDPTallyItem } from '../../redux/pdp';
import { LocationWithDetailsModel } from '../services/locationService/types';
import { getDiscountPrice, getDiscountType, getTallyPriceType } from './discountHelper';
import { WritableDraft } from 'immer/dist/internal';
import { BagState } from '../../redux/bag';
import { MAX_PRODUCT_QUANTITY } from '../constants/bag';
import { IBagRenderItem } from '../../components/organisms/bag/bagModel';
import { IUnavailableItem } from '../hooks/useUnavailableItems';
import { sortObject } from './sortObject';

function getTallyBagItemProductIds(bagEntry: TallyProductModel): string[] {
    let productIds = [bagEntry.productId];

    if (bagEntry.modifierGroups) {
        productIds = productIds.concat(
            bagEntry.modifierGroups
                .flatMap((group) => group.modifiers)
                .filter(Boolean)
                .map((modifier) => modifier.productId)
        );
    }

    if (bagEntry.childItems) {
        bagEntry.childItems.forEach(
            (childItem) => (productIds = productIds.concat(getTallyBagItemProductIds(childItem)))
        );
    }

    return productIds;
}

export const getTallyBagProductIds = (bagEntries: TallyProductModel[] = []): string[] => {
    return bagEntries.reduce((productIds, bagEntry) => productIds.concat(getTallyBagItemProductIds(bagEntry)), []);
};

const getTallyBagModifier = (modifier: TallyModifierModel): TallyModifierModel => ({
    productId: modifier.productId,
    quantity: modifier.quantity,
    price: modifier.price, // getCheapestPriceFromProduct(domainProducts[modifier.productId], location), keep like a tip
    modifierGroups: modifier.modifierGroups,
});

function getTallyBagEntry(
    bagEntry: TallyProductModel,
    domainProducts: Dictionary<ItemModel>,
    location?: LocationWithDetailsModel,
    date?: Date
): TallyProductModel {
    const { lineItemId, productId, quantity, ssRecommendationId } = bagEntry;

    const discountPrice = getDiscountPrice(domainProducts[productId], location, date);
    const discountType = getDiscountType(domainProducts[productId], location, date);

    const tallyItem: TallyProductModel = {
        lineItemId,
        productId,
        quantity,
        price: discountPrice !== null ? discountPrice : domainProducts[productId]?.price?.currentPrice || 0,
        priceType: getTallyPriceType(discountType),
        ssRecommendationId,
        // price: getCheapestPriceFromProduct(domainProducts[productId], location), // actual price from current domain products, keep like a tip
    };

    if (bagEntry.childItems) {
        tallyItem.childItems = bagEntry.childItems.map((childItem) => {
            const item = getTallyBagEntry(childItem, domainProducts);

            item.price = childItem.price;

            return item;
        });
    }

    if (bagEntry.modifierGroups) {
        tallyItem.modifierGroups = bagEntry.modifierGroups.map((group) => ({
            productId: group?.productId,
            modifiers: group?.modifiers?.map((modifier) => getTallyBagModifier(modifier)),
        }));
    }

    return tallyItem;
}

export const getTallyBagEntries = (
    bagEntries: TallyProductModel[],
    domainProducts: Dictionary<ItemModel>,
    location?: LocationWithDetailsModel,
    date?: Date
): TallyProductModel[] => {
    return bagEntries.map((bagEntry) => getTallyBagEntry(bagEntry, domainProducts, location, date));
};

export function getBagItemSizeLabel(size?: string): string {
    if (!size || size.toLocaleLowerCase() === 'none') return '';

    switch (size) {
        case 'Small':
            return 'S';
        case 'Medium':
            return 'M';
        case 'Large':
            return 'L';
        default:
            return size;
    }
}

export const getAvailableEntries = (
    bagEntries: TallyProductModel[],
    domainProducts: Dictionary<ItemModel>,
    unavailableCategories?: string[]
): TallyProductModel[] => {
    return bagEntries.filter((entry) => {
        const categoryIds = domainProducts?.[entry.productId]?.categoryIds;
        const isAvailable = domainProducts?.[entry.productId]?.availability?.isAvailable;
        return isAvailable && !checkIsUnavailabeProduct(categoryIds, unavailableCategories);
    });
};

export const areAllProductsAvailable = (
    bagEntries: TallyProductModel[],
    domainProducts: Dictionary<ItemModel>,
    unavailableCategories: string[]
): boolean => {
    return bagEntries.every((entry) => {
        const categoryIds = domainProducts?.[entry.productId]?.categoryIds;
        const isAvailable = domainProducts?.[entry.productId]?.availability?.isAvailable;
        return isAvailable && !checkIsUnavailabeProduct(categoryIds, unavailableCategories);
    });
};

const modifiersSorting = (a: TallyModifierModel, b: TallyModifierModel) => a.productId.localeCompare(b.productId);
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const removeUndefinedValues = (obj: { [key: string]: any }): { [key: string]: any } => JSON.parse(JSON.stringify(obj));
const prepareForComparsion = (modifier: TallyModifierModel): TallyModifierModel => ({
    productId: modifier.productId,
    price: modifier.price,
    quantity: modifier.quantity,
    modifierGroups:
        modifier.modifierGroups?.map((modifierGroup) => ({
            productId: modifierGroup.productId,
            modifiers: modifierGroup.modifiers?.map(prepareForComparsion),
        })) || [],
});

export const compareModifierGroups = (a: TallyModifierGroupModel[], b: TallyModifierGroupModel[]): boolean => {
    return a.every((left, index) => {
        const right = b[index];

        if (left.productId !== right.productId || (left.modifiers || []).length !== (right.modifiers || []).length) {
            return false;
        }

        const leftModifiers = (left.modifiers || [])
            .map(prepareForComparsion)
            .map(removeUndefinedValues)
            .sort(modifiersSorting)
            .map(sortObject);

        const rightModifiers = (right.modifiers || [])
            .map(prepareForComparsion)
            .map(removeUndefinedValues)
            .sort(modifiersSorting)
            .map(sortObject);

        return JSON.stringify(leftModifiers) === JSON.stringify(rightModifiers);
    });
};

export const compareTallyItems = (
    a: TallyProductModel | PDPTallyItem,
    b: TallyProductModel | PDPTallyItem
): boolean => {
    if (
        a.productId !== b.productId ||
        (a.modifierGroups || []).length !== (b.modifierGroups || []).length ||
        (a.childItems || []).length !== (b.childItems || []).length
    ) {
        return false;
    }

    const isModifierGroupsEqual = compareModifierGroups(a.modifierGroups || [], b.modifierGroups || []);

    // @ts-ignore - every doesn't work well with union types in ts 4+
    const isChildItemsEqual = (a.childItems || []).every(
        (tallyItem, index) => b.childItems[index] && compareTallyItems(tallyItem, b.childItems[index])
    );

    return isModifierGroupsEqual && isChildItemsEqual;
};

export const findItemIndexInBag = (bagEntries: TallyProductModel[], item: TallyProductModel | PDPTallyItem): number => {
    return bagEntries.findIndex((bagEntry) => compareTallyItems(bagEntry, item));
};

export const updateStateBagItemCount = (state: WritableDraft<BagState>, bagEntryIndex: number, value: number): void => {
    if (state.LineItems[bagEntryIndex].quantity + value <= MAX_PRODUCT_QUANTITY) {
        state.LineItems[bagEntryIndex].quantity += value;
    } else {
        state.LineItems[bagEntryIndex].quantity = MAX_PRODUCT_QUANTITY;
    }
};

export function checkIsUnavailabeProduct(productCategoryIds?: string[], unavailableCategories?: string[]): boolean {
    if (!(productCategoryIds && unavailableCategories)) return false;
    return productCategoryIds
        .map((category) => unavailableCategories.includes(category))
        .reduce((cur, prev) => cur && prev);
}

export const isItemWithUnavailableModifires = (item: IBagRenderItem, unavailableModifiers: string[]): boolean =>
    item.entry?.modifierGroups?.some((group) =>
        group.modifiers.some((modifier) => unavailableModifiers.includes(modifier.productId))
    );

export const isItemWithUnavailableSubModifires = (item: IBagRenderItem, unavailableSubModifiers: string[]): boolean =>
    item.entry?.modifierGroups?.some((group) =>
        group.modifiers.some((modifier) =>
            modifier?.modifierGroups?.some((group) =>
                group?.modifiers.some((modifier) => unavailableSubModifiers.includes(modifier.productId))
            )
        )
    );

export const isItemUnavailableByModifiers = (item: IBagRenderItem, unavailableTallyItems: IUnavailableItem[]) =>
    !!unavailableTallyItems.find(
        ({ productId, modifierIds, subModifierIds, isWholeProductUnavailable, lineItemId }) =>
            productId === item.entry.productId &&
            lineItemId === item.entry.lineItemId &&
            !isWholeProductUnavailable &&
            (isItemWithUnavailableModifires(item, modifierIds) ||
                isItemWithUnavailableSubModifires(item, subModifierIds))
    );

export const isWholeItemUnavailable = (item: IBagRenderItem, unavailableTallyItems: IUnavailableItem[]) =>
    !!unavailableTallyItems.find(
        ({ productId, isWholeProductUnavailable, lineItemId }) =>
            productId === item.entry.productId && lineItemId === item.entry.lineItemId && isWholeProductUnavailable
    );

export const isWholeItemUnavailableByModifiers = (item: IBagRenderItem, unavailableTallyItems: IUnavailableItem[]) =>
    !!unavailableTallyItems.find(
        ({ productId, isDefaultModifiersUnavailable, modifierIds, lineItemId }) =>
            productId === item.entry.productId &&
            lineItemId === item.entry.lineItemId &&
            isDefaultModifiersUnavailable &&
            isItemWithUnavailableModifires(item, modifierIds)
    );

export const isAvailableProductByTally = (item: IBagRenderItem, unavailableTallyItems: IUnavailableItem[]) =>
    !(
        isWholeItemUnavailable(item, unavailableTallyItems) ||
        isWholeItemUnavailableByModifiers(item, unavailableTallyItems) ||
        isItemUnavailableByModifiers(item, unavailableTallyItems)
    );

export const removeUnavailableModifiers = (
    tallyItem: TallyProductModel,
    unavailableModifiers: string[],
    unavailableSubModifiers: string[]
): TallyProductModel => {
    return {
        ...tallyItem,
        modifierGroups: tallyItem.modifierGroups.map((modifierGroupsItem) => {
            return {
                ...modifierGroupsItem,
                modifiers: modifierGroupsItem.modifiers
                    .filter((modifier) => !unavailableModifiers?.includes(modifier.productId))
                    .map((modifier) => {
                        return {
                            ...modifier,
                            ...(modifier?.modifierGroups && {
                                modifierGroups: modifier?.modifierGroups.map((modifierGroupsItem) => {
                                    return {
                                        ...modifierGroupsItem,
                                        modifiers: modifierGroupsItem.modifiers.filter(
                                            (modifier) => !unavailableSubModifiers?.includes(modifier.productId)
                                        ),
                                    };
                                }),
                            }),
                        };
                    }),
            };
        }),
    };
};
