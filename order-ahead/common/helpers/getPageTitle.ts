import getBrandInfo from '../../lib/brandInfo';

export const getHomePageTitle = (brandName = '', text = ''): string => `${brandName} | ${text}`;

export const getPdpPageTitle = (brandName = '', productName = ''): string =>
    `${productName} - Nearby For Delivery or Pick Up | ${brandName}`;

export const getMenuCategoryPageTitle = (brandName = '', categoryName = ''): string =>
    `${categoryName} Nearby For Delivery or Pick Up | ${brandName}`;

export const getPageTitle = (brandName = '', text = ''): string => `${text} | ${brandName}`;

export const getLocationDetailsPageTitle = (template = '', locationName = 'Location Details'): string => {
    const { brandName } = getBrandInfo();
    const result = template.replace(/{location}/gi, locationName);
    return result ? `${result} | ${brandName}` : `${locationName} | ${brandName}`;
};

export const getCityLocationsPageTitle = (template = '', state = '', city = ''): string => {
    const { brandName } = getBrandInfo();
    const result = template.replace(/{state}/gi, state).replace(/{city}/gi, city);
    return `${brandName} ${result}`;
};

export const getStateLocationsPageTitle = (template = '', state = ''): string => {
    const { brandName } = getBrandInfo();
    const result = template.replace(/{state}/gi, state);
    return `${brandName} ${result}`;
};
