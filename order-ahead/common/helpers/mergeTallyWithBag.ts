import { TallyProductModel, TallyResponseModel } from '../../@generated/webExpApi';

/**
 *
 * - Merge local bag tally items to tally response
 * - Merge missing data from tally response to local bag tally items
 *   - discounts data
 *   - actionCode for modifier
 *
 */
export const mergeTallyWithBag = (
    tallyResponse: TallyResponseModel,
    bagProducts: TallyProductModel[]
): TallyResponseModel => {
    const restoredProducts = bagProducts?.map((product) => {
        const responseProduct = tallyResponse?.products?.find((resProduct) => {
            return resProduct.productId === product.productId && resProduct?.lineItemId === product?.lineItemId;
        });

        const modifierGroups = product?.modifierGroups?.map((modifierGroup) => {
            const responseModifierGroup = responseProduct?.modifierGroups?.find(
                (resModifierGroup) => resModifierGroup.productId === modifierGroup.productId
            );

            const restoredModifiers = modifierGroup.modifiers?.map((modifier) => {
                const responseModifier = responseModifierGroup?.modifiers?.find(
                    (resModifier) => resModifier.productId === modifier.productId
                );

                return { ...modifier, actionCode: responseModifier?.actionCode };
            });

            return { ...modifierGroup, modifiers: restoredModifiers };
        });

        return { ...product, discounts: responseProduct?.discounts, modifierGroups };
    });

    return { ...tallyResponse, products: restoredProducts };
};
