const maskCreditCard = (cardNumber: string): string => `**** ${String(cardNumber).slice(-4)}`;

export default maskCreditCard;
