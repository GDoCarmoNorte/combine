import { IProductFields } from '../../@generated/@types/contentful';

export const getContentfulProductIdsByFields = (fields: IProductFields): string[] => {
    return fields?.productIdList && fields.productIdList.length ? fields.productIdList : [fields?.productId];
};
