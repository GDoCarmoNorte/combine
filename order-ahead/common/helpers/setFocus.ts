export const setFocus = (element: HTMLElement, preventScroll = false): void => {
    element.setAttribute('tabIndex', '-1');
    element.focus({ preventScroll });
    element.removeAttribute('tabIndex');
};
