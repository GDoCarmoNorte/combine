const maskGiftCard = (cardNumber: string): string => String(cardNumber).replace(/.(?=.{4})/g, '*');

export default maskGiftCard;
