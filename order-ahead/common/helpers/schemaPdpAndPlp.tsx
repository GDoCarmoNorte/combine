import { getImageSrcset } from './contentfulImage';
import { IProductFields, IMenuCategoryFields } from '../../@generated/@types/contentful';
import { useGlobalProps } from '../../redux/hooks';
import { BRANDS, IBrandInfo } from '../../lib/brandInfo';
import { InspireCmsEntry } from '../types';
import { NutritionModel } from '../../@generated/webExpApi';

enum WeightEnum {
    g = 'grams',
    mg = 'milligrams',
}

const SchemaPdpAndPlp = (
    menuCategory: InspireCmsEntry<IMenuCategoryFields>,
    brandInfo: IBrandInfo,
    product?: InspireCmsEntry<IProductFields>,
    nutrition?: NutritionModel
) => {
    const { brandName, brandId } = brandInfo;
    const maxWidthImgProduct = 200;
    const maxWidthImgLogo = 1500;

    const descriptionMap = {
        [BRANDS.arbys.brandId]:
            "Arby's sandwich shops are known for slow roasted roast beef, turkey, and premium Angus beef sandwiches, sliced fresh every day.",
        [BRANDS.bww.brandId]:
            'Enjoy all Buffalo Wild Wings® has to offer when you order online or stop by a location near you. Buffalo Wild Wings® is the ultimate place to get together with your friends, watch sports, drink beer, and eat wings.',
    };

    const servesCuisineMap = {
        [BRANDS.arbys.brandId]: 'Fast Food Restaurant',
        [BRANDS.bww.brandId]: 'Chicken Wings Restaurant',
    };

    const { navigation } = useGlobalProps();
    const webNavigation = navigation?.items?.find((item) => item?.fields?.name === 'Web');
    const assetLogo = webNavigation?.fields?.logo;
    const assetProduct = menuCategory?.fields.image;

    const urlImgLogo = getImageSrcset(assetLogo, maxWidthImgLogo);
    const urlImgProduct = getImageSrcset(assetProduct, maxWidthImgProduct);

    const macroNutrients = nutrition?.macroNutrients;

    const calories = nutrition ? nutrition?.totalCalories : 0;
    const carbohydrateContent = macroNutrients?.['Total Carbohydrates (g)']?.weight;
    const cholesterolContent = macroNutrients?.['Cholesterol (mg)']?.weight;
    const fatContent = macroNutrients?.['Fat - Total (g)']?.weight;
    const fiberContent = macroNutrients?.['Dietary Fiber (g)']?.weight;
    const proteinContent = macroNutrients?.['Proteins (g)']?.weight;
    const saturatedFatContent = macroNutrients?.['Saturated Fat (g)']?.weight;
    const servingSize = macroNutrients?.['Serving Weight (g)']?.weight;
    const sodiumContent = macroNutrients?.['Sodium (mg)']?.weight;
    const sugarContent = macroNutrients?.['Sugars (g)']?.weight;
    const transFatContent = macroNutrients?.['Trans Fat (g)']?.weight;

    const getNutritionInfo = () => ({
        calories: `${calories} calories`,
        ...(carbohydrateContent && {
            carbohydrateContent: `${carbohydrateContent?.value} ${WeightEnum[carbohydrateContent?.unit]}`,
        }),
        ...(cholesterolContent && {
            cholesterolContent: `${cholesterolContent?.value} ${WeightEnum[cholesterolContent?.unit]}`,
        }),
        ...(fatContent && { fatContent: `${fatContent?.value} ${WeightEnum[fatContent?.unit]}` }),
        ...(fiberContent && { fiberContent: `${fiberContent?.value} ${WeightEnum[fiberContent?.unit]}` }),
        ...(proteinContent && { proteinContent: `${proteinContent?.value} ${WeightEnum[proteinContent?.unit]}` }),
        ...(saturatedFatContent && {
            saturatedFatContent: `${saturatedFatContent?.value} ${WeightEnum[saturatedFatContent?.unit]}`,
        }),
        ...(servingSize && { servingSize: `${servingSize?.value} ${WeightEnum[servingSize?.unit]}` }),
        ...(sodiumContent && { sodiumContent: `${sodiumContent?.value} ${WeightEnum[sodiumContent?.unit]}` }),
        ...(sugarContent && { sugarContent: `${sugarContent?.value} ${WeightEnum[sugarContent?.unit]}` }),
        ...(transFatContent && { transFatContent: `${transFatContent?.value} ${WeightEnum[transFatContent?.unit]}` }),
    });

    const dataSchema = {
        '@context': 'http://schema.org',
        '@type': 'Restaurant',
        url: `${process.env.NEXT_PUBLIC_APP_URL}`,
        name: brandName,
        image: `https:${urlImgLogo}`,
        description: descriptionMap[brandId],
        servesCuisine: [servesCuisineMap[brandId]],
        hasMenu: {
            '@type': 'Menu',
            hasMenuSection: {
                '@type': 'MenuSection',
                name: `${menuCategory?.fields?.categoryName}`,
                description: `${menuCategory?.fields?.metaDescription}`,
                image: [`https:${urlImgProduct}`],
                hasMenuItem: {
                    '@type': 'MenuItem',
                    name: `${product?.fields?.name}`,
                    description: `${product?.fields?.metaDescription}`,
                    nutrition: {
                        '@type': 'NutritionInformation',
                        ...getNutritionInfo(),
                    },
                },
            },
            inLanguage: 'English',
        },
    };
    return dataSchema;
};
export default SchemaPdpAndPlp;
