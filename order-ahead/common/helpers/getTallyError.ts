import {
    ITallyError500ExternalResponseModel,
    TTallyErrorCodeModel,
    TTallyErrorMessageModel,
} from '../../@generated/webExpApi/models';

export const getTallyError = (
    error: ITallyError500ExternalResponseModel,
    phoneNumber?: string
): ITallyError500ExternalResponseModel => {
    const errorsMap: { [key in TTallyErrorCodeModel | string]: ITallyError500ExternalResponseModel } = {
        [TTallyErrorCodeModel.DeliveryProviderDeliveryTimeNotAvailable]: {
            ...error,
            message: (error.message as TTallyErrorMessageModel).replace('{earliestTime}', error.data?.earliestTime),
        },
        [TTallyErrorCodeModel.ExceededMaxOrderAmount]: {
            ...error,
            message: (error.message as TTallyErrorMessageModel)
                .replace('{orderMaxValue}', error.data?.max)
                .replace('{phoneNumber}', phoneNumber),
        },
    };

    return errorsMap[error?.code as TTallyErrorCodeModel] || error;
};
