import { IProductGroupModel } from '../../@generated/webExpApi';
import { PDPTallyItemModifierGroup } from '../../redux/pdp';
import { ISelectedModifier, ModifierGroupType } from '../../redux/types';

export const isWingTypeModifierGroup = (
    group?: IProductGroupModel | PDPTallyItemModifierGroup | ISelectedModifier
): boolean => {
    return group?.metadata?.MODIFIER_GROUP_TYPE === ModifierGroupType.WINGTYPE;
};
