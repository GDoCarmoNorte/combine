export const calculateAge = (dateOfBirth: Date): number => {
    const today = new Date();
    const monthDiff = today.getMonth() - dateOfBirth.getMonth();

    let age = today.getFullYear() - dateOfBirth.getFullYear();

    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < dateOfBirth.getDate())) {
        age--;
    }

    return age;
};
