import { IMenuCategory, IMenuCategoryFields } from '../../@generated/@types/contentful';
import { CategoryModel } from '../../@generated/webExpApi';

export const getContentfulMenuCategoryIdsByFields = (fields: IMenuCategoryFields): string[] => {
    return fields.categoryIdList && fields.categoryIdList.length ? fields.categoryIdList : [fields.categoryId];
};

export const getDomainMenuCategoryIdByIds = (ids: string[], categories: { [key: string]: CategoryModel }) => {
    for (const id of ids) {
        if (categories[id]) return id;
    }
    return null;
};

export const isMenuCategoryUnavailable = (
    category: IMenuCategory,
    unavailableCategoriesIds: string[],
    menuCategories: { [key: string]: CategoryModel }
) => {
    const id = getDomainMenuCategoryIdByIds(getContentfulMenuCategoryIdsByFields(category.fields), menuCategories);

    return unavailableCategoriesIds.includes(id);
};
