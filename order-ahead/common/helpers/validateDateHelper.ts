import { FieldValidator } from 'formik';
import { differenceInDays, parse, isValid, isAfter } from './dateTime';
import { dateAndYearValidator, requireValidator } from './validateHelper';

export const differenceBetweenDayAndCurrentDay = (difference: number, errorMessage: string): FieldValidator => (
    value: string
) => {
    return differenceInDays(new Date(), new Date(value)) > difference || isAfter(new Date(value), new Date())
        ? errorMessage
        : undefined;
};

export const isValidDate = (errorMessage: string): FieldValidator => (value: string) => {
    const parsed = parse(value, 'MM/dd/yyyy', new Date());
    return isValid(parsed) ? undefined : errorMessage;
};

export const validateReceiptDate = (label: string, messageMap: { require?: string } = {}): FieldValidator => {
    const require = requireValidator(messageMap.require || `${label} is incomplete`);
    const date = dateAndYearValidator(`Please enter a valid ${label} (MM/DD/YYYY)`);
    const isValid = isValidDate(`${label} CANNOT BE OLDER THAN 60 DAYS AND MUST BE A VALID DATE.`);
    const isCorrectDate = differenceBetweenDayAndCurrentDay(
        60,
        `${label} CANNOT BE OLDER THAN 60 DAYS AND MUST BE A VALID DATE.`
    );
    return (value: string) => require(value) || date(value) || isCorrectDate(value) || isValid(value);
};
