import { IMenuCategory, ITapListMenuCategory } from '../../@generated/@types/contentful';

const isTapListMenuCategory = (category: IMenuCategory | ITapListMenuCategory): category is ITapListMenuCategory =>
    category.sys.contentType.sys.id === 'tapListMenuCategory';

export default isTapListMenuCategory;
