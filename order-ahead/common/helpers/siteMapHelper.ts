import { Entry, EntryCollection } from 'contentful';
import { IMenuCategorySection, IPageFields, ISiteMapCategoryFields } from '../../@generated/@types/contentful';
import { IGlobalContentfulProps } from '../services/globalContentfulProps';
import { getLinkDetails } from '../../lib/link';
import isTapListMenuCategory from './isTapListMenuCategory';
import { DEFAULT_LOCALE } from '../constants/configDefaults';

const LOCALE = process.env.LOCALE as string;
export const BASE_URL = process.env.NEXT_PUBLIC_APP_URL;

export interface ISiteMapLink {
    nameInUrl: string;
    name: string;
    isExternal: boolean;
}

export interface ISiteMapLinkBlock {
    nameInUrl?: string;
    name?: string;
    links: Array<ISiteMapLink>;
}

export interface ISiteMapCategory {
    mainLink: string;
    name: string;
    linkBlocks?: Array<ISiteMapLinkBlock>;
}

export function groupSiteMapData(
    menuPage: Entry<IPageFields>,
    siteMapCategories: EntryCollection<ISiteMapCategoryFields>,
    globalContentfulProps: IGlobalContentfulProps
): ISiteMapCategory[] {
    const menuCategorySection = menuPage.fields.section.find(
        (section) => section.sys.contentType.sys.id === 'menuCategorySection'
    ) as IMenuCategorySection;
    const menuCategories = menuCategorySection?.fields.categories;
    const menuLinkBlocks = menuCategories?.reduce<ISiteMapLinkBlock[]>((acc, menuCategory) => {
        if (isTapListMenuCategory(menuCategory)) {
            return acc;
        }

        const products = menuCategory?.fields?.products;

        if (!products?.length) {
            return acc;
        }

        const { nameInUrl: menuCategoryNameInUrl } = menuCategory.fields.link.fields;
        const productLinks = products
            .filter((product) => product.fields.isVisible)
            .map((product) => ({
                name: product.fields.name,
                nameInUrl: `/menu/${menuCategoryNameInUrl}/${product.fields.nameInUrl}`,
                isExternal: false,
            }))
            .sort((a, b) => a.name.localeCompare(b.name));

        const newLinkBlock = {
            links: productLinks,
            name: menuCategory.fields.categoryName,
            nameInUrl: `/menu/${menuCategoryNameInUrl}`,
        };

        return [...acc, newLinkBlock];
    }, []);

    const menuSiteMapCategories: ISiteMapCategory = {
        mainLink: '/menu',
        name: 'Menu',
        linkBlocks: menuLinkBlocks,
    };

    const siteMap = siteMapCategories.items.reduce<ISiteMapCategory[]>(
        (acc, category) => {
            const links = category.fields.links
                .map((it) => {
                    const { name, href, isExternal } = getLinkDetails(it);

                    return {
                        name,
                        nameInUrl: href || '/',
                        isExternal,
                    };
                })
                .sort((a, b) => a.name.localeCompare(b.name));

            if (!links.length) {
                return acc;
            }

            const { productDetailsPagePaths } = globalContentfulProps;

            const mainLinkDetails = getLinkDetails(category.fields.mainLink, {
                productDetailsPagePaths,
            });

            const newCategory = {
                linkBlocks: [{ links }],
                name: category.fields.name,
                mainLink: mainLinkDetails.href,
            };

            return [...acc, newCategory];
        },
        [menuSiteMapCategories]
    );

    return siteMap;
}

export const isRegionSitemapSupported = (brand: string) => {
    return brand !== 'arbys';
};

export const getSitemapDirectory = () => {
    return LOCALE && LOCALE !== DEFAULT_LOCALE ? `./${LOCALE}` : null;
};

export const getBaseUrlWithoutLocale = (baseUrl: string) => {
    return baseUrl.includes(LOCALE) ? baseUrl.split(`/${LOCALE}`)[0] : baseUrl;
};
export const getBaseUrl = () => {
    return BASE_URL;
};
