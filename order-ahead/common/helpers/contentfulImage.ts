import { Asset } from 'contentful';

export const getImageUrl = (asset: Asset, width = 1500): string =>
    `${asset.fields.file.url}${width ? `?w=${width}` : ''}`;

export const getImageSrcset = (asset: Asset, width = 1500): string =>
    `${asset.fields.file.url}?fm=webp${width ? `&w=${width}` : ''}`;
