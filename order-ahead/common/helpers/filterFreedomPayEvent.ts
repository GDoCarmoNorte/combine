import { TFPPaymentTypes } from '../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

export const filterFreedomPayEvent = (event: MessageEvent): boolean => {
    const FP_MESSAGE_ORIGIN = process.env.NEXT_PUBLIC_PAYMENT_DOMAIN;

    // valid FP events have freedom pay script origin
    if (event.origin !== FP_MESSAGE_ORIGIN) {
        // except the Apple Pay event, which have local origin

        const LOCAL_ORIGIN = process.env.NEXT_PUBLIC_APP_URL;
        const paymentType = event.data.data?.paymentType;
        if (event.origin !== LOCAL_ORIGIN || paymentType !== TFPPaymentTypes.APPLE_PAY) return false;
    }

    return true;
};
