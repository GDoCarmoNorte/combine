export const getCountryCodeFromLocale = (locale: string): string => {
    if (!locale || !locale.includes('-')) {
        return '';
    }

    return locale.split('-')[1].toLocaleUpperCase();
};

export const getLocale = (): string => process.env.LOCALE || process.env.NEXT_PUBLIC_LOCALE;
