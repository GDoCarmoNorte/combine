export const formatAccountName = (value: string, maxLength: number): string =>
    value?.length > maxLength ? value.slice(0, maxLength) : value;
