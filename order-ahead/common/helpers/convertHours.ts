import { parse, format, isValid } from './dateTime';

//timeString format - 01:59:59 | 01:59 | 1:59:59 | 1:59
export const convertHours = (timeString: string): string => {
    const parsed = parse(timeString, 'HH:mm', new Date());

    if (!isValid(parsed)) {
        return '';
    }

    return format(parsed, 'h:mma');
};
