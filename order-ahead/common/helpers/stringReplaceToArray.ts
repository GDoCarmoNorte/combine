export const stringReplaceToArray = <T>(str: string, match: string | RegExp, replacement: T): Array<string | T> => {
    return str
        .split(match)
        .flatMap((part) => [part, replacement])
        .filter((part) => part !== '')
        .slice(0, -1);
};
