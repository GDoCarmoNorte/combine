import { TallyProductModel, TallyModifierGroupModel, TallyModifierActionCodeModel } from '../../@generated/webExpApi';

export const getAddedModifiersSum = (modifierGroups: TallyModifierGroupModel[]): number => {
    return modifierGroups.reduce((acc, curr) => {
        const addedModifiersSum = curr.modifiers
            ? curr.modifiers.reduce((acc, curr) => {
                  return curr.actionCode === TallyModifierActionCodeModel.Add ? acc + curr.price * curr.quantity : acc;
              }, 0)
            : 0;
        return acc + addedModifiersSum;
    }, 0);
};

export const calculateTallyItemPrice = (tallyItem: TallyProductModel): number => {
    if (tallyItem?.childItems?.length) {
        const productPrice = tallyItem.childItems.reduce((acc, curr) => {
            const addedModifiersSum = curr.modifierGroups ? getAddedModifiersSum(curr.modifierGroups) : 0;
            return acc + curr.price + addedModifiersSum;
        }, 0);
        return productPrice * tallyItem.quantity;
    }
    const addedModifiersSum = tallyItem.modifierGroups ? getAddedModifiersSum(tallyItem.modifierGroups) : 0;

    return (tallyItem.price + addedModifiersSum) * tallyItem.quantity;
};
