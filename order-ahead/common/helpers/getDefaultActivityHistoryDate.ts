import { format } from './dateTime';

const last18Month = 547 * 24 * 60 * 60 * 1000;
export const getDefaultDate = (): string => format(Date.now() - last18Month, 'yyyy-MM-dd');
