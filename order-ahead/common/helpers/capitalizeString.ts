import { capitalize } from 'lodash';

// can capitalize a string with many words
export function capitalizeString(str = ''): string {
    return str
        .split(' ')
        .map((substring) => capitalize(substring))
        .join(' ');
}
