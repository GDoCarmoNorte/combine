export enum DesktopOperatingSystem {
    MAC = 'MAC',
}

export const getDesktopOperatingSystem = (): DesktopOperatingSystem | null => {
    const userAgent = navigator.userAgent;

    if (userAgent && /Mac/i.test(userAgent)) {
        return DesktopOperatingSystem.MAC;
    }

    return null;
};
