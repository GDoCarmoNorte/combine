import { ISurveyModel, ISurveyRespondModel } from '../../@generated/webExpApi/models';

export const formatSurveyName = (name: string) => {
    return name.slice(0, -1);
};

export const getDefaultSurveyRespond = (
    teamsSurvey: ISurveyModel,
    changedSurveyRespond: ISurveyRespondModel[]
): ISurveyRespondModel[] =>
    teamsSurvey.questions
        ?.filter((q) => !changedSurveyRespond.map((c) => c.questionId).includes(q.id))
        ?.map((m) => ({ questionId: m.id, answerId: m.answers.find((a) => a.isDefault).id })) || [];
