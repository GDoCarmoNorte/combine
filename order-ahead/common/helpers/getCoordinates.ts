export const LOCATION_SERVICES_DISABLED_ERROR_CODE = 'LOCATION_SERVICES_DISABLED';

export function getCoordinates(): Promise<GeolocationPosition> {
    return new Promise(function (resolve, reject) {
        navigator.geolocation.getCurrentPosition(resolve, () => {
            reject({ code: LOCATION_SERVICES_DISABLED_ERROR_CODE });
        });
    });
}
