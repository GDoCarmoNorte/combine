export const getFloatNumber = (value: string | number, dec = 2): number => {
    if (typeof value === 'number') return parseFloat(value.toFixed(dec));
    return Number(parseFloat(value).toFixed(dec));
};
