export const locationUnavailableError = (placePhone: string): string =>
    `We apologize. We are currently not taking online orders. You can call us at ${placePhone} to place your order over the phone, or stop by and place your order directly with a team member!.`;
