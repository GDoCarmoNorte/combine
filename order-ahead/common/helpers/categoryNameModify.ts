export const categoryNameModify = (categoryName: string): string => {
    if (!categoryName) return categoryName;

    const categoryWords = categoryName.split('-');
    return categoryWords
        .map((word) => word.replace(/\b\w/g, (firstLetter: string) => firstLetter.toUpperCase()))
        .join(' ');
};
