import { IHoursModel, ILocationServiceHoursModel } from '../../@generated/webExpApi';
import { LocationsDaysModel } from '../constants/locations';
import { getHoursWithTimezone } from '../../lib/locations/resolveOpeningHours';
import isEmpty from 'lodash/isEmpty';

interface ISStoreStatus {
    statusText: string;
    isClosingSoon: boolean;
}

export const transformHoursByDay = (hoursByDay: { [key: string]: ILocationServiceHoursModel }): IHoursModel[] => {
    if (!hoursByDay || isEmpty(hoursByDay)) {
        return null;
    }

    const daysOfWeek = Object.keys(hoursByDay);
    return daysOfWeek.reduce((hours, day) => {
        const dayHours = {
            dayOfWeek: LocationsDaysModel[day],
            startTime: hoursByDay[day].start,
            endTime: hoursByDay[day].end,
            isTwentyFourHourService: hoursByDay[day].isOpen24Hs,
        };

        if (dayHours.startTime && dayHours.endTime) {
            hours.push(dayHours);
        }

        return hours;
    }, [] as IHoursModel[]);
};

export const getStoreStatus = (
    hoursByDay: { [key: string]: ILocationServiceHoursModel },
    timezone: string
): ISStoreStatus | null => {
    if (!hoursByDay || isEmpty(hoursByDay) || !timezone) {
        return null;
    }

    const transformedHoursByDay = transformHoursByDay(hoursByDay);

    const { isOpen, openTime, closeTime, isTwentyFourHourService, isClosingSoon } = getHoursWithTimezone(
        transformedHoursByDay,
        timezone
    );

    if (isOpen && isTwentyFourHourService) {
        return {
            statusText: 'Open 24H',
            isClosingSoon: false,
        };
    }

    if (isOpen) {
        return {
            statusText: `Open Now - Closes ${closeTime}`,
            isClosingSoon,
        };
    }

    return {
        statusText: `Closed Now - Opens ${openTime}`,
        isClosingSoon: false,
    };
};
