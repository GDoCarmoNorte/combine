import _find from 'lodash/find';
import { TAccountPaymentMethodTypeModel } from '../../@generated/webExpApi';
interface ICreditCardMapper {
    idpPrefix: string;
    freedomPayName: string;
}
const creditCards = [
    {
        idpPrefix: 'VI',
        freedomPayName: 'Visa',
    },
    {
        idpPrefix: 'MC',
        freedomPayName: 'Mastercard',
    },
    {
        idpPrefix: 'AX',
        freedomPayName: 'AmericanExpress',
    },
    {
        idpPrefix: 'DS',
        freedomPayName: 'Discover',
    },
    {
        idpPrefix: 'AM',
        freedomPayName: 'Amex',
    },
    {
        idpPrefix: 'JC',
        freedomPayName: 'JapanCreditBureau',
    },
    {
        idpPrefix: 'CB',
        freedomPayName: 'CarteBlanche',
    },
    {
        idpPrefix: 'DN',
        freedomPayName: 'DinersClub',
    },
] as ICreditCardMapper[];

export const getCreditCardIssuerPrefixFromFreedomPayName = (freedomPayName: string): TAccountPaymentMethodTypeModel => {
    const result = _find(creditCards, (creditCard) => {
        return creditCard.freedomPayName === freedomPayName;
    });

    return result ? (result.idpPrefix as TAccountPaymentMethodTypeModel) : TAccountPaymentMethodTypeModel.Un;
};

export const getCreditCardIssuerFromFreedomPayType = (freedomPayNameType: string): string => {
    const result = _find(creditCards, (creditCard) => {
        return creditCard.idpPrefix === freedomPayNameType;
    });

    return result ? result.freedomPayName : '';
};
