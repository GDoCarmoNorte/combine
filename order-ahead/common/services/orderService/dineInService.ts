import {
    DineInOrderRequest,
    DineInContactlessPaymentRequest,
} from '../../../@generated/webExpApi/apis/WebExperienceApi';
import { WebExperienceApi } from '../../../@generated/webExpApi/apis/index';

import createErrorWrapper from '../createErrorWrapper';
import {
    IDineInOrderDetailsModel,
    IDineInPayOrderRequestModel,
    ITallyError500ExternalResponseModel,
} from '../../../@generated/webExpApi';

const expApi = new WebExperienceApi();

const getDineInOrderDetailsApi = createErrorWrapper<IDineInOrderDetailsModel, DineInOrderRequest>(
    'dineInOrderRetrieval',
    expApi.dineInOrder.bind(expApi)
);

const dineInOrderPaymentApi = createErrorWrapper<void, DineInContactlessPaymentRequest>(
    'dineInOrderPayment',
    expApi.dineInContactlessPayment.bind(expApi)
);

export const getDineInOrderDetails = (
    payload: DineInOrderRequest
): Promise<IDineInOrderDetailsModel | ITallyError500ExternalResponseModel> => {
    const { orderId, locationId } = payload;

    return getDineInOrderDetailsApi({
        orderId,
        locationId,
    });
};

export const submitDineInOrder = (payload: IDineInPayOrderRequestModel): Promise<void> => {
    return dineInOrderPaymentApi({ iDineInPayOrderRequestModel: payload });
};
