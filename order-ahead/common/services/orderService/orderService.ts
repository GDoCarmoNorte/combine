import {
    ITallyError500ExternalResponseModel,
    OrderRequest,
    OrderResponseModel,
    WebExperienceApi,
} from '../../../@generated/webExpApi';

import createErrorWrapper from '../createErrorWrapper';

class OrderService {
    createOrder: (request: OrderRequest) => Promise<OrderResponseModel | ITallyError500ExternalResponseModel>;

    constructor() {
        const expApi = new WebExperienceApi();

        this.createOrder = createErrorWrapper<OrderResponseModel, OrderRequest>(
            'createOrder',
            expApi.order.bind(expApi)
        );
    }
}

export default new OrderService();
