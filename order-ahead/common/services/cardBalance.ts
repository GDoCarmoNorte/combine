import { TCardBalanceCode } from '../hooks/useCardBalance';

export const getCardBalance = async (
    cardNumber: string,
    cardPin?: string
): Promise<{ balance?: number; message?: string; code?: TCardBalanceCode }> => {
    const NEXT_PUBLIC_WEB_EXP_API = process.env.NEXT_PUBLIC_WEB_EXP_API;
    const body: { cardNumber: string; cardPin?: string } = { cardNumber };

    if (cardPin) {
        body.cardPin = cardPin;
    }

    const res = await fetch(`${NEXT_PUBLIC_WEB_EXP_API}/v1/gift-card/balance`, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    });

    const balance = res.ok ? await res.json() : undefined;

    return balance;
};
