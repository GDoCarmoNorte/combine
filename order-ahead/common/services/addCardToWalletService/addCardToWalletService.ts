import { AddCreditCardRequest, WebExperienceApi } from '../../../@generated/webExpApi';
import { getAuthorizationConfig } from '../../helpers/getAuthorizationConfig';
import addJsonError from '../addJsonError';

const addCardToWalletService = async (request: AddCreditCardRequest, JWT: string) => {
    const expApi = new WebExperienceApi(getAuthorizationConfig(JWT));
    const response = await addJsonError('addCreditCard', expApi.addCreditCard(request));

    return response;
};

export default addCardToWalletService;
