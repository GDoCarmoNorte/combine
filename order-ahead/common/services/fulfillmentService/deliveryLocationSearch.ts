import { IDeliveryLocationsSearchResponseModel, WebExperienceApi } from '../../../@generated/webExpApi';
import addJsonError from '../addJsonError';

import { SearchDeliveryLocationRequest } from '../../../@generated/webExpApi/apis/WebExperienceApi';

const deliveryLocationSearch = async (
    payload: SearchDeliveryLocationRequest
): Promise<IDeliveryLocationsSearchResponseModel> => {
    const expApi = new WebExperienceApi();
    const { longitude, latitude } = payload;

    const response = await addJsonError(
        'searchDeliveryLocation',
        expApi.searchDeliveryLocation({
            latitude,
            longitude,
        })
    );

    return response;
};

export default deliveryLocationSearch;
