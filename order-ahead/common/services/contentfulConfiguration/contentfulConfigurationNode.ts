import fs from 'fs';
import { GetStaticPropsContext } from 'next';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import { CMS_CONFIGURATION_FILE_PATH } from './constants';

export const getAndWriteContentfulConfiguration = async (context: GetStaticPropsContext) => {
    const contentfulDelivery = ContentfulDelivery(context.preview);

    try {
        const {
            response: { fields: contentfulConfiguration },
        } = await contentfulDelivery.getConfiguration();

        fs.writeFileSync(CMS_CONFIGURATION_FILE_PATH, JSON.stringify(contentfulConfiguration), 'utf8');
    } catch (error) {
        console.log(error);
        console.log('problems with writing CMS configuration');
    }
};
