import path from 'path';

export const CMS_CONFIGURATION_FILE_PATH = path.join(process.cwd(), 'contentfulConfiguration.json');
