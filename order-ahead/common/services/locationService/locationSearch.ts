import { WebExperienceApi } from '../../../@generated/webExpApi';
import { getLocale } from '../../helpers/locale';
import addJsonError from '../addJsonError';
import { Coordinates, ILocationSearchResult } from './types';

const locationSearch = async (coordinates: Coordinates, page = 0): Promise<ILocationSearchResult> => {
    const expApi = new WebExperienceApi();
    const DEFAULT_RADIUS = 50;

    const response = await addJsonError(
        'searchLocationByCoordinates',
        expApi.searchLocation({
            latitude: coordinates.lat,
            longitude: coordinates.lng,
            radius: DEFAULT_RADIUS, // miles
            limit: 10,
            page,
            locale: getLocale(),
        })
    );

    return response;
};

export default locationSearch;
