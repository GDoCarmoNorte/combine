import { ILocationUrlsResponseModel, SearchLocationUrlsRequest, WebExperienceApi } from '../../../@generated/webExpApi';
import { getLocale } from '../../helpers/locale';
import createErrorWrapper from '../createErrorWrapper';
import { AllLocationUrlsListModel } from './types';

const expApi = new WebExperienceApi();

const getLocationUrlListApi = createErrorWrapper<AllLocationUrlsListModel, SearchLocationUrlsRequest>(
    'searchLocationUrls',
    expApi.searchLocationUrls.bind(expApi)
);

const getLocationUrlList = (): Promise<ILocationUrlsResponseModel> => {
    return getLocationUrlListApi({ locale: getLocale() });
};

export default getLocationUrlList;
