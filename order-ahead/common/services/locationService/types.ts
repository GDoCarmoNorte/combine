import {
    ILocationWithDetailsModel as LocationWithDetailsGeneratedModel,
    ILocationsPageModel,
    IPickupModel,
    TAllLocationsResponseDataModel,
    ILocationUrlsResponseModel,
} from '../../../@generated/webExpApi';

export type Address = string | string[];
export type Coordinates = { lat: number; lng: number };
export type LocationWithDetailsModel = LocationWithDetailsGeneratedModel;
export type ILocationSearchResult = ILocationsPageModel;
export type AllLocationsListModel = TAllLocationsResponseDataModel;
export type AllLocationUrlsListModel = ILocationUrlsResponseModel;

export interface IDeliveryLocation {
    addressLine1: string;
    addressLine2?: string;
    businessName?: string;
    city: string;
    state: string;
    zipCode: string;
}

export interface IDeliveryWrapperType {
    pickUpLocation: IPickupModel;
    deliveryLocation: IDeliveryLocation;
}

export interface IDeliveryAddressObject extends IDeliveryWrapperType {
    locationDetails: LocationWithDetailsModel;
}

export interface ISearchLocationsRequest {
    countryCode?: string;
    stateOrProvinceCode?: string;
}
