import { GetStaticPropsContext } from 'next';
import { IProductDetailsPagePath } from '../../../lib/contentfulDelivery';
import {
    INavigationFields,
    IAlertBannerFields,
    IProductModifierFields,
    IFooterFields,
    IProductFields,
    IDealFields,
    IUserAccountMenuFields,
    IEmptyShoppingBagFields,
    ISodiumWarningFields,
    IActionParameterFields,
} from '../../../@generated/@types/contentful';
import { Dictionary } from '@reduxjs/toolkit';
import { InspireCmsEntry, InspireCmsEntryCollection } from '../../types';

import fs from 'fs';
import { getAndWriteContentfulProps } from './getAndWriteContentfulProps';
import { CMS_FILE_PATH } from './constants';

export type IModifierItemById = Dictionary<InspireCmsEntry<IProductModifierFields>>;
export type IProductItemById = Dictionary<InspireCmsEntry<IProductFields>>;
export type IDealItemsById = Dictionary<InspireCmsEntry<IDealFields>>;

export interface IGlobalContentfulProps {
    navigation: InspireCmsEntryCollection<INavigationFields>;
    alertBanners: InspireCmsEntryCollection<IAlertBannerFields>;
    productsById: IProductItemById;
    modifierItemsById: IModifierItemById;
    footer: InspireCmsEntry<IFooterFields>;
    productDetailsPagePaths: IProductDetailsPagePath[];
    dealItemsById: IDealItemsById;
    userAccountMenu: InspireCmsEntry<IUserAccountMenuFields>;
    emptyShoppingBag: InspireCmsEntry<IEmptyShoppingBagFields>;
    sodiumWarnings: InspireCmsEntryCollection<ISodiumWarningFields>;
    actionParameters: InspireCmsEntryCollection<IActionParameterFields>;
}

export const getGlobalContentfulProps = async (context: GetStaticPropsContext): Promise<IGlobalContentfulProps> => {
    const file = fs.existsSync(CMS_FILE_PATH) && fs.readFileSync(CMS_FILE_PATH, 'utf8');
    if (file !== undefined) {
        try {
            return JSON.parse(file) as IGlobalContentfulProps;
        } catch (error) {
            console.log('problems with parsing CMS DATA');
        }
    }

    return await getAndWriteContentfulProps(context);
};
