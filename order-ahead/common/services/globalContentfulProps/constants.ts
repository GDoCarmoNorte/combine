import path from 'path';

export const CMS_FILE_PATH = path.join(process.cwd(), 'contentfulData.json');
