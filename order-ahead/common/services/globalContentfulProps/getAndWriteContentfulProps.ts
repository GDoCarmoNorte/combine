import fs from 'fs';
import { GetStaticPropsContext } from 'next';
import { IGlobalContentfulProps, IProductItemById, IModifierItemById, IDealItemsById } from '.';
import { optimizeContentfulEntry, optimizeContentfulCollection } from '../../../lib/cmsOptimization';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import { getContentfulProductIdsByFields } from '../../helpers/getContentfulProductIdsByFields';
import { CMS_FILE_PATH } from './constants';

export const fetchGlobalContentfulProps = async (context: GetStaticPropsContext): Promise<IGlobalContentfulProps> => {
    // TODO: Move this to _app.tsx once getStaticProps is supported there
    // https://github.com/zeit/next.js/discussions/10949
    const contentfulDelivery = ContentfulDelivery(context.preview);
    const [
        navigation,
        alertBanners,
        products,
        modifierItems,
        footer,
        productDetailsPagePaths,
        deals,
        userAccountMenu,
        emptyShoppingBag,
        sodiumWarnings,
    ] = await Promise.all([
        contentfulDelivery.getNavigation(),
        contentfulDelivery.getAlertBanners(),
        contentfulDelivery.getProducts(),
        contentfulDelivery.getProductModifiers(),
        contentfulDelivery.getFooter(),
        contentfulDelivery.getProductDetailsPagePaths(),
        contentfulDelivery.getDeals(),
        contentfulDelivery.getUserAccountMenu(),
        contentfulDelivery.getEmptyShoppingBag(),
        contentfulDelivery.getSodiumWarnings(),
    ]);

    // typescript Promise.all doesn't support more than 10 promises https://github.com/microsoft/TypeScript/issues/35617
    const [actionParameters] = await Promise.all([contentfulDelivery.getActionParameters()]);

    const productsById: IProductItemById = {};

    products.items.forEach((product) => {
        const ids = getContentfulProductIdsByFields(product.fields);
        ids.forEach((id) => {
            productsById[id] = optimizeContentfulEntry(product);
        });
    });

    const modifierItemsById: IModifierItemById = {};

    modifierItems.items.forEach((modifier) => {
        if (modifier.fields.productId) {
            modifierItemsById[modifier.fields.productId] = optimizeContentfulEntry(modifier);
        }
    });

    const dealItemsById: IDealItemsById = {};

    deals.items.forEach((deal) => {
        if (deal.fields.offerId) {
            dealItemsById[deal.fields.offerId] = optimizeContentfulEntry(deal);
        }
    });

    return {
        navigation: optimizeContentfulCollection(navigation),
        alertBanners: optimizeContentfulCollection(alertBanners),
        productsById,
        modifierItemsById,
        footer: optimizeContentfulEntry(footer),
        productDetailsPagePaths,
        dealItemsById,
        userAccountMenu: optimizeContentfulEntry(userAccountMenu),
        emptyShoppingBag: optimizeContentfulEntry(emptyShoppingBag),
        sodiumWarnings: optimizeContentfulCollection(sodiumWarnings),
        actionParameters: optimizeContentfulCollection(actionParameters),
    };
};

export async function getAndWriteContentfulProps(context: GetStaticPropsContext) {
    try {
        const data = await fetchGlobalContentfulProps(context);
        fs.writeFileSync(CMS_FILE_PATH, JSON.stringify(data), 'utf8');
        return data;
    } catch (error) {
        console.log(error);
        console.log('problems with writing CMS data');
    }
}
