import { SearchDeliveryLocationRequest, IDeliveryLocationsSearchResponseModel } from '../../@generated/webExpApi';
import { WebExperienceApi } from '../../@generated/webExpApi/apis/index';

import createErrorWrapper, { experiencingTechnicalDifficultiesError } from './createErrorWrapper';
import HttpStatusCode from './httpStatusCode';

const errorMap = {
    [HttpStatusCode.NOT_FOUND]: experiencingTechnicalDifficultiesError,
    [HttpStatusCode.INTERNAL_SERVER_ERROR]: experiencingTechnicalDifficultiesError,
};

const expApi = new WebExperienceApi();

const getNationalMenuApi = createErrorWrapper<IDeliveryLocationsSearchResponseModel, SearchDeliveryLocationRequest>(
    'searchDeliveryLocation',
    expApi.searchDeliveryLocation.bind(expApi),
    errorMap
);

export function getDeliveryLocationByPlaceId(googlePlaceId: string): Promise<IDeliveryLocationsSearchResponseModel> {
    return getNationalMenuApi({ googlePlaceId });
}
