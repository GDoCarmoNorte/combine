import { WebExperienceApi } from '../../../@generated/webExpApi/apis/index';

import {
    ProductsRecommendationRequestModel,
    ProductsRecommendationResponseModel,
    SellingChannelNamesModel,
} from '../../../@generated/webExpApi';
import addJsonError from '../addJsonError';

const fetchRecommendedProducts = async (
    payload: ProductsRecommendationRequestModel
): Promise<ProductsRecommendationResponseModel> => {
    const expApi = new WebExperienceApi();
    return await addJsonError(
        'fetchRecommendedItems',
        expApi.fetchRecommendedItems({
            sellingChannel: SellingChannelNamesModel.Weboa,
            menuType: 'ALLDAY',
            productsRecommendationRequestModel: payload,
        })
    );
};

export default fetchRecommendedProducts;
