export const isNewRelicAvailable = typeof window !== 'undefined' && !!window.newrelic;

const init = (): void => {
    const version = process.env.NEXT_PUBLIC_ARTIFACT_VERSION
        ? process.env.NEXT_PUBLIC_ARTIFACT_VERSION.split('-')[0]
        : 'no version';

    window.newrelic.setCustomAttribute('environment', process.env.NEXT_PUBLIC_ENVIRONMENT_NAME);
    window.newrelic.setCustomAttribute('labels.environment', process.env.NEXT_PUBLIC_ENVIRONMENT_NAME);
    window.newrelic.setCustomAttribute('labels.revision', version);

    window.newrelic.addRelease(version, version);
};

const logError = (error: Error | string, info?: { [key: string]: string | number }): void => {
    isNewRelicAvailable && window.newrelic.noticeError(error, info);
    console.error(error, info);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const logEvent = (name: string, attributes: { [key: string]: string | number | any }): void => {
    isNewRelicAvailable && window.newrelic.addPageAction(name, attributes);
};

export default {
    init,
    logError,
    logEvent,
};
