import {
    GetSurveyRequest,
    ISurveyModel,
    ISurveyRespondModel,
    SellingChannelNamesModel,
    UpdateSurveyRequest,
    WebExperienceApi,
} from '../../../@generated/webExpApi';
import createErrorWrapper from '../createErrorWrapper';
import { getAuthorizationConfig } from '../../helpers/getAuthorizationConfig';

class SurveyService {
    getSurveyAPI: (request: GetSurveyRequest) => Promise<ISurveyModel>;
    updateSurveyAPI: (request: UpdateSurveyRequest) => Promise<ISurveyModel>;

    constructor(JWT: string) {
        const expApi = new WebExperienceApi(getAuthorizationConfig(JWT));

        this.getSurveyAPI = createErrorWrapper<ISurveyModel, GetSurveyRequest>(
            'getSurvey',
            expApi.getSurvey.bind(expApi)
        );

        this.updateSurveyAPI = createErrorWrapper<ISurveyModel, UpdateSurveyRequest>(
            'updateSurvey',
            expApi.updateSurvey.bind(expApi)
        );
    }

    getSurvey(): Promise<ISurveyModel> {
        return this.getSurveyAPI({
            sellingChannel: SellingChannelNamesModel.Weboa,
        });
    }

    updateSurvey(payload: { surveyId: string; surveyRespond: ISurveyRespondModel[] }): Promise<ISurveyModel> {
        return this.updateSurveyAPI({
            surveyId: payload.surveyId,
            iSurveyRespondModel: payload.surveyRespond,
            sellingChannel: SellingChannelNamesModel.Weboa,
        });
    }
}

export default SurveyService;
