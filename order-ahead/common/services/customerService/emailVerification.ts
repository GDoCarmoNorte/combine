import { VerifyEmailRequest, WebExperienceApi } from '../../../@generated/webExpApi';
import createErrorWrapper from '../createErrorWrapper';

const expApi = new WebExperienceApi();

const verifyEmailApi = createErrorWrapper<void, VerifyEmailRequest>('verifyEmail', expApi.verifyEmail.bind(expApi));

const verifyEmailRequest = (payload: VerifyEmailRequest): Promise<void> => {
    return verifyEmailApi(payload);
};

export default verifyEmailRequest;
