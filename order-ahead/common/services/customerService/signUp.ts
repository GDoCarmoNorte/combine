import * as DomainCustomer from '../../../@generated/domainCustomer/apis/index';
import {
    CustomerSignUpResponseModel,
    TermsAndConditionsModelTypeEnum,
    MarketingRequestModelTypeEnum,
    SignUpBrandEnum,
} from '../../../@generated/domainCustomer';

import addJsonError from '../addJsonError';
import getBrandInfo from '../../../lib/brandInfo';

export interface ISignUpPayload {
    firstName: string;
    lastName: string;
    email: string;
    postalCode?: string;
    storeId?: string;
    storeName?: string;
    birthDate?: string;
}

const signUp = (payload: ISignUpPayload): Promise<CustomerSignUpResponseModel> => {
    const { brandId } = getBrandInfo();
    const mappedBrandId = brandId === 'Arbys' ? SignUpBrandEnum.Arb : SignUpBrandEnum[brandId];

    const domainCustomer = new DomainCustomer.CustomerControllerV2Api();

    return addJsonError(
        'signup',
        domainCustomer.signup({
            brandId: mappedBrandId,
            cHANNELID: TermsAndConditionsModelTypeEnum.Weboa,
            customerSignUpRequestModel: {
                firstName: payload.firstName,
                lastName: payload.lastName,
                email: payload.email,
                birthDate: payload.birthDate || undefined,
                preferences: {
                    postalCode: payload.postalCode,
                    marketing: [
                        {
                            type: MarketingRequestModelTypeEnum.Email,
                        },
                    ],
                    locations: [
                        {
                            id: payload.storeId,
                            isPreferred: true,
                        },
                    ],
                },
            },
        })
    );
};

export default signUp;
