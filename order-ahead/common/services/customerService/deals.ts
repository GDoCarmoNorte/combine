import {
    ClaimMissingPointsRequest,
    GetCustomerAccountRewardsCatalogRequest,
    GetCustomerAccountRewardsRequest,
    GetQRCodeForOfferRequest,
} from '../../../@generated/webExpApi/apis/WebExperienceApi';
import { WebExperienceApi } from '../../../@generated/webExpApi/apis/index';
import {
    IClaimMissingPointsResponseModel,
    IGetCustomerAccountRewardsCatalogResponseModel,
    IGetCustomerAccountRewardsResponseModel,
    SellingChannelNamesModel,
    IError500ExternalResponseModel,
} from '../../../@generated/webExpApi/models';

import createErrorWrapper from '../createErrorWrapper';
import { getAuthorizationConfig } from '../../helpers/getAuthorizationConfig';

// TODO need to refactor after implement common service to add JWT to secure routes

class DealsService {
    getCustomerAccountRewardsApi: (
        request: GetCustomerAccountRewardsRequest
    ) => Promise<IGetCustomerAccountRewardsResponseModel>;

    getQRCodeForOfferApi: (request: GetQRCodeForOfferRequest) => Promise<string>;

    getCustomerAccountRewardsCatalogApi: (
        request: GetCustomerAccountRewardsCatalogRequest
    ) => Promise<IGetCustomerAccountRewardsCatalogResponseModel>;

    claimPointsApi: (
        request: ClaimMissingPointsRequest
    ) => Promise<IClaimMissingPointsResponseModel> | Promise<IError500ExternalResponseModel>;

    constructor(JWT: string) {
        const expApi = new WebExperienceApi(getAuthorizationConfig(JWT));
        expApi.getQRCodeForOffer;

        this.getCustomerAccountRewardsApi = createErrorWrapper<
            IGetCustomerAccountRewardsResponseModel,
            GetCustomerAccountRewardsRequest
        >('getAccountDeals', expApi.getCustomerAccountRewards.bind(expApi));

        this.getCustomerAccountRewardsCatalogApi = createErrorWrapper<
            IGetCustomerAccountRewardsCatalogResponseModel,
            GetCustomerAccountRewardsCatalogRequest
        >('getCustomerAccountRewards', expApi.getCustomerAccountRewardsCatalog.bind(expApi));

        this.getQRCodeForOfferApi = createErrorWrapper<string, GetQRCodeForOfferRequest>(
            'getAccountDeals',
            expApi.getQRCodeForOffer.bind(expApi)
        );
        this.claimPointsApi = createErrorWrapper<IClaimMissingPointsResponseModel, ClaimMissingPointsRequest>(
            'claimPoints',
            expApi.claimMissingPoints.bind(expApi)
        );
    }

    getCustomerAccountRewards(
        payload: GetCustomerAccountRewardsCatalogRequest
    ): Promise<IGetCustomerAccountRewardsCatalogResponseModel> {
        const { sellingChannel, pointsBalance } = payload;

        return this.getCustomerAccountRewardsCatalogApi({ sellingChannel, pointsBalance });
    }

    getAccountDeals(payload: GetCustomerAccountRewardsRequest): Promise<IGetCustomerAccountRewardsResponseModel> {
        const { sellingChannel } = payload;

        return this.getCustomerAccountRewardsApi({ sellingChannel });
    }

    getQRCodeForOffer(payload: GetQRCodeForOfferRequest): Promise<string> {
        return this.getQRCodeForOfferApi({
            userOfferId: payload.userOfferId,
            sellingChannel: SellingChannelNamesModel.Weboa,
        });
    }

    claimPoints(
        payload: ClaimMissingPointsRequest
    ): Promise<IClaimMissingPointsResponseModel> | Promise<IError500ExternalResponseModel> {
        const {
            sellingChannel,
            iClaimMissingPointsFromClientModel: { checkNumber, claimNumber, date, locationId, subTotal },
        } = payload;
        return this.claimPointsApi({
            iClaimMissingPointsFromClientModel: {
                checkNumber,
                claimNumber,
                date,
                locationId,
                subTotal,
            },
            sellingChannel,
        });
    }
}

export default DealsService;
