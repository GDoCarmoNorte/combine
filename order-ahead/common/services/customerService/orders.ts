import {
    WebExperienceApi,
    IGetCustomerOrderHistoryResponseModel,
    GetOrdersHistoryRequest,
} from '../../../@generated/webExpApi';

import createErrorWrapper from '../createErrorWrapper';
import { getAuthorizationConfig } from '../../helpers/getAuthorizationConfig';

class CustomerOrdersService {
    private readonly getCustomerOrderHistory: (
        request: GetOrdersHistoryRequest
    ) => Promise<IGetCustomerOrderHistoryResponseModel>;

    constructor(JWT: string) {
        const expApi = new WebExperienceApi(getAuthorizationConfig(JWT));

        this.getCustomerOrderHistory = createErrorWrapper<
            IGetCustomerOrderHistoryResponseModel,
            GetOrdersHistoryRequest
        >('getOrdersHistory', expApi.getOrdersHistory.bind(expApi));
    }

    getOrderHistory(): Promise<IGetCustomerOrderHistoryResponseModel> {
        return this.getCustomerOrderHistory({});
    }
}

export default CustomerOrdersService;
