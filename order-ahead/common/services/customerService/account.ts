import { SellingChannelNamesModel } from '../../../@generated/webExpApi';
import {
    WebExperienceApi,
    UpdateCustomerAccountRequest,
    DeleteCustomerAccountRequest,
} from '../../../@generated/webExpApi/apis/index';
import {
    ITransformedGetCustomerAccountResponseForClientModel,
    ITransformedCustomerIdResponseForClientModel,
} from '../../../@generated/webExpApi/models';
import createErrorWrapper from '../createErrorWrapper';
import { getAuthorizationConfig } from '../../helpers/getAuthorizationConfig';
import addJsonError from '../addJsonError';

const initAccountService = (jwt) => {
    const expApi = new WebExperienceApi(getAuthorizationConfig(jwt));
    const getAccount = createErrorWrapper<ITransformedGetCustomerAccountResponseForClientModel, void>(
        'getCustomerAccount',
        expApi.getCustomerAccount.bind(expApi)
    );

    const updateAccount = async (
        requestParameters: UpdateCustomerAccountRequest
    ): Promise<ITransformedCustomerIdResponseForClientModel> => {
        return await addJsonError('updateCustomerAccount', expApi.updateCustomerAccount(requestParameters));
    };

    const getDeleteAccountApi = createErrorWrapper<
        ITransformedGetCustomerAccountResponseForClientModel,
        DeleteCustomerAccountRequest
    >('deleteCustomerAccount', expApi.deleteCustomerAccount.bind(expApi));

    const deleteAccount = async (): Promise<ITransformedGetCustomerAccountResponseForClientModel> => {
        return getDeleteAccountApi({
            sellingChannel: SellingChannelNamesModel.Weboa,
        });
    };

    return { getAccount, updateAccount, deleteAccount };
};
export { initAccountService };
