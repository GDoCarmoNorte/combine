import { defaultHttpStatusCodeErrorMap, NetworkErrorMessage } from './createErrorWrapper';

export const resetPassword = async (email: string): Promise<void> => {
    const NEXT_PUBLIC_AUTH_0_DOMAIN = process.env.NEXT_PUBLIC_AUTH_0_DOMAIN;
    const NEXT_PUBLIC_AUTH_0_CLIENT_ID = process.env.NEXT_PUBLIC_AUTH_0_CLIENT_ID;

    const body = {
        email,
        client_id: NEXT_PUBLIC_AUTH_0_CLIENT_ID,
        connection: 'firebase-auth',
    };

    try {
        const res = await fetch(`https://${NEXT_PUBLIC_AUTH_0_DOMAIN}/dbconnections/change_password`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify(body),
        });

        const errorMessage = res.status ? defaultHttpStatusCodeErrorMap[res.status] : NetworkErrorMessage;
        if (!res.ok) {
            return Promise.reject({ message: errorMessage });
        }
    } catch (err) {
        return Promise.reject({ message: NetworkErrorMessage });
    }
    return Promise.resolve();
};
