import {
    GmtCheckoutViewData,
    GmtQtyDecreaseData,
    GmtQtyIncreaseData,
    GtmActionFields,
    GtmCheckoutPaymentEvent,
    GtmComboSelectionEventData,
    GtmEventNames,
    GtmImpressionViewData,
    GtmMakeMyStoreEventData,
    GtmMapListLocationClickData,
    GtmMapLocationClickData,
    GtmModifierCustomizationEventData,
    GtmProductViewData,
    GtmRemoveFromCartData,
    GtmRestoreToCartData,
    GtmTransactionCompleteData,
    GtmTransactionInfoEventData,
    GtmUserIdData,
    IGtmBaseEventData,
    IGtmPickUpTimeData,
    IGtmSignInData,
    ModifierSelectionData,
    GtmErrorEvent,
    LocationOrderEvent,
    LocationChange,
    AccountDeletedSubmittedData,
    AccountDeletedFeedbackData,
} from './types';
import { AddToBagPayload, PutToBagPayload, ToggleIsOpenPayload } from '../../../redux/bag';
import { getRelatedProduct } from '../../../lib/domainProduct';
import { format, utcToZonedTime } from '../../../common/helpers/dateTime';
import { PDPTallyItem } from '../../../redux/pdp';
import { RootState } from '../../../redux/store';
import { selectProductSize } from '../../../redux/selectors/domainMenu';
import { gtmServicePush } from '../../../lib/gtmServicePush';

import { categoryNameModify } from '../../helpers/categoryNameModify';
import getBrandInfo from '../../../lib/brandInfo';
import { TInitialPaymentTypes } from '../../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';

const getPrice = (childItems: PDPTallyItem[], price: number) => {
    const childItemsTotalPrice = childItems && childItems.reduce((acc, item) => acc + item.price, 0);

    return price || childItemsTotalPrice;
};

const gtmService = {
    pushSignInSuccessEvent: (data: IGtmSignInData): void => {
        gtmServicePush(GtmEventNames.SignInSuccess, data);
    },

    pushSignInFailureEvent: (data: IGtmSignInData): void => {
        gtmServicePush(GtmEventNames.SignInFailure, data);
    },
    pushPageEvent: (data: IGtmBaseEventData): void => {
        gtmServicePush(GtmEventNames.PageEvent, data);
    },

    pushApiEvent: (data: IGtmBaseEventData): void => {
        gtmServicePush(GtmEventNames.ApiEvent, data);
    },

    pushImpressionViewEvent: (data: GtmImpressionViewData): void => {
        const products = data.products || [];

        gtmServicePush(GtmEventNames.ImpressionView, {
            ecommerce: {
                [GtmActionFields.Impressions]: products.map(({ productId, name, price }, index) => {
                    return {
                        id: productId,
                        name,
                        category: data?.category,
                        position: index + 1,
                        price: typeof price === 'string' ? price.replace(/\$/g, '') : price,
                    };
                }),
            },
        });
    },

    pushProductViewEvent: (data: GtmProductViewData): void => {
        const { product, price, category } = data;

        gtmServicePush(GtmEventNames.ProductView, {
            ecommerce: {
                [GtmActionFields.Detail]: {
                    actionField: {
                        list: 'Product View',
                    },
                    products: [
                        {
                            id: product?.fields?.productId,
                            name: product?.fields?.name,
                            quantity: 1,
                            price, // TODO: should check all price loading scripts
                            category,
                        },
                    ],
                },
            },
        });
    },

    pushRestoreItemToCardEvent: (data: GtmRestoreToCartData, state: RootState): void => {
        const { category } = data;

        gtmService.pushAddToCardEvent(
            {
                bagEntry: data,
                category: categoryNameModify(category),
                name: data.name,
            },
            state
        );
    },

    pushAddToCardEvent: (data: AddToBagPayload | PutToBagPayload, state: RootState): void => {
        const { category, name, sauce } = data;
        const cartItem: PDPTallyItem = 'bagEntry' in data ? data.bagEntry : data.pdpTallyItem;
        const { productId, quantity, price, childItems, reAddCart } = cartItem;
        if (childItems) {
            const sideFood: string = childItems.length >= 2 ? childItems[1].name : '';
            const sideDrink: string = childItems.length >= 3 ? childItems[2].name : '';
            gtmServicePush(GtmEventNames.AddToCart, {
                ecommerce: {
                    currencyCode: 'USD',
                    [GtmActionFields.Add]: {
                        products: [
                            {
                                id: productId,
                                quantity: quantity,
                                onceRemoved: !!reAddCart,
                                price: getPrice(childItems, price),
                                name,
                                category: categoryNameModify(category),
                                sideFood,
                                sideDrink,
                                ...sauce,
                            },
                        ],
                    },
                },
                comboName: name,
                comboSize: selectProductSize(state, productId),
                comboSide: sideFood,
                comboDrink: sideDrink,
                comboPrice: getPrice(childItems, null),
            });
        } else {
            gtmServicePush(GtmEventNames.AddToCart, {
                ecommerce: {
                    currencyCode: 'USD',
                    [GtmActionFields.Add]: {
                        products: [
                            {
                                id: productId,
                                quantity: quantity,
                                onceRemoved: !!reAddCart,
                                price: getPrice(childItems, price),
                                name,
                                category: categoryNameModify(category),
                                sideFood: 'none',
                                sideDrink: 'none',
                                ...sauce,
                            },
                        ],
                    },
                },
            });
        }
    },

    pushRemoveFromCartEvent: (data: GtmRemoveFromCartData): void => {
        const { price, quantity, productId, name, category, childItems, sauce } = data;

        gtmServicePush(GtmEventNames.RemoveFromCart, {
            ecommerce: {
                currencyCode: 'USD',
                [GtmActionFields.Remove]: {
                    products: [
                        {
                            id: productId,
                            quantity: quantity,
                            onceRemoved: false,
                            price: getPrice(childItems, price),
                            name,
                            category: categoryNameModify(category),
                            ...sauce,
                        },
                    ],
                },
            },
        });
    },

    pushCheckoutPaymentMethodType: (paymentMethod: TInitialPaymentTypes): void => {
        gtmServicePush(GtmEventNames.Checkout, {
            action: GtmActionFields.Click,
            label: paymentMethod,
        });
    },

    pushCheckoutViewEvent: (data: GmtCheckoutViewData[]): void => {
        const products = data || [];

        gtmServicePush(GtmEventNames.Checkout, {
            ecommerce: {
                [GtmActionFields.Checkout]: {
                    actionField: {
                        step: 1,
                        action: 'Checkout',
                    },
                    products: products.map(({ quantity, price, productId, name, category, childItems }) => {
                        return {
                            id: productId,
                            price: getPrice(childItems, price),
                            quantity,
                            category: categoryNameModify(category),
                            name,
                        };
                    }),
                },
            },
        });
    },

    pushCheckoutPaymentEvent: (data: GtmCheckoutPaymentEvent): void => {
        const {
            tallyOrder: { products = [] },
            bagEntries,
            step,
            action,
        } = data;

        const entries = bagEntries || [];

        gtmServicePush(GtmEventNames.Checkout, {
            ecommerce: {
                [GtmActionFields.Checkout]: {
                    actionField: {
                        step,
                        action,
                    },
                    products: products.map(({ quantity, price, productId, childItems }) => {
                        const entry = entries.reduce((acc, curr) => {
                            if (curr?.productId === productId) {
                                return {
                                    category: curr && categoryNameModify(curr.category),
                                    name: curr?.name,
                                };
                            }
                            return acc;
                        }, {});
                        return {
                            id: productId,
                            price: getPrice(childItems, price),
                            quantity,
                            ...entry,
                        };
                    }),
                },
            },
        });
    },

    pushTransactionCompleteEvent: (data: GtmTransactionCompleteData): void => {
        const { tallyOrder, orderId, productsById, domainProducts, bagEntries } = data || {};
        const { products = [], tax, total, sellingChannel } = tallyOrder || {};
        const brandId = getBrandInfo().brandId;
        const entries = bagEntries || [];

        gtmServicePush(GtmEventNames.TransactionComplete, {
            ecommerce: {
                [GtmActionFields.Purchase]: {
                    actionField: {
                        id: orderId,
                        affiliation: sellingChannel,
                        revenue: total,
                        tax,
                    },
                    products: products.map(({ productId, price, quantity, childItems }) => {
                        const category = entries.find((curr) => curr?.productId === productId)?.category;

                        return {
                            id: productId,
                            price: getPrice(childItems, price),
                            brand: brandId,
                            quantity,
                            name:
                                productsById[productId]?.fields?.name ||
                                getRelatedProduct(domainProducts[productId], productsById)?.fields?.name,
                            category: categoryNameModify(category),
                        };
                    }),
                },
            },
        });
    },

    pushQtyDecreaseEvent: (data: GmtQtyDecreaseData): void => {
        const { price, productId, name, childItems } = data;

        gtmServicePush(GtmEventNames.QtyDecrease, {
            id: productId,
            price: getPrice(childItems, price),
            name,
        });
    },

    pushQtyIncreaseEvent: (data: GmtQtyIncreaseData): void => {
        const { price, productId, name, childItems } = data;

        gtmServicePush(GtmEventNames.QtyIncrease, {
            id: productId,
            price: getPrice(childItems, price),
            name,
        });
    },

    pushCartOpenEvent: (data: ToggleIsOpenPayload): void => {
        const { isOpen } = data;

        isOpen && gtmServicePush(GtmEventNames.CartOpen);
    },

    pushModifyItemEvent: (): void => {
        gtmServicePush(GtmEventNames.ModifyItem);
    },

    pushAddMoreItemsEvent: (): void => {
        gtmServicePush(GtmEventNames.AddMoreItems);
    },

    pushSearchNewLocationEvent: (): void => {
        gtmServicePush(GtmEventNames.SearchNewLocation);
    },

    pushStartPickupOrderEvent: (): void => {
        gtmServicePush(GtmEventNames.StartPickupOrder);
    },

    pushMakeMyStoreEvent: (data: GtmMakeMyStoreEventData): void => {
        gtmServicePush(GtmEventNames.MakeMyStore, {
            storeNumber: data.id,
            storeName: data.name,
        });
    },

    pushMakeOnlineOrderingComingSoonEvent: (data: GtmMakeMyStoreEventData): void => {
        gtmServicePush(GtmEventNames.OnlineOrderingComingSoon, {
            storeNumber: data.id,
            storeName: data.name,
        });
    },
    pushEmailSignUp: (): void => {
        gtmServicePush(GtmEventNames.EmailSignUp);
    },

    pushGiftCardBalanceFormSubmitSuccess: (): void => {
        gtmServicePush(GtmEventNames.GiftCardBalanceFormSubmit);
    },

    pushUserId: (data: GtmUserIdData): void => {
        gtmServicePush(GtmEventNames.UserId, data);
    },

    pushTransactionInfoEvent: (data: GtmTransactionInfoEventData): void => {
        const locationTimezone = data.location.timezone;
        const locationLocalTime = utcToZonedTime(new Date(data.order.fulfillment.time), locationTimezone);

        gtmServicePush(GtmEventNames.TransactionInfo, {
            fulfillmentDate: format(locationLocalTime, 'MM/dd/yyyy'),
            fulfillmentTime: format(locationLocalTime, 'HH:mm'),
            fulfillmentType: data.tallyOrder.fulfillment.type,
            isAsap: data.request.orderData.isAsap,
            storeNumber: data.request.orderData.locationId,
            paymentMethod:
                data.request.payments[0].type === 'NO_PAYMENT' ? '' : data.request.payments[0].details.cardIssuer,
            loginType: data.loginType,
        });
    },

    pushModifierCustomizationEvent: (data: GtmModifierCustomizationEventData): void => {
        gtmServicePush(GtmEventNames.ModifierCustomization, {
            category: data.category,
            label: data.productName,
            action: data.modifierName,
        });
    },

    pushComboSelectionEvent: (data: GtmComboSelectionEventData): void => {
        gtmServicePush(GtmEventNames.ComboSelection, data);
    },

    pushMapClickEvent: (): void => {
        gtmServicePush(GtmEventNames.MapClick);
    },

    pushMapDoubleClickEvent: (): void => {
        gtmServicePush(GtmEventNames.MapDoubleClick);
    },

    pushMapDragStartEvent: (): void => {
        gtmServicePush(GtmEventNames.MapDragStart);
    },

    pushMapDragEvent: (): void => {
        gtmServicePush(GtmEventNames.MapDrag);
    },

    pushMapDragEndEvent: (): void => {
        gtmServicePush(GtmEventNames.MapDragEnd);
    },

    pushMapLocationClickEvent: (data: GtmMapLocationClickData): void => {
        gtmServicePush(GtmEventNames.MapLocationClick, { storeID: data.location.id });
    },

    pushMapListLocationClickEvent: (data: GtmMapListLocationClickData): void => {
        gtmServicePush(GtmEventNames.MapListLocationClick, { storeID: data.location.id });
    },

    pushPickupTimeSelection: (data: IGtmPickUpTimeData): void => {
        gtmServicePush(GtmEventNames.PickupTimeSelection, data);
    },

    pushModifierSelection: (data: ModifierSelectionData): void => {
        gtmServicePush(GtmEventNames.ModifierSelection, data);
    },

    pushError: (error: GtmErrorEvent): void => {
        gtmServicePush(GtmEventNames.Error, {
            error,
        });
    },

    pushTipSelection: (data: { tipAmount: number }): void => {
        gtmServicePush(GtmEventNames.Checkout, {
            action: GtmActionFields.TipSelection,
            label: data.tipAmount,
        });
    },

    pushLocationOrder: (data: LocationOrderEvent): void => {
        gtmServicePush(GtmEventNames.LocationOrderCTA, {
            data,
        });
    },

    pushLocationChange: (data: LocationChange): void => {
        gtmServicePush(GtmEventNames.LocationChange, {
            data,
        });
    },

    pushCheckInEvent: (storeId: number): void => {
        gtmServicePush(GtmEventNames.CheckIn, {
            action: GtmActionFields.Click,
            label: storeId,
        });
    },

    pushAccountDeletedFeedback: (data: AccountDeletedFeedbackData): void => {
        gtmServicePush(GtmEventNames.AccountDeletedFeedback, data);
    },

    pushAccountDeletedSubmitted: (data: AccountDeletedSubmittedData): void => {
        gtmServicePush(GtmEventNames.AccountDeletedSubmitted, data);
    },
};

export default gtmService;
