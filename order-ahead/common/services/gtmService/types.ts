import { Dictionary } from '@reduxjs/toolkit';
import { Entry } from 'contentful';

import { IProductFields } from '../../../@generated/@types/contentful';
import {
    ItemModel,
    OrderRequestModel,
    OrderResponseModel,
    TallyProductModel,
    TallyResponseModel,
} from '../../../@generated/webExpApi';

import { IProductItemById } from '../globalContentfulProps';
import { PickupAddress } from '../../../redux/orderLocation';
import { LocationWithDetailsModel } from '../locationService/types';

export enum GtmEventNames {
    PageEvent = 'PageEvent',
    ApiEvent = 'ApiEvent',
    ImpressionView = 'ImpressionView',
    ProductView = 'ProductView',
    AddToCart = 'AddToCart',
    RemoveFromCart = 'RemoveFromCart',
    Checkout = 'Checkout',
    TransactionInfo = 'TransactionInfo',
    TransactionComplete = 'TransactionComplete',
    CartOpen = 'CartOpen',
    ComboSelection = 'ComboSelection',
    ModifyItem = 'ModifyItem',
    ModifierCustomization = 'ModifierCustomization',
    AddMoreItems = 'AddMoreItems',
    SearchNewLocation = 'SearchNewLocation',
    StartPickupOrder = 'StartPickupOrder',
    MakeMyStore = 'MakeMyStore',
    OnlineOrderingComingSoon = 'OnlineOrderingComingSoon',
    QtyDecrease = 'QtyDecrease',
    QtyIncrease = 'QtyIncrease',
    EmailSignUp = 'EmailSignUp',
    GiftCardBalanceFormSubmit = 'giftCardBalanceFormSubmit',
    MapClick = 'mapClick',
    MapDoubleClick = 'mapDblClick',
    MapDragStart = 'mapDragStart',
    MapDrag = 'mapDrag',
    MapDragEnd = 'mapDragEnd',
    MapLocationClick = 'locationClick',
    MapListLocationClick = 'listLocationClick',
    SignInSuccess = 'signInSuccess',
    SignInFailure = 'signInFailure',
    UserId = 'userId',
    AccountCreationSuccess = 'accountCreationSuccess',
    PickupTimeSelection = 'pickupTimeSelection',
    ModifierSelection = 'modifierSelection',
    Error = 'error',
    LocationOrderCTA = 'locationOrderCTA',
    LocationChange = 'locationChange',
    CheckIn = 'CheckIn',
    AccountDeletedSubmitted = 'accountDeletedSubmitted',
    AccountDeletedFeedback = 'accountDeletedFeedback',
}

export enum GtmActionFields {
    Add = 'add',
    Remove = 'remove',
    Detail = 'detail',
    Checkout = 'checkout',
    Purchase = 'purchase',
    Impressions = 'impressions',
    Click = 'Click',
    TipSelection = 'Tip Selection',
    Delete = 'Delete',
}

export interface IGtmBaseEventData {
    cid?: string;
}

export interface IGtmSignInData {
    signInType: string;
}

export interface GtmProductViewData {
    product: Entry<IProductFields>;
    price: number | string;
    category: string;
}

export interface GtmMakeMyStoreEventData {
    id?: string;
    name?: string;
}

export interface GtmRemoveFromCartData extends TallyProductModel {
    category: string;
    name: string;
    sauce?: { [key: string]: string };
}

export type GtmRestoreToCartData = GtmRemoveFromCartData;
export type GmtCheckoutViewData = GtmRemoveFromCartData;
export type GmtQtyDecreaseData = GtmRemoveFromCartData;
export type GmtQtyIncreaseData = GtmRemoveFromCartData;

export interface GtmCheckoutPaymentEvent {
    tallyOrder: TallyResponseModel;
    bagEntries: GmtCheckoutViewData[];
    step?: number;
    action?: string;
}

export interface GtmImpressionProduct extends IProductFields {
    price?: string | number;
}

export interface GtmImpressionViewData {
    products: GtmImpressionProduct[];
    category: string;
}

export interface GtmTransactionCompleteData {
    tallyOrder: TallyResponseModel;
    domainProducts: Dictionary<ItemModel>;
    productsById: IProductItemById;
    orderId: string;
    bagEntries: GmtCheckoutViewData[];
}

export interface GtmTransactionInfoEventData {
    order: OrderResponseModel;
    request: OrderRequestModel;
    tallyOrder: TallyResponseModel;
    location: LocationWithDetailsModel;
    loginType: string;
}

export interface GtmModifierCustomizationEventData {
    category: GtmModifierCustomizationCategory;
    productName: string;
    modifierName: string;
}

export enum GtmModifierCustomizationCategory {
    addition = 'modifier addition',
    removal = 'modifier removal',
}

export interface GtmComboSelectionEventData {
    comboName: string;
    comboSize: string;
    comboSide: string;
    comboDrink: string;
    comboPrice: number;
}

export interface GtmMapLocationClickData {
    location: PickupAddress;
}

export type GtmMapListLocationClickData = GtmMapLocationClickData;

export enum SignInTypes {
    FACEBOOK = 'facebook',
    GOOGLE = 'google',
    APPLE = 'apple',
}

export enum SignInResultTypes {
    SUCCESS = 'signInSuccess',
    FAILURE = 'signInFailure',
}

export interface GtmUserIdData {
    userId: string;
    isLoggedIn: boolean;
    userAgent: string;
}

export interface IGtmPickUpTimeData {
    pickupDate: string;
    pickupTime: string;
}

export enum ModifierSelectionAction {
    ADD = 'add',
    REMOVE = 'remove',
    SIZE = 'size',
}
export interface ModifierSelectionData {
    action: ModifierSelectionAction;
    label: string;
}

export interface GtmErrorEvent {
    ErrorCategory: string;
    ErrorDescription: string;
}

export enum GtmErrorCategory {
    BAG_ERROR = 'Bag Error',
    BAG_ERROR_MAX_TOTAL = 'Bag Error - Max Total Exceeds',
    CHECKOUT_PAYMENT = 'Checkout Error - Payment',
    CHECKOUT_UNAVAILABLE_ITEMS = 'Checkout Error - Unavailable items',
    CHECKOUT_SUBMIT = 'Checkout Error - Submit Order',
    CHECKOUT_UNAVAILABLE_LOCATION = 'Checkout Error - Location Unavailable',
    EDIT_ACCOUNT = 'Edit Account Error',
    REWARDS = 'Rewards Error',
}

export enum GtmCategory {
    ORDER = 'Order',
    ACCOUNT = 'Account',
}

export interface LocationOrderEvent {
    category: GtmCategory.ORDER;
    action: string;
    label: string;
    date: string;
    time: string;
}

export interface LocationChange {
    category: GtmCategory.ORDER;
    action: string;
    label: string;
}

export interface AccountDeletedFeedbackData {
    category: GtmCategory.ACCOUNT;
    action: GtmActionFields.Delete;
    reason_1: string;
}
export interface AccountDeletedSubmittedData {
    category: GtmCategory.ACCOUNT;
    action: GtmActionFields.Delete;
    label: string;
}
