import { TErrorCodeModel, TTallyErrorCodeModel } from '../../@generated/webExpApi';
import addJsonError from './addJsonError';
import HttpStatusCode from './httpStatusCode';

export enum FetchError {
    NetworkError = 'NetworkError',
}

export const experiencingTechnicalDifficultiesError =
    'We are experiencing technical difficulties. Please try again later.';

export const defaultHttpStatusCodeErrorMap = {
    [HttpStatusCode.BAD_REQUEST]: experiencingTechnicalDifficultiesError,
    [HttpStatusCode.NOT_FOUND]: experiencingTechnicalDifficultiesError,
    [HttpStatusCode.INTERNAL_SERVER_ERROR]: experiencingTechnicalDifficultiesError,
    [HttpStatusCode.SERVICE_UNAVAILABLE]: experiencingTechnicalDifficultiesError,
};

export const NetworkErrorMessage =
    'Our connection was interrupted. Please check your internet connection or wait for us to try again.';

const defaultErrorMap = {
    [FetchError.NetworkError]: NetworkErrorMessage,
    ...defaultHttpStatusCodeErrorMap,
};

// don't add Generic/Validation/etc. in handledErrors it should handle specific case
export const handledErrors = [
    TTallyErrorCodeModel.ProductsNotAvailable,
    TTallyErrorCodeModel.DeliveryProviderDeliveryTimeNotAvailable,
    TTallyErrorCodeModel.OrderTimeNotValid,
    TTallyErrorCodeModel.LocationNotAvailable,
    TTallyErrorCodeModel.ExceededMaxOrderAmount,
    TTallyErrorCodeModel.QuantitiesModifierGroupNotValid,
    TTallyErrorCodeModel.QuantitiesModifiersNotValid,
    TTallyErrorCodeModel.ProductsNotAvailable,
    TErrorCodeModel.PaymentAmountLower,
    TErrorCodeModel.DeliveryOrderInvalidPhone,
    TErrorCodeModel.PaymentFailure,
    TErrorCodeModel.PaymentAmountLower,
    TErrorCodeModel.PaymentAmountHigher,
    TErrorCodeModel.PaymentNotValid,
    TErrorCodeModel.InvalidFutureDate,
    TErrorCodeModel.LocationNotAvailable,
    TErrorCodeModel.InvalidPastDate,
    TErrorCodeModel.InvalidPastDateBeforeEnrollment,
    TErrorCodeModel.AlreadyAwarded,
    TErrorCodeModel.EmailNotVerified,
    TErrorCodeModel.CheckNotFound,
];

export class RequestError extends Error {
    code: string;

    constructor(public status: number | null, message: string, code?: string) {
        super(message);
        this.code = code;
    }
}

const createErrorHandler = <T, R>(
    api: string,
    apiCall: (args: R) => Promise<T>,
    errorMap: Record<string, string> = {}
): ((args: R) => Promise<T>) => {
    return async (args: R) => {
        return addJsonError<T>(api, apiCall(args)).catch((error) => {
            if (handledErrors.includes(error.code)) {
                return error;
            }

            if (!error.status) {
                throw new RequestError(
                    null,
                    errorMap[FetchError.NetworkError] || defaultErrorMap[FetchError.NetworkError]
                );
            }

            throw new RequestError(
                error.status,
                errorMap[error.status] || defaultErrorMap[error.status] || errorMap.default,
                error?.code
            );
        });
    };
};

export default createErrorHandler;
