import {
    IGetCustomerAccountPaymentMethodsResponseModel,
    WebExperienceApi,
    DeleteCreditCardRequest,
} from '../../@generated/webExpApi';

import { getAuthorizationConfig } from '../helpers/getAuthorizationConfig';
import addJsonError from './addJsonError';

const initPaymentMethodsService = (jwt: string) => {
    const expApi = new WebExperienceApi(getAuthorizationConfig(jwt));

    const fetchPaymentMethods = async (): Promise<IGetCustomerAccountPaymentMethodsResponseModel> => {
        return await addJsonError('fetchPaymentMethods', expApi.getPaymentMethod());
    };

    const deleteCreditCard = async (deleteCreditCardModel: DeleteCreditCardRequest) => {
        return await addJsonError('deleteCreditCard', expApi.deleteCreditCard(deleteCreditCardModel));
    };

    return { fetchPaymentMethods, deleteCreditCard };
};

export { initPaymentMethodsService };
