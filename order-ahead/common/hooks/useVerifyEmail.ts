import { VerifyEmailRequest } from '../../@generated/webExpApi';
import verifyEmailRequest from '../services/customerService/emailVerification';

export default function useVerifyEmail() {
    const verifyEmail = async (payload: VerifyEmailRequest): Promise<void> => {
        const response = await verifyEmailRequest(payload);
        if (response !== undefined) {
            throw new Error();
        }
    };

    return {
        verifyEmail,
    };
}
