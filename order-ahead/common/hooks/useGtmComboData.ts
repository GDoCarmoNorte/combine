import { useDomainMenuSelectors } from '../../redux/hooks/domainMenu';
import { PDPTallyItem } from '../../redux/pdp';
import { IDisplayFullProduct } from '../../redux/types';
import { GtmComboSelectionEventData } from '../services/gtmService/types';

export default function useGtmComboData(
    tallyItem: PDPTallyItem,
    displayProduct: IDisplayFullProduct
): GtmComboSelectionEventData | undefined {
    const {
        selectProductSize,
        selectProductGroupByProductId,
        selectProductById,
        selectProductGroupByName,
    } = useDomainMenuSelectors();

    const isCombo = !!tallyItem.childItems;

    if (!isCombo) return undefined;

    const drinkMainGroupName = 'Drink';
    const productDetails = tallyItem.childItems.map((i) => selectProductById(i.productId));
    const comboSide = productDetails.find((product) => selectProductGroupByProductId(product?.id)?.name === 'Side');
    const drinkMainGroupId = selectProductGroupByName(drinkMainGroupName)?.id;
    const comboDrink = productDetails.find((product) => {
        if (!product?.id) {
            return false;
        }
        const productGroup = selectProductGroupByProductId(product.id);
        return productGroup?.name === drinkMainGroupName || productGroup?.parentGroupId === drinkMainGroupId;
    });

    return {
        comboName: displayProduct.displayName,
        comboSize: selectProductSize(displayProduct.productId),
        comboSide: comboSide?.name,
        comboDrink: comboDrink?.name,
        comboPrice: displayProduct.price,
    };
}
