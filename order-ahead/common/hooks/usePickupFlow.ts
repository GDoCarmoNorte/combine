import { useState, useEffect, useMemo, KeyboardEvent, ChangeEvent } from 'react';
import { useRouter } from 'next/router';

import { PickupAddress } from '../../redux/orderLocation';
import { useOrderLocation, useDomainMenu, useTallyOrder } from '../../redux/hooks';

import {
    GTM_MAP_LIST_LOCATION_CLICK,
    GTM_MAP_LOCATION_CLICK,
    GTM_SEARCH_NEW_LOCATION,
} from '../services/gtmService/constants';

import { useAppDispatch } from '../../redux/store';
import usePickupLocationsSearch from './usePickupLocationsSearch';
import { ILocationWithDetailsModel } from '../../@generated/webExpApi';
import { LoadingStatusEnum } from '../types';
import { getPreviousLocationData } from '../helpers/getPreviousLocationData';

interface IUsePickupFlow {
    locationsList: ILocationWithDetailsModel[];
    selectedLocation: ILocationWithDetailsModel;
    currentLocation: ILocationWithDetailsModel;
    isAbleToFetchMoreLocations: boolean;
    locationsSearchStatus: LoadingStatusEnum;
    moreLocationsFetchStatus: LoadingStatusEnum;
    locationQuery: string;
    searchInputValue: string;
    onMapLocationSelect: (location: PickupAddress) => void;
    onViewMoreLocationsClick: () => void;
    onSearchClick: () => void;
    onSearchInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onSearchInputKeyPress: (e: KeyboardEvent<HTMLInputElement>) => void;
    onLocationSelect: (location: PickupAddress) => void;
    onLocationSet: (location: PickupAddress) => void;
    onUseMyLocationClick: () => void;
}

const usePickupFlow = (): IUsePickupFlow => {
    const [searchValue, setSearchValue] = useState('');
    const [timestampSearchValue, setTimestampSearchValue] = useState<number | null>(null);
    const [selectedLocation, setSelectedLocation] = useState<PickupAddress | null>(null);
    const dispatch = useAppDispatch();

    const {
        actions: { setPickupLocation, setPreviousLocation },
        pickupAddress: currentLocation,
        method,
        deliveryAddress,
    } = useOrderLocation();
    const {
        actions: { getDomainMenu },
    } = useDomainMenu();
    const {
        searchLocationsByQuery,
        searchLocationsByCoordinates,
        searchLocationsByUserGeolocation,
        fetchMoreLocations,
        resetSearchResult,
        locationsSearchStatus,
        locationsSearchResult,
        moreLocationsFetchStatus,
        isAbleToFetchMoreLocations,
    } = usePickupLocationsSearch();
    const { setUnavailableTallyItems } = useTallyOrder();

    const router = useRouter();
    const locationQuery = router.query.q as string;
    const shouldShowCurrentLocationInTop = !!(router.query.intop as string);

    let myLocationSSValue: string | null;

    if (typeof window !== 'undefined') {
        myLocationSSValue = sessionStorage.getItem('myLocation');
    }

    useEffect(() => {
        setSearchValue(locationQuery);
        if (locationQuery) {
            searchLocationsByQuery(locationQuery);
            dispatch({ type: GTM_SEARCH_NEW_LOCATION });
        } else if (locationQuery === '') {
            resetSearchResult();
        }
        // infinity loop
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [locationQuery, timestampSearchValue]);

    useEffect(() => {
        const [locationLat, locationLng] = (myLocationSSValue && myLocationSSValue.split(',')) || [];

        if (!locationQuery && locationLng && locationLat) {
            searchLocationsByCoordinates({ lat: Number(locationLat), lng: Number(locationLng) });
        }
        // to much calls of this effect with all deps
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [myLocationSSValue, locationQuery]);

    const locationsList = useMemo(() => {
        const list = locationsSearchResult?.locations || [];
        if (!shouldShowCurrentLocationInTop || timestampSearchValue) {
            return list;
        }

        const currentLocationFromSearch = currentLocation && list.find((it) => it.id === currentLocation.id);
        if (currentLocationFromSearch) {
            const filtered = list.filter((it) => it.id !== currentLocationFromSearch.id);
            return [currentLocationFromSearch, ...filtered];
        }
        return list;
    }, [currentLocation, locationsSearchResult, shouldShowCurrentLocationInTop, timestampSearchValue]);

    const onLocationSet = (place: PickupAddress) => {
        setPreviousLocation(getPreviousLocationData({ method, pickupAddress: currentLocation, deliveryAddress }));
        setPickupLocation(place);
        getDomainMenu(place);
        setUnavailableTallyItems([]);
    };

    const handleSearch = () => {
        if (locationsSearchStatus === LoadingStatusEnum.Loading) {
            return;
        }

        setSelectedLocation(null);

        // to update data when value is not changed
        if (searchValue === locationQuery) {
            setTimestampSearchValue(Date.now());
        }

        // TODO update next-page-tester to support objects in router.push
        router.push(`/locations?q=${searchValue || ''}`, null, { shallow: true });
    };

    const onViewMoreLocationsClick = () => {
        fetchMoreLocations();
    };

    const onMapLocationSelect = (location: PickupAddress) => {
        setSelectedLocation(location);
        dispatch({ type: GTM_MAP_LOCATION_CLICK, payload: { location } });
    };

    const onLocationSelect = (location: PickupAddress) => {
        setSelectedLocation(location);
        dispatch({ type: GTM_MAP_LIST_LOCATION_CLICK, payload: { location } });
    };

    const onSearchInputKeyPress = (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            handleSearch();
        }
    };

    const onSearchInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchValue(e.target.value);
    };

    const onUseMyLocationClick = () => {
        searchLocationsByUserGeolocation();
    };

    return {
        locationsList,
        selectedLocation,
        currentLocation,
        isAbleToFetchMoreLocations,
        locationsSearchStatus,
        moreLocationsFetchStatus,
        locationQuery,
        searchInputValue: searchValue,
        onMapLocationSelect,
        onViewMoreLocationsClick,
        onSearchClick: handleSearch,
        onSearchInputChange,
        onSearchInputKeyPress,
        onLocationSelect,
        onLocationSet,
        onUseMyLocationClick,
    };
};

export default usePickupFlow;
