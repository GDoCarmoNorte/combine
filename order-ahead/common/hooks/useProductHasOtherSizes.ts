import { useProductSizes } from '../../redux/hooks/domainMenu';

export const useProductHasOtherSizes = (id: string): boolean => {
    const selections = useProductSizes(id);

    return !!selections && selections?.length > 1;
};
