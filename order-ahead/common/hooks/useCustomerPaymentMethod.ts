import { useState, useEffect, useMemo } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { isCustomerPaymentMethodEnabled } from '../../lib/getFeatureFlags';
import { authorizationHeaderBuilder } from '../helpers/accountHelper';
import { initPaymentMethodsService } from '../services/paymentMethods';
import { useNotifications } from '../../redux/hooks';

import { IGetCustomerAccountPaymentMethodsResponseModel, DeleteCreditCardRequest } from '../../@generated/webExpApi';

interface IUseCustomerPaymentMethod {
    loading: boolean;
    handleSetLoading: (boolean: boolean) => void;
    paymentMethods: IGetCustomerAccountPaymentMethodsResponseModel | null;
    handleDeletePaymentMethods: (jwtToken: string) => void;
    handleFetchPaymentMethods: () => void;
}

export const useCustomerPaymentMethod = (): IUseCustomerPaymentMethod => {
    const [token, setToken] = useState(null);
    const [paymentMethods, setPaymentMethods] = useState<IGetCustomerAccountPaymentMethodsResponseModel>(null);
    const [loading, setLoading] = useState<boolean>(true);

    const { isAuthenticated, user, getIdTokenClaims } = useAuth0();
    const {
        actions: { enqueueError },
    } = useNotifications();

    const { fetchPaymentMethods, deleteCreditCard } = useMemo(
        () => initPaymentMethodsService(authorizationHeaderBuilder(token)),
        [token]
    );

    const handleFetchPaymentMethods = async () => {
        try {
            setLoading(true);
            await fetchPaymentMethods().then((methods) => {
                setPaymentMethods(methods);
            });
            setLoading(false);
        } catch (error) {
            enqueueError({
                message: 'Something went wrong, Please try again later.',
            });
            setLoading(false);
        }
    };

    useEffect(() => {
        if (isAuthenticated && isCustomerPaymentMethodEnabled()) {
            getIdTokenClaims().then((res) => {
                setToken(res?.__raw);
            });
        }
    }, [isAuthenticated, user, getIdTokenClaims]);

    useEffect(() => {
        if (isAuthenticated && token && isCustomerPaymentMethodEnabled()) {
            fetchPaymentMethods().then((methods) => {
                setPaymentMethods(methods);
                setLoading(false);
            });
        } else {
            setLoading(false);
            setPaymentMethods(null);
        }
    }, [isAuthenticated, token, fetchPaymentMethods]);

    const handleDeletePaymentMethods = async (jwtToken: string) => {
        try {
            setLoading(true);
            const deleteCreditCardModel: DeleteCreditCardRequest = {
                iDeleteCreditCardRequestModel: {
                    token: jwtToken,
                },
            };
            await deleteCreditCard(deleteCreditCardModel).then(() => handleFetchPaymentMethods());
        } catch (error) {
            enqueueError({
                message: 'Something went wrong, Please try again later.',
            });
            setLoading(false);
        }
    };

    const handleSetLoading = (boolean: boolean) => {
        setLoading(boolean);
    };

    return { paymentMethods, loading, handleFetchPaymentMethods, handleDeletePaymentMethods, handleSetLoading };
};
