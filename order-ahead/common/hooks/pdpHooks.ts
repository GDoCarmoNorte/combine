import { useEffect, useState } from 'react';
import { IDisplayFullProduct, ProductTypesEnum } from '../../redux/types';
import { useAppDispatch } from '../../redux/store';
import { GTM_COMBO_SELECTION } from '../services/gtmService/constants';
import { PDPTallyItem } from '../../redux/pdp';
import useGtmComboData from './useGtmComboData';

export const useComboPriceIncreaseAnalytics = (
    pdpTallyItem: PDPTallyItem,
    displayProduct: IDisplayFullProduct,
    productPath: string
): void => {
    const isCombo = displayProduct?.productType === ProductTypesEnum.Meal;

    const dispatch = useAppDispatch();
    const [initialComboPrice, setInitialComboPrice] = useState(null);
    const gtmComboData = useGtmComboData(pdpTallyItem, displayProduct);

    const getComboItems = (tallyItem: PDPTallyItem) => {
        if (!isCombo) return null;

        const selectedComboProductDetailsIds = tallyItem.childItems.map((i) => i.productId);

        // stringify to avoid rerenders when displayProduct object is passed with new reference
        return JSON.stringify(selectedComboProductDetailsIds);
    };

    const getMainProductModifiers = (tallyItem: PDPTallyItem) => {
        if (!isCombo) return null;
        const mainComboProduct = tallyItem.childItems[0];

        const selectedModifiersIds = mainComboProduct.modifierGroups.reduce(
            (modifiers, group) => [...modifiers, ...group.modifiers.map((modifier) => modifier.productId)],
            []
        );

        // stringify to avoid rerenders when displayProduct object is passed with new reference
        return JSON.stringify(selectedModifiersIds);
    };

    const comboItems = getComboItems(pdpTallyItem);
    const mainProductModifiers = getMainProductModifiers(pdpTallyItem);

    useEffect(() => {
        if (!isCombo) return;
        setInitialComboPrice(displayProduct.price);
    }, [productPath, displayProduct.price, isCombo]);

    useEffect(() => {
        // run effect only for meal products
        if (!isCombo || initialComboPrice === null) return;
        if (initialComboPrice < displayProduct.price) {
            dispatch({
                type: GTM_COMBO_SELECTION,
                payload: gtmComboData,
            });
        }
    }, [displayProduct.price, comboItems, mainProductModifiers, isCombo, initialComboPrice, dispatch, gtmComboData]);
};
