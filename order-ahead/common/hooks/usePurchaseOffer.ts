import { useEffect, useRef, useState } from 'react';
import {
    IError500ExternalResponseModel,
    IPurchaseCustomerAccountRewardResponseModel,
    TErrorCodeModel,
} from '../../@generated/webExpApi';
import { useLoyalty, useNotifications, useRewards } from '../../redux/hooks';

import { useDealsService } from './useDealsService';
import useLoyaltyService from './useLoyaltyService';
import { useRewardsService } from './useRewardsService';
import { useRewardsActivityHistory } from './useRewardsActivityHistory';
import { useGtmErrorEvent } from './useGtmErrorEvent';
import { GtmErrorCategory } from '../services/gtmService/types';

interface IUsePurchaseOffer {
    loading: boolean;
    lastPurchaseData: IPurchaseCustomerAccountRewardResponseModel;
    error: string;
    purchaseOffer: (code: string) => void;
    cleanLastPurchaseData: () => void;
    errorCode: TErrorCodeModel;
    cleanErrorCode: () => void;
}

export const usePurchaseOffer = (): IUsePurchaseOffer => {
    const isMounted = useRef(false);
    const { pushGtmErrorEvent } = useGtmErrorEvent();

    useEffect(() => {
        isMounted.current = true;
        return () => {
            isMounted.current = false;
        };
    }, []);

    const { getRewardsActivityHistory } = useRewardsActivityHistory();
    const rewardsService = useRewardsService();
    const loyaltyService = useLoyaltyService();
    const dealsService = useDealsService();
    const {
        actions: { enqueueError },
    } = useNotifications();

    const {
        actions: { setLoyaltyPoints },
    } = useLoyalty();

    const {
        lastPurchasedCertificate,
        actions: { setRewards, setLastPurchasedCertificate },
    } = useRewards();

    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string | null>(null);
    const [errorCode, setErrorCode] = useState<TErrorCodeModel>(null);

    const purchaseOffer = async (code: string) => {
        setLoading(true);
        setError(null);
        setLastPurchasedCertificate(null);

        try {
            try {
                const response = await rewardsService.purchaseOffer(code);
                const errorResponse = response as IError500ExternalResponseModel;
                if (errorResponse.code) {
                    setErrorCode(errorResponse.code);
                    setError(errorResponse.message);
                } else {
                    setLastPurchasedCertificate(response as IPurchaseCustomerAccountRewardResponseModel);
                }
            } catch (error) {
                setError(error);
            }

            try {
                const pointsData = await loyaltyService.getAccountLoyaltyPoints({});
                setLoyaltyPoints(pointsData);
            } catch {
                enqueueError({
                    message: "We couldn't display your points balance. Please try to update the page.",
                });
            }

            try {
                const rewardsData = await dealsService.getAccountDeals({});
                setRewards(rewardsData);
            } catch {
                const message = "We couldn't display your rewards. Please try again later.";
                enqueueError({ message });
                pushGtmErrorEvent({
                    ErrorCategory: GtmErrorCategory.REWARDS,
                    ErrorDescription: message,
                });
            }

            await getRewardsActivityHistory();
        } finally {
            // checking if component is mounted since it's possible that the component is unmounted before the async call is finished
            // (e.g. calling setLoyaltyPoints triggers dataProvider to retrieve loyalty points and usePurchaseOffer consumer is unmounted)
            if (isMounted.current) {
                setLoading(false);
            }
        }
    };

    const cleanLastPurchaseData = () => setLastPurchasedCertificate(null);

    const cleanErrorCode = () => {
        setErrorCode(null);
        setError(null);
    };

    return {
        loading,
        lastPurchaseData: lastPurchasedCertificate,
        error,
        purchaseOffer,
        cleanLastPurchaseData,
        errorCode,
        cleanErrorCode,
    };
};
