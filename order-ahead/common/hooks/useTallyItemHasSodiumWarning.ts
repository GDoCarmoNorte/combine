import { useProducts } from '../../redux/hooks/domainMenu';
import { PDPTallyItem } from '../../redux/pdp';
import { getModifierGroupsSodiumValue, getProductSodiumValue } from '../helpers/sodiumWarningHelper';
import { useSodiumLegalWarning } from './useSodiumLegalWarning';

export const useTallyItemHasSodiumWarning = (pdpTallyItem: PDPTallyItem): boolean => {
    const products = useProducts();
    const sodiumLegalWarning = useSodiumLegalWarning();

    const productSodium = getProductSodiumValue(products?.[pdpTallyItem.productId]);

    const modifiersSodium = getModifierGroupsSodiumValue(pdpTallyItem.modifierGroups, products);

    const childItemsSodium = (pdpTallyItem.childItems || []).reduce((acc, childItem) => {
        const childSodium = getProductSodiumValue(products?.[childItem.productId]);
        const childModifiersSodium = getModifierGroupsSodiumValue(pdpTallyItem.modifierGroups, products);

        return acc + childSodium + childModifiersSodium;
    }, 0);

    const totalSodium = productSodium + modifiersSodium + childItemsSodium;

    return totalSodium > sodiumLegalWarning?.additionalProperties?.threshold;
};
