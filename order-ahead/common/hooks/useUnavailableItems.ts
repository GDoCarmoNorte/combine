import { useState, useEffect } from 'react';
import { ItemModifierGroupModel, TallyModifierModel, TallyProductModel } from '../../@generated/webExpApi';
import { useBag } from '../../redux/hooks';
import { useProducts } from '../../redux/hooks/domainMenu';

export interface IUnavailableItem {
    productId: string;
    lineItemId: number;
    modifierIds: string[];
    subModifierIds: string[];
    isWholeProductUnavailable: boolean;
    isDefaultModifiersUnavailable: boolean;
    isTouched?: boolean;
}

export interface IUseUnavailableItems {
    unavailableItems: IUnavailableItem[];
    setTouched: (lineItemId: number) => void;
}

// An unavailable ID can be a 'product' ID as well as a 'modifier' ID
export const useUnavailableItems = (unavailableIds: string[]): IUseUnavailableItems => {
    const [unavailableItems, setUnavailableItems] = useState<IUnavailableItem[]>([]);
    const { bagEntries } = useBag();
    const products = useProducts();

    const isDefaultItemUnavailable = (
        domainModifierGroup: ItemModifierGroupModel,
        bagEntity: TallyProductModel | TallyModifierModel
    ): boolean => {
        const bagModifierGroup = bagEntity.modifierGroups?.find(
            (group) => group.productId === domainModifierGroup.productGroupId
        );

        if (!bagModifierGroup) return false;

        const availableBugModifiers =
            bagModifierGroup?.modifiers.filter((modifier) => !unavailableIds?.includes(modifier.productId)) || [];

        const totalSubCount = availableBugModifiers?.reduce(
            (totalCount, modifier) => totalCount + (modifier.quantity || 0),
            0
        );

        return totalSubCount < domainModifierGroup.min;
    };

    useEffect(() => {
        setUnavailableItems(
            bagEntries.reduce((unavailableItems, bagEntry) => {
                if (!unavailableIds?.length) return unavailableItems;

                const isWholeProductUnavailable = unavailableIds?.includes(bagEntry.productId);

                const unavailableModifierIds =
                    bagEntry.modifierGroups
                        ?.flatMap((modifierGroup) =>
                            (modifierGroup.modifiers || []).filter((modifier) =>
                                unavailableIds?.includes(modifier.productId)
                            )
                        )
                        .map((item) => item.productId) || [];

                const unavailableSubModifierIds =
                    bagEntry.modifierGroups
                        ?.flatMap((modifierGroup) =>
                            (modifierGroup.modifiers || [])
                                .filter((modifier) => modifier?.modifierGroups)
                                .flatMap((modifier) =>
                                    modifier?.modifierGroups.flatMap((modifierGroup) =>
                                        (modifierGroup.modifiers || []).filter((modifier) =>
                                            unavailableIds?.includes(modifier.productId)
                                        )
                                    )
                                )
                        )
                        .map((item) => item.productId) || [];

                if (!(isWholeProductUnavailable || unavailableModifierIds.length || unavailableSubModifierIds.length)) {
                    return unavailableItems;
                }

                const isDefaultModifiersUnavailable = products[bagEntry.productId]?.itemModifierGroups
                    ?.filter((modifierGroup) => modifierGroup.min > 0)
                    ?.some((domainModifierGroup) => {
                        if (isDefaultItemUnavailable(domainModifierGroup, bagEntry)) {
                            return true;
                        }

                        // check is product has unavailable default submodidiers

                        const modifierGroup = bagEntry.modifierGroups?.find(
                            (bagModifierGroup) => bagModifierGroup.productId === domainModifierGroup.productGroupId
                        );

                        const modifierWithGroups =
                            modifierGroup?.modifiers.filter((modifier) => modifier?.modifierGroups) || [];

                        const isDefaultSubModifiersUnavailable = modifierWithGroups.some((bagModifier) =>
                            products[bagModifier.productId]?.itemModifierGroups
                                ?.filter((domainSubModifierGroup) => domainSubModifierGroup.min > 0)
                                ?.some((domainSubModifierGroup) =>
                                    isDefaultItemUnavailable(domainSubModifierGroup, bagModifier)
                                )
                        );

                        return isDefaultSubModifiersUnavailable;
                    });

                if (isWholeProductUnavailable) {
                    return [
                        ...unavailableItems,
                        {
                            isWholeProductUnavailable: true,
                            isDefaultModifiersUnavailable: false,
                            productId: bagEntry.productId,
                            lineItemId: bagEntry.lineItemId,
                            modifierIds: [],
                            subModifierIds: [],
                        },
                    ];
                }

                if (isDefaultModifiersUnavailable) {
                    return [
                        ...unavailableItems,
                        {
                            isWholeProductUnavailable: false,
                            isDefaultModifiersUnavailable: true,
                            productId: bagEntry.productId,
                            lineItemId: bagEntry.lineItemId,
                            modifierIds: unavailableModifierIds,
                            subModifierIds: unavailableSubModifierIds,
                        },
                    ];
                }

                if (unavailableSubModifierIds.length && unavailableModifierIds.length) {
                    return [
                        ...unavailableItems,
                        {
                            isDefaultModifiersUnavailable: false,
                            isWholeProductUnavailable: false,
                            productId: bagEntry.productId,
                            lineItemId: bagEntry.lineItemId,
                            modifierIds: unavailableModifierIds,
                            subModifierIds: unavailableSubModifierIds,
                        },
                    ];
                }

                if (unavailableModifierIds.length) {
                    return [
                        ...unavailableItems,
                        {
                            isDefaultModifiersUnavailable: false,
                            isWholeProductUnavailable: false,
                            productId: bagEntry.productId,
                            lineItemId: bagEntry.lineItemId,
                            modifierIds: unavailableModifierIds,
                            subModifierIds: [],
                        },
                    ];
                }

                if (unavailableSubModifierIds.length) {
                    return [
                        ...unavailableItems,
                        {
                            isDefaultModifiersUnavailable: false,
                            isWholeProductUnavailable: false,
                            productId: bagEntry.productId,
                            lineItemId: bagEntry.lineItemId,
                            modifierIds: [],
                            subModifierIds: unavailableSubModifierIds,
                        },
                    ];
                }

                return unavailableItems;
            }, [])
        );
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [unavailableIds]);

    const setTouched = (lineItemId: number) => {
        const bagEntry = bagEntries.find((bagEntry) => bagEntry.lineItemId === lineItemId);

        setUnavailableItems(
            unavailableItems.map((unavailableItem) =>
                unavailableItem.productId === bagEntry.productId && unavailableItem.lineItemId === bagEntry.lineItemId
                    ? { ...unavailableItem, isTouched: true }
                    : unavailableItem
            )
        );
    };

    return {
        unavailableItems,
        setTouched,
    };
};
