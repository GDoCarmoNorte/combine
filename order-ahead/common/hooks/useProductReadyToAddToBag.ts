import { selectIsPDPProductReadyToAddToBag } from '../../redux/selectors/pdp';
import { useAppSelector } from '../../redux/store';

export const useProductReadyToAddToBag = (): boolean => {
    return useAppSelector(selectIsPDPProductReadyToAddToBag);
};
