import { useRewards } from '../../redux/hooks';
import { getDefaultDate } from '../helpers/getDefaultActivityHistoryDate';

import { useRewardsService } from './useRewardsService';

interface IUseRewardsActivityHistory {
    getRewardsActivityHistory: () => void;
}

export const useRewardsActivityHistory = (): IUseRewardsActivityHistory => {
    const rewardsService = useRewardsService();

    const {
        actions: { setRewardsActivityHistory, setRewardsActivityHistoryLoading },
    } = useRewards();

    const getRewardsActivityHistory = async () => {
        setRewardsActivityHistoryLoading(true);
        try {
            const activityHistoryData = await rewardsService.getRewardsActivityHistory(getDefaultDate());
            setRewardsActivityHistory(activityHistoryData);
        } catch (error) {
            setRewardsActivityHistory([]);
        } finally {
            setRewardsActivityHistoryLoading(false);
        }
    };

    return { getRewardsActivityHistory };
};
