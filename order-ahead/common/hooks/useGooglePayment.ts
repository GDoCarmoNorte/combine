import { useState, useEffect, useMemo } from 'react';
import getBrandInfo from '../../lib/brandInfo';
import createErrorWrapper from '../services/createErrorWrapper';
import googlePayStyles from '../constants/googlePayStyles';
import {
    GoogleButtonColorEnumModel,
    GoogleButtonTypeEnumModel,
    InitGooglePaymentRequest,
    IPaymentInitResponseModel,
    WebExperienceApi,
} from '../../@generated/webExpApi';
import { injectScriptOnce } from '../../lib/injectScriptOnce';
import { useConfiguration } from './useConfiguration';

interface IInitPaymentProps {
    locationId: string;
    totalAmount: number;
    submitButtonColor?: GoogleButtonColorEnumModel;
    submitButtonType?: GoogleButtonTypeEnumModel;
}

export interface UseGooglePaymentHook {
    payment?: IPaymentInitResponseModel;
    error: Error;
    isLoading: boolean;
    isCompatible: boolean;
    reset: () => void;
    initPayment: (paymentInfo: IInitPaymentProps) => Promise<void>;
}

export const useGooglePayment = (): UseGooglePaymentHook => {
    const {
        configuration: { isGooglePayEnabled },
    } = useConfiguration();
    const { brandId } = getBrandInfo();
    const styles = googlePayStyles[brandId]?.replace(/(\r\n|\n|\r)/gm, '');

    const [error, setError] = useState<Error>(null);
    const [payment, setPayment] = useState<IPaymentInitResponseModel>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isCompatible, setCompatibility] = useState<boolean>(false);

    useEffect(() => {
        if (!isGooglePayEnabled) {
            setIsLoading(false);
            return;
        }

        setIsLoading(true);

        const allowedAuthMethods = ['PAN_ONLY', 'CRYPTOGRAM_3DS'];
        const allowedCardNetworks = ['AMEX', 'DISCOVER', 'JCB', 'MASTERCARD', 'VISA'];

        injectScriptOnce(process.env.NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL).then(() => {
            if (google) {
                // @ts-ignore
                const paymentsClient = new google.payments.api.PaymentsClient({
                    environment: process.env.NEXT_PUBLIC_GOOGLE_PAY_MODE,
                });
                paymentsClient
                    .isReadyToPay({
                        apiVersion: 2,
                        apiVersionMinor: 0,
                        allowedPaymentMethods: [
                            {
                                type: 'CARD',
                                parameters: {
                                    allowedAuthMethods,
                                    allowedCardNetworks,
                                },
                            },
                        ],
                    })
                    .then((response) => {
                        if (response.result) {
                            setCompatibility(true);
                        }
                    })
                    .finally(() => {
                        setIsLoading(false);
                    });
            }
        });
    }, [isGooglePayEnabled]);

    const initPayment = useMemo(() => {
        if (!isCompatible) return;

        const expApi = new WebExperienceApi();
        const initPaymentApi = createErrorWrapper<IPaymentInitResponseModel, InitGooglePaymentRequest>(
            'initPaymentGoogle',
            expApi.initGooglePayment.bind(expApi)
        );

        return async (paymentInfo: IInitPaymentProps) => {
            setIsLoading(true);
            try {
                const paymentPayload = await initPaymentApi({
                    iGooglePaymentInitBodyModel: {
                        ...paymentInfo,
                        isBillingAddressRequired: true,
                        ...(styles && { styles }),
                    },
                });

                setPayment(paymentPayload);
                setError(null);
            } catch (error) {
                setError(new Error('Payment service is not available. Please, try again later.'));
            } finally {
                setIsLoading(false);
            }
        };
    }, [isCompatible, styles]);

    const reset = () => {
        setPayment(null);
        setError(null);
        setIsLoading(false);
    };

    return { payment, error, reset, isLoading, isCompatible, initPayment };
};
