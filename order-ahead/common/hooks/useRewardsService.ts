import { useAuth0 } from '@auth0/auth0-react';
import { useEffect, useMemo, useState } from 'react';
import { authorizationHeaderBuilder } from '../helpers/accountHelper';
import CustomerRewardsService from '../services/customerService/rewards';

export const useRewardsService = (): CustomerRewardsService => {
    const { isAuthenticated, user, getIdTokenClaims } = useAuth0();
    const [idToken, setIdToken] = useState<string>('');

    useEffect(() => {
        getIdTokenClaims().then((res) => {
            setIdToken(res?.__raw);
        });
    }, [isAuthenticated, user, getIdTokenClaims]);

    const rewardsService = useMemo(() => {
        return new CustomerRewardsService(authorizationHeaderBuilder(idToken));
    }, [idToken]);

    return rewardsService;
};
