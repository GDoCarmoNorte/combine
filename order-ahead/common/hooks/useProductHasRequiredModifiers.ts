import { selectIsProductHasRequiredModifiers } from '../../redux/selectors/domainMenu';
import { useAppSelector } from '../../redux/store';

export const useProductHasRequiredModifiers = (productId: string) => {
    return useAppSelector((state) => selectIsProductHasRequiredModifiers(state, productId));
};
