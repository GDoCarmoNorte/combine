import { useDomainProduct } from '../../redux/hooks/domainMenu';
import { checkIsUnavailabeProduct } from '../helpers/bagHelper';
import { useUnavailableCategories } from './useUnavailableCategories';

export interface IUseProductIsAvailable {
    isAvailable: boolean;
}

export const useProductIsAvailable = (productId: string, category?: string): IUseProductIsAvailable => {
    const unavailableCategories = useUnavailableCategories();
    const product = useDomainProduct(productId);
    const isAvailable =
        !unavailableCategories.includes(category) &&
        !checkIsUnavailabeProduct(product?.categoryIds, unavailableCategories);

    return {
        isAvailable,
    };
};
