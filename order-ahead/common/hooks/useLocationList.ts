import { ILocationUrlsResponseModel, TAllLocationsResponseDataModel } from '../../@generated/webExpApi';
import getLocationUrlList from '../services/locationService/getLocationUrlList';
import searchLocations from '../services/locationService/searchLocations';

export interface UseLocationListHook {
    getLocationList: (countryCode?: string, stateOrProvinceCode?: string) => Promise<TAllLocationsResponseDataModel>;
    getLocationUrlsList: () => Promise<ILocationUrlsResponseModel>;
}

export default function useLocationList(): UseLocationListHook {
    const getLocationList = async (
        countryCode?: string,
        stateOrProvinceCode?: string
    ): Promise<TAllLocationsResponseDataModel> => {
        return await searchLocations(countryCode, stateOrProvinceCode);
    };

    const getLocationUrlsList = async (): Promise<ILocationUrlsResponseModel> => {
        return await getLocationUrlList();
    };

    return {
        getLocationList,
        getLocationUrlsList,
    };
}
