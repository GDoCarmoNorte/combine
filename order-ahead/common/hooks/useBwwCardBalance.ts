import { useState } from 'react';
import {
    IGiftCardBalanceResponseModel,
    IGiftCardBWWBalanceRequestModel,
    WebExperienceApi,
} from '../../@generated/webExpApi';
import createErrorWrapper from '../services/createErrorWrapper';

interface ICardBalanceHook {
    balance: number | null;
    loading: boolean;
    errorMessage: string | null;
    code: string | null;
    getBalance: (params: IGiftCardBWWBalanceRequestModel) => Promise<void>;
    resetCard: () => void;
}

export default function useBwwCardBalance(): ICardBalanceHook {
    const [balance, setBalance] = useState<number | null>(null);
    const [code, setCode] = useState<string | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string | null>(null);

    const expApi = new WebExperienceApi();

    const giftCardBalanceApi = createErrorWrapper<
        IGiftCardBalanceResponseModel,
        { iGiftCardBalanceRequestModel: IGiftCardBWWBalanceRequestModel }
    >('initCreditCardPaymentApi', expApi.getBalance.bind(expApi));

    const getBalance = async (params: IGiftCardBWWBalanceRequestModel) => {
        setLoading(true);
        setBalance(null);
        setErrorMessage(null);
        setCode(null);

        try {
            const { balance, message, code } = await giftCardBalanceApi({
                iGiftCardBalanceRequestModel: params,
            });

            if (balance !== undefined && !isNaN(balance)) {
                setBalance(balance);
            } else {
                setErrorMessage(message);
                setCode(code);
            }

            setLoading(false);
        } catch (e) {
            setLoading(false);
            setErrorMessage('Card Information Not Available');
        }
    };

    const resetCard = () => {
        setLoading(false);
        setBalance(null);
        setErrorMessage(null);
        setCode(null);
    };

    return { balance, loading, errorMessage, code, getBalance, resetCard };
}
