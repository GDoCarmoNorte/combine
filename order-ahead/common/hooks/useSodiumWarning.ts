import { ISodiumWarningFields } from '../../@generated/@types/contentful';
import { useGlobalProps } from '../../redux/hooks';
import { InspireCmsEntry } from '../types';
import { useSodiumLegalWarning } from './useSodiumLegalWarning';

export const useSodiumWarning = (): InspireCmsEntry<ISodiumWarningFields> => {
    const sodiumLegalWarning = useSodiumLegalWarning();
    const { sodiumWarnings } = useGlobalProps();

    const sodiumWarning = sodiumWarnings.items.find(
        (i) => i.fields.type === sodiumLegalWarning?.additionalProperties?.area
    );

    return sodiumWarning;
};
