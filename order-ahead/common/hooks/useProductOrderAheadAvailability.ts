import { useOrderLocation } from '../../redux/hooks';
import { useDomainProduct } from '../../redux/hooks/domainMenu';
interface IUseProductOrderAheadAvailability {
    isOrderAheadAvailable: boolean;
}

export const useProductOrderAheadAvailability = (productId: string): IUseProductOrderAheadAvailability => {
    const { isCurrentLocationOAAvailable } = useOrderLocation();
    const product = useDomainProduct(productId);

    const isOrderAheadAvailable = !!(productId && product?.availability?.isAvailable && isCurrentLocationOAAvailable);

    return { isOrderAheadAvailable };
};
