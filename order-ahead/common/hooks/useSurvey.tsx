import { useNotifications, useMyTeams, useAccount } from '../../redux/hooks';
import { useSurveyService } from './useSurveyService';
import { useEffect } from 'react';
import { ISurveyRespondModel } from '../../@generated/webExpApi';
import { useIDToken } from './useIDToken';

const GET_SURVEY_ERROR = "We couldn't display teams. Please try again later.";
const UPDATE_SURVEY_ERROR = "We couldn't remove the team. Please try again later.";
const SAVE_SURVEY_ERROR = 'Something was wrong. Please try again later.';

export enum TypeSurveyEnum {
    DELETE = 'delete',
}

const useSurvey = () => {
    const { account } = useAccount();
    const { idToken } = useIDToken();
    const { getSurveys: getSurveysRequest, updateSurveys: updateSurveysRequest } = useSurveyService(idToken);

    const {
        actions: { enqueueError },
    } = useNotifications();

    const {
        myTeams,
        actions: { setMyTeams, setMyTeamsLoading, setMyTeamsError },
    } = useMyTeams();

    const getSurveys = async () => {
        try {
            setMyTeamsLoading(true);
            const surveys = await getSurveysRequest();
            setMyTeams(surveys);
        } catch {
            enqueueError({ message: GET_SURVEY_ERROR });
            setMyTeamsError(GET_SURVEY_ERROR);
        } finally {
            setMyTeamsLoading(false);
        }
    };

    const updateSurveys = async (payload: {
        surveyId: string;
        surveyRespond: ISurveyRespondModel[];
        typeSurvey?: TypeSurveyEnum;
    }) => {
        const isTypeSurveyDelete = payload.typeSurvey === TypeSurveyEnum.DELETE;
        const errorMessage = isTypeSurveyDelete ? UPDATE_SURVEY_ERROR : SAVE_SURVEY_ERROR;
        try {
            isTypeSurveyDelete && setMyTeamsLoading(true);
            const surveys = await updateSurveysRequest(payload);
            setMyTeams(surveys);
        } catch {
            enqueueError({ message: errorMessage });
        } finally {
            setMyTeamsLoading(false);
        }
    };

    useEffect(() => {
        if (account && idToken) getSurveys();
        // infinity loop
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account, idToken]);

    return {
        myTeams,
        isLoading: myTeams.loading,
        actions: {
            updateSurveys,
        },
    };
};

export default useSurvey;
