import { useAuth0 } from '@auth0/auth0-react';

import { clearCookie } from '../helpers/cookieHelper';
import {
    HASHED_BLAZIN_REWARDS_COOKIE_NAME,
    RAW_BLAZIN_REWARDS_COOKIE_NAME,
} from '../../components/organisms/play/constants';

export const useLogout = () => {
    const { logout } = useAuth0();

    // redirectUrl?: string argument could be passed additionally in future
    const logoutAndClearCookies = () => {
        const returnTo = window.location.origin;
        logout({ returnTo });
        clearCookie(RAW_BLAZIN_REWARDS_COOKIE_NAME);
        clearCookie(HASHED_BLAZIN_REWARDS_COOKIE_NAME);
        // more cookies to clean could be added here
    };

    return { logoutAndClearCookies };
};
