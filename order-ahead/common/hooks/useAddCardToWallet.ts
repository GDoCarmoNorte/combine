import { useState } from 'react';
import {
    AddCreditCardRequest,
    IAccountPaymentMethodModel,
    IAddCreditCardRequestModel,
} from '../../@generated/webExpApi';
import addCardToWalletService from '../services/addCardToWalletService';

interface IUseAddCardToWallet {
    error: Error;
    isLoading: boolean;
    addCardToWallet: (request: IAddCreditCardRequestModel, jwtToken: string) => Promise<IAccountPaymentMethodModel>;
}

export const useAddCardToWallet = (): IUseAddCardToWallet => {
    const [error, setError] = useState<Error>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const addCardToWallet = async (request: IAddCreditCardRequestModel, jwtToken: string) => {
        try {
            setIsLoading(true);
            const response = await addCardToWalletService(
                {
                    iAddCreditCardRequestModel: request,
                } as AddCreditCardRequest,
                jwtToken
            );
            return response;
        } catch (error) {
            setError(new Error('Could not save card to wallet. Please, try again later.'));
        } finally {
            setIsLoading(false);
        }
    };

    return { error, isLoading, addCardToWallet };
};

export default useAddCardToWallet;
