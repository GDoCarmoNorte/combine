import { useOrderLocation } from '../../redux/hooks';
import { checkIsUnavailabeProduct, getTallyBagProductIds } from '../helpers/bagHelper';
import { useUnavailableCategories } from './useUnavailableCategories';
import { getDiscountBanner, IBagRenderItem } from '../../components/organisms/bag/bagModel';
import { getProductPath, getRelatedProduct } from '../../lib/domainProduct';
import { useDomainProducts } from '../../redux/hooks/domainMenu';
import useBag from '../../redux/hooks/useBag';
import useGlobalProps from '../../redux/hooks/useGlobalProps';
import { useTallyItemIsPromoList } from '../../redux/hooks/useTally';

export const useBagRenderItems = (): IBagRenderItem[] => {
    const { bagEntries, entriesMarkedAsRemoved } = useBag();
    const globalProps = useGlobalProps();
    const allItemsIds = getTallyBagProductIds(bagEntries);
    const domainProducts = useDomainProducts(allItemsIds);
    const tallyPromoMap = useTallyItemIsPromoList(bagEntries);
    const unavailableCategories = useUnavailableCategories();
    const { currentLocation: location } = useOrderLocation();

    return (
        bagEntries
            ?.map((entry) => {
                let productHasPrice = !!entry.price || entry.price === 0;
                const product = domainProducts[entry.productId];
                const otherPrices = Object.keys(product?.price?.otherPrices || {}).map(
                    (key) => product.price.otherPrices[key]
                );

                otherPrices.forEach((item) => {
                    if (!item.price && item.price !== 0) {
                        productHasPrice = false;
                    }
                });
                const categoryIds = domainProducts?.[entry.productId]?.categoryIds;

                return {
                    entry,
                    isAvailable:
                        !!domainProducts?.[entry.productId]?.isSaleable &&
                        domainProducts[entry.productId].availability?.isAvailable &&
                        productHasPrice &&
                        !checkIsUnavailabeProduct(categoryIds, unavailableCategories),

                    markedAsRemoved: entriesMarkedAsRemoved.includes(entry.lineItemId),
                    contentfulProduct:
                        globalProps.productsById[entry.productId] ||
                        getRelatedProduct(domainProducts[entry.productId], globalProps.productsById),
                    entryPath: getProductPath(domainProducts[entry.productId], globalProps.productDetailsPagePaths),
                    discountBanner: getDiscountBanner(
                        entry,
                        location,
                        domainProducts[entry.productId],
                        tallyPromoMap[entry.productId]
                    ),
                };
            })
            .filter((entry) => !!entry.contentfulProduct) || []
    );
};
