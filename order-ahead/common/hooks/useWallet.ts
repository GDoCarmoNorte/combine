import { useEffect, useState } from 'react';
import createErrorWrapper from '../services/createErrorWrapper';
import {
    CardIconTypeEnumModel,
    InitWalletRequest,
    IPaymentInitResponseModel,
    WebExperienceApi,
} from '../../@generated/webExpApi';
import getBrandInfo from '../../lib/brandInfo';
import walletIFrameStyles from '../constants/walletIFrameStyles';

export interface IUseWallet {
    wallet: IPaymentInitResponseModel;
    error: Error;
    isLoading: boolean;
}

export const useWallet = (): IUseWallet => {
    const [error, setError] = useState<Error>(null);
    const [wallet, setWallet] = useState<IPaymentInitResponseModel>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const { brandId } = getBrandInfo();
    const expApi = new WebExperienceApi();

    const initWalletApi = createErrorWrapper<IPaymentInitResponseModel, InitWalletRequest>(
        'initIframeWallet',
        expApi.initWallet.bind(expApi)
    );

    const getWallet = async () => {
        try {
            setIsLoading(true);
            const walletPayload = await initWalletApi({
                iWalletInitParamsModel: {
                    styles: walletIFrameStyles[brandId].replace(/(\r\n|\n|\r)/gm, ''),
                    cardIconDisplayType: CardIconTypeEnumModel.Hidden,
                },
            });
            setWallet(walletPayload);
        } catch (error) {
            setError(new Error('Wallet service is not available. Please, try again later.'));
        } finally {
            setIsLoading(false);
        }
    };
    useEffect(() => {
        getWallet();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return { error, wallet, isLoading };
};
