import { useDomainProducts } from '../../redux/hooks/domainMenu';
import { getFormattedPriceFromProduct } from '../../lib/domainProduct';
import { useDomainMenu, useOrderLocation } from '../../redux/hooks';
import { useEffect, useState } from 'react';
import { IProductFields, IMenuCategoryFields } from '../../@generated/@types/contentful';
import { Entry } from 'contentful';
import { getContentfulProductIdsByFields } from '../helpers/getContentfulProductIdsByFields';

interface gtmImpression extends IProductFields {
    price: string | number;
}

export default function useGtmImpression(menuCategory: Entry<IMenuCategoryFields>): gtmImpression[] {
    const [impression, setImpression] = useState(null);
    const products = menuCategory?.fields?.products || [];
    const ids = products.reduce((acc, { fields }) => [...acc, ...getContentfulProductIdsByFields(fields)], []);
    const domainProducts = useDomainProducts(ids);
    const { currentLocation } = useOrderLocation();
    const { loading } = useDomainMenu();

    useEffect(() => {
        if (!loading) {
            const result = products.map((item) => {
                return {
                    ...item.fields,
                    price: getFormattedPriceFromProduct(domainProducts[item.fields.productId], currentLocation),
                };
            });
            setImpression(result);
        }
        // infinity loop
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loading]);

    return impression;
}
