import { useState, useRef, useEffect } from 'react';
import { ICheckinModel, IError500InternalResponseModel } from '../../@generated/webExpApi';
import { getCoordinates, LOCATION_SERVICES_DISABLED_ERROR_CODE } from '../helpers/getCoordinates';
import { useRewardsService } from './useRewardsService';
import { useAppDispatch } from '../../redux/store';
import { GTM_CHECK_IN } from '../services/gtmService/constants';

export type ICheckinError = IError500InternalResponseModel | { code: typeof LOCATION_SERVICES_DISABLED_ERROR_CODE };

interface useCheckinHook {
    checkin: () => Promise<void>;
    isShowTooltip: boolean;
    loading: boolean;
    error: ICheckinError | null;
    payload: ICheckinModel | null;
}

const TOOLTIP_TIMEOUT = 2000;

export const useCheckin = (): useCheckinHook => {
    const rewardsService = useRewardsService();
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<ICheckinError | null>(null);
    const [payload, setPayload] = useState<ICheckinModel | null>(null);
    const [isShowTooltip, setIsShowTooltip] = useState<boolean>(false);
    const tooltipTimerRef = useRef<ReturnType<typeof setTimeout>>();
    const dispatch = useAppDispatch();

    const showTooltip = () => {
        setIsShowTooltip(true);

        clearTimeout(tooltipTimerRef.current);
        tooltipTimerRef.current = setTimeout(() => {
            setIsShowTooltip(false);
        }, TOOLTIP_TIMEOUT);
    };

    const reset = () => {
        setError(null);
        setPayload(null);
    };

    const checkin = async () => {
        if (!loading) {
            reset();
            setLoading(true);

            let storeId;

            try {
                const {
                    coords: { latitude, longitude },
                } = await getCoordinates();

                const payload = await rewardsService.checkin(latitude, longitude);
                storeId = payload.storeId;

                setPayload(payload);
                showTooltip();
            } catch (e) {
                setError(e);
            } finally {
                setLoading(false);
                dispatch({
                    type: GTM_CHECK_IN,
                    payload: storeId,
                });
            }
        }
    };

    useEffect(() => {
        return () => {
            clearTimeout(tooltipTimerRef.current);
        };
    }, []);

    return {
        checkin,
        isShowTooltip,
        loading,
        error,
        payload,
    };
};
