import { Asset } from 'contentful';

import { useGlobalProps } from '../../redux/hooks';

interface IDealHook {
    getDealImage: (id: string) => Asset;
}

export default function useDeal(): IDealHook {
    const globalProps = useGlobalProps();

    const getDealImage = (id: string) => {
        const { dealItemsById } = globalProps;
        const deal = dealItemsById[id];
        const dealImage = deal?.fields.image || dealItemsById['default'].fields.image;

        return dealImage;
    };

    return { getDealImage };
}
