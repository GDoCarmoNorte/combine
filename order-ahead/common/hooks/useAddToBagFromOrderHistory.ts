import * as PDPStore from '../../redux/pdp';
import { useBag } from '../../redux/hooks';
import { useSelectHistoryModifierGroups } from '../../redux/hooks/domainMenu';
import { IOrderProductModel } from '../../@generated/webExpApi';

const useAddToBagFromOrderHistory = (products: IOrderProductModel[]) => {
    const bag = useBag();
    const modifierGroups = useSelectHistoryModifierGroups(products);

    const addFromOrderHistory = () => {
        products.forEach((item, index) => {
            const newTallyItemState: PDPStore.PDPTallyItem = {
                modifierGroups: modifierGroups[index],
                price: item.price,
                productId: item.id,
                quantity: item.quantity,
            };

            bag.actions.putToBag({
                pdpTallyItem: newTallyItemState,
            });
        });
    };

    return { addFromOrderHistory, modifierGroups };
};

export default useAddToBagFromOrderHistory;
