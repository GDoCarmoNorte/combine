import { useState, useMemo, useEffect } from 'react';
import { injectScriptOnce } from '../../lib/injectScriptOnce';
import isLocalRun from '../../lib/isLocalRun';
import getBrandInfo from '../../lib/brandInfo';
import createErrorWrapper from '../services/createErrorWrapper';
import applePayStyles from '../constants/applePayStyles';
import {
    TApplePaymentFulfillmentTypesModel,
    TApplePaymentSellingChannelsModel,
    InitApplePaymentRequest,
    IPaymentInitResponseModel,
    WebExperienceApi,
} from '../../@generated/webExpApi';
import { useConfiguration } from './useConfiguration';

interface IInitPaymentProps {
    locationId: string;
    totalAmount: number;
    fulfillmentType?: TApplePaymentFulfillmentTypesModel;
    submitButtonType?: string;
    submitButtonColor?: string;
    isAutoFinalizePayment?: boolean;
    styles?: string;
    cartId?: string;
}

interface IUseApplePayHook {
    error: Error;
    isLoading: boolean;
    isCompatible: boolean;
    payment: IPaymentInitResponseModel;
    flush: () => void;
    initPayment: (paymentInfo: IInitPaymentProps) => Promise<void>;
}

export const useApplePay = (): IUseApplePayHook => {
    const {
        configuration: { isApplePayEnabled },
    } = useConfiguration();
    const { brandId } = getBrandInfo();
    const styles = applePayStyles[brandId]?.replace(/(\r\n|\n|\r)/gm, '');

    const [error, setError] = useState<Error>(null);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [payment, setPayment] = useState<IPaymentInitResponseModel>(null);
    const [isCompatible, setCompatibility] = useState<boolean>(false);

    useEffect(() => {
        if (!isApplePayEnabled || isLocalRun()) {
            setIsLoading(false);
            return;
        }

        injectScriptOnce(process.env.NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL).then(() => {
            if (window.FreedomPay.Apm.ApplePay.canMakePayments()) setCompatibility(true);
            setIsLoading(false);
        });
    }, [isApplePayEnabled]);

    const initPayment = useMemo(() => {
        if (!isCompatible) return;

        const expApi = new WebExperienceApi();
        const initPaymentApi = createErrorWrapper<IPaymentInitResponseModel, InitApplePaymentRequest>(
            'initApplePay',
            expApi.initApplePayment.bind(expApi)
        );

        return async (paymentInfo: IInitPaymentProps) => {
            setIsLoading(true);
            try {
                const paymentResponse = await initPaymentApi({
                    sellingChannel: TApplePaymentSellingChannelsModel.Weboa,
                    iApplePaymentInitBodyModel: {
                        ...paymentInfo,
                        ...(styles && { styles }),
                    },
                });

                setPayment(paymentResponse);
                setError(null);
            } catch (error) {
                setError(new Error('Payment service is not available. Please, try again later.'));
            } finally {
                setIsLoading(false);
            }
        };
    }, [isCompatible, styles]);

    const flush = () => {
        setPayment(null);
        setError(null);
        setIsLoading(false);
    };

    return { isLoading, error, isCompatible, payment, flush, initPayment };
};
