import { useEffect, useState } from 'react';
import {
    contactlessTipPercentage,
    defaultTipPercentage,
    defaultTipsId,
    deliveryTipPercentage,
    pickupTipPercentage,
    startAmountOfContactlessTip,
    startAmountOfDeliveryTip,
    startAmountOfPickupTip,
    thresholdToCalculateContactlessTip,
    thresholdToCalculateDeliveryTip,
    thresholdToCalculatePickupTip,
    tipAllowedByDoorDash,
    titleTextForDeliveryTip,
    titleTextForPickupTip,
} from '../../components/organisms/checkout/tipsBlock/tipsConstants';

interface TipsItem {
    amount: number;
    percentage: number;
}

export interface IUseTips {
    tipsItems: TipsItem[];
    titleText: string;
    isError: boolean;
    errorMessage: string;
    defaultTipsId: number;
}

export const useTips = (
    subtotal: number,
    customTipsValue: string,
    isPickUp: boolean,
    isContactless?: boolean
): IUseTips => {
    const [isError, setIsError] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>('');

    const parsedCustomTip = parseFloat(customTipsValue);
    const isCustomTipMoreThanSubtotal = parsedCustomTip >= subtotal;
    const isContactlessMoreThanSubtotalError = isContactless && isCustomTipMoreThanSubtotal;
    const isDoorDashError = !isPickUp && parsedCustomTip > tipAllowedByDoorDash;
    const isMoreThanSubtotalError =
        (isPickUp || subtotal > thresholdToCalculateDeliveryTip) && isCustomTipMoreThanSubtotal;

    useEffect(() => {
        if (isContactlessMoreThanSubtotalError) {
            setIsError(true);
            return;
        }

        if (isDoorDashError) {
            setIsError(true);
            setErrorMessage(`Enter amount less than $${tipAllowedByDoorDash}`);
            return;
        }

        if (isMoreThanSubtotalError) {
            setIsError(true);
            setErrorMessage(`Enter amount less than your subtotal $${subtotal}`);
            return;
        }

        setIsError(false);
    }, [isContactlessMoreThanSubtotalError, isDoorDashError, isMoreThanSubtotalError, subtotal]);

    const titleText = isPickUp || isContactless ? titleTextForPickupTip : titleTextForDeliveryTip;

    const getTipsItems = ({ thresholdToCalculateTip, tipPercentage, startAmountOfTip }): TipsItem[] => {
        const tipsPercentage = subtotal <= thresholdToCalculateTip ? defaultTipPercentage : tipPercentage;
        const tipsItems: TipsItem[] = tipsPercentage.map((percentage, index) => {
            const baseTipValue = index + startAmountOfTip;
            const valueWithDecimals = parseFloat(((subtotal * percentage) / 100).toFixed(2));
            const valueWithoutDecimals = Math.round((subtotal * percentage) / 100);

            if (isContactless) {
                return {
                    amount: valueWithDecimals || baseTipValue,
                    percentage,
                };
            }

            if (isPickUp) {
                const isLessThanBaseTipValue = valueWithDecimals < baseTipValue;
                return {
                    amount: isLessThanBaseTipValue ? baseTipValue : valueWithDecimals,
                    percentage: isLessThanBaseTipValue ? 0 : percentage,
                };
            }

            return {
                amount: valueWithoutDecimals || baseTipValue,
                percentage: 0,
            };
        });

        //return tips items without duplicate values
        return tipsItems.reduce((array, item, i) => {
            if (item.amount === array[i - 1]?.amount) {
                return [
                    ...array,
                    {
                        ...item,
                        amount: item.amount + 1,
                    },
                ];
            }
            return [...array, item];
        }, []);
    };

    const getTipsProperties = () => {
        if (isContactless) {
            return {
                thresholdToCalculateTip: thresholdToCalculateContactlessTip,
                startAmountOfTip: startAmountOfContactlessTip,
                tipPercentage: contactlessTipPercentage,
            };
        }

        if (isPickUp) {
            return {
                thresholdToCalculateTip: thresholdToCalculatePickupTip,
                startAmountOfTip: startAmountOfPickupTip,
                tipPercentage: pickupTipPercentage,
            };
        }

        return {
            thresholdToCalculateTip: thresholdToCalculateDeliveryTip,
            startAmountOfTip: startAmountOfDeliveryTip,
            tipPercentage: deliveryTipPercentage,
        };
    };

    const tipsProperties = getTipsProperties();
    const tipsItems = getTipsItems(tipsProperties);

    return {
        tipsItems,
        titleText,
        isError,
        errorMessage,
        defaultTipsId,
    };
};
