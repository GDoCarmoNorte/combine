import { useBag } from '../../redux/hooks';
import { useSelectUnavailableCategories } from '../../redux/hooks/domainMenu';
import { useLocationTimeZone } from './useLocationTimeZone';

export const useUnavailableCategories = (): string[] => {
    const { orderTime } = useBag();
    const locationTimeZone = useLocationTimeZone();
    return useSelectUnavailableCategories(orderTime, locationTimeZone);
};
