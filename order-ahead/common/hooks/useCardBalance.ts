import { useState } from 'react';
import { getCardBalance } from '../services/cardBalance';

interface ICardBalanceHook {
    balance: number | null;
    loading: boolean;
    error: string | null;
    code: TCardBalanceCode | null;
    getBalance: (cardNumber: string, cardPin: string) => Promise<void>;
}

export enum TCardBalanceCode {
    SUCCESS = 'SUCCESS',
    INVALID = 'INVALID',
    DECLINED = 'DECLINED',
    OTHER = 'OTHER',
}

export default function useCardBalance(): ICardBalanceHook {
    const [balance, setBalance] = useState<number | null>(null);
    const [code, setCode] = useState<TCardBalanceCode | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string | null>(null);

    const getBalance = async (cardNumber: string, cardPin: string) => {
        setLoading(true);
        setBalance(null);
        setError(null);
        setCode(null);

        try {
            const { balance, message, code: initCode } = await getCardBalance(cardNumber, cardPin);

            if (balance) {
                setBalance(balance);
                setCode(TCardBalanceCode.SUCCESS);
            } else {
                setError(message);
                setCode(initCode);
            }

            setLoading(false);
        } catch (e) {
            setLoading(false);
            setCode(TCardBalanceCode.OTHER);
            setError('Card Information Not Available');
        }
    };

    return { balance, loading, error, code, getBalance };
}
