import { IOrderModel } from '../../@generated/webExpApi/models';
import { useDomainMenuSelectors, useDomainProducts } from '../../redux/hooks/domainMenu';
import { getBagItemSizeLabel } from '../helpers/bagHelper';

export const useOrderItem = (order: IOrderModel) => {
    const domainProducts = useDomainProducts(order.products.map((product) => product.id));
    const { selectProductSize } = useDomainMenuSelectors();

    return {
        ...order,
        products: order.products.map((product) => {
            const domainProduct = domainProducts[product.id];
            const productSize = selectProductSize(domainProduct?.id);
            const productSizeLabel = getBagItemSizeLabel(productSize);
            return {
                ...product,
                domainProduct,
                sizeLabel: productSizeLabel,
            };
        }),
    };
};
