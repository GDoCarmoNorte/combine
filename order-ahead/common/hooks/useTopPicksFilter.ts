import { IProduct } from '../../@generated/@types/contentful';
import getBrandInfo, { BRANDS } from '../../lib/brandInfo';
import { useOrderLocation } from '../../redux/hooks';
import { useProducts } from '../../redux/hooks/domainMenu';
import { getContentfulProductIdsByFields } from '../helpers/getContentfulProductIdsByFields';
import { getProductByIds } from '../helpers/getProductByIds';

export default function useTopPicksFilter(): (product: IProduct) => boolean {
    const { brandId } = getBrandInfo();
    const domainProducts = useProducts();
    const { isCurrentLocationOAAvailable } = useOrderLocation();

    return (product: IProduct) => {
        const domainProduct = getProductByIds(getContentfulProductIdsByFields(product.fields), domainProducts);

        // show all products for national menu
        if (!isCurrentLocationOAAvailable) return true;

        if (brandId === BRANDS.bww.brandId) return domainProduct?.availability?.isAvailable;

        return true;
    };
}
