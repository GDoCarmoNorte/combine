import { useAuth0 } from '@auth0/auth0-react';
import { useEffect, useState } from 'react';
import { useFeatureFlags } from '../../redux/hooks/useFeatureFlags';
import { useAccount } from '../../redux/hooks';

export const useIDToken = (): { idToken: string } => {
    const { account } = useAccount();
    const { isAuthenticated, user, getIdTokenClaims } = useAuth0();
    const { featureFlags } = useFeatureFlags();
    const [idToken, setIdToken] = useState<string>('');

    useEffect(() => {
        if (featureFlags.account) {
            getIdTokenClaims().then((res) => {
                setIdToken(res?.__raw);
            });
        }
    }, [account, isAuthenticated, user, featureFlags.account, getIdTokenClaims]);

    return { idToken };
};
