import { useDomainProduct } from '../../redux/hooks/domainMenu';
import { useLocationOrderAheadAvailability } from './useLocationOrderAheadAvailability';
import { LOCATION_NOT_SELECTED_DESCRIPTION_TEXT } from '../../components/sections/productDetailsContainer/constants';

export interface IUseLocalization {
    locationLinkText: string;
    descriptionText: string;
    isProductAvailable: boolean;
    isLocationOrderAheadAvailable: boolean;
}

export const useLocalization = (productId: string): IUseLocalization => {
    const domainProduct = useDomainProduct(productId);
    const { isLocationOrderAheadAvailable, location } = useLocationOrderAheadAvailability();

    let locationLinkText = '';
    let descriptionText = '';
    const isProductAvailable = domainProduct?.availability?.isAvailable;

    if (!location) {
        locationLinkText = 'Select a location';
        descriptionText = LOCATION_NOT_SELECTED_DESCRIPTION_TEXT;
    } else if (!isLocationOrderAheadAvailable || !isProductAvailable) {
        locationLinkText = 'Select a different location';

        if (!isLocationOrderAheadAvailable) {
            descriptionText = 'Online ordering not available at this location.';
        }

        if (isLocationOrderAheadAvailable && !isProductAvailable) {
            descriptionText = '<b>Item not available</b> at this location.';
        }
    }

    return {
        locationLinkText,
        descriptionText,
        isProductAvailable,
        isLocationOrderAheadAvailable,
    };
};
