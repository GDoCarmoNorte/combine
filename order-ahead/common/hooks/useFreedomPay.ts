import { useState, useEffect, useCallback, useMemo } from 'react';
import { CARD_ON_FILE } from '../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/constants';
import {
    TFPPaymentTypes,
    TInitialPaymentTypes,
    TPaymentMethodTypes,
} from '../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import logger from '../services/logger';
import {
    defaultNormalizer as billingAddressDefaultNormalizer,
    applePayNormalizer as billingAddressApplePayNormalizer,
} from '../helpers/billingAddressNormalize';
import { filterFreedomPayEvent } from '../helpers/filterFreedomPayEvent';

const MESSAGE_EVENT = 'message';

enum MessageType {
    Error = 1,
    FrameHeight = 2,
    PaymentKeys = 3,
    Others = 4,
}

interface IAttributesMap {
    CARD_ISSUER_ATTR: string;
    MASKED_CARD_NUMBER_ATTR: string;
    BILLING_ADDRESS_ATTR: string;
}

const emittedType = {
    CardNumber: 'CardNumber',
    ExpirationDate: 'ExpirationDate',
    SecurityCode: 'SecurityCode',
    PostalCode: 'PostalCode',
};

const defaultAttrMap: IAttributesMap = {
    CARD_ISSUER_ATTR: 'CardIssuer',
    MASKED_CARD_NUMBER_ATTR: 'MaskedCardNumber',
    BILLING_ADDRESS_ATTR: 'BillingAddress',
};
const applePayAttrMap: IAttributesMap = {
    CARD_ISSUER_ATTR: 'cardIssuer',
    MASKED_CARD_NUMBER_ATTR: 'maskedCardNumber',
    BILLING_ADDRESS_ATTR: 'billingContact',
};

export interface FreedomPayDto {
    keys: string[];
    cardIssuer?: string;
    maskedCardNumber?: string;
    cardHolderName?: string;
    billingPostalCode?: string;
    paymentType?: TPaymentMethodTypes;
}

export interface IFreedomPayFieldsStatus {
    CardNumber: boolean;
    ExpirationDate: boolean;
    SecurityCode: boolean;
    PostalCode: boolean;
}

export interface UseFreedomPayHook {
    payment: FreedomPayDto;
    height: number;
    errors: { message: string }[];
    fieldsStatus: IFreedomPayFieldsStatus;
    isValid: boolean;
    reset: () => void;
}

const paymentTypeMap = {
    [TFPPaymentTypes.APPLE_PAY]: TInitialPaymentTypes.APPLE_PAY,
    [TFPPaymentTypes.GOOGLE_PAY]: TInitialPaymentTypes.GOOGLE_PAY,
    [TFPPaymentTypes.GIFT_CARD]: TInitialPaymentTypes.GIFT_CARD,
    [TFPPaymentTypes.CREDIT_OR_DEBIT]: TInitialPaymentTypes.CREDIT_OR_DEBIT,
    [TFPPaymentTypes.CARD_ON_FILE]: CARD_ON_FILE,
};

const createFreedomPayResponse = (data): FreedomPayDto => {
    const { attributes, paymentKeys } = data;

    const paymentType = data.paymentType && paymentTypeMap[data.paymentType];

    if (Array.isArray(attributes)) {
        const attrMap = paymentType === TInitialPaymentTypes.APPLE_PAY ? applePayAttrMap : defaultAttrMap;
        const cardIssuer = attributes.find((attr) => attr.Key === attrMap.CARD_ISSUER_ATTR);
        const maskedCardNumber = attributes.find((attr) => attr.Key === attrMap.MASKED_CARD_NUMBER_ATTR);
        const billingAddress = attributes.find((attr) => attr.Key === attrMap.BILLING_ADDRESS_ATTR);
        const billingAddressDataNormalized =
            paymentType === TInitialPaymentTypes.APPLE_PAY
                ? billingAddressApplePayNormalizer(billingAddress)
                : billingAddressDefaultNormalizer(billingAddress);

        return {
            paymentType,
            keys: paymentKeys,
            cardIssuer: cardIssuer && cardIssuer.Value,
            maskedCardNumber: maskedCardNumber && maskedCardNumber.Value,
            ...billingAddressDataNormalized,
        };
    }

    return {
        keys: paymentKeys,
        paymentType,
    };
};

const PAYMENT_FIELDS_TO_VALIDATE = {
    [TInitialPaymentTypes.CREDIT_OR_DEBIT]: ['cardNumber', 'expirationDate', 'securityCode', 'postalCode'],
    [TInitialPaymentTypes.PLACEHOLDER]: ['cardNumber', 'securityCode'],
    [CARD_ON_FILE]: ['securityCode'],
};

export const useFreedomPay = (paymentType?: TPaymentMethodTypes): UseFreedomPayHook => {
    const fieldsToValidate =
        PAYMENT_FIELDS_TO_VALIDATE[paymentType] || PAYMENT_FIELDS_TO_VALIDATE[TInitialPaymentTypes.CREDIT_OR_DEBIT];

    const [payment, setPayment] = useState<FreedomPayDto>(null);
    const [height, setHeight] = useState<number>(null);
    const [errors, setErrors] = useState<{ message: string }[]>(null);

    const [cardNumber, setCardNumber] = useState(!fieldsToValidate.includes('cardNumber'));
    const [expirationDate, setExpirationDate] = useState(!fieldsToValidate.includes('expirationDate'));
    const [securityCode, setSecurityCode] = useState(!fieldsToValidate.includes('securityCode'));
    const [postalCode, setPostalCode] = useState(!fieldsToValidate.includes('postalCode'));

    const fieldsStatus = useMemo(
        () => ({
            CardNumber: cardNumber,
            ExpirationDate: expirationDate,
            SecurityCode: securityCode,
            PostalCode: postalCode,
        }),
        [cardNumber, expirationDate, securityCode, postalCode]
    );

    const fieldsStatusValues = Object.values(fieldsStatus);
    const isValid = fieldsStatusValues.filter(Boolean).length === fieldsStatusValues.length;

    const reset = useCallback(() => {
        setPayment(null);
        setErrors(null);
    }, []);

    useEffect(() => {
        const messageHandler = (event: MessageEvent): void => {
            const isEligibleFBEvent = filterFreedomPayEvent(event);
            if (!isEligibleFBEvent) return;

            const { type, data } = event.data;
            switch (type) {
                case MessageType.Error:
                    setErrors(data.errors);
                    logger.logError('payment_iframe_error', { type: paymentType || 'default', errors: data.errors });
                    break;
                case MessageType.FrameHeight:
                    setHeight(data.height);
                    break;
                case MessageType.PaymentKeys:
                    setPayment(createFreedomPayResponse(data));
                    setErrors([]);
                    break;
                case MessageType.Others:
                    if (data.emittedBy) {
                        switch (data.emittedBy) {
                            case emittedType.CardNumber:
                                setCardNumber(data.isValid);
                                break;
                            case emittedType.ExpirationDate:
                                setExpirationDate(data.isValid);
                                break;
                            case emittedType.PostalCode:
                                setPostalCode(data.isValid);
                                break;
                            case emittedType.SecurityCode:
                                setSecurityCode(data.isValid);
                                break;
                        }
                    }
                    break;
            }
        };

        window.addEventListener(MESSAGE_EVENT, messageHandler, false);

        return () => {
            window.removeEventListener(MESSAGE_EVENT, messageHandler);
        };
    }, [fieldsStatus, paymentType]);

    return { payment, height, errors, fieldsStatus, isValid, reset };
};
