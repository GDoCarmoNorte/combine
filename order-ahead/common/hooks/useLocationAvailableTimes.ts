import { GetLocationAvailableTimesRequest, LocationPickupAndDeliveryResponseModel } from '../../@generated/webExpApi';
import getLocationAvailableTimes from '../services/locationService/getLocationAvailableTimes';

export interface UseLocationAvailableTimesHook {
    getLocationAvailableTimeSlots: (
        payload: GetLocationAvailableTimesRequest
    ) => Promise<LocationPickupAndDeliveryResponseModel>;
}

export default function useLocationAvailableTimes(): UseLocationAvailableTimesHook {
    const getLocationAvailableTimeSlots = async (
        payload: GetLocationAvailableTimesRequest
    ): Promise<LocationPickupAndDeliveryResponseModel> => {
        return await getLocationAvailableTimes(payload);
    };

    return {
        getLocationAvailableTimeSlots,
    };
}
