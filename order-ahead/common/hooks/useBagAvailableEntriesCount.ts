import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useBag } from '../../redux/hooks';
import { useDomainProducts } from '../../redux/hooks/domainMenu';
import { selectBagEntriesCount } from '../../redux/selectors/bag';
import { RootState, useAppSelector } from '../../redux/store';
import { getTallyBagProductIds } from '../helpers/bagHelper';

export const useBagAvailableEntriesCount = (): number => {
    const { bagEntries } = useBag();
    const allItemsIds = getTallyBagProductIds(bagEntries);
    const bagDomaminProducts = useDomainProducts(allItemsIds);

    const bagEntriesCount = useAppSelector(selectBagEntriesCount);
    const entriesMarkedAsRemoved = useSelector<RootState, number[]>((state) => state?.bag?.markedAsRemoved);

    const bagAvailableEntriesCount = useMemo<number>(() => {
        let filteredCount = bagEntriesCount;
        if (entriesMarkedAsRemoved.length > 0) {
            entriesMarkedAsRemoved.forEach((removedEntryId) => {
                const removedEntry = bagEntries.find((entry) => entry.lineItemId === removedEntryId);
                const removedEntryCount = removedEntry ? removedEntry.quantity : 0;
                filteredCount -= removedEntryCount;
            });
        }

        bagEntries.map((item) => {
            if (bagDomaminProducts[item.productId]?.isSaleable === false) {
                filteredCount -= item.quantity;
            }
        });

        return filteredCount;
    }, [bagEntriesCount, bagEntries, entriesMarkedAsRemoved, bagDomaminProducts]);

    return bagAvailableEntriesCount;
};

export default useBagAvailableEntriesCount;
