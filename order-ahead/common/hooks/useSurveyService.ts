import { useMemo } from 'react';
import { ISurveyModel, ISurveyRespondModel } from '../../@generated/webExpApi';
import { authorizationHeaderBuilder } from '../helpers/accountHelper';
import SurveyService from '../services/customerService/surveys';

interface IUseSurveyService {
    getSurveys: () => Promise<ISurveyModel>;
    updateSurveys: (payload: { surveyId: string; surveyRespond: ISurveyRespondModel[] }) => Promise<ISurveyModel>;
}

export const useSurveyService = (idToken): IUseSurveyService => {
    const surveyService = useMemo(() => {
        return new SurveyService(authorizationHeaderBuilder(idToken));
    }, [idToken]);

    const getSurveys = () => {
        return surveyService.getSurvey();
    };

    const updateSurveys = (payload: { surveyId: string; surveyRespond: ISurveyRespondModel[] }) => {
        return surveyService.updateSurvey(payload);
    };

    return { getSurveys, updateSurveys };
};
