import { Entry } from 'contentful';
import { IMenuCategoryFields } from '../../@generated/@types/contentful';
import { useProducts } from '../../redux/hooks/domainMenu';
import { isProductsHasSodiumWarning } from '../helpers/sodiumWarningHelper';
import { useSodiumLegalWarning } from './useSodiumLegalWarning';

export const useMenuCategoryHasSodiumWarning = (menuCategory: Entry<IMenuCategoryFields>): boolean => {
    const products = useProducts();
    const sodiumLegalWarning = useSodiumLegalWarning();

    if (isProductsHasSodiumWarning(menuCategory.fields.products, products, sodiumLegalWarning)) {
        return true;
    }

    if (
        menuCategory.fields.subcategories?.some((subcategory) =>
            isProductsHasSodiumWarning(subcategory.fields.products, products, sodiumLegalWarning)
        )
    ) {
        return true;
    }

    return false;
};
