import { MutableRefObject, useRef } from 'react';

export const useScrollToElement = <T>(): [MutableRefObject<T>, () => void] => {
    const ref = useRef(null);

    const scrollToElement = (): void => {
        ref.current.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
    };

    return [ref, scrollToElement];
};
