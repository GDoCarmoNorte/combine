import { useMemo } from 'react';
import { useBag } from '../../redux/hooks';
import { useBagRenderItems } from './useBagRenderItems';

export default function useBagAvailableItemsCounter(): number {
    const view = useBagRenderItems();

    const { bagEntries } = useBag();

    const availableItemsInBag = useMemo<number>(() => {
        const numberOfUnavailableItems = view.filter((item) => !item.isAvailable).length;
        const finalQuantity = bagEntries?.length - numberOfUnavailableItems || 0;

        return finalQuantity;
    }, [bagEntries, view]);

    return availableItemsInBag;
}
