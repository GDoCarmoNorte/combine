import { useCallback, useEffect, useState } from 'react';
import {
    ButtonTypeEnumModel,
    FulfillmentTypeEnumModel,
    InitCreditCardPaymentRequest,
    InitGiftCardPaymentRequest,
    IPaymentInitResponseModel,
    WebExperienceApi,
} from '../../@generated/webExpApi';
import { CARD_ON_FILE } from '../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/constants';
import {
    TInitialPaymentTypes,
    TPaymentMethodTypes,
} from '../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import getBrandInfo from '../../lib/brandInfo';
import { useOrderLocation } from '../../redux/hooks';
import paymentInfoStyles from '../constants/paymentInfoStyles';
import createErrorWrapper from '../services/createErrorWrapper';

export interface UsePaymentHook {
    payment?: IPaymentInitResponseModel;
    error: Error;
    isLoading: boolean;
    reset: () => Promise<void>;
}
const expApi = new WebExperienceApi();

export const usePayment = (
    paymentType: TPaymentMethodTypes,
    orderLocationId?: string,
    fulfillmentType?: FulfillmentTypeEnumModel
): UsePaymentHook => {
    const [error, setError] = useState<Error>(null);
    const [payment, setPayment] = useState<IPaymentInitResponseModel>(null);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    const { currentLocation } = useOrderLocation();

    const { brandId } = getBrandInfo();
    const locationId = orderLocationId || currentLocation?.id;
    const getPayment = useCallback(async () => {
        try {
            setIsLoading(true);

            let result: IPaymentInitResponseModel;

            const styles = paymentInfoStyles[brandId].replace(/(\r\n|\n|\r)/gm, '');

            switch (paymentType) {
                case TInitialPaymentTypes.PLACEHOLDER: {
                    const initPaymentApiGC = createErrorWrapper<IPaymentInitResponseModel, InitGiftCardPaymentRequest>(
                        'initIframeGC',
                        expApi.initGiftCardPayment.bind(expApi)
                    );
                    result = await initPaymentApiGC({
                        iGiftcardPaymentInitBodyModel: {
                            locationId,
                            submitButtonText: ButtonTypeEnumModel.Save,
                            styles,
                            fulfillmentType,
                        },
                    });
                    break;
                }
                case CARD_ON_FILE: {
                    const initPaymentApiCCWallet = createErrorWrapper<
                        IPaymentInitResponseModel,
                        InitCreditCardPaymentRequest
                    >('initIframeCC', expApi.initCreditCardPayment.bind(expApi));

                    result = await initPaymentApiCCWallet({
                        iCreditCardPaymentInitBodyModel: {
                            locationId,
                            submitButtonText: ButtonTypeEnumModel.Pay,
                            styles,
                            isOnFile: true,
                            fulfillmentType,
                        },
                    });
                    break;
                }
                default: {
                    const initPaymentApiCC = createErrorWrapper<
                        IPaymentInitResponseModel,
                        InitCreditCardPaymentRequest
                    >('initIframeCC', expApi.initCreditCardPayment.bind(expApi));
                    result = await initPaymentApiCC({
                        iCreditCardPaymentInitBodyModel: {
                            locationId,
                            submitButtonText: ButtonTypeEnumModel.Pay,
                            styles,
                            fulfillmentType,
                        },
                    });
                }
            }

            setError(null);
            setPayment(result);
        } catch (error) {
            setError(new Error('Payment service is not available. Please, try again later.'));
        } finally {
            setIsLoading(false);
        }
    }, [brandId, fulfillmentType, locationId, paymentType]);

    useEffect(() => {
        getPayment();
    }, [getPayment]);

    return { payment, error, reset: getPayment, isLoading };
};
