import { useOrderLocation, useDomainMenu, useTallyOrder } from '../../redux/hooks';
import getLocationById from '../services/locationService/getLocationById';

import { IDeliveryWrapperType } from '../services/locationService/types';
import { OrderLocationMethod } from '../../redux/orderLocation';
import { useLocationUnavailableError } from './useLocationUnavailableError';
import { getPreviousLocationData } from '../helpers/getPreviousLocationData';

interface IUseDeliveryAddressHook {
    setDeliveryAddress: (deliveryAddress: IDeliveryWrapperType) => Promise<void>;
}

export const useDeliveryAddress = (): IUseDeliveryAddressHook => {
    const {
        actions: { setDeliveryLocation, setPreviousLocation },
        deliveryAddress,
        pickupAddress,
        method,
    } = useOrderLocation();
    const {
        actions: { getDomainMenu },
    } = useDomainMenu();
    const { setUnavailableTallyItems } = useTallyOrder();
    const { pushLocationUnavailableError } = useLocationUnavailableError();

    const setDeliveryAddress = async (delivery: IDeliveryWrapperType) => {
        const locationDetails = await getLocationById({ locationId: String(delivery.pickUpLocation.id) });
        setPreviousLocation(getPreviousLocationData({ method, pickupAddress, deliveryAddress }));
        setDeliveryLocation({ ...delivery, locationDetails });
        getDomainMenu(locationDetails, OrderLocationMethod.DELIVERY);
        setUnavailableTallyItems([]);
        if (!locationDetails.isDigitallyEnabled) pushLocationUnavailableError(locationDetails);
    };

    return { setDeliveryAddress };
};
