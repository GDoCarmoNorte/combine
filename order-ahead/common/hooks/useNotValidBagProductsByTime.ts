import { useBag } from '../../redux/hooks';
import { useDomainProducts } from '../../redux/hooks/domainMenu';
import { checkIsUnavailabeProduct } from '../helpers/bagHelper';
import { useUnavailableCategories } from './useUnavailableCategories';

export const useNotValidBagProductsByTime = (): string[] => {
    const { bagEntries } = useBag();
    const unavailableCategories = useUnavailableCategories();
    const bagProducts = bagEntries.map((entry) => entry.productId);
    const domainProductsFromBag = useDomainProducts(bagProducts);

    return Object.values(domainProductsFromBag)
        .filter((p) => checkIsUnavailabeProduct(p?.categoryIds, unavailableCategories))
        .map((p) => p.id);
};
