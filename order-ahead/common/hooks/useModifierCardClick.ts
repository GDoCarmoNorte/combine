import { useCallback } from 'react';
import { IModifierSelectedParams } from '../../components/organisms/modifierCardContainer/modifierCardContainer';
import { useProductTallyModifierGroup } from '../../redux/hooks/pdp';
import { useNoSauceDomainProduct } from '../../redux/hooks/domainMenu';
import { IDisplayModifierGroup, IDisplayProduct } from '../../redux/types';

export interface IUseCardClickParams {
    modifierGroup: IDisplayModifierGroup;
    childProductId: string;
    sectionIndex: number;
    onModifierQuantityChange?: (modifier: IDisplayProduct, newQuantity: number, prevQuantity: number) => void;
}

export default function useCardClick(clickParams: IUseCardClickParams): (params: IModifierSelectedParams) => void {
    const { modifierGroup, childProductId, sectionIndex, onModifierQuantityChange } = clickParams;

    const { selectedItems, setSelectedItems, totalCount } = useProductTallyModifierGroup(
        modifierGroup.modifierGroupId,
        childProductId,
        sectionIndex
    );

    const { minQuantity, maxQuantity } = modifierGroup;

    const noSauceDomainProduct = useNoSauceDomainProduct(modifierGroup.modifiers);
    const onCardClick = useCallback(
        (params: IModifierSelectedParams) => {
            const { id, quantity, displayProduct } = params;
            // Current cannot be below zero
            const currentQuantity = Math.max(quantity, 0);

            // HANDLE RADIO SELECT WITH or WITHOUT SIZE
            if (minQuantity <= 1 && maxQuantity === 1 && currentQuantity) {
                return setSelectedItems({ [id]: { quantity: 1 } });
            }

            // HANDLE "No Sauce" case
            if (id === noSauceDomainProduct?.id && quantity === 1) {
                return setSelectedItems({ [id]: { quantity: 1 } });
            }

            // HANDLE deselect "No Sauce" case
            if (Object.keys(selectedItems).length === 1 && noSauceDomainProduct?.id === Object.keys(selectedItems)[0]) {
                return setSelectedItems({ [id]: { quantity: 1 } });
            }

            if (displayProduct.displayProductDetails.selections) {
                // HANDLE MODIFIERS WITH SELECTION
                const prevSelectionId = displayProduct.displayProductDetails.productId;
                setSelectedItems({
                    ...selectedItems,
                    [prevSelectionId]: { quantity: 0 },
                    [id]: { quantity: currentQuantity },
                });
                return;
            }

            const selectedItem = selectedItems && selectedItems[id];
            const prevQuantity = selectedItem?.quantity || 0;

            // Copy state & default current item
            const newSelectedItems = { ...selectedItems };
            newSelectedItems[id] = newSelectedItems[id] || { quantity: prevQuantity };

            const totalWithoutPreviousQuantity = totalCount - prevQuantity;
            const totalWithNewQuantity = totalWithoutPreviousQuantity + currentQuantity;
            let newQuantity = null;
            // HANDLE INCREMENT WITHOUT GOING OVER maxQuantity
            if (currentQuantity > prevQuantity && quantity <= displayProduct.displayProductDetails.maxQuantity) {
                newQuantity =
                    totalWithNewQuantity > maxQuantity
                        ? currentQuantity - (totalWithNewQuantity - maxQuantity)
                        : currentQuantity;
                newSelectedItems[id].quantity = newQuantity;
            }

            // HANDLE DECREMENT WITHOUT GOING BELOW minQuantity
            if (currentQuantity < prevQuantity) {
                newQuantity =
                    totalWithNewQuantity < minQuantity
                        ? currentQuantity + (minQuantity - totalWithNewQuantity)
                        : currentQuantity;
                newSelectedItems[id].quantity = newQuantity;
            }

            const quantityChanged = newQuantity !== null && newQuantity !== prevQuantity;
            if (quantityChanged && typeof onModifierQuantityChange === 'function') {
                onModifierQuantityChange(displayProduct, newQuantity, prevQuantity);
            }

            return setSelectedItems(newSelectedItems);
        },
        [
            minQuantity,
            maxQuantity,
            noSauceDomainProduct?.id,
            selectedItems,
            totalCount,
            onModifierQuantityChange,
            setSelectedItems,
        ]
    );

    return onCardClick;
}
