import {
    IClaimMissingPointsFromClientModel,
    SellingChannelNamesModel,
    TErrorCodeModel,
} from '../../@generated/webExpApi';
import { useState } from 'react';
import { useDealsService } from './useDealsService';
import { useRewardsActivityHistory } from './useRewardsActivityHistory';

interface useClaimPointsHook {
    hasError: boolean;
    isLoading: boolean;
    errorMessage: string;
    errorCode: TErrorCodeModel;
    onClaimPoints: (options: IClaimMissingPointsFromClientModel, resetAndValidateForm: () => void) => Promise<void>;
}

export const useClaimPoints = (): useClaimPointsHook => {
    const [hasError, setHasError] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [errorCode, setErrorCode] = useState<TErrorCodeModel>(null);

    const claimPointsService = useDealsService();
    const { getRewardsActivityHistory } = useRewardsActivityHistory();

    const updateRewardsActivity = () => {
        getRewardsActivityHistory();
    };

    const onClaimPoints = (options: IClaimMissingPointsFromClientModel, resetAndValidateForm: () => void) => {
        setIsLoading(true);
        return claimPointsService
            .claimPointsApi({
                sellingChannel: SellingChannelNamesModel.Weboa,
                iClaimMissingPointsFromClientModel: options,
            })
            .then((res) => {
                if (res.code) {
                    setHasError(true);
                    setErrorMessage(res.message);
                    setErrorCode(res.code);
                } else {
                    setHasError(false);
                    resetAndValidateForm();
                    updateRewardsActivity();
                }
                setIsLoading(false);
            })
            .catch(() => {
                setHasError(true);
                setIsLoading(false);
            });
    };

    return { hasError, isLoading, errorMessage, errorCode, onClaimPoints };
};
