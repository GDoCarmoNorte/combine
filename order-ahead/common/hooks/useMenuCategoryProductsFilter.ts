import { IProduct } from '../../@generated/@types/contentful';
import getBrandInfo from '../../lib/brandInfo';
import { useOrderLocation } from '../../redux/hooks';
import { useProducts } from '../../redux/hooks/domainMenu';
import { getContentfulProductIdsByFields } from '../helpers/getContentfulProductIdsByFields';
import { getProductByIds } from '../helpers/getProductByIds';

export default function useMenuCategoryProductsFilter(): (product: IProduct) => boolean {
    const { brandId } = getBrandInfo();
    const domainProducts = useProducts();

    const { isCurrentLocationOAAvailable } = useOrderLocation();

    return (product: IProduct) => {
        const { isVisible } = product.fields;
        const domainProduct = getProductByIds(getContentfulProductIdsByFields(product.fields), domainProducts);

        if (brandId === 'Bww' && isCurrentLocationOAAvailable) {
            return isVisible && domainProduct?.availability?.isAvailable && domainProduct?.isSaleable;
        }

        return isVisible;
    };
}
