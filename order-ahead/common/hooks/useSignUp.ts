import { ISignUpPayload } from '../services/customerService/signUp';
import { signUp } from '../services/customerService';
import { GTM_EMAIL_SIGN_UP } from '../services/gtmService/constants';
import { useAppDispatch } from '../../redux/store';
import { isMarketingPreferencesSignupEnabled } from '../../lib/getFeatureFlags';

export interface IUseSignUp {
    signUpMarketingPreferences: (payload: ISignUpPayload) => void;
}

export const useSignUp = (): IUseSignUp => {
    const dispatch = useAppDispatch();

    const signUpMarketingPreferences = (payload) => {
        if (isMarketingPreferencesSignupEnabled()) {
            signUp(payload).then(() => {
                dispatch({
                    type: GTM_EMAIL_SIGN_UP,
                });
            });
        }
    };

    return { signUpMarketingPreferences };
};
