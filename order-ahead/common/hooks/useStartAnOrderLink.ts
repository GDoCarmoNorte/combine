import { useOrderLocation } from '../../redux/hooks';
import { OrderLocationMethod } from '../../redux/orderLocation';

const useStartAnOrderLink = () => {
    const { method } = useOrderLocation();

    return method === OrderLocationMethod.NOT_SELECTED ? '/locations' : '/menu';
};

export default useStartAnOrderLink;
