import { useMemo } from 'react';
import { useOrderLocation, useBag, useConfiguration } from '../../redux/hooks';
import {
    getLocationAddressString,
    getOpeningHoursInfoString,
    isLocationClosed,
    isLocationOrderAheadAvailable,
} from '../../lib/locations';
import getLocationDetailsPageUrl from '../helpers/getLocationDetailsPageUrl';

export interface UsePickupTabHook {
    title: string;
    displayName?: string;
    detailsPageUrl?: string;
    address: string;
    phone?: string;
    storeStatus?: string;
    changeLocationText: string;
    ctaTooltip?: string;
    ctaText: string;
}

export const usePickupTab = (): UsePickupTabHook => {
    const {
        configuration: { isOAEnabled },
    } = useConfiguration();
    const { pickupAddress } = useOrderLocation();
    const bag = useBag();

    return useMemo(() => {
        const changeLocationText = 'Change Address';

        if (pickupAddress) {
            const isOnlineOrderAvailable = isLocationOrderAheadAvailable(pickupAddress, isOAEnabled);
            const isStatusClosed = isLocationClosed(pickupAddress);
            const isOnlineOrderUnavailable = isOnlineOrderAvailable && !isStatusClosed;

            let storeStatus = null;
            if (pickupAddress.status) {
                storeStatus = isStatusClosed ? 'Closed' : getOpeningHoursInfoString(pickupAddress);
            }

            return {
                title: isOnlineOrderUnavailable ? 'Pickup location' : 'Preferred location',
                displayName: pickupAddress.displayName,
                detailsPageUrl: getLocationDetailsPageUrl(pickupAddress),
                address: getLocationAddressString(pickupAddress),
                phone: pickupAddress.contactDetails?.phone,
                storeStatus,
                changeLocationText,
                ctaTooltip:
                    !isOnlineOrderUnavailable && `Online ordering not available for store ${pickupAddress.displayName}`,
                ctaText: isOnlineOrderUnavailable
                    ? bag.bagEntriesCount
                        ? 'continue pickup order'
                        : 'start pickup order'
                    : 'start order',
            };
        }

        return {
            title: null,
            displayName: null,
            detailsPageUrl: null,
            address: null,
            phone: null,
            storeStatus: null,
            changeLocationText,
            ctaTooltip: null,
            ctaText: null,
        };
    }, [pickupAddress, isOAEnabled, bag.bagEntriesCount]);
};
