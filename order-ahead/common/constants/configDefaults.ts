export const DEFAULT_BRAND = 'arbys';
export const DEFAULT_ENVIRONMENT_NAME = 'arb-dev01';
export const DEFAULT_ENVIRONMENT_TYPE = 'fast';
export const DEFAULT_AKV_TOKEN_SOURCE = 'cli';
export const DEFAULT_LOCALE = 'en-us';
