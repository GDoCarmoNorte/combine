export const EMAIL_NOT_VERIFIED_MESSAGE =
    'We need to verify your email address before you can redeem a reward. Check your email now.';
export const EMAIL_NOT_VERIFIED_TITLE = 'EMAIL UNVERIFIED';
export const ERROR_MODAL_CLOSE_BUTTON_TEXT = 'GO BACK';
