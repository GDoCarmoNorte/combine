export type Brand = 'arbys' | 'bww' | 'jje' | 'sdi';

export type BrandId = 'Arbys' | 'Bww' | 'Jje' | 'Sdi';
