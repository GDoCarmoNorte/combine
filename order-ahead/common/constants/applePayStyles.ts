import { BrandId } from './brands';

const styles: { [key in BrandId]: string | null } = {
    Arbys: null,
    Jje: null,
    Sdi: null,
    Bww: `.fp-apple-pay-button {
        width: 100%;
        height: 40px;
        border-radius: 2px;
        border-color: #CDCDCD;
        cursor: pointer;
    }

    form {
        padding: 0px;
    }`,
};

export default styles;
