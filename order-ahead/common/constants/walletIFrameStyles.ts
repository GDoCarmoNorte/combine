import { BrandId } from './brands';

const styles: { [key in BrandId]: string } = {
    Arbys: `button {
        font-family: Arial, sens-serif;
        font-size: 16px;
        font-weight: 700;
        line-height: 24px;
        color: #FFFFFF;
        height: 50px;
        background-color: #000000;
        border-radius: 0;
        border: none;
    }

    form {
        padding: 0 1.5em 0 1.5em;
    }

    input {
        border-color: #CACACA;
        border-radius: 0;
        height: 50px;
        padding: 0 0 0 20px;
    }

    input:focus {
        box-shadow: none;
        border-color: #CACACA
    }

    .invalid input {
        border-color: #d71920;
    }

    .invalid input:not(:disabled) {
        border-color: #d71920;
        border-right-width: 17px
    }

    .invalid input:not(:disabled):focus {
        border-color: #d71920;
        box-shadow: none;
    }

    .valid input:not(:disabled) {
        border-color: #CACACA;
    }

    .valid input:not(:disabled):focus {
        border-color: #CACACA;
        box-shadow: none;
    }

    label {
        color: #2a221c;
        font-family: Helvetica;
        font-weight: 700;
        font-size: 15px;
        line-height: 15px;
        text-transform: none;
    }

    .validation-message.feedback {
        line-height: 12px;
    }`,
    Bww: `button {
        font-family: Arial, sens-serif;
        font-size: 16px;
        font-weight: 700;
        line-height: 24px;
        color: #FFFFFF;
        height: 50px;
        background-color: #000000;
        border-radius: 0;
        border: none;
    }

    form {
        padding: 0 1.5em 0 1.5em;
    }

    input {
        border-color: #CACACA;
        border-radius: 0;
        height: 50px;
        padding: 0 0 0 20px;
    }

    input:focus {
        box-shadow: none;
        border-color: #CACACA
    }

    .invalid input {
        border-color: #d71920;
    }

    .invalid input:not(:disabled) {
        border-color: #d71920;
        border-right-width: 17px
    }

    .invalid input:not(:disabled):focus {
        border-color: #d71920;
        box-shadow: none;
    }

    .valid input:not(:disabled) {
        border-color: #CACACA;
    }

    .valid input:not(:disabled):focus {
        border-color: #CACACA;
        box-shadow: none;
    }

    label {
        color: #2a221c;
        font-family: Helvetica;
        font-weight: 700;
        font-size: 15px;
        line-height: 15px;
        text-transform: none;
    }

    .validation-message.feedback {
        line-height: 12px;
    }`,
    Jje: `button {
        font-family: Arial, sens-serif;
        font-size: 16px;
        font-weight: 700;
        line-height: 24px;
        color: #FFFFFF;
        height: 50px;
        background-color: #000000;
        border-radius: 0;
        border: none;
    }

    form {
        padding: 0 1.5em 0 1.5em;
    }

    input {
        border-color: #CACACA;
        border-radius: 0;
        height: 50px;
        padding: 0 0 0 20px;
    }

    input:focus {
        box-shadow: none;
        border-color: #CACACA
    }

    .invalid input {
        border-color: #d71920;
    }

    .invalid input:not(:disabled) {
        border-color: #d71920;
        border-right-width: 17px
    }

    .invalid input:not(:disabled):focus {
        border-color: #d71920;
        box-shadow: none;
    }

    .valid input:not(:disabled) {
        border-color: #CACACA;
    }

    .valid input:not(:disabled):focus {
        border-color: #CACACA;
        box-shadow: none;
    }

    label {
        color: #2a221c;
        font-family: Helvetica;
        font-weight: 700;
        font-size: 15px;
        line-height: 15px;
        text-transform: none;
    }

    .validation-message.feedback {
        line-height: 12px;
    }`,
    Sdi: ``,
};

export default styles;
