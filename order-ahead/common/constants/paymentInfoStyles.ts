import { BrandId } from './brands';

const styles: { [key in BrandId]: string } = {
    Arbys: `button {
        font-family: League Gothic, Arial, sens-serif;
        font-size: 26px;
        line-height: 26px;
        color: #fff;
        height: 50px;
        background-color: #d71920;
        border-radius: 0;
        box-shadow: 0 20px 20px rgba(0, 0, 0, .1);
        border: none;
    }
    
    button:hover {
        background-color: #d71920
    }
    
    button:focus {
        border: none
    }
    
    input {
        border-color: #737373;
        border-radius: 0;
        height: 50px;
        padding: 0 0 0 20px;
    }
    
    input:focus {
        box-shadow: none;
        border-color: #737373
    }
    
    .invalid input {
        border-color: #d71920
    }
    
    .invalid input:not(:disabled) {
        border-color: #d71920;
        border-right-width: 17px
    }
    
    .invalid input:not(:disabled):focus {
        border-color: #d71920;
        box-shadow: none
    }
    
    .valid input:not(:disabled) {
        border-color: #737373
    }
    
    .valid input:not(:disabled):focus {
        border-color: #737373;
        box-shadow: none
    }
    
    label {
        color: #2a221c;
        font-family: Arial, sens-serif;
        text-transform: uppercase;
    }
    
    .validation-message.feedback {
        line-height: 12px;
    }`,
    Bww: `button {
        font-family: Arial, sens-serif;
        font-size: 18px;
        font-weight: 700;
        line-height: 18px;
        color: #382E2C;
        height: 50px;
        background-color: #FFC600;
        border-radius: 0;
        border: none;
        text-transform: uppercase;
    }

    button:hover {
        background-color: #EDB900;
    }
    
    input {
        border-color: #382E2C;
        border-radius: 0;
        height: 50px;
        padding: 0 0 0 20px;
    }
    
    input:focus {
        box-shadow: none;
        border-color: #382E2C
    }
    
    .invalid input {
        border-color: #d71920;
    }
    
    .invalid input:not(:disabled) {
        border-color: #d71920;
        border-right-width: 17px
    }
    
    .invalid input:not(:disabled):focus {
        border-color: #d71920;
        box-shadow: none;
    }
    
    .valid input:not(:disabled) {
        border-color: #382E2C;
    }
    
    .valid input:not(:disabled):focus {
        border-color: #382E2C;
        box-shadow: none;
    }
    
    label {
        color: #382E2C;
        font-family: Arial, sens-serif;
        text-transform: uppercase;
        font-size: 12px;
    }
    
    .validation-message.feedback {
        line-height: 12px;
    }`,
    Jje: `button {
        font-family: League Gothic, Arial, sens-serif;
        font-size: 26px;
        line-height: 26px;
        color: #fff;
        height: 50px;
        background-color: #d71920;
        border-radius: 0;
        box-shadow: 0 20px 20px rgba(0, 0, 0, .1);
        border: none;
    }
    
    button:hover {
        background-color: #d71920
    }
    
    button:focus {
        border: none
    }
    
    input {
        border-color: #737373;
        border-radius: 0;
        height: 50px;
        padding: 0 0 0 20px;
    }
    
    input:focus {
        box-shadow: none;
        border-color: #737373
    }
    
    .invalid input {
        border-color: #d71920
    }
    
    .invalid input:not(:disabled) {
        border-color: #d71920;
        border-right-width: 17px
    }
    
    .invalid input:not(:disabled):focus {
        border-color: #d71920;
        box-shadow: none
    }
    
    .valid input:not(:disabled) {
        border-color: #737373
    }
    
    .valid input:not(:disabled):focus {
        border-color: #737373;
        box-shadow: none
    }
    
    label {
        color: #2a221c;
        font-family: Arial, sens-serif;
        text-transform: uppercase;
    }
    
    .validation-message.feedback {
        line-height: 12px;
    }`,
    Sdi: ``,
};

export default styles;
