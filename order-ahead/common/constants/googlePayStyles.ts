import { BrandId } from './brands';

const styles: { [key in BrandId]: string | null } = {
    Arbys: null,
    Jje: null,
    Sdi: null,
    Bww: `form {
        padding: 0 1px;
        height: 50px;
    }`,
};

export default styles;
