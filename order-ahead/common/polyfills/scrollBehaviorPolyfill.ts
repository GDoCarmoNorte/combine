const init = async (): Promise<void> => {
    if (typeof window !== 'undefined') {
        if (!('scrollBehavior' in document.documentElement.style)) {
            await import('scroll-behavior-polyfill');
        }
    }
};

export default init;
