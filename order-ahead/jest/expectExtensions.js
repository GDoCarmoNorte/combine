expect.extend({
    toContainElements(received, elements) {
        const notFoundElements = [];
        for (let element of elements) {
            if (!received[element]) notFoundElements.push(element);
        }

        if (notFoundElements.length) {
            const text =
                notFoundElements.length === 1
                    ? `Element ${notFoundElements} is not found`
                    : `Elements ${notFoundElements.join(',')} are not found`;
            return { message: () => text, pass: false };
        }
        return { message: () => 'All elements found', pass: true };
    },
});
