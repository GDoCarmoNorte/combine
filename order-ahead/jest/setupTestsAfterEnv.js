import '@testing-library/jest-dom';

import jestFetchMock from 'jest-fetch-mock';

import './expectExtensions';
// a11y
import { toHaveNoViolations } from 'jest-axe';
import { createClient as createContentfulClient } from 'contentful';

import configurationMock from '../tests/mocks/configuration.mock';

jestFetchMock.enableMocks();

expect.extend(toHaveNoViolations);

// env variables
process.env.NEXT_PUBLIC_BRAND = 'arbys';
process.env.ENVIRONMENT_NAME = 'arb-dev01';
process.env.LOCALE = 'en-us';

process.env.NEXT_PUBLIC_DOMAIN_CUSTOMER = '/domainCustomerPath';
process.env.NEXT_PUBLIC_WEB_EXP_API = '/webExpApiPath';

process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = 'https://locations.arbys.com';

process.env.CONTENTFUL_SPACE = 'mock-space';
process.env.CONTENTFUL_ENV = 'mock-env';
process.env.CONTENTFUL_DELIVERY_TOKEN = 'mock-delivery-token';
process.env.CONTENTFUL_PREVIEW_TOKEN = 'mock-preview-token';

process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY = 'mock-google-key';

process.env.NEXT_PUBLIC_PAYMENT_DOMAIN = 'https://hpc.uat.freedompay.test';
process.env.NEXT_PUBLIC_APP_URL = 'http://www.test.app.url';
process.env.NEXT_PUBLIC_MENU_VERSION = 2;

process.env.NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL = 'mock url';
process.env.NEXT_PUBLIC_GOOGLE_PAY_MODE = 'TEST';

process.env.NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL = 'mock-fp-script-url';
process.env.NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID = 'mocked-apple-merchant-id';

process.env.NEXT_PUBLIC_KOUNT_DATA_COLLECTOR_URL = 'mock url';
process.env.NEXT_PUBLIC_KOUNT_MERCHANT_ID = 100181;

process.env.NEXT_PUBLIC_AUTH_0_CLIENT_ID = 'AUTH0CLIENTID';

const localStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn(),
    removeItem: jest.fn(),
};
Object.defineProperty(window, 'localStorage', { value: localStorageMock });

const sessionStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn(),
    removeItem: jest.fn(),
};
Object.defineProperty(window, 'sessionStorage', { value: sessionStorageMock });

const geolocationMock = {
    getCurrentPosition: jest.fn(),
};

Object.defineProperty(window.navigator, 'geolocation', { value: geolocationMock });

const newrelicMock = {
    noticeError: jest.fn(),
    addPageAction: jest.fn(),
    setCustomAttribute: jest.fn(),
    addRelease: jest.fn(),
};

Object.defineProperty(window, 'newrelic', { value: newrelicMock });

// mock useLayoutEffect on SSR
jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useLayoutEffect: jest.requireActual('react').useEffect,
}));

// mock material-ui JSS as we do not support it yet
jest.mock('@material-ui/styles/makeStyles', () => ({
    __esModule: true,
    default: () => jest.fn().mockImplementation(() => ({})),
}));

// mock user agent parser
jest.mock('ua-parser-js', () => ({
    __esModule: true,
    default: jest.fn().mockImplementation(() => ({
        getBrowser: jest.fn().mockReturnValue({ name: 'Chrome', version: '88' }),
    })),
}));

jest.mock('../common/services/contentfulConfiguration/contentfulConfiguration', () => {
    return {
        __esModule: true,
        getContentfulConfigurationExpApi: jest.fn().mockReturnValue(Promise.resolve(undefined)),
    };
});

jest.mock('../redux/hooks/useConfiguration', () =>
    jest.fn(() => ({
        configuration: configurationMock,
        lastSyncedAt: new Date('2021-01-01T00:00:00.000Z'),
        actions: {
            setConfiguration: jest.fn(),
        },
    }))
);

const { CONTENTFUL_ENV, CONTENTFUL_SPACE, CONTENTFUL_DELIVERY_TOKEN } = process.env;
const mockContentfulClient = createContentfulClient({
    space: CONTENTFUL_SPACE,
    environment: CONTENTFUL_ENV,
    accessToken: CONTENTFUL_DELIVERY_TOKEN,
});

jest.mock('../lib/contentfulClient', () => ({
    ...jest.requireActual('../lib/contentfulClient'),
    createClient: jest.fn().mockImplementation(() => mockContentfulClient),
}));

Object.defineProperty(global.self, 'crypto', {
    value: {
        getRandomValues: (arr) => crypto.randomBytes(arr.length),
    },
});

global.scrollTo = jest.fn();

global.crypto.subtle = {};
