const { configureAxe } = require('jest-axe');

const configuredAxe = configureAxe({
    rules: {
        region: { enabled: false },
        'landmark-no-duplicate-banner': { enabled: false },
        'landmark-unique': { enabled: false },
    },
});

const axe = async (options) => {
    const axeResult = await configuredAxe(options);
    return axeResult;
};

module.exports = { axe };
// https://www.npmjs.com/package/jest-axe#setting-global-configuration
