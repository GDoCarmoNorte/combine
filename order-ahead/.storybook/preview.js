import viewports from './viewports';

import '../public/normalize.css';
import '../styles/variables.css';
import '../styles/global.css';
import '../public/legacy/inspire-icons.css';

export const parameters = {
    controls: {
        expanded: true,
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
    actions: { argTypesRegex: '^on[A-Z].*' },
    viewport: { viewports },
    backgrounds: {
        values: [
            { name: 'Light', value: 'var(--col--light)' },
            { name: 'Dark', value: 'var(--col--dark)' },
            { name: 'Primary', value: 'var(--col--primary1)' },
            { name: 'Primary Variant', value: 'var(--col--primary1variant)' },
        ],
    },
    storySort: {
        order: ['Pages', 'Components', ['Atoms', 'Molecules', 'Sections', 'Organisms', 'ClientOnly']],
    },
};
