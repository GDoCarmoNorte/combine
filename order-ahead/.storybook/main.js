const componentToggler = require('../webpack/componentToggler');
const setFeatureFlags = require('../webpack/setFeatureFlags');

module.exports = {
    framework: '@storybook/react',
    stories: ['../stories/**/*.stories.mdx', '../stories/**/*.stories.@(ts|tsx)'],
    staticDirs: [{ from: '../public', to: '/' }],
    core: { builder: 'webpack5' },
    addons: [
        '@storybook/addon-links',
        '@storybook/addon-essentials',
        '@storybook/addon-viewport',
        '@storybook/addon-a11y',
        'storybook-css-modules-preset',
    ],
    previewHead: (head) => {
        // process.env.NEXT_PUBLIC_BRAND is not available outside
        const brandId = process.env.NEXT_PUBLIC_BRAND;

        /* Injecting brand specific styles */
        return `
            ${head}
            <link rel="stylesheet" href="brands/${brandId}/theme.css" />
            <link rel="stylesheet" href="brands/${brandId}/brand-icons.css" />
            <link rel="stylesheet" href="brands/${brandId}/fonts.css" />
            <link rel="stylesheet" href="brands/${brandId}/typography.css" />
            <link rel="stylesheet" href="brands/${brandId}/lists.css" />
            <link rel="stylesheet" href="brands/${brandId}/molecules.css" />
            <link rel="stylesheet" href="brands/${brandId}/global.css" />
        `;
    },
    webpackFinal: async (config) => {
        /* normalizing css import() */
        for (let rule of config.module.rules) {
            if (rule.use && rule.use.length > 0) {
                for (let use of rule.use) {
                    if (use.loader && use.loader.includes('/css-loader/')) {
                        use.options = {
                            ...use.options,
                            url: (url, resourcePath) => !url.startsWith('/'),
                        };
                    }
                }
            }
        }

        return componentToggler(setFeatureFlags(config));
    },
};
