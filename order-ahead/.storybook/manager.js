import { addons } from '@storybook/addons';
import { create } from '@storybook/theming';

const brandId = process.env.NEXT_PUBLIC_BRAND;

const inspireTheme = create({
    base: 'light',
    brandTitle: `${brandId} storybook`,
    brandImage: `/brands/${brandId}/sb-logo.png`,
});

addons.setConfig({
    theme: inspireTheme,
});
