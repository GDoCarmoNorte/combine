import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

const inspireViewports = {
    inspireMobile: {
        name: 'Inspire:Mobile',
        styles: {
            height: '660px',
            width: '375px',
        },
        type: 'mobile',
    },
    inspireDesktop: {
        name: 'Inspire:Desktop',
        styles: {
            height: '900px',
            width: '1440px',
        },
        type: 'desktop',
    },
};

export default { ...inspireViewports, ...INITIAL_VIEWPORTS };
