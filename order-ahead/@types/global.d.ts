import NewRelicBrowser from '@types/new-relic-browser';

declare global {
    interface Window {
        newrelic: typeof NewRelicBrowser;
        opera: any;
        FreedomPay: any;
        ka: any;
        ReactNativeWebView: any;
    }
}
