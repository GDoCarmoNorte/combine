import { resolve } from 'path';
import { writeFileSync } from 'fs';
import dotenv from 'dotenv';

import {
    AKV_TOKEN_SOURCE_KEY,
    BRAND_KEY,
    CONTENTFUL_ENV_KEY,
    ENVIRONMENT_NAME_KEY,
    ENVIRONMENT_TYPE_KEY,
    LOCALE_KEY,
    getArgv,
} from '../lib/cli';
import { loadSecureKeys } from '../lib/azureCli';
import {
    DEFAULT_BRAND,
    DEFAULT_ENVIRONMENT_NAME,
    DEFAULT_ENVIRONMENT_TYPE,
    DEFAULT_LOCALE,
    DEFAULT_AKV_TOKEN_SOURCE,
} from '../common/constants/configDefaults';

const brand = getArgv(BRAND_KEY) || DEFAULT_BRAND;
const environmentName = getArgv(ENVIRONMENT_NAME_KEY) || DEFAULT_ENVIRONMENT_NAME;
const environmentType = getArgv(ENVIRONMENT_TYPE_KEY) || DEFAULT_ENVIRONMENT_TYPE;
const locale = (getArgv(LOCALE_KEY) || DEFAULT_LOCALE).toLowerCase();
const tokenSource = getArgv(AKV_TOKEN_SOURCE_KEY) || DEFAULT_AKV_TOKEN_SOURCE;

const generateConfigurationFile = () => {
    let configurationFile = `config/${environmentName}/${brand}.env`;
    if (locale !== DEFAULT_LOCALE) configurationFile = `config/${environmentName}/${brand}-${locale}.env`;
    console.log(
        '\n[CONFIG]',
        `Generating configuration file for brand: "${brand}", environment: "${environmentName}", locale: "${locale}", type: "${environmentType}'\n`
    );

    const config = dotenv.config({
        path: resolve(process.cwd(), configurationFile),
    });
    if ('error' in config) throw config.error;

    return loadSecureKeys(tokenSource, config).then((configWithSecrets) => {
        const {
            CONTENTFUL_ENV,
            CONTENTFUL_SPACE,
            NEXT_PUBLIC_GA_TRACKING_ID,
            NEXT_PUBLIC_GOOGLE_MAP_API_KEY,
            NEXT_PUBLIC_PAYMENT_DOMAIN,
            NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN,
            NEXT_PUBLIC_DOMAIN_CUSTOMER,
            NEXT_PUBLIC_MENU_VERSION,
            NEXT_PUBLIC_WEB_EXP_API,
            ARTIFACT_VERSION,
            NEXT_PUBLIC_NEWRELIC_ACCOUNT_ID,
            NEXT_PUBLIC_NEWRELIC_TRUST_KEY,
            NEXT_PUBLIC_NEWRELIC_AGENT_ID,
            NEXT_PUBLIC_NEWRELIC_LICENSE_KEY,
            NEXT_PUBLIC_NEWRELIC_APPLICATION_ID,
            NEXT_PUBLIC_NEWRELIC_DISTRIBUTED_TRACING_ALLOWED_ORIGINS,
            NEXT_PUBLIC_APP_URL,
            NEXT_PUBLIC_AUTH_0_DOMAIN,
            NEXT_PUBLIC_AUTH_0_CLIENT_ID,
            NEXT_PUBLIC_AUTH_0_PROGRESSIVE_PROFILE_JWT_PUBLIC_KEY,
            NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL,
            NEXT_PUBLIC_GOOGLE_PAY_MODE,
            NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL,
            NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID,
            NEXT_PUBLIC_KOUNT_DATA_COLLECTOR_URL,
            NEXT_PUBLIC_KOUNT_MERCHANT_ID,
            NEXT_PUBLIC_PLAY_PAGE_URL,
            NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY,
        } = process.env;

        const {
            CONTENTFUL_PREVIEW_TOKEN,
            CONTENTFUL_DELIVERY_TOKEN,
            CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN,
        } = configWithSecrets;

        const contentfulEnv = getArgv(CONTENTFUL_ENV_KEY) || CONTENTFUL_ENV;

        const APPLE_PAY_MERCHANT_ID = NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID ? `"${NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID}"` : '';

        const config = `NEXT_PUBLIC_BRAND=${brand}
CONTENTFUL_ENV=${contentfulEnv}
CONTENTFUL_SPACE=${CONTENTFUL_SPACE}
CONTENTFUL_PREVIEW_TOKEN=${CONTENTFUL_PREVIEW_TOKEN}
CONTENTFUL_DELIVERY_TOKEN=${CONTENTFUL_DELIVERY_TOKEN}
CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN=${CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN}
ENVIRONMENT_NAME=${environmentName}
LOCALE=${locale}
SSG=${environmentType === 'fast' ? 'true' : 'false'}
NEXT_PUBLIC_LOCALE=${locale}
NEXT_PUBLIC_GA_TRACKING_ID=${NEXT_PUBLIC_GA_TRACKING_ID}
NEXT_PUBLIC_GOOGLE_MAP_API_KEY=${NEXT_PUBLIC_GOOGLE_MAP_API_KEY}
NEXT_PUBLIC_PAYMENT_DOMAIN=${NEXT_PUBLIC_PAYMENT_DOMAIN}
NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN=${NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN}
NEXT_PUBLIC_DOMAIN_CUSTOMER=${NEXT_PUBLIC_DOMAIN_CUSTOMER}
NEXT_PUBLIC_MENU_VERSION=${NEXT_PUBLIC_MENU_VERSION}
NEXT_PUBLIC_WEB_EXP_API=${NEXT_PUBLIC_WEB_EXP_API}
NEXT_PUBLIC_ARTIFACT_VERSION=${ARTIFACT_VERSION || ''}
NEXT_PUBLIC_NEWRELIC_ACCOUNT_ID=${NEXT_PUBLIC_NEWRELIC_ACCOUNT_ID || ''}
NEXT_PUBLIC_NEWRELIC_TRUST_KEY=${NEXT_PUBLIC_NEWRELIC_TRUST_KEY || ''}
NEXT_PUBLIC_NEWRELIC_AGENT_ID=${NEXT_PUBLIC_NEWRELIC_AGENT_ID || ''}
NEXT_PUBLIC_NEWRELIC_LICENSE_KEY=${NEXT_PUBLIC_NEWRELIC_LICENSE_KEY || ''}
NEXT_PUBLIC_NEWRELIC_APPLICATION_ID=${NEXT_PUBLIC_NEWRELIC_APPLICATION_ID || ''}
NEXT_PUBLIC_NEWRELIC_DISTRIBUTED_TRACING_ALLOWED_ORIGINS=${
            NEXT_PUBLIC_NEWRELIC_DISTRIBUTED_TRACING_ALLOWED_ORIGINS || ''
        }
NEXT_PUBLIC_ENVIRONMENT_NAME=${environmentName}
NEXT_PUBLIC_APP_URL=${NEXT_PUBLIC_APP_URL}
NEXT_PUBLIC_AUTH_0_DOMAIN=${NEXT_PUBLIC_AUTH_0_DOMAIN}
NEXT_PUBLIC_AUTH_0_CLIENT_ID=${NEXT_PUBLIC_AUTH_0_CLIENT_ID}
NEXT_PUBLIC_AUTH_0_PROGRESSIVE_PROFILE_JWT_PUBLIC_KEY="${NEXT_PUBLIC_AUTH_0_PROGRESSIVE_PROFILE_JWT_PUBLIC_KEY}"
NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL=${NEXT_PUBLIC_GOOGLE_PAY_SCRIPT_URL}
NEXT_PUBLIC_GOOGLE_PAY_MODE=${NEXT_PUBLIC_GOOGLE_PAY_MODE}
NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL=${NEXT_PUBLIC_APPLE_PAY_SCRIPT_URL || ''}
NEXT_PUBLIC_APPLE_PAY_MERCHANT_ID=${APPLE_PAY_MERCHANT_ID}
NEXT_PUBLIC_KOUNT_DATA_COLLECTOR_URL=${NEXT_PUBLIC_KOUNT_DATA_COLLECTOR_URL}
NEXT_PUBLIC_KOUNT_MERCHANT_ID=${NEXT_PUBLIC_KOUNT_MERCHANT_ID}
NEXT_PUBLIC_PLAY_PAGE_URL=${NEXT_PUBLIC_PLAY_PAGE_URL || ''}
NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY=${NEXT_PUBLIC_PLAY_PAGE_INTEGRATION_KEY}
    `.trimStart();

        writeFileSync(resolve(process.cwd(), '.env'), config, { encoding: 'utf-8' });
        console.log('[CONFIG]', `Configuration file generated successfully!\n`);
    });
};

generateConfigurationFile().catch((error) => {
    console.error(error.message);
    console.error(
        '[CONFIG]',
        `Error occurred while generating configuration file for brand: "${brand}", environment: "${environmentName}", locale: "${locale}", type: "${environmentType}'\n`
    );
    process.exit(-1);
});
