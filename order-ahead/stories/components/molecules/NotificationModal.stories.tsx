import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';

import NotificationModal from '../../../components/molecules/notificationModal/notificationModal';

export default {
    title: 'Design system/Molecules/NotificationModal',
    component: NotificationModal,
} as ComponentMeta<typeof NotificationModal>;

const Template: ComponentStory<typeof NotificationModal> = (args) => <NotificationModal {...args} />;

export const Primary = Template.bind({});

Primary.args = {
    open: true,
    title: 'Test',
    message: 'Test message',
    rejectButtonProps: { text: 'No' },
    confirmButtonProps: { text: 'Yes' },
};
