import React from 'react';

import Footer from '../../../components/organisms/footer';
import footerMock from '../../../tests/components/footer/index.mock';

export default {
    title: 'Design system/Organisms/Footer',
    component: Footer,
};

const Template = () => <Footer footer={footerMock} />;

export const Default = Template.bind({});
