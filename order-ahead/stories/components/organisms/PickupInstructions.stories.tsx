import React from 'react';

import PickupInstructions from '../../../components/organisms/confirmation/pickupInstructions/pickupInstructions';
import pickupInstructionsMock from '../../../tests/components/organisms/confirmation/pickupInstructions/pickupInstructions.mock';

export default {
    title: 'Design system/Organisms/PickupInstructions',
    component: PickupInstructions,
};

const Template = () => <PickupInstructions pickupInstructions={pickupInstructionsMock.fields.instractions} />;

export const Default = Template.bind({});
