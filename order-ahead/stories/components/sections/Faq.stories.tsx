import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';

import Faq from '../../../components/sections/faq';
import { withSubItems } from '../../../tests/components/sections/faq/faqWithAccordion/faqWithAccordion.mock';

export default {
    title: 'Design system/Sections/Faq',
    component: Faq,
} as ComponentMeta<typeof Faq>;

const Template: ComponentStory<any> = ({ title, withAccordion }) => {
    const entry = { ...withSubItems, fields: { ...withSubItems.fields, title, withAccordion } } as any;

    return <Faq entry={entry} />;
};

export const Default = Template.bind({});

Default.args = {
    title: 'Title',
    withAccordion: true,
};
