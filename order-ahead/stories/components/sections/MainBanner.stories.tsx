import React from 'react';

import MainBanner from '../../../components/sections/mainBanner';
import arbysMock from './__mocks__/arbysMainBanner.mock';
import bwwMock from './__mocks__/bwwMainBanner.mock';
import { IMainBanner, IMainBannerFields } from '../../../@generated/@types/contentful';

function getMainBannerMock(): IMainBanner {
    switch (process.env.NEXT_PUBLIC_BRAND) {
        case 'arbys':
            return arbysMock;
        case 'bww':
            return bwwMock;
        default:
            console.warn('the data is missed');
            return arbysMock;
    }
}

export default {
    title: 'Design system/Sections/MainBanner',
    component: MainBanner,
    argTypes: {
        backgroundColor: {
            control: { type: 'text' },
        },
        mainTextSize: {
            control: { type: 'radio' },
            options: ['h1', 'h2'],
        },
    },
};

const mock = getMainBannerMock();

const Template = ({
    topText,
    mainText,
    backgroundColor,
    mainLinkText,
    secondaryLink,
    mainTextSize,
    verticallyCentered,
    centeredContent,
}) => {
    const mock = getMainBannerMock();

    return (
        <MainBanner
            entry={{
                ...mock,
                fields: {
                    ...mock.fields,
                    topText,
                    mainText,
                    backgroundColor,
                    mainLinkText,
                    secondaryLink,
                    mainTextSize,
                    verticallyCentered,
                    centeredContent,
                },
            }}
        />
    );
};

export const Default = Template.bind({});

Default.args = {
    topText: mock.fields.topText || '',
    mainText: mock.fields.mainText || '',
    backgroundColor: mock.fields.backgroundColor || '',
    mainLinkText: mock.fields.mainLinkText || '',
    mainTextSize: mock.fields.mainTextSize || 'h1',
    verticallyCentered: mock.fields.verticallyCentered || false,
    centeredContent: mock.fields.centeredContent || false,
} as IMainBannerFields;
