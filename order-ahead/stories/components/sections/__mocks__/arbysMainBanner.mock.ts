import { IMainBanner } from '../../../../@generated/@types/contentful';

export default ({
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'o19mhvm9a2cm',
            },
        },
        id: '59N1K7lT9A3RUwKfhFTkFF',
        type: 'Entry',
        createdAt: '2022-01-18T21:53:15.560Z',
        updatedAt: '2022-01-18T21:53:15.560Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 1,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'mainBanner',
            },
        },
        locale: 'en-US',
    },
    fields: {
        mainText: 'THE DIABLO DARE',
        bottomText: {
            data: {},
            content: [
                {
                    data: {},
                    content: [
                        {
                            data: {},
                            marks: [],
                            value:
                                "Take on the heat with the spiciest sandwich in fast food! So hot, we'll give you a FREE snack size Vanilla shake to help cool ya down! \n \n\n\n\n\n",
                            nodeType: 'text',
                        },
                    ],
                    nodeType: 'paragraph',
                },
            ],
            nodeType: 'document',
        },
        rightImage: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '6547xpeaSALvNE4GlsBG2g',
                type: 'Asset',
                createdAt: '2022-01-18T21:52:44.036Z',
                updatedAt: '2022-01-18T21:52:44.036Z',
                environment: {
                    sys: {
                        id: 'dev',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'Website LTO Jan22 Meal DiabloBrisket',
                description: '',
                file: {
                    url:
                        '//images.ctfassets.net/o19mhvm9a2cm/6547xpeaSALvNE4GlsBG2g/ccadcc3448fef7255c1e075e8420903f/Website_LTO_Jan22_Meal_DiabloBrisket.png',
                    details: {
                        size: 2568039,
                        image: {
                            width: 4000,
                            height: 3000,
                        },
                    },
                    fileName: 'Website_LTO_Jan22_Meal_DiabloBrisket.png',
                    contentType: 'image/png',
                },
            },
        },
        mainLinkText: "Find Your Arby's",
        mainLinkHref: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'o19mhvm9a2cm',
                    },
                },
                id: '5f665bbc-78ab-4600-8e0a-36f73c9107fe',
                type: 'Entry',
                createdAt: '2021-04-21T08:05:17.831Z',
                updatedAt: '2021-04-21T08:05:17.831Z',
                environment: {
                    sys: {
                        id: 'dev',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'pageLink',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'LOCATIONS',
                nameInUrl: 'locations',
            },
        },
    },
} as unknown) as IMainBanner;
