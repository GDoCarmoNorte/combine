import { IMainBanner } from '../../../../@generated/@types/contentful';

export default ({
    metadata: {
        tags: [],
    },
    sys: {
        space: {
            sys: {
                type: 'Link',
                linkType: 'Space',
                id: 'l5fkpck1mwg3',
            },
        },
        id: '7bIkTlsIkhmKKN96BN3wSY',
        type: 'Entry',
        createdAt: '2022-01-05T19:14:17.766Z',
        updatedAt: '2022-01-13T16:02:13.625Z',
        environment: {
            sys: {
                id: 'dev',
                type: 'Link',
                linkType: 'Environment',
            },
        },
        revision: 2,
        contentType: {
            sys: {
                type: 'Link',
                linkType: 'ContentType',
                id: 'mainBanner',
            },
        },
        locale: 'en-US',
    },
    fields: {
        topText: 'BONELESS THURSDAYS ',
        mainText: 'BOGO FREE BONELESS WINGS',
        bottomText: {
            data: {},
            content: [
                {
                    data: {},
                    content: [
                        {
                            data: {},
                            marks: [],
                            value:
                                'Get more wings with your wings with buy one, get one FREE boneless wings every Thursday.\n \n\n\n\n\n',
                            nodeType: 'text',
                        },
                    ],
                    nodeType: 'paragraph',
                },
            ],
            nodeType: 'document',
        },
        backgroundImage: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: 'g3nlqLKMYJbAdVlht3rlZ',
                type: 'Asset',
                createdAt: '2022-01-05T19:01:25.143Z',
                updatedAt: '2022-01-05T19:01:25.143Z',
                environment: {
                    sys: {
                        id: 'dev',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'BoneLessThursday Web 4000x1650 FadeLeft',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/g3nlqLKMYJbAdVlht3rlZ/8d368906d15ace1aeb38c0a17cacdc6e/BoneLessThursday_Web_4000x1650_FadeLeft.jpg',
                    details: {
                        size: 533231,
                        image: {
                            width: 4000,
                            height: 1650,
                        },
                    },
                    fileName: 'BoneLessThursday_Web_4000x1650_FadeLeft.jpg',
                    contentType: 'image/jpeg',
                },
            },
        },
        mainLinkText: 'Order Now',
        mainLinkHref: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '6hExIJafUrvolJ0F8Z65tA',
                type: 'Entry',
                createdAt: '2021-08-03T13:49:17.690Z',
                updatedAt: '2022-01-13T15:59:56.460Z',
                environment: {
                    sys: {
                        id: 'dev',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 7,
                contentType: {
                    sys: {
                        type: 'Link',
                        linkType: 'ContentType',
                        id: 'product',
                    },
                },
                locale: 'en-US',
            },
            fields: {
                name: 'BOGO 6 Boneless Wings (12 Total)',
                nameInUrl: 'boneless-bogo',
                image: {
                    sys: {
                        type: 'Link',
                        linkType: 'Asset',
                        id: '1HxxOxYjd9fXySGIxycxFh',
                    },
                },
                productId: 'IDPSalesItem-5297',
                metaDescription:
                    'Enjoy our BOGO 6 Boneless Wings when you order for delivery or pick up from a nearby Buffalo Wild Wings®, the ultimate place for wings, beer, and sports.',
                isVisible: true,
                productIdList: ['IDPSalesItem-5297'],
            },
        },
        verticallyCentered: false,
        backgroundForMobile: {
            metadata: {
                tags: [],
            },
            sys: {
                space: {
                    sys: {
                        type: 'Link',
                        linkType: 'Space',
                        id: 'l5fkpck1mwg3',
                    },
                },
                id: '3g4MOQ4Ya8ADMKjMJVJK5V',
                type: 'Asset',
                createdAt: '2022-01-05T19:01:25.142Z',
                updatedAt: '2022-01-05T19:01:25.142Z',
                environment: {
                    sys: {
                        id: 'dev',
                        type: 'Link',
                        linkType: 'Environment',
                    },
                },
                revision: 1,
                locale: 'en-US',
            },
            fields: {
                title: 'BoneLessThursday Web 2000x2400 AllFaded',
                file: {
                    url:
                        '//images.ctfassets.net/l5fkpck1mwg3/3g4MOQ4Ya8ADMKjMJVJK5V/6246f81299925dd1d2f5c1941ae175f0/BoneLessThursday_Web_2000x2400_AllFaded.jpg',
                    details: {
                        size: 272297,
                        image: {
                            width: 2000,
                            height: 2400,
                        },
                    },
                    fileName: 'BoneLessThursday_Web_2000x2400_AllFaded.jpg',
                    contentType: 'image/jpeg',
                },
            },
        },
        centeredContent: false,
    },
} as unknown) as IMainBanner;
