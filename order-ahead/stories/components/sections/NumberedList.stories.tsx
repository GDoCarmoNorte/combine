import React from 'react';

import { ComponentStory, ComponentMeta } from '@storybook/react';

import NumberedListSection from '../../../components/sections/numberedList/numberedListSection';
import numberedListMock from '../../../tests/components/sections/numberedList/numberedList.mock';

export default {
    title: 'Design system/Sections/NumberedList',
    component: NumberedListSection,
} as ComponentMeta<typeof NumberedListSection>;

const Template: ComponentStory<any> = ({ title, description }) => {
    const entry = { ...numberedListMock, fields: { ...numberedListMock.fields, title, description } } as any;

    return <NumberedListSection entry={entry} />;
};

export const Default = Template.bind({});

Default.args = {
    title: numberedListMock.fields.title,
    description: numberedListMock.fields.description,
};
