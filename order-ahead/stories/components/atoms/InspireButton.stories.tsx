import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { InspireButton, IInspireButtonProps } from '../../../components/atoms/button';

export default {
    title: 'Design system/Atoms/InspireButton',
    component: InspireButton,
} as ComponentMeta<typeof InspireButton>;

const Template: ComponentStory<typeof InspireButton> = (args) => <InspireButton {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    text: 'Button Primary',
    type: 'primary',
} as Partial<IInspireButtonProps>;

export const Secondary = Template.bind({});
Secondary.args = {
    text: 'Button secondary',
    type: 'secondary',
} as Partial<IInspireButtonProps>;

export const Large = Template.bind({});
Large.args = {
    text: 'Button large',
    type: 'large',
} as Partial<IInspireButtonProps>;

export const Small = Template.bind({});
Small.args = {
    text: 'Button small',
    type: 'small',
} as Partial<IInspireButtonProps>;
