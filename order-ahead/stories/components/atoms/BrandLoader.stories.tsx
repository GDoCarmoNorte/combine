import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import BrandLoader from '../../../components/atoms/BrandLoader';

export default {
    title: 'Design system/Atoms/BrandLoader',
    component: BrandLoader,
    decorators: [
        (Story) => <div style={{ height: '100vh', display: 'flex', justifyContent: 'center' }}>{Story()}</div>,
    ],
} as ComponentMeta<typeof BrandLoader>;

const Template: ComponentStory<typeof BrandLoader> = (args) => <BrandLoader {...args} />;

export const Loader = Template.bind({});
