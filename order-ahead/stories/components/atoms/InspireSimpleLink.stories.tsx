import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { InspireSimpleLink, IInspireSimpleLinkProps } from '../../../components/atoms/link/simpleLink';

export default {
    title: 'Design system/Atoms/InspireSimpleLink',
    component: InspireSimpleLink,
} as ComponentMeta<typeof InspireSimpleLink>;

const Template: ComponentStory<typeof InspireSimpleLink> = (args) => <InspireSimpleLink {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    text: 'Simple Link Primary',
    type: 'primary',
} as Partial<IInspireSimpleLinkProps>;

export const Secondary = Template.bind({});
Secondary.args = {
    text: 'Simple Link Secondary',
    type: 'secondary',
} as Partial<IInspireSimpleLinkProps>;
