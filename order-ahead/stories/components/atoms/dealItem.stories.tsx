import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import DealItem, { IDealItemProps } from '../../../components/atoms/dealItem/dealItem';

export default {
    title: 'Design system/Atoms/DealItem',
    component: DealItem,
} as ComponentMeta<typeof DealItem>;

const Template: ComponentStory<typeof DealItem> = (args) => <DealItem {...args} />;

const mockItem = {
    id: 'eaf972b7-f04f-44db-af1b-a67ace13f77c',
    userOfferId: 'eaf972b7-f04f-44db-af1b-a67ace13f77a',
    name: 'Free Cookie',
    type: 'BUY_X_GET_Y_SET_PRICE',
    startDateTime: '2021-06-08T08:04:58.329Z',
    endDateTime: '2021-06-22T08:04:58.329Z',
    terms:
        "Offer not valid with slider purchases. Offer is not transferrable. Offer cannot be combined with any other coupon or offer. Limit one coupon per person, per order, per visit. No cash value. Offer valid at participating U.S. locations only. Offer and participation in Hawaii and Alaska may vary. TM & © 2019 Arby's IP Holder, LLC. All Rights Reserved. ",
    isRedeemableInStoreOnly: false,
    isRedeemableOnlineOnly: false,
    status: 'OPEN',
    termsLanguageCode: 'en',
    description: 'with Purchase of Any Sandwich',
    applicability: {},
};

export const WithoutImage = Template.bind({});
WithoutImage.args = {
    item: mockItem as any,
} as Partial<IDealItemProps>;

export const WithoutImageUnavailable = Template.bind({});
WithoutImageUnavailable.args = {
    item: mockItem as any,
    isUnavailable: true,
} as Partial<IDealItemProps>;
