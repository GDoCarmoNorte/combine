# Inspire web application

This nextjs/reactjs application implements webApp as well as some of the experience API for the PoC

## Prerequisites

-   [NodeJS v12+ & NPM](https://nodejs.org/en/)

-   [JDK or OpenJDK v8+](https://www.oracle.com/java/technologies/javase-downloads.html)

-   [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/)

## Getting started - first time

-   Log into azure by executing the following command:
    ```bash
    az login
    ```
-   Checkout this project by executing the following command:
    ```bash
    git clone https://gitlab.com/inspire1/digital-platform/web/inspire-oa
    ```
-   Connect to VPN
-   Update hosts file w/ mappings. See [this file](https://inspirebrands.atlassian.net/wiki/spaces/DBBP/pages/931037234/arb+dev01) for more info
-   Execute the following commands:

    ```bash
    npm install

    npm run config

    npm run generate

    npm run dev
    ```

-   The application should be running at `http://localhost:3000`

## Build Dependencies

-   `Configuration` - WebApp uses dotenv to configure application build and runtime. There are two levels of configuration - environment and brand. Configuration located under the environment specific folder within /config directory.

-   `OpenApi` - WebApp generates open API (swagger) integration code (definition types and services) based on server-side interfaces (JSON). Open API generator is Java-based tools and required JDK installed on the build machine

-   `Contentful integration` - WebApp generates data models (TypeScript interfaces) using Contentful model types to render them appropriately.

> Make sure that configuration is generated [check .env file in the project root], open API is regenerated [check @generated/Domain* folders] and Contentful definition is up to date [check @generated/contentful.d.ts]

## Build scripts

### Run app

-   `npm install`

    installs the application packages

-   `npm run dev`

    starts the application in development mode, see Next.JS documentation for details

    > General development configuration:
    > `cross-env NODE_ENV=dev BRAND=bww npx next dev`

    > NODE_ENV - environment

    > BRAND - application brand (`bww`: Buffalo wild wings, `arbys`: Arbys, `sdi`: Sonic)

-   `npm run debug`

    starts the application in debug mode

-   `npm start`

    runs the application in production mode, make sure you’ve built the application using `npm run build` command

### Tests

-   `npm test`

    executes unit tests

-   `npm run test:watch`

    watches test files and re-run unit tests on update

-   `npm run test:coverage`

    executes unit tests and generates coverage report

-   refer to existing integration tests for further information & [TEST DOCS](./TESTING.md)

### Linting

-   `npm run lint`

    runs lint check against the application code

-   `npm run lint:fix`

    runs lint check with auto fixes

### Others

-   `npm run config -- -b bww -n bww-qa01 -e qa -t fast -l en-ca`

    generates the application configuration for build and runtime

    -b [--brand] - sets application brand

    -n [--environment-name] - sets target environment (dev, qa)

    -l [--locale] - sets brand locale

    -e [--contentful-env] - sets contentfull environment (useful to set feature environment)

    -t [--environment-type] - enables ISR (incremental server rendering), for CI only, could be [fast] or [slow]

-   `npm run build`

    generates the application production build

-   `npm run build-api`

    generates open API interfaces and services

-   `npm run build-types`

    generates Contentful models definition, make sure you have up to date contentful JSON models using `"npm run contentful-pull"` command

-   `npm run generate`

    aggregates `"npm run build-api"` and `"npm run build-types"` commands

-   `npm run contentful-pull`

    export Contentful model types into JSON files that used later in pipeline to update Contentful environment, make sure that config is generated - `"npm run config"`

-   `npm run contentful-push`

    -d [--contentful-dest] - defines Contentful environment to import models

    import Contentful models into the environment

    only pipeline or Contentful admins could run the command

-   `npm run generate-contentful-theme`

    generates CSS variables from Contentful, make sure that config is generated - [npm run config]

## Docker image

How to build docker image

`docker build --tag arbys-webapp:1.0 .`

To run above image as a container

`docker run --publish 3000:3000 --detach --name arbys-webapp arbys-webapp:1.0`

## To deploy to kubernetes

Create name space if does not exists

`kubectl create namespace webapp`

Deploy and check your application
In a terminal, navigate to where you created bb.yaml and deploy your application to Kubernetes:

`kubectl apply -f deployment.yaml`

you should see output that looks like the following, indicating your Kubernetes objects were created successfully:

`deployment.apps/arbys-webapp-demo unchanged service/arbys-entrypoint created`

Make sure everything worked by listing your deployments:

`kubectl get deployments --namespace webapp`

if all is well, your deployment should be listed as follows:

```
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
arbys-webapp-demo   1/1     1            1           79s
```

Add service

`kubectl apply -f service.yaml`

This indicates all one of the pods you asked for in your YAML are up and running. Do the same check for your services:

`kubectl get services --namespace webapp`

```
NAME               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
arbys-entrypoint   NodePort    10.102.43.161   <none>        3000:30001/TCP   66s
kubernetes         ClusterIP   10.96.0.1       <none>        443/TCP          39m
```

In addition to the default kubernetes service, we see our arbys-entrypoint service, accepting traffic on port 30001/TCP.

Open a browser and visit your bulletin board at localhost:30001

Once satisfied, tear down your application:

`kubectl delete -f service.yaml`
`kubectl delete -f deployment.yaml`

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app).

## NEW RELIC

### Monitoring

-   The app is instrumented using New Relic.
-   Only the following environments are set up: `PROD`, `STG`, `QA`.
-   There should be 1 app per brand and per environment. Examples below for `Arbys` apps:

    -   Arbys Web OA - `PROD` (App ID `911136446`)
    -   Arbys Web OA - `STG` (App ID `911133091`)
    -   Arbys Web OA - `QA` (App ID `911137534`)

### How to create new app

    -   Open new relic one dashboard
    -   select `+ Add more data' > 'New relic browser' > select account # '1220976341'
    -   Choose `Copy/Paste javascript code` deployment method
    -   Enter your app name e.g. `BWW Web OA - QA` and enable
    -   App ID is displayed in right tab when you click on tag in dashboard view of the app

### Synthetics tests

-   `Synthetics` monitors code could be found at: [synthetics](./new-relic/synthetics) folder

### Dashboards

-   Should you need to create -or recreate- the webapp dashboards, the following tool could be used: [create NR dashboard](./lib/createNRDashboard.js)
-   The tool above will create (JSON output) a New Relic dashboard containing 3 tabs/views, one for each environment: "qa", "stg" and "prod".
-   Configuration is taken from existing config files under `/config` folder.

## Generated Files

### Domain Services

`npm run build-api`

Configure API generator in `openapi-generator.json`

## Contentful

`npm run build-types`

### Contentful development flow:

#### Local dev

1. Create content type in contentful
2. Export new type from contentful into local env (JSON) - `npm run ctlf-export`
3. Regenerate contentful.d.ts to add new content type (content types interfaces) - `npm run build-types`
4. Implement the component that renders that content type - see /components/sections/index.tsx
5. Commit both content type json and react component

CI / CD

1. Import content types from JSON to contentful (contentful validates types with existing content)
2. regenerate contentful.d.ts (content types interfaces)
3. build the application

#### Tokens

-   tokens need to be set up in Contentful by:

    -   navigating to Contentful app
    -   selecting the brand space
    -   selecting the `"master"` environment
    -   in header, go to `Settings` -> `API Keys` -> `Content delivery / preview tokens`
    -   hit the button `"Add API key"`
    -   give a name and select the environments it would apply to:
        -   `"master"` for "PROD" token
        -   all other environments for "NONRPROD" token

-   The tokens need to be added to config files per brand and environment:

    CONTENTFUL_PREVIEW_TOKEN=kv:https://ibue1ci01dp-webappci.vault.azure.net/secrets/CONTENTFUL-PREVIEW-[BRAND]-[ENV-TYPE]-TOKEN

    CONTENTFUL_DELIVERY_TOKEN=kv:https://ibue1ci01dp-webappci.vault.azure.net/secrets/CONTENTFUL-DELIVERY-[BRAND]-[ENV-TYPE]-TOKEN

    CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN=kv:https://ibue1ci01dp-webappci.vault.azure.net/secrets/CONTENTFUL-MANAGEMENT-API-ACCESS-[BRAND]-[ENV-TYPE]-TOKEN

    > "CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN" is a personal token

    > NOTE: ENV-TYPE should be either "PROD" or "NONPROD"

-   The tokens need to be added to the Azure CI/CD KV.

## Cloudflare Custom Error Pages

Full instructions for creating a custom error page can be found [here](https://support.cloudflare.com/hc/en-us/articles/200172706).

### Creating the static HTML file

1. Create static HTML page. All CSS must be defined within the HTML itself. There should be no dependencies here.
2. Place static HTML file in respective `{brandName}` folder under `public/brands/` (i.e. "public/brands/arbys/500.html").

### Using collapsify tool

1. Download [collapsify](https://github.com/cloudflare/collapsify) tool recommended by Cloudflare.
2. Test that size of page is within Cloudflare's max limit (1.43 MB), and styles/fonts/images render properly.
3. Copy HTML generated from Developer Tools into your HTML page so that everything is inline, as recommended by Cloudflare.

### Modifying Contentful models

-   See [Confluence](https://inspirebrands.atlassian.net/wiki/spaces/DBBP/pages/1094242404458/Modifying+Contentful+models) for further more details

### Migrations

-   See [Confluence](https://inspirebrands.atlassian.net/wiki/spaces/DBBP/pages/1519681537/Contentful+migration+scripts) for further more details

### Personalization

-   See [Confluence](https://inspirebrands.atlassian.net/wiki/spaces/DBBP/pages/787788072379/Personalization+documentation) for further more details

### Conventions

-   See [CONVENTIONS.md](./CONVENTIONS.md) for further details

---

## Auth0

-   See [auth0 - README.md](./auth0/README.md) for further details
