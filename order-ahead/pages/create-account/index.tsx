import { useEffect } from 'react';
import useNotifications from '../../redux/hooks/useNotifications';
import { useAuth0 } from '@auth0/auth0-react';

export default function Signup(): null {
    const { loginWithRedirect, isAuthenticated } = useAuth0();
    const {
        actions: { enqueueError },
    } = useNotifications();

    const getRedirectUrl = (searchParams: string): string => {
        const params = new URLSearchParams(searchParams);
        return params.get('redirect');
    };

    const redirectPage = (typeof window !== 'undefined' && getRedirectUrl(window.location.search)) || '/';

    useEffect(() => {
        if (!isAuthenticated) {
            try {
                loginWithRedirect({
                    appState: {
                        target: redirectPage,
                    },
                    screen_hint: 'signup',
                });
            } catch ({ message }) {
                location.replace(`${location.origin}/${redirectPage}`);
                enqueueError({ message });
            }
            return;
        }
        location.replace(`${location.origin}/${redirectPage}`);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return null;
}
