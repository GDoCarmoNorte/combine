import React, { useEffect } from 'react';
import { GetStaticProps } from 'next';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import { useRouter } from 'next/router';

import { IAccountHeader } from '../../../@generated/@types/contentful';

import revalidate from '../../../lib/revalidate';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import AccountPageWrapper from '../../../components/organisms/accountPageWrapper';
import { isRewardsOn } from '../../../lib/getFeatureFlags';
import PointsCounter from '../../../components/organisms/pointsCounter/pointsCounter';
import OffersAndCertificates from '../../../components/organisms/offersAndCertificates';
import styles from './rewards.module.css';
import ClaimPoints from '../../../components/organisms/claimPoints/claimPoints';
import { RewardsActivityHistory } from '../../../components/organisms/rewardsActivityHistory/rewardsActivityHistory';
import { useGlobalProps } from '../../../redux/hooks';

export interface IAccountPage {
    accountSections: {
        accountHeader: IAccountHeader;
    };
    isRewardsOn: boolean;
}

function AccountDealsPage(props: IAccountPage): JSX.Element {
    const globalProps = useGlobalProps();

    const { isRewardsOn } = props;

    // Since we can't use "notFound: true" flag with static build
    const router = useRouter();
    useEffect(() => {
        if (!isRewardsOn) {
            router.replace('/404');
        }
    }, [isRewardsOn, router]);

    const accountHeader = props.accountSections?.accountHeader;

    return (
        <AccountPageWrapper
            accountHeader={accountHeader}
            globalProps={globalProps}
            pageWrapperClassName={styles.pageWrapper}
        >
            <PointsCounter />
            <OffersAndCertificates />
            <RewardsActivityHistory />
            <ClaimPoints />
        </AccountPageWrapper>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const accountPage = await ContentfulDelivery(context.preview).getPage('account');
    const accountSections = accountPage.fields?.section || [];

    const accountSectionsProps = accountSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            accountSections: accountSectionsProps,
            isRewardsOn: isRewardsOn(),
        },
        revalidate: revalidate(),
    };
};

export default withAuthenticationRequired(AccountDealsPage);
