import React, { useEffect } from 'react';
import { GetStaticProps } from 'next';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import { useRouter } from 'next/router';

import revalidate from '../../../lib/revalidate';
import { isMyTeamsEnabled } from '../../../lib/getFeatureFlags';
import AccountPageWrapper from '../../../components/organisms/accountPageWrapper';
import { useGlobalProps } from '../../../redux/hooks';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import { IAccountHeader } from '../../../@generated/@types/contentful';
import MyTeams from '../../../components/organisms/myTeams';
import { useConfiguration } from '../../../common/hooks/useConfiguration';
import TeamsView from '../../../components/organisms/myTeams/teamsView';

export interface IMyTeamsPage {
    accountSections: {
        accountHeader: IAccountHeader;
    };
    isMyTeamsEnabled: boolean;
}

function MyTeamsPage(props: IMyTeamsPage): JSX.Element {
    const { configuration } = useConfiguration();
    const globalProps = useGlobalProps();

    // Since we can't use "notFound: true" flag with static build
    const router = useRouter();
    useEffect(() => {
        if (!configuration.isMyTeamsEnabled) {
            router.replace('/404');
        }
    }, [configuration.isMyTeamsEnabled, router]);

    if (!configuration.isMyTeamsEnabled) return null;

    const accountHeader = props.accountSections?.accountHeader;
    const teamId = Array.isArray(router.query.team) ? router.query.team[0] : router.query.team;

    return (
        <AccountPageWrapper accountHeader={accountHeader} globalProps={globalProps}>
            {teamId ? <TeamsView teamId={teamId} /> : <MyTeams />}
        </AccountPageWrapper>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const accountPage = await ContentfulDelivery(context.preview).getPage('account');
    const accountSections = accountPage.fields?.section || [];

    const accountSectionsProps = accountSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            accountSections: accountSectionsProps,
            isMyTeamsEnabled: isMyTeamsEnabled(),
        },
        revalidate: revalidate(),
    };
};

export default withAuthenticationRequired(MyTeamsPage);
