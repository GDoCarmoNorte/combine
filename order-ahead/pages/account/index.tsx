import React, { useEffect, useState, useMemo } from 'react';
import { GetStaticProps } from 'next';
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react';

import revalidate from '../../lib/revalidate';
import { isAccountOn, isCustomerPaymentMethodEnabled } from '../../lib/getFeatureFlags';
import NotificationSettings from '../../components/molecules/notificationSettings/notificationSettings';
import TermsAndConditionsLinks from '../../components/organisms/termsAndConditionsLinks';
import AccountInfo from '../../components/organisms/accountInfo';
import PaymentMethods from '../../components/organisms/paymentMethods/paymentMethods';
import useAccount from '../../redux/hooks/useAccount';
import { initAccountService } from '../../common/services/customerService/account';
import AccountPageWrapper from '../../components/organisms/accountPageWrapper';
import { resetPassword } from '../../common/services/resetPassword';
import useNotifications from '../../redux/hooks/useNotifications';
import { authorizationHeaderBuilder } from '../../common/helpers/accountHelper';
import { ContentfulDelivery } from '../../lib/contentfulDelivery';
import { IAccountHeader } from '../../@generated/@types/contentful';
import { useGlobalProps } from '../../redux/hooks';
import { TErrorCodeModel } from '../../@generated/webExpApi';
import { GtmErrorCategory, GtmErrorEvent } from '../../common/services/gtmService/types';
import { useAppDispatch } from '../../redux/store';
import { GTM_ERROR_EVENT } from '../../common/services/gtmService/constants';

export interface IAccountPage {
    isAccountOn: boolean;
    isCustomerPaymentMethodEnabled: boolean;
    accountSections: {
        accountHeader: IAccountHeader;
    };
}

function AccountPage(props: IAccountPage): JSX.Element {
    const { actions, account, accountInfo } = useAccount();
    const [pendingUpdate, setPendingUpdate] = useState(false);
    const [tooltiopIsOpen, toggleTooltip] = useState(false);
    const globalProps = useGlobalProps();
    const dispatch = useAppDispatch();

    const { user, isAuthenticated, getIdTokenClaims } = useAuth0();
    const {
        actions: { enqueueError, enqueueSuccess },
    } = useNotifications();
    const [idToken, setIdToken] = useState<string>('');

    const onChangePasswordClick = () => {
        resetPassword(account.email)
            .then(() =>
                enqueueSuccess({
                    message: `We have sent you an email to ${account.email} address with a link to change your password.`,
                })
            )
            .catch(({ message }) => enqueueError({ message }));
    };

    useEffect(() => {
        getIdTokenClaims().then((res) => {
            setIdToken(res?.__raw);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isAuthenticated, user]);

    const token = authorizationHeaderBuilder(idToken);

    const { getAccount, updateAccount } = useMemo(() => initAccountService(token), [token]);

    const handleSubmit = async ({ firstName, lastName, phone, birthDate }) => {
        if (!account) return;

        setPendingUpdate(true);
        const unmaskedPhone = phone.replace(/[()]/g, '').replace(' ', '-');
        const ummaskedBirthDate = birthDate.replace('/', '-');

        const updatedAccount = {
            iUpdateCustomerAccountRequestFromClientModel: {
                firstName,
                lastName,
                phones: [{ number: unmaskedPhone, isPreferred: true }],
                birthDate: ummaskedBirthDate,
            },
        };

        try {
            await updateAccount(updatedAccount);

            const updatedAccountInfo = await getAccount();

            actions.setAccount(updatedAccountInfo);
            toggleTooltip(true);
            setTimeout(() => toggleTooltip(false), 1500);
        } catch (error) {
            const message = error?.message;
            if (error?.code === TErrorCodeModel.PhoneNumberAlreadyUsed) {
                const payload = {
                    ErrorCategory: GtmErrorCategory.EDIT_ACCOUNT,
                    ErrorDescription: message,
                } as GtmErrorEvent;
                dispatch({ type: GTM_ERROR_EVENT, payload });
            }
            enqueueError({ message });
        }

        setPendingUpdate(false);
    };

    const updateAccountRequest = async (marketing = []) => {
        if (!account || !accountInfo) return;

        setPendingUpdate(true);

        const { preferences, firstName, lastName, phone } = accountInfo;
        const phoneNumberMask = phone
            ? [
                  {
                      number: phone.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3'),
                      isPreferred: true,
                  },
              ]
            : [];
        const updatedAccount = {
            iUpdateCustomerAccountRequestFromClientModel: {
                firstName,
                lastName,
                phones: phoneNumberMask,
                preferences: {
                    ...preferences,
                    marketing,
                },
            },
        };

        try {
            await updateAccount(updatedAccount);

            const updatedAccountInfo = await getAccount();

            actions.setAccount(updatedAccountInfo);
            if (!marketing.length) {
                const message = 'You are successfully unsubscribed!';
                enqueueSuccess({ message });
            }
        } catch (e) {
            enqueueError({ message: e.message });
        }

        setPendingUpdate(false);
    };

    // Since we can't use "notFound: true" flag with static build
    useEffect(() => {
        if (!props.isAccountOn) {
            window.location.replace('/404');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (!props.isAccountOn || !accountInfo) {
        return <div aria-label="Account Page" />;
    }

    const { preferences, ...accountFormData } = accountInfo;
    const { accountSections, isCustomerPaymentMethodEnabled } = props;
    const accountHeader = accountSections?.accountHeader;
    return (
        <AccountPageWrapper globalProps={globalProps} accountHeader={accountHeader}>
            <AccountInfo
                {...accountFormData}
                onSubmit={handleSubmit}
                onChangePasswordClick={onChangePasswordClick}
                pending={pendingUpdate}
                tooltipIsOpen={tooltiopIsOpen}
            />
            {isCustomerPaymentMethodEnabled && <PaymentMethods customerId={account.idpCustomerId} jwtToken={idToken} />}
            <NotificationSettings onChange={updateAccountRequest} marketing={preferences?.marketing} />
            <TermsAndConditionsLinks />
        </AccountPageWrapper>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const accountPage = await ContentfulDelivery(context.preview).getPage('account');

    const accountSections = accountPage.fields?.section || [];

    const accountSectionsProps = accountSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});
    return {
        props: {
            isAccountOn: isAccountOn(),
            isCustomerPaymentMethodEnabled: isCustomerPaymentMethodEnabled(),
            accountSections: accountSectionsProps,
        },
        revalidate: revalidate(),
    };
};

export default withAuthenticationRequired(AccountPage);
