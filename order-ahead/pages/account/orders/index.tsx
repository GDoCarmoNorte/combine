import React, { useEffect } from 'react';
import { GetStaticProps } from 'next';
import { withAuthenticationRequired } from '@auth0/auth0-react';
import { useRouter } from 'next/router';
import { IAccountHeader } from '../../../@generated/@types/contentful';
import revalidate from '../../../lib/revalidate';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import { isOrdersHistoryOn } from '../../../lib/getFeatureFlags';
import AccountPageWrapper from '../../../components/organisms/accountPageWrapper';
import { OrderHistory } from '../../../components/organisms/orderHistory/orderHistory';
import styles from './orders.module.css';
import { useGlobalProps } from '../../../redux/hooks';

export interface IAccountPage {
    accountSections: {
        accountHeader: IAccountHeader;
    };
    isOrdersHistoryOn: boolean;
}

function AccountOrdersPage(props: IAccountPage): JSX.Element {
    const globalProps = useGlobalProps();

    const { isOrdersHistoryOn } = props;

    // Since we can't use "notFound: true" flag with static build
    const router = useRouter();
    useEffect(() => {
        if (!isOrdersHistoryOn) {
            router.replace('/404');
        }
    }, [isOrdersHistoryOn, router]);

    const accountHeader = props.accountSections?.accountHeader;

    return (
        <AccountPageWrapper
            accountHeader={accountHeader}
            globalProps={globalProps}
            pageWrapperClassName={styles.pageWrapper}
        >
            <OrderHistory productsById={globalProps.productsById} />
        </AccountPageWrapper>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const accountPage = await ContentfulDelivery(context.preview).getPage('account');
    const accountSections = accountPage.fields?.section || [];

    const accountSectionsProps = accountSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            accountSections: accountSectionsProps,
            isOrdersHistoryOn: isOrdersHistoryOn(),
        },
        revalidate: revalidate(),
    };
};

export default withAuthenticationRequired(AccountOrdersPage);
