import React, { useEffect, useMemo } from 'react';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import { getPageTitle } from '../../../common/helpers/getPageTitle';
import getBrandInfo from '../../../lib/brandInfo';
import { useAuth0 } from '@auth0/auth0-react';
import getFeatureFlags, { IFeatureFlags } from '../../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../../redux/hooks/useFeatureFlags';
import { PageContentWrapper } from '../../../components/sections/PageContentWrapper';
import Feedback from '../../../components/clientOnly/deleteAccount/feedback';
import DeletePermissions from '../../../components/clientOnly/deleteAccount/deletePermissions';
import DeleteSuccess from '../../../components/clientOnly/deleteAccount/deleteSuccess';
import DeleteFailure from '../../../components/clientOnly/deleteAccount/deleteFailure';
import styles from '../../../components/clientOnly/deleteAccount/page/page.module.css';
import { useRouter } from 'next/router';
import DeleteAccountLayout from '../../../components/clientOnly/deleteAccount/layout/layout';
import BrandLoader from '../../../components/atoms/BrandLoader';
import DeleteConfirmation from '../../../components/clientOnly/deleteAccount/deleteConfirmation/deleteConfirmation';
import { initAccountService } from '../../../common/services/customerService/account';
import { authorizationHeaderBuilder } from '../../../common/helpers/accountHelper';
import { useLogout } from '../../../common/hooks/useLogout';

export interface IDeleteAccountPageProps {
    featureFlags: IFeatureFlags;
}

export enum TDeleteAccountSteps {
    AREYOUSURE = 'areyousure',
    FEEDBACK = 'feedback',
    DELETE = 'delete',
    SUCCESS = 'success',
    FAILURE = 'failure',
    LOADING = 'loading',
}

const DeleteAccountPage = (props: IDeleteAccountPageProps): JSX.Element => {
    const { brandName } = getBrandInfo();
    const { featureFlags } = props;
    const { isAuthenticated, isLoading: isAuthLoading, loginWithRedirect, getIdTokenClaims } = useAuth0();
    const router = useRouter();
    const { logoutAndClearCookies } = useLogout();

    const [feedback, setFeedback] = React.useState<string>(null);
    const [deleteConfirmation, setDeleteConfirmation] = React.useState(false);
    const [deletePersonalData, setDeletePersonalData] = React.useState(false);
    const [idToken, setIdToken] = React.useState<string>('');

    const [step, setStep] = React.useState(TDeleteAccountSteps['AREYOUSURE']);

    useEffect(() => {
        const query = router.query;
        if (TDeleteAccountSteps['FEEDBACK'] === query.step) {
            router.replace(router.pathname);
            setStep(TDeleteAccountSteps['FEEDBACK']);
        }
    }, [router]);

    useEffect(() => {
        if (step === TDeleteAccountSteps['FEEDBACK'] && !isAuthLoading && !isAuthenticated) {
            setStep(TDeleteAccountSteps['AREYOUSURE']);
        }
    }, [isAuthLoading, isAuthenticated, step]);

    useEffect(() => {
        if (!featureFlags.account) {
            router.replace('/');
        }
    }, [featureFlags, router]);

    useEffect(() => {
        getIdTokenClaims().then((res) => {
            setIdToken(res?.__raw);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isAuthenticated]);
    const token = authorizationHeaderBuilder(idToken);
    const { deleteAccount } = useMemo(() => initAccountService(token), [token]);

    const handleCancel = () => {
        if (window['ReactNativeWebView']) {
            window['ReactNativeWebView'].postMessage(JSON.stringify({ messageCode: 'account_deletion_cancelled' }));
        } else {
            router.replace('/');
        }
    };

    const handleSuccessfulRedirect = () => {
        if (window['ReactNativeWebView']) {
            window['ReactNativeWebView'].postMessage(JSON.stringify({ messageCode: 'account_deletion_success' }));
        } else {
            logoutAndClearCookies();
        }
    };

    const handleSubmit = async () => {
        if (deleteConfirmation && isAuthenticated && token) {
            try {
                setStep(TDeleteAccountSteps['LOADING']);
                await deleteAccount();
                setStep(TDeleteAccountSteps['SUCCESS']);
            } catch (error) {
                setStep(TDeleteAccountSteps['FAILURE']);
            }
        }
    };

    const handleFeedbackContinue = () => {
        setStep(TDeleteAccountSteps['DELETE']);
    };

    const steps = {
        [TDeleteAccountSteps['AREYOUSURE']]: (
            <DeleteConfirmation
                onCancel={handleCancel}
                onDelete={() => {
                    loginWithRedirect({
                        appState: { target: `${window.location.pathname}?step=${TDeleteAccountSteps.FEEDBACK}` },
                        max_age: 0,
                    });
                }}
            />
        ),
        [TDeleteAccountSteps['LOADING']]: (
            <DeleteAccountLayout>
                <div className={styles.loader}>
                    <BrandLoader />
                </div>
            </DeleteAccountLayout>
        ),
        [TDeleteAccountSteps['SUCCESS']]: <DeleteSuccess onFinish={handleSuccessfulRedirect} />,
        [TDeleteAccountSteps['FAILURE']]: <DeleteFailure onRetry={handleSubmit} />,
        [TDeleteAccountSteps['FEEDBACK']]: (
            <Feedback
                onSetFeedback={setFeedback}
                onCancel={handleCancel}
                onContinue={handleFeedbackContinue}
                feedback={feedback}
            />
        ),
        [TDeleteAccountSteps['DELETE']]: (
            <DeletePermissions
                onToggleDeletePersonalData={() => {
                    setDeletePersonalData(!deletePersonalData);
                }}
                onToggleDeleteConfirmation={() => {
                    setDeleteConfirmation(!deleteConfirmation);
                }}
                onCancel={handleCancel}
                onSubmit={handleSubmit}
                deleteConfirmation={deleteConfirmation}
                deletePersonalData={deletePersonalData}
            />
        ),
    };

    const isFeedbackLoading = step === TDeleteAccountSteps['FEEDBACK'] && isAuthLoading;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <Head>
                <title>{getPageTitle(brandName, 'Delete Account')}</title>
            </Head>
            <PageContentWrapper>
                <div className={styles.container}>
                    {isFeedbackLoading ? steps[TDeleteAccountSteps.LOADING] : steps[step]}
                </div>
            </PageContentWrapper>
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async () => {
    const featureFlags = getFeatureFlags();

    return {
        props: {
            featureFlags,
        },
    };
};

export default DeleteAccountPage;
