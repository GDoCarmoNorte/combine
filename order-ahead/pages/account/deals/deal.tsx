import React, { useCallback, useMemo, useState } from 'react';
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react';

import { isValid, format } from '../../../common/helpers/dateTime';
import revalidate from '../../../lib/revalidate';
import { Deal } from '../../../components/clientOnly/deals';
import AccountPageWrapper from '../../../components/organisms/accountPageWrapper';
import { InspireLink } from '../../../components/atoms/link';
import { useBag, useGlobalProps, useRewards } from '../../../redux/hooks';
import { useLocationOrderAheadAvailability } from '../../../common/hooks/useLocationOrderAheadAvailability';
import Loader from '../../../components/atoms/Loader';
import { useDealsService } from '../../../common/hooks/useDealsService';

import useDeal from '../../../common/hooks/useDeal';
import styles from './deal.module.css';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import { IAccountHeader } from '../../../@generated/@types/contentful';

export interface IDealPage {
    accountSections: {
        accountHeader: IAccountHeader;
    };
}

const DealPage = (props: IDealPage): JSX.Element => {
    const globalProps = useGlobalProps();

    const { accountSections } = props;
    const accountHeader = accountSections?.accountHeader;
    const router = useRouter();
    const bag = useBag();
    const { getOfferById, isLoading } = useRewards();

    const { isLocationOrderAheadAvailable, location } = useLocationOrderAheadAvailability();

    const [showQr, setShowQr] = useState(false);
    const [qr, setQr] = useState<string>();
    const [buttonsDisabled, setButtonsDisabled] = useState(false);

    const userOfferId = Array.isArray(router.query.id) ? router.query.id[0] : router.query.id;

    const dealsService = useDealsService();

    const dealIdInBag = bag?.dealId;
    const offer = getOfferById(userOfferId);

    const { isLoading: isAuthLoading } = useAuth0();

    const expiresDate = isValid(new Date(offer?.endDateTime)) ? format(new Date(offer.endDateTime), 'MM/dd') : '';

    const labelText = useMemo(() => {
        if (offer?.isRedeemableOnlineOnly && !offer?.isRedeemableInStoreOnly) {
            return 'online only';
        }
        if (offer?.isRedeemableInStoreOnly && !offer?.isRedeemableOnlineOnly) {
            return 'in store only';
        }
        return '';
    }, [offer]);

    const primaryCtaText = useMemo(() => {
        const isRedeemOnline =
            offer?.isRedeemableOnlineOnly || (!offer?.isRedeemableOnlineOnly && !offer?.isRedeemableInStoreOnly);
        if (dealIdInBag && dealIdInBag === offer?.userOfferId) {
            return 'Continue to check out';
        }
        if (isRedeemOnline && dealIdInBag && dealIdInBag !== offer?.userOfferId) {
            return 'Swap to this deal';
        }
        if (!location || (!isLocationOrderAheadAvailable && !offer?.isRedeemableInStoreOnly)) {
            return 'Select a location';
        }
        if (isRedeemOnline) {
            return 'redeem online';
        }
        if (offer?.isRedeemableInStoreOnly) {
            return 'redeem in store';
        }
        return '';
    }, [location, isLocationOrderAheadAvailable, offer, dealIdInBag]);

    const secondaryCtaText = useMemo(() => {
        if (!offer?.isRedeemableOnlineOnly && !offer?.isRedeemableInStoreOnly && !!location) {
            return 'redeem in store';
        }
        return '';
    }, [location, offer]);

    const handleRedeemInStore = useCallback(async () => {
        const imgSrc = await dealsService.getQRCodeForOfferApi({ userOfferId: offer.userOfferId });
        // remove quotations
        setQr(imgSrc.replace(/['"]+/g, ''));
        setShowQr(true);
        window.scrollTo(0, 0);
    }, [dealsService, offer]);

    const handlePrimaryCtaClick = useCallback(() => {
        setButtonsDisabled(true);
        if (!location || (!isLocationOrderAheadAvailable && !offer?.isRedeemableInStoreOnly)) {
            router.push('/locations');
            return;
        }

        if (offer?.isRedeemableInStoreOnly) {
            handleRedeemInStore();
            return;
        }

        if (dealIdInBag === offer?.userOfferId) {
            bag.actions.toggleIsOpen({ isOpen: true });
            return;
        }

        bag.actions.updateTooltip({
            tooltipIsOpen: true,
            createdWithDeal: !dealIdInBag,
            updatedWithDeal: !!dealIdInBag,
        });
        bag.actions.addDealToBag({ id: offer.userOfferId });
        setButtonsDisabled(false);
    }, [location, router, dealIdInBag, offer, handleRedeemInStore, bag.actions, isLocationOrderAheadAvailable]);

    const handleSecondaryCtaClick = useCallback(() => {
        if (!offer?.isRedeemableOnlineOnly && !offer?.isRedeemableInStoreOnly && !!location) {
            handleRedeemInStore();
            setButtonsDisabled(true);
        }
    }, [offer, location, handleRedeemInStore]);

    const handleBackClick = useCallback(
        (e: React.MouseEvent) => {
            if (showQr) {
                e.preventDefault();
                setShowQr(false);
            }
            setButtonsDisabled(false);
        },
        [showQr]
    );

    const DATA_GTM_ID_MAP = {
        ['redeem in store']: 'inStore-redeem',
        ['redeem online']: 'online-redeem',
    };

    const { getDealImage } = useDeal();
    const imageUrl = getDealImage(offer?.id);

    return (
        <AccountPageWrapper
            globalProps={globalProps}
            containerClassName={styles.containerWrapper}
            pageWrapperClassName={styles.pageWrapper}
            accountHeader={accountHeader}
        >
            <InspireLink type="primary" link="./" text="back" className={styles.backLink} onClick={handleBackClick} />
            {(isLoading || isAuthLoading) && (
                <div className={styles.loaderContainer}>
                    <Loader size={50} />
                </div>
            )}
            {offer && (
                <Deal
                    title={offer.name}
                    label={labelText}
                    expires={`Expires ${expiresDate}`}
                    description={offer.description}
                    primaryCtaText={primaryCtaText}
                    onPrimaryCtaClick={handlePrimaryCtaClick}
                    onSecondaryCtaClick={handleSecondaryCtaClick}
                    disabled={buttonsDisabled}
                    secondaryCtaText={secondaryCtaText}
                    image={imageUrl}
                    qrImage={qr}
                    showQr={showQr}
                    qrDescription="Please show QR code to cashier."
                    terms={offer.terms}
                    type={offer.type}
                    id={offer.userOfferId}
                    gtmId={DATA_GTM_ID_MAP[primaryCtaText] || ''}
                />
            )}
        </AccountPageWrapper>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const accountPage = await ContentfulDelivery(context.preview).getPage('account');
    const accountSections = accountPage.fields?.section || [];
    const accountSectionsProps = accountSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            accountSections: accountSectionsProps,
        },
        revalidate: revalidate(),
    };
};

export default withAuthenticationRequired(DealPage);
