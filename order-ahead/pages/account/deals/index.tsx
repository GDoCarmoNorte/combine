import React, { useState, useEffect } from 'react';
import { GetStaticProps } from 'next';
import { Modal } from '@material-ui/core';
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react';
import { Entry } from 'contentful';

import { IAccountHeader, IMyDealsEmptyFields } from '../../../@generated/@types/contentful';

import revalidate from '../../../lib/revalidate';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';

import Icon from '../../../components/atoms/BrandIcon';
import AccountPageWrapper from '../../../components/organisms/accountPageWrapper';
import MyDealsEmpty from '../../../components/sections/myDealsEmpty';
import { DealsList } from '../../../components/clientOnly/deals';
import { useBag, useGlobalProps } from '../../../redux/hooks';
import useRewards, { isOfferModel } from '../../../redux/hooks/useRewards';
import Loader from '../../../components/atoms/Loader';
import { useLocationOrderAheadAvailability } from '../../../common/hooks/useLocationOrderAheadAvailability';
import styles from './deals.module.css';
import classnames from 'classnames';
import { InspireButton } from '../../../components/atoms/button';
import { isAccountDealsPageOn } from '../../../lib/getFeatureFlags';
import { useRouter } from 'next/router';

export interface IAccountPage {
    sections: {
        myDealsEmpty: Entry<IMyDealsEmptyFields>;
    };
    accountSections: {
        accountHeader: IAccountHeader;
    };
    isDealsPageOn: boolean;
}

function AccountDealsPage(props: IAccountPage): JSX.Element {
    const {
        sections: { myDealsEmpty },
        isDealsPageOn,
    } = props;

    const globalProps = useGlobalProps();

    // Since we can't use "notFound: true" flag with static build
    const router = useRouter();
    useEffect(() => {
        if (!isDealsPageOn) {
            router.replace('/404');
        }
    }, [isDealsPageOn, router]);

    const myDealsEmptyProps = myDealsEmpty?.fields;

    const { offers, isLoading } = useRewards();

    const { isLocationOrderAheadAvailable, location } = useLocationOrderAheadAvailability();

    const { isLoading: isAuthLoading } = useAuth0();
    const regularOffers = offers?.filter(isOfferModel);

    const isAllNoInstoreRedeemable = !offers.some(
        (offer) =>
            isOfferModel(offer) &&
            (offer.isRedeemableInStoreOnly || (!offer.isRedeemableInStoreOnly && !offer.isRedeemableOnlineOnly))
    );

    const isAllNoOnlineRedeemable = !offers.some(
        (offer) =>
            isOfferModel(offer) &&
            (offer.isRedeemableOnlineOnly || (!offer.isRedeemableInStoreOnly && !offer.isRedeemableOnlineOnly))
    );

    const showErrorModal =
        (location && !isLocationOrderAheadAvailable && isAllNoInstoreRedeemable) ||
        (location && isLocationOrderAheadAvailable && isAllNoInstoreRedeemable && isAllNoOnlineRedeemable);

    const [isModalOpened, setIsModalOpened] = useState(showErrorModal);

    const handleCloseModal = () => {
        setIsModalOpened(false);
    };

    const { dealId: dealIdInBag } = useBag();
    const accountHeader = props.accountSections?.accountHeader;
    if (isLoading || isAuthLoading) {
        return (
            <AccountPageWrapper
                globalProps={globalProps}
                containerClassName={styles.emptyContainerWrapper}
                accountHeader={accountHeader}
            >
                <Loader size={50} />
            </AccountPageWrapper>
        );
    }

    if (offers.length === 0) {
        return (
            <AccountPageWrapper
                globalProps={globalProps}
                containerClassName={styles.emptyContainerWrapper}
                accountHeader={accountHeader}
            >
                <MyDealsEmpty fields={myDealsEmptyProps} />
            </AccountPageWrapper>
        );
    }

    return (
        <AccountPageWrapper
            accountHeader={accountHeader}
            globalProps={globalProps}
            containerClassName={styles.containerWrapper}
            pageWrapperClassName={styles.pageWrapper}
            titleTooltip={!location ? 'Select your location to use the deals' : ''}
        >
            <h1 className="t-header-h2">Your Deals</h1>
            {!location && <div className={styles.generalError}>Select a location to use these deals.</div>}
            {dealIdInBag && <div className={styles.generalError}>You can use only one deal at a time.</div>}
            <DealsList offers={regularOffers} />
            <Modal
                open={showErrorModal && isModalOpened}
                onClose={handleCloseModal}
                onClick={(e) => e.stopPropagation()}
                className={styles.modal}
            >
                <div className={styles.modalContainer}>
                    <Icon className={styles.modalIconBan} size="xl" icon="info-ban" variant="colorful" />
                    <Icon className={styles.modalIconClose} size="m" icon="action-close" onClick={handleCloseModal} />
                    <h2 className={classnames(styles.modalTitle, 't-header-h3')}>
                        Deals cannot be redeemed at this location
                    </h2>
                    <div className={styles.modalButtons}>
                        <InspireButton text="View Menu" onClick={() => router.push('/menu')} />
                        <InspireButton text="Select New Location" onClick={() => router.push('/locations')} />
                    </div>
                </div>
            </Modal>
        </AccountPageWrapper>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const page = await ContentfulDelivery(context.preview).getPage('account/deals');
    const isDealsPageOn = isAccountDealsPageOn();

    const sections = page.fields?.section || [];

    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    const accountPage = await ContentfulDelivery(context.preview).getPage('account');
    const accountSections = accountPage.fields?.section || [];

    const accountSectionsProps = accountSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            sections: sectionsProps,
            accountSections: accountSectionsProps,
            isDealsPageOn,
        },
        revalidate: revalidate(),
    };
};

export default withAuthenticationRequired(AccountDealsPage);
