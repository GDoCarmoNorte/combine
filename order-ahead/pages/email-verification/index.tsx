import { useEffect } from 'react';
import { useRouter } from 'next/router';
import useVerifyEmail from '../../common/hooks/useVerifyEmail';

export default function EmailVerification(): null {
    const router = useRouter();
    const cid = router.query?.cid && `${router.query?.cid}`;
    const { verifyEmail } = useVerifyEmail();

    useEffect(() => {
        if (cid) {
            verifyEmail({ cid })
                .then(() => {
                    router.push(
                        {
                            pathname: '/',
                            query: { isEmailVerification: true },
                        },
                        '/'
                    );
                })
                .catch(() => {
                    router.push(
                        {
                            pathname: '/',
                            query: { isEmailVerification: '' },
                        },
                        '/'
                    );
                });
        }
    }, [cid, router, verifyEmail]);

    return null;
}
