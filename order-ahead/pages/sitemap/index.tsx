import React from 'react';
import { Entry } from 'contentful';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import classnames from 'classnames';

import { IPageFields } from '../../@generated/@types/contentful';

import revalidate from '../../lib/revalidate';
import { ContentfulDelivery } from '../../lib/contentfulDelivery';
import { getGlobalContentfulProps } from '../../common/services/globalContentfulProps';
import Footer from '../../components/organisms/footer';
import BaseHeader from '../../components/organisms/header/baseHeader';
import SiteMapCategory from '../../components/sections/SiteMapCategory';

import sectionIndentsStyles from '../../components/sections/sectionIndents.module.css';
import styles from './index.module.css';
import { groupSiteMapData, ISiteMapCategory } from '../../common/helpers/siteMapHelper';
import getBrandInfo from '../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags } from '../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';
import { getPageTitle } from '../../common/helpers/getPageTitle';
import { getPageDescription } from '../../common/helpers/getPageDescription';
import { PageContentWrapper } from '../../components/sections/PageContentWrapper';
import { getCanonicalUrl } from '../../common/helpers/getCanonicalUrl';
import { useGlobalProps } from '../../redux/hooks';

export interface ISiteMapPage {
    page: Entry<IPageFields>;
    siteMap: ISiteMapCategory[];
    featureFlags: IFeatureFlags;
    canonicalPath: string;
}

export default function SiteMapPage(props: ISiteMapPage): JSX.Element {
    const { siteMap, page, featureFlags, canonicalPath } = props;
    const { brandName } = getBrandInfo();
    const globalProps = useGlobalProps();
    const { alertBanners, navigation, footer, userAccountMenu } = globalProps;

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const { metaTitle, hideFooter } = page.fields;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <div>
                <BaseHeader
                    alertBanners={alertBanners}
                    webNavigation={webNavigation}
                    userAccountMenu={userAccountMenu}
                />
                <div className={styles.container}>
                    <Head>
                        <title>{getPageTitle(brandName, metaTitle)}</title>
                        <meta name="description" content={getPageDescription(page)} />
                        <link rel="canonical" href={canonicalPath} />
                    </Head>

                    <PageContentWrapper>
                        {/* @TODO: need to define generic logic to display / hide page title */}
                        <h1 className={classnames(sectionIndentsStyles.wrapper, styles.heading, 't-header-hero')}>
                            SITEMAP
                        </h1>

                        {siteMap.map((category, index) => (
                            <SiteMapCategory key={`SiteMapCategory-${index}-${category.name}`} {...category} />
                        ))}
                    </PageContentWrapper>
                </div>
                {footer && !hideFooter && <Footer footer={footer} />}
            </div>
        </FeatureFlagsContext.Provider>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const page = await ContentfulDelivery(context.preview).getPage('sitemap');
    const globalContentfulProps = await getGlobalContentfulProps(context);
    const menuPage = await ContentfulDelivery(context.preview).getPage('menu');
    const siteMapCategories = await ContentfulDelivery().getSiteMapCategories();

    const siteMap = groupSiteMapData(menuPage, siteMapCategories, globalContentfulProps);
    const featureFlags = getFeatureFlags();
    const canonicalPath = getCanonicalUrl(page, `${process.env.NEXT_PUBLIC_APP_URL}/sitemap/`);

    return {
        props: {
            page,
            siteMap,
            featureFlags,
            canonicalPath,
        },
        revalidate: revalidate(),
    };
};
