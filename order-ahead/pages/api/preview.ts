async function fetchGraphQL(query) {
    return fetch(
        `https://graphql.contentful.com/content/v1/spaces/${process.env.CONTENTFUL_SPACE}/environments/${process.env.CONTENTFUL_ENV}`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${process.env.CONTENTFUL_PREVIEW_TOKEN}`,
            },
            body: JSON.stringify({ query }),
        }
    ).then((response) => response.json());
}

async function getPageById(id) {
    const entry = await fetchGraphQL(
        `query {
            pageCollection(where: { sys: { id: "${id}" }}, preview: true, limit: 2) {
                items {
                    name
                    link {
                        nameInUrl
                    }
                }
            }
        }
        `
    );

    return entry?.data?.pageCollection?.items?.[0];
}

export default async (req, res) => {
    const { secret, id, environmentId } = req.query;

    // in case when preview mode opens in another environment
    if (environmentId !== process.env.CONTENTFUL_ENV) {
        return res.status(401).json({ message: 'Please change environment or preview platform' });
    }

    // This secret should only be known to this API route and the CMS
    if (secret !== process.env.CONTENTFUL_PREVIEW_TOKEN) {
        return res.status(401).json({ message: 'Invalid token' });
    }

    // If the id doesn't exist prevent preview mode from being enabled
    if (!id) {
        return res.status(401).json({ message: 'Invalid id' });
    }

    // Fetch the headless CMS to check if the provided `id` exists
    // getPageById would implement the required fetching logic to the headless CMS
    const page = await getPageById(id);

    if (!page) {
        return res.status(401).json({ message: `Page with id=${id} not exist` });
    }

    // Enable Preview Mode by setting the cookies
    res.setPreviewData({});

    // Redirect to the path from the fetched page
    res.redirect(`/${page.link.nameInUrl ? page.link.nameInUrl : ''}`); // '' in case if nameInUrl = null on Home Page
    ``;
};
