import React from 'react';
import { Entry } from 'contentful';
import { GetStaticPaths, GetStaticProps } from 'next';

import {
    ICaloriesLegal,
    IMenuCategory,
    IProductFields,
    IProductNutritionLinkFields,
} from '../../../@generated/@types/contentful';
import revalidate from '../../../lib/revalidate';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';

import { getGlobalContentfulProps } from '../../../common/services/globalContentfulProps';
import { getProductPagePathByProductId } from '../../../lib/domainProduct';

import getFeatureFlags, { IFeatureFlags } from '../../../lib/getFeatureFlags';

import ProductDetailsContainer from '../../../components/sections/productDetailsContainer';
import { getCanonicalUrl } from '../../../common/helpers/getCanonicalUrl';
import { useGlobalProps } from '../../../redux/hooks';
import { getContentfulProductIdsByFields } from '../../../common/helpers/getContentfulProductIdsByFields';
import { optimizeContentfullData } from '../../../lib/cmsOptimization';

export interface IProductDetailsPageProps {
    product: Entry<IProductFields>;
    currentCategory: IMenuCategory;
    productNutritionLink: IProductNutritionLinkFields | null;
    canonicalPath: string;
    featureFlags: IFeatureFlags;
    isPreviewMode?: boolean;
    caloriesLegal?: ICaloriesLegal;
}

export default function ProductDetailPage(props: IProductDetailsPageProps): JSX.Element {
    const globalProps = useGlobalProps();
    return (
        <ProductDetailsContainer
            globalProps={globalProps}
            product={props.product}
            currentCategory={props.currentCategory}
            productNutritionLink={props.productNutritionLink}
            canonicalPath={props.canonicalPath}
            featureFlags={props.featureFlags}
            isPreviewMode={props.isPreviewMode}
            caloriesLegal={props.caloriesLegal}
        />
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const { menuCategoryUrl, productDetailsPageUrl } = context.params;
    const nameInUrl = Array.isArray(productDetailsPageUrl) ? productDetailsPageUrl[0] : productDetailsPageUrl;
    const menuCategoryNameInUrl = Array.isArray(menuCategoryUrl) ? menuCategoryUrl[0] : menuCategoryUrl;

    const [
        globalContentfulProps,
        product,
        currentCategory,
        productNutritionLinks,
        caloriesLegal = null,
    ] = await Promise.all([
        getGlobalContentfulProps(context),
        ContentfulDelivery(context.preview).getProduct(nameInUrl),
        ContentfulDelivery(context.preview).getMenuCategory(menuCategoryNameInUrl),
        ContentfulDelivery(context.preview).getProductNutritionLink(),
        ContentfulDelivery(context.preview).getCaloriesLegal(),
    ]);

    const productNutritionLink = productNutritionLinks.items[0]?.fields || null;

    const productPagePathByProductId = getProductPagePathByProductId(
        getContentfulProductIdsByFields(product.fields)[0],
        globalContentfulProps.productDetailsPagePaths
    );

    const canonicalPath = getCanonicalUrl(product, `${productPagePathByProductId.canonicalPath.replace(/\/+$/, '')}/`);

    const featureFlags = getFeatureFlags();

    return {
        props: {
            product: optimizeContentfullData(product),
            currentCategory: optimizeContentfullData(currentCategory),
            productNutritionLink,
            canonicalPath,
            isPreviewMode: !!context?.preview,
            featureFlags,
            caloriesLegal,
        },
        revalidate: revalidate(),
    };
};

export const getStaticPaths: GetStaticPaths = async () => {
    const paths = [];

    const pdpPaths = await ContentfulDelivery().getProductDetailsPagePaths();

    pdpPaths.forEach((path) => {
        paths.push({
            params: path,
        });
    });

    return { paths, fallback: false };
};
