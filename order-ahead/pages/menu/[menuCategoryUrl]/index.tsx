import React from 'react';
import Head from 'next/head';
import { Entry, EntryCollection } from 'contentful';
import { GetStaticPaths, GetStaticProps } from 'next';
import {
    ICaloriesLegal,
    IMenuCategory,
    IMenuCategoryFields,
    IMenuSubcategoryFields,
    ITapListMenuCategory,
    ITapListMenuCategoryFields,
} from '../../../@generated/@types/contentful';
import revalidate from '../../../lib/revalidate';
import { ContentfulDelivery } from '../../../lib/contentfulDelivery';
import Footer from '../../../components/organisms/footer';
import BaseHeader from '../../../components/organisms/header/baseHeader';
import getBrandInfo from '../../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags } from '../../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../../redux/hooks/useFeatureFlags';
import { getMenuCategoryPageTitle } from '../../../common/helpers/getPageTitle';
import { getMenuCategoryPageDescription } from '../../../common/helpers/getPageDescription';
import isTapListMenuCategory from '../../../common/helpers/isTapListMenuCategory';
import TapListMenuCategoryPageContent from '../../../components/organisms/menuCategory/tapListMenuCategoryPageContent';
import MenuCategoryPageContent from '../../../components/organisms/menuCategory/menuCategoryPageContent';
import { getCanonicalUrl } from '../../../common/helpers/getCanonicalUrl';
import { useGlobalProps, usePersonalization } from '../../../redux/hooks';
import hasPersonalizedContent from '../../../lib/hasPersonalizedContent';
import { optimizeContentfullData } from '../../../lib/cmsOptimization';

interface IMenuProps {
    menuCategory: IMenuCategory | ITapListMenuCategory;
    menuCategories: IMenuCategory[];
    subcategorys: EntryCollection<IMenuSubcategoryFields>;
    canonicalPath: string;
    featureFlags: IFeatureFlags;
    isPreviewMode?: boolean;
    caloriesLegal?: ICaloriesLegal;
    hasPersonalizedContent: boolean;
}

export default function ProductListingPage(props: IMenuProps): JSX.Element {
    const globalProps = useGlobalProps();

    const { brandName } = getBrandInfo();
    const {
        menuCategory,
        canonicalPath,
        featureFlags,
        isPreviewMode,
        menuCategories,
        subcategorys,
        caloriesLegal,
        hasPersonalizedContent,
    } = props;

    const { alertBanners, navigation, footer, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const { categoryName } = menuCategory.fields;

    const { loading } = usePersonalization();
    const shouldShowLoader = hasPersonalizedContent && loading;
    // MenuCategoryPageContent doesn't work properly if no products at all
    const shouldShowPageContent =
        menuCategory?.fields && 'products' in menuCategory.fields && !!menuCategory.fields.products?.[0];

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <Head>
                <title>{getMenuCategoryPageTitle(brandName, categoryName)}</title>
                <link rel="canonical" href={canonicalPath} />
                <meta name="description" content={getMenuCategoryPageDescription(menuCategory)} />
            </Head>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                userAccountMenu={userAccountMenu}
                isPreviewMode={isPreviewMode}
            />

            <div>
                {isTapListMenuCategory(menuCategory) && (
                    <TapListMenuCategoryPageContent
                        tapListMenuCategory={menuCategory}
                        menuCategories={menuCategories}
                        areSectionsloading={shouldShowLoader}
                    />
                )}
                {!isTapListMenuCategory(menuCategory) && shouldShowPageContent && (
                    <MenuCategoryPageContent
                        menuCategory={menuCategory}
                        menuCategories={menuCategories}
                        subcategorys={subcategorys}
                        caloriesLegal={caloriesLegal}
                        areSectionsloading={shouldShowLoader}
                    />
                )}
            </div>
            {footer && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const { menuCategoryUrl } = context.params;
    const nameInUrl = Array.isArray(menuCategoryUrl) ? menuCategoryUrl[0] : menuCategoryUrl;

    const [menu, subcategorys, caloriesLegal = null] = await Promise.all([
        ContentfulDelivery(context.preview).getMenu(),
        ContentfulDelivery(context.preview).getMenuSubcategories(),
        ContentfulDelivery(context.preview).getCaloriesLegal(),
    ]);

    const optimizedMenu = optimizeContentfullData(menu);
    const optimizedSubcategories = optimizeContentfullData(subcategorys);

    const menuCategories = optimizedMenu.fields.categories.sort((a, b) =>
        a.fields.categoryName.localeCompare(b.fields.categoryName)
    );

    const menuCategory: Entry<IMenuCategoryFields | ITapListMenuCategoryFields> =
        (await ContentfulDelivery(context.preview).getMenuCategory(nameInUrl)) ||
        (await ContentfulDelivery(context.preview).getTapListMenuCategory(nameInUrl));

    const optimizedMenuCategory = optimizeContentfullData(menuCategory);

    const canonicalPath = getCanonicalUrl(
        optimizedMenuCategory,
        `${process.env.NEXT_PUBLIC_APP_URL}/menu/${nameInUrl.replace(/\/+$/, '')}/`
    );

    const featureFlags = getFeatureFlags();

    return {
        props: {
            menuCategory: optimizedMenuCategory,
            menuCategories,
            subcategorys: optimizedSubcategories,
            canonicalPath,
            hasPersonalizedContent: hasPersonalizedContent(optimizedMenuCategory.fields.sections),
            isPreviewMode: !!context?.preview,
            featureFlags,
            caloriesLegal,
        },
        revalidate: revalidate(),
    };
};

export const getStaticPaths: GetStaticPaths = async () => {
    const menuCategories = await ContentfulDelivery().getMenuCategories();
    const tapListMenuCategories = await ContentfulDelivery().getTapListMenuCategories();

    const paths = [...menuCategories.items, ...tapListMenuCategories.items].map((item) => {
        return {
            params: {
                menuCategoryUrl: item.fields.link.fields.nameInUrl,
            },
        };
    });
    return { paths, fallback: false };
};
