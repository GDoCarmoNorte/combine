import React from 'react';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import Footer from '../../components/organisms/footer';
import BaseHeader from '../../components/organisms/header/baseHeader';
import PageSections from '../../components/sections';
import { Entry } from 'contentful';

import revalidate from '../../lib/revalidate';
import { ContentfulDelivery } from '../../lib/contentfulDelivery';

import styles from './index.module.css';
import getBrandInfo from '../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags } from '../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';
import { getPageTitle } from '../../common/helpers/getPageTitle';
import { getCanonicalUrl } from '../../common/helpers/getCanonicalUrl';
import { IPageFields } from '../../@generated/@types/contentful';
import { getPageDescription } from '../../common/helpers/getPageDescription';
import { PageContentWrapper } from '../../components/sections/PageContentWrapper';
import { useGlobalProps, usePersonalization } from '../../redux/hooks';
import hasPersonalizedContent from '../../lib/hasPersonalizedContent';
import BrandLoader from '../../components/atoms/BrandLoader';
import { optimizeContentfullData } from '../../lib/cmsOptimization';

interface IMenuProps {
    page: Entry<IPageFields>;
    hasPersonalizedContent: boolean;
    featureFlags: IFeatureFlags;
    isPreviewMode?: boolean;
    canonicalPath: string;
}
export default function MenuPage(props: IMenuProps): JSX.Element {
    const globalProps = useGlobalProps();
    const { alertBanners, userAccountMenu, footer, navigation } = globalProps;

    const { brandName } = getBrandInfo();
    const { page, featureFlags, isPreviewMode, canonicalPath, hasPersonalizedContent } = props;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const { metaTitle, hideFooter } = page.fields;

    const { loading: isPersonalizationLoading } = usePersonalization();
    const shouldShowLoader = hasPersonalizedContent && isPersonalizationLoading;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                userAccountMenu={userAccountMenu}
                isPreviewMode={isPreviewMode}
            />
            <div className="container">
                <Head>
                    <title>{getPageTitle(brandName, metaTitle)}</title>
                    <meta name="description" content={getPageDescription(page)} />
                    <link rel="canonical" href={canonicalPath} />
                </Head>
                <PageContentWrapper>
                    <h1 className="visually-hidden">{`${brandName} Menu`}</h1>
                    {shouldShowLoader ? (
                        <BrandLoader className={styles.loader} />
                    ) : (
                        <PageSections pageSections={page.fields.section} />
                    )}
                </PageContentWrapper>
            </div>
            {footer && !hideFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
}
export const getStaticProps: GetStaticProps = async (context) => {
    // TODO: Move navigation, menuCategories & alertBanners to _app.tsx once getStaticProps is supported there
    // https://github.com/zeit/next.js/discussions/10949
    const page = await ContentfulDelivery(context.preview).getPage('menu');
    const optimizedPage = optimizeContentfullData(page);
    const featureFlags = getFeatureFlags();
    const canonicalPath = getCanonicalUrl(page, `${process.env.NEXT_PUBLIC_APP_URL}/menu/`);

    return {
        props: {
            page: optimizedPage,
            hasPersonalizedContent: hasPersonalizedContent(optimizedPage.fields.section),
            isPreviewMode: !!context?.preview,
            featureFlags,
            canonicalPath,
        },
        revalidate: revalidate(),
    };
};
