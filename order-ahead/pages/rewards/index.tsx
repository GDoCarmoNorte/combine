import React, { useEffect } from 'react';
import Head from 'next/head';
import { GetStaticProps } from 'next';
import { Entry } from 'contentful';
import { useRouter } from 'next/router';
import { useAuth0 } from '@auth0/auth0-react';

import { IFrequentlyAskedQuestions, IPageFields } from '../../@generated/@types/contentful';

import { ContentfulDelivery } from '../../lib/contentfulDelivery';

import revalidate from '../../lib/revalidate';
import Footer from '../../components/organisms/footer';
import BaseHeader from '../../components/organisms/header/baseHeader';
import PageSections from '../../components/sections';

import getBrandInfo from '../../lib/brandInfo';
import { isRewardsOn } from '../../lib/getFeatureFlags';
import { getPageTitle } from '../../common/helpers/getPageTitle';
import { getPageDescription } from '../../common/helpers/getPageDescription';
import { PageContentWrapper } from '../../components/sections/PageContentWrapper';
import useAccount from '../../redux/hooks/useAccount';
import useLoyalty from '../../redux/hooks/useLoyalty';
import RewardsRoster from '../../components/organisms/rewardsRoster';
import BrandLoader from '../../components/atoms/BrandLoader';

import styles from './index.module.css';
import { getCanonicalUrl } from '../../common/helpers/getCanonicalUrl';
import { useGlobalProps, usePersonalization } from '../../redux/hooks';
import hasPersonalizedContent from '../../lib/hasPersonalizedContent';

interface IRewardsPageProps {
    page: Entry<IPageFields>;
    hasPersonalizedContent: boolean;
    rewardsRosterSections: {
        frequentlyAskedQuestions: IFrequentlyAskedQuestions;
    };
    isRewardsOn: boolean;
}

const Rewards = (props: IRewardsPageProps): JSX.Element => {
    const globalProps = useGlobalProps();

    const { isRewardsOn, page, rewardsRosterSections, hasPersonalizedContent } = props;
    const sections = page.fields?.section || [];
    const { brandName } = getBrandInfo();
    const { accountInfo } = useAccount();
    const { user, isLoading } = useAuth0();
    const { loyalty, isLoading: isLoadingLoyality, error } = useLoyalty();

    const router = useRouter();
    const canonicalPath = getCanonicalUrl(page, `${process.env.NEXT_PUBLIC_APP_URL}${router.asPath}`);

    const { footer, alertBanners, navigation, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const faq = rewardsRosterSections?.frequentlyAskedQuestions;

    const { metaTitle, hideFooter } = props.page.fields;

    // Since we can't use "notFound: true" flag with static build
    useEffect(() => {
        if (!isRewardsOn) {
            router.replace('/404');
        }
    }, [isRewardsOn, router]);

    const { loading } = usePersonalization();
    const isPersonalizationLoading = hasPersonalizedContent && loading;

    if (!props.isRewardsOn) {
        return <div aria-label="Rewards Page" />;
    }

    const isAccountLoading = isLoading || (user && !accountInfo);

    const content = accountInfo ? (
        <RewardsRoster faq={faq} points={loyalty.pointsBalance} isLoading={isLoadingLoyality} isError={!!error} />
    ) : (
        <PageContentWrapper className="container" ariaLabel="Rewards Page">
            <h1 className="visually-hidden">{`${brandName} Rewards`}</h1>
            {sections.length > 0 && <PageSections pageSections={sections} className={styles.mainBanner} />}
        </PageContentWrapper>
    );

    return (
        <>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <Head>
                <title>{getPageTitle(brandName, metaTitle)}</title>
                <meta name="description" content={getPageDescription(page)} />
                <link rel="canonical" href={canonicalPath} />
            </Head>
            {isAccountLoading || isPersonalizationLoading ? <BrandLoader className={styles.loader} /> : content}
            {footer && !hideFooter && <Footer footer={footer} />}
        </>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const page = await ContentfulDelivery(context.preview).getPage('rewards');

    const rewardsRosterPage = await ContentfulDelivery(context.preview).getPage('rewardsroster');
    const rewardsRosterSections = rewardsRosterPage.fields?.section || [];
    const rewardsRosterProps = rewardsRosterSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            page,
            hasPersonalizedContent: hasPersonalizedContent(page.fields.section),
            rewardsRosterSections: rewardsRosterProps,
            isRewardsOn: isRewardsOn(),
        },
        revalidate: revalidate(),
    };
};

export default Rewards;
