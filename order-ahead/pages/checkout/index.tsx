import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import { Entry } from 'contentful';
import { useAuth0 } from '@auth0/auth0-react';

import { TServiceTypeModel } from '../../@generated/webExpApi';

import revalidate from '../../lib/revalidate';

import Checkout from '../../components/organisms/checkout/checkout';
import Loader from '../../components/atoms/Loader';

import useTallyOrder from '../../redux/hooks/useTallyOrder';
import { actions as tallyActions } from '../../redux/tallyOrder';
import { useAccount, useBag, useDomainMenu, useOrderLocation, useGlobalProps } from '../../redux/hooks';
import {
    areAllProductsAvailable,
    getAvailableEntries,
    getTallyBagEntries,
    getTallyBagProductIds,
} from '../../common/helpers/bagHelper';
import { useDomainProducts } from '../../redux/hooks/domainMenu';

import { resolveOpeningHours } from '../../lib/locations';
import { ContentfulDelivery } from '../../lib/contentfulDelivery';
import {
    ISecondaryBannerFields,
    IPickupInstructionsFields,
    ICheckoutHeader,
    ICheckoutLegalFields,
} from '../../@generated/@types/contentful';

import getFeatureFlags, { IFeatureFlags, isAccountOn, locationTimeSlotsEnabled } from '../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';

import styles from './index.module.css';
import { enhanceDaysWithLabel, enhanceTimesWithLabel } from '../../common/helpers/checkoutHelpers';

import { GTM_CHECKOUT_PICK_UP_TIME_SELECTION } from '../../common/services/gtmService/constants';
import { useLocationTimeZone } from '../../common/hooks/useLocationTimeZone';

import { useUnavailableCategories } from '../../common/hooks/useUnavailableCategories';

interface ICheckoutPageProps {
    confirmationSections: {
        pickupInstructions: Entry<IPickupInstructionsFields>;
        secondaryBanner: Entry<ISecondaryBannerFields>;
    };
    checkoutSections: {
        checkoutHeader: ICheckoutHeader;
    };
    checkoutLegal?: ICheckoutLegalFields;
    isAccountOn: boolean;
    featureFlags: IFeatureFlags;
}

const CheckoutPage = (props: ICheckoutPageProps): JSX.Element => {
    const { featureFlags, isAccountOn } = props;
    const dispatch = useDispatch();
    const globalProps = useGlobalProps();

    const router = useRouter();
    const { method, currentLocation: location, isCurrentLocationOAAvailable } = useOrderLocation();
    const resolvedOpeningHours = location && resolveOpeningHours(location, TServiceTypeModel.Pickup);
    const isOrderAheadAvailable = location && isCurrentLocationOAAvailable;

    const { bagEntries, orderTime, orderTimeType, pickupTimeIsValid, dealId, actions: bagActions } = useBag();
    const { loading: isMenuLoading } = useDomainMenu();
    const { tallyOrder, isLoading: isTallyLoading, submitTallyOrder } = useTallyOrder();
    const { account } = useAccount();
    const idpCustomerId = account?.idpCustomerId;
    const { isLoading: isAuthLoading, isAuthenticated } = useAuth0();
    const isGettingUserInfoInProgress = isAccountOn && (isAuthLoading || (isAuthenticated && !idpCustomerId));

    const bagProductIds = getTallyBagProductIds(bagEntries);
    const domainProducts = useDomainProducts(bagProductIds);
    const locationTimeZone = useLocationTimeZone();
    const unavailableCategories = useUnavailableCategories();

    const isLoading = !tallyOrder || (isMenuLoading && isTallyLoading);

    const redirect = () => router.replace('/');

    const pickupInstructions = props.confirmationSections?.pickupInstructions?.fields?.instractions || [];

    const instructions = pickupInstructions.map((item) => item?.fields.instruction);

    const checkoutHeader = props.checkoutSections?.checkoutHeader;

    const checkoutLegal = props.checkoutLegal;

    useEffect(() => {
        if (!bagEntries.length) {
            redirect();
        }
        if (resolvedOpeningHours && !locationTimeSlotsEnabled()) {
            bagActions.initPickupTimeValues({
                openedTimeRanges: resolvedOpeningHours.openedTimeRanges,
                timezone: resolvedOpeningHours.storeTimezone,
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!resolvedOpeningHours) return;
    }, [resolvedOpeningHours]);

    useEffect(() => {
        if (!location) {
            redirect();
            return;
        }

        if (tallyOrder || isMenuLoading || isGettingUserInfoInProgress) {
            return;
        }

        const availableBagEntries = getAvailableEntries(bagEntries, domainProducts, unavailableCategories);
        const allProductsAreAvailable = areAllProductsAvailable(bagEntries, domainProducts, unavailableCategories);

        if (availableBagEntries.length === 0) {
            redirect();
            return;
        }

        if (!isOrderAheadAvailable || !allProductsAreAvailable) {
            redirect().then(() => {
                bagActions.toggleIsOpen({ isOpen: true });
            });
        }

        const pickupTimeParam = orderTimeType !== 'asap' && pickupTimeIsValid && new Date(orderTime);

        submitTallyOrder(
            getTallyBagEntries(availableBagEntries, domainProducts, location),
            pickupTimeParam,
            dealId,
            idpCustomerId
        ).then((action) => {
            if (action.type !== tallyActions.fulfilled.type) {
                redirect();
            }
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isMenuLoading, isGettingUserInfoInProgress]);

    const handleBagChange = () => {
        const availableBagEntries = getAvailableEntries(bagEntries, domainProducts, unavailableCategories);
        const pickupTimeParam = orderTimeType !== 'asap' && pickupTimeIsValid && new Date(orderTime);
        bagActions.clearLastRemovedLineItemId();

        if (!availableBagEntries.length) {
            redirect();
            return;
        }

        submitTallyOrder(
            getTallyBagEntries(availableBagEntries, domainProducts, location),
            pickupTimeParam,
            dealId,
            idpCustomerId
        ).then((action) => {
            action.type !== tallyActions.fulfilled.type && redirect();
        });
    };

    const handleOrderTimeChange = (payload: { time?: string; asap?: boolean }) => {
        const availableBagEntries = getAvailableEntries(bagEntries, domainProducts);
        const currentTime = new Date().toISOString();
        const time = payload.time ? payload.time : currentTime;
        const dayLabel = enhanceDaysWithLabel([time], locationTimeZone);
        const dayInfo = dayLabel && dayLabel.length > 0 ? dayLabel[0].label : '';
        if (payload.asap) {
            dispatch({
                type: GTM_CHECKOUT_PICK_UP_TIME_SELECTION,
                payload: { pickupDate: dayInfo, pickupTime: 'ASAP', handoffType: method },
            });
            return submitTallyOrder(
                getTallyBagEntries(availableBagEntries, domainProducts, location),
                null,
                dealId,
                idpCustomerId
            );
        } else {
            const parsedPickupTime = new Date(payload.time);
            const timeLabel = enhanceTimesWithLabel([payload.time], locationTimeZone);
            dispatch({
                type: GTM_CHECKOUT_PICK_UP_TIME_SELECTION,
                payload: { pickupDate: dayInfo, pickupTime: timeLabel[0].label, handoffType: method },
            });
            return submitTallyOrder(
                getTallyBagEntries(availableBagEntries, domainProducts, location, parsedPickupTime),
                parsedPickupTime,
                dealId,
                idpCustomerId
            );
        }
    };

    const checkoutPage = isLoading ? (
        <div className={styles.loaderContainer}>
            <Loader />
        </div>
    ) : (
        <Checkout
            {...globalProps}
            handleOrderTimeChange={handleOrderTimeChange}
            tallyLoading={isTallyLoading}
            pickupInstructions={instructions}
            checkoutHeader={checkoutHeader}
            checkoutLegal={checkoutLegal}
            onBagChange={handleBagChange}
        />
    );

    return <FeatureFlagsContext.Provider value={{ featureFlags }}>{checkoutPage}</FeatureFlagsContext.Provider>;
};

export const getStaticProps: GetStaticProps = async (context) => {
    const confirmationPage = await ContentfulDelivery(context.preview).getPage('confirmation');
    const confirmationSections = confirmationPage.fields?.section || [];
    const confirmationSectionsProps = confirmationSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    const checkoutPage = await ContentfulDelivery(context.preview).getPage('checkout');
    const checkoutSections = checkoutPage.fields?.section || [];
    const checkoutSectionsProps = checkoutSections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});
    const checkoutLegal = (await ContentfulDelivery(context.preview).getCheckoutLegal())?.fields || null;

    const featureFlags = getFeatureFlags();

    return {
        props: {
            confirmationSections: confirmationSectionsProps,
            checkoutSections: checkoutSectionsProps,
            isAccountOn: isAccountOn(),
            featureFlags,
            checkoutLegal,
        },
        revalidate: revalidate(),
    };
};

export default CheckoutPage;
