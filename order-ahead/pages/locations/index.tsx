import React from 'react';
import Head from 'next/head';
import { GetStaticProps } from 'next';

import { ContentfulDelivery } from '../../lib/contentfulDelivery';

import revalidate from '../../lib/revalidate';
import Footer from '../../components/organisms/footer';
import BaseHeader from '../../components/organisms/header/baseHeader';
import { ILocationNotFoundFields, IPageFields, ILocationFailedFields } from '../../@generated/@types/contentful';
import { Entry } from 'contentful';

import styles from './locations.module.css';
import getBrandInfo from '../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags } from '../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';
import { getPageTitle } from '../../common/helpers/getPageTitle';
import { getPageDescription } from '../../common/helpers/getPageDescription';
import { useRouter } from 'next/router';
import {
    LocationsPageContentPickupFlow,
    LocationsPageContentPickupAndDeliveryFlow,
} from '../../components/organisms/locationsPageContent';
import { useConfiguration, useGlobalProps } from '../../redux/hooks';
import { getCanonicalUrl } from '../../common/helpers/getCanonicalUrl';

interface ILocationPageProps {
    page: Entry<IPageFields>;
    sections: {
        locationNotFound: Entry<ILocationNotFoundFields>;
        locationFailed: Entry<ILocationFailedFields>;
    };
    featureFlags: IFeatureFlags;
}

const Locations = (props: ILocationPageProps): JSX.Element => {
    const globalProps = useGlobalProps();

    const { page, featureFlags } = props;
    const { brandName } = getBrandInfo();
    const {
        configuration: { isDeliveryEnabled },
    } = useConfiguration();

    const router = useRouter();
    const canonicalPath = getCanonicalUrl(page, `${process.env.NEXT_PUBLIC_APP_URL}${router.asPath}`);

    const { footer, alertBanners, navigation, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const { metaTitle, hideFooter } = props.page.fields;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <div className={styles.container}>
                <Head>
                    <title>{getPageTitle(brandName, metaTitle)}</title>
                    <meta name="description" content={getPageDescription(page)} />
                    <link rel="canonical" href={canonicalPath} />
                </Head>
                {isDeliveryEnabled ? (
                    <LocationsPageContentPickupAndDeliveryFlow sections={props.sections} />
                ) : (
                    <LocationsPageContentPickupFlow sections={props.sections} />
                )}
            </div>
            {footer && !hideFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const page = await ContentfulDelivery(context.preview).getPage('locations');

    const sections = page.fields?.section || [];

    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    const featureFlags = getFeatureFlags();

    return {
        props: {
            page,
            sections: sectionsProps,
            featureFlags,
        },
        revalidate: revalidate(),
    };
};

export default Locations;
