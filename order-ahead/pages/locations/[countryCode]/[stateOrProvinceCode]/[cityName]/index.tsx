import React, { useState, useEffect, useMemo } from 'react';
import Head from 'next/head';
import { GetStaticProps, GetStaticPaths } from 'next';

import { ContentfulDelivery } from '../../../../../lib/contentfulDelivery';

import revalidate from '../../../../../lib/revalidate';
import Footer from '../../../../../components/organisms/footer';
import BaseHeader from '../../../../../components/organisms/header/baseHeader';
import { ILocationNotFoundFields } from '../../../../../@generated/@types/contentful';
import { Entry } from 'contentful';

import styles from './index.module.css';
import getFeatureFlags, { IFeatureFlags, isLocationPagesOn } from '../../../../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../../../../redux/hooks/useFeatureFlags';
import { getCityLocationsPageTitle } from '../../../../../common/helpers/getPageTitle';
import { getCityPageDescription } from '../../../../../common/helpers/getPageDescription';
import { useRouter } from 'next/router';
import { ILocationUrlsResponseModel, ILocationsByCityModel } from '../../../../../@generated/webExpApi';
import BrandLoader from '../../../../../components/atoms/BrandLoader';
import useLocationList from '../../../../../common/hooks/useLocationList';
import NotFoundScreen from '../../../../../components/organisms/locationsPageContent/pickupSearchResults/notFoundScreen';
import CityLocationsDetailsPageLayout from '../../../../../components/organisms/locations/cityLocationsPageLayout';
import useNotifications from '../../../../../redux/hooks/useNotifications';
import { IPageSections } from '../../../../../components/sections';
import { getCanonicalUrl } from '../../../../../common/helpers/getCanonicalUrl';
import { useGlobalProps } from '../../../../../redux/hooks';

interface IStateLocationPageProps {
    featureFlags: IFeatureFlags;
    sections?: {
        locationNotFound: Entry<ILocationNotFoundFields>;
    };
    pageSections?: IPageSections;
    isPageOn: boolean;
    metaTitle?: string;
    metaDescription?: string;
    showFooter?: boolean;
    countryCode: string;
    stateOrProvinceCode: string;
    cityName: string;
    address: string;
    storeId: string;
    canonicalPath: string;
}

const GetLocationUrls = async (): Promise<ILocationUrlsResponseModel> => {
    return await useLocationList().getLocationUrlsList();
};

const CityLocationsPage = (props: IStateLocationPageProps): JSX.Element => {
    const globalProps = useGlobalProps();

    const {
        featureFlags,
        countryCode,
        stateOrProvinceCode,
        cityName,
        isPageOn,
        pageSections,
        metaTitle,
        showFooter,
        metaDescription,
        canonicalPath,
    } = props;

    const router = useRouter();
    const [cityLocationsLoaded, setCityLocationsLoaded] = useState(false);
    const [cityLocations, setCityLocations] = useState(null);
    const [breadcrumbsData, setBreadcrumbsData] = useState(null);
    const locationList = useLocationList();
    const {
        actions: { enqueueError },
    } = useNotifications();

    useEffect(() => {
        if (!isPageOn) {
            router.replace('/404');
        }

        isPageOn &&
            (async () => {
                try {
                    const result = await locationList.getLocationList(
                        countryCode.toUpperCase(),
                        stateOrProvinceCode.toUpperCase()
                    );
                    const cityLocationsResponse = result?.stateOrProvince?.cities?.find(
                        (c) => c.displayName === cityName
                    );
                    const stateOrProvince = result?.stateOrProvince?.name;

                    cityLocationsResponse && setCityLocations(cityLocationsResponse as ILocationsByCityModel);
                    setBreadcrumbsData({ stateOrProvince, stateOrProvinceCode, countryCode });
                } catch (e) {
                    enqueueError({
                        title: 'Location details were not found',
                        message: 'Please try again later',
                    });
                } finally {
                    setCityLocationsLoaded(true);
                }
            })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const pageMetaTitle = useMemo(
        () => getCityLocationsPageTitle(metaTitle, breadcrumbsData?.stateOrProvince, cityLocations?.name),
        [breadcrumbsData, cityLocations, metaTitle]
    );

    const pageDescription = useMemo(
        () => getCityPageDescription(metaDescription, stateOrProvinceCode.toUpperCase(), cityLocations?.name),
        [cityLocations, metaDescription, stateOrProvinceCode]
    );

    if (!isPageOn) {
        return <div aria-label="City locations Page" />;
    }

    const { footer, alertBanners, navigation, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const { header, body, icon } = props.sections.locationNotFound.fields;

    const isLoading = !cityLocationsLoaded;
    const isLocationsListAvailable = !!cityLocations;
    const isBreadcrumbsDataAvailable = !!breadcrumbsData;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <div className={styles.container}>
                <Head>
                    <title>{pageMetaTitle}</title>
                    <meta name="description" content={pageDescription} />
                    <link rel="canonical" href={canonicalPath} />
                </Head>
                {!isLoading && !isLocationsListAvailable && <NotFoundScreen header={header} body={body} icon={icon} />}
                {isLoading && <BrandLoader className={styles.loader} />}
                {isLocationsListAvailable && isBreadcrumbsDataAvailable && (
                    <CityLocationsDetailsPageLayout
                        cityLocations={cityLocations}
                        sections={pageSections}
                        breadcrumbsData={breadcrumbsData}
                        pageMetaTitle={pageMetaTitle}
                    />
                )}
            </div>
            {footer && showFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const { countryCode, stateOrProvinceCode, cityName } = context.params;
    const featureFlags = getFeatureFlags();
    const isPageOn = isLocationPagesOn();
    const defaultCanonical = `${process.env.NEXT_PUBLIC_APP_URL}/locations/${countryCode}/${stateOrProvinceCode}/${cityName}/`;

    const props = {
        featureFlags,
        countryCode,
        stateOrProvinceCode,
        cityName,
        isPageOn,
        canonicalPath: defaultCanonical,
    };
    if (!isPageOn) {
        return {
            props,
            revalidate: revalidate(),
        };
    }

    const page = await ContentfulDelivery(context.preview).getPage('citylocations');
    const sections = page.fields?.section || [];
    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});
    const canonicalPath = getCanonicalUrl(page, defaultCanonical);

    return {
        props: {
            featureFlags,
            countryCode,
            stateOrProvinceCode,
            cityName,
            isPageOn,
            metaTitle: page.fields?.metaTitle,
            metaDescription: page.fields?.metaDescription,
            showFooter: !page.fields?.hideFooter,
            sections: sectionsProps,
            pageSections: sections,
            canonicalPath,
        },
    };
};

export const getStaticPaths: GetStaticPaths = async () => {
    let result;
    let paths = [];
    const isPageOn = isLocationPagesOn();
    if (!isPageOn) {
        return { paths, fallback: false };
    }
    try {
        result = await GetLocationUrls();
    } catch (e) {
        return { paths, fallback: false };
    }
    const urlsResponse = result && (result as ILocationUrlsResponseModel);
    paths =
        urlsResponse?.urls &&
        urlsResponse.urls.reduce((array, urlParts) => {
            if (
                urlParts.countryCode &&
                urlParts.stateOrProvinceCode &&
                urlParts.cityName &&
                urlParts.address &&
                urlParts.storeId
            ) {
                array.push({
                    params: urlParts,
                });
            }
            return array;
        }, []);

    return { paths, fallback: false };
};

export default CityLocationsPage;
