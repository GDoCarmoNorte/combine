import React, { useState, useEffect, useMemo } from 'react';
import Head from 'next/head';
import { GetStaticProps, GetStaticPaths } from 'next';

import { ContentfulDelivery } from '../../../../../../../lib/contentfulDelivery';

import revalidate from '../../../../../../../lib/revalidate';
import Footer from '../../../../../../../components/organisms/footer';
import BaseHeader from '../../../../../../../components/organisms/header/baseHeader';
import { ILocationNewsSection, ILocationNotFoundFields } from '../../../../../../../@generated/@types/contentful';
import { Entry } from 'contentful';

import styles from './index.module.css';
import getFeatureFlags, { IFeatureFlags, isLocationPagesOn } from '../../../../../../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../../../../../../redux/hooks/useFeatureFlags';
import { getLocationDetailsPageTitle } from '../../../../../../../common/helpers/getPageTitle';
import { getPageDescriptionByMetaInfo } from '../../../../../../../common/helpers/getPageDescription';
import { useRouter } from 'next/router';
import {
    ILocationByStateOrProvinceDetailsModel,
    ILocationUrlsResponseModel,
    IServiceTypeModel,
} from '../../../../../../../@generated/webExpApi';
import BrandLoader from '../../../../../../../components/atoms/BrandLoader';
import useLocationList from '../../../../../../../common/hooks/useLocationList';
import NotFoundScreen from '../../../../../../../components/organisms/locationsPageContent/pickupSearchResults/notFoundScreen';
import LocationDetailsPageLayout from '../../../../../../../components/organisms/locations/locationDetailsPageLayout';
import useNotifications from '../../../../../../../redux/hooks/useNotifications';
import { IPageSections } from '../../../../../../../components/sections';
import { EXPRESS_LOCATION_TYPE, GO_LOCATION_TYPE } from '../../../../../../../common/constants/location';
import { getLocationDescription } from '../../../../../../../common/helpers/locationHelper';
import { useGlobalProps, useLdp, usePersonalization } from '../../../../../../../redux/hooks';
import hasPersonalizedContent from '../../../../../../../lib/hasPersonalizedContent';

interface IStateLocationPageProps {
    featureFlags: IFeatureFlags;
    sections?: {
        locationNotFound: Entry<ILocationNotFoundFields>;
    };
    pageSections?: IPageSections;
    hasPersonalizedContent: boolean;
    isPageOn: boolean;
    metaTitle?: string;
    metaDescription?: string;
    showFooter?: boolean;
    countryCode: string;
    stateOrProvinceCode: string;
    cityName: string;
    address: string;
    storeId: string;
}

const GetLocationUrls = async (): Promise<ILocationUrlsResponseModel> => {
    return await useLocationList().getLocationUrlsList();
};

const LocationDetailsPage = (props: IStateLocationPageProps): JSX.Element => {
    const {
        featureFlags,
        countryCode,
        stateOrProvinceCode,
        cityName,
        storeId,
        isPageOn,
        pageSections,
        showFooter,
        metaDescription,
        hasPersonalizedContent,
    } = props;

    const globalProps = useGlobalProps();

    const router = useRouter();
    const [areLocationDetailsLoaded, setLocationDetailsLoaded] = useState(false);
    const [locationDetails, setLocationDetails] = useState(null);
    const [locationDescription, setLocationDescription] = useState(null);
    const locationList = useLocationList();
    const {
        actions: { enqueueError },
    } = useNotifications();
    const ldp = useLdp();

    useEffect(() => {
        if (!isPageOn) {
            router.replace('/404');
        }

        isPageOn &&
            (async () => {
                try {
                    ldp.actions.resetLocationDetails();

                    const result = await locationList.getLocationList(
                        countryCode.toUpperCase(),
                        stateOrProvinceCode.toUpperCase()
                    );
                    const locationDetailsResponse = result?.stateOrProvince?.cities
                        ?.find((c) => c.displayName === cityName)
                        ?.locations?.find((l) => l.id === storeId);

                    if (locationDetailsResponse) {
                        setLocationDetails(locationDetailsResponse as ILocationByStateOrProvinceDetailsModel);
                        ldp.actions.putLocationDetails(locationDetailsResponse);
                    }

                    setLocationDescription(
                        getLocationDescription(locationDetailsResponse, result.locationDescriptionMapping)
                    );
                } catch (e) {
                    enqueueError({
                        title: 'Location details were not found',
                        message: 'Please try again later',
                    });
                } finally {
                    setLocationDetailsLoaded(true);
                }
            })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const pageDescription = useMemo(() => getPageDescriptionByMetaInfo(metaDescription), [metaDescription]);
    const pageMetaTitle = useMemo(() => getLocationDetailsPageTitle(locationDetails?.displayName), [locationDetails]);

    const { loading: isPersonalizationLoading } = usePersonalization();

    if (!isPageOn) {
        return <div aria-label="Location details Page" />;
    }

    const filterApplicableSections = (sections: IPageSections): IPageSections => {
        if (!sections) {
            return sections;
        }
        const isGoLocation = locationDetails.services?.some((x) => x === IServiceTypeModel.GoLocation);
        const isExpressLocation = locationDetails.services?.some((x) => x === IServiceTypeModel.ExpressLocation);
        return sections.filter((s) => {
            if (s.sys.contentType.sys.id !== 'locationNewsSection' && !isGoLocation && !isExpressLocation) {
                return true;
            }

            const locationNewsSection = s as ILocationNewsSection;
            if (!locationNewsSection?.fields?.type) {
                return true;
            }

            return (
                (isGoLocation && locationNewsSection.fields.type.toUpperCase() === GO_LOCATION_TYPE) ||
                (isExpressLocation && locationNewsSection.fields.type.toUpperCase() === EXPRESS_LOCATION_TYPE)
            );
        });
    };

    const canonicalPath = locationDetails && `${process.env.NEXT_PUBLIC_APP_URL}/locations/${locationDetails.url}`;

    const { footer, alertBanners, navigation, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const { header, body, icon } = props.sections.locationNotFound.fields;

    const isLoading = !areLocationDetailsLoaded || (hasPersonalizedContent && isPersonalizationLoading);
    const isLocationsListAvailable = !!locationDetails;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <div className={styles.container}>
                <Head>
                    <title>{pageMetaTitle}</title>
                    <meta name="description" content={pageDescription} />
                    <link rel="canonical" href={canonicalPath} />
                </Head>
                {!isLoading && !isLocationsListAvailable && <NotFoundScreen header={header} body={body} icon={icon} />}
                {isLoading && <BrandLoader className={styles.loader} />}
                {!isLoading && isLocationsListAvailable && (
                    <LocationDetailsPageLayout
                        locationDetails={locationDetails}
                        sections={filterApplicableSections(pageSections)}
                        description={locationDescription}
                    />
                )}
            </div>
            {footer && showFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const { countryCode, stateOrProvinceCode, cityName, storeId } = context.params;
    const storeIdParts = (storeId as string)?.split('-');
    const storeIdParsed = (storeIdParts && storeIdParts[storeIdParts.length - 1]) || storeId;
    const featureFlags = getFeatureFlags();
    const isPageOn = isLocationPagesOn();
    const props = {
        featureFlags,
        countryCode,
        stateOrProvinceCode,
        cityName,
        storeId: storeIdParsed,
        isPageOn,
    };
    if (!isPageOn) {
        return {
            props,
            revalidate: revalidate(),
        };
    }

    const page = await ContentfulDelivery(context.preview).getPage('locationdetails');
    const sections = page.fields?.section || [];
    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    return {
        props: {
            featureFlags,
            countryCode,
            stateOrProvinceCode,
            cityName,
            storeId: storeIdParsed,
            isPageOn,
            metaTitle: page.fields?.metaTitle,
            metaDescription: page.fields?.metaDescription,
            showFooter: !page.fields?.hideFooter,
            sections: sectionsProps,
            pageSections: sections,
            hasPersonalizedContent: hasPersonalizedContent(sections),
        },
        revalidate: revalidate(),
    };
};

export const getStaticPaths: GetStaticPaths = async () => {
    let result;
    let paths = [];
    const isPageOn = isLocationPagesOn();
    if (!isPageOn) {
        return { paths, fallback: false };
    }

    try {
        result = await GetLocationUrls();
    } catch (e) {
        return { paths, fallback: false };
    }
    const urlsResponse = result && (result as ILocationUrlsResponseModel);
    paths =
        urlsResponse?.urls &&
        urlsResponse.urls.reduce((array, urlParts) => {
            if (
                urlParts.countryCode &&
                urlParts.stateOrProvinceCode &&
                urlParts.cityName &&
                urlParts.address &&
                urlParts.storeId
            ) {
                array.push({
                    params: urlParts,
                });
            }
            return array;
        }, []);

    return { paths, fallback: false };
};

export default LocationDetailsPage;
