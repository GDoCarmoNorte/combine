import React, { useState, useEffect, useMemo } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { GetStaticProps, GetStaticPaths } from 'next';

import { ContentfulDelivery } from '../../../../lib/contentfulDelivery';

import revalidate from '../../../../lib/revalidate';
import Footer from '../../../../components/organisms/footer';
import BaseHeader from '../../../../components/organisms/header/baseHeader';
import { ILocationNotFoundFields } from '../../../../@generated/@types/contentful';
import { Entry } from 'contentful';

import styles from './index.module.css';
import getFeatureFlags, { IFeatureFlags, isLocationPagesOn } from '../../../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../../../redux/hooks/useFeatureFlags';
import { getStateLocationsPageTitle } from '../../../../common/helpers/getPageTitle';
import StateLocationsPageLayout from '../../../../components/organisms/locations/stateLocationsPageLayout';
import { ILocationUrlsResponseModel, ILocationsByStateOrProvinceModel } from '../../../../@generated/webExpApi';
import BrandLoader from '../../../../components/atoms/BrandLoader';
import useLocationList from '../../../../common/hooks/useLocationList';
import NotFoundScreen from '../../../../components/organisms/locationsPageContent/pickupSearchResults/notFoundScreen';
import useNotifications from '../../../../redux/hooks/useNotifications';
import { IPageSections } from '../../../../components/sections';
import { getStatePageDescription } from '../../../../common/helpers/getPageDescription';
import { getCanonicalUrl } from '../../../../common/helpers/getCanonicalUrl';
import { useGlobalProps } from '../../../../redux/hooks';

interface ILocationPageProps {
    metaTitle?: string;
    metaDescription?: string;
    showFooter?: boolean;
    sections?: {
        locationNotFound: Entry<ILocationNotFoundFields>;
    };
    pageSections?: IPageSections;
    featureFlags: IFeatureFlags;
    isPageOn: boolean;
    countryCode: string;
    stateOrProvinceCode: string;
    canonicalPath: string;
}

const GetLocationUrls = async (): Promise<ILocationUrlsResponseModel> => {
    return await useLocationList().getLocationUrlsList();
};

const StateLocationsPage = (props: ILocationPageProps): JSX.Element => {
    const globalProps = useGlobalProps();

    const {
        featureFlags,
        countryCode,
        stateOrProvinceCode,
        isPageOn,
        pageSections,
        metaTitle,
        showFooter,
        metaDescription,
        canonicalPath,
    } = props;

    const router = useRouter();
    const [stateLocationsLoaded, setStateLocationsLoaded] = useState(false);
    const [stateLocations, setStateLocations] = useState(null);
    const locationList = useLocationList();
    const {
        actions: { enqueueError },
    } = useNotifications();

    useEffect(() => {
        if (!isPageOn) {
            router.replace('/404');
        }

        isPageOn &&
            (async () => {
                try {
                    const result = await locationList.getLocationList();
                    const country = countryCode.toUpperCase();
                    const state = stateOrProvinceCode.toUpperCase();
                    const stateLocationsResponse = result?.locationsByCountry
                        ?.find((c) => c.countryCode.toUpperCase() === country)
                        ?.statesOrProvinces?.find((l) => l.code.toUpperCase() === state);
                    stateLocationsResponse &&
                        setStateLocations(stateLocationsResponse as ILocationsByStateOrProvinceModel);
                } catch (e) {
                    enqueueError({
                        title: 'Locations were not found',
                        message: 'Please try again later',
                    });
                } finally {
                    setStateLocationsLoaded(true);
                }
            })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const pageMetaTitle = useMemo(() => getStateLocationsPageTitle(metaTitle, stateLocations?.name), [
        stateLocations,
        metaTitle,
    ]);
    const pageDescription = useMemo(() => getStatePageDescription(metaDescription, stateLocations?.name), [
        stateLocations,
        metaDescription,
    ]);

    if (!isPageOn) {
        return <div aria-label="State locations Page" />;
    }

    const { footer, alertBanners, navigation, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const { header, body, icon } = props.sections.locationNotFound.fields;

    const isLoading = !stateLocationsLoaded;
    const isLocationsListAvailable = !!stateLocations;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <div className={styles.container}>
                <Head>
                    <title>{pageMetaTitle}</title>
                    <meta name="description" content={pageDescription} />
                    <link rel="canonical" href={canonicalPath} />
                </Head>
                {!isLoading && !isLocationsListAvailable && <NotFoundScreen header={header} body={body} icon={icon} />}
                {isLoading && <BrandLoader className={styles.loader} />}
                {isLocationsListAvailable && (
                    <StateLocationsPageLayout
                        countryCode={countryCode}
                        stateLocationDetails={stateLocations}
                        sections={pageSections}
                        pageMetaTitle={pageMetaTitle}
                    />
                )}
            </div>
            {footer && showFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const { countryCode, stateOrProvinceCode } = context.params;

    const featureFlags = getFeatureFlags();
    const isPageOn = isLocationPagesOn();
    const defaultCanonical = `${process.env.NEXT_PUBLIC_APP_URL}/locations/${countryCode}/${stateOrProvinceCode}/`;

    const props = {
        featureFlags,
        countryCode,
        stateOrProvinceCode,
        isPageOn,
        canonicalPath: defaultCanonical,
    };
    if (!isPageOn) {
        return {
            props,
            revalidate: revalidate(),
        };
    }

    const page = await ContentfulDelivery(context.preview).getPage('statelocations');
    const sections = page.fields?.section || [];
    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});
    const canonicalPath = getCanonicalUrl(page, defaultCanonical);

    return {
        props: {
            featureFlags,
            countryCode,
            stateOrProvinceCode,
            isPageOn,
            metaTitle: page.fields?.metaTitle,
            metaDescription: page.fields?.metaDescription,
            showFooter: !page.fields?.hideFooter,
            sections: sectionsProps,
            pageSections: sections,
            canonicalPath,
        },
    };
};

export const getStaticPaths: GetStaticPaths = async () => {
    let result;
    let paths = [];
    const isPageOn = isLocationPagesOn();
    if (!isPageOn) {
        return { paths, fallback: false };
    }
    try {
        result = await GetLocationUrls();
    } catch (e) {
        return { paths, fallback: false };
    }
    const urlsResponse = result && (result as ILocationUrlsResponseModel);
    paths =
        urlsResponse?.urls &&
        urlsResponse.urls.reduce((array, urlParts) => {
            if (
                urlParts.countryCode &&
                urlParts.stateOrProvinceCode &&
                urlParts.cityName &&
                urlParts.address &&
                urlParts.storeId
            ) {
                array.push({
                    params: urlParts,
                });
            }
            return array;
        }, []);

    return { paths, fallback: false };
};

export default StateLocationsPage;
