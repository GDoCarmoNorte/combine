import React, { useState, useEffect, useMemo } from 'react';
import Head from 'next/head';
import { GetStaticProps } from 'next';

import { ContentfulDelivery } from '../../../lib/contentfulDelivery';

import revalidate from '../../../lib/revalidate';
import Footer from '../../../components/organisms/footer';
import BaseHeader from '../../../components/organisms/header/baseHeader';
import { ILocationNotFoundFields } from '../../../@generated/@types/contentful';
import { Entry } from 'contentful';

import styles from './alllocations.module.css';
import getBrandInfo from '../../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags, isLocationPagesOn } from '../../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../../redux/hooks/useFeatureFlags';
import { getPageTitle } from '../../../common/helpers/getPageTitle';
import { getPageDescriptionByMetaInfo } from '../../../common/helpers/getPageDescription';
import { useRouter } from 'next/router';
import AllLocationsPageLayout from '../../../components/organisms/locations/allLocationsPageLayout';
import { ICountryLocationsResponseModel } from '../../../@generated/webExpApi';
import BrandLoader from '../../../components/atoms/BrandLoader';
import useLocationList from '../../../common/hooks/useLocationList';
import NotFoundScreen from '../../../components/organisms/locationsPageContent/pickupSearchResults/notFoundScreen';
import useNotifications from '../../../redux/hooks/useNotifications';
import { IPageSections } from '../../../components/sections';
import { getCanonicalUrl } from '../../../common/helpers/getCanonicalUrl';
import { useGlobalProps } from '../../../redux/hooks';

interface ILocationPageProps {
    sections: {
        locationNotFound: Entry<ILocationNotFoundFields>;
    };
    metaTitle?: string;
    metaDescription?: string;
    showFooter?: boolean;
    featureFlags: IFeatureFlags;
    isPageOn: boolean;
    pageSections: IPageSections;
    canonicalPath: string;
}

const AllLocationsPage = (props: ILocationPageProps): JSX.Element => {
    const globalProps = useGlobalProps();

    const { featureFlags, isPageOn, pageSections, metaTitle, showFooter, metaDescription, canonicalPath } = props;
    const { brandName } = getBrandInfo();

    const router = useRouter();
    const [isLocationsListLoaded, setLocationsListLoaded] = useState(false);
    const [locations, setLocations] = useState(null);
    const locationList = useLocationList();
    const {
        actions: { enqueueError },
    } = useNotifications();

    useEffect(() => {
        if (!isPageOn) {
            router.replace('/404');
        }

        isPageOn &&
            (async () => {
                try {
                    const result = await locationList.getLocationList();
                    setLocations(result as ICountryLocationsResponseModel);
                } catch (e) {
                    enqueueError({
                        title: 'Locations were not found',
                        message: 'Please try again later',
                    });
                } finally {
                    setLocationsListLoaded(true);
                }
            })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const pageMetaTitle = useMemo(() => getPageTitle(brandName, metaTitle), [brandName, metaTitle]);
    const pageDescription = useMemo(() => getPageDescriptionByMetaInfo(metaDescription), [metaDescription]);

    if (!isPageOn) {
        return <div aria-label="Locations list Page" />;
    }

    const { footer, alertBanners, navigation, userAccountMenu } = globalProps;
    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const { header, body, icon } = props.sections.locationNotFound.fields;

    const isLoading = !isLocationsListLoaded;
    const isLocationsListAvailable = !!(locations?.locationsByCountry && locations.totalCount);

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <div className={styles.container}>
                <Head>
                    <title>{pageMetaTitle}</title>
                    <meta name="description" content={pageDescription} />
                    <link rel="canonical" href={canonicalPath} />
                </Head>
                {!isLoading && !isLocationsListAvailable && <NotFoundScreen header={header} body={body} icon={icon} />}
                {isLoading && <BrandLoader className={styles.loader} />}
                {isLocationsListAvailable && (
                    <AllLocationsPageLayout
                        allLocationDetails={locations}
                        sections={pageSections}
                        pageMetaTitle={pageMetaTitle}
                    />
                )}
            </div>
            {footer && showFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const isPageOn = isLocationPagesOn();
    const featureFlags = getFeatureFlags();
    const props = {
        featureFlags,
        isPageOn,
    };
    if (!isPageOn) {
        return {
            props,
            revalidate: revalidate(),
        };
    }
    const page = await ContentfulDelivery(context.preview).getPage('locations/all');
    const sections = page.fields?.section || [];
    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    const canonicalPath = getCanonicalUrl(
        page,
        `${process.env.NEXT_PUBLIC_APP_URL}/${page.fields.link.fields.nameInUrl}`
    );

    return {
        props: {
            featureFlags,
            isPageOn,
            metaTitle: page.fields?.metaTitle,
            metaDescription: page.fields?.metaDescription,
            showFooter: !page.fields?.hideFooter,
            sections: sectionsProps,
            pageSections: sections,
            canonicalPath,
        },
    };
};

export default AllLocationsPage;
