// global styles for all brands (style resets, default styles, and etc.)
// TODO: Move this as it causes a "cascading dependency" that hurts our Lighthouse score
import '../public/normalize.css';
import '../styles/variables.css';
// TODO: remove global css typography elements styles
import '../styles/global.css';

import { AppProps } from 'next/app';
import Router from 'next/router';

import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { StylesProvider } from '@material-ui/core/styles';

import Notifications from '../components/organisms/notifications/notifications';
import Bag from '../components/organisms/bag';
import { AppStore, createStore } from '../redux/store';
import CookieBanner from '../components/clientOnly/cookieBanner';

import DataProvider from '../components/dataProvider';
import ErrorBoundary from '../components/organisms/errorBoundary';
import PageLoader from '../components/clientOnly/pageLoader';

import scrollBehaviorPolyfillInit from '../common/polyfills/scrollBehaviorPolyfill';
import NewRelicSnippet from '../components/NewRelicSnippet';
import logger, { isNewRelicAvailable } from '../common/services/logger';
import UnsupportedBrowser from '../components/clientOnly/unsupportedBrowser';
import getBrandInfo from '../lib/brandInfo';
import { useInitApp } from '../common/hooks/useInitApp';
import checkSupportBrowser from '../lib/checkSupportBrowser';
import useRouteChangeComplete from '../common/hooks/useRouteChangeComplete';
import { Auth0Provider } from '@auth0/auth0-react';
import { useFeatureFlags } from '../redux/hooks/useFeatureFlags';
import { PersonalizationProvider } from '../components/PersonalizationProvider';
import { isBreadcrumbSchemaOn } from '../lib/getFeatureFlags';
import { useBreadcrumbSchema } from '../common/hooks/useBreadcrumbSchema';
import { UnavailableItemsByLocationModal } from '../components/organisms/unavailableItemsByLocationModal/unavailableItemsByLocationModal';

scrollBehaviorPolyfillInit();

function InspireApp({ Component, pageProps }: AppProps): JSX.Element {
    const [store, setStore] = useState<AppStore>(null);
    const { featureFlags } = useFeatureFlags();

    const breadcrumbSchema = useBreadcrumbSchema();

    let acceptCookie = false;
    let continueAnyway = true;
    let isCurrentBrowserSupported = true;
    let isMobile = false;
    let redirectUri = `/callback`;

    if (process.browser) {
        const resultCheck = checkSupportBrowser();

        isCurrentBrowserSupported = resultCheck.isCurrentBrowserSupported;
        isMobile = resultCheck.isMobile;

        acceptCookie = JSON.parse(localStorage.getItem('acceptCookie'));
        continueAnyway = JSON.parse(sessionStorage.getItem('continueAnyway'));
        redirectUri = window && `${window.location.origin}/callback`;
    }

    const [isSetContinueAnyway, setIsContinueAnyway] = useState(continueAnyway);
    const [showCookieBanner, setShowCookieBanner] = useState(!acceptCookie);

    if (!store) {
        setStore(createStore());
    }

    useInitApp(store);

    useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement.removeChild(jssStyles);
        }
        if (isNewRelicAvailable) {
            logger.init();
        }
    }, []);
    const brand = process.env.NEXT_PUBLIC_BRAND;
    const GA_TRACKING_ID = process.env.NEXT_PUBLIC_GA_TRACKING_ID;
    const AUTH_0_DOMAIN = process.env.NEXT_PUBLIC_AUTH_0_DOMAIN;
    const AUTH_0_CLIENT_ID = process.env.NEXT_PUBLIC_AUTH_0_CLIENT_ID;
    const { brandName } = getBrandInfo();

    useRouteChangeComplete(() => {
        // Makes every NEXT.JS route change event scroll window to top
        window.scrollTo(0, 0);

        window.dataLayer.push({ event: 'optimize.activate' });
    });

    // Forces browser to reload page when navigating back/forward in history
    // without using back-forward caching (used in some mobile browsers (Safari, Chrome))
    // https://developers.google.com/web/updates/2019/02/back-forward-cache
    // https://github.com/vercel/next.js/issues/6128
    useEffect(() => {
        window.onpageshow = function (event) {
            if (event.persisted) {
                window.location.reload();
            }
        };
    }, []);

    const handleCloseCookieBanner = () => {
        setShowCookieBanner(false);
    };

    const handleContinueAnywayStorageChange = () => {
        setIsContinueAnyway(JSON.parse(sessionStorage.getItem('continueAnyway')));
    };

    // cross-browser solution to support viewport height (100vh)
    const handleWindowResize = () => {
        document.documentElement.style.setProperty('--app-height', `${window.innerHeight}px`);
    };

    useEffect(() => {
        handleWindowResize();
        window.addEventListener('storage', handleContinueAnywayStorageChange, false);
        window.addEventListener('resize', handleWindowResize);

        return () => {
            window.removeEventListener('storage', handleContinueAnywayStorageChange);
            window.removeEventListener('resize', handleWindowResize);
        };
    }, []);

    const onRedirectCallback = (appState) => {
        // Use Next.js's Router.replace method to replace the url
        Router.replace(appState?.target || appState?.returnTo || window.location.pathname);
    };

    const renderComponents = () => {
        return (
            <Provider store={store}>
                <DataProvider>
                    <PersonalizationProvider>
                        <div id="main">
                            <StylesProvider injectFirst>
                                <ErrorBoundary>
                                    {isCurrentBrowserSupported || isSetContinueAnyway ? (
                                        <Component {...pageProps} />
                                    ) : (
                                        <UnsupportedBrowser isMobile={isMobile} {...pageProps} />
                                    )}
                                </ErrorBoundary>
                                <Bag />
                                <Notifications />
                                <UnavailableItemsByLocationModal />
                                {showCookieBanner && <CookieBanner onClose={handleCloseCookieBanner} />}
                                <PageLoader />
                            </StylesProvider>
                        </div>
                    </PersonalizationProvider>
                </DataProvider>
            </Provider>
        );
    };

    return (
        <>
            <Head>
                <title>{brandName}</title>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
                <link rel="icon" type="image/png" href={`/brands/${brand}/favicon-16x16.png`} sizes="16x16" />
                <link rel="icon" type="image/png" href={`/brands/${brand}/favicon-32x32.png`} sizes="32x32" />
                <link rel="icon" type="image/png" href={`/brands/${brand}/favicon-48x48.png`} sizes="48x48" />
                <link rel="apple-touch-icon" href={`/brands/${brand}/apple-touch-icon.png`} sizes="180x180" />
                <link rel="mask-icon" href={`/brands/${brand}/logo.svg`} />
                <link rel="manifest" href={`/brands/${brand}/site.webmanifest`} />

                {/* legacy, non brand-specific iconography */}
                <link type="text/css" rel="stylesheet" href={`/legacy/inspire-icons.css`} />

                {/* type identity */}
                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/theme.css`} />
                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/brand-icons.css`} />
                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/fonts.css`} />
                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/typography.css`} />
                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/lists.css`} />
                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/molecules.css`} />

                <link type="text/css" rel="stylesheet" href={`/brands/${brand}/global.css`} />
                <NewRelicSnippet />
                <script
                    dangerouslySetInnerHTML={{
                        __html: `
                        (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
                        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
                        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
                        })(window,document.documentElement,'async-hide','dataLayer',4000,
                        {'${GA_TRACKING_ID}':true});
                    `,
                    }}
                />
                <script
                    dangerouslySetInnerHTML={{
                        __html: `
                        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                        })(window,document,'script','dataLayer','${GA_TRACKING_ID}');
                    `,
                    }}
                />
                <script
                    dangerouslySetInnerHTML={{
                        __html: `
                        (function(w,l){w[l]=w[l]||[];w[l].push({'event':'optimize.activate'})
                        })(window,'dataLayer');
                    `,
                    }}
                />
                {isBreadcrumbSchemaOn() && (
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(breadcrumbSchema) }}
                    />
                )}
            </Head>

            {featureFlags.account ? (
                <Auth0Provider
                    domain={AUTH_0_DOMAIN}
                    clientId={AUTH_0_CLIENT_ID}
                    redirectUri={redirectUri}
                    onRedirectCallback={onRedirectCallback}
                    useRefreshTokens={true}
                    advancedOptions={{
                        defaultScope: 'openid',
                    }}
                >
                    {renderComponents()}
                </Auth0Provider>
            ) : (
                renderComponents()
            )}
        </>
    );
}

// TODO: In future version of NextJS, we will be able to use getStaticProps to populate our navigation & alertBanners
// export const getStaticProps: GetStaticProps = async (context) => {
//     const navigation = await ContentfulDelivery(context.preview).getNavigation()
//     return { props: { navigation } };
// };

export default InspireApp;
