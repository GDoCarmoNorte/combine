import React, { useEffect, useRef, useState } from 'react';
import { IPageProps } from '../[nameInUrl]';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';
import { GetStaticProps } from 'next';
import getFeatureFlags from '../../lib/getFeatureFlags';
import revalidate from '../../lib/revalidate';
import Navigation from '../../auth0/src/components/navigation';
import InfoBanner from '../../components/atoms/infoBanner';
import ContentfulImage from '../../components/atoms/ContentfulImage';
import styles from './index.module.css';
import { useRouter } from 'next/router';
import Card from '../../auth0/src/components/card';
import FormikInput from '../../components/organisms/formikInput';
import { InspireButton } from '../../auth0/src/components/button';
import { Field, Form, Formik } from 'formik';
import { validateDateOfBirth, validateEmail } from '../../common/helpers/complexValidateHelper';
import { autoCorrectedDatePipe } from '../../common/helpers/autoCorrectedDatePipe';
import getBrandInfo from '../../lib/brandInfo';
import classNames from 'classnames';
import CheckIcon from '@material-ui/icons/Check';
import Icon from '../../components/atoms/BrandIcon';
import { verifyJWT } from '../../lib/jwt';
import { useGlobalProps } from '../../redux/hooks';

const validateEmailField = validateEmail('Email');
const validateDOBField = validateDateOfBirth('Date Of Birth');

const AUTH0_DOMAIN = process.env.NEXT_PUBLIC_AUTH_0_DOMAIN;

/**
 * This is a page for progressive profiling for users that have required fields not filled in profile.
 *
 * Users are not supposed to visit this page directly.
 * Auth0 redirects users here and this pages redirects back to Auth0 with
 * filled user information to complete authorization process.
 */
export default function ProgressiveProfile(props: IPageProps): JSX.Element {
    const globalProps = useGlobalProps();
    const [tokenVerified, setTokenVerified] = useState(false);
    const [tokenVerificationComplete, setTokenVerificationComplete] = useState(false);
    const { query, replace, isReady } = useRouter();
    const { brandName } = getBrandInfo();
    const { featureFlags } = props;
    const webNavigation = globalProps.navigation?.items?.find((item) => item.fields.name === 'Web');

    const initialFormValues = {
        email: '',
        birthDate: '',
        marketingOptIn: false,
    };

    useEffect(() => {
        if (!isReady) return;
        if (typeof query.token !== 'string') {
            setTokenVerificationComplete(true);
            return;
        }

        try {
            verifyJWT(query.token);
            setTokenVerified(true);
        } catch (error) {
            console.error(error);
        } finally {
            setTokenVerificationComplete(true);
        }
    }, [isReady, query.token]);

    useEffect(() => {
        if (!tokenVerified && tokenVerificationComplete) {
            replace('/');
        }
    }, [tokenVerified, tokenVerificationComplete, replace]);

    const formRef = useRef(null);
    const handleSubmit = () => {
        formRef.current.submit();
    };

    if (!tokenVerified || !tokenVerificationComplete) return null;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <div className="sticky-top">
                <Navigation
                    pictureComponent={
                        <ContentfulImage
                            className={styles.logoImage}
                            asset={webNavigation?.fields?.logo}
                            useInitialFormat
                        />
                    }
                />
            </div>
            <InfoBanner title="Hello!" mainText="COME AND GET YOUR MEATS" wrapperClassName={styles.wrapper} />
            <Card containerClassName={styles.formWrapper}>
                <div className={styles.iconWrapper}>
                    <Icon icon="action-check" size="huge" className={styles.icon} />
                </div>
                <h4 className={classNames('t-header-h3', styles.heading)}>
                    Please provide additional info to complete your sign-up
                </h4>
                <p className={styles.infoText}>We are missing your email and date of birth to send you deals</p>
                <p className={classNames(styles.infoSubtitle, 't-paragraph-hint')}>
                    Don’t worry, we wont tell anyone how old are you
                </p>
                <Formik onSubmit={handleSubmit} initialValues={initialFormValues} validateOnMount>
                    {({ setFieldValue, isValid }) => (
                        <Form
                            action={`https://${AUTH0_DOMAIN}/continue?state=${query.state}`}
                            method="POST"
                            ref={formRef}
                        >
                            <FormikInput
                                label="Email"
                                labelClassName={styles.label}
                                name="email"
                                onChange={(e) => {
                                    setFieldValue('email', e.target.value);
                                }}
                                placeholder="Enter Email"
                                required
                                validate={validateEmailField}
                            />
                            <FormikInput
                                label="Date Of Birth"
                                labelClassName={styles.label}
                                name="birthDate"
                                placeholder="MM / DD"
                                mask={[/\d/, /\d/, '/', /\d/, /\d/]}
                                validate={validateDOBField}
                                pipe={autoCorrectedDatePipe}
                            />
                            <Field name="marketingOptIn">
                                {({ field: { value } }) => (
                                    <div className={styles.marketingContainer}>
                                        <input
                                            className={styles.checkbox}
                                            type="checkbox"
                                            id="marketing"
                                            name="marketingOptIn"
                                            checked={value}
                                            onChange={(e) => setFieldValue('marketingOptIn', e.target.checked)}
                                        />
                                        <div
                                            tabIndex={0}
                                            className={styles.styledCheckbox}
                                            onClick={() => setFieldValue('marketingOptIn', !value)}
                                            onKeyPress={({ key }) =>
                                                key === 'Enter' && setFieldValue('marketingOptIn', !value)
                                            }
                                        >
                                            <CheckIcon className={styles.checkIcon} />
                                        </div>
                                        <label
                                            htmlFor="marketing"
                                            className={classNames(styles.marketingLabel, 't-paragraph-hint')}
                                        >
                                            {`I would like to receive email communication from ${brandName}`}
                                        </label>
                                    </div>
                                )}
                            </Field>
                            <InspireButton
                                tabIndex={0}
                                className={styles.submitButton}
                                disabled={!isValid}
                                submit
                                text="SAVE"
                            />
                        </Form>
                    )}
                </Formik>
            </Card>
        </FeatureFlagsContext.Provider>
    );
}

export const getStaticProps: GetStaticProps = async () => {
    const featureFlags = getFeatureFlags();

    return {
        props: {
            featureFlags,
        },
        revalidate: revalidate(),
    };
};
