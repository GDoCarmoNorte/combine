import React, { useEffect, useMemo } from 'react';
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import { ContentfulDelivery } from '../../lib/contentfulDelivery';

import revalidate from '../../lib/revalidate';
import BaseHeader from '../../components/organisms/header/baseHeader';
import Head from 'next/head';
import Footer from '../../components/organisms/footer';

import { getPageDescriptionByMetaInfo } from '../../common/helpers/getPageDescription';

import { getPageTitle } from '../../common/helpers/getPageTitle';
import getBrandInfo from '../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags, isPlayExperienceEnabled } from '../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';
import PlayPageLayout from '../../components/organisms/play/playPageLayout';
import { IPageSections } from '../../components/sections';
import { useMediaQuery } from '@material-ui/core';
import { PageContentWrapper } from '../../components/sections/PageContentWrapper';
import { useGlobalProps } from '../../redux/hooks';

interface IPlayPageProps {
    isPageOn: boolean;
    featureFlags: IFeatureFlags;
    pageSections: IPageSections;
    metaTitle?: string;
    metaDescription?: string;
    showFooter?: boolean;
}

const PlayPage = (props: IPlayPageProps): JSX.Element => {
    const globalProps = useGlobalProps();

    const { footer, navigation, alertBanners, userAccountMenu } = globalProps;
    const { isPageOn, metaTitle, metaDescription, showFooter, featureFlags } = props;
    const router = useRouter();
    const { brandName } = getBrandInfo();

    const isDesktop = useMediaQuery('(min-width: 600px)');

    useEffect(() => {
        if (!isPageOn) {
            router.replace('/404');
            return;
        }
    }, [isPageOn, router]);

    useEffect(() => {
        if (isPageOn && isDesktop) {
            router.replace(process.env.NEXT_PUBLIC_PLAY_PAGE_URL);
        }
    }, [isDesktop, isPageOn, router]);

    const pageDescription = useMemo(() => getPageDescriptionByMetaInfo(metaDescription), [metaDescription]);

    if (!isPageOn || isDesktop) {
        return <div aria-label="Play Page" />;
    }

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <Head>
                <title>{getPageTitle(brandName, metaTitle)}</title>
                <meta name="description" content={pageDescription} />
            </Head>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <PageContentWrapper>
                <h1 className="visually-hidden">{`find an ${brandName}`}</h1>
                <PlayPageLayout />
            </PageContentWrapper>
            {footer && showFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const isPageOn = isPlayExperienceEnabled();
    const featureFlags = getFeatureFlags();
    const props = {
        featureFlags,
        isPageOn,
    };

    if (!isPageOn) {
        return {
            props,
            revalidate: revalidate(),
        };
    }

    const page = await ContentfulDelivery(context.preview).getPage('play');

    return {
        props: {
            featureFlags,
            isPageOn,
            metaTitle: page.fields?.metaTitle,
            metaDescription: page.fields?.metaDescription,
            showFooter: !page.fields?.hideFooter,
        },
    };
};

export default PlayPage;
