import React from 'react';
import { Entry } from 'contentful';
import { GetStaticProps } from 'next';
import Head from 'next/head';

import { IPageFields } from '../@generated/@types/contentful';

import PageSections from '../components/sections';

import styles from './index.module.css';
import { ContentfulDelivery } from '../lib/contentfulDelivery';
import Footer from '../components/organisms/footer';
import BaseHeader from '../components/organisms/header/baseHeader';
import getBrandInfo from '../lib/brandInfo';
import { getPageTitle } from '../common/helpers/getPageTitle';
import { getPageDescription } from '../common/helpers/getPageDescription';
import getFeatureFlags, { IFeatureFlags } from '../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../redux/hooks/useFeatureFlags';
import { PageContentWrapper } from '../components/sections/PageContentWrapper';
import { useGlobalProps, usePersonalization } from '../redux/hooks';
import hasPersonalizedContent from '../lib/hasPersonalizedContent';
import BrandLoader from '../components/atoms/BrandLoader';

export interface I404PageProps {
    page: Entry<IPageFields>;
    hasPersonalizedContent: boolean;
    featureFlags: IFeatureFlags;
    isPreviewMode: boolean;
}

export default function Custom404(props: I404PageProps): JSX.Element {
    const { page, featureFlags, isPreviewMode, hasPersonalizedContent } = props;
    const sections = page.fields.section || [];
    const globalProps = useGlobalProps();

    const { alertBanners, navigation, footer, userAccountMenu } = globalProps;
    const { brandName } = getBrandInfo();

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const { metaTitle, hideFooter } = props.page.fields;
    const { loading: isPersonalizationLoading } = usePersonalization();
    const shouldShowLoader = hasPersonalizedContent && isPersonalizationLoading;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                userAccountMenu={userAccountMenu}
                isPreviewMode={isPreviewMode}
            />
            <div className="container">
                <Head>
                    <title>{getPageTitle(brandName, metaTitle)}</title>
                    <meta name="description" content={getPageDescription(page)} />
                </Head>
                <PageContentWrapper>
                    {shouldShowLoader && <BrandLoader className={styles.loader} />}
                    {!shouldShowLoader && sections.length > 0 && <PageSections pageSections={sections} />}
                </PageContentWrapper>
            </div>
            {footer && !hideFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const page = await ContentfulDelivery(context.preview).getPage('404');
    const featureFlags = getFeatureFlags();

    return {
        props: {
            page,
            hasPersonalizedContent: hasPersonalizedContent(page.fields.section),
            isPreviewMode: !!context?.preview,
            featureFlags,
        },
    };
};
