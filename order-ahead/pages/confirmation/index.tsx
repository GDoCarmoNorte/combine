import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import { Entry } from 'contentful';
import { ContentfulDelivery } from '../../lib/contentfulDelivery';

import {
    ISecondaryBannerFields,
    IPickupInstructionsFields,
    IPageFields,
    ICheckoutLegalFields,
} from '../../@generated/@types/contentful';

import revalidate from '../../lib/revalidate';
import BaseHeader from '../../components/organisms/header/baseHeader';
import Head from 'next/head';
import Footer from '../../components/organisms/footer';
import useTallyOrder from '../../redux/hooks/useTallyOrder';
import useSubmitOrder from '../../redux/hooks/useSubmitOrder';

import styles from './index.module.css';
import { useBag, useGlobalProps, useOrderLocation } from '../../redux/hooks';

import { getPageDescription } from '../../common/helpers/getPageDescription';

import { getPageTitle } from '../../common/helpers/getPageTitle';
import getBrandInfo from '../../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags } from '../../lib/getFeatureFlags';
import { FeatureFlagsContext } from '../../redux/hooks/useFeatureFlags';
import { PageContentWrapper } from '../../components/sections/PageContentWrapper';
import { useAuth0 } from '@auth0/auth0-react';
import Confirmation from '../../components/organisms/confirmation/confirmation';
import { GTM_TRANSACTION_INFO } from '../../common/services/gtmService/constants';
import { TallyFulfillmentTypeModel } from '../../@generated/webExpApi';
import { OrderLocationMethod } from '../../redux/orderLocation';
import { useConfiguration } from '../../common/hooks/useConfiguration';

interface OrderConfirmationPageProps {
    sections: {
        pickupInstructions: Entry<IPickupInstructionsFields>;
        secondaryBanner: Entry<ISecondaryBannerFields>;
    };
    page: Entry<IPageFields>;
    featureFlags: IFeatureFlags;
    checkoutLegal?: ICheckoutLegalFields;
}
const OrderConfirmationPage = (props: OrderConfirmationPageProps): JSX.Element => {
    const globalProps = useGlobalProps();
    const {
        configuration: { isTippingEnabled },
    } = useConfiguration();

    const { footer, navigation, productsById, alertBanners, userAccountMenu } = globalProps;

    const { brandName } = getBrandInfo();

    const { sections, page, featureFlags, checkoutLegal } = props;

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');

    const pickupInstructions = sections?.pickupInstructions?.fields?.instractions;
    const backgroundColor = sections?.secondaryBanner?.fields?.backgroundColor;

    const dispatch = useDispatch();
    const router = useRouter();

    const { currentLocation: location } = useOrderLocation();
    const { tallyOrder, resetTallyOrder } = useTallyOrder();
    const { lastOrder, lastRequest, resetSubmitOrder } = useSubmitOrder();
    const { isAuthenticated } = useAuth0();
    const {
        actions: { resetOrderTime },
    } = useBag();

    useEffect(() => {
        if (!tallyOrder) {
            console.log('[CONFIRMATION]', 'no tally order found', 'redirecting...');
            router.replace('/');
            return;
        }
        dispatch({
            type: GTM_TRANSACTION_INFO,
            payload: {
                order: lastOrder,
                request: lastRequest?.orderRequestModel,
                tallyOrder,
                location,
                loginType: isAuthenticated ? 'LoggedIn' : 'guest',
            },
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tallyOrder]);

    useEffect(() => {
        resetOrderTime();
        return () => {
            resetTallyOrder();
            resetSubmitOrder();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const lastOrderData = lastRequest?.orderRequestModel?.orderData;
    const specialDeliveryIstructions = lastOrderData?.instructions[0];
    const tipsAmount = isTippingEnabled ? lastOrderData?.driverTip || lastOrderData?.serverTip : 0;
    // TODO remove hardcode after integration
    const deliveryInfo = {
        deliveryOption: '',
        specialIstructions: specialDeliveryIstructions,
    };

    const { metaTitle, hideFooter } = props.page.fields;

    const orderLocationMethodMap = {
        [TallyFulfillmentTypeModel.PickUp]: OrderLocationMethod.PICKUP,
        [TallyFulfillmentTypeModel.Delivery]: OrderLocationMethod.DELIVERY,
    };

    const method = orderLocationMethodMap[tallyOrder?.fulfillment.type] || OrderLocationMethod.NOT_SELECTED;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <Head>
                <title>{getPageTitle(brandName, metaTitle)}</title>
                <meta name="description" content={getPageDescription(page)} />
            </Head>
            <BaseHeader alertBanners={alertBanners} webNavigation={webNavigation} userAccountMenu={userAccountMenu} />
            <PageContentWrapper className={styles.container}>
                <Confirmation
                    deliveryInfo={deliveryInfo}
                    tallyOrder={tallyOrder}
                    productsById={productsById}
                    backgroundColor={backgroundColor}
                    secondaryBanner={sections.secondaryBanner}
                    pickupInstructions={pickupInstructions}
                    method={method}
                    isPickUp={method === OrderLocationMethod.PICKUP}
                    isAuthenticated={isAuthenticated}
                    tipsAmount={tipsAmount}
                    checkoutLegal={checkoutLegal}
                />
            </PageContentWrapper>
            {footer && !hideFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
};

export const getStaticProps: GetStaticProps = async (context) => {
    const page = await ContentfulDelivery(context.preview).getPage('confirmation');
    const sections = page.fields?.section || [];

    const sectionsProps = sections.reduce((acc, curr) => {
        return { ...acc, [curr.sys.contentType.sys.id]: curr };
    }, {});

    const featureFlags = getFeatureFlags();

    const checkoutLegal = (await ContentfulDelivery(context.preview).getCheckoutLegal())?.fields || null;

    return {
        props: {
            page,
            sections: sectionsProps,
            featureFlags,
            checkoutLegal,
        },
        revalidate: revalidate(),
    };
};

export default OrderConfirmationPage;
