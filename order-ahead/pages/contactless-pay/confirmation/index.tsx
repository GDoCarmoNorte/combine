import React from 'react';
import { isDineInOrdersEnabled } from '../../../lib/getFeatureFlags';
import ContactlessPayError from '../../../components/clientOnly/contactlessPayError';
import { TContactlessPayErrorTypes } from '../../../components/clientOnly/contactlessPayError/types';
import ContactlessPay from '../../../components/organisms/contactlessPay/contactlessPay';
import { useGlobalProps } from '../../../redux/hooks';

export default function ContactlessPayPage(): JSX.Element {
    const globalProps = useGlobalProps();
    return (
        <>
            {isDineInOrdersEnabled() ? (
                <ContactlessPay {...globalProps} isConfirmationPage />
            ) : (
                <ContactlessPayError errorType={TContactlessPayErrorTypes.DISABLED} />
            )}
        </>
    );
}
