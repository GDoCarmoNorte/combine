import React, { useEffect } from 'react';
import { Entry } from 'contentful';
import { GetStaticPaths, GetStaticProps } from 'next';
import Head from 'next/head';

import { IPageFields } from '../@generated/@types/contentful';

import PageSections from '../components/sections';

import styles from './index.module.css';
import revalidate from '../lib/revalidate';
import { ContentfulDelivery } from '../lib/contentfulDelivery';
import Footer from '../components/organisms/footer';
import BaseHeader from '../components/organisms/header/baseHeader';
import getBrandInfo from '../lib/brandInfo';
import getFeatureFlags, { IFeatureFlags } from '../lib/getFeatureFlags';
import { getPageTitle } from '../common/helpers/getPageTitle';
import { getPageDescription } from '../common/helpers/getPageDescription';
import { FeatureFlagsContext } from '../redux/hooks/useFeatureFlags';
import { useRouter } from 'next/router';
import { PageContentWrapper } from '../components/sections/PageContentWrapper';
import { setFocus } from '../common/helpers/setFocus';
import { getCanonicalUrl } from '../common/helpers/getCanonicalUrl';
import { useGlobalProps, usePersonalization } from '../redux/hooks';
import hasPersonalizedContent from '../lib/hasPersonalizedContent';
import BrandLoader from '../components/atoms/BrandLoader';

export interface IPageProps {
    page: Entry<IPageFields>;
    hasPersonalizedContent: boolean;
    isPreviewMode: boolean;
    featureFlags: IFeatureFlags;
    canonicalPath: string;
}

export default function GenericPage(props: IPageProps): JSX.Element {
    const { page, featureFlags, isPreviewMode, canonicalPath, hasPersonalizedContent } = props;
    const sections = page.fields.section || [];
    const globalProps = useGlobalProps();
    const { alertBanners, navigation, footer, userAccountMenu } = globalProps;
    const { brandName } = getBrandInfo();

    const webNavigation = navigation?.items?.find((item) => item.fields.name === 'Web');
    const { metaTitle, hideFooter } = props.page.fields;

    const router = useRouter();

    // Reset focus to the top of the application
    useEffect(() => {
        setFocus(document.body);
    }, [router.asPath]);

    const { loading: isPersonalizationLoading } = usePersonalization();
    const shouldShowLoader = hasPersonalizedContent && isPersonalizationLoading;

    return (
        <FeatureFlagsContext.Provider value={{ featureFlags }}>
            <Head>
                <title>{getPageTitle(brandName, metaTitle)}</title>
                <meta name="description" content={getPageDescription(page)} />
                <link rel="canonical" href={canonicalPath} />
            </Head>
            <BaseHeader
                alertBanners={alertBanners}
                webNavigation={webNavigation}
                userAccountMenu={userAccountMenu}
                isPreviewMode={isPreviewMode}
            />
            <PageContentWrapper className="container">
                {shouldShowLoader && <BrandLoader className={styles.loader} />}
                {!shouldShowLoader && sections.length > 0 && <PageSections pageSections={sections} />}
            </PageContentWrapper>
            {footer && !hideFooter && <Footer footer={footer} />}
        </FeatureFlagsContext.Provider>
    );
}

export const getStaticProps: GetStaticProps = async (context) => {
    const nameInUrl = Array.isArray(context.params.nameInUrl) ? context.params.nameInUrl[0] : context.params.nameInUrl;

    const page = await ContentfulDelivery(context.preview).getPage(nameInUrl);
    const featureFlags = getFeatureFlags();
    const canonicalPath = getCanonicalUrl(page, `${process.env.NEXT_PUBLIC_APP_URL}/${nameInUrl.replace(/\/+$/, '')}/`);

    return {
        props: {
            page,
            hasPersonalizedContent: hasPersonalizedContent(page.fields.section),
            isPreviewMode: !!context?.preview,
            featureFlags,
            canonicalPath,
        },
        revalidate: revalidate(),
    };
};

const STATIC_PATHS = [
    'account',
    'menu',
    'locations',
    'checkout',
    'confirmation',
    'sitemap',
    '404',
    'rewards',
    'rewardsroster',
    'play',
];

export const getStaticPaths: GetStaticPaths = async () => {
    const pages = await ContentfulDelivery().getPages();

    const paths = pages.items
        .map((page) => page.fields.link)
        .filter((link) => !!link.fields.nameInUrl) // Don't include homepage
        .filter((link) => !STATIC_PATHS.includes(link.fields.nameInUrl)) // Don't include static pages
        .map((link) => {
            return { params: { nameInUrl: link.fields.nameInUrl } };
        });

    return { paths, fallback: false };
};
