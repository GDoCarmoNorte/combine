const path = require('path');
const Trool = require('trool');

const RULE_FILE = path.join(process.cwd(), 'data/ArbysDynamicPriceRules.csv');

const DAYS = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];

const getPrice = (product) => {
    let tier = (product.activeTierAndPrices || []).find((tier) => tier.tier.default);
    if (tier) return tier.price;
    tier = (product.activeTierAndPrices || []).find((tier) => tier.tier.active);
    return tier ? tier.price : 0;
};
const getNewPrice = (wrappedProduct) => {
    let currentPrice = getPrice(wrappedProduct.product);
    if (wrappedProduct.discount) {
        return {
            price: Math.round(Math.max(currentPrice - wrappedProduct.discount, 0) * 100) / 100,
            tier: { name: wrappedProduct.rule, type: 'dynamic', active: true },
        };
    }
    if (wrappedProduct.percentOff) {
        return {
            price: Math.round(Math.max(currentPrice - (currentPrice * wrappedProduct.percentOff) / 100, 0) * 100) / 100,
            tier: { name: wrappedProduct.rule, type: 'dynamic', active: true },
        };
    }
    return {};
};
const addPrice = (wrappedProduct) => {
    if (wrappedProduct.rule && getPrice(wrappedProduct.product) > 0) {
        wrappedProduct.product.activeTierAndPrices.push(getNewPrice(wrappedProduct));
    }
};

function wrap(product, user, location) {
    let dayOfWeek = DAYS[new Date().getDay()];
    let hours = new Date().getHours();
    let obj = {
        basePrice: getPrice(product),
        productName: product.name,
        plu: product.plu,
        customerAge: user.customerAge,
        busyIndex: location.busyIndex || 50,
        dayOfWeek: dayOfWeek,
        orderTime: hours,
        addToDiscount: (d) => d,
        discount: 0,
        percentOff: 0,
        rule: '',
        productCategory: product.productCategory,
        product: product,
    };
    obj.addToDiscount = (d) => (obj.discount = obj.discount + d);
    return obj;
}

async function getMenuFacts(products, user, location) {
    let wrappedProducts = products.map((p) => wrap(p, user, location));
    //console.log(wrappedProducts[0])
    return { MenuPrice: wrappedProducts };
}
async function addDynamicPrice(menu, user, location) {
    let products = menu.productDetails;
    let productIdx = products.reduce((p, c) => ((p[c.productId] = c), p), {});
    products.forEach((p) => (p['productCategory'] = ''));
    menu.menu.subMenus.forEach((s) =>
        s.products.forEach((p) => {
            if (productIdx[p.productID]) productIdx[p.productID].productCategory = s.subMenuName;
        })
    );

    let trool = new Trool.default(true);
    const facts = await getMenuFacts(products, user, location);
    const updatedFacts = await trool.applyRules(RULE_FILE, facts);
    //console.log(updatedFacts);
    updatedFacts.MenuPrice.forEach(addPrice);
    return updatedFacts.MenuPrice.map((f) => f.product);
}

export default addDynamicPrice;
