/* eslint-disable camelcase */
import fetch from 'isomorphic-unfetch';

import auth0 from './auth0';

import config from './config';
import { HttpError } from './http-error';

const TOKEN_DELAY = 60 * 1000;

class Token {
    constructor({ access_token, expires_in }) {
        this.accessToken = access_token;
        this.ttl = Date.now() + expires_in * 1000 - TOKEN_DELAY;
    }

    get isValid() {
        return Date.now() < this.ttl;
    }
}

let currentToken;

export const getAccessToken = async () => {
    if (currentToken && currentToken.isValid) {
        return Promise.resolve(currentToken.accessToken);
    }

    const audience = encodeURIComponent(`https://${config.AUTH0_DOMAIN}/api/v2/`);
    const body = `grant_type=client_credentials&client_id=${config.AUTH0_API_ID}&client_secret=${config.AUTH0_API_SECRET}&audience=${audience}`;

    return fetch(`https://${config.AUTH0_DOMAIN}/oauth/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body,
    })
        .then((res) => {
            return res.ok ? res.json() : Promise.reject(new HttpError(res.status, res.statusText));
        })
        .then((token) => {
            currentToken = new Token(token);

            return currentToken.accessToken;
        });
};

/**
 * Resource Owner authorization
 * @param {String} email
 * @param {String} password
 */
export const authorize = async (email, password) => {
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/oauth/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `grant_type=password&client_id=${config.AUTH0_CLIENT_ID}&username=${email}&password=${password}&scope=${config.AUTH0_SCOPE}`,
    });

    const session = res.ok ? await res.json() : undefined;

    return session;
};

export const getSession = async (req) => {
    const session = await auth0.getSession(req);
    const isValid = session ? session.accessTokenExpiresAt * 1000 - TOKEN_DELAY > Date.now() : false;

    return isValid ? session : undefined;
};
