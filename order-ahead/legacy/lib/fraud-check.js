import fetch from 'isomorphic-unfetch';

import { HttpError } from './http-error';

const TRUST_CHECK_API_URL = `${process.env.FRAUD_CHECK_SERVICE_URL}`;
const TRUST_RATING_THRESHOLD = 6;

export const fraudCheck = async (email) => {
    if (!email) {
        throw new HttpError(400, 'Bad request');
    }

    const res = await fetch(`${TRUST_CHECK_API_URL}/fraud/${email}`);

    if (res.ok) {
        const result = await res.json();

        console.log('[DEBUG]', __filename, 'Email fraud check response: ', result);

        return result.emailTrustRating <= TRUST_RATING_THRESHOLD;
    }

    throw new HttpError(res.status, res.statusText);
};
