import path from 'path';
import readCSV from './readCSV';

const IMAGE_FILE = path.join(process.cwd(), 'data/arbys_images.csv');
const IMAGE_PLACEHOLDER = '/images/arbys/arbys-logo.svg';

function getImages() {
    const imageData = readCSV(IMAGE_FILE);
    imageData.forEach((p) => (p.imageURL = p.uberURL || p.arbysURL || IMAGE_PLACEHOLDER));
    const imagesByProductId = {};
    imageData.reduce((p, c) => ((p[c.productID] = c.imageURL), p), imagesByProductId);

    return imagesByProductId;
}

export default getImages;
