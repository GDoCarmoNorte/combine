if (typeof window === 'undefined') {
    /**
     * Settings exposed to the server.
     */
    module.exports = {
        AUTH0_CLIENT_ID: process.env.NEXT_PUBLIC_AUTH0_CLIENT_ID,
        AUTH0_CLIENT_SECRET: process.env.NEXT_PUBLIC_AUTH0_CLIENT_SECRET,
        AUTH0_SCOPE: 'openid profile',
        AUTH0_DOMAIN: process.env.NEXT_PUBLIC_AUTH0_DOMAIN,
        AUTH0_N_CLIENT_ID: process.env.NEXT_PUBLIC_AUTH0_N_CLIENT_ID, // Inspire POC Native (for passwordless grant type)
        REDIRECT_URI: `${process.env.NEXT_PUBLIC_AUTH0_REDIRECT_URI}/api/callback`,
        POST_LOGOUT_REDIRECT_URI: process.env.NEXT_PUBLIC_AUTH0_REDIRECT_URI,
        SESSION_COOKIE_SECRET: process.env.NEXT_PUBLIC_SESSION_COOKIE_SECRET,
        SESSION_COOKIE_LIFETIME: process.env.NEXT_PUBLIC_SESSION_COOKIE_LIFETIME,
        // see https://manage.auth0.com/dashboard/us/poc-inspirebrands/applications/kgssoXk7f3qMwHrxQZR4nHq1CSy698ML/settings
        AUTH0_API_ID: process.env.NEXT_PUBLIC_AUTH0_API_ID,
        AUTH0_API_SECRET: process.env.NEXT_PUBLIC_AUTH0_API_SECRET,
        AUTH0_DEFAULT_CONNECTION: 'Username-Password-Authentication',
    };
} else {
    /**
     * Settings exposed to the client.
     */
    module.exports = {
        AUTH0_CLIENT_ID: process.env.NEXT_PUBLIC_AUTH0_CLIENT_ID,
        AUTH0_N_CLIENT_ID: process.env.NEXT_PUBLIC_AUTH0_N_CLIENT_ID, // Inspire POC Native (for passwordless grant type)
        AUTH0_SCOPE: 'openid profile',
        AUTH0_DOMAIN: process.env.NEXT_PUBLIC_AUTH0_DOMAIN,
        REDIRECT_URI: `${process.env.NEXT_PUBLIC_AUTH0_REDIRECT_URI}api/callback`,
        POST_LOGOUT_REDIRECT_URI: process.env.NEXT_PUBLIC_AUTH0_REDIRECT_URI,
    };
}
