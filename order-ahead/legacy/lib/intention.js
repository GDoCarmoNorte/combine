import auth0 from 'auth0-js';

import config from './config';

const parseHash = (hash) => {
    if (hash.startsWith('#')) {
        hash = hash.substring(1);
    }

    const result = {};
    const tokens = decodeURIComponent(hash).split('&');

    tokens.forEach((token) => {
        const [key, value] = token.split('=');

        if (key && value) {
            result[key] = value;
        }
    });

    return result;
};

const getIntention = () => {
    const hash = parseHash(window.location.hash);

    const webAuth = new auth0.WebAuth({
        domain: config.AUTH0_DOMAIN,
        clientID: config.AUTH0_CLIENT_ID,
    });

    webAuth.parseHash({ hash: window.location.hash }, (error, result) => {
        // debugger;
    });

    return hash.state && `/${hash.state}`;
};

export default getIntention;
