import axios from 'axios';

function hasSelectedModifier(modifier) {
    return modifier.selectedModifier;
}

function processModifierGroup(group) {
    return {
        itemId: group.modifierGroupId,
        description: group.name,
        modifiers: [
            {
                itemId: group.selectedModifier.productId,
                description: group.selectedModifier.name,
            },
        ],
    };
}

function processBagItem(bagItem) {
    const processedItem = {};

    processedItem.itemId = bagItem.product.productId;
    processedItem.description = bagItem.product.name;
    processedItem.price = bagItem.priceTier ? bagItem.priceTier.price : 4.72;
    processedItem.quantity = bagItem.quantity;

    if (bagItem.modifiers.length) {
        processedItem.modifierGroups = bagItem.modifiers.filter(hasSelectedModifier).map(processModifierGroup);
    }

    return processedItem;
}

export async function submitOrder(user, location, bag) {
    const order = {
        customerId: user.user_id,
        brand: 'Arby',
        destination: 641906860,
        location: {
            locationId: location.LocationId,
            addressLine1: location.AddressLine1,
            locationName: location.LocationName,
        },
    };

    if (user.nickname) {
        order.name = user.nickname;
    } else {
        order.name = user.user_id;
    }

    const currentDate = new Date();
    const hourLater = new Date(currentDate.setTime(currentDate.getTime() + 1 * 60 * 60 * 1000));
    order.deliveryDate = hourLater.toISOString();

    const items = bag.map((bagItem) => processBagItem(bagItem));
    order.items = items;

    const response = await axios.post('/api/checkout', { user, order });

    return response.data;
}
