import config from './config';
import { getAccessToken } from './auth0-api';
import { HttpError } from './http-error';

const getUser = async (userId) => {
    const tokens = userId.split('|');
    const accessToken = await getAccessToken();
    const res = await fetch(
        `https://${config.AUTH0_DOMAIN}/api/v2/users?search_engine=v3&q=identities.user_id:${
            tokens[tokens.length - 1]
        }`,
        {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        }
    );

    if (res.ok) {
        const [user] = await res.json();

        return user;
    }

    throw new HttpError(res.status, res.statusText);
};

const updateUser = async (userId, userInfo) => {
    const accessToken = await getAccessToken();
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users/${userId}`, {
        method: 'PATCH',
        headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userInfo),
    });

    if (res.ok) {
        const user = await res.json();

        return user;
    }

    throw new HttpError(res.status, res.statusText);
};

export const checkPasswordTicket = async (userId) => {
    const user = await getUser(userId);

    if (user.user_metadata) {
        const { resetPassword, recent_password: recentPassword } = user.user_metadata;
        if (resetPassword && recentPassword) {
            await updateUser(userId, {
                password: recentPassword,
                user_metadata: {
                    resetPassword: false,
                    recent_password: null,
                },
            });
        }
    }
};
