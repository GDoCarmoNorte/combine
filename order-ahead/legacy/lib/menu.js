import arbysMenu from './arbysGenMenu';
import bwwMenu from './bwwMenu';
import getBrand from '../src/brand';

export default async function getMenu(locationId) {
    // TODO: refactor - move to lazy module
    const brand = getBrand(process.env.NEXT_PUBLIC_BRAND);
    const fn = brand.brand.toUpperCase() === 'BWW' ? bwwMenu : arbysMenu;

    return await fn(locationId);
}
