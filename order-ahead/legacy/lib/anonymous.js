import fetch from 'isomorphic-unfetch';

import config from './config';
import { HttpError } from './http-error';
import { getAccessToken } from './auth0-api';
import { parseUser } from './user';

const getAnonymousUser = async (deviceId) => {
    const accessToken = await getAccessToken();
    const res = await fetch(
        `https://${config.AUTH0_DOMAIN}/api/v2/users?search_engine=v3&q=identities.user_id:${deviceId}`,
        {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        }
    );

    if (res.ok) {
        const [user] = await res.json();

        return user;
    }

    throw new HttpError(res.status, res.statusText);
};

const createAnonymousUser = async (deviceId) => {
    const token = await getAccessToken();
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
            user_id: deviceId,
            password: deviceId,
            email: `${deviceId}@anonymous.com`,
            connection: config.AUTH0_DEFAULT_CONNECTION,
            user_metadata: {
                accountType: 'anonymous',
                resetPassword: true,
                invalidEmail: true,
                device_id: deviceId,
            },
        }),
    });

    if (res.ok) {
        const user = await res.json();

        return user;
    }

    throw new HttpError(res.status, res.statusText);
};

export const getOrCreateAnonymousUser = async (deviceId) => {
    let user = await getAnonymousUser(deviceId);

    if (!user) {
        user = await createAnonymousUser(deviceId);
    }

    user = parseUser(user);

    return user;
};
