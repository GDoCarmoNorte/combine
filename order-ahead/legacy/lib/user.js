import fetch from 'isomorphic-unfetch';
import axios from 'axios';

export const parseUser = (user) => {
    const result = { ...user };
    const deviceId = user.user_metadata && user.user_metadata.device_id;
    const identity = deviceId ? user.identities.find((i) => i.user_id === deviceId) : undefined;

    if (identity) {
        result.user_id = `${identity.provider}|${identity.user_id}`;
    }

    return result;
};

export const createProfile = (user) => {
    const {
        name,
        email,
        nickname,
        picture,
        user_id: sub,
        updated_at: updatedAt,
        user_metadata: userMetadata,
    } = parseUser(user);

    const [firstName, lastName] = user.nickname.split(' ');

    return {
        sub,
        name,
        email,
        picture,
        nickname,
        updatedAt,
        user_id: sub,
        firstName: user.firstName || firstName || '',
        lastName: user.lastName || lastName || '',
        ...userMetadata,
    };
};

export const getUser = async (deviceId) => {
    let res = await fetch('/api/me', {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    });

    const user = res.ok ? await res.json() : undefined;

    if (user) {
        user.authorized = true;
        user.user_id = user.sub;

        return user;
    }

    res = await fetch('/api/anonymous', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ deviceId }),
    });

    const anonymous = res.ok ? await res.json() : undefined;

    if (anonymous) {
        anonymous.authorized = false;
    }

    return anonymous;
};

export const updateUser = async (user) => {
    const res = await fetch('/api/account', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ user }),
    });

    const updatedUser = res.ok ? await res.json() : undefined;

    if (updatedUser) {
        updatedUser.authorized = true;

        return updatedUser;
    }

    return Promise.reject(new Error(res.statusText));
};

export const getUserProfile = async () => {
    const res = await fetch('/api/user', {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
    });
    const profile = res.ok ? await res.json() : undefined;

    return profile;
};

export const getOrderHistory = async (user) => {
    try {
        const response = await axios.post('/api/order-history', { user });
        return response.data;
    } catch {
        return null;
    }
};

export const getLoginUrl = (user) => {
    const tokens = [];

    if (user) {
        const { user_id: userId, email } = user;

        tokens.push(`user_id=${userId}`);

        if (email.endsWith('@anonymous.com') === false) {
            tokens.push(`email=${email}`);
        }
    }

    return `/api/login${tokens.length > 0 ? `?${tokens.join('&')}` : ''}`;
};
