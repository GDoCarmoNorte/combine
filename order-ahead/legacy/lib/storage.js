class StorageFallback {
    constructor() {
        const storage = {};

        this.getItem = (key) => storage[key];
        this.setItem = (key, value) => {
            storage[key] = value;
        };
    }
}

const storageFallback = new StorageFallback();

const getStorage = () => (typeof window !== 'undefined' ? window.localStorage : storageFallback);

export const getItem = (key) => {
    let value;
    const storage = getStorage();

    value = storage.getItem(key);
    if (typeof value !== 'undefined') {
        value = JSON.parse(value);
    }

    return value;
};

export const setItem = (key, value) => {
    const storage = getStorage();

    if (typeof value !== 'undefined') {
        value = JSON.stringify(value);
    }

    storage.setItem(key, value);
};
