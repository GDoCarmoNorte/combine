import readCSV from './readCSV';
import path from 'path';

const IMAGE_FILE = path.join(process.cwd(), 'data/bww_images.csv');
const IMAGE_PLACEHOLDER = '/images/bww/bww-logo.svg';

function getImages() {
    let imageData = readCSV(IMAGE_FILE);
    imageData.forEach((p) => (p.imageURL = 'https://www.buffalowildwings.com/' + p.BaseImageName));
    let imagesByProductId = imageData.reduce((p, c) => ((p[c.MenuItemId] = c.imageURL), p), {});

    return imagesByProductId;
}

export default getImages;
