export default function enrichMenu(data, imagesByProductId, productPlaceHolder) {
    const { productDetails, modifierGroupDetails, modifierItemDetails } = data;
    const { subMenus } = data.menu;

    const productMapById = productDetails.reduce((map, product) => {
        return { ...map, [product.productId]: product };
    }, {});

    const subMenusWithProducts = subMenus.filter(
        (subMenu) => (subMenu.products && subMenu.products.length) || (subMenu.menuItems && subMenu.menuItems.length)
    );

    const subMenuMapById = subMenusWithProducts.reduce((obj, subMenu) => {
        return {
            ...obj,
            [subMenu.subMenuId]: processSubMenu({
                subMenu,
                productMapById,
                imagesByProductId,
                productPlaceHolder,
                modifierItemDetails,
                modifierGroupDetails,
            }),
        };
    }, {});

    // remove * prefix - this should be fixed in domain menu.
    data.modifierItemDetails.forEach((item) => (item.name = ('' || item.name).replace(/^\*/, '')));
    return {
        subMenuObj: subMenuMapById,
        modifierGroups: data.modifierGroupDetails,
        modifierItems: data.modifierItemDetails,
        products: data.productDetails,
    };
}

function processSubMenu({
    subMenu,
    imagesByProductId,
    productMapById,
    productPlaceHolder,
    modifierItemDetails,
    modifierGroupDetails,
}) {
    const items = subMenu.products.length > 0 ? subMenu.products : subMenu.menuItems || [];
    const products = items
        .map((product) =>
            processProduct({
                subMenu,
                product,
                imagesByProductId,
                productMapById,
                productPlaceHolder,
                modifierItemDetails,
                modifierGroupDetails,
            })
        )
        .filter((product) => typeof product.isActive === 'undefined' || product.isActive);

    return {
        subMenuId: subMenu.subMenuId,
        subMenu: subMenu.subMenuName,
        products,
    };
}

function processProduct({
    subMenu,
    product,
    imagesByProductId,
    productMapById,
    productPlaceHolder,
    modifierItemDetails,
    modifierGroupDetails,
}) {
    const productDetail = productMapById[product.productID] || {};
    const { defaultSalesItemId } = product;

    const productWithDetails = {
        ...productDetail,
        menuProductId: product.menuProductId || product.id,
        productId: product.productID || product.id,
        subMenuId: subMenu.subMenuId,
        subMenu: subMenu.subMenuName,
        productName: product.productName || product.name,
        name: product.name || product.productName, // TODO - BWW does not have name
        productDescription: product.description,
        isAvailable: product.products
            ? product.products.find((item) => item.id === +defaultSalesItemId).isAvailable
            : null,
        // TODO: replace with valid product image url
        imageURL: imagesByProductId[product.productID || product.id] || productPlaceHolder,
        AllowAddToCartWithoutCustomization: true,
    };

    return processProductModifiers(productWithDetails, modifierGroupDetails, modifierItemDetails);
}

function processProductModifiers(product, modifierGroupDetails, modifierItemDetails) {
    const productModifierGroupsIds = product.modifierGroups || [];

    const productModifierGroups = productModifierGroupsIds
        .map((id) => getModifierGroupById(modifierGroupDetails, id))
        .filter(Boolean)
        .map((group) => ({ ...group }));

    productModifierGroups.forEach((group) => {
        group.modifierItems = getModifierGroupItems(group, modifierItemDetails);
    });

    product.modifierGroupsWithItems = productModifierGroups;

    return product;
}
function getModifierGroupById(modifierGroups, id) {
    return modifierGroups.find((group) => group.modifierGroupId === id);
}

function getModifierGroupItems(modifierGroup, modifierItems) {
    const items = modifierGroup.modifierItems.map((item) => getModifierItemById(modifierItems, item.modifierItemid));
    const filteredItems = items.filter((item) => item && (typeof item.isActive === 'undefined' || item.isActive));
    return filteredItems;
}

function getModifierItemById(modifierItems, id) {
    return modifierItems.find((item) => item.productId === id);
}
