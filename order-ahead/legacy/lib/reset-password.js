import fetch from 'isomorphic-unfetch';

import config from './config';
import { HttpError } from './http-error';
import { getAccessToken } from './auth0-api';

export const resetPassword = async (userId) => {
    const accessToken = await getAccessToken();
    const result = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/tickets/password-change`, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            user_id: userId,
            result_url: `${config.POST_LOGOUT_REDIRECT_URI}profile`,
        }),
    });

    if (result.ok) {
        const data = await result.json();

        console.log('[DEBUG]', data);

        return data;
    }

    throw new HttpError(result.status, result.statusText);
};
