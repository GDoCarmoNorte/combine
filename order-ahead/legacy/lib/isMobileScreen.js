import useWidth from '../hooks/useWidth';

const isMobileScreen = () => useWidth() === 'xs';

export default isMobileScreen;
