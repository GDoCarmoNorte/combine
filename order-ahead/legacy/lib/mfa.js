import fetch from 'isomorphic-unfetch';

import config from './config';
import { HttpError } from './http-error';
import { requestMfa, ANONYMOUS, SEMI_ANONYMOUS } from './account';

const mfaEnrolled = async (ticket) => {
    const { mfa_token: token } = ticket;
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/mfa/authenticators`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Bearer ${token}`,
        },
    });

    if (res.ok) {
        const authenticators = await res.json();
        const authenticator = authenticators.find(
            (item) => item.active && item.authenticator_type === 'oob' && item.oob_channel === 'sms'
        );

        return authenticator;
    }

    throw new HttpError(res.status, res.statusText);
};

const mfaChallenge = async (authenticator, ticket) => {
    const { mfa_token: token } = ticket;
    const { id: authenticatorId } = authenticator;

    const res = await fetch(`https://${config.AUTH0_DOMAIN}/mfa/challenge`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            client_id: config.AUTH0_CLIENT_ID,
            client_secret: config.AUTH0_CLIENT_SECRET,
            challenge_type: 'oob',
            authenticator_id: authenticatorId,
            mfa_token: token,
        }),
    });

    if (res.ok) {
        const enrollment = await res.json();

        return { ...ticket, ...enrollment };
    }

    throw new HttpError(res.status, res.statusText);
};

const mfaEnroll = async (phone, ticket) => {
    let res;
    const authenticator = await mfaEnrolled(ticket);

    if (authenticator) {
        res = await mfaChallenge(authenticator, ticket);

        return { ...res, phone };
    }

    res = await fetch(`https://${config.AUTH0_DOMAIN}/mfa/associate`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${ticket.mfa_token}`,
        },
        body: JSON.stringify({
            authenticator_types: ['oob'],
            oob_channels: ['sms'],
            phone_number: phone,
        }),
    });

    if (res.ok) {
        const result = await res.json();

        return { ...ticket, ...result, phone };
    }

    throw new HttpError(res.status, res.statusText);
};

export const getMfaTicket = async (user) => {
    const { accountType } = user.user_metadata;

    // TODO: for anonymous and semi-anonymous usres for now
    if (accountType !== ANONYMOUS && accountType !== SEMI_ANONYMOUS) {
        throw new HttpError(400, 'Bad request');
    }

    const updatedUser = await requestMfa(user);
    const {
        email,
        user_id: userId,
        user_metadata: { phone_number: phone },
    } = updatedUser;
    const password = userId.replace('auth0|', '');

    const res = await fetch(`https://${config.AUTH0_DOMAIN}/oauth/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: `grant_type=password&client_id=${config.AUTH0_CLIENT_ID}&client_secret=${config.AUTH0_CLIENT_SECRET}&username=${email}&password=${password}&scope=${config.AUTH0_SCOPE}`,
    });

    if (res.ok) {
        const session = await res.json();

        return { ...user, session };
    }

    if (res.status === 403) {
        const mfa = await res.json();
        const mfaTicket = await mfaEnroll(phone, mfa);

        return mfaTicket;
    }

    throw new HttpError(res.status, res.statusText);
};

export const verifyMfaTicket = async (user, ticket) => {
    const { oob_code: oobCode, code: smsCode, mfa_token: token } = ticket;
    const { phone_number: phone } = user.user_metadata;

    if (typeof oobCode === 'undefined' || typeof smsCode === 'undefined' || typeof token === 'undefined') {
        throw new HttpError(400, 'Bad request');
    }

    const res = await fetch(`https://${config.AUTH0_DOMAIN}/oauth/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: `Bearer ${token}`,
        },
        body: `grant_type=http://auth0.com/oauth/grant-type/mfa-oob&client_id=${config.AUTH0_CLIENT_ID}&client_secret=${config.AUTH0_CLIENT_SECRET}&mfa_token=${token}&oob_code=${oobCode}&binding_code=${smsCode}`,
    });

    if (res.ok) {
        const session = await res.json();

        return { ...user, session };
    }

    throw new HttpError(res.status, res.statusText);
};
