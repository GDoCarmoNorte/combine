import jwtDecode from 'jwt-decode';

import { HttpError } from './http-error';
import { getSession } from './auth0-api';

function verifyOTPSession(user) {
    let isVerified;

    try {
        const otpUser = jwtDecode(user.session.id_token);

        const authorizedUserToken = user.user_id;
        const otpUserToken = otpUser.sub;
        const isUserValid = authorizedUserToken === otpUserToken;

        const otpUserTokenExpireTime = otpUser.exp * 1000;
        const isTokenExpired = new Date(otpUserTokenExpireTime) < new Date();

        isVerified = isUserValid && !isTokenExpired;
    } catch (error) {
        isVerified = false;
    }

    return isVerified;
}

async function verifyAuthorizedSession(request) {
    const session = await getSession(request);
    return !!session;
}

export const verifySession = async (request) => {
    let isVerified;
    const { user } = request.body;

    if (typeof user === 'undefined') {
        throw new HttpError(400, 'Bad request');
    }

    if (user.session) {
        isVerified = verifyOTPSession(user);
    } else {
        isVerified = await verifyAuthorizedSession(request);
    }

    if (!isVerified) {
        throw new HttpError(401, 'Unauthorized');
    }

    return isVerified;
};
