import { v4 as uuid } from 'uuid';

import { getItem, setItem } from './storage';

const DEVICE_ID_ATTR = '_deviceId';

export const getDeviceId = () => {
    let deviceId = getItem(DEVICE_ID_ATTR);

    if (!deviceId) {
        deviceId = uuid();
        setItem(DEVICE_ID_ATTR, deviceId);
    }

    return deviceId;
};
