import axios from 'axios';
import enrichMenu from './enrichMenu';
import getImages from './bwwImages';
const BWW_MENU_URL = `${process.env.MENU_DOMAIN_SERVICE_URL}/menu/1000?includeDetails=true`;
const PRODUCT_PLACEHOLDER_URL = '/images/bww/bww-logo.svg';

export default async function getMenu() {
    const menuResponse = await axios.get(BWW_MENU_URL);
    const imagesByProductId = getImages();
    const processedMenu = enrichMenu(menuResponse.data, imagesByProductId, PRODUCT_PLACEHOLDER_URL);
    return processedMenu;
}
