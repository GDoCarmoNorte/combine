import fetch from 'isomorphic-unfetch';

import config from './config';
import { HttpError } from './http-error';
import { getAccessToken } from './auth0-api';

const UPDATE_ATTRS = [
    'app_metadata',
    'blocked',
    'email',
    'email_verified',
    'family_name',
    'given_name',
    'name',
    'nickname',
    // 'phone_number',
    // 'phone_verified',
    'picture',
    'username',
    'verify_email',
];

export const ANONYMOUS = 'anonymous';
export const SEMI_ANONYMOUS = 'semi-anonymous';

const getUserMetadata = (user) => {
    const isAnonymous = user.email.endsWith('@anonymous.com');
    const accountType = isAnonymous ? ANONYMOUS : SEMI_ANONYMOUS;

    return { ...user.user_metadata, accountType, invalidEmail: isAnonymous };
};

const getUpdateOptions = (user) => {
    const userMetadata = getUserMetadata(user);
    const options = {};

    UPDATE_ATTRS.forEach((attr) => {
        if (user[attr]) {
            options[attr] = user[attr];
        }
    });

    if (user.name && user.name.endsWith('@anonymous.com')) {
        options.name = user.email;
    }
    if (user.username && user.username.endsWith('@anonymous.com')) {
        options.username = user.email;
    }

    options.user_metadata = userMetadata;
    if (user.phone) {
        options.user_metadata.phone_number = user.phone;
    }

    return options;
};

export const getAccount = async (user) => {
    const accessToken = await getAccessToken();

    if (typeof user.user_id === 'undefined') {
        throw new HttpError(400, 'Bad request');
    }

    const tokens = user.user_id.split('|');
    const userId = tokens[tokens.length - 1];
    const res = await fetch(
        `https://${config.AUTH0_DOMAIN}/api/v2/users?search_engine=v3&q=identities.user_id:${userId}`,
        {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        }
    );

    if (res.ok) {
        const [result] = await res.json();

        return result;
    }

    // if (res.status === 404) {
    //     return undefined;
    // }

    throw new HttpError(res.status, res.statusText);
};

export const findAccount = async (email) => {
    const accessToken = await getAccessToken();
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users?search_engine=v3&q=email:${email}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    });

    if (res.ok) {
        const [user] = await res.json();

        return user;
    }

    throw new HttpError(res.status, res.statusText);
};

export const updateAccount = async (user) => {
    if (typeof user.user_id === 'undefined') {
        throw new HttpError(400, 'Bad request');
    }

    const accessToken = await getAccessToken();
    const account = await getAccount({ user_id: user.user_id });

    if (typeof account === 'undefined') {
        throw new HttpError(404, 'Not found');
    }

    const res = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users/${account.user_id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
        },
        body: JSON.stringify(getUpdateOptions(user)),
    });

    if (res.ok) {
        const updatedUser = await res.json();

        return updatedUser;
    }

    throw new HttpError(res.status, res.statusText);
};

export const requestMfa = async (user) => {
    if (typeof user.user_id === 'undefined') {
        throw new HttpError(400, 'Bad request');
    }

    // see auth0 rule for details - https://manage.auth0.com/dashboard/us/poc-inspirebrands/rules/rul_eL95Ygts91xuDoxp
    const accessToken = await getAccessToken();
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users/${user.user_id}`, {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
        },
        body: JSON.stringify({
            app_metadata: {
                groups: 'MFArequired',
            },
        }),
    });

    if (res.ok) {
        const updatedUser = await res.json();

        return updatedUser;
    }

    throw new HttpError(res.status, res.statusText);
};

export const linkAccounts = async (userId, options) => {
    const { user_id: secondaryUserId, provider: secondaryProvider } = options;

    if (!userId || !secondaryUserId || !secondaryProvider) {
        throw new HttpError(400, 'Bad request');
    }

    const accessToken = await getAccessToken();
    const res = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users/${userId}/identities`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
        },
        body: JSON.stringify({
            user_id: secondaryUserId,
            provider: secondaryProvider,
        }),
    });

    if (res.ok) {
        const result = await res.json();

        return result;
    }

    throw new HttpError(res.status, res.statusText);
};
