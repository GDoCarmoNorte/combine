import fetch from 'isomorphic-unfetch';

export const getCardBalance = async (cardNumber, pin, brand) => {
    const res = await fetch('/api/card-balance', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            brand,
            cardNumber,
            pin,
        }),
    });

    const balance = res.ok ? await res.json() : undefined;

    return balance;
};
