const parse = require('csv-parse/lib/sync');
const fs = require('fs');

export default function readCSV(fileName) {
    //console.log('Reading file',fileName);
    const csv = fs.readFileSync(fileName, 'utf8');
    const data = parse(csv + '', { columns: true });
    return data;
}
