import axios from 'axios';
import enrichMenu from './enrichMenu';
import getImages from './arbysImages';
import dynamicPricing from './dynamicPricing';

const ARBYS_MENU_URL = `${process.env.MENU_DOMAIN_SERVICE_URL}/menu/674389404?includeDetails=true`;
const PRODUCT_PLACEHOLDER_URL = '/images/arbys/arbys-logo.svg';

export default async function getMenu() {
    const menuResponse = await axios.get(ARBYS_MENU_URL);
    const user = { customerAge: 30 };
    const location = { busyIndex: 30 };
    const imagesByProductId = getImages();
    menuResponse.data.productDetails = await dynamicPricing(menuResponse.data, user, location);
    const processedMenu = enrichMenu(menuResponse.data, imagesByProductId, PRODUCT_PLACEHOLDER_URL);
    return processedMenu;
}
