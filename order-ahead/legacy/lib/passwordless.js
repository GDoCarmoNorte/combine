import auth0 from 'auth0-js';
import axios from 'axios';

import config from './config';

// Client only API

const getRedirectUrl = (targetUrl) => {
    const { protocol, host } = window.location;

    return `${protocol}//${host}/api/login${targetUrl ? `?target_url=${targetUrl}` : ''}`;
};

const getEmailOptions = ({ email }) => {
    return {
        email,
        send: 'code',
        connection: 'email',
    };
};

const createWebAuth = (targetUrl) => {
    return new auth0.WebAuth({
        domain: config.AUTH0_DOMAIN,
        clientID: config.AUTH0_N_CLIENT_ID,
        responseType: 'token id_token',
        redirectUri: getRedirectUrl(targetUrl),
    });
};

export const passwordlessPrepare = async (user) => {
    return axios
        .post(
            '/api/passwordless',
            { user },
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then((res) => res.data);
};

const startByEmail = (user) => {
    const webAuth = createWebAuth();
    const options = getEmailOptions(user);

    return new Promise((resolve, reject) => {
        webAuth.passwordlessStart(options, (error, result) => {
            if (error) {
                reject(error);
            } else {
                resolve({ user, ticket: result });
            }
        });
    });
};

const startByMfa = (user) => {
    return axios
        .post(
            '/api/mfa',
            { user },
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then((res) => ({ user, ticket: res.data }));
};

export const passwordlessStart = async (user) => {
    return passwordlessPrepare(user).then((updatedUser) => {
        console.log('[DEBUG]', 'passwordless prepare', updatedUser);

        if (updatedUser.session) {
            return { user: updatedUser };
        }

        // check the requested type of authentication
        const { phone } = user;
        const passwordlessFn = phone ? startByMfa : startByEmail;

        return passwordlessFn(updatedUser);
    });
};

const verifyByEmail = (user, ticket) => {
    return axios
        .put(
            '/api/passwordless',
            { user, ticket },
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then((res) => res.data);
};

const verifyByMfa = (user, ticket) => {
    return axios
        .put(
            '/api/mfa',
            { user, ticket },
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then((res) => res.data);
};

export const passwordlessVerify = async (user, ticket) => {
    const verifyFn = user.phone ? verifyByMfa : verifyByEmail;

    return verifyFn(user, ticket);
};
