import React, { useContext } from 'react';
import Link from 'next/link';

import styles from './itemGrid.module.css';

import { StoreContext } from '../state/store';

export default function ItemGrid(props) {
    // console.log(props.items.categories);

    const { brand } = useContext(StoreContext);
    const flex = brand.brand !== 'ARBYS';

    return (
        <>
            <div className={styles.gridContainer}>
                {props.items.menu.map((item) => (
                    <div className={flex ? styles.gridItemFlex : styles.gridItem} key={item.productId}>
                        <span
                            className={flex ? styles.titleFlex : styles.title}
                            dangerouslySetInnerHTML={{ __html: item.productName }}
                        ></span>
                        <img className={flex ? styles.gridImageFlex : styles.gridImage} src={item.imageURL}></img>
                    </div>
                ))}
            </div>
        </>
    );
}
