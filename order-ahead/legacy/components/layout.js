import React from 'react';
import OAHeader from './oaHeader';

import styles from './layout.module.css';

const Layout = (props) => {
    return (
        <div className={styles.layout}>
            <OAHeader location={props.location} />
            <main className={styles.container}>{props.children}</main>
        </div>
    );
};

export default Layout;
