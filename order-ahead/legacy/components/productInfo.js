import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import styles from './productInfo.module.css';

export default function ProductInfoPopup({ product, includedItems, open, onClose }) {
    const { productId, productName, productDescription, imageURL } = product;
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));
    return (
        <div>
            <Dialog fullScreen={fullScreen} classes={{ paper: styles.popup }} open={open} onClose={onClose}>
                <MuiDialogTitle disableTypography className={styles.dialogTitle}>
                    <Typography align="center" variant="h6" color="primary">
                        {productName}
                    </Typography>
                </MuiDialogTitle>
                <MuiDialogContent dividers>
                    <div className={styles.imageDiv}>
                        <img className={styles.image} src={imageURL}></img>
                    </div>
                    <div className={styles.productInfo}>
                        {includedItems && includedItems.length > 0 && (
                            <div className={styles.includedItems}>
                                {includedItems.map((item) => (
                                    <Typography variant="button" align="right" key={item.itemId}>
                                        {item.product.name}
                                    </Typography>
                                ))}
                            </div>
                        )}
                        {productDescription && (
                            <Typography variant="body2" className={styles.productDescription}>
                                {productDescription}
                            </Typography>
                        )}
                    </div>
                </MuiDialogContent>
                <MuiDialogActions>
                    <Button onClick={onClose} color="primary" startIcon={<CloseIcon />}>
                        Close
                    </Button>
                </MuiDialogActions>
            </Dialog>
        </div>
    );
}

ProductInfoPopup.propTypes = {
    productId: PropTypes.string,
    productName: PropTypes.string,
    productDescription: PropTypes.string,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};
