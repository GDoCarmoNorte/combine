import React from 'react';

const button = (props) => {
    return (
        <div>
            <button className="customButton test" onClick={props.onClick}>
                {props.children}
            </button>
            <style jsx>
                {`
                    .customButton {
                        margin-top: 10px;
                        margin-left: 5px;
                        font-size: 1rem;
                        overflow: visible;
                        outline: none;
                        touch-action: manipulation;
                        border-radius: 5px;
                    }
                    .test {
                        text-transform: uppercase;
                        border-right: none;
                        border-bottom: none;
                        -webkit-box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
                        box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
                        -webkit-transition: all 0.2s;
                        -o-transition: all 0.2s;
                        transition: all 0.2s;
                        background-image: -webkit-radial-gradient(circle, #2196f3 80%, #0d87e9 81%);
                        background-image: -o-radial-gradient(circle, #2196f3 80%, #0d87e9 81%);
                        background-image: radial-gradient(circle, #2196f3 80%, #0d87e9 81%);
                        background-repeat: no-repeat;
                        -webkit-background-size: 200% 200%;
                        background-size: 200%;
                        background-position: 50%;
                        -webkit-transition: background-size 2s;
                        -o-transition: background-size 2s;
                        transition: background-size 2s;
                        color: var(--col--light);
                        background-color: var(--button-color);
                        border-color: transparent;
                        display: inline-block;
                        margin-bottom: 0;
                        font-weight: normal;
                        text-align: center;
                        vertical-align: middle;
                        -ms-touch-action: manipulation;
                        touch-action: manipulation;
                        cursor: pointer;
                        background-image: none;
                        border: 1px solid transparent;
                        white-space: nowrap;
                        padding: 6px 16px;
                        font-size: 13px;
                        line-height: 1.846;
                        border-radius: 3px;
                        -webkit-user-select: none;
                        -moz-user-select: none;
                        -ms-user-select: none;
                        user-select: none;
                    }
                `}
            </style>
        </div>
    );
};

export default button;
