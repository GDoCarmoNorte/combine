import React from 'react';

const icon = function () {
    var width = 448;
    var height = 512;
    var svgPathData =
        'M 11 5 H 9 V 9 H 5 V 11 H 9 V 15 H 11 V 11 H 15 V 9 H 11 V 5 Z M 10 0 C 4.48 0 0 4.48 0 10 C 0 15.52 4.48 20 10 20 C 15.52 20 20 15.52 20 10 C 20 4.48 15.52 0 10 0 Z M 10 18 C 5.59 18 2 14.41 2 10 C 2 5.59 5.59 2 10 2 C 14.41 2 18 5.59 18 10 C 18 14.41 14.41 18 10 18 Z';

    return (
        <span>
            <svg viewBox="0,0,20,20" className="svg-inline">
                <path fill="currentcolor" d={svgPathData}></path>
            </svg>
            <style jsx>{`
                svg.svg-inline {
                    overflow: visible;
                    display: inline-block;
                    font-size: inherit;
                    height: 15px;
                    width: 15px;
                    vertical-align: -0.125em;
                }
            `}</style>
        </span>
    );
};

export default icon;
