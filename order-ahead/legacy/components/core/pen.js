import React from 'react';

const icon = function () {
    var svgPathData =
        'M 2 11.5002 V 14.0002 H 4.5 L 11.8733 6.62687 L 9.37333 4.12687 L 2 11.5002 Z M 13.8067 4.69354 C 14.0667 4.43354 14.0667 4.01354 13.8067 3.75354 L 12.2467 2.19354 C 11.9867 1.93354 11.5667 1.93354 11.3067 2.19354 L 10.0867 3.41354 L 12.5867 5.91354 L 13.8067 4.69354 Z';
    return (
        <span>
            <svg viewBox="0,0,18,18" className="svg-inline">
                <path fill="currentcolor" d={svgPathData}></path>
            </svg>
            <style jsx>{`
                svg.svg-inline {
                    overflow: visible;
                    display: inline-block;
                    font-size: inherit;
                    height: 20px;
                    width: 20px;
                    vertical-align: -0.125em;
                }
            `}</style>
        </span>
    );
};

export default icon;
