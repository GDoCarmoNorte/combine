import React from 'react';
import Layout from './layout';

export default function withLayout(Component) {
    const Wrapper = (props) => (
        <Layout>
            <Component {...props} />
        </Layout>
    );

    Wrapper.WrappedComponent = Component;
    Wrapper.displayName = `withLayout(${Component.displayName || Component.name || 'Unknown'})`;

    return Wrapper;
}
