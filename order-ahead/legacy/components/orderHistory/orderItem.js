import React from 'react';
import Button from '@material-ui/core/Button';

import Paper from '@material-ui/core/Paper';
import { List, ListItem, ListItemText } from '@material-ui/core';

import styles from './index.module.css';

export default function OrderItem({ order, onReorder }) {
    return (
        <li>
            <Paper className={styles.orderItem} variant="outlined">
                <div className={styles.orderItemHeader}>
                    <h5>
                        {`${order.id} - ${order.destination} - ${new Date(order.deliveryDate).toLocaleDateString()} - ${
                            order.orderStatus
                        } - ${order.orderId}`}
                    </h5>
                    <Button color="primary" variant="outlined" onClick={onReorder}>
                        Reorder
                    </Button>
                </div>
                <List dense={true} className={styles.orderItemProductList}>
                    {order.items.map((i, index) => (
                        <ListItem key={order.id + i.itemId}>
                            <ListItemText primary={i.description} secondary={`qty ${i.quantity}, $${i.price}`} />
                        </ListItem>
                    ))}
                </List>
            </Paper>
        </li>
    );
}
