import React from 'react';
import { Typography } from '@material-ui/core';

import OrderItem from './orderItem';
import styles from './index.module.css';

export default function OrderHistory({ orders, onReorder }) {
    const reorder = (id) => () => onReorder(id);

    return (
        <div className={styles.container}>
            <Typography variant="h5" className={styles.title}>
                Last Orders
            </Typography>
            <ul className={styles.orderList}>
                {orders.map((order) => (
                    <OrderItem key={order.id} order={order} onReorder={reorder(order.id)} />
                ))}
            </ul>
        </div>
    );
}
