import React, { useState } from 'react';

import { Paper, InputBase, IconButton } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import isMobileScreen from '../../lib/isMobileScreen';

import styles from './index.module.css';

const locationSelection = function ({
    headerLabel,
    inputPlaceholder,
    defaultLocationString,
    onLocationSearch,
    onLocationSelect,
    renderLocationList,
    renderMap,
}) {
    const [locationList, setlocationList] = useState([]);

    let currentValue = null;
    const displayMap = !isMobileScreen();

    const handleButtonClick = async () => {
        // Update the keyword of the input element
        currentValue = currentValue || defaultLocationString;
        console.log('place', currentValue);
        const locations = await onLocationSearch(currentValue);
        if (locations && locations.length > 0) {
            setlocationList(locations);
        } else {
            setlocationList([]);
        }
    };

    const handleSearchEnter = async (e) => {
        if (e.key === 'Enter') {
            handleButtonClick(e);
        }
    };

    const handleSearchInput = (e) => {
        currentValue = e.target.value;
    };

    return (
        <div className={styles.locations}>
            <div className={styles.left}>
                <h5 className={styles.searchTitle}>{headerLabel}</h5>
                <div>
                    <Paper className={styles.searchBox} variant="outlined">
                        <InputBase
                            onKeyPress={handleSearchEnter}
                            className={styles.searchInput}
                            placeholder={inputPlaceholder}
                            onChange={handleSearchInput}
                        />
                        <IconButton onClick={handleButtonClick}>
                            <SearchIcon />
                        </IconButton>
                    </Paper>
                </div>
                <div>{renderLocationList(locationList, onLocationSelect)}</div>
            </div>
            {displayMap && <div className={styles.right}>{renderMap(locationList, onLocationSelect)}</div>}
            <style jsx global>{`
                ul.mapList {
                    list-style: none;
                    padding-inline-start: 10px;
                }

                li.mapListItemWrap {
                    padding: 20px;
                    background: #fff;
                    position: relative;
                    list-style: none;
                    box-shadow: 0 5px 11px -8px rgba(0, 0, 0, 0.24), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
                    border-radius: 6px;
                    margin-bottom: 20px;
                }

                .locationName {
                    font-family: var(--brand-font-family);
                    color: #231f20 !important;
                    font-size: 18px !important;
                    font-weight: 700;
                    text-transform: uppercase;
                    margin-bottom: 5px;
                    display: inline-block;
                    padding-right: 40px;
                }

                .distance {
                    font-size: 12px;
                    position: absolute;
                    right: 20px;
                    bottom: 20px;
                    line-height: 20px;
                }

                .hoursStatus.isOpen {
                    color: #349827;
                }
                .hoursStatus {
                    color: #231f20;
                    font-size: 15px;
                    outline: 0;
                    font-family: var(--brand-font-family);
                }
                .mb10 {
                    margin-bottom: 10px !important;
                }

                .locationClosureMessage {
                    padding: 10px;
                    color: #f31431;
                }

                .locationAlertMessage {
                    background-color: #f6f6f6;
                    font-size: 13px;
                    font-family: Helvetica;
                    line-height: 16px;
                    color: #333;
                    background-image: url(https://locations.arbys.com/images/alert.svg);
                    background-size: 30px auto;
                    background-position: left center;
                    background-repeat: no-repeat;
                    padding: 15px 10px 15px 35px;
                }

                .address {
                    margin-top: 15px;
                    font-size: 15px;
                    line-height: 18px;
                    outline: 0;
                }
                [data-hide-empty=''] {
                    display: none;
                }

                .mapListLinks {
                    margin-top: 5px;
                    color: gray;
                    font-size: 0.7rem;
                }
            `}</style>
        </div>
    );
};

export default locationSelection;
