import React from 'react';
import { Button } from '@material-ui/core';
// InfoWindow component
import styles from './tooltip.module.css';

const Tooltip = (props) => {
    const { place, onLocationSelect } = props;
    function onClick(e) {
        onLocationSelect(place);
    }
    return (
        <div className={styles.info}>
            <a
                className={styles.locationName}
                href={place.url}
                title={[place.LocationName, place.city, place.LocationId].join(' ')}
            >
                {' '}
                <span> {place.LocationName}</span>
            </a>
            <div className={styles.orderButton}>
                <Button variant="contained" onClick={onClick}>
                    Order Pickup
                </Button>
            </div>
            <div className={styles.distance} aria-label={`This location is ${place.Distance} miles away`}>
                <span>{place.Distance.toLocaleString()} mi</span>
            </div>

            <div
                className={styles.address}
                aria-label="This location is located at {place.AddressLine1} {place.AddressLine1}, {place.city}, {place.region} {place.post_code}"
            >
                <div>
                    {place.AddressLine1} {place.AddressLine2}
                </div>
                <div>
                    {place.City}, {place.State} {place.PostalCode}
                </div>
            </div>
            <div className={`${styles.mapListLinks} ${styles.mt - 10}`}>
                <div>Store ID: {place.LocationId}</div>
                <a
                    className={`${styles.gaLink} ${styles.phone}`}
                    title="Click to call"
                    alt="Call Store"
                    href="tel:{place.local_phone_pn_dashes}"
                    data-ga={`Maplist, Phone - ${place.LocationId}`}
                >
                    {place.Phone}
                </a>
                <a
                    className={`${styles.gaLink} ${styles.directions} ${styles.button}`}
                    href={`https://www.google.com/maps?hl=en&saddr=current+location&daddr=${place.lat},${place.lng}`}
                    title="Get directions"
                    data-ga="Maplist, Get Directions - {place.LocationId}"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Directions
                </a>
            </div>

            <div className={styles.clearfix}></div>
        </div>
    );
};

export default Tooltip;
