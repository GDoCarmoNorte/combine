import React, { useState, useEffect, Fragment } from 'react';
import GoogleMapReact from 'google-map-react';

import styles from './map.module.css';

import Tooltip from './tooltip';
import useWidth from '../../../hooks/useWidth';

// TODO: move to env and SSR
const API_KEY = 'AIzaSyCgLpOWiCLPazClgP41G6mKQV78OEFamoU';

const defaultProps = {
    center: { lat: 37.65, lng: -99.38 },
    zoom: 4.2,
};
const zoomMap = {
    xs: 3.2,
    sm: 3.8,
    md: 4,
};

// InfoWindow component
const InfoWindow = (props) => {
    const { place, onLocationSelect } = props;

    function pickStore(pickPlace) {
        return (e) => {
            console.log('Place is selected', pickPlace);

            e.stopPropagation();
            onLocationSelect(pickPlace);
        };
    }

    return (
        <div className={styles.infoContainer}>
            <div className={styles.info}>
                <Tooltip place={place} onLocationSelect={onLocationSelect} />
            </div>
        </div>
    );
};

// Marker component
const Marker = (props) => {
    // TODO: move to css / scss
    const markerBackground = {
        userSelect: 'none',
        width: '30px',
        border: '0px',
        cursor: 'pointer',
    };
    const markerStyle = {
        border: '1px solid white',
        borderRadius: '50%',
        height: 10,
        width: 10,
        cursor: 'pointer',
        zIndex: 10,
        backgroundColor: 'red',
    };

    return (
        <Fragment>
            <img
                style={markerBackground}
                src={`/brands/arbys/location.png`}
                onClick={(e) => {
                    e.stopPropagation();
                    props.onLocationSelect(props.place);
                }}
            />
            {/* {props.place.show && <InfoWindow place={props.place} onLocationSelect={props.onLocationSelect} />} */}
        </Fragment>
    );
};

const MyMap = function (props) {
    const list = props.locations || [];
    const { onLocationSelect } = props;
    const [locationList, setLocationList] = useState([...list]);
    useEffect(() => {
        setLocationList([...list]);
    }, [list]);
    const [api, setAPI] = useState(null);
    const [map, setMap] = useState(null);
    console.log(locationList);

    const width = useWidth();
    const zoom = zoomMap[width] || defaultProps.zoom;

    const apiIsLoaded = (map, maps, places) => {
        setAPI(maps);
        setMap(map);
        if (places.length === 0) return;
        // Get bounds by our places
        const bounds = getMapBounds(maps, places);
        // Fit map to bounds
        map.fitBounds(bounds);
        // Bind the resize listener
        // bindResizeListener(map, maps, bounds);
    };
    const resetBounds = () => {
        if (api && locationList.length > 0) {
            const bounds = getMapBounds(api, locationList);
            map.fitBounds(bounds);
        }
    };
    const getMapBounds = (maps, places) => {
        const bounds = new maps.LatLngBounds();
        places.forEach((place) => {
            bounds.extend(new maps.LatLng(place.lat, place.lng));
        });
        return bounds;
    };

    resetBounds();

    // onChildClick callback can take two arguments: key and childProps
    const onChildClickCallback = (key) => {
        console.log(key);
        locationList.forEach((s) => {
            s.show = false;
        });
        const location = locationList.find((s) => s.LocationId === key);
        if (location) {
            location.show = !location.show;
        }
        setLocationList([...locationList]);
        return { locationList };
    };

    return (
        <GoogleMapReact
            bootstrapURLKeys={{ key: API_KEY }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
            zoom={zoom}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps, locationList)}
            onChildClick={onChildClickCallback}
            options={createMapOptions}
        >
            {(locationList || []).map((place) => (
                <Marker
                    key={place.LocationId}
                    lat={place.lat}
                    lng={place.lng}
                    place={place}
                    onLocationSelect={onLocationSelect}
                />
            ))}
        </GoogleMapReact>
    );
};

function createMapOptions() {
    return {
        disableDefaultUI: true,
        gestureHandling: 'cooperative',
        styles: [
            {
                featureType: 'administrative',
                elementType: 'all',
                stylers: [
                    {
                        saturation: '-100',
                    },
                ],
            },
            {
                featureType: 'administrative.province',
                elementType: 'all',
                stylers: [
                    {
                        visibility: 'off',
                    },
                ],
            },
            {
                featureType: 'landscape',
                elementType: 'all',
                stylers: [
                    {
                        saturation: -100,
                    },
                    {
                        lightness: 65,
                    },
                    {
                        visibility: 'on',
                    },
                ],
            },
            {
                featureType: 'poi',
                elementType: 'all',
                stylers: [
                    {
                        saturation: -100,
                    },
                    {
                        lightness: '50',
                    },
                    {
                        visibility: 'simplified',
                    },
                ],
            },
            {
                featureType: 'road',
                elementType: 'all',
                stylers: [
                    {
                        saturation: '-100',
                    },
                ],
            },
            {
                featureType: 'road.highway',
                elementType: 'all',
                stylers: [
                    {
                        visibility: 'simplified',
                    },
                ],
            },
            {
                featureType: 'road.arterial',
                elementType: 'all',
                stylers: [
                    {
                        lightness: '30',
                    },
                ],
            },
            {
                featureType: 'road.local',
                elementType: 'all',
                stylers: [
                    {
                        lightness: '40',
                    },
                ],
            },
            {
                featureType: 'transit',
                elementType: 'all',
                stylers: [
                    {
                        saturation: -100,
                    },
                    {
                        visibility: 'simplified',
                    },
                ],
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [
                    {
                        hue: '#ffff00',
                    },
                    {
                        lightness: -25,
                    },
                    {
                        saturation: -97,
                    },
                ],
            },
            {
                featureType: 'water',
                elementType: 'labels',
                stylers: [
                    {
                        lightness: -25,
                    },
                    {
                        saturation: -100,
                    },
                ],
            },
        ],
    };
}
export default MyMap;
