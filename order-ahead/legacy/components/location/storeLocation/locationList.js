import React from 'react';

import StoreTooltip from './locationToolTip';

import styles from './locationList.module.css';

export default function LocationList(props) {
    const { locationList = [], onLocationSelect } = props;

    console.log('[DEBUG]', __filename, locationList);

    return (
        <ul className={styles.mapList}>
            {locationList.map((place) => (
                <li className={styles.mapListItemWrap} key={place.LocationId}>
                    <StoreTooltip place={place} onLocationSelect={onLocationSelect} />
                </li>
            ))}
        </ul>
    );
}
