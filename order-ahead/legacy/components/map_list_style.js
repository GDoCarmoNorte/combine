const map_list_style = `
<li class="mapListItemWrap map-list-item-wrap" data-fid="{{fid}}" data-particles="" data-lid="{{lid}}" data-specialties="" id="lid{{lid}}">
    <div class="mapListItem map-list-item">
        
        <a href="{{url}}" title="{{location_name}} {{city}} #{{fid}}" class="location-name ga-link" data-ga="Maplist, Location Link - {{fid}}">
            <span>{{location_name}} {{city}}</span>
        </a>

        <div class="distance" aria-label="This location is {{distance}} miles away"><span>{{distance}} mi</span></div>

        <div class="hoursStatus hours-status status-primary-{{lid}} mb10 mb-10" data-hide-not-empty="{{location_closure_message}}" aria-label="Opening Hours for this store"></div>
 
        <div class="locationClosureMessage location-closure-message" data-hide-empty="{{location_closure_message}}">{{location_closure_message}}</div>
        <div class="locationAlertMessage location-alert-message" data-hide-empty="{{location_alert_message}}">{{location_alert_message}}</div>

        <div class="address" aria-label="This location is located at {{address_1}} {{address_2}}, {{city}}, {{region}} {{post_code}}">
            <div>{{address_1}} {{address_2}}</div>
            <div>{{city}}, {{region}} {{post_code}}</div>
        </div>
        <div class="mapListLinks map-list-links mt-10">
            <div>Store ID: {{fid}}</div>
            <a class="gaLink phone ga-link" title="Click to call" alt="Call Store" href="tel:{{local_phone_pn_dashes}}" data-ga="Maplist, Phone - {{fid}}">{{local_phone}}</a>
            <a class="gaLink directions ga-link button" href="https://www.google.com/maps?hl=en&saddr=current+location&daddr={{lat}},{{lng}}" title="Get directions" data-ga="Maplist, Get Directions - {{fid}}" target="_blank" rel="noopener">Directions</a>
            <a href="{{url}}" title="Click to view more details" class="ga-link button" data-ga="Maplist, Location Link - {{fid}}">More Details</a>
        </div>
        <div class="clearfix"></div>
    </div>
</li>
`;

export default map_list_style;
