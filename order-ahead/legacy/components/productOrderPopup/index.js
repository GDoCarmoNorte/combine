import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';

import CloseIcon from '@material-ui/icons/Close';

import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import { StoreContext } from '../../state/store';
import * as gtag from '../../lib/gtag';
import { formatPrice } from '../../lib/format';

import styles from './index.module.css';

import ModifierGroup from './modifierGroup';
import ModifierItems from './modifierItems';

import useDomainProductPriceTier from '../../hooks/useDomainProductPriceTier';
import useDomainProductOrderForm from '../../hooks/useDomainProductOrderForm';

export default function ProductOrderPopup({ product, open, onClose }) {
    if (!product) return null;

    const {
        bag: { addToBag },
        user: [user],
    } = useContext(StoreContext);
    const [priceTier] = useDomainProductPriceTier(product);
    const [modifiersWithValue, handleModifierSelect] = useDomainProductOrderForm(product);
    const [quantity, setQuantity] = useState(1);

    useEffect(() => {
        if (product) {
            setQuantity(1);
        }
    }, [product]);

    const onModifierSelect = (groupId) => (modifier) => () => {
        handleModifierSelect(groupId, modifier);
    };

    const onAddToBag = () => {
        gtag.event({
            action: `${product.productId} item added to cart`,
            category: 'ecommerce',
            label: 'Add to cart',
            value: {
                user_id: user.user_id,
                product_id: product.productId,
            },
        });

        addToBag(product, modifiersWithValue, priceTier, quantity);
        onClose();
    };
    const onAddQty = () => {
        if (quantity < 99) setQuantity(quantity + 1);
    };
    const onRemoveQty = () => {
        if (quantity > 0) setQuantity(quantity - 1);
    };
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

    return (
        <div>
            <Dialog open={open} fullScreen={fullScreen} fullWidth onClose={onClose}>
                <MuiDialogTitle disableTypography className={styles.dialogTitle}>
                    <Typography variant="h5" color="primary">
                        {product.productName}
                    </Typography>
                    <IconButton className={styles.dialogTitleButton} onClick={onClose} color="primary">
                        <CloseIcon />
                    </IconButton>
                </MuiDialogTitle>
                <MuiDialogContent dividers>
                    <div className={styles.imageDiv}>
                        <img className={styles.image} src={product.imageURL}></img>
                    </div>
                    {product.productDescription && (
                        <Typography variant="body2" className={styles.productDescription}>
                            {product.productDescription}
                        </Typography>
                    )}
                    {modifiersWithValue &&
                        modifiersWithValue.map((group) => (
                            <ModifierGroup
                                key={group.modifierGroupId}
                                groupName={group.name}
                                className={styles.modifierGroup}
                            >
                                <ModifierItems
                                    items={group.modifierItems}
                                    value={group.selectedModifier && group.selectedModifier.productId}
                                    onChange={onModifierSelect(group.modifierGroupId)}
                                />
                            </ModifierGroup>
                        ))}
                </MuiDialogContent>
                <MuiDialogActions>
                    <Grid container direction="row" justify="space-between" alignItems="center">
                        <Grid item>
                            <Grid container direction="row" justify="flex-start" alignItems="center">
                                <IconButton className={styles.removeButton} onClick={onRemoveQty}>
                                    <RemoveIcon />
                                </IconButton>
                                <div className={styles.qty}>{quantity}</div>
                                <IconButton className={styles.addButton} onClick={onAddQty}>
                                    <AddIcon />
                                </IconButton>
                            </Grid>
                        </Grid>
                        <Button
                            onClick={onAddToBag}
                            color="primary"
                            startIcon={<AddShoppingCartIcon />}
                            disabled={quantity === 0}
                        >
                            Add to bag - $
                            {formatPrice(priceTier ? Math.round(priceTier.price * quantity * 100) / 100 : 0)}
                        </Button>
                    </Grid>
                </MuiDialogActions>
            </Dialog>
        </div>
    );
}

ProductOrderPopup.propTypes = {
    product: PropTypes.object,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};
