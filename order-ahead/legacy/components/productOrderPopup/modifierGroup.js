import React, { useContext } from 'react';
import { PropTypes } from 'prop-types';

import { StoreContext } from '../../../state/store';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import styles from './modifierItems.module.css';

function ModifierGroup({ groupName, children }) {
    const { brand } = useContext(StoreContext);
    const logo = brand.brand === 'arbys' ? '/arbys-logo.svg' : '/bww-logo.svg';

    return (
        <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <img src={logo} className={styles.groupLogo} />
                {groupName}
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>{children}</ExpansionPanelDetails>
        </ExpansionPanel>
    );
}

ModifierGroup.propTypes = {
    groupName: PropTypes.string.isRequired,
    children: PropTypes.node,
};

export default ModifierGroup;
