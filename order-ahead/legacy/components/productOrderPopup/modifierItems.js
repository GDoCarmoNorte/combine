import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import RadioGroup from '@material-ui/core/RadioGroup';
import { Paper, Avatar, Radio } from '@material-ui/core';

import styles from './modifierItems.module.css';

export default function ModifierItems({ value, items, onChange }) {
    return (
        <RadioGroup row value={value} className={styles.groupRadio}>
            <div className={styles.groupContainer}>
                {items.map((item) => (
                    <Item
                        key={item.productId}
                        label={item.name}
                        selected={value === item.productId}
                        onClick={onChange(item)}
                    />
                ))}
            </div>
        </RadioGroup>
    );
}
function Item({ label, selected, onClick }) {
    return (
        <div className={styles.groupItem} onClick={onClick}>
            <Radio checked={selected} />
            <div className={styles.groupText}>
                <Typography variant="subtitle1">{label}</Typography>
            </div>
        </div>
    );
}

function Item_full({ label, selected, onClick }) {
    return (
        <Paper variant="outlined" className={styles.groupItem} onClick={onClick}>
            <Avatar src="/images/arbys/arbys-logo.svg" className={styles.groupImage}></Avatar>
            <div className={styles.groupText}>
                <Typography variant="subtitle2">{label}</Typography>
                <Typography variant="caption">70 cals | $1.69</Typography>
            </div>
            <Radio checked={selected} />
        </Paper>
    );
}

ModifierItems.propTypes = {
    value: PropTypes.any,
    items: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func.isRequired,
};

Item.propTypes = {
    label: PropTypes.string.isRequired,
    selected: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
};
