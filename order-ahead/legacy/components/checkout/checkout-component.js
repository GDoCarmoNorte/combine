import React, { useEffect, useContext, useState } from 'react';

import { Typography } from '@material-ui/core';

import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Link from 'next/link';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
// import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Button from '@material-ui/core/Button';
// import IconButton from '@material-ui/core/IconButton';
// import Avatar from '@material-ui/core/Avatar';
// import DeleteIcon from '@material-ui/icons/Delete';
// import LocalMallIcon from '@material-ui/icons/LocalMall';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import LocationBox from '../locationBox';

import { StoreContext } from '../../state/store';
import { useLocation } from '../../redux/hooks';

import bagDrawerStyles from '../bagDrawer/index.module.css';
import styles from './index.module.css';

import { useUserProfile } from '../../hooks/useProfile';
import CodeVerification from './code-verification';
// import * as gtag from '../../lib/gtag';
import { getLoginUrl } from '../../lib/user';

const itemPrice = (item) => (item.priceTier ? item.priceTier.price : 4.73);

const Checkout = ({ isLoading, isSubmitted, verificationMessage, verify, checkout }) => {
    const {
        user: [user],
        bag: { bag, removeFromBag },
        brand,
    } = useContext(StoreContext);

    const { locationEntry: location } = useLocation();

    const city = location ? location.LocationName : '';
    const street = location ? location.address_1 || location.AddressLine1 : '';

    const [profile] = useUserProfile();

    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [phone, setPhone] = useState();
    const [email, setEmail] = useState();
    const [isFormValid, setIsFormValid] = useState(true);

    useEffect(() => {
        setIsFormValid(firstName && email);
    }, [firstName, lastName, phone, email]);

    const round2 = (v) => Math.round(v * 100) / 100;

    const subtotal = round2(bag.reduce((sum, item) => sum + itemPrice(item) * item.quantity, 0));
    // TODO get tax info from order service
    const tax = round2(subtotal * 0.0);
    const total = round2(subtotal + tax);
    const price = (item) => round2(itemPrice(item) * item.quantity);

    const isEmpty = bag.length < 1;

    console.log('Checkout', isEmpty || isLoading);

    useEffect(() => {
        if (profile) {
            setFirstName(profile.firstName);
            setLastName(profile.lastName);
            setPhone(profile.phone);
            setEmail(profile.email);
        } else if (user) {
            setFirstName(user.firstName);
            setLastName(user.lastName);
            setPhone(user.phone);
            setEmail(user.email.endsWith('@anonymous.com') ? '' : user.email);
        }
    }, [profile, user]);

    const onCheckout = () => {
        const nickname = `${firstName} ${lastName}`.trim() || email;

        checkout({ phone, email, firstName, lastName, nickname });
    };

    return (
        <>
            <div className={styles.header}>
                <Typography variant="h6" align="right">
                    Checkout
                </Typography>
                {isLoading && <CircularProgress size={28} />}
                <div className={styles.buttonWrapper}>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={isEmpty || isLoading || verificationMessage || !isFormValid || !location}
                        onClick={onCheckout}
                    >
                        Submit order - ${total}
                    </Button>
                </div>
            </div>
            <div className={styles.order}>
                <div>
                    {verificationMessage && (
                        <CodeVerification
                            isLoading={isLoading}
                            verificationMessage={verificationMessage}
                            verify={verify}
                        />
                    )}
                    <Paper elevation={3} className={styles.paper}>
                        <Typography variant="h6">Your Information</Typography>
                        <form className={styles.orderForm}>
                            <TextField
                                id="firstName"
                                label="First Name"
                                size="small"
                                value={firstName || ''}
                                onChange={(e) => setFirstName(e.target.value)}
                                margin="normal"
                                fullWidth
                                required
                                error={!firstName}
                                helperText={!firstName ? 'Name is required' : ''}
                            />
                            <TextField
                                id="lastName"
                                label="Last Name"
                                size="small"
                                value={lastName || ''}
                                fullWidth
                                margin="normal"
                                onChange={(e) => setLastName(e.target.value)}
                            />
                            <TextField
                                id="phone"
                                label="Phone Number"
                                size="small"
                                value={phone || ''}
                                fullWidth
                                margin="normal"
                                onChange={(e) => setPhone(e.target.value)}
                            />
                            <TextField
                                id="email"
                                label="Email"
                                size="small"
                                value={email || ''}
                                fullWidth
                                margin="normal"
                                required
                                onChange={(e) => setEmail(e.target.value)}
                                error={!email}
                                helperText={!email ? 'Email is required' : ''}
                            />
                        </form>
                        {user && user.authorized === false && (
                            <Link href={getLoginUrl(user)}>
                                <a>
                                    <Button variant="contained" color="primary">
                                        Sign In
                                    </Button>
                                </a>
                            </Link>
                        )}
                    </Paper>
                </div>
                <div>
                    {!isEmpty && (
                        <Paper elevation={3} className={styles.paper}>
                            <>
                                <Typography variant="h6">Your Order</Typography>
                                <LocationBox location={location}></LocationBox>
                                <Divider />
                                <List className={styles.bagList}>
                                    {bag.map((item, i) => {
                                        return (
                                            <ListItem key={i} alignItems="flex-start">
                                                <ListItemIcon>{item.quantity}</ListItemIcon>
                                                <ListItemText>
                                                    <div>{item.product.name}</div>
                                                    {item.modifiers &&
                                                        item.modifiers.map(
                                                            (modifier, j) =>
                                                                modifier.selectedModifier && (
                                                                    <Typography
                                                                        className={bagDrawerStyles.modifier}
                                                                        variant="caption"
                                                                        key={j}
                                                                    >
                                                                        {modifier.selectedModifier.name}
                                                                    </Typography>
                                                                )
                                                        )}
                                                </ListItemText>
                                                <ListItemSecondaryAction>{`$${price(item)}`}</ListItemSecondaryAction>
                                            </ListItem>
                                        );
                                    })}
                                    <Divider></Divider>
                                    <ListItem key="subtotal" alignItems="flex-start">
                                        <ListItemText>Subtotal</ListItemText>
                                        <ListItemSecondaryAction>{`$${subtotal}`}</ListItemSecondaryAction>
                                    </ListItem>
                                    <ListItem key="tax" alignItems="flex-start">
                                        <ListItemText>Tax</ListItemText>
                                        <ListItemSecondaryAction>{`$${tax}`}</ListItemSecondaryAction>
                                    </ListItem>
                                    <ListItem key="total" alignItems="flex-start">
                                        <ListItemText>Total</ListItemText>
                                        <ListItemSecondaryAction>{`$${total}`}</ListItemSecondaryAction>
                                    </ListItem>
                                </List>
                            </>
                            {isEmpty && <div className={styles.emptyBag}></div>}
                        </Paper>
                    )}
                </div>
            </div>
        </>
    );
};

export default Checkout;
