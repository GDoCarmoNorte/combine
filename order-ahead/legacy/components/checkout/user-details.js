import React, { useContext, useState } from 'react';
import { Typography, TextField, Button } from '@material-ui/core';

import { StoreContext } from '../../../state/store';

const UserDetails = (props) => {
    const {
        bag: { bag },
    } = useContext(StoreContext);

    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [phone, setPhone] = useState();
    const [email, setEmail] = useState();

    const { checkout, isLoading } = props;

    const onClick = () => checkout(phone, email);

    return (
        <>
            <Typography variant="h5">Checkout</Typography>
            <form>
                <TextField
                    id="firstName"
                    label="First Name"
                    variant="outlined"
                    size="small"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                    margin="normal"
                    fullWidth
                />
                <TextField
                    id="lastName"
                    label="Last Name"
                    variant="outlined"
                    size="small"
                    value={lastName}
                    fullWidth
                    margin="normal"
                    onChange={(e) => setLastName(e.target.value)}
                />
                <TextField
                    id="phone"
                    label="Phone Number"
                    variant="outlined"
                    size="small"
                    value={phone}
                    fullWidth
                    margin="normal"
                    onChange={(e) => setPhone(e.target.value)}
                />
                <TextField
                    id="email"
                    label="Email"
                    variant="outlined"
                    size="small"
                    value={email}
                    fullWidth
                    margin="normal"
                    onChange={(e) => setEmail(e.target.value)}
                />
            </form>
            <Button color="primary" variant="contained" onClick={onClick} disabled={isLoading}>
                Continue
            </Button>
        </>
    );
};

export default UserDetails;
