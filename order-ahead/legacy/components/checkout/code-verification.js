import React, { useState } from 'react';
import { Typography, TextField, Button, Paper } from '@material-ui/core';

import styles from './index.module.css';

const CodeVerification = (props) => {
    const { verificationMessage, verify, isLoading } = props;
    const [code, setCode] = useState();

    const onClick = () => verify(code);

    return (
        <Paper variant="outlined" className={styles.verification}>
            <Typography variant="subtitle2">{verificationMessage}</Typography>
            <TextField
                id="code"
                label="Verification Code"
                variant="outlined"
                size="small"
                value={code}
                onChange={(e) => setCode(e.target.value)}
                margin="normal"
                fullWidth
            />
            <Button color="primary" variant="contained" disabled={isLoading} onClick={onClick}>
                Verify
            </Button>
        </Paper>
    );
};

export default CodeVerification;
