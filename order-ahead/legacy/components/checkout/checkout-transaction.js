const getProductCategory = (bag, item) => {
    // eslint-disable-next-line eqeqeq
    const product = bag.find((p) => p.product.productId == item.itemId);

    return product ? product.product.productCategory : '';
};

/**
 * @param {Object} bag
 * @param {Object} response
 * @param {Object} brand
 */
export const createTransaction = (bag, response, brand) => {
    const total = response.items.reduce((sum, { price, quantity }) => sum + price * quantity, 0);
    const transaction = {
        transaction_id: response.id,
        value: total,
        tax: 0,
        shipping: 0,
        currency: 'USD',
        items: response.items.map((item, i) => {
            return {
                id: item.itemId,
                name: item.description,
                brand: brand.brand,
                category: getProductCategory(bag, item),
                list_position: i + 1,
                quantity: item.quantity,
                price: item.price,
            };
        }),
    };

    return transaction;
};
