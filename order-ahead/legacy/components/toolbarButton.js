import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Link from 'next/link';
import isMobileScreen from '../lib/isMobileScreen';

const useStyles = makeStyles({
    label: {
        display: 'flex',
        flexDirection: 'column',
    },
    active: {},
});

export default function ToolbarButton({ href, linkAs, target, isActive, icon, text, children }) {
    const classes = useStyles();
    const textVariant = isMobileScreen() ? 'caption' : 'button';
    console.log(icon, text);
    return (
        <Link href={href} as={linkAs}>
            <a target={target} className={isActive ? classes.active : ''}>
                <IconButton variant="text" color="primary" classes={{ label: classes.label }}>
                    {children}
                    <Typography variant={textVariant}>{text}</Typography>
                </IconButton>
            </a>
        </Link>
    );
}

ToolbarButton.propTypes = {
    href: PropTypes.string,
    linkAs: PropTypes.string,
    target: PropTypes.string,
    isActive: PropTypes.bool,
    text: PropTypes.object,
};
