import React from 'react';

import styles from './iframe.module.css';

export default function Iframe({ src }) {
    return <iframe className={styles.iframe} src={src}></iframe>;
}
