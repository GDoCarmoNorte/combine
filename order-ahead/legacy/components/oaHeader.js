import React, { useContext, useEffect, useState } from 'react';
import { withRouter } from 'next/router';

import { StoreContext } from '../state/store';
import { useLocation } from '../redux/hooks';

// User related
import * as gtag from '../lib/gtag';
import InspireToolbar from './toolbar';

const Header = function (props) {
    // const classes = useStyles();
    const { target = null } = props;
    const {
        user: [user],
        brand,
    } = useContext(StoreContext);

    const { locationEntry: location } = useLocation();

    const [currentUser, setCurrentUser] = useState({});
    const address = location ? location.LocationName : '';
    const defaultMenuRoute = brand.brand === 'arbys' ? '/menu/674389620' : '/menu/107';

    useEffect(() => {
        if (user && user.user_id !== currentUser.user_id) {
            setCurrentUser(user);
            gtag.setUserId(user);

            if (user.authorized) {
                gtag.event({
                    action: 'authentication',
                    category: 'engagement',
                    label: 'Authentication',
                    value: user.user_id,
                });
            } else {
                gtag.event({
                    action: 'login',
                    category: 'engagement',
                    label: 'Login',
                    value: user.user_id,
                });
            }
        }
    }, [user, currentUser]);

    return (
        <InspireToolbar
            user={user}
            target={target}
            address={address}
            location={location}
            defaultMenuRoute={defaultMenuRoute}
        />
    );
};

export default withRouter(Header);
