import React, { useContext, useState } from 'react';
import Link from 'next/link';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

// import MenuIcon from '@material-ui/icons/Menu';
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import NotListedLocationIcon from '@material-ui/icons/NotListedLocation';
import PersonIcon from '@material-ui/icons/Person';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import EventNoteIcon from '@material-ui/icons/EventNote';

// import { Menu, MenuItem, Divider, ListItemIcon, Typography } from '@material-ui/core';

import { useRouter } from 'next/router';
import isMobileScreen from '../lib/isMobileScreen';

import BagButton from './legacy/bagButton';
import BagDrawer from './bagDrawer';

import styles from './toolbar.module.css';

import { StoreContext } from '../state/store';
import { getLoginUrl } from '../lib/user';

// const anchorOrigin = { vertical: 'bottom', horizontal: 'left' };

const InspireToolbar = ({ user, location, address, defaultMenuRoute, target }) => {
    const { brand } = useContext(StoreContext);
    const [menuAnchor, setMenuAnchor] = useState(null);
    const authorized = user && user.authorized;

    const handleClick = (event) => setMenuAnchor(event.currentTarget);
    const handleClose = () => setMenuAnchor(null);

    const city = location ? location.LocationName : '';
    const street = location ? location.address_1 || location.AddressLine1 : '';
    const router = useRouter();
    const smallToopBar = isMobileScreen();

    return (
        <>
            <AppBar position="static" color="transparent">
                <Toolbar></Toolbar>{' '}
            </AppBar>
            <AppBar position="fixed" color="default">
                <Toolbar>
                    <div className={`${styles.toolbox} ${styles.largeScreen}`}>
                        <Link href="/">
                            <a target={target} className={router.pathname === '/' ? styles.active : ''}>
                                <img
                                    alt="Logo"
                                    src={`/brands/${brand.brand}/logo.svg`}
                                    className={styles.logo}
                                    height="36px"
                                ></img>
                            </a>
                        </Link>
                        <Link href="/menu/[category]" as={defaultMenuRoute}>
                            <a target={target} className={router.pathname.startsWith('/menu/') ? styles.active : ''}>
                                <Button variant="text" color="primary" startIcon={<RestaurantMenuIcon />}>
                                    Menu
                                </Button>
                            </a>
                        </Link>
                        {location ? (
                            <Link href="/order/[...location]" as={`/order/${location.LocationId}`}>
                                <a
                                    target={target}
                                    className={router.pathname.startsWith('/order/') ? styles.active : ''}
                                >
                                    <Button variant="text" color="primary" startIcon={<LocalGroceryStoreIcon />}>
                                        Order
                                    </Button>
                                </a>
                            </Link>
                        ) : (
                            <Link href="/locations">
                                <a
                                    target={target}
                                    className={router.pathname.startsWith('/locations') ? styles.active : ''}
                                >
                                    <Button variant="text" color="primary" startIcon={<LocalGroceryStoreIcon />}>
                                        Order
                                    </Button>
                                </a>
                            </Link>
                        )}
                        <Link href="/locations">
                            <a target={target}>
                                <Button variant="outlined" color="primary" startIcon={<PersonPinCircleIcon />}>
                                    <span className={styles.located}>{city || 'Pick up from'}</span>
                                    <span className={styles.pickupAt}>
                                        {street ? ['Pickup@ ', street].join('') : ''}
                                    </span>
                                </Button>
                            </a>
                        </Link>
                        <Link href="/gift-cards">
                            <a
                                target={target}
                                className={router.pathname.startsWith('/gift-cards') ? styles.active : ''}
                            >
                                <IconButton color="primary">
                                    <CardGiftcardIcon />
                                </IconButton>
                            </a>
                        </Link>
                        <Link href="/order-history">
                            <a
                                target={target}
                                className={router.pathname.startsWith('/order-history') ? styles.active : ''}
                            >
                                <IconButton color="primary">
                                    <EventNoteIcon />
                                </IconButton>
                            </a>
                        </Link>
                        <Link href={authorized ? '/profile' : getLoginUrl(user)}>
                            <a
                                target={target}
                                disabled={!user}
                                className={
                                    router.pathname.startsWith('/profile') || router.pathname.startsWith('/api/login')
                                        ? styles.active
                                        : ''
                                }
                            >
                                <IconButton color="primary" disabled={!user}>
                                    {authorized ? <PersonIcon /> : <AccountBoxIcon />}
                                </IconButton>
                            </a>
                        </Link>
                        <BagButton />
                    </div>
                    <div className={`${styles.menubox} ${styles.smallScreen}`}>
                        <div className={`${styles.smallToolbarBlock} ${styles.left}`}>
                            <Link href="/menu/[category]" as={defaultMenuRoute}>
                                <a
                                    target={target}
                                    className={router.pathname.startsWith('/menu/') ? styles.active : ''}
                                >
                                    <IconButton color="primary">
                                        <RestaurantMenuIcon />{' '}
                                    </IconButton>
                                </a>
                            </Link>
                            {location ? (
                                <Link href="/order/[...location]" as={`/order/${location.LocationId}`}>
                                    <a
                                        target={target}
                                        className={router.pathname.startsWith('/order/') ? styles.active : ''}
                                    >
                                        <IconButton color="primary">
                                            <LocalGroceryStoreIcon />{' '}
                                        </IconButton>
                                    </a>
                                </Link>
                            ) : (
                                <Link href="/locations">
                                    <a
                                        target={target}
                                        className={router.pathname.startsWith('/locations') ? styles.active : ''}
                                    >
                                        <IconButton color="primary">
                                            <LocalGroceryStoreIcon />{' '}
                                        </IconButton>
                                    </a>
                                </Link>
                            )}
                            <Link href="/locations">
                                <IconButton color="primary">
                                    {location ? <LocationOnIcon /> : <NotListedLocationIcon />}
                                </IconButton>
                            </Link>
                        </div>
                        <div className={`${styles.smallToolbarBlock} ${styles.center}`}>
                            <Link href="/">
                                <a target={target} className={router.pathname === '/' ? styles.active : ''}>
                                    <IconButton color="primary">
                                        <img
                                            alt="Logo"
                                            src={`/brands/${brand.brand}/logo.svg`}
                                            className={styles.logo}
                                            height="36px"
                                        ></img>
                                    </IconButton>
                                </a>
                            </Link>
                        </div>
                        <div className={`${styles.smallToolbarBlock} ${styles.right}`}>
                            <Link href={authorized ? '/profile' : getLoginUrl(user)}>
                                <a
                                    target={target}
                                    disabled={!user}
                                    className={
                                        router.pathname.startsWith('/profile') ||
                                        router.pathname.startsWith('/api/login')
                                            ? styles.active
                                            : ''
                                    }
                                >
                                    <IconButton color="primary" disabled={!user}>
                                        {authorized ? <PersonIcon /> : <AccountBoxIcon />}
                                    </IconButton>
                                </a>
                            </Link>
                            <BagButton />
                        </div>
                    </div>
                    <BagDrawer />
                </Toolbar>
            </AppBar>
        </>
    );
};

export default InspireToolbar;
