import React from 'react';

import { Typography, Button } from '@material-ui/core';
import orderStyles from '../../components/orderHistory/index.module.css';

import userStyles from './userProfile.module.css';
import useOrderHistory from '../../hooks/useOrderHistory';

function Orders() {
    const [orders, isLoading, isLoaded, reorder] = useOrderHistory();
    const lastOrder = orders && orders[0];

    return (
        <div className={userStyles.form}>
            <Typography variant="h5" className={userStyles.title}>
                LAST ORDER
            </Typography>
            {lastOrder && (
                <div className={orderStyles.orderItem} variant="outlined">
                    <div className={orderStyles.orderItemHeader}>
                        <h5>
                            {lastOrder.id} - {lastOrder.destination} -{' '}
                            {new Date(lastOrder.deliveryDate).toLocaleDateString()} - {lastOrder.orderStatus}
                        </h5>
                        <Button color="primary" variant="outlined" onClick={() => reorder(lastOrder.id)}>
                            Reorder
                        </Button>
                    </div>
                    <ol className={orderStyles.orderItemProductList}>
                        {lastOrder.items.map((i, index) => (
                            <li key={lastOrder.id + i.itemId}>
                                {i.description}, ${i.price}
                            </li>
                        ))}
                    </ol>
                </div>
            )}
        </div>
    );
}

export default Orders;
