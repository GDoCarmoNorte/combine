import React from 'react';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';

import styles from './userProfile.module.css';
import ProfileDetailsForm from './profileDetailsForm';
import Offers from './offers';
import Orders from './orders';
import * as gtag from '../../lib/gtag';

const UserProfile = ({ user }) => {
    const { user_id: userId } = user;

    const handleLogoutClick = () => {
        gtag.event({
            action: 'User Logout',
            category: 'Logout',
            value: userId,
        });
    };

    return (
        <>
            <Container component="div" maxWidth="md" className={styles.marginTop}>
                <ProfileDetailsForm user={user} />
                <Offers />
                <Orders />
                <div className={styles.linkButtons}>
                    <Link href="/api/reset-password">
                        <a>
                            <Button variant="outlined" className={styles.button} color="primary">
                                Reset password
                            </Button>
                        </a>
                    </Link>
                    <Link href="/api/logout">
                        <a>
                            <Button
                                onClick={handleLogoutClick}
                                variant="outlined"
                                className={styles.button}
                                color="primary"
                            >
                                Logout
                            </Button>
                        </a>
                    </Link>
                </div>
            </Container>
        </>
    );
};

export default UserProfile;
