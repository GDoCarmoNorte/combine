import React, { useContext } from 'react';

import { Typography } from '@material-ui/core';

import styles from '../itemGrid.module.css';
import userStyles from './userProfile.module.css';
import { StoreContext } from '../../../state/store';

function Offers() {
    const {
        user: [user],
    } = useContext(StoreContext);

    const name = user ? user.name : '';
    const imgsrc = 'B1031-002319-00_Feb_MacNCheese_M_2ndMH_768x640_v3-WithLegal.jpg';
    return (
        <div className={userStyles.form}>
            <Typography variant="h5" className={userStyles.title}>
                OFFERS
            </Typography>
            <p>Here is the list of offers just for you, {name}</p>
            <div className={styles.gridContainer}>
                {[imgsrc].map((item) => (
                    <div className={styles.gridItem} key={item}>
                        <img className={styles.gridImage} src={`/images/${item}`}></img>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Offers;
