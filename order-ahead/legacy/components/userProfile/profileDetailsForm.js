import React, { useState, useEffect } from 'react';
import { TextField, Button, Typography } from '@material-ui/core';

import styles from './userProfile.module.css';

let savedUser;

export default function ProfileDetailsForm({ user }) {
    savedUser = savedUser === undefined ? { ...user } : savedUser;

    const [firstName, setFirstName] = useState(savedUser.firstName);
    const [lastName, setLastName] = useState(savedUser.lastName);
    const [nickname, setNickname] = useState(savedUser.nickname);
    const [zip, setZip] = useState(savedUser.zip);
    const [phone, setPhone] = useState(savedUser.phone);
    const [email, setEmail] = useState(savedUser.email);
    const [LastOrder, setLastOrder] = useState(savedUser.LastOrder);
    const [birthyear, setBirthYear] = useState(savedUser.birthyear);
    const [isButtonActive, setIsButtonActive] = useState(false);

    const secondary = 'Secondary text';

    const { app_metadata } = user;

    const dataChanged = () =>
        firstName !== savedUser.firstName ||
        lastName !== savedUser.lastName ||
        nickname !== savedUser.nickname ||
        phone !== savedUser.phone ||
        zip !== savedUser.zip ||
        LastOrder !== savedUser.LastOrder ||
        email !== savedUser.email ||
        birthyear !== savedUser.birthyear;

    useEffect(() => {
        if (dataChanged()) setIsButtonActive(true);
        else setIsButtonActive(false);
    }, [firstName, lastName, nickname, zip, phone, LastOrder, email, birthyear]);

    const getChangedAttributes = () => {
        const changedAttributes = [];
        firstName !== savedUser.firstName && changedAttributes.push('First Name');
        lastName !== savedUser.lastName && changedAttributes.push('Last Name');
        nickname !== savedUser.nickname && changedAttributes.push('Nickname');
        phone !== savedUser.phone && changedAttributes.push('Phone');
        zip !== savedUser.zip && changedAttributes.push('Zip');
        email !== savedUser.email && changedAttributes.push('Email');
        birthyear !== savedUser.birthyear && changedAttributes.push('Birth Year');
        return changedAttributes.join(', ');
    };

    const resetChanges = () => {
        setFirstName(savedUser.firstName);
        setLastName(savedUser.lastName);
        setNickname(savedUser.nickname);
        setPhone(savedUser.phone);
        setEmail(savedUser.email);
        setLastOrder(savedUser.LastOrder);
        setZip(savedUser.zip);
        setBirthYear(savedUser.birthyear);
    };

    const saveChanges = (e) => {
        e.preventDefault();
        setIsButtonActive(false);
        fetch('/api/user', {
            method: 'PUT',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                firstName,
                lastName,
                phone,
                zip,
                nickname,
                email,
                LastOrder,
                birthyear,
            }),
        })
            .then((res) => res.json())
            .then((user) => {
                savedUser = user;
            });
    };

    return (
        <form key="profile-details" className={styles.form} onSubmit={saveChanges}>
            <Typography variant="h5" className={styles.title}>
                PROFILE DETAILS
            </Typography>
            <TextField
                key="firstName"
                id="firstName"
                label="First Name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            <TextField
                id="lastName"
                label="Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            <TextField
                id="nickname"
                label="Nickname"
                value={nickname}
                onChange={(e) => setNickname(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            <TextField
                id="phone"
                label="Phone Number"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            <TextField
                id="email"
                label="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            <TextField
                id="zip"
                label="Zip Code"
                value={zip}
                onChange={(e) => setZip(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            <TextField
                id="birthyear"
                label="Birth Year"
                value={birthyear}
                onChange={(e) => setBirthYear(e.target.value)}
                size="small"
                fullWidth
                margin="normal"
            />
            {app_metadata && (
                <div className={styles.marginTop}>
                    <Typography variant="span">App metadata:</Typography>
                    {Object.keys(app_metadata).map((key) => (
                        <div key={key}>
                            <span>{key}: </span>
                            <span>{app_metadata[key]}</span>
                        </div>
                    ))}
                </div>
            )}
            <div className={styles.buttons}>
                <Button type="submit" color="primary" disabled={!isButtonActive} className={styles.button}>
                    Save
                </Button>
                <Button color="primary" onClick={resetChanges} disabled={!isButtonActive} className={styles.button}>
                    Cancel
                </Button>
            </div>
        </form>
    );
}
