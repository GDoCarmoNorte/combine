import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import styles from './userProfile.module.css';

export default function AccountSettingsForm() {
    const [currentPassword, setCurrentPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [reNewPassword, setReNewPassword] = useState('');

    const passwordsEqualMessage = <Typography className={styles.success}>Passwords are equal!</Typography>;

    const passwordsNotEqualessage = <Typography className={styles.error}>Passwords are not equal!</Typography>;

    const handleEnterRePassword = (e) => {
        setReNewPassword(e.target.value);
    };

    const handleSaveClick = (e) => {
        e.preventDefaut();
        console.log('password updated');
    };

    const passwordsValidationMessage = () => {
        if (reNewPassword === '') return null;

        return newPassword === reNewPassword ? passwordsEqualMessage : passwordsNotEqualessage;
    };

    return (
        <form onSubmit={handleSaveClick} className={styles.form}>
            <Typography variant="h5">PASSWORD SETTINGS</Typography>
            <TextField
                id="current-password"
                label="Current password"
                type="password"
                autoComplete="current-password"
                variant="outlined"
                fullWidth
                margin="normal"
                value={currentPassword}
                onChange={(e) => setCurrentPassword(e.target.value)}
            />
            <TextField
                id="new-password"
                label="New password"
                type="password"
                variant="outlined"
                fullWidth
                margin="normal"
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
            />
            <TextField
                id="reenter-new-password"
                label="Re-enter new password"
                type="password"
                variant="outlined"
                fullWidth
                margin="normal"
                value={reNewPassword}
                onChange={handleEnterRePassword}
            />
            {passwordsValidationMessage()}
            <div className={styles.buttons}>
                <Button
                    variant="outlined"
                    color="primary"
                    type="submit"
                    disabled={reNewPassword === '' || reNewPassword !== newPassword}
                >
                    Save
                </Button>
            </div>
        </form>
    );
}
