import React, { useState, useContext, useEffect } from 'react';
import Link from 'next/link';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Divider from '@material-ui/core/Divider';

import Button from '@material-ui/core/Button';
import Drawer from '@material-ui/core/Drawer';
import CloseIcon from '@material-ui/icons/Close';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import { Typography } from '@material-ui/core';
import * as gtag from '../../../lib/gtag';
import { formatPrice } from '../../../lib/format';

import { StoreContext } from '../../../state/store';
import { useLocation } from '../../../redux/hooks';

import styles from './index.module.css';

const itemPrice = (item) => (item.priceTier ? item.priceTier.price : 4.73);
const getBaseCost = (item) => getBasePrice(item.product) * item.quantity;
const calculateTotal = (bag) => {
    return Math.round(bag.reduce((sum, item) => sum + itemPrice(item) * item.quantity, 0) * 100) / 100;
};
const getBasePrice = (product) => {
    const activeTierAndPrices = product.activeTierAndPrices || [];
    const defaultPriceTier = activeTierAndPrices.find((tier) => tier.tier.default);
    const activePriceTier = activeTierAndPrices.find((tier) => tier.tier.type !== 'dynamic' && !tier.tier.default);

    if (defaultPriceTier) return defaultPriceTier.price;
    if (activePriceTier) return activePriceTier.price;
    return 4.72;
};

const getDynPrice = (product) =>
    ((product.activeTierAndPrices || []).find((t) => t.tier.type === 'dynamic') || { price: 4.72 }).price;

const getDiscount = (item) => (getBasePrice(item.product) - itemPrice(item)) * item.quantity;

const getDynPriceName = (product) => {
    let dynTier = (product.activeTierAndPrices || []).find((t) => t.tier.type === 'dynamic');
    return dynTier ? dynTier.tier.name : null;
};
export default function BagDrawer() {
    const {
        user: [user],
        bag: { bag, removeFromBag, bagOpen, setBagOpen, updateInBag },
        brand,
    } = useContext(StoreContext);

    const { locationEntry: currentLocation } = useLocation();

    const [total, setTotal] = useState(calculateTotal(bag));

    const onCloseBag = () => setBagOpen(false);
    const isEmpty = bag.length < 1;
    const price = (item) => Math.round(itemPrice(item) * item.quantity * 100) / 100;

    const handleClickRemoveFromBag = (productIndex, productId) => {
        gtag.event({
            action: `${productId} item removed from cart`,
            category: 'ecommerce',
            label: 'Remove from cart',
            value: {
                product_id: productId,
                user_id: user.user_id,
            },
        });
        removeFromBag(productIndex);
    };

    const handleCheckoutClick = () => {
        const value = {
            transaction_id: null,
            value: total,
            tax: null,
            items: bag.map((item, i) => {
                return {
                    id: item.product.productId,
                    name: item.product.name || item.product.productName,
                    brand: brand.brand,
                    category: item.product.productCategory,
                    list_position: i + 1,
                    quantity: item.product.quantity,
                    price: itemPrice(item),
                };
            }),
        };
        gtag.event({
            action: 'begin_checkout',
            category: 'ecommerce',
            label: 'Begin checkout',
            value,
        });
        onCloseBag();
    };

    const addQuantity = (index, item) => () => {
        if (item.quantity < 99) {
            updateInBag(index, { ...item, quantity: item.quantity + 1 });
        }
    };

    const removeQuantity = (index, item) => () => {
        if (item.quantity > 1) {
            updateInBag(index, { ...item, quantity: item.quantity - 1 });
        }
    };

    useEffect(() => {
        setTotal(calculateTotal(bag));
    }, [bag]);

    const options = ['Remove', ...[...Array(5).keys()].map((d) => d + 1)];

    return (
        <Drawer className={styles.drawer} anchor="right" open={bagOpen} onClose={onCloseBag}>
            <IconButton className={styles.closeButton} onClick={onCloseBag}>
                <CloseIcon />
            </IconButton>
            {isEmpty && (
                <div className={styles.emptyBag}>
                    <span className={styles.emptyBagIcon}>
                        <img className={styles.bagImage} src={`/brands/${brand.brand}/bag.svg`}></img>
                        <span className={styles.badge} color="primary">
                            0
                        </span>
                    </span>
                    <span className={styles.emptyBagNotice}>Your bag is empty</span>
                </div>
            )}
            {!isEmpty && (
                <>
                    {currentLocation && currentLocation.LocationName && (
                        <div className={styles.locationBox}>
                            <Typography variant="h6">Pick From</Typography>
                            <div className={styles.locationDetails}>
                                <div className={styles.locationLine}>
                                    <LocationOnIcon color="primary" />
                                    <Typography color="primary">{currentLocation.LocationName}</Typography>
                                </div>
                                <div className={styles.locationLine}>
                                    <Typography className={styles.locationLabel} variant="caption">
                                        Phone:
                                    </Typography>
                                    <span className={styles.locationItem}>{currentLocation.Phone}</span>
                                </div>
                                <div className={styles.locationLine}>
                                    <Typography className={styles.locationLabel} variant="caption">
                                        Address:
                                    </Typography>
                                    <span className={styles.locationItem}>{currentLocation.AddressLine1}</span>
                                </div>
                            </div>
                            <Divider />
                        </div>
                    )}
                    <div className={styles.listCaption}>
                        <Typography variant="h6">Selected Items</Typography>
                    </div>
                    <List className={styles.list} dense>
                        {bag.map((item, i) => {
                            return (
                                <div key={i}>
                                    <ListItem className={styles.item} key={i} alignItems="flex-start">
                                        <ListItemIcon className={styles.itemIcon}>
                                            <Button
                                                className={styles.qtyButton}
                                                disabled={item.quantity < 2}
                                                onClick={removeQuantity(i, item)}
                                            >
                                                -
                                            </Button>
                                            {item.quantity}
                                            <Button
                                                className={styles.qtyButton}
                                                disabled={item.quantity >= 99}
                                                onClick={addQuantity(i, item)}
                                            >
                                                +
                                            </Button>
                                        </ListItemIcon>
                                        <ListItemText className={styles.itemText}>
                                            <div>{item.product.name}</div>
                                            {item.modifiers &&
                                                item.modifiers.map((modifier, j) => {
                                                    return (
                                                        modifier.selectedModifier && (
                                                            <Typography
                                                                className={styles.modifier}
                                                                variant="body2"
                                                                key={j}
                                                            >
                                                                {modifier.selectedModifier.name}
                                                            </Typography>
                                                        )
                                                    );
                                                })}
                                        </ListItemText>
                                        <ListItemSecondaryAction>
                                            <div>{`$${formatPrice(getBaseCost(item))}`}</div>
                                            <IconButton
                                                edge="end"
                                                aria-label="delete"
                                                onClick={() => handleClickRemoveFromBag(i, item.product.productId)}
                                            >
                                                <DeleteIcon />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                    {getDynPriceName(item.product) && (
                                        <ListItem className={styles.discount} key={i + 'dicount'}>
                                            <ListItemText className={styles.itemText}>
                                                <div>{getDynPriceName(item.product)}</div>
                                            </ListItemText>
                                            <ListItemSecondaryAction>
                                                <div className={styles.discount}>{`-${formatPrice(
                                                    getDiscount(item)
                                                )}`}</div>
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    )}
                                </div>
                            );
                        })}
                    </List>
                    <Link href="/checkout">
                        <a className={styles.checkoutLink}>
                            <Button
                                className={styles.checkoutButton}
                                variant="contained"
                                color="primary"
                                startIcon={<ShoppingBasketIcon />}
                                onClick={handleCheckoutClick}
                            >
                                Checkout - ${total}
                            </Button>
                        </a>
                    </Link>
                </>
            )}
        </Drawer>
    );
}
