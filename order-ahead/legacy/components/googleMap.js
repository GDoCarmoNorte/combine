import React, { useState, useEffect, Fragment, useContext } from 'react';
import GoogleMapReact from 'google-map-react';

import styles from './googleMap.module.css';
import LocationToolTip from './location/storeLocation/locationToolTip';
import { StoreContext } from '../state/store';

const API_KEY = 'AIzaSyCgLpOWiCLPazClgP41G6mKQV78OEFamoU';

const defaultProps = {
    center: { lat: 37.65, lng: -99.38 },
    zoom: 4.2,
};
const isTouchDevice = () => {
    return window ? 'ontouchstart' in window : false;
};

// InfoWindow component
const InfoWindow = (props) => {
    const { place, onLocationSelect } = props;

    function pickStore(place) {
        return (e) => {
            console.log('Place is selected', place);
            e.stopPropagation();
            onLocationSelect(place);
        };
    }
    return (
        <div className={styles.infoContainer}>
            <div className={styles.info}>
                <LocationToolTip place={place} onLocationSelect={onLocationSelect} />
            </div>
        </div>
    );
};

// Marker component
const Marker = (props) => {
    const { brand } = useContext(StoreContext);

    const markerBackground = {
        userSelect: 'none',
        width: '30px',
        border: '0px',
        cursor: 'pointer',
    };
    const markerStyle = {
        border: '1px solid white',
        borderRadius: '50%',
        height: 10,
        width: 10,
        cursor: 'pointer',
        zIndex: 10,
        backgroundColor: 'red',
    };
    return (
        <Fragment>
            <img style={markerBackground} src={`/brands/${brand.brand}/location.png`} />
            {props.place.show && <InfoWindow place={props.place} onLocationSelect={props.onLocationSelect} />}
        </Fragment>
    );
};

const MyMap = function (props) {
    const list = props.locations || [];
    const { onLocationSelect } = props;
    const [locationList, setLocationList] = useState([...list]);
    useEffect(() => {
        setLocationList([...list]);
    }, [list]);
    const [api, setAPI] = useState(null);
    const [map, setMap] = useState(null);
    console.log(locationList);

    const apiIsLoaded = (map, maps, places) => {
        setAPI(maps);
        setMap(map);
        if (places.length === 0) return;
        // Get bounds by our places
        const bounds = getMapBounds(maps, places);
        // Fit map to bounds
        map.fitBounds(bounds);
        // Bind the resize listener
        // bindResizeListener(map, maps, bounds);
    };
    const resetBounds = () => {
        if (api && locationList.length > 0) {
            const bounds = getMapBounds(api, locationList);
            map.fitBounds(bounds);
        }
    };
    const getMapBounds = (maps, places) => {
        const bounds = new maps.LatLngBounds();
        places.forEach((place) => {
            bounds.extend(new maps.LatLng(place.lat, place.lng));
        });
        return bounds;
    };

    resetBounds();

    // onChildClick callback can take two arguments: key and childProps
    const onChildClickCallback = (key) => {
        console.log(key);
        locationList.forEach((s) => (s.show = false));
        const location = locationList.find((s) => s.LocationId === key);
        if (location) {
            location.show = !location.show;
        }
        setLocationList([...locationList]);
        return { locationList };
    };

    return (
        <GoogleMapReact
            bootstrapURLKeys={{ key: API_KEY }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps, locationList)}
            onChildClick={onChildClickCallback}
            options={createMapOptions}
        >
            {(locationList || []).map((place) => (
                <Marker
                    key={place.LocationId}
                    lat={place.lat}
                    lng={place.lng}
                    place={place}
                    onLocationSelect={onLocationSelect}
                />
            ))}
        </GoogleMapReact>
    );
};

function createMapOptions() {
    return {
        zoomControl: !isTouchDevice(),
        styles: [
            {
                featureType: 'administrative',
                elementType: 'all',
                stylers: [
                    {
                        saturation: '-100',
                    },
                ],
            },
            {
                featureType: 'administrative.province',
                elementType: 'all',
                stylers: [
                    {
                        visibility: 'off',
                    },
                ],
            },
            {
                featureType: 'landscape',
                elementType: 'all',
                stylers: [
                    {
                        saturation: -100,
                    },
                    {
                        lightness: 65,
                    },
                    {
                        visibility: 'on',
                    },
                ],
            },
            {
                featureType: 'poi',
                elementType: 'all',
                stylers: [
                    {
                        saturation: -100,
                    },
                    {
                        lightness: '50',
                    },
                    {
                        visibility: 'simplified',
                    },
                ],
            },
            {
                featureType: 'road',
                elementType: 'all',
                stylers: [
                    {
                        saturation: '-100',
                    },
                ],
            },
            {
                featureType: 'road.highway',
                elementType: 'all',
                stylers: [
                    {
                        visibility: 'simplified',
                    },
                ],
            },
            {
                featureType: 'road.arterial',
                elementType: 'all',
                stylers: [
                    {
                        lightness: '30',
                    },
                ],
            },
            {
                featureType: 'road.local',
                elementType: 'all',
                stylers: [
                    {
                        lightness: '40',
                    },
                ],
            },
            {
                featureType: 'transit',
                elementType: 'all',
                stylers: [
                    {
                        saturation: -100,
                    },
                    {
                        visibility: 'simplified',
                    },
                ],
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [
                    {
                        hue: '#ffff00',
                    },
                    {
                        lightness: -25,
                    },
                    {
                        saturation: -97,
                    },
                ],
            },
            {
                featureType: 'water',
                elementType: 'labels',
                stylers: [
                    {
                        lightness: -25,
                    },
                    {
                        saturation: -100,
                    },
                ],
            },
        ],
    };
}
export default MyMap;
