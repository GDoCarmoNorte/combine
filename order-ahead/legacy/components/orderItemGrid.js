import { Box } from '@material-ui/core';
import NativeSelect from '@material-ui/core/NativeSelect';
import CheckIcon from '@material-ui/icons/Check';
import React, { useContext, useState } from 'react';
import { formatPrice } from '../lib/format';
import isMobileScreen from '../lib/isMobileScreen';
import { StoreContext } from '../state/store';
import styles from './itemGrid.module.css';
// get current location if not selected use 'all'
// load menu of the location only once
import ProductOrderPopup from './productOrderPopup';

const OrderItemGrid = (props) => {
    const {
        bag: { bag },
        appState: [appState, setAppState],
        brand,
    } = useContext(StoreContext);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [popupOpen, setPopupOpen] = useState(false);

    const openPopup = () => setPopupOpen(true);
    const closePopup = () => setPopupOpen(false);

    const menu = props.menu.subMenuObj;

    const category = appState && appState.category ? appState.category : Object.keys(menu)[0];

    const [selectedCategory, setSelectedCategory] = React.useState(category);

    const { products } = menu[category] ? menu[category] : {};
    const orderedItems = bag.reduce(
        (p, c) => ((p[c.product.productId] = (p[c.product.productId] || 0) + c.quantity), p),
        {}
    );
    // This was erroring when we added redux.  Just commenting out for now as we will likely depricate this logic.
    // products.forEach(p => p.quantity = orderedItems[p.productId] || 0);

    const categories = Object.values(menu).map((cat) => ({
        id: cat.subMenuId,
        name: cat.subMenu,
        quantity: cat.products.reduce((p, c) => ((p = p + (orderedItems[c.productId] || 0)), p), 0),
    }));

    const getBasePrice = (product) => {
        const activeTierAndPrices = product.activeTierAndPrices || [];
        const defaultPriceTier = activeTierAndPrices.find((tier) => tier.tier.default);
        const activePriceTier = activeTierAndPrices.find((tier) => tier.tier.type !== 'dynamic' && !tier.tier.default);

        if (defaultPriceTier) return defaultPriceTier.price;
        if (activePriceTier) return activePriceTier.price;
        return 4.72;
    };

    const getDynPrice = (product) =>
        ((product.activeTierAndPrices || []).find((t) => t.tier.type === 'dynamic') || { price: 4.72 }).price;
    const getDynPriceName = (product) => {
        let dynTier = (product.activeTierAndPrices || []).find((t) => t.tier.type === 'dynamic');
        return dynTier ? dynTier.tier.name : null;
    };

    const handleModifyProduct = (product) => () => {
        setSelectedProduct(product);
        openPopup();
    };

    function routeTo(nextCategory) {
        setAppState({ ...appState, category: nextCategory });
    }

    const notAvailableLabel = (
        <Box component="span" color="error.main">
            Not Available
        </Box>
    );

    const handleCategoryChange = (event) => {
        let id = event.target.value;
        setSelectedCategory(id);
        routeTo(id);
    };

    return (
        <>
            {isMobileScreen() && (
                <div className={styles.categorySelectContainer}>
                    <NativeSelect
                        id="order-category-select"
                        value={selectedCategory}
                        onChange={handleCategoryChange}
                        className={styles.categorySelect}
                    >
                        {categories.map(({ id, name, quantity }) => (
                            <option className={styles.categorySmall} onClick={() => routeTo(id)} key={id} value={id}>
                                {quantity > 0 ? name + ' *' : name}
                            </option>
                        ))}
                    </NativeSelect>
                </div>
            )}
            {!isMobileScreen() && (
                <div>
                    <div className={styles.categoryBar}>
                        {categories.map(({ id, name, quantity }) => (
                            <div
                                className={[styles.category, category === id ? styles.active : ''].join(' ')}
                                onClick={(d) => routeTo(id)}
                                key={id}
                            >
                                {quantity > 0 && <CheckIcon className={styles.categoryQuantity}></CheckIcon>}
                                {name}
                            </div>
                        ))}
                    </div>
                </div>
            )}
            <div className={styles.gridContainer}>
                {products.map((item) => (
                    <div
                        className={[styles.gridItemFlex, styles.order].join(' ')}
                        key={item.productId}
                        onClick={handleModifyProduct(item)}
                    >
                        {item.quantity > 0 && <div className={styles.quantity}>{item.quantity}</div>}
                        <div className={[styles.titleImageFlex, item.quantity ? styles.selected : ''].join(' ')}>
                            <span
                                className={styles.titleFlex}
                                dangerouslySetInnerHTML={{
                                    __html: item.productName,
                                }}
                            ></span>
                            {item.isAvailable !== null &&
                                typeof item.isAvailable !== 'undefined' &&
                                !item.isAvailable && (
                                    <div className={styles.availabilityLabel}>{notAvailableLabel}</div>
                                )}
                            <div className={styles.gridImageBox}>
                                <img className={styles.gridImageFlex} src={item.imageURL}></img>
                            </div>
                            {getDynPriceName(item) && (
                                <div className={styles.priceFlex} title={getDynPriceName(item)}>
                                    <span className={styles.lineThrough}>${formatPrice(getBasePrice(item))}</span>
                                    <span className={styles.dynamicPrice}>${formatPrice(getDynPrice(item))}</span>
                                </div>
                            )}
                            {!getDynPriceName(item) && (
                                <div className={styles.staticPrice}>${formatPrice(getBasePrice(item))}</div>
                            )}
                        </div>
                    </div>
                ))}
            </div>
            <ProductOrderPopup product={selectedProduct} open={popupOpen} onClose={closePopup} />
        </>
    );
};

export default OrderItemGrid;
