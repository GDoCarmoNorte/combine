import React, { useContext } from 'react';
import Link from 'next/link';

import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';

import styles from './bagButton.module.css';

import { StoreContext } from '../../state/store';

const anchorOrigin = { vertical: 'bottom', horizontal: 'right' };

const BagButton = (props) => {
    const {
        bag: { bag, setBagOpen },
        brand,
    } = useContext(StoreContext);
    const value = bag && bag.length;

    const openBag = () => setBagOpen(true);

    return (
        <IconButton onClick={openBag}>
            <Badge anchorOrigin={anchorOrigin} showZero={true} badgeContent={value} color="primary">
                <img className={styles.bagImage} src={`/brands/${brand.brand}/bag.svg`}></img>
            </Badge>
        </IconButton>
    );
};

export default BagButton;
