import React, { useContext, useState } from 'react';

import { withRouter } from 'next/router';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { useTheme } from '@material-ui/core/styles';

import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
import AccountBoxIcon from '@material-ui/icons/AccountBox';

import Link from 'next/link';

import { PersonOutline } from '@material-ui/icons';

import BagButton from './bagButton';
import BagDrawer from './bagDrawer';

import styles from './header.module.css';

import { StoreContext } from '../state/store';
import { useLocation } from '../redux/hooks';

const Header = (props) => {
    const [drawerOpen, setDrawerOpen] = useState(false);
    const { pathname } = props.router;

    const toggleDrawer = () => {
        setDrawerOpen(!drawerOpen);
    };

    const getLinkClasses = (path) => {
        return [styles.menus, pathname.startsWith(path) ? styles.active : ''].join(' ');
    };

    const {
        user: [user],
        brand,
    } = useContext(StoreContext);

    const { locationEntry: location } = useLocation();

    // let address = props.location.location;
    const address = location ? location.LocationName : '';
    const defaultMenuRoute = brand.brand === 'ARBYS' ? '/menu/674389620' : '/menu/wing-bundles';
    return (
        <>
            <div className={styles.header}>
                <div className={styles.logo}>
                    <a href="/">
                        <img src={`/brands/${brand.brand}/logo.svg`} alt="Logo" height="50"></img>
                    </a>
                </div>
                <div className={styles.menus}>
                    {location && (
                        <Link href="/order/[...location]" as={['/order/', location.LocationId].join('')}>
                            <a className={getLinkClasses('/order')}>Order</a>
                        </Link>
                    )}

                    {!location && (
                        <Link href="/locations">
                            <a className={getLinkClasses('/locations')}>Order</a>
                        </Link>
                    )}
                </div>
                <div className={styles.menus}>
                    <Link href="/menu/[category]" as={defaultMenuRoute}>
                        <a className={getLinkClasses('/menu')}>Menu</a>
                    </Link>
                </div>
                <div className={styles.custom}>
                    <div className={styles.pickup}>
                        <Link href="/locations">
                            <a className={styles.locations}>
                                <div className={styles.thin}>PICKUP FROM</div>
                                <div className={styles.red}>{address}</div>
                            </a>
                        </Link>
                    </div>
                    <div className={styles.signin}>
                        {user && (user.id || user.name) && (
                            <span className={styles.userName}>{user.name || user.id}</span>
                        )}
                        {(!user || !user.id) && (
                            <Link href="/signin">
                                <a>
                                    <PersonOutline />
                                    Sign In
                                </a>
                            </Link>
                        )}
                    </div>
                    <div className={styles.bag}>
                        <BagButton onClick={toggleDrawer} />
                    </div>
                </div>
            </div>
            <div className={styles.makeRoom} />
            <BagDrawer open={drawerOpen} onClose={toggleDrawer} />

            <AppBar position="static" color="transparent">
                <Toolbar>
                    <Link href="/">
                        <img src={`/brands/${brand.brand}/logo.svg`} alt="Logo" className={styles.logo}></img>
                    </Link>
                    <div className={styles.toolbox}>
                        <Button
                            href={defaultMenuRoute}
                            variant="text"
                            color="primary"
                            startIcon={<RestaurantMenuIcon />}
                        >
                            Menu
                        </Button>
                        {location && (
                            <Button
                                href={['/order/', location.LocationId].join('')}
                                variant="text"
                                color="primary"
                                startIcon={<LocalGroceryStoreIcon />}
                            >
                                Order
                            </Button>
                        )}
                        {!location && (
                            <Button
                                href="/locations"
                                variant="text"
                                color="primary"
                                startIcon={<LocalGroceryStoreIcon />}
                            >
                                Order
                            </Button>
                        )}
                        <Button
                            href="/locations"
                            variant="outlined"
                            color="primary"
                            startIcon={<PersonPinCircleIcon />}
                        >
                            Pickup From
                            <span>{address}</span>
                        </Button>
                    </div>
                    <IconButton href="/signin" color="primary">
                        <AccountBoxIcon />
                    </IconButton>
                    <BagButton onClick={toggleDrawer} />
                </Toolbar>
            </AppBar>
        </>
    );
};

export default withRouter(Header);
