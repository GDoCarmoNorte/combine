import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';

import styles from './productInfo.module.css';
import LocationBox from './locationBox';

export default function OrderConfirmation({ response, open, onClose }) {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));
    if (!response) {
        return <></>;
    }
    return (
        <div>
            <Dialog fullScreen={fullScreen} classes={{ paper: styles.popup }} open={open} onClose={onClose}>
                <MuiDialogTitle disableTypography className={styles.dialogTitle}>
                    <Typography align="center" variant="h6" color="primary">
                        Thank you for your order
                    </Typography>
                </MuiDialogTitle>
                <MuiDialogContent dividers align="center">
                    {response.location && (
                        <div className={styles.location}>
                            <Typography variant="h6">You order is being prepared at</Typography>
                            <LocationBox location={response.location}></LocationBox>
                        </div>
                    )}
                    {response.id && (
                        <div className={styles.orderId}>
                            Order Id: {response.id} - {response.destination}
                        </div>
                    )}
                </MuiDialogContent>
                <MuiDialogActions>
                    <Button onClick={onClose} color="primary" startIcon={<CloseIcon />}>
                        Close
                    </Button>
                </MuiDialogActions>
            </Dialog>
        </div>
    );
}
