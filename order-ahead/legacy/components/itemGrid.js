import React, { useState, useEffect, useContext } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

import { StoreContext } from '../../state/store';

import isMobileScreen from '../../lib/isMobileScreen';
import styles from './itemGrid.module.css';
import ProductInfoPopup from './productInfo';
import { Typography } from '@material-ui/core';

export default function ItemGrid(props) {
    // console.log(props.items.categories);
    const menu = props.menu.subMenuObj;
    const [category, setCategory] = useState(props.category);

    const categories = Object.values(menu).map((c) => ({ id: c.subMenuId, name: c.subMenu }));
    const { products } = menu[category] ? menu[category] : {};

    const { brand } = useContext(StoreContext);
    const flex = brand.brand !== 'ARBYS';

    const [product, setProduct] = useState({});
    const [includedItems, setIncludedItems] = useState([]);

    const [popupOpen, setPopupOpen] = useState(false);
    const openPopup = () => setPopupOpen(true);
    const closePopup = () => setPopupOpen(false);

    const handleClick = (nextProduct) => () => {
        setProduct(nextProduct);
        setIncludedItems(getIncludedItems(nextProduct.productId));
        openPopup();
    };

    function getIncludedItems(productId) {
        let idx = props.menu.products.reduce((p, c) => ((p[c.productId] = c), p), {});
        if (!idx[productId]) return [];
        let includedItemsIdx = props.menu.modifierItems.reduce((p, c) => ((p[c.productId] = c), p), {});
        return idx[productId].includedItems
            .map((item) => ({ ...item, product: includedItemsIdx[item.itemId] }))
            .filter((i) => i.product);
    }

    const router = useRouter();

    function routeTo(nextCategory) {
        // useEffect(() => {
        // Always do navigations after the first render
        setCategory(nextCategory);
        router.push(`/menu/[category]`, `/menu/${nextCategory}`, { shallow: true });
        //    }, [])
    }
    //    console.log(category, products, categories);

    const handleCategoryChange = (event) => {
        setCategory(event.target.value);
    };

    return (
        <>
            {isMobileScreen() && (
                <div className={styles.categorySelectContainer}>
                    <NativeSelect
                        id="menu-category-select"
                        value={category}
                        onChange={handleCategoryChange}
                        className={styles.categorySelect}
                    >
                        {categories.map(({ id, name }) => (
                            <option className={styles.categorySmall} key={id} value={id}>
                                {name}
                            </option>
                        ))}
                    </NativeSelect>
                </div>
            )}
            {!isMobileScreen() && (
                <div>
                    <div className={styles.categoryBar}>
                        {categories.map(({ id, name }) => (
                            <Link href="/menu/[category]" as={['/menu/', id].join('')} key={id}>
                                <a
                                    className={[styles.category, category === id ? styles.active : ''].join(' ')}
                                    onClick={(d) => routeTo(id)}
                                    key={id}
                                >
                                    {name}
                                </a>
                            </Link>
                        ))}
                    </div>
                </div>
            )}
            <div className={styles.gridContainer}>
                {products.map((item) => (
                    <div
                        className={flex ? styles.gridItemFlex : styles.gridItem}
                        key={item.productId}
                        onClick={handleClick(item)}
                    >
                        <span
                            className={flex ? styles.titleFlex : styles.title}
                            dangerouslySetInnerHTML={{ __html: item.productName }}
                        ></span>
                        <div className={flex ? styles.gridImageBox : styles.imageBox}>
                            <img className={flex ? styles.gridImageFlex : styles.gridImage} src={item.imageURL}></img>
                        </div>
                    </div>
                ))}
            </div>
            <ProductInfoPopup product={product} includedItems={includedItems} open={popupOpen} onClose={closePopup} />
        </>
    );
}
