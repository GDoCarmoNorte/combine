import React, { useState, useContext } from 'react';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Typography from '@material-ui/core/Typography';
import styles from './locationBox.module.css';

export default function LocationBox({ location }) {
    if (!location) return <></>;
    return (
        <div className={styles.locationBox}>
            <div className={styles.locationDetails}>
                <div className={styles.locationLine}>
                    <LocationOnIcon color="primary" />
                    <Typography color="primary">{location.LocationName || location.locationName}</Typography>
                </div>
                <div className={styles.locationLine}>
                    <span className={styles.locationItem}>{location.AddressLine1 || location.addressLine1}</span>
                </div>
                {location.Phone && (
                    <div className={styles.locationLine}>
                        <Typography className={styles.locationLabel} variant="caption">
                            Phone:
                        </Typography>
                        <span className={styles.locationItem}>{location.Phone}</span>
                    </div>
                )}
            </div>
        </div>
    );
}
