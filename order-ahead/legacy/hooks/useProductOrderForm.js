/* eslint-disable no-use-before-define */
import { useState, useEffect } from 'react';

export default function useDomainProductOrderForm(product) {
    const [modifiersWithValue, setModifiersWithValue] = useState(null);

    const handleModifierSelect = (groupId, modifier) => {
        const updatedModifiers = updateModifiersWithValue(modifiersWithValue, groupId, modifier);

        setModifiersWithValue(updatedModifiers);
    };

    useEffect(() => {
        if (product) {
            setModifiersWithValue(getInitialModifiersValue(product));
        }
    }, [product]);

    return [modifiersWithValue, handleModifierSelect];
}

function getInitialModifiersValue(product) {
    const modifierGroups = product.modifierGroupsWithItems;
    const includedModifiers = product.includedItems;

    const modifiersWithValue = modifierGroups.map((group) => {
        const initialModifierValue = findIncludedModifierByGroupId(includedModifiers, group.modifierGroupId);

        if (!initialModifierValue) {
            return { ...group, selectedModifier: null };
        }

        // TODO: make API unified with productId and itemId naming and then remove renaming on frontend
        initialModifierValue.productId = initialModifierValue.itemId;

        const groupItem =
            group.modifierItems &&
            group.modifierItems.find((item) => item.productId === initialModifierValue.productId);

        if (groupItem) {
            initialModifierValue.name = groupItem.name;
        }

        return { ...group, selectedModifier: initialModifierValue };
    });

    return modifiersWithValue;
}

function findIncludedModifierByGroupId(includedModifiers, groupId) {
    return includedModifiers.find((modifier) => modifier.modifierGroupId === groupId);
}

function updateModifiersWithValue(groupsWithValues, groupId, modifier) {
    const copy = groupsWithValues.slice();
    const groupToUpdate = findModifierGroupById(groupsWithValues, groupId);

    groupToUpdate.selectedModifier = modifier;

    return copy;
}

function findModifierGroupById(modifierGroups, id) {
    return modifierGroups.find((group) => group.modifierGroupId === id);
}
