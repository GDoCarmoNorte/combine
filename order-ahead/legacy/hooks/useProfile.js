import { useContext, useEffect, useState } from 'react';

import { getUserProfile } from '../lib/user';
import { StoreContext } from '../state/store';

export const useUserProfile = () => {
    const {
        user: [user],
    } = useContext(StoreContext);
    const [profile, setProfile] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        let isMounted = true;

        const dispose = () => {
            isMounted = false;
        };

        if (user && user.authorized) {
            setIsLoading(true);

            getUserProfile()
                .then((nextProfile) => setProfile(nextProfile))
                .finally(() => setIsLoading(false));
        }

        return dispose;
    }, [user]);

    return [profile, isLoading];
};
