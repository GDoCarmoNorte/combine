import { useState, useEffect } from 'react';

import { getDeviceId } from '../lib/deviceId';
import { getUser } from '../lib/user';

export const useUser = () => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        const deviceId = getDeviceId();
        getUser(deviceId).then((nextUser) => {
            setUser(nextUser);
        });
    }, []);

    return [user, setUser];
};
