import { useState, useEffect } from 'react';

export default function useDomainProductPriceTier(product) {
    const [priceTier, setPriceTier] = useState(null);

    const getActivePriceTier = (tiers) => {
        if (!tiers) return null;
        const dynamicPriceTier = tiers.find((tier) => tier.tier.type === 'dynamic');
        const defaultPriceTier = tiers.find((tier) => tier.tier.default);
        const activePriceTier = tiers.find((tier) => tier.tier.type !== 'dynamic' && !tier.tier.default);

        return dynamicPriceTier || activePriceTier || defaultPriceTier;
    };

    useEffect(() => {
        if (product) {
            const activePriceTier = getActivePriceTier(product.activeTierAndPrices);
            setPriceTier(activePriceTier);
        }
    }, [product]);

    return [priceTier];
}
