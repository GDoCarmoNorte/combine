import { useState, useContext } from 'react';

import { submitOrder } from '../lib/order';
import { StoreContext } from '../state/store';
import { useLocation } from '../redux/hooks';

import usePasswordless from './usePasswordless';

export default function useSubmitOrder() {
    const {
        user: [user],
        bag: { bag },
    } = useContext(StoreContext);

    const { locationEntry: location } = useLocation();
    const [isSubmitted, setIsSubmitted] = useState(false);
    const [response, setResponse] = useState(null);

    const [userInfo, setUserInfo, ticket, setCode, isLoading, setIsLoading, isOTPComplete] = usePasswordless();

    const submit = async () => {
        const userHasSession = user.session || user.authorized;

        if (userHasSession) {
            setIsLoading(true);

            console.log('[DEBUG]', 'SUBMIT!!!', userInfo.firstName);

            const res = await submitOrder(
                { ...user, firstName: userInfo.firstName, lastName: userInfo.lastName },
                location,
                bag
            );
            setResponse(res);
            setIsLoading(false);
            setIsSubmitted(true);

            return response;
        }
    };

    // useEffect(() => setIsSubmitted(false), [bag]);

    return [
        userInfo,
        setUserInfo,
        ticket,
        setCode,
        submit,
        isLoading,
        setIsLoading,
        isSubmitted,
        isOTPComplete,
        response,
    ];
}
