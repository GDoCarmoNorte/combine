import { useState, useEffect, useContext } from 'react';

import { passwordlessStart, passwordlessVerify, passwordlessPrepare } from '../lib/passwordless';
import { StoreContext } from '../state/store';

const getOldEmail = (user, ticket) => {
    return (ticket && ticket.email) || (user && user.session && user.email);
};

const getOldPhone = (user, ticket) => {
    return (ticket && ticket.phone) || (user && user.session && user.user_metadata && user.user_metadata.phone_number);
};

const usePasswordless = () => {
    const {
        user: [user, setUser],
    } = useContext(StoreContext);

    const [userInfo, setUserInfo] = useState({});

    const [ticket, setTicket] = useState(null);
    const [code, setCode] = useState(null);

    const [isOtpComplete, setIsOtpComplete] = useState(false);

    const [isLoading, setIsLoading] = useState(false);

    const handleError = (err) => {
        const { response } = err;
        const { email } = userInfo;

        if (response.status === 409) {
            setTicket({
                error: {
                    status: response.status,
                    statusText: response.statusText,
                    message: `${email} is already registered, please, sign in to proceed checkout`,
                },
            });
        }
    };

    useEffect(() => {
        const { phone, email } = userInfo;

        if (user && (email || phone)) {
            const oldEmail = getOldEmail(user, ticket);
            const oldPhone = getOldPhone(user, ticket);
            const hasUpdates = (email && email !== oldEmail) || (phone && phone !== oldPhone);

            // Authorized user should submit order directly
            if (user.authorized) {
                throw new Error('Only anonymous or semi-anonymous users are supported');
            }

            if (user.session) {
                if (hasUpdates) {
                    setIsLoading(true);
                    passwordlessPrepare({ ...user, ...userInfo })
                        .then((updatedUser) => {
                            setUser({ ...updatedUser, session: user.session });
                            setIsLoading(false);
                            setIsOtpComplete(true);
                        })
                        .catch(handleError);
                } else {
                    setIsOtpComplete(true);
                }
            } else if (hasUpdates) {
                setIsLoading(true);
                passwordlessStart({ ...user, ...userInfo })
                    .then(({ user: updatedUser, ticket: nextTicket }) => {
                        if (updatedUser.session) {
                            setUser(updatedUser);
                            setIsLoading(false);
                            setIsOtpComplete(true);
                        } else {
                            setTicket(nextTicket);
                            setUser(updatedUser);
                        }
                    })
                    .catch(handleError)
                    .finally(() => {
                        setIsLoading(false);
                    });
            }
        }
    }, [user, userInfo]);

    useEffect(() => {
        if (code) {
            setIsLoading(true);

            passwordlessVerify({ ...user, ...userInfo }, { ...ticket, code })
                .then((verifiedUser) => {
                    console.log(verifiedUser);

                    setIsLoading(false);
                    if (verifiedUser.session) {
                        setUser(verifiedUser);
                        setTicket(null);
                        setCode(null);
                    }
                })
                .catch((error) => {
                    console.warn(error);

                    setIsLoading(false);
                    setTicket(null);
                    setCode(null);
                })
                .finally(() => {
                    setIsOtpComplete(true);
                });
        }
    }, [code]);

    return [userInfo, setUserInfo, ticket, setCode, isLoading, setIsLoading, isOtpComplete];
};

export default usePasswordless;
