import { useState, useEffect, useContext } from 'react';

import { getOrderHistory } from '../lib/user';
import { StoreContext } from '../../state/store';
import { useLocation } from '../../redux/hooks';

export default function useOrderHistory() {
    const {
        user: [user],
        bag: { restoreBag },
    } = useContext(StoreContext);

    const {
        actions: { setLocation },
    } = useLocation();

    const [orders, setOrders] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);

    const fetch = async (displayLoading = true) => {
        setIsLoading(displayLoading);

        const fetchedOrders = await getOrderHistory(user);

        setOrders(fetchedOrders);
        setIsLoading(false);
        setIsLoaded(true);
    };

    useEffect(() => {
        let poller;

        const poll = function poll() {
            poller = setTimeout(() => fetch(false).then(poll), 30000);
        };

        // TODO: refactor user to have unified structure and use only user_id
        if (user && (user.user_id || user.sub)) {
            fetch().then(poll);
        }

        return () => poller && clearTimeout(poller);
    }, [user]);

    const reorder = (id) => {
        const order = orders.find((item) => item.id === id);

        const {
            location: { locationId, addressLine1, locationName },
        } = order;

        if (order) {
            setLocation({
                locationEntry: {
                    LocationId: locationId,
                    AddressLine1: addressLine1,
                    LocationName: locationName,
                },
            });
            restoreBag(order);
        }

        return order;
    };

    return [orders, isLoading, isLoaded, reorder];
}
