import React from 'react';
import { useRouter } from 'next/router';

import getBrand from '../../../src/brand';
import arbysGenMenu from '../../lib/arbysGenMenu';
import bwwGenMenu from '../../lib/bwwGenMenu';

import ItemGrid from '../../components/itemGrid';
import withLayout from '../../components/withLayout';

function Menu(props) {
    const router = useRouter();
    const { category } = router.query;

    return <ItemGrid category={category} menu={props.menu} />;
}

export default withLayout(Menu);

const getMenu = async () => {
    const brand = getBrand(process.env.NEXT_PUBLIC_BRAND);

    // TODO: add empty brand error
    if (brand.brand === 'arbys') return await arbysGenMenu();
    if (brand.brand === 'bww') return await bwwGenMenu();
};

export const getStaticPaths = async () => {
    const menu = await getMenu();
    const paths = Object.keys(menu.subMenuObj).map((c) => ({ params: { category: c } }));

    return { paths, fallback: false };
};

export const getStaticProps = async ({ params }) => {
    const menuData = await getMenu();
    // const ret = { props: { menu: menuData[params.category], categories: Object.values(menuData).map(c => ({id: c.subMenuId, name: c.subMenu}))}}
    const ret = { props: { menu: menuData } };

    return ret;
};

/*
export async function getServerSideProps({ params }) {
    const menuData = getMenu();
    return { props: { menu: menuData.filter(m => m.subMenuId === params.category) , categories: categoryIdNameList(menuData)} }
}
*/
