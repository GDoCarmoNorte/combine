import React from 'react';
import { useRouter } from 'next/router';

import { CircularProgress } from '@material-ui/core';
import OrderHistory from '../components/orderHistory';
import withLayout from '../components/withLayout';
import useOrderHistory from '../hooks/useOrderHistory';

import styles from '../components/orderHistory/index.module.css';

function OrderHistoryPage() {
    const router = useRouter();
    const [orders, isLoading, isLoaded, reorder] = useOrderHistory();

    const onReorder = (orderId) => {
        const order = reorder(orderId);

        if (order) {
            router.push('/checkout');
        }
    };

    if (isLoading || !isLoaded)
        return (
            <div className={styles.loader}>
                <CircularProgress />
            </div>
        );

    if (!orders && isLoaded) return <p>No orders</p>;

    return <OrderHistory orders={orders} onReorder={onReorder}></OrderHistory>;
}

export default withLayout(OrderHistoryPage);
