import React, { useState, useContext, useEffect } from 'react';
import { useRouter } from 'next/router';

import Alert from '@material-ui/lab/Alert';

import CheckoutComponent from '../components/checkout/checkout-component';
import withLayout from '../components/withLayout';

import useSubmitOrder from '../hooks/useSubmitOrder';
// import usePasswordless from '../hooks/usePasswordless';

import * as gtag from '../../lib/gtag';

import styles from '../components/checkout/index.module.css';
import { StoreContext } from '../state/store';
import { updateUser } from '../lib/user';
import OrderConfirmation from '../components/orderConfirmation';
import { createTransaction } from '../components/checkout/checkout-transaction';

const Checkout = () => {
    const router = useRouter();
    const {
        brand,
        user: [user, setUser],
        bag: { bag, clearBag },
    } = useContext(StoreContext);

    const [
        userInfo,
        setUserInfo,
        ticket,
        setCode,
        submit,
        isLoading,
        setIsLoading,
        isSubmitted,
        isOTPComplete,
        response,
    ] = useSubmitOrder();

    const showVerification = ticket && (ticket.Id || ticket.mfa_token);
    const verificationMessage = showVerification
        ? `Enter the code you have received on your ${userInfo.phone ? 'phone' : 'email'}`
        : null;

    const errorMessage = ticket && ticket.error && ticket.error.message;

    const onCheckout = async (nextUserInfo) => {
        if (user && user.authorized) {
            setIsLoading(true);

            const updatedUser = await updateUser({ ...nextUserInfo, user_id: user.user_id });
            setUser(updatedUser);

            submit();
        } else {
            setUserInfo(nextUserInfo);
        }
    };

    const onVerify = (code) => {
        setCode(code);
    };
    const [popupOpen, setPopupOpen] = useState(false);

    const openPopup = () => setPopupOpen(true);

    const closePopup = () => {
        setPopupOpen(false);
        router.push('/');
    };

    useEffect(() => {
        if (isOTPComplete) {
            submit();
        }
    }, [isOTPComplete]);

    useEffect(() => {
        if (isSubmitted && response) {
            gtag.purchase(createTransaction(bag, response, brand));

            clearBag();
            setPopupOpen(true);
        }
    }, [isSubmitted, response]);

    return (
        <>
            <div className={styles.checkoutContainer}>
                {errorMessage && (
                    <Alert className={styles.alert} variant="outlined" severity="error">
                        {errorMessage}
                    </Alert>
                )}
                <CheckoutComponent
                    isLoading={isLoading}
                    isSubmitted={isSubmitted}
                    verificationMessage={verificationMessage}
                    verify={onVerify}
                    checkout={onCheckout}
                />
            </div>
            <OrderConfirmation response={response} open={popupOpen} onClose={closePopup} />
        </>
    );
};

export default withLayout(Checkout);
