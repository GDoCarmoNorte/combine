import React from 'react';
import OAHeader from '../../components/oaHeader';

import styles from '../../components/layout.module.css';

const EmbeddedHeader = (props) => {
    return (
        <div className={styles.layout}>
            <OAHeader location={props.location} target="_top" />
        </div>
    );
};

export default EmbeddedHeader;
