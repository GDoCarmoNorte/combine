import React from 'react';

import UserProfile from '../components/userProfile/userProfile';
import withLayout from '../components/withLayout';
import { useUserProfile } from '../hooks/useProfile';

const Profile = () => {
    const [profile, profileLoading] = useUserProfile();

    if (profileLoading) {
        return <div>Loading...</div>;
    }

    return profile ? <UserProfile user={profile} /> : <div>No user found!</div>;
};

export default withLayout(Profile);
