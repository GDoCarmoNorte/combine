import React from 'react';

import OrderItemGrid from '../../components/orderItemGrid';
import inspireMenu from '../../lib/menu';
import withLayout from '../../components/withLayout';

function Order(props) {
    const { locationId, menu, category } = props;

    return <OrderItemGrid locationId={locationId} menu={menu} category={category} />;
}

export default withLayout(Order);

export async function getServerSideProps({ params }) {
    const locationId = params.location[0];
    const menu = await inspireMenu(locationId);
    const category = params.category || Object.keys(menu)[0];

    return { props: { locationId, menu, category } };
}
