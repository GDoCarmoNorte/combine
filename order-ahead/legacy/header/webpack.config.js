const path = require('path');
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');

// eslint-disable-next-line import/no-extraneous-dependencies
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'production',
    entry: {
        'header-widget': [path.resolve(__dirname, './index.js')],
    },
    output: {
        path: path.resolve(__dirname, '../../public', 'widgets', 'header-widget'),
        filename: '[name].bundle.js',
    },
    resolve: {
        extensions: ['.js', '.mjs', '.jsx', '.json', '.wasm'],
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ['@babel/preset-env', { useBuiltIns: 'entry', corejs: 3, targets: '> 1%, not dead' }],
                                '@babel/preset-react',
                            ],
                        },
                    },
                ],
                exclude: [/node_modules/],
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, { loader: 'css-loader' }],
            },
        ],
    },
    // optimization: {
    //     splitChunks: {
    //         chunks: 'all',
    //     },
    // },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].bundle.css',
        }),
        new webpack.DefinePlugin({
            DOMAIN: JSON.stringify('http://52.255.177.221:3000'),
            BRAND: JSON.stringify(process.env.NEXT_PUBLIC_BRAND),
        }),
    ],
};
