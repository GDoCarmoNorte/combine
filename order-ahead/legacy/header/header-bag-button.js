import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';

import '../../components/bagButton.module.css';

const anchorOrigin = { vertical: 'bottom', horizontal: 'right' };

const BagButton = ({ domain, brand }) => {
    const value = 0;

    // const openBag = () => setBagOpen(true);

    return (
        <IconButton>
            <Badge anchorOrigin={anchorOrigin} showZero={true} badgeContent={value} color="primary">
                <img className="bagImage" src={`${domain}/brands/${brand.brand}/bag.svg`}></img>
            </Badge>
        </IconButton>
    );
};

export default BagButton;
