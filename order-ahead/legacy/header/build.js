// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');
const config = require('./webpack.config');

console.log('[WIDGET]', 'build header widget');

webpack(config, (err, stats) => {
    if (err) {
        console.error(err);
        return;
    }

    console.log(
        stats.toString({
            chunks: false,
            colors: true,
        })
    );
});
