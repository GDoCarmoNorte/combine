import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

// import MenuIcon from '@material-ui/icons/Menu';
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore';
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle';
// import AccountBoxIcon from '@material-ui/icons/AccountBox';
// import LocationOnIcon from '@material-ui/icons/LocationOn';
import NotListedLocationIcon from '@material-ui/icons/NotListedLocation';
// import PersonIcon from '@material-ui/icons/Person';
// import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import EventNoteIcon from '@material-ui/icons/EventNote';

// import { Menu, MenuItem, Divider, ListItemIcon, Typography } from '@material-ui/core';

import BagButton from './header-bag-button';

import '../../components/toolbar.module.css';

const HeaderWidget = ({ domain, brand }) => {
    return (
        <>
            <AppBar position="static" color="transparent">
                <Toolbar></Toolbar>{' '}
            </AppBar>
            <AppBar position="fixed" color="default">
                <Toolbar>
                    <div className="toolbox largeScreen">
                        <a href={domain}>
                            <img
                                alt="Logo"
                                src={`${domain}/brands/${brand.brand}/logo.svg`}
                                className="logo"
                                height="36px"
                            ></img>
                        </a>
                        <a href={domain}>
                            <Button variant="text" color="primary" startIcon={<RestaurantMenuIcon />}>
                                Menu
                            </Button>
                        </a>
                        <a href={domain}>
                            <Button variant="text" color="primary" startIcon={<LocalGroceryStoreIcon />}>
                                Order
                            </Button>
                        </a>
                        <a href={`${domain}/locations`}>
                            <Button variant="outlined" color="primary" startIcon={<PersonPinCircleIcon />}>
                                <span className="located">Pick up from</span>
                                <span className="pickupAt"></span>
                            </Button>
                        </a>
                        <a href={`${domain}/order-history`}>
                            <IconButton color="primary">
                                <EventNoteIcon />
                            </IconButton>
                        </a>
                        <BagButton domain={domain} brand={brand} />
                    </div>
                    <div className="menubox smallScreen">
                        <div className="smallToolbarBlock left">
                            <a href={domain}>
                                <IconButton color="primary">
                                    <RestaurantMenuIcon />{' '}
                                </IconButton>
                            </a>
                            <a href={domain}>
                                <IconButton color="primary">
                                    <LocalGroceryStoreIcon />{' '}
                                </IconButton>
                            </a>
                            <a href={`${domain}/locations`}>
                                <IconButton color="primary">
                                    <NotListedLocationIcon />
                                </IconButton>
                            </a>
                        </div>
                        <div className="smallToolbarBlock center">
                            <a href={domain}>
                                <IconButton color="primary">
                                    <img
                                        alt="Logo"
                                        src={`${domain}/brands/${brand.brand}/logo.svg`}
                                        className="logo"
                                        height="36px"
                                    ></img>
                                </IconButton>
                            </a>
                        </div>
                        <div className="smallToolbarBlock right">
                            <BagButton domain={domain} brand={brand} />
                        </div>
                    </div>
                </Toolbar>
            </AppBar>
        </>
    );
};

export default HeaderWidget;
