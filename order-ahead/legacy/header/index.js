/*eslint import/no-unresolved: 0*/
import 'core-js';
import 'regenerator-runtime/runtime';

import React from 'react';
import ReactDOM from 'react-dom';

import { ThemeProvider, StylesProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import HeaderWidget from './header-widget';
import createBrandTheme from '../../src/theme';

import '../../styles/global.css';
import '../../public/brands/arbys/global.css';

// eslint-disable-next-line no-undef
const brand = { brand: BRAND };

// eslint-disable-next-line no-undef
const domain = DOMAIN;

const brandTheme = createBrandTheme(brand.brand);

// eslint-disable-next-line no-undef
ReactDOM.render(
    <StylesProvider injectFirst>
        <ThemeProvider theme={brandTheme}>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <HeaderWidget domain={domain} brand={brand} />
        </ThemeProvider>
    </StylesProvider>,
    document.getElementById('header-widget')
);
