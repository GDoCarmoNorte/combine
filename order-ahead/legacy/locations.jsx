import React, { useContext } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';

import { StoreContext } from '../state/store';
import { useLocation } from '../redux/hooks';
import { LocationEntry } from '../redux/location';

import LocationSelection from '../components/location';
import Map from '../components/location/storeLocation/map';
import LocationList from '../components/location/storeLocation/locationList';
import withLayout from './components/withLayout';
import isMobileScreen from '../lib/isMobileScreen';

function locations(props) {
    console.log('[DEBUG]', 'locations', props);

    const { brand } = useContext(StoreContext);
    const {
        actions: { setLocation },
    } = useLocation();
    const router = useRouter();

    console.log('[DEBUG]', 'locations', props);

    const displayMap = !isMobileScreen();

    function onLocationSelect(place: LocationEntry) {
        setLocation({ locationEntry: place });
        // move to /order/store
        router.push('/order/[...location]', `/order/${place.LocationId}`);
    }

    async function onLocationSearch(value) {
        const res1 = await axios.get(`/api/locationSearch?q=${value}`);
        return res1.data;
    }

    return (
        <>
            <LocationSelection
                headerLabel={`Welcome to ${brand.brandName}!`}
                inputPlaceholder={`Find an ${brand.brandName}`}
                defaultLocationString="Alpharetta, GA 30004"
                onLocationSearch={onLocationSearch}
                onLocationSelect={onLocationSelect}
                renderLocationList={(locationList, onLocationSelect) => (
                    <LocationList locationList={locationList} onLocationSelect={onLocationSelect}></LocationList>
                )}
                renderMap={(locationList, onLocationSelect) =>
                    displayMap && <Map locations={locationList} onLocationSelect={onLocationSelect}></Map>
                }
            />
        </>
    );
}

export default locations;
