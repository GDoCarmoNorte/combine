import auth0 from '../../lib/auth0';

export default async function me(req, res) {
    try {
        const tokenCache = await auth0.tokenCache(req, res);
        const { accessToken } = await tokenCache.getAccessToken();
        const session = await auth0.getSession(req);
        res.status(200).json({
            accessToken,
            idToken: session.idToken,
        });
        // auth0.tokenCache
        await auth0.handleProfile(req, res);
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).end(error.message);
    }
}
