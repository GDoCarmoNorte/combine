import { HttpError } from '../../lib/http-error';
import { getOrCreateAnonymousUser } from '../../lib/anonymous';
import { getSession } from '../../lib/auth0-api';

const anonymous = async (req, res) => {
    try {
        const { deviceId } = req.body;
        const session = await getSession(req);

        if (session || typeof deviceId === 'undefined') {
            throw new HttpError(400, 'Bad request');
        }

        switch (req.method) {
            case 'POST': {
                const user = await getOrCreateAnonymousUser(deviceId);

                console.log('[DEBUG]', __filename, user);

                res.json(user);
                break;
            }
            default:
                throw new HttpError(405, 'Method Not Allowed');
        }
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
};

export default anonymous;
