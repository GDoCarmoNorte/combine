import fetch from 'isomorphic-unfetch';

import config from '../../lib/config';
import { HttpError } from '../../lib/http-error';
import { getAccessToken, getSession } from '../../lib/auth0-api';

const prepareMetadata = ({ name, email, nickname, ...userMetadata }) => {
    const metadata = {};

    if (name) {
        metadata.name = name;
    }
    if (email) {
        metadata.email = email;
    }
    if (nickname) {
        metadata.nickname = nickname;
    }
    if (userMetadata && Object.keys(userMetadata).length > 0) {
        metadata.user_metadata = userMetadata;
    }

    return metadata;
};

const getUserMetadata = async (req, res, userId) => {
    const accessToken = await getAccessToken();
    const result = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users/${userId}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
        },
    });

    if (result.ok) {
        const metadata = await result.json();
        const [firstName, lastName] = metadata.nickname.split(' ');

        const data = {
            user_id: metadata.user_id,
            name: metadata.name,
            email: metadata.email,
            nickname: metadata.nickname,
            app_metadata: metadata.app_metadata,
            firstName: metadata.firstName || firstName || '',
            lastName: metadata.lastName || lastName || '',
            ...metadata.user_metadata,
        };

        res.status(200).json(data);
    } else {
        throw new HttpError(result.status, result.statusText);
    }
};

const updateUserMetadata = async (req, res, userId) => {
    const body = prepareMetadata(req.body);

    if (Object.keys(body).length < 1) {
        throw new HttpError(400, 'Bad request');
    }

    const accessToken = await getAccessToken();
    const result = await fetch(`https://${config.AUTH0_DOMAIN}/api/v2/users/${userId}`, {
        method: 'PATCH',
        headers: {
            Authorization: `Bearer ${accessToken}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    });

    if (result.ok) {
        const metadata = await result.json();

        console.log('[DEBUG]', metadata);

        res.status(200).json({
            name: metadata.name,
            email: metadata.email,
            nickname: metadata.nickname,
            ...metadata.user_metadata,
        });
    } else {
        throw new HttpError(result.status, result.statusText);
    }
};

const userMetadata = async (req, res) => {
    try {
        const session = await getSession(req);

        if (!session) {
            throw new HttpError(401, 'Unauthorized');
        }

        switch (req.method) {
            case 'GET':
                await getUserMetadata(req, res, session.user.sub);
                break;
            case 'PUT':
            case 'POST':
                await updateUserMetadata(req, res, session.user.sub);
                break;
            default:
                throw new HttpError(405, 'Method Not Allowed');
        }
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
};

export default userMetadata;
