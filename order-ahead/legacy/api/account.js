import { updateAccount } from '../../lib/account';
import { HttpError } from '../../lib/http-error';
import { createProfile } from '../../lib/user';
import { getSession } from '../../lib/auth0-api';

export default async function account(req, res) {
    try {
        const session = await getSession(req);

        if (!session) {
            throw new HttpError(401, 'Unauthorized');
        }

        switch (req.method) {
            case 'POST': {
                const { user: userInfo } = req.body;

                if (typeof userInfo === 'undefined') {
                    throw new HttpError(404, 'Not found');
                }

                const user = await updateAccount(userInfo);

                res.json(createProfile(user));
                break;
            }
            default:
                throw new HttpError(405, 'Method Not Allowed');
        }
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
}
