import inspireMenu from '../../lib/menu';

export default async function getMenu(req, res) {
    const { locationId } = req.query;
    try {
        console.log('INFO API menu', locationId);
        const menu = await inspireMenu(locationId);
        return res.json({ locationId, menu });
    } catch (err) {
        console.log('ERROR getMenu for location', locationId, err);
        res.status(500).json(err);
    }
}
