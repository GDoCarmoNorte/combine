import axios from 'axios';

import { verifySession } from '../../lib/session';

const { ORDER_DOMAIN_SERVICE_URL } = process.env;

const parseOrderItems = (items) => {
    const orderItems = [];
    const result = {};

    items.forEach((item) => {
        if (!result[item.itemId]) {
            result[item.itemId] = [];
        }
        result[item.itemId].push(item);
    });

    // TODO: add modifier support
    Object.values(result).forEach((entries) => {
        orderItems.push({ ...entries[0], quantity: entries.length });
    });

    return orderItems;
};

export default async function orderHistory(req, res) {
    try {
        // await verifySession(req);
        // TODO: refactor user to have unified structure and use only user_id
        const userId = req.body.user.user_id || req.body.user.sub;
        console.log('DEBUG Getting order history', userId);
        console.time('TIME Order history', userId);
        const response = await axios.get(`${ORDER_DOMAIN_SERVICE_URL}/order/customer/${userId}`);
        console.timeEnd('TIME Order history', userId);

        const orderList = response.data.map((order) => {
            return { ...order, items: parseOrderItems(order.items) };
        });

        res.status(200).json(orderList);
    } catch (error) {
        res.status(error.response.status || 500).end(error.response.data.message);
    }
}
