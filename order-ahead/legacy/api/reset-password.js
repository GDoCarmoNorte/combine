import { HttpError } from '../../lib/http-error';
import { resetPassword } from '../../lib/reset-password';
import { getSession } from '../../lib/auth0-api';

const postPasswordReset = async (req, res, userId) => {
    const resetTicket = await resetPassword(userId);

    res.writeHead(302, {
        Location: resetTicket.ticket,
    });
    res.end();
};

const passwordReset = async (req, res) => {
    try {
        const session = await getSession(req);

        if (!session) {
            throw new HttpError(401, 'Unauthorized');
        }

        await postPasswordReset(req, res, session.user.sub);
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
};

export default passwordReset;
