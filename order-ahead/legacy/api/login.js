import auth0 from '../../lib/auth0';

export default async function login(req, res) {
    try {
        console.log('[DEBUG]', __filename, req.url, req.query);

        const authParams = {};
        const { user_id: userId, email } = req.query;
        const options = { redirectTo: req.query.target_url };

        if (userId) {
            const tokens = userId.split('|');

            authParams.secondary_user_id = userId;
            authParams.device_id = tokens[tokens.length - 1];
        }
        if (email) {
            authParams.email = req.query.email;
        }
        if (Object.keys(authParams).length > 0) {
            options.authParams = authParams;
        }

        await auth0.handleLogin(req, res, options);
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).end(error.message);
    }
}
