import { HttpError } from '../../lib/http-error';
import { getAccount } from '../../lib/account';
import { createProfile } from '../../lib/user';
import { getSession } from '../../lib/auth0-api';

export default async function me(req, res) {
    try {
        const session = await getSession(req);

        if (!session) {
            throw new HttpError(401, 'Unauthorized');
        }

        switch (req.method) {
            case 'GET': {
                const user = await getAccount({ user_id: session.user.sub });

                if (typeof user === 'undefined') {
                    throw new HttpError(404, 'Not found');
                }

                res.json(createProfile(user));
                break;
            }
            default:
                throw new HttpError(405, 'Method Not Allowed');
        }
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
}
