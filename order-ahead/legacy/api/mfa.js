import { HttpError } from '../../lib/http-error';
import { getMfaTicket, verifyMfaTicket } from '../../lib/mfa';

const mfa = async (req, res) => {
    let result;

    try {
        const { user, ticket } = req.body;

        switch (req.method) {
            case 'POST': {
                const { phone_number: phone } = user.user_metadata;

                if (typeof phone === 'undefined') {
                    throw new HttpError(400, 'Bad request');
                }

                const mfaTicket = await getMfaTicket(user);

                console.log('[DEBUG]', 'mfa ticket created: ', mfaTicket);

                res.json(mfaTicket);
                break;
            }
            case 'PUT':
                if (typeof ticket === 'undefined') {
                    throw new HttpError(400, 'Bad request');
                }

                result = await verifyMfaTicket(user, ticket);
                res.json(result);
                break;
            default:
                throw new HttpError(405, 'Method not allowed');
        }
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
};

export default mfa;
