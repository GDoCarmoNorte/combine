import fetch from 'isomorphic-unfetch';

import config from '../../lib/config';

import { HttpError } from '../../lib/http-error';
import { fraudCheck } from '../../lib/fraud-check';
import { updateAccount, findAccount, linkAccounts } from '../../lib/account';
import { getSession, authorize } from '../../lib/auth0-api';

const getAccount = async (email) => {
    const account = await findAccount(email);

    if (account) {
        account.isAnonymous = email.endsWith('@anonymous.com');
    }

    return account;
};

// see - https://manage.auth0.com/dashboard/us/poc-inspirebrands/rules/rul_eL95Ygts91xuDoxp
const preparePasswordless = async (user) => {
    // isEmailTrusted = false requests MFA
    const account = await getAccount(user.email);

    // Do not allow passwordless for already existing authorized account (DP)
    if (account && account.user_metadata && account.user_metadata.accountType === 'authorized') {
        throw new HttpError(409, 'Conflict');
    }

    // TODO: should we always make email fraud check even if user uses mfa with phone?
    const isEmailTrusted = user.phone
        ? undefined
        : process.env.FRAUD_CHECK_ENABLED
        ? await fraudCheck(user.email)
        : true;

    console.log('[DEBUG]', __filename, 'is email trusted: ', isEmailTrusted);

    // If semi-anonymous account is already exists, use it for passwordless
    const options = account && account.isAnonymous === false ? { ...account } : { ...user };

    options.user_metadata = { ...options.user_metadata, isEmailTrusted };

    let updatedUser = await updateAccount(options);

    // TODO: accounts should be linked only after email verification
    if (account && account.user_id !== user.user_id) {
        const [userIdentity] = user.identities;
        const identities = await linkAccounts(account.user_id, {
            user_id: userIdentity.user_id,
            provider: userIdentity.provider,
        });

        updatedUser.identities = identities;
    }

    if (isEmailTrusted) {
        const { email, user_metadata: metadata } = updatedUser;
        const session = await authorize(email, metadata.device_id);

        updatedUser = { ...updatedUser, session };
    }

    console.log('[DEBUG]', __filename, 'updated user: ', updatedUser);

    return updatedUser;
};

const verifyPasswordless = async (user, ticket) => {
    const { email } = user;
    const { code } = ticket;

    const res = await fetch(`https://${config.AUTH0_DOMAIN}/oauth/token`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            grant_type: 'http://auth0.com/oauth/grant-type/passwordless/otp',
            client_id: config.AUTH0_N_CLIENT_ID,
            scope: config.AUTH0_SCOPE,
            realm: 'email',
            username: email,
            otp: code,
        }),
    });

    if (res.ok) {
        const session = await res.json();

        return { ...user, session };
    }

    throw new HttpError(res.status, res.statusText);
};

const passwordless = async (req, res) => {
    try {
        let result;
        const session = await getSession(req);

        if (session) {
            throw new HttpError(400, 'Bad request');
        }

        const { user, ticket } = req.body;

        console.log('[DEBUG]', 'email ticket: ', ticket);

        if (typeof user === 'undefined') {
            throw new HttpError(400, 'Bad request');
        }

        switch (req.method) {
            case 'POST':
                if (typeof user.email === 'undefined') {
                    throw new HttpError(400, 'Bad request');
                }

                result = await preparePasswordless(user);

                res.json(result);
                break;
            case 'PUT': {
                const { code } = ticket;

                if (typeof code === 'undefined') {
                    throw new HttpError(400, 'Bad request');
                }

                result = await verifyPasswordless(user, ticket);

                res.json(result);
                break;
            }
            default:
                throw new HttpError(405, 'Method not allowed');
        }
    } catch (error) {
        res.status(error.status || 500).end(error.message);
    }
};

export default passwordless;
