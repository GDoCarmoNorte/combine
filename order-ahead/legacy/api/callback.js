import auth0 from '../../lib/auth0';
import { checkPasswordTicket } from '../../lib/password-ticket';

export default async function callback(req, res) {
    try {
        console.log('[DEBUG]', __filename, req.url, req.query);

        await auth0.handleCallback(req, res, {
            onUserLoaded: async (request, response, session, state) => {
                await checkPasswordTicket(session.user.sub);

                return { ...session };
            },
        });
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).end(error.message);
    }
}
