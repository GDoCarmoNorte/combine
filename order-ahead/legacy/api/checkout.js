import axios from 'axios';

import { verifySession } from '../../lib/session';
import { HttpError } from '../../lib/http-error';

const { ORDER_DOMAIN_SERVICE_URL } = process.env;

export default async function checkout(req, res) {
    try {
        await verifySession(req);

        if (typeof req.body.order === 'undefined') {
            throw new HttpError(400, 'Bad request');
        }

        console.log('DEBUG Submiting order');
        console.time('Submiting order');

        const response = await axios.post(`${ORDER_DOMAIN_SERVICE_URL}/order`, req.body.order);

        console.timeEnd('Submiting order');

        res.status(200).json(response.data);
    } catch (error) {
        console.error(error);
        res.status(error.status || 500).end(error.message);
    }
}
