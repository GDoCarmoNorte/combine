import React, { useState } from 'react';

import useBag from './useBag';
import { useUser } from '../hooks/useUser';

// https://dev.to/nazmifeeroz/using-usecontext-and-usestate-hooks-as-a-store-mnm

// TODO: do we need brand in context?
//       bag and location uses brand in key
export const StoreContext = React.createContext(null);

export default ({ children, brand }) => {
    const [user, setUser] = useUser();
    const [bag, addToBag, removeFromBag, clearBag, restoreBag, updateInBag, getFromBag] = useBag({
        brand,
        user,
    });

    const [appState, setAppState] = useState({});
    const [bagOpen, setBagOpen] = useState(false);

    const store = {
        user: [user, setUser],
        bag: {
            bag,
            addToBag,
            removeFromBag,
            clearBag,
            restoreBag,
            bagOpen,
            setBagOpen,
            updateInBag,
            getFromBag,
        },
        appState: [appState, setAppState],
        brand,
    };

    return <StoreContext.Provider value={store}>{children}</StoreContext.Provider>;
};
