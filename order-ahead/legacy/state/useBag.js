import { useEffect, useState } from 'react';
import * as gtag from '../lib/gtag';
import { getItem, setItem } from '../lib/storage';

const useBag = ({ brand, user }) => {
    const [bagAttribute, setBagAttribute] = useState(null);
    const [bag, setBag] = useState([]);

    useEffect(() => {
        if (user) {
            setBagAttribute(`_bag:${brand.brand}:${user.user_id}`);
        }
    }, [user]);

    useEffect(() => {
        if (bagAttribute) {
            setBag(getItem(bagAttribute) || []);
        }
    }, [bagAttribute]);

    const addToBag = (product, modifiers, priceTier, quantity = 1) => {
        const bagCopy = bag.slice();
        bagCopy.push({ product, modifiers, priceTier, quantity });

        setBag(bagCopy);
        setItem(bagAttribute, bagCopy);
        console.log('bagCopy', bagCopy);
    };

    const updateInBag = (index, { product, modifiers, priceTier, quantity = 1 }) => {
        if (typeof index !== 'number' && index < 0) return;

        const bagCopy = bag.slice();

        bagCopy[index] = { product, modifiers, priceTier, quantity };
        setBag(bagCopy);
        setItem(bagAttribute, bagCopy);
    };

    const removeFromBag = (productIndex) => {
        const bagCopy = bag.slice();
        //const productIndex = bagCopy.findIndex((product) => product.id === productId);
        let productId = bag[productIndex].product.productId;

        bagCopy.splice(productIndex, 1);
        gtag.event({
            action: `${productId} item removed from cart`,
            category: 'ecommerce',
            label: 'Remove from cart',
            value: productId,
        });
        setBag(bagCopy);
        setItem(bagAttribute, bagCopy);
    };

    const restoreBag = (order) => {
        const items = order.items.map((item) => ({
            product: {
                ...item,
                productId: item.itemId,
                name: item.description,
            },
            modifiers: [],
            priceTier: { price: item.price },
            quantity: item.quantity,
        }));

        setBag(items);
        setItem(bagAttribute, items);
    };

    const getFromBag = (product) => {
        return bag.find((item) => item.product.productId === product.productId);
    };

    const clearBag = () => {
        setBag([]);
        setItem(bagAttribute, []);
    };
    return [bag, addToBag, removeFromBag, clearBag, restoreBag, updateInBag, getFromBag];
};

export default useBag;
