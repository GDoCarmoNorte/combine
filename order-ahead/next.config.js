const withFonts = require('next-fonts');

const setFeatureFlags = require('./webpack/setFeatureFlags');
const componentToggler = require('./webpack/componentToggler');

// TODO: refactor
// same here webpack/getConfig.js & config/config.ts
const DEFAULT_LOCALE = 'en-us';

const hasNonDefaultLocale = typeof process.env.LOCALE !== 'undefined' && process.env.LOCALE !== DEFAULT_LOCALE;

module.exports = withFonts({
    enableSvg: true,
    trailingSlash: true,
    webpack(config) {
        const configWithFeatureFlags = setFeatureFlags(config);
        return componentToggler(configWithFeatureFlags);
    },
    productionBrowserSourceMaps: true,
    ...(hasNonDefaultLocale && { basePath: `/${process.env.LOCALE}` }),
});
