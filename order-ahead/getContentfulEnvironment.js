/* eslint-disable @typescript-eslint/no-var-requires */
// THIS FILE IS USED TO CREATE CONTENTFUL TYPES
// SEE https://github.com/intercom/contentful-typescript-codegen

require('dotenv').config();

const contentfulManagement = require('contentful-management');
const parser = require('yargs-parser');

// see lib/cli.ts
const CONTENTFUL_ENV_KEY = 'contentful-env';

const argv = parser(process.argv.slice(2), {
    alias: {
        [CONTENTFUL_ENV_KEY]: ['e'],
    },
});

const { CONTENTFUL_ENV: PROCESS_CONTENFUL_ENV, CONTENTFUL_SPACE, CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN } = process.env;
const CONTENTFUL_ENV = argv[CONTENTFUL_ENV_KEY] || PROCESS_CONTENFUL_ENV;

module.exports = function () {
    const contentfulClient = contentfulManagement.createClient({
        accessToken: CONTENTFUL_MANAGEMENT_API_ACCESS_TOKEN,
    });

    console.log('[CONTENTFUL]', 'generating contentful typescript definition from:', CONTENTFUL_ENV);

    return contentfulClient.getSpace(CONTENTFUL_SPACE).then((space) => space.getEnvironment(CONTENTFUL_ENV));
};
