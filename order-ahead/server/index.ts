import { createServer } from 'http';
import { parse } from 'url';
import next from 'next';
import { getAndWriteContentfulProps } from '../common/services/globalContentfulProps/getAndWriteContentfulProps';
import { getAndWriteContentfulConfiguration } from '../common/services/contentfulConfiguration/contentfulConfigurationNode';

const port = parseInt(process.env.PORT || '3000', 10);
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(async () => {
    await getAndWriteContentfulConfiguration({ preview: false });

    createServer(async (req, res) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const parsedUrl = parse(req.url!, true);
        // filter all .js, .css, .woff requests
        !req.url.includes('.') && (await getAndWriteContentfulProps({ preview: false }));
        handle(req, res, parsedUrl);
    }).listen(port);

    // tslint:disable-next-line:no-console
    console.log(`> Server listening at http://localhost:${port} as ${dev ? 'development' : process.env.NODE_ENV}`);
});
