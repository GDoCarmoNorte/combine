// This snippet comes from the Optimize team, and is loaded in the Optimize front-end.
// Added here for posterity, original story: https://inspirebrands.atlassian.net/browse/DBBP-33052
function gtag() {
    dataLayer.push(arguments);
}
function initExperience() {
    document.cookie = 'customerType=PC';
}
gtag('event', 'optimize.callback', { name: '7jPW4nY6TwiIc5e9_7BZMQ', callback: initExperience });
window.addEventListener('DOMContentLoaded', function (event) {
    initExperience();
});
