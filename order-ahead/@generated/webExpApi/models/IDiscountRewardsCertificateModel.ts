/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface IDiscountRewardsCertificateModel
 */
export interface IDiscountRewardsCertificateModel {
    /**
     * 
     * @type {string}
     * @memberof IDiscountRewardsCertificateModel
     */
    id: string;
}

export function IDiscountRewardsCertificateModelFromJSON(json: any): IDiscountRewardsCertificateModel {
    return IDiscountRewardsCertificateModelFromJSONTyped(json, false);
}

export function IDiscountRewardsCertificateModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): IDiscountRewardsCertificateModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': json['id'],
    };
}

export function IDiscountRewardsCertificateModelToJSON(value?: IDiscountRewardsCertificateModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
    };
}


