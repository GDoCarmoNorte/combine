/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * 
 * @export
 * @enum {string}
 */
export enum GoogleButtonColorEnumModel {
    Black = 'BLACK',
    White = 'WHITE'
}

export function GoogleButtonColorEnumModelFromJSON(json: any): GoogleButtonColorEnumModel {
    return GoogleButtonColorEnumModelFromJSONTyped(json, false);
}

export function GoogleButtonColorEnumModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): GoogleButtonColorEnumModel {
    return json as GoogleButtonColorEnumModel;
}

export function GoogleButtonColorEnumModelToJSON(value?: GoogleButtonColorEnumModel | null): any {
    return value as any;
}

