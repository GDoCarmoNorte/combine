/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * 
 * @export
 * @enum {string}
 */
export enum TRewardOfferStatusModel {
    Active = 'ACTIVE',
    Inactive = 'INACTIVE',
    Locked = 'LOCKED',
    Redeemed = 'REDEEMED'
}

export function TRewardOfferStatusModelFromJSON(json: any): TRewardOfferStatusModel {
    return TRewardOfferStatusModelFromJSONTyped(json, false);
}

export function TRewardOfferStatusModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): TRewardOfferStatusModel {
    return json as TRewardOfferStatusModel;
}

export function TRewardOfferStatusModelToJSON(value?: TRewardOfferStatusModel | null): any {
    return value as any;
}

