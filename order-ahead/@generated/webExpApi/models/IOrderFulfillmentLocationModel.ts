/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    IOrderFulfillmentLocationContactDetailsModel,
    IOrderFulfillmentLocationContactDetailsModelFromJSON,
    IOrderFulfillmentLocationContactDetailsModelFromJSONTyped,
    IOrderFulfillmentLocationContactDetailsModelToJSON,
} from './';

/**
 * 
 * @export
 * @interface IOrderFulfillmentLocationModel
 */
export interface IOrderFulfillmentLocationModel {
    /**
     * 
     * @type {string}
     * @memberof IOrderFulfillmentLocationModel
     */
    id?: string;
    /**
     * 
     * @type {string}
     * @memberof IOrderFulfillmentLocationModel
     */
    displayName?: string;
    /**
     * 
     * @type {string}
     * @memberof IOrderFulfillmentLocationModel
     */
    url?: string;
    /**
     * 
     * @type {IOrderFulfillmentLocationContactDetailsModel}
     * @memberof IOrderFulfillmentLocationModel
     */
    contactDetails?: IOrderFulfillmentLocationContactDetailsModel;
}

export function IOrderFulfillmentLocationModelFromJSON(json: any): IOrderFulfillmentLocationModel {
    return IOrderFulfillmentLocationModelFromJSONTyped(json, false);
}

export function IOrderFulfillmentLocationModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): IOrderFulfillmentLocationModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'displayName': !exists(json, 'displayName') ? undefined : json['displayName'],
        'url': !exists(json, 'url') ? undefined : json['url'],
        'contactDetails': !exists(json, 'contactDetails') ? undefined : IOrderFulfillmentLocationContactDetailsModelFromJSON(json['contactDetails']),
    };
}

export function IOrderFulfillmentLocationModelToJSON(value?: IOrderFulfillmentLocationModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'displayName': value.displayName,
        'url': value.url,
        'contactDetails': IOrderFulfillmentLocationContactDetailsModelToJSON(value.contactDetails),
    };
}


