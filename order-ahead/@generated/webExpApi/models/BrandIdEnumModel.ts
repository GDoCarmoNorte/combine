/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * 
 * @export
 * @enum {string}
 */
export enum BrandIdEnumModel {
    Arb = 'ARB',
    Bww = 'BWW',
    Sdi = 'SDI',
    Jje = 'JJE'
}

export function BrandIdEnumModelFromJSON(json: any): BrandIdEnumModel {
    return BrandIdEnumModelFromJSONTyped(json, false);
}

export function BrandIdEnumModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): BrandIdEnumModel {
    return json as BrandIdEnumModel;
}

export function BrandIdEnumModelToJSON(value?: BrandIdEnumModel | null): any {
    return value as any;
}

