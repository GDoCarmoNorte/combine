/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    TErrorCodeEXTERNALModel,
    TErrorCodeEXTERNALModelFromJSON,
    TErrorCodeEXTERNALModelFromJSONTyped,
    TErrorCodeEXTERNALModelToJSON,
    TErrorCodeVALIDATIONModel,
    TErrorCodeVALIDATIONModelFromJSON,
    TErrorCodeVALIDATIONModelFromJSONTyped,
    TErrorCodeVALIDATIONModelToJSON,
} from './';

/**
 * 
 * @export
 * @interface TError400ResponseCodeModel
 */
export interface TError400ResponseCodeModel {
}

export function TError400ResponseCodeModelFromJSON(json: any): TError400ResponseCodeModel {
    return TError400ResponseCodeModelFromJSONTyped(json, false);
}

export function TError400ResponseCodeModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): TError400ResponseCodeModel {
    return json;
}

export function TError400ResponseCodeModelToJSON(value?: TError400ResponseCodeModel | null): any {
    return value;
}


