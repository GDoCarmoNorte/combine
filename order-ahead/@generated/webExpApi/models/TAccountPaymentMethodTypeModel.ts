/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

/**
 * Account Payment Method Type
 * @export
 * @enum {string}
 */
export enum TAccountPaymentMethodTypeModel {
    Ax = 'AX',
    Am = 'AM',
    Ds = 'DS',
    Mc = 'MC',
    Vi = 'VI',
    Gc = 'GC',
    Jc = 'JC',
    Cb = 'CB',
    Dn = 'DN',
    Un = 'UN'
}

export function TAccountPaymentMethodTypeModelFromJSON(json: any): TAccountPaymentMethodTypeModel {
    return TAccountPaymentMethodTypeModelFromJSONTyped(json, false);
}

export function TAccountPaymentMethodTypeModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): TAccountPaymentMethodTypeModel {
    return json as TAccountPaymentMethodTypeModel;
}

export function TAccountPaymentMethodTypeModelToJSON(value?: TAccountPaymentMethodTypeModel | null): any {
    return value as any;
}

