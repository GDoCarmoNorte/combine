/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    IError500ExternalResponseQRCodeModel,
    IError500ExternalResponseQRCodeModelFromJSON,
    IError500ExternalResponseQRCodeModelFromJSONTyped,
    IError500ExternalResponseQRCodeModelToJSON,
} from './';

/**
 * 
 * @export
 * @interface IError500ExternalResponseQRModel
 */
export interface IError500ExternalResponseQRModel {
    /**
     * 
     * @type {IError500ExternalResponseQRCodeModel}
     * @memberof IError500ExternalResponseQRModel
     */
    code: IError500ExternalResponseQRCodeModel;
    /**
     * 
     * @type {string}
     * @memberof IError500ExternalResponseQRModel
     */
    message: string;
}

export function IError500ExternalResponseQRModelFromJSON(json: any): IError500ExternalResponseQRModel {
    return IError500ExternalResponseQRModelFromJSONTyped(json, false);
}

export function IError500ExternalResponseQRModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): IError500ExternalResponseQRModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'code': IError500ExternalResponseQRCodeModelFromJSON(json['code']),
        'message': json['message'],
    };
}

export function IError500ExternalResponseQRModelToJSON(value?: IError500ExternalResponseQRModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'code': IError500ExternalResponseQRCodeModelToJSON(value.code),
        'message': value.message,
    };
}


