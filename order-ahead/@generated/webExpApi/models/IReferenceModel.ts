/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface IReferenceModel
 */
export interface IReferenceModel {
    /**
     * 
     * @type {string}
     * @memberof IReferenceModel
     */
    type: string;
    /**
     * 
     * @type {string}
     * @memberof IReferenceModel
     */
    id: string;
}

export function IReferenceModelFromJSON(json: any): IReferenceModel {
    return IReferenceModelFromJSONTyped(json, false);
}

export function IReferenceModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): IReferenceModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'type': json['type'],
        'id': json['id'],
    };
}

export function IReferenceModelToJSON(value?: IReferenceModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'type': value.type,
        'id': value.id,
    };
}


