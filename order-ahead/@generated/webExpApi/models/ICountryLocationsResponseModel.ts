/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    ILocationsByCountryModel,
    ILocationsByCountryModelFromJSON,
    ILocationsByCountryModelFromJSONTyped,
    ILocationsByCountryModelToJSON,
} from './';

/**
 * 
 * @export
 * @interface ICountryLocationsResponseModel
 */
export interface ICountryLocationsResponseModel {
    /**
     * 
     * @type {number}
     * @memberof ICountryLocationsResponseModel
     */
    totalCount: number;
    /**
     * 
     * @type {Array<ILocationsByCountryModel>}
     * @memberof ICountryLocationsResponseModel
     */
    locationsByCountry?: Array<ILocationsByCountryModel>;
}

export function ICountryLocationsResponseModelFromJSON(json: any): ICountryLocationsResponseModel {
    return ICountryLocationsResponseModelFromJSONTyped(json, false);
}

export function ICountryLocationsResponseModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): ICountryLocationsResponseModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'totalCount': json['totalCount'],
        'locationsByCountry': !exists(json, 'locationsByCountry') ? undefined : ((json['locationsByCountry'] as Array<any>).map(ILocationsByCountryModelFromJSON)),
    };
}

export function ICountryLocationsResponseModelToJSON(value?: ICountryLocationsResponseModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'totalCount': value.totalCount,
        'locationsByCountry': value.locationsByCountry === undefined ? undefined : ((value.locationsByCountry as Array<any>).map(ILocationsByCountryModelToJSON)),
    };
}


