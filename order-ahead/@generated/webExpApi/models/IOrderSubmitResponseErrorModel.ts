/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface IOrderSubmitResponseErrorModel
 */
export interface IOrderSubmitResponseErrorModel {
    /**
     * 
     * @type {string}
     * @memberof IOrderSubmitResponseErrorModel
     */
    code: string;
    /**
     * 
     * @type {string}
     * @memberof IOrderSubmitResponseErrorModel
     */
    reasonCode: string;
    /**
     * 
     * @type {string}
     * @memberof IOrderSubmitResponseErrorModel
     */
    message: string;
}

export function IOrderSubmitResponseErrorModelFromJSON(json: any): IOrderSubmitResponseErrorModel {
    return IOrderSubmitResponseErrorModelFromJSONTyped(json, false);
}

export function IOrderSubmitResponseErrorModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): IOrderSubmitResponseErrorModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'code': json['code'],
        'reasonCode': json['reasonCode'],
        'message': json['message'],
    };
}

export function IOrderSubmitResponseErrorModelToJSON(value?: IOrderSubmitResponseErrorModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'code': value.code,
        'reasonCode': value.reasonCode,
        'message': value.message,
    };
}


