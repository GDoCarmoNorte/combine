/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface VersionResponseModel
 */
export interface VersionResponseModel {
    /**
     * 
     * @type {string}
     * @memberof VersionResponseModel
     */
    version: string;
}

export function VersionResponseModelFromJSON(json: any): VersionResponseModel {
    return VersionResponseModelFromJSONTyped(json, false);
}

export function VersionResponseModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): VersionResponseModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'version': json['version'],
    };
}

export function VersionResponseModelToJSON(value?: VersionResponseModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'version': value.version,
    };
}


