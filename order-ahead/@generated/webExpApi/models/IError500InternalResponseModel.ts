/* tslint:disable */
/* eslint-disable */
/**
 * web-exp-api
 * BFF (backend for frontend) for Order Ahead (OA)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    TErrorCodeModel,
    TErrorCodeModelFromJSON,
    TErrorCodeModelFromJSONTyped,
    TErrorCodeModelToJSON,
    TErrorTypeINTERNALModel,
    TErrorTypeINTERNALModelFromJSON,
    TErrorTypeINTERNALModelFromJSONTyped,
    TErrorTypeINTERNALModelToJSON,
} from './';

/**
 * 
 * @export
 * @interface IError500InternalResponseModel
 */
export interface IError500InternalResponseModel {
    /**
     * 
     * @type {TErrorCodeModel}
     * @memberof IError500InternalResponseModel
     */
    code: TErrorCodeModel;
    /**
     * 
     * @type {string}
     * @memberof IError500InternalResponseModel
     */
    message: string;
    /**
     * 
     * @type {TErrorTypeINTERNALModel}
     * @memberof IError500InternalResponseModel
     */
    type: TErrorTypeINTERNALModel;
}

export function IError500InternalResponseModelFromJSON(json: any): IError500InternalResponseModel {
    return IError500InternalResponseModelFromJSONTyped(json, false);
}

export function IError500InternalResponseModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): IError500InternalResponseModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'code': TErrorCodeModelFromJSON(json['code']),
        'message': json['message'],
        'type': TErrorTypeINTERNALModelFromJSON(json['type']),
    };
}

export function IError500InternalResponseModelToJSON(value?: IError500InternalResponseModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'code': TErrorCodeModelToJSON(value.code),
        'message': value.message,
        'type': TErrorTypeINTERNALModelToJSON(value.type),
    };
}


