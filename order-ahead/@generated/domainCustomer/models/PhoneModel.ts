/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface PhoneModel
 */
export interface PhoneModel {
    /**
     * 
     * @type {string}
     * @memberof PhoneModel
     */
    number: string;
    /**
     * 
     * @type {boolean}
     * @memberof PhoneModel
     */
    isPreferred?: boolean;
}

export function PhoneModelFromJSON(json: any): PhoneModel {
    return PhoneModelFromJSONTyped(json, false);
}

export function PhoneModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): PhoneModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'number': json['number'],
        'isPreferred': !exists(json, 'isPreferred') ? undefined : json['isPreferred'],
    };
}

export function PhoneModelToJSON(value?: PhoneModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'number': value.number,
        'isPreferred': value.isPreferred,
    };
}


