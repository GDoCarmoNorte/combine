/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
import {
    PreferenceReponseDtoModel,
    PreferenceReponseDtoModelFromJSON,
    PreferenceReponseDtoModelFromJSONTyped,
    PreferenceReponseDtoModelToJSON,
} from './';

/**
 * 
 * @export
 * @interface CustomerSignUpResponseDtoModel
 */
export interface CustomerSignUpResponseDtoModel {
    /**
     * Id
     * @type {string}
     * @memberof CustomerSignUpResponseDtoModel
     */
    id?: string;
    /**
     * 
     * @type {PreferenceReponseDtoModel}
     * @memberof CustomerSignUpResponseDtoModel
     */
    preferences?: PreferenceReponseDtoModel;
}

export function CustomerSignUpResponseDtoModelFromJSON(json: any): CustomerSignUpResponseDtoModel {
    return CustomerSignUpResponseDtoModelFromJSONTyped(json, false);
}

export function CustomerSignUpResponseDtoModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): CustomerSignUpResponseDtoModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'id': !exists(json, 'id') ? undefined : json['id'],
        'preferences': !exists(json, 'preferences') ? undefined : PreferenceReponseDtoModelFromJSON(json['preferences']),
    };
}

export function CustomerSignUpResponseDtoModelToJSON(value?: CustomerSignUpResponseDtoModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'id': value.id,
        'preferences': PreferenceReponseDtoModelToJSON(value.preferences),
    };
}


