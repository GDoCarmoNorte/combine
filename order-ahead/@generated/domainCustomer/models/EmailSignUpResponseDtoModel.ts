/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * Email SingUp
 * @export
 * @interface EmailSignUpResponseDtoModel
 */
export interface EmailSignUpResponseDtoModel {
    /**
     * Status
     * @type {string}
     * @memberof EmailSignUpResponseDtoModel
     */
    status?: EmailSignUpResponseDtoModelStatusEnum;
}

/**
* @export
* @enum {string}
*/
export enum EmailSignUpResponseDtoModelStatusEnum {
    Published = 'PUBLISHED',
    Received = 'RECEIVED'
}

export function EmailSignUpResponseDtoModelFromJSON(json: any): EmailSignUpResponseDtoModel {
    return EmailSignUpResponseDtoModelFromJSONTyped(json, false);
}

export function EmailSignUpResponseDtoModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): EmailSignUpResponseDtoModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'status': !exists(json, 'status') ? undefined : json['status'],
    };
}

export function EmailSignUpResponseDtoModelToJSON(value?: EmailSignUpResponseDtoModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'status': value.status,
    };
}


