/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * Preferred location
 * @export
 * @interface PreferredLocationDtoModel
 */
export interface PreferredLocationDtoModel {
    /**
     * Name
     * @type {string}
     * @memberof PreferredLocationDtoModel
     */
    name?: string;
    /**
     * Store Id
     * @type {string}
     * @memberof PreferredLocationDtoModel
     */
    storeId?: string;
}

export function PreferredLocationDtoModelFromJSON(json: any): PreferredLocationDtoModel {
    return PreferredLocationDtoModelFromJSONTyped(json, false);
}

export function PreferredLocationDtoModelFromJSONTyped(json: any, ignoreDiscriminator: boolean): PreferredLocationDtoModel {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'name': !exists(json, 'name') ? undefined : json['name'],
        'storeId': !exists(json, 'storeId') ? undefined : json['storeId'],
    };
}

export function PreferredLocationDtoModelToJSON(value?: PreferredLocationDtoModel | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'name': value.name,
        'storeId': value.storeId,
    };
}


