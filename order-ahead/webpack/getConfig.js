const path = require('path');

const DEFAULT_LOCALE = 'en-us';

module.exports = ({ logger }) => {
    const envName = process.env.ENVIRONMENT_NAME; // eg: 'arb-dev01' or 'prd01'
    const brandName = process.env.NEXT_PUBLIC_BRAND; // eg: 'arbys'
    const locale = process.env.LOCALE; // eg: 'en-us'
    const brandNameWithLocale = locale === DEFAULT_LOCALE ? brandName : `${brandName}-${locale}`;

    console.log(path.resolve(__dirname));
    console.log(path.resolve(__dirname, '../'));
    const rootDir = path.resolve(__dirname, '../');
    const environmentNameTokens = envName.split('-');
    let brandPrefix = environmentNameTokens[0];

    if (environmentNameTokens.length === 1) {
        // 'stg01' & 'prd01' scenarios for arbys
        // all other brands will be "brand-env"
        brandPrefix = 'arb';
    }
    const devEnvironment = `${brandPrefix}-dev01`; // always from dev environment

    const toggleComponentsPath = path.resolve(
        rootDir,
        `multiBrand/config/${devEnvironment}/${brandNameWithLocale}.json`
    );
    const toggleFeaturesPath = path.resolve(rootDir, `multiBrand/config/${envName}/${brandNameWithLocale}.json`);

    logger.info(`- Grabbing the multi brand config file for "toggleComponents" from ${toggleComponentsPath}`);
    logger.info(`- Grabbing the multi brand config file for "toggleFeatures" from ${toggleFeaturesPath}`);

    try {
        return {
            toggleComponents: require(toggleComponentsPath).toggleComponents || {},
            toggleFeatures: require(toggleFeaturesPath).toggleFeatures || {},
        };
        // toggleComponents: {} will not toggle any component
        // toggleFeatures: {} will not disable ant feature(all features are enabled by default)
    } catch (e) {
        logger.warn(
            `- Config file(s) not found at "${toggleComponentsPath}" or "${toggleFeaturesPath}", using empty multi-brand configs`
        );
        logger.warn({ e });
        return { toggleComponents: {}, toggleFeatures: {} };
    }
};
