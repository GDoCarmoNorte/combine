// See` CONVENTIONS.md -> Inspire-OA feature flagging

const chalk = require('chalk');
const { DefinePlugin } = require('webpack');
const getConfig = require('./getConfig');
const getLogger = require('./getLogger');

module.exports = (webpackConfig) => {
    const envName = process.env.ENVIRONMENT_NAME;
    const brandName = process.env.NEXT_PUBLIC_BRAND;
    const locale = process.env.LOCALE;

    const logger = getLogger(
        'feature flags',
        `environment: ${chalk.black.bgWhite(envName)}; brand: ${chalk.black.bgWhite(
            brandName
        )}; locale: ${chalk.black.bgWhite(locale)}`
    );

    logger.info('- Injecting feature flags configs');
    const { toggleFeatures } = getConfig({ logger });
    const toggleFeaturesEntries = Object.entries(toggleFeatures);

    let featureFlagConfigs = {};
    if (toggleFeaturesEntries.length === 0) {
        logger.info('- No feature flags to inject');
    } else {
        featureFlagConfigs = toggleFeaturesEntries.reduce((accumulator, [flag, meta]) => {
            accumulator[flag] = !!meta.enabled;
            return accumulator;
        }, featureFlagConfigs);
        logger.info(`- Prepare ${toggleFeaturesEntries.length} feature flags for injecting`);
    }

    webpackConfig.plugins.push(new DefinePlugin({ FEATURE_FLAGS: JSON.stringify(featureFlagConfigs) }));
    logger.info('- Finished!');
    logger.br();

    return webpackConfig;
};
