// See` CONVENTIONS.md -> Inspire-OA build-time component toggling

const os = require('os');
const path = require('path');
const chalk = require('chalk');
const { NormalModuleReplacementPlugin } = require('webpack');
const getConfig = require('./getConfig');
const getLogger = require('./getLogger');

module.exports = (webpackConfig) => {
    const envName = process.env.ENVIRONMENT_NAME;
    const brandName = process.env.NEXT_PUBLIC_BRAND;
    const locale = process.env.LOCALE;

    const logger = getLogger(
        'component toggler',
        `environment: ${chalk.black.bgWhite(envName)}; brand: ${chalk.black.bgWhite(
            brandName
        )}; locale: ${chalk.black.bgWhite(locale)}`
    );
    const pathSlash = os.platform() === 'win32' ? '\\\\' : '/';

    const { toggleComponents } = getConfig({ logger });
    const toggleComponentEntries = Object.entries(toggleComponents);

    if (toggleComponentEntries.length === 0) {
        logger.info('- No components to replace');
        return webpackConfig;
    }

    logger.info('- Registering components to replace');
    let replaceComponentsCount = 0;
    toggleComponentEntries.forEach(([defaultComponent, brandComponent]) => {
        replaceComponentsCount += 1;

        let absolutePath;
        let brandComponentPath;
        const rootPath = path.resolve(__dirname, '../');
        const componentPath = 'components';

        if (!defaultComponent.includes('auth0')) {
            absolutePath = path.resolve(rootPath, componentPath, defaultComponent);
            brandComponentPath = path.resolve(rootPath, 'multiBrand', componentPath, brandComponent);
        } else {
            const getComponentPath = (path) => path.split('/', -1).slice(1).join('/');
            absolutePath = path.resolve(rootPath, 'auth0/src/', componentPath, getComponentPath(defaultComponent));
            brandComponentPath = path.resolve(
                rootPath,
                'multiBrand/auth0',
                componentPath,
                getComponentPath(brandComponent)
            );
        }

        const pathArray = absolutePath.split(path.sep);
        const pathRegex = new RegExp(pathArray.join(pathSlash), 'gi');

        logger.info(`${pathRegex} will be replace to ${brandComponentPath}`);
        webpackConfig.plugins.unshift(
            new NormalModuleReplacementPlugin(pathRegex, (resource) => {
                logger.info(
                    `- Replacing ${resource.createData.resource} component to ${brandComponentPath} in ${resource.createData.context}`
                );
                if (resource.createData) {
                    resource.createData.resource = brandComponentPath;
                    resource.createData.context = path.dirname(brandComponentPath);
                }
            })
        );
    });

    logger.br();
    logger.info(`- ${replaceComponentsCount} components will be replaced`);
    logger.br();

    return webpackConfig;
};
