const chalk = require('chalk');

module.exports = (resource, postfix = '') => {
    const resourceToUpperCased = resource.toUpperCase();
    const postfixText = postfix.length ? ` | ${postfix}` : '';

    const addPostfix = (painText) => {
        return `${painText}${postfixText}`;
    };

    return {
        info(text) {
            console.log(chalk.black.bgGreenBright(`[${resourceToUpperCased}]`), addPostfix(text));
        },
        warn(text) {
            console.warn(chalk.black.bgYellow(`[${resourceToUpperCased}]`), addPostfix(text));
        },
        br() {
            console.log('');
        },
    };
};
