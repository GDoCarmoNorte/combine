import NewRelicBrowser from '@types/new-relic-browser';

declare global {
    interface Window {
        newrelic: typeof NewRelicBrowser;
        dataLayer?: unknown[];
        webAuth: any;
    }
}
