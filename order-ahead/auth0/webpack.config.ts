import path from 'path';
import webpack from 'webpack';

import HtmlWebpackPlugin from 'html-webpack-plugin';
import InlineChunkHtmlPlugin from 'inline-chunk-html-plugin';
import Dotenv from 'dotenv-webpack';
import svgToMiniDataURI from 'mini-svg-data-uri';

import { getNewRelicScript } from '../common/helpers/getNewRelicScript';
import setFeatureFlags from '../webpack/setFeatureFlags';
import componentToggler from '../webpack/componentToggler';

const auth0Dir = path.resolve(__dirname);
const rootDir = path.resolve(__dirname, '../');

require('dotenv').config({ path: path.resolve(rootDir, './.env') });

const isDevelopment = process.env.NODE_ENV === 'development';
const BRAND = process.env.NEXT_PUBLIC_BRAND;
const APP_URL = process.env.NEXT_PUBLIC_APP_URL;
const NEXT_PUBLIC_GA_TRACKING_ID = process.env.NEXT_PUBLIC_GA_TRACKING_ID;

const newRelicScript = getNewRelicScript({
    newRelicAccountId: process.env.NEXT_PUBLIC_NEWRELIC_ACCOUNT_ID,
    newRelicTrustKey: process.env.NEXT_PUBLIC_NEWRELIC_TRUST_KEY,
    newRelicAgentId: process.env.NEXT_PUBLIC_NEWRELIC_AGENT_ID,
    newRelicLicenseKey: process.env.NEXT_PUBLIC_NEWRELIC_LICENSE_KEY,
    newRelicApplicationId: process.env.NEXT_PUBLIC_NEWRELIC_APPLICATION_ID,
    newRelicDistributedTracingAllowedOrigins: process.env.NEXT_PUBLIC_NEWRELIC_DISTRIBUTED_TRACING_ALLOWED_ORIGINS,
});

const config = {
    mode: isDevelopment ? 'development' : 'production',
    entry: {
        login: path.resolve(auth0Dir, 'src/pages/login/app.tsx'),
        passwordReset: path.resolve(auth0Dir, 'src/pages/passwordReset/app.tsx'),
    },
    output: {
        path: path.resolve(auth0Dir, 'dist'),
        publicPath: '',
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: {
                                localIdentName: '[local]___[contenthash:base64:8]',
                            },
                        },
                    },
                ],
            },
            {
                test: /\.svg$/i,
                type: 'asset/inline',
                generator: {
                    dataUrl: (content) => {
                        return svgToMiniDataURI(content.toString());
                    },
                },
            },
        ],
    },
    resolve: {
        alias: {
            '@brand': path.resolve(auth0Dir, 'public', 'brands', BRAND),
        },
        extensions: ['.tsx', '.ts', '.js', '.css'],
        modules: [path.resolve(auth0Dir, 'node_modules')],
    },
    devServer: {
        historyApiFallback: {
            rewrites: [
                { from: /^\/$/, to: '/login.html' },
                { from: /^\/login$/, to: '/login.html' },
                { from: /^\/password-reset$/, to: '/password-reset.html' },
            ],
        },
        port: 3000,
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'login.html',
            chunks: ['login'],
            inject: true,
            template: path.resolve(auth0Dir, 'index.ejs'),
            scriptLoading: 'blocking',
            production: !isDevelopment,
            brandName: BRAND,
            appUrl: APP_URL,
            gaTrackingId: NEXT_PUBLIC_GA_TRACKING_ID,
            newRelicScript,
        }),
        new HtmlWebpackPlugin({
            filename: 'password-reset.html',
            chunks: ['passwordReset'],
            inject: true,
            template: path.resolve(auth0Dir, 'index.ejs'),
            scriptLoading: 'blocking',
            production: !isDevelopment,
            brandName: BRAND,
            appUrl: APP_URL,
            gaTrackingId: NEXT_PUBLIC_GA_TRACKING_ID,
            newRelicScript,
        }),
        !isDevelopment && new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/[login|password-reset]/]),
        new webpack.ProvidePlugin({
            process: 'process/browser',
        }),
        new Dotenv({
            path: path.resolve(rootDir, './.env'),
        }),
    ].filter(Boolean),
};

const configWithFeatureFlags = setFeatureFlags(config);

module.exports = componentToggler(configWithFeatureFlags);
