import '../../../../public/normalize.css';
import '../../../../styles/variables.css';
import '../../../../styles/global.css';

import React from 'react';
import PasswordReset from '../../components/passwordReset';
import { StylesProvider } from '@material-ui/core';
import Navigation from '../../components/navigation';
import Logger from '../../components/logger';
import ErrorHandler from '../../components/errorHandler/errorHandler';

// eslint-disable-next-line import/no-unresolved
import logo from '@brand/logo.svg';
import { logoDesktop } from '../../components/login/constants';

export default function App(): JSX.Element {
    return (
        <Logger>
            <div id="main">
                <StylesProvider injectFirst>
                    <div className="sticky-top">
                        <Navigation logo={logo} logoDesktop={logoDesktop} />
                    </div>
                    <ErrorHandler />
                    <PasswordReset />
                </StylesProvider>
            </div>
        </Logger>
    );
}
