import React from 'react';
import ReactDOM from 'react-dom';
import { auth } from '../../lib/auth';
import PasswordReset from './passwordReset';

const container = document.getElementById('container');

window.addEventListener('authInit', (event: CustomEvent) => {
    auth.initAuth0(event.detail.webAuth);
});

ReactDOM.render(<PasswordReset />, container);
