import React from 'react';
import ReactDOM from 'react-dom';
import { auth } from '../../lib/auth';
import Login from '../../components/login/login';

const container = document.getElementById('container');

window.addEventListener('authInit', (event: CustomEvent) => {
    auth.initAuth0(event.detail.webAuth);
});

ReactDOM.render(<Login />, container);
