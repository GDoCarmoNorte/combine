export const maskString = (string: string, startShift = 1, endShift = 1, mask = 'x'): string => {
    const start = string.length > startShift ? startShift : 0;
    const end = string.length > startShift + endShift ? -endShift : string.length;

    const match = string.slice(start, end);

    return string.replace(match, Array(match.length).fill(mask).join(''));
};

export const maskEmail = (string: string): string => {
    const [address] = string.split('@');
    const [siteName, domain] = string.split('@')?.[1]?.split('.') || [];

    if (address && siteName && domain) {
        return `${maskString(address)}@${maskString(siteName)}.${domain}`;
    }

    return maskString(string);
};

export const maskPassword = (string: string): string => {
    return new Array(string.length).fill('*').join('');
};

export const maskPhone = (string: string, startShift = 3, endShift = 2, mask = 'x'): string => {
    const startPart = string.slice(0, startShift);
    const endPart = string.slice(-endShift);
    const middlePart = string.slice(startShift, -endShift).replace(/\d/g, mask);

    return startPart + middlePart + endPart;
};
