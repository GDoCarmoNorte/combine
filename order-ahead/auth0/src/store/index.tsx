import React, { useReducer } from 'react';

type typePage = 'signup' | 'signin' | 'forgotpassword' | null;

type Error = {
    statusCode?: number | string;
    message?: string;
    type?: string;
    code?: string;
};

export type GO_TO_SIGN_UP = { type: 'GO_TO_SIGN_UP' };
export type GO_TO_SIGN_IN = { type: 'GO_TO_SIGN_IN' };
export type GO_TO_FORGOT_PASSWORD = { type: 'GO_TO_FORGOT_PASSWORD' };
export type UPDATE_EMAIL = { type: 'UPDATE_EMAIL'; payload: string };
export type SET_ERROR = { type: 'SET_ERROR'; payload: Error };
export type REMOVE_ERROR = { type: 'REMOVE_ERROR' };

type StoreActions = GO_TO_SIGN_IN | GO_TO_SIGN_UP | GO_TO_FORGOT_PASSWORD | UPDATE_EMAIL | SET_ERROR | REMOVE_ERROR;

type GoToSignInAction = () => void;
type GoToSignUpAction = () => void;
type GoToForgotPasswordAction = () => void;
type UpdateEmailAction = (email: string) => void;
type SetError = (error: Error) => void;
type RemoveError = () => void;

type IStore = {
    state: IState;
    actions: {
        goToSignUp: GoToSignUpAction;
        goToSignIn: GoToSignInAction;
        goToForgotPassword: GoToForgotPasswordAction;
        updateEmail: UpdateEmailAction;
        setError: SetError;
        removeError: RemoveError;
    };
};

export interface IState {
    screen: typePage;
    email: string;
    error: Error;
}

export const initialState: IState = {
    screen: null,
    email: '',
    error: {},
};

export const reducer = (state: IState, action: StoreActions): IState => {
    switch (action.type) {
        case 'GO_TO_SIGN_UP':
            return {
                ...state,
                screen: 'signup',
            };
        case 'GO_TO_SIGN_IN':
            return {
                ...state,
                screen: 'signin',
            };
        case 'GO_TO_FORGOT_PASSWORD':
            return {
                ...state,
                screen: 'forgotpassword',
            };
        case 'UPDATE_EMAIL':
            return {
                ...state,
                email: action.payload,
            };
        case 'SET_ERROR':
            return {
                ...state,
                error: action.payload,
            };
        case 'REMOVE_ERROR':
            return {
                ...state,
                error: {},
            };

        default:
            return state;
    }
};

const noop = () => undefined;
const noopWithValue = () => undefined;

export const StoreContext: React.Context<IStore> = React.createContext({
    state: initialState,
    actions: {
        goToSignIn: noop,
        goToSignUp: noop,
        goToForgotPassword: noop,
        updateEmail: noopWithValue,
        setError: noopWithValue,
        removeError: noop,
    },
});

const Store = ({ children }: { children: JSX.Element[] | JSX.Element }): JSX.Element => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const goToSignUp = () => dispatch({ type: 'GO_TO_SIGN_UP' });
    const goToSignIn = () => dispatch({ type: 'GO_TO_SIGN_IN' });
    const goToForgotPassword = () => dispatch({ type: 'GO_TO_FORGOT_PASSWORD' });
    const updateEmail = (email: string) => dispatch({ type: 'UPDATE_EMAIL', payload: email });
    const setError = (error: Error) => dispatch({ type: 'SET_ERROR', payload: error });
    const removeError = () => dispatch({ type: 'REMOVE_ERROR' });

    return (
        <StoreContext.Provider
            value={{
                state,
                actions: {
                    goToSignIn,
                    goToSignUp,
                    goToForgotPassword,
                    updateEmail,
                    setError,
                    removeError,
                },
            }}
        >
            {children}
        </StoreContext.Provider>
    );
};

export default Store;
