import { GtmEventNames, SignInResultTypes, SignInTypes } from '../../../common/services/gtmService/types';
import { pushSocialSignInEvent } from './helpers';
import logger from '../../../common/services/logger';
import { maskEmail, maskPassword, maskPhone, maskString } from '../utils/string';
import {
    AUTH0_FORGOT_PASSWORD,
    AUTH0_PASSWORD_RESET,
    AUTH0_SIGN_IN,
    AUTH0_SIGN_UP,
} from '../../constants/loggerEvents';
import { gtmServicePush } from '../../../lib/gtmServicePush';
export default class Auth {
    private webAuth: any = null;

    initAuth0(webAuth: any): void {
        if (!webAuth) throw new Error('Invalid Auth0 SDK passed to Auth Helper');
        this.webAuth = webAuth;
    }

    isWeb(): any {
        if (!this.webAuth) return true;
        return process.env.NEXT_PUBLIC_AUTH_0_CLIENT_ID === this.webAuth?.baseOptions?.clientID;
    }

    getInitialPage(): 'signin' | 'signup' {
        if (!this.webAuth) throw new AuthError('sdk_not_configured', 'Auth0 SDK is not configured');
        return this.webAuth?.baseOptions?.page;
    }

    async signUpAndLogin(formData: {
        email: string;
        password: string;
        firstName: string;
        lastName: string;
        birthDate?: string;
        zipCode?: string;
        phone: string;
        marketingOptIn: boolean;
    }): Promise<void> {
        if (!this.webAuth) throw new AuthError('sdk_not_configured', 'Auth0 SDK is not configured');
        const { email, password, firstName, lastName, birthDate, zipCode, phone, marketingOptIn } = formData;

        return new Promise((resolve, reject) => {
            this.webAuth.redirect.signupAndLogin(
                {
                    connection: 'firebase-auth',
                    email,
                    password,
                    given_name: firstName,
                    family_name: lastName,
                    user_metadata: {
                        birthDate,
                        zipCode,
                        phone,
                        termsLink: process.env.NEXT_PUBLIC_APP_URL + '/terms-of-use',
                        termsAcceptedAt: new Date().toISOString(),
                        marketingOptIn: marketingOptIn ? 'on' : 'off',
                    },
                },
                (error) => {
                    if (error) {
                        logger.logEvent(AUTH0_SIGN_UP, {
                            isSuccess: false,
                            request: {
                                firstName: maskString(firstName),
                                lastName: maskString(lastName),
                                email: maskEmail(email),
                                phone: maskPhone(phone),
                                password: maskPassword(password),
                            },
                            error,
                        });
                        return reject(new AuthError(error.code, error.description, error.statusCode));
                    }
                    gtmServicePush(GtmEventNames.AccountCreationSuccess, { method: 'email' });
                    logger.logEvent(AUTH0_SIGN_UP, { isSuccess: true });
                    resolve();
                }
            );
        });
    }

    async login({ email, password }: { email: string; password: string }): Promise<void> {
        if (!this.webAuth) throw new AuthError('sdk_not_configured', 'Auth0 SDK is not configured');
        window.dataLayer = window.dataLayer || [];
        return new Promise((resolve, reject) => {
            this.webAuth.login(
                {
                    email,
                    password,
                    realm: 'firebase-auth',
                    onRedirecting: (doAuth) => {
                        logger.logEvent(AUTH0_SIGN_IN, { isSuccess: true });
                        window.dataLayer.push({ event: 'signInSuccess', signInType: 'email' });
                        resolve();
                        doAuth();
                    },
                },
                (error) => {
                    if (error) {
                        logger.logEvent(AUTH0_SIGN_IN, {
                            isSuccess: false,
                            request: { email: maskEmail(email) },
                            error,
                        });
                        window.dataLayer.push({ event: 'signInFailure', signInType: 'email' });
                        return reject(new AuthError(error.code, error.description, error.statusCode));
                    }
                }
            );
        });
    }

    async sendPasswordResetEmail({ email }: { email: string }): Promise<void> {
        if (!this.webAuth) throw new AuthError('sdk_not_configured', 'Auth0 SDK is not configured');

        return new Promise((resolve, reject) => {
            this.webAuth.changePassword(
                {
                    email,
                    connection: 'firebase-auth',
                },
                (error) => {
                    if (error) {
                        logger.logEvent(AUTH0_FORGOT_PASSWORD, {
                            isSuccess: false,
                            request: { email: maskEmail(email) },
                            error,
                        });
                        return reject(new AuthError(error.code, error.description, error.statusCode));
                    }
                    logger.logEvent(AUTH0_FORGOT_PASSWORD, { isSuccess: true });
                    resolve();
                }
            );
        });
    }

    async loginWithSocial(socialProvider: SignInTypes): Promise<void> {
        if (!this.webAuth) throw new Error('Auth0 SDK is not configured');

        return new Promise((resolve, reject) => {
            this.webAuth.authorize(
                {
                    connection: socialProvider,
                },
                (error) => {
                    if (error) {
                        pushSocialSignInEvent(socialProvider, SignInResultTypes.FAILURE);
                        reject(new AuthError(error.code, error.description, error.statusCode));
                    }
                    pushSocialSignInEvent(socialProvider, SignInResultTypes.SUCCESS);
                    resolve();
                }
            );
        });
    }

    async updatePassword({
        password,
        email,
        _csrf,
        ticket,
    }: {
        password: string;
        _csrf: string;
        email: string;
        ticket: string;
    }): Promise<void | AuthError> {
        const hostname = window.location.origin;

        try {
            const response = await fetch(`${hostname}/lo/reset`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: new URLSearchParams({
                    _csrf,
                    ticket,
                    email,
                    newPassword: password,
                    confirmNewPassword: password,
                }),
            });

            if (!response.ok) {
                logger.logEvent(AUTH0_PASSWORD_RESET, {
                    isSuccess: false,
                    request: { email: maskEmail(email) },
                    error: {
                        code: 'unexpected_error',
                        description: 'error_description',
                    },
                });
                return Promise.reject(new AuthError('unexpected_error', 'Error'));
            }
        } catch (error) {
            return Promise.reject(new AuthError(error.code, error.description, error.statusCode));
        }
        logger.logEvent(AUTH0_PASSWORD_RESET, { isSuccess: true });
        return Promise.resolve();
    }
}

export class AuthError extends Error {
    code = '';
    message = '';
    statusCode;
    constructor(code: string, message: string, statusCode?: number | string) {
        super(message);
        this.code = code;
        this.message = message;
        this.statusCode = statusCode;
    }
}

export const auth = new Auth();
