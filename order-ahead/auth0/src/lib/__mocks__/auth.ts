const { AuthError, default: Auth, auth } = jest.requireActual('../auth');
const authMock = {
    ...auth,
    isWeb: jest.fn().mockReturnValue(true),
};

export { authMock as auth, AuthError, Auth };
