import {
    GtmErrorEvent,
    GtmEventNames,
    SignInResultTypes,
    SignInTypes,
} from '../../../common/services/gtmService/types';
import { auth } from './auth';
import { gtmServicePush } from '../../../lib/gtmServicePush';

export const pushSocialSignInEvent = (signInType: SignInTypes, signInResult: SignInResultTypes): void => {
    gtmServicePush(SignInResultTypes[signInResult], { signInType });
};

export const pushErrorEvent = (error: GtmErrorEvent): void => {
    gtmServicePush(GtmEventNames.Error, {
        error,
    });
};

export const loginWithFacebook = async (): Promise<void> => await auth.loginWithSocial(SignInTypes.FACEBOOK);
export const loginWithGoogle = async (): Promise<void> => await auth.loginWithSocial(SignInTypes.GOOGLE);
export const loginWithApple = async (): Promise<void> => await auth.loginWithSocial(SignInTypes.APPLE);
