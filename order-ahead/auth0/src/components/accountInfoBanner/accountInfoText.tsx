import React from 'react';
import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';
import styles from './index.module.css';

export const InfoLink = () => (
    <p>
        GET 50% OFF ANY SANDWICH ON YOUR NEXT{' '}
        <InspireSimpleLink className={styles.link} link={process.env.NEXT_PUBLIC_APP_URL}>
            ARBY&apos;S
        </InspireSimpleLink>{' '}
        ORDER
    </p>
);

const InfoTextMap = {
    signUp: {
        TITLE: 'CREATE AN ACCOUNT AND',
        MAIN_TEXT: <InfoLink />,
        DESCRIPTION:
            'Valid on any full priced sandwich or wrap in-store or online. Limit one per customer. Not valid with any promo or offer. At participating U.S. locations',
        BACKGROUND_COLOR: null,
    },
    signIn: {
        TITLE: 'sign in',
        MAIN_TEXT: 'come and get your meats',
        DESCRIPTION: '',
        BACKGROUND_COLOR: null,
    },
    forgotPassword: {
        TITLE: 'sign in',
        MAIN_TEXT: 'come and get your meats',
        DESCRIPTION: '',
        BACKGROUND_COLOR: null,
    },
};

export default InfoTextMap;
