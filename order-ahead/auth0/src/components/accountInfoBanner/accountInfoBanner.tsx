import React from 'react';
import InfoBanner from '../../../../components/atoms/infoBanner';
import InfoTextMap from './accountInfoText';

import styles from './index.module.css';

interface IAccountInfoBanner {
    type: 'signIn' | 'signUp' | 'forgotPassword';
    children: React.ReactNode;
}

const AccountInfoBanner: React.FC<IAccountInfoBanner> = ({ children, type }): JSX.Element => {
    const accountInfoText = InfoTextMap[type];

    return (
        <>
            <InfoBanner
                title={accountInfoText.TITLE}
                mainText={accountInfoText.MAIN_TEXT}
                description={accountInfoText.DESCRIPTION}
                wrapperClassName={styles.wrapper}
                backgroundColor={accountInfoText.BACKGROUND_COLOR}
            />
            {children}
        </>
    );
};

export default AccountInfoBanner;
