import React, { useContext, useState } from 'react';
import { Field, Form, Formik } from 'formik';
import classNames from 'classnames';
import CheckIcon from '@material-ui/icons/Check';
import { StoreContext } from '../../store/index';
import { InspireButton } from '../button';
import FormikInput from '../../../../components/organisms/formikInput';
import FormikPasswordInput from '../../../../components/organisms/formikPasswordInput';
import getBrandInfo from '../../../../lib/brandInfo';
import { loginWithFacebook, loginWithApple } from '../../lib/helpers';
import { autoCorrectedDatePipe } from '../../../../common/helpers/autoCorrectedDatePipe';
import {
    unmaskPhoneField,
    validateFirstNameField,
    validateLastNameField,
    validateEmailField,
    validatePhoneField,
    validateDOBField,
    validateTermsField,
    validatePasswordField,
    validatePasswordRequired,
    validateZipCodeField,
} from './validation';
import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';
import { isSignUpPhoneTooltip } from '../../../../lib/getFeatureFlags';
import styles from './index.module.css';
import socialButtonStyles from '../signin/index.module.css';
import BrandLoader from '../../../../components/atoms/BrandLoader';

const SignUpForm = (props: any): JSX.Element => {
    const APP_URL = process.env.NEXT_PUBLIC_APP_URL;
    const { handleSignUpEmail, loading } = props;
    const { brandName } = getBrandInfo();
    const {
        state: { email },
    } = useContext(StoreContext);

    const [showPasswordHint, setShowPasswordHint] = useState(true);

    const initialFormValues = {
        firstName: '',
        lastName: '',
        email,
        phone: '',
        birthDate: '',
        zipCode: '',
        password: '',
        termsAgreed: false,
        marketingOptIn: true,
    };

    const passwordValidationCoordinator = (value) => {
        setShowPasswordHint(true);

        let error = validatePasswordRequired(value);
        if (error) {
            return error;
        }

        error = validatePasswordField(value);
        if (error) {
            setShowPasswordHint(false);
        }
        return error;
    };

    return (
        <Formik initialValues={initialFormValues} {...props}>
            {({ setFieldValue, isValid }) => (
                <Form>
                    <FormikInput
                        label="First Name"
                        labelClassName={styles.label}
                        name="firstName"
                        maxLength={256}
                        placeholder="Enter First Name"
                        required
                        validate={validateFirstNameField}
                    />
                    <FormikInput
                        label="Last Name"
                        labelClassName={styles.label}
                        name="lastName"
                        maxLength={256}
                        placeholder="Enter Last Name"
                        required
                        validate={validateLastNameField}
                    />
                    <FormikInput
                        label="Email"
                        labelClassName={styles.label}
                        name="email"
                        onChange={(e) => {
                            setFieldValue('email', e.target.value);
                            handleSignUpEmail(e.target.value);
                        }}
                        placeholder="Enter Email"
                        required
                        validate={validateEmailField}
                    />
                    <FormikInput
                        label="Phone"
                        type="tel"
                        labelClassName={styles.label}
                        name="phone"
                        placeholder="(xxx) xxx-xxxx"
                        validate={validatePhoneField}
                        onChange={(e) => {
                            setFieldValue('phone', unmaskPhoneField(e.target.value));
                        }}
                        mask={['(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                        withTooltip={isSignUpPhoneTooltip()}
                        tooltipTitle="Your mobile phone will be used as your member ID"
                        tooltipClassName={styles.tooltip}
                        required
                    />
                    <FormikInput
                        label="Date Of Birth"
                        labelClassName={styles.label}
                        name="birthDate"
                        placeholder="MM-DD"
                        mask={[/\d/, /\d/, '-', /\d/, /\d/]}
                        validate={validateDOBField}
                        pipe={autoCorrectedDatePipe}
                    />
                    <FormikInput
                        name="zipCode"
                        label="Zip Code"
                        placeholder="Enter Zip Code"
                        errorClassName={styles.inputError}
                        labelClassName={styles.label}
                        onChange={(e) => {
                            setFieldValue('zipCode', e.target.value);
                        }}
                        validate={validateZipCodeField}
                        required
                    />
                    <FormikPasswordInput
                        label="Password"
                        labelClassName={styles.label}
                        name="password"
                        placeholder="Enter Password"
                        description={
                            showPasswordHint
                                ? '(Min. 8 characters, one number, one uppercase letter, one lowercase letter and at least one symbol)'
                                : ''
                        }
                        required
                        validate={passwordValidationCoordinator}
                        showErrorImmediately={true}
                        showHideButton={true}
                    />

                    <Field name="marketingOptIn">
                        {({ field: { value } }) => (
                            <div className={styles.termsContainer}>
                                <input
                                    className={styles.checkbox}
                                    type="checkbox"
                                    id="emailShare"
                                    checked={value}
                                    onChange={(e) => setFieldValue('marketingOptIn', e.target.checked)}
                                />
                                <div
                                    tabIndex={0}
                                    className={styles.styledCheckbox}
                                    onClick={() => setFieldValue('marketingOptIn', !value)}
                                    onKeyPress={({ key }) => key === 'Enter' && setFieldValue('marketingOptIn', !value)}
                                >
                                    <CheckIcon className={styles.checkIcon} />
                                </div>
                                <label
                                    htmlFor="emailShare"
                                    className={classNames(styles.termsLabel, 't-paragraph-hint')}
                                >
                                    {`I would like to receive email communication from ${brandName}`}
                                </label>
                            </div>
                        )}
                    </Field>
                    <Field name="termsAgreed" validate={validateTermsField}>
                        {({ field: { value } }) => (
                            <div className={styles.termsContainer}>
                                <input
                                    className={styles.checkbox}
                                    type="checkbox"
                                    id="terms"
                                    checked={value}
                                    onChange={(e) => setFieldValue('termsAgreed', e.target.checked)}
                                />
                                <div
                                    tabIndex={0}
                                    className={styles.styledCheckbox}
                                    onClick={() => setFieldValue('termsAgreed', !value)}
                                    onKeyPress={({ key }) => key === 'Enter' && setFieldValue('termsAgreed', !value)}
                                >
                                    <CheckIcon className={styles.checkIcon} />
                                </div>
                                <label htmlFor="terms" className={classNames(styles.termsLabel, 't-paragraph-hint')}>
                                    I have read and agree to the&nbsp;
                                    <InspireSimpleLink
                                        type="secondary"
                                        link={`${APP_URL}/terms-of-use`}
                                        newtab
                                        className={styles.termsLink}
                                    >
                                        Terms and Conditions
                                    </InspireSimpleLink>
                                    . Information will be used as described here and in the&nbsp;
                                    <InspireSimpleLink
                                        type="secondary"
                                        link={`${APP_URL}/privacy-policy`}
                                        newtab
                                        className={styles.termsLink}
                                    >
                                        Privacy Policy
                                    </InspireSimpleLink>
                                </label>
                            </div>
                        )}
                    </Field>
                    <InspireButton className={styles.submitButton} submit disabled={!isValid || loading} tabIndex={0}>
                        {loading ? <BrandLoader className={styles.loader} /> : 'CREATE AN ACCOUNT'}
                    </InspireButton>

                    {(props.hasFacebookSignup || props.hasAppleSignup) && (
                        <div className={socialButtonStyles.socialButtonsWrap} tabIndex={0}>
                            {props.hasFacebookSignup && (
                                <InspireButton
                                    onClick={loginWithFacebook}
                                    type={'secondary'}
                                    text={'Sign in with Facebook'}
                                    className={classNames(
                                        socialButtonStyles.socialButton,
                                        socialButtonStyles.facebookButton
                                    )}
                                    tabIndex={0}
                                />
                            )}
                            {props.hasAppleSignup && (
                                <InspireButton
                                    onClick={loginWithApple}
                                    type="secondary"
                                    text="Sign in with Apple"
                                    className={classNames(
                                        socialButtonStyles.socialButton,
                                        socialButtonStyles.appleButton
                                    )}
                                    tabIndex={0}
                                />
                            )}
                        </div>
                    )}
                </Form>
            )}
        </Formik>
    );
};

export default SignUpForm;
