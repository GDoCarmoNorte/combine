import {
    validateAlpha,
    validateDateOfBirth,
    validateEmail,
    validatePasswordComplexity,
    validateRequire,
    validateRequireBoolean,
    validateZipCode,
} from '../../../../common/helpers/complexValidateHelper';
import { optionalValidator, phoneWithDashesValidator } from '../../../../common/helpers/validateHelper';

export const validateFirstNameField = validateAlpha('First Name', 256);
export const validateLastNameField = validateAlpha('Last Name', 256);
export const validateEmailField = validateEmail('Email');
export const validatePhoneField = optionalValidator(phoneWithDashesValidator('Incorrect phone number'));
export const validateDOBField = validateDateOfBirth('Date Of Birth');
export const validateTermsField = validateRequireBoolean('Terms');
export const validatePasswordField = validatePasswordComplexity();
export const validatePasswordRequired = validateRequire('Password');
export const validateZipCodeField = validateZipCode('Zip Code');

export const unmaskPhoneField = (value: string) => {
    // removes '(' and spaces
    let newPhone = value.replace(/[(\s]/g, '');
    // replaces ')' and '-' with '-'
    newPhone = newPhone.replace(/[)-]/g, '-');
    return newPhone;
};
