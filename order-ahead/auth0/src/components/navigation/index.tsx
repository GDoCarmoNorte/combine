import React from 'react';

import styles from './index.module.css';

import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';
import Icon from '../../../../components/atoms/BrandIcon';
import Logo from '../../../../components/atoms/Logo';
import { auth } from '../../lib/auth';

interface IProps {
    logo?: string;
    pictureComponent?: JSX.Element;
    logoDesktop?: string;
}

export default function Navigation({ logo, pictureComponent, logoDesktop }: IProps): JSX.Element {
    if (!logo && !pictureComponent) return;

    const isWeb = auth.isWeb();
    const APP_URL = process.env.NEXT_PUBLIC_APP_URL;

    const handleIconClick = () => {
        if (window.history.length > 1) {
            window.history.back();
        } else {
            window.location.replace(`${APP_URL}`);
        }
    };

    return (
        <nav className={styles.navigation}>
            <InspireSimpleLink link={APP_URL} className={styles.link} newtab={false} aria-label="Main Page">
                {!logo && pictureComponent}
                {!pictureComponent && logo && (
                    <Logo urlLogo={logo} urlLogoDesktop={logoDesktop} className={styles.logo} />
                )}
            </InspireSimpleLink>
            {isWeb ? (
                <InspireSimpleLink onClick={handleIconClick} aria-label="Close">
                    <Icon className={styles.closeIcon} icon="action-close" size="m" variant="light" />
                </InspireSimpleLink>
            ) : (
                <div className={styles.closeIconPlaceholder} />
            )}
        </nav>
    );
}
