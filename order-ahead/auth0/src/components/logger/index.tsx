import { useEffect } from 'react';
import logger, { isNewRelicAvailable } from '../../../../common/services/logger';

export default function Logger({ children }: { children: JSX.Element }): JSX.Element {
    useEffect(() => {
        if (isNewRelicAvailable) {
            logger.init();
        }
    }, []);

    return children;
}
