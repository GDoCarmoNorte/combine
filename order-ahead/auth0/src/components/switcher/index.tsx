import React, { useContext, useEffect } from 'react';
import { auth, AuthError } from '../../lib/auth';
import { StoreContext } from '../../store';
import SignUp from '../signup';
import SignIn from '../signin';
import ForgotPassword from '../forgotPassword';

const usePage = () => {
    const {
        actions: { goToSignIn, goToSignUp, setError },
    } = useContext(StoreContext);

    useEffect(() => {
        try {
            const page = auth.getInitialPage();
            if (page === 'signup') {
                goToSignUp();
            } else {
                goToSignIn();
            }
        } catch (error) {
            if (error instanceof AuthError) {
                setError({
                    type: 'LOADING PAGE',
                    code: error.code,
                    message: error.message,
                    statusCode: error.statusCode,
                });
            } else {
                console.error(error);
            }
        }
    }, []);
};

export default function Switcher(): JSX.Element {
    const { state } = useContext(StoreContext);

    usePage();

    switch (state.screen) {
        case 'signin':
            return <SignIn />;
        case 'signup':
            return <SignUp />;
        case 'forgotpassword':
            return <ForgotPassword />;
        default:
            return null;
    }
}
