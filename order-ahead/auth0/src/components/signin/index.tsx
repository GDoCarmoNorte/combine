import React, { useContext, useEffect, useState } from 'react';
import classNames from 'classnames';

import FormikInput from '../../../../components/organisms/formikInput';
import { Field, Form, Formik } from 'formik';

import styles from './index.module.css';

import { validateEmail, validateRequire } from '../../../../common/helpers/complexValidateHelper';

import { useScrollToElement } from '../../../../common/hooks/useScrollToElement';
import getBrandInfo from '../../../../lib/brandInfo';
import { StoreContext } from '../../store/index';
import { auth, AuthError } from '../../lib/auth';
import { InspireButton } from '../button';
import { InspireLinkButton } from '../../../../components/atoms/link/linkButton';
import ErrorMessage from '../errorMessage';
import FormikPasswordInput from '../../../../components/organisms/formikPasswordInput';
import CheckIcon from '@material-ui/icons/Check';
import { loginWithApple, loginWithFacebook } from '../../lib/helpers';
import Card from '../card';
import { genericSignInErrorMessage, LABELS, SOCIAL_SIGNIN } from './signin.constants';
import AccountInfoBanner from '../accountInfoBanner';
import BrandLoader from '../../../../components/atoms/BrandLoader';
import { ERRORS_MAP } from '../errorHandler/errors.constants';

const validateEmailField = validateEmail('Email');
const validatePasswordField = validateRequire('Password');

const errorMessageMapping = {
    ...ERRORS_MAP,
};

const SignIn = (): JSX.Element => {
    const { brandName } = getBrandInfo();
    const {
        state: { email },
        actions: { goToSignUp, goToForgotPassword, updateEmail, setError },
    } = useContext(StoreContext);
    const [errorCode, setErrorCode] = useState<string>();

    const [signInEmail, setSignInEmail] = useState(email || '');

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        document.title = `${brandName} | Sign In`;
    }, [brandName]);

    const initialFormValues = {
        email,
        password: '',
    };

    const [element, scrollToElement] = useScrollToElement<HTMLHeadingElement>();

    const handleSubmit = async (formData) => {
        const { email, password } = formData;

        setLoading(true);
        try {
            await auth.login({
                email,
                password,
            });
        } catch (error) {
            setLoading(false);

            if (error instanceof AuthError) {
                setErrorCode(error.code);
                setError({
                    type: 'SIGN IN',
                    code: error.code,
                    message: error.message,
                    statusCode: error.statusCode,
                });
                scrollToElement();
            } else {
                console.error(error);
            }
        }
    };

    const { APPLE, FACEBOOK } = SOCIAL_SIGNIN;

    return (
        <AccountInfoBanner type="signIn">
            <Card>
                <h2 ref={element} className={classNames('t-header-h2', styles.heading)}>
                    SIGN IN
                </h2>
                {errorCode && (
                    <ErrorMessage
                        code={errorCode}
                        messageMap={errorMessageMapping}
                        specificErrorMessage={genericSignInErrorMessage}
                    />
                )}
                <Formik onSubmit={handleSubmit} initialValues={initialFormValues} validateOnMount>
                    {({ setFieldValue, isValid }) => (
                        <Form>
                            <FormikInput
                                label="Email"
                                labelClassName={styles.label}
                                name="email"
                                placeholder="Enter Email Address"
                                onChange={(e) => {
                                    setFieldValue('email', e.target.value);
                                    setSignInEmail(e.target.value);
                                }}
                                validate={validateEmailField}
                                required
                            />
                            <FormikPasswordInput
                                label="Password"
                                labelClassName={styles.label}
                                className={styles.lastInput}
                                name="password"
                                placeholder="Enter Password"
                                validate={validatePasswordField}
                                required
                            />
                            <Field name="termsAgreed">
                                {({ field: { value } }) => (
                                    <div className={styles.termsContainer}>
                                        <input
                                            className={styles.checkbox}
                                            type="checkbox"
                                            id="terms"
                                            checked={value}
                                            onChange={(e) => setFieldValue('termsAgreed', e.target.checked)}
                                        />
                                        <div
                                            tabIndex={0}
                                            className={styles.styledCheckbox}
                                            onClick={() => setFieldValue('termsAgreed', !value)}
                                            onKeyPress={({ key }) =>
                                                key === 'Enter' && setFieldValue('termsAgreed', !value)
                                            }
                                        >
                                            <CheckIcon className={styles.checkIcon} />
                                        </div>
                                        <label htmlFor="terms" className={styles.termsLabel}>
                                            Remember me
                                        </label>
                                        <InspireLinkButton
                                            linkType="secondary"
                                            aria-label="Forgot Password?"
                                            gtmId="forgotPassword"
                                            className={styles.rememberAndForgotLink}
                                            onClick={goToForgotPassword}
                                        >
                                            Forgot password?
                                        </InspireLinkButton>
                                    </div>
                                )}
                            </Field>
                            <InspireButton
                                tabIndex={0}
                                data-gtm-id="CTA_signin"
                                className={styles.submitButton}
                                disabled={!isValid || loading}
                                submit
                            >
                                {loading ? <BrandLoader className={styles.loader} /> : 'SIGN IN'}
                            </InspireButton>
                            {(APPLE || FACEBOOK) && (
                                <>
                                    <div className={styles.buttonsDivider}>
                                        <span className={styles.dividerLine} />
                                        <p className={styles.orWord}>or</p>
                                        <span className={styles.dividerLine} />
                                    </div>
                                    <div className={styles.socialButtonsWrap} tabIndex={0}>
                                        {FACEBOOK && (
                                            <InspireButton
                                                onClick={loginWithFacebook}
                                                type={'secondary'}
                                                text={'Sign in with Facebook'}
                                                className={classNames(styles.socialButton, styles.facebookButton)}
                                                tabIndex={0}
                                            />
                                        )}
                                        {APPLE && (
                                            <InspireButton
                                                onClick={loginWithApple}
                                                type={'secondary'}
                                                text={'Sign in with Apple'}
                                                className={classNames(styles.socialButton, styles.appleButton)}
                                                tabIndex={0}
                                            />
                                        )}
                                    </div>
                                </>
                            )}

                            <div className={styles.dividerLineSingle}>
                                <span className={styles.dividerLine} />
                            </div>
                        </Form>
                    )}
                </Formik>
                <div className={styles.signInLabel}>
                    <p>
                        <span className={styles.footerSubtitle}>{LABELS.FOOTER_SUBTITLE} </span>
                        <span className={styles.footerTitle}>{LABELS.FOOTER_TITLE}</span>
                    </p>
                    <InspireLinkButton
                        linkType="secondary"
                        className={styles.createAnAccountLink}
                        onClick={() => {
                            updateEmail(signInEmail);
                            goToSignUp();
                        }}
                        gtmId="createAnAccount"
                    >
                        CREATE AN ACCOUNT
                    </InspireLinkButton>
                </div>
            </Card>
        </AccountInfoBanner>
    );
};

export default SignIn;
