import { getMobileOperatingSystem, MobileOperatingSystem } from '../../../../common/helpers/getMobileOperatingSystem';
import {
    getDesktopOperatingSystem,
    DesktopOperatingSystem,
} from '../../../../common/helpers/getDesktopOperatingSystem';

const showAppleButton =
    getMobileOperatingSystem() === MobileOperatingSystem.IOS ||
    getDesktopOperatingSystem() === DesktopOperatingSystem.MAC;

export const LABELS = {
    FOOTER_SUBTITLE: 'No account?',
    FOOTER_TITLE: 'Sign up to get access to special deals!',
};

export const SOCIAL_SIGNIN = {
    FACEBOOK: true,
    APPLE: showAppleButton,
};

export const genericSignInErrorMessage =
    'We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly.';
