import React, { useEffect, useState, useContext } from 'react';

import { InspireButton } from '../button';

import { Form, Formik } from 'formik';

import styles from './index.module.css';

import { auth, AuthError } from '../../lib/auth';
import { validatePasswordComplexity, validateRequire } from '../../../../common/helpers/complexValidateHelper';
import getBrandInfo from '../../../../lib/brandInfo';
import ErrorMessage from '../errorMessage';
import { InspireSimpleLink } from '../../../../components/atoms/link/simpleLink';
import FormikPasswordInput from '../../../../components/organisms/formikPasswordInput';
import Card from '../card';
import { StoreContext } from '../../store/index';
import classNames from 'classnames';

const validatePasswordFieldComplexity = validatePasswordComplexity();
const validatePasswordFieldRequired = validateRequire('Password');

const PasswordReset = (): JSX.Element => {
    const { brandName } = getBrandInfo();
    const [errorCode, setErrorCode] = useState<string>();
    const [passwordResetRequested, setPasswordResetRequested] = useState<boolean>();
    const {
        actions: { setError },
    } = useContext(StoreContext);

    useEffect(() => {
        document.title = `${brandName} | Change Password`;
    }, [brandName]);

    const initialFormValues = {
        password: '',
        retypePassword: '',
    };

    const handleSubmit = async (formData) => {
        const _csrf = (document.getElementById('_csrf') as HTMLInputElement).value;
        const ticket = (document.getElementById('ticket') as HTMLInputElement).value;
        const email = (document.getElementById('email') as HTMLInputElement).value;

        const { password } = formData;

        try {
            await auth.updatePassword({ password, _csrf, ticket, email });
        } catch (error) {
            if (error instanceof AuthError) {
                setErrorCode(error.code);
                setError({
                    type: 'RESET PASSWORD',
                    code: error.code,
                    message: error.message,
                    statusCode: error.statusCode,
                });
            } else {
                console.error(error);
            }
        } finally {
            setPasswordResetRequested(true);
        }
    };

    return (
        <Card>
            {!passwordResetRequested && (
                <ResetForm errorCode={errorCode} initialFormValues={initialFormValues} handleSubmit={handleSubmit} />
            )}
            {passwordResetRequested && !errorCode && <ResetSuccess />}
        </Card>
    );
};

const ResetForm = ({ errorCode, initialFormValues, handleSubmit }) => {
    const validateForm = (values) => {
        const errors: any = {};
        if (values.retypePassword !== values.password) {
            errors.retypePassword = 'Passwords do not match';
        }

        return errors;
    };

    const validatePasswordField = (value) => {
        const error = validatePasswordFieldRequired(value) || validatePasswordFieldComplexity(value);
        return error;
    };

    const validateRetypePasswordField = validatePasswordFieldRequired;

    return (
        <>
            <h3 className={classNames('t-header-h2', styles.heading)}>TIME FOR AN UPGRADE</h3>
            <h3 className={classNames('t-header-h2', styles.subheading)}>CHANGE PASSWORD</h3>
            {errorCode && <ErrorMessage code={errorCode} />}
            <Formik onSubmit={handleSubmit} validate={validateForm} initialValues={initialFormValues} validateOnMount>
                {({ isValid }) => (
                    <Form>
                        <input
                            id="_csrf"
                            type="hidden"
                            //value is populated by Auth0
                            value="{{csrf_token}}"
                        />
                        <input
                            id="ticket"
                            type="hidden"
                            //value is populated by Auth0
                            value="{{ticket}}"
                        />
                        <input
                            id="email"
                            type="hidden"
                            //value is populated by Auth0
                            value="{{email}}"
                        />
                        <FormikPasswordInput
                            label="New Password *"
                            labelClassName={styles.label}
                            name="password"
                            placeholder="Enter New Password"
                            validate={validatePasswordField}
                        />
                        <FormikPasswordInput
                            label="Retype New Password *"
                            labelClassName={styles.label}
                            name="retypePassword"
                            placeholder="Retype New Password"
                            showHideButton={false}
                            validate={validateRetypePasswordField}
                        />
                        <InspireButton
                            tabIndex={0}
                            className={styles.submitButton}
                            disabled={!isValid}
                            submit
                            ariaLabel="Save"
                            text="SAVE"
                        />
                    </Form>
                )}
            </Formik>
        </>
    );
};

const ResetSuccess = () => {
    return (
        <>
            <h3 className={classNames('t-header-h2', styles.heading)}>TIME FOR AN UPGRADE</h3>
            <h3 className={classNames('t-header-h2', styles.subheading)}>PASSWORD SUCCESSFULLY CHANGED</h3>
            <p className={styles.infoText}>You can now login to the application with the new password.</p>
            <InspireSimpleLink link={process.env.NEXT_PUBLIC_APP_URL} newtab={false}>
                <InspireButton className={styles.submitButton} text="CLOSE" />
            </InspireSimpleLink>
        </>
    );
};

export default PasswordReset;
