import React from 'react';
import BrandLoader from '../../../../components/atoms/BrandLoader';
import styles from './index.module.css';

const LoadingPage = () => {
    return (
        <div className={styles.wrapper}>
            <BrandLoader />
        </div>
    );
};
export default LoadingPage;
