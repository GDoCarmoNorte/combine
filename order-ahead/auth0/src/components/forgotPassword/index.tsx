import React, { useContext, useEffect, useState } from 'react';
import classNames from 'classnames';

import { StoreContext } from '../../store/index';

import { InspireButton } from '../button';

import FormikInput from '../../../../components/organisms/formikInput';
import { Form, Formik } from 'formik';

import styles from './index.module.css';

import { auth, AuthError } from '../../lib/auth';
import { validateEmail } from '../../../../common/helpers/complexValidateHelper';
import getBrandInfo from '../../../../lib/brandInfo';
import ErrorMessage from '../errorMessage';
import Icon from '../../../../components/atoms/BrandIcon';
import Card from '../card';
import AccountInfoBanner from '../accountInfoBanner';

const validateEmailField = validateEmail('Email');

const errorMessageMapping = {
    invalid_user_password: 'Invalid email or password. Please try again or try to create an account.',
};

const ForgotPassword = (): JSX.Element => {
    const { brandName } = getBrandInfo();
    const {
        actions: { goToSignIn, setError },
    } = useContext(StoreContext);
    const [email, setEmail] = useState<string>('');
    const [errorCode, setErrorCode] = useState<string>();
    const [passwordChangeRequested, setPasswordChangeRequested] = useState<boolean>();

    useEffect(() => {
        document.title = `${brandName} | Sign In`;
    }, [brandName]);

    const initialFormValues = {
        email,
    };

    const handleSubmit = async (formData) => {
        const { email } = formData;

        try {
            await auth.sendPasswordResetEmail({ email });
            setPasswordChangeRequested(true);
            setEmail(email);
        } catch (error) {
            if (error instanceof AuthError) {
                setErrorCode(error.code);
                setError({
                    type: 'FORGOT PASSWORD',
                    code: error.code,
                    message: error.message,
                    statusCode: error.statusCode,
                });
            } else {
                console.error(error);
            }
        }
    };

    return (
        <AccountInfoBanner type="forgotPassword">
            <Card>
                <div className={styles.actionBar}>
                    <Icon className={styles.backIcon} icon="direction-left" />
                    <p tabIndex={0} className={classNames('link-primary-active')} onClick={goToSignIn}>
                        BACK TO SIGN IN
                    </p>
                </div>
                {passwordChangeRequested && !errorCode && <MessageSent email={email} onSignInClick={goToSignIn} />}
                {!passwordChangeRequested && (
                    <RequestEmail
                        errorCode={errorCode}
                        handleSubmit={handleSubmit}
                        initialFormValues={initialFormValues}
                    />
                )}
            </Card>
        </AccountInfoBanner>
    );
};

const RequestEmail = ({ errorCode, handleSubmit, initialFormValues }) => {
    return (
        <>
            <h3 className={classNames('t-header-h2', styles.heading)}>FORGOT PASSWORD</h3>
            {errorCode && <ErrorMessage code={errorCode} messageMap={errorMessageMapping} />}
            <Formik onSubmit={handleSubmit} initialValues={initialFormValues} validateOnMount>
                {({ isValid }) => (
                    <Form>
                        <FormikInput
                            label="Email *"
                            labelClassName={styles.label}
                            name="email"
                            placeholder="Enter Email Address"
                            validate={validateEmailField}
                        />
                        <InspireButton
                            tabIndex={0}
                            className={styles.submitButton}
                            disabled={!isValid}
                            submit
                            text="RECOVER PASSWORD"
                        />
                    </Form>
                )}
            </Formik>
        </>
    );
};

const MessageSent = ({ email, onSignInClick }) => {
    return (
        <>
            <h3 className={classNames('t-header-h2', styles.heading)}>CHECK YOUR EMAIL</h3>
            <p className={styles.infoText}>
                We have sent you an email to <b>{email}</b> with a link to reset your password.
            </p>
            <p className={styles.infoText}>If you don&apos;t see an email from us, check your spam or junk folder.</p>
            <InspireButton tabIndex={0} className={styles.submitButton} onClick={onSignInClick} text="SIGN IN" />
        </>
    );
};

export default ForgotPassword;
