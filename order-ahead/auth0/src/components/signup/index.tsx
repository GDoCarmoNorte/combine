import React, { useContext, useEffect, useState } from 'react';

import classNames from 'classnames';

import { StoreContext } from '../../store/index';

import styles from './index.module.css';
import { auth, AuthError } from '../../lib/auth';
import { useScrollToElement } from '../../../../common/hooks/useScrollToElement';

import ErrorMessage from '../errorMessage';

import getBrandInfo from '../../../../lib/brandInfo';
import Card from '../card';

import SignUpForm from '../signupForm/signupForm';
import { InspireLinkButton } from '../../../../components/atoms/link/linkButton';
import { ERRORS_MAP } from '../errorHandler/errors.constants';
import { genericSignUpErrorMessage, SOCIAL_SIGNUP } from './signup.constants';
import labels from './labels';
import AccountInfoBanner from '../accountInfoBanner';

const errorMessageMapping = {
    ...ERRORS_MAP,
};

const SignUp = (): JSX.Element => {
    const { brandName, brandId } = getBrandInfo();
    const {
        state: { email },
        actions: { goToSignIn, setError, updateEmail },
    } = useContext(StoreContext);
    const [signUpEmail, setSignUpEmail] = useState(email || '');

    const [errorCode, setErrorCode] = useState<string>();

    const [loading, setLoading] = useState(false);

    const brandLabels = labels[brandId];

    useEffect(() => {
        document.title = `${brandName} | Sign Up`;
    }, [brandName]);

    const [element, scrollToElement] = useScrollToElement<HTMLHeadingElement>();

    const handleSubmit = async (formData) => {
        setErrorCode(null);
        setLoading(true);

        try {
            await auth.signUpAndLogin(formData);
        } catch (error) {
            setLoading(false);

            if (error instanceof AuthError) {
                setErrorCode(error.code);
                setError({
                    type: 'SIGN UP',
                    code: error.code,
                    message: error.message,
                    statusCode: error.statusCode,
                });
                scrollToElement();
            } else {
                console.error(error);
            }
        }
    };
    const handleSignUpEmail = (email) => {
        setSignUpEmail(email);
    };

    return (
        <AccountInfoBanner type="signUp">
            <Card>
                <h3 ref={element} className={classNames('t-header-h2', styles.heading)}>
                    CREATE AN ACCOUNT
                </h3>
                {errorCode && (
                    <ErrorMessage
                        code={errorCode}
                        messageMap={errorMessageMapping}
                        specificErrorMessage={genericSignUpErrorMessage}
                    />
                )}

                <SignUpForm
                    onSubmit={handleSubmit}
                    validateOnMount
                    className={styles.form}
                    handleSignUpEmail={handleSignUpEmail}
                    loading={loading}
                    hasAppleSignup={SOCIAL_SIGNUP.APPLE}
                    hasFacebookSignup={SOCIAL_SIGNUP.FACEBOOK}
                />

                <p className={styles.signInLabel}>
                    {brandLabels.FOOTER_TITLE}
                    <InspireLinkButton
                        linkType="secondary"
                        className={styles.signInLink}
                        onClick={() => {
                            updateEmail(signUpEmail);
                            goToSignIn();
                        }}
                    >
                        SIGN IN
                    </InspireLinkButton>
                </p>
            </Card>
        </AccountInfoBanner>
    );
};

export default SignUp;
