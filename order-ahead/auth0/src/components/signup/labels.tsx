import { BrandId } from '../../../../common/constants/brands';

interface ILabels {
    FOOTER_TITLE: string;
}

const labels: { [key in BrandId]: ILabels | null } = {
    Arbys: {
        FOOTER_TITLE: 'Already have an account?',
    },
    Bww: {
        FOOTER_TITLE: 'Already a member?',
    },
    Jje: null,
    Sdi: null,
};

export default labels;
