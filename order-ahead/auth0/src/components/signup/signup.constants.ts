import { getMobileOperatingSystem, MobileOperatingSystem } from '../../../../common/helpers/getMobileOperatingSystem';
import {
    getDesktopOperatingSystem,
    DesktopOperatingSystem,
} from '../../../../common/helpers/getDesktopOperatingSystem';

const showAppleButton =
    getMobileOperatingSystem() === MobileOperatingSystem.IOS ||
    getDesktopOperatingSystem() === DesktopOperatingSystem.MAC;

export const SOCIAL_SIGNUP = {
    FACEBOOK: true,
    APPLE: showAppleButton,
};

export const genericSignUpErrorMessage =
    'We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly.';
