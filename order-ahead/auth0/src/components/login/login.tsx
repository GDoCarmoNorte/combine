import '../../../../public/normalize.css';
import '../../../../styles/variables.css';
import '../../../../styles/global.css';

import React, { useLayoutEffect, useState } from 'react';
import classnames from 'classnames';
import { StylesProvider } from '@material-ui/core/styles';

import Navigation from './../../components/navigation';
import Store from './../../store/index';
import Switcher from './../../components/switcher';
import Logger from '../../components/logger';
import { logoDesktop } from './constants';
// eslint-disable-next-line import/no-unresolved
import logo from '@brand/logo.svg';
// eslint-disable-next-line import/no-unresolved
import styles from './login.module.css';
import LoadingPage from '../loadingPage';

export default function App(): JSX.Element {
    const [hasAuthState, setHasAuthState] = useState(false);

    useLayoutEffect(() => {
        function event(event: CustomEvent) {
            if (event) {
                setHasAuthState(true);
            }
        }

        window.addEventListener('authInit', event);

        return () => {
            window.removeEventListener('authInit', event);
        };
    }, []);

    return (
        <Logger>
            {!hasAuthState ? (
                <LoadingPage />
            ) : (
                <div id="main" className={classnames('auth0-login-background', styles.main)}>
                    <StylesProvider injectFirst>
                        <Store>
                            <div className="sticky-top">
                                <Navigation logo={logo} logoDesktop={logoDesktop} />
                            </div>
                            <Switcher />
                        </Store>
                    </StylesProvider>
                </div>
            )}
        </Logger>
    );
}
