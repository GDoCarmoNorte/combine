import React, { FunctionComponent } from 'react';
import classnames from 'classnames';

import styles from './index.module.css';

export type InspireButtonType = 'primary' | 'secondary' | 'large' | 'small';

export interface IInspireButtonProps {
    link?: string;
    text?: string;
    phone?: string;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    onKeyUp?: (event: React.KeyboardEvent<HTMLButtonElement>) => void;
    disabled?: boolean;
    className?: string;
    linkClassName?: string;
    type?: InspireButtonType;
    gtmId?: string;
    submit?: boolean;
    fullWidth?: boolean;
    tabIndex?: number;
    ariaLabel?: string;
}

export const InspireButton: FunctionComponent<IInspireButtonProps> = (props) => {
    const {
        link,
        type = 'primary',
        phone,
        text,
        onClick,
        onKeyUp,
        children,
        disabled,
        className,
        linkClassName,
        gtmId,
        submit,
        fullWidth,
        tabIndex,
        ariaLabel,
    } = props;

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onClick && onClick(event);
    };

    const handleKeyUp = (event: React.KeyboardEvent<HTMLButtonElement>) => {
        onKeyUp && onKeyUp(event);
    };

    const getLinkDetails = (link: string) => {
        const isExternal = !link.startsWith('/');
        const href = link;

        return {
            href,
            isExternal,
        };
    };

    const buttonClasses = classnames(
        styles.button,
        {
            [styles[type]]: !!type,
            [styles.fullWidth]: !!fullWidth,
        },
        className
    );

    const getChild = () => {
        if (!link) return <>{text}</>;

        const { href, isExternal } = getLinkDetails(link);

        const isPhone = !!phone;

        if (isExternal || isPhone) {
            return (
                <a
                    data-gtm-id={gtmId}
                    target={isExternal ? '_blank' : undefined}
                    rel="noreferrer"
                    href={isPhone ? `tel:${phone}` : href}
                    className={linkClassName}
                >
                    {text}
                </a>
            );
        }

        return (
            <a data-gtm-id={gtmId} className={linkClassName} href={href}>
                {text}
            </a>
        );
    };

    return (
        <button
            type={submit ? 'submit' : 'button'}
            data-gtm-id={gtmId}
            disabled={disabled}
            className={buttonClasses}
            onClick={handleClick}
            onKeyUp={handleKeyUp}
            tabIndex={tabIndex >= 0 ? tabIndex : -1}
            aria-label={ariaLabel}
        >
            {children || getChild()}
        </button>
    );
};
