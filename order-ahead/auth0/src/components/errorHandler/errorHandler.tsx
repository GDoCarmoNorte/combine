import React, { useContext, useState, useEffect } from 'react';

import { StoreContext } from '../../store';
import styles from './errorHandler.module.css';
import Notification from '../../../../components/organisms/notifications/notification';
import { ERRORS_MAP } from './errors.constants';
import { defaultHttpStatusCodeErrorMap, NetworkErrorMessage } from '../../../../common/services/createErrorWrapper';
import { NOTIFICATION_SHOW_TIME } from '../../../../common/constants/notification';

export default function ErrorHandler(): JSX.Element {
    const {
        state: { error },
        actions,
    } = useContext(StoreContext);
    const [timeoutId, setTimeoutId] = useState(null);

    const { statusCode, code, type } = error;

    useEffect(() => {
        if (type) {
            const timeoutId = setTimeout(() => actions.removeError(), NOTIFICATION_SHOW_TIME);
            setTimeoutId(timeoutId);
        }

        return () => clearTimeout(timeoutId);
    }, [statusCode, code, type]);

    const errorMessage = statusCode
        ? ERRORS_MAP[code] || defaultHttpStatusCodeErrorMap[statusCode] || 'Unknown Error'
        : NetworkErrorMessage;

    return type ? (
        <div className={styles.container}>
            <Notification title={type} message={errorMessage} onCloseClick={actions.removeError} />
        </div>
    ) : null;
}
