export const USERNAME_EXISTS = 'username_exists';
export const USER_EXISTS = 'user_exists';
export const PASSWORD_NO_USER_INFO_ERROR = 'password_dictionary_error';
export const PASSWORD_STRENGTH_ERROR = 'password_strength_error';
export const TOO_MANY_ATTEMPTS = 'too_many_attempts';
export const INVALID_USER_PASSWORD = 'invalid_user_password';
export const PHONE_NUMBER_EXISTS = 'phone_number_exists';
export const INVALID_POSTAL_CODE = 'invalid_postal_code';
export const USER_EXISTS_FIREBASE = 'user_exists_firebase';

const USER_EXISTS_ERROR = 'This email account is already in use by another user. Please specify different one.';

export const ERRORS_MAP = {
    [USERNAME_EXISTS]: USER_EXISTS_ERROR,
    [USER_EXISTS]: USER_EXISTS_ERROR,
    [PASSWORD_NO_USER_INFO_ERROR]: 'Please try another password',
    [PASSWORD_STRENGTH_ERROR]:
        'The chosen password is too weak (Min. 8 characters, one number, one uppercase letter, one lowercase letter and at least one symbol)',
    [TOO_MANY_ATTEMPTS]: 'The account is blocked due to too many attempts to sign in',
    [INVALID_USER_PASSWORD]:
        'We apologize. Our system was unable to verify your email and/or password. Please try again, or retrieve your forgotten password or setup a new account.',
    [PHONE_NUMBER_EXISTS]: 'This phone number is already in use by another user. Please specify different one.',
    [INVALID_POSTAL_CODE]: 'Enter a valid postal code',
    [USER_EXISTS_FIREBASE]: USER_EXISTS_ERROR,
};
