import React, { useEffect } from 'react';
import { pushErrorEvent } from '../../lib/helpers';
import Icon from '../../../../components/atoms/BrandIcon';

import styles from './index.module.css';

interface IErrorMessageProps {
    code: string;
    messageMap?: {
        [code: string]: string;
    };
    specificErrorMessage?: string;
}

const genericMessage =
    'We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly.';

const ErrorMessage = ({ code, messageMap, specificErrorMessage }: IErrorMessageProps): JSX.Element => {
    const errorMessage = messageMap?.[code] || specificErrorMessage || genericMessage;

    useEffect(() => {
        if (errorMessage && errorMessage !== genericMessage) {
            pushErrorEvent({
                ErrorCategory: 'Account Creation Error',
                ErrorDescription: errorMessage,
            });
        }
    }, [errorMessage]);

    return (
        <div className={styles.container}>
            <Icon className={styles.errorIcon} icon="info-error" />
            <span className={styles.errorText}>{errorMessage}</span>
        </div>
    );
};

export default ErrorMessage;
