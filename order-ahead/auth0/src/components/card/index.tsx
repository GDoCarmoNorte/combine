import React, { ReactNode } from 'react';

import styles from './index.module.css';
import classNames from 'classnames';

interface IProps {
    children: ReactNode;
    containerClassName?: string;
}

export default function Card({ children, containerClassName }: IProps): JSX.Element {
    return (
        <div className={styles.wrapper}>
            <div className={classNames(styles.formContainer, containerClassName)}>{children}</div>
        </div>
    );
}
