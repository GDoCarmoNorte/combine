const path = require('path');

module.exports = {
    displayName: 'dom',
    collectCoverageFrom: [
        '**/*.{ts,tsx}',
        '<rootDir>/webpack/**',
        '!**/*.d.ts',
        '!**/node_modules/**',
        '!**/tests/**',
        '!**/widgets/**',
        '!<rootDir>/@generated/**',
        '!<rootDir>/@types/',
        '!**/.next/**',
        '!**/.utils/**',
        '!<rootDir>/legacy/**',
        '!<rootDir>/jest/**',
        '!<rootDir>/config/**',
        '!<rootDir>/public/**',
        '!<rootDir>/.gitlab/**',
        '!<rootDir>/docker/**',
        '!<rootDir>/gradle/**',
        '!<rootDir>/manifests/**',
        '!**/webpack.config.ts',
    ],
    coverageDirectory: '.coverage',
    coverageReporters: ['lcov', 'text-summary'],
    coverageThreshold: {
        global: {
            statements: 92,
            lines: 94,
            functions: 79,
            branches: 83,
        },
    },
    slowTestThreshold: 5,
    testEnvironment: 'jsdom',
    snapshotSerializers: ['enzyme-to-json/serializer'],
    testRegex: '^.+\\.(test|spec)\\.(tsx|jsx|js|ts)$',
    transform: {
        '^.+\\.(ts|tsx|js)?$': 'ts-jest',
    },
    preset: 'ts-jest',
    setupFiles: ['jest-date-mock'],
    setupFilesAfterEnv: ['<rootDir>/jest/setupTestsAfterEnv.js'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
    moduleDirectories: [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, '..', 'node_modules')],
    moduleNameMapper: {
        '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
            '<rootDir>/jest/__mocks__/fileMock.js',
        '\\.(css|less)$': 'identity-obj-proxy',
    },
    testResultsProcessor: 'jest-junit',
    testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/', '<rootDir>/legacy/'],
    globals: {
        // should be an empty object for now
        FEATURE_FLAGS: {},
        'ts-jest': {
            isolatedModules: true,
        },
    },
    reporters: ['default'],
    verbose: true,
};
