import '@testing-library/jest-dom';

import jestFetchMock from 'jest-fetch-mock';
// a11y
import { toHaveNoViolations } from 'jest-axe';

jestFetchMock.enableMocks();

expect.extend(toHaveNoViolations);

// env variables
process.env.NEXT_PUBLIC_BRAND = 'arbys';

process.env.NEXT_PUBLIC_DOMAIN_CUSTOMER = '/domainCustomerPath';
process.env.NEXT_PUBLIC_WEB_EXP_API = '/webExpApiPath';

process.env.NEXT_PUBLIC_RIO_LOCATIONS_DOMAIN = 'https://locations.arbys.com';

process.env.CONTENTFUL_SPACE = 'mock-space';
process.env.CONTENTFUL_ENV = 'mock-env';
process.env.CONTENTFUL_DELIVERY_TOKEN = 'mock-delivery-token';
process.env.CONTENTFUL_PREVIEW_TOKEN = 'mock-preview-token';

process.env.NEXT_PUBLIC_GOOGLE_MAP_API_KEY = 'mock-google-key';

process.env.ENVIRONMENT_NAME = 'arb-dev01';

process.env.NEXT_PUBLIC_APP_URL = 'http://www.test.app.url';
process.env.NEXT_PUBLIC_MENU_VERSION = 2;

process.env.NEXT_PUBLIC_AUTH_0_CLIENT_ID = 'AUTH0CLIENTID';

const localStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn(),
    removeItem: jest.fn(),
};
Object.defineProperty(window, 'localStorage', { value: localStorageMock });

const sessionStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn(),
    removeItem: jest.fn(),
};
Object.defineProperty(window, 'sessionStorage', { value: sessionStorageMock });

const newrelicMock = {
    noticeError: jest.fn(),
    addPageAction: jest.fn(),
    setCustomAttribute: jest.fn(),
    addRelease: jest.fn(),
};

Object.defineProperty(window, 'newrelic', { value: newrelicMock });

// mock useLayoutEffect on SSR
jest.mock('react', () => ({
    ...jest.requireActual('react'),
    useLayoutEffect: jest.requireActual('react').useEffect,
}));

// mock material-ui JSS as we do not support it yet
jest.mock('@material-ui/styles/makeStyles', () => ({
    __esModule: true,
    default: () => jest.fn().mockImplementation(() => ({})),
}));
