export const AUTH0_SIGN_UP = 'auth0-sign-up';
export const AUTH0_SIGN_IN = 'auth0-sign-in';
export const AUTH0_FORGOT_PASSWORD = 'auth0-forgot-password';
export const AUTH0_PASSWORD_RESET = 'auth0-password-reset';
