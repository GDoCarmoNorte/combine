import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';

import Login from '../../src/components/login/login';
import { axe } from 'jest-axe';

jest.mock('../../src/lib/auth');

describe('app component', () => {
    it('should render progressbar by default', async () => {
        render(<Login />);

        await waitFor(() => {
            expect(screen.getByRole('progressbar')).toBeInTheDocument();
        });
    });

    it('should not render default progressbar after initial auth0 event', async () => {
        jest.spyOn(React, 'useState').mockReturnValue([true, jest.fn()]);

        render(<Login />);

        await waitFor(() => {
            expect(screen.queryByRole('progressbar')).not.toBeInTheDocument();
        });
    });

    // TODO: fix a11y
    it.skip('a11y', async () => {
        const { container } = render(<Login />);

        const results = await axe(container);

        expect(results).toHaveNoViolations();
    });
});
