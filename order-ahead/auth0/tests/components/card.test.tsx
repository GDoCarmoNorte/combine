import React from 'react';
import { render, screen } from '@testing-library/react';

import Card from '../../src/components/card';

describe('Card component', () => {
    it('should render children', () => {
        render(<Card>Hello World!</Card>);

        screen.getByText(/Hello World!/i);
    });
});
