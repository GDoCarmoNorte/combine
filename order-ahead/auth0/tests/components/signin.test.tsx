import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import SignIn from '../../../auth0/src/components/signin';
import { auth, AuthError } from '../../src/lib/auth';
import { initialState, StoreContext } from '../../src/store';

describe('sign in component', () => {
    it('should load and display form', () => {
        const { container } = render(<SignIn />);

        expect(container).toMatchSnapshot();
    });

    it('should have submit button disabled initially', async () => {
        render(<SignIn />);

        await waitFor(() => {
            expect(screen.getByRole('button', { name: 'SIGN IN' })).toHaveAttribute('disabled');
        });
    });

    it('should have loader on submit button', async () => {
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce(['', jest.fn()])
            .mockReturnValueOnce(['', jest.fn()])
            .mockReturnValueOnce([true, jest.fn()]);
        render(<SignIn />);

        await waitFor(() => {
            expect(screen.getByRole('progressbar')).toBeInTheDocument();
        });
    });

    it('should have gtm ids', async () => {
        render(<SignIn />);

        await waitFor(() => {
            expect(screen.queryByText(/Forgot password?/i)).toHaveAttribute('data-gtm-id', 'forgotPassword');
        });
        expect(screen.queryByText(/CREATE AN ACCOUNT/i)).toHaveAttribute('data-gtm-id', 'createAnAccount');
    });

    it('should display error when nothing is typed in email field', async () => {
        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Email *'), '');
        userEvent.click(screen.getByLabelText('Password *'));

        await waitFor(() => {
            screen.getByText(/Email is incomplete/i);
        });
    });

    it('should display error when invalid email is typed in email field', async () => {
        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Email *'), '12345');
        userEvent.click(screen.getByLabelText('Password *'));

        await waitFor(() => {
            screen.getByText(/Incorrect email/i);
        });
    });

    it('should call updateEmail action when "CREATE AN ACCOUNT" button is clicked', async () => {
        const state = initialState;
        const updateEmailMock = jest.fn();
        const goToSignUpMock = jest.fn();
        render(
            <StoreContext.Provider
                value={{ state, actions: { updateEmail: updateEmailMock, goToSignUp: goToSignUpMock } as any }}
            >
                <SignIn />
            </StoreContext.Provider>
        );

        userEvent.type(screen.getByLabelText('Email *'), '12345');
        userEvent.click(screen.getByLabelText('Password *'));

        expect(updateEmailMock).not.toHaveBeenCalled();
        expect(goToSignUpMock).not.toHaveBeenCalled();

        userEvent.click(screen.getByText('CREATE AN ACCOUNT'));

        expect(updateEmailMock).toHaveBeenCalledWith('12345');
        expect(goToSignUpMock).toHaveBeenCalled();
    });

    it('should display "Password is incomplete" when nothing is typed in password field', async () => {
        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Password *'), '');
        userEvent.click(screen.getByLabelText('Email *'));

        await waitFor(() => {
            screen.getByText(/Password is incomplete/i);
        });
    });

    it('should call auth.login method on submit when all fields are valid', async () => {
        const loginMock = jest.fn();
        ((auth.login as unknown) as jest.Mock) = loginMock;

        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Email *'), 'test@email.com');
        userEvent.type(screen.getByLabelText('Password *'), '123123123qQ!');

        userEvent.click(screen.getByRole('button', { name: 'SIGN IN' }));
        // expect(screen.getByRole('progressbar')).toBeInTheDocument();
        await waitFor(() =>
            expect(loginMock).toHaveBeenCalledWith({
                email: 'test@email.com',
                password: '123123123qQ!',
            })
        );
    });

    it('should display error on submit when auth.login throws AuthError', async () => {
        const loginMock = jest.fn().mockRejectedValue(new AuthError('invalid_user_password', 'message'));
        ((auth.login as unknown) as jest.Mock) = loginMock;

        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Email *'), 'test@email.com');
        userEvent.type(screen.getByLabelText('Password *'), 'aB1!___8');

        userEvent.click(screen.getByRole('button', { name: 'SIGN IN' }));

        await waitFor(() => expect(loginMock).toHaveBeenCalled());
        await waitFor(() =>
            expect(
                screen.getByText(
                    'We apologize. Our system was unable to verify your email and/or password. Please try again, or retrieve your forgotten password or setup a new account.'
                )
            )
        );
    });

    it('should log error on submit when auth.login throws not AuthError', async () => {
        const consoleSpy = jest.spyOn(console, 'error');
        const loginMock = jest.fn().mockRejectedValue(new Error('message'));
        ((auth.login as unknown) as jest.Mock) = loginMock;

        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Email *'), 'test@email.com');
        userEvent.type(screen.getByLabelText('Password *'), 'aB1!___8');

        userEvent.click(screen.getByRole('button', { name: 'SIGN IN' }));

        await waitFor(() => expect(loginMock).toHaveBeenCalled());
        await waitFor(() => expect(consoleSpy).toBeCalledWith(new Error('message')));
    });

    it("shouldn't display error message when email have symbol +", async () => {
        render(<SignIn />);

        userEvent.type(screen.getByLabelText('Email *'), 'test+50@email.com');
        userEvent.click(screen.getByLabelText('Password *'));

        await waitFor(() => {
            expect(screen.queryByText(/Incorrect email/i)).not.toBeInTheDocument();
        });
    });

    // it.skip('should display forgot password form when clicking "forgot password?" button', async () => {
    //     // TODO: fix - test not working although passing.
    //     render(<SignIn />);
    //     userEvent.click(screen.getByLabelText('Forgot Password?'));

    //     expect(screen.findByRole('heading', { name: 'FORGOT PASSWORD' }));
    //     expect(screen.findByLabelText('Email *'));
    //     expect(screen.findByRole('button', { name: 'RECOVER PASSWORD' }));
    // });
});
