import React from 'react';
import { axe } from 'jest-axe';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import TestUtils from 'react-dom/test-utils';

import SignUp from '../../src/components/signup';

jest.mock('../../../lib/getFeatureFlags', () => ({
    isSignUpPhoneTooltip: jest.fn().mockReturnValue(true),
}));

jest.mock('../../src/lib/auth', () => ({
    __esModule: true,
    // @ts-ignore
    ...jest.requireActual('../../src/lib/auth'),
    auth: {
        signUpAndLogin: jest.fn(),
    },
}));
import { auth, AuthError } from '../../src/lib/auth';
import { initialState, StoreContext } from '../../src/store';
import { InspireButton } from '../../src/components/button';

// Open issue about testing components with userEvent and react-text-mask
// https://github.com/sanniassin/react-input-mask/issues/174#issuecomment-743882703
function changeInputMaskValue(element, value) {
    // eslint-disable-next-line no-param-reassign
    element.value = value;
    // eslint-disable-next-line no-param-reassign
    element.selectionStart = element.selectionEnd = value.length;
    TestUtils.Simulate.change(element);
}

describe('sign up component', () => {
    it('should have submit button disabled initially', async () => {
        render(<SignUp />);

        await waitFor(() => {
            expect(screen.getByRole('button', { name: 'CREATE AN ACCOUNT' })).toHaveAttribute('disabled');
        });
    });

    it('should have loader on submit button', async () => {
        jest.spyOn(React, 'useState')
            .mockReturnValueOnce(['', jest.fn()])
            .mockReturnValueOnce(['', jest.fn()])
            .mockReturnValueOnce([true, jest.fn()]);
        render(<SignUp />);

        await waitFor(() => {
            expect(screen.getByRole('progressbar')).toBeInTheDocument();
        });
    });

    it('should display error when non alpha symbol is typed in first name field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('First Name *'), '12345');
        userEvent.click(screen.getByLabelText('Last Name *'));

        await waitFor(() => {
            screen.getByText(/Alpha characters only/i);
        });
    });

    it('should display error when nothing is typed in first name field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('First Name *'), '');
        userEvent.click(screen.getByLabelText('Last Name *'));

        await waitFor(() => {
            screen.getByText(/First Name is incomplete/i);
        });
    });

    it("should trim first name if it's more than 256 characters first name field", async () => {
        render(<SignUp />);

        const value = 'a'.repeat(256);

        userEvent.type(screen.getByLabelText('First Name *'), value + 'b');
        userEvent.click(screen.getByLabelText('Last Name *'));

        await waitFor(() => {
            expect(screen.getByLabelText('First Name *')).toHaveValue(value);
        });
    });

    it('should display error when non alpha symbol is typed in last name field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Last Name *'), '12345');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Alpha characters only/i);
        });
    });

    it('should display error when nothing is typed in last name field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Last Name *'), '');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Last Name is incomplete/i);
        });
    });

    it("should trim last name if it's more than 256 characters last name field", async () => {
        render(<SignUp />);

        const value = 'a'.repeat(256);

        userEvent.type(screen.getByLabelText('Last Name *'), value + 'b');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            expect(screen.getByLabelText('Last Name *')).toHaveValue(value);
        });
    });

    it('should display error when nothing is typed in email field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Email *'), '');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Email is incomplete/i);
        });
    });

    it('should display error when invalid email is typed in email field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Email *'), '12345');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Incorrect email/i);
        });
    });

    it("shouldn't display error message when email have symbol +", async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Email *'), 'test+50@email.com');
        userEvent.click(screen.getByLabelText('Password *'));

        await waitFor(() => {
            expect(screen.queryByText(/Incorrect email/i)).not.toBeInTheDocument();
        });
    });

    it('should call updateEmail action when "SIGN IN" button is clicked', async () => {
        const state = initialState;
        const updateEmailMock = jest.fn();
        const goToSignInMock = jest.fn();
        render(
            <StoreContext.Provider
                value={{ state, actions: { updateEmail: updateEmailMock, goToSignIn: goToSignInMock } as any }}
            >
                <SignUp />
            </StoreContext.Provider>
        );

        userEvent.type(screen.getByLabelText('Email *'), '12345');
        userEvent.click(screen.getByLabelText('Password *'));

        expect(goToSignInMock).not.toHaveBeenCalled();

        userEvent.click(screen.getByText('SIGN IN'));

        expect(updateEmailMock).toHaveBeenCalledWith('12345');
        expect(goToSignInMock).toHaveBeenCalled();
    });

    it('should display error when invalid phone is typed in phone field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Phone *'), '12312');
        changeInputMaskValue(screen.getByLabelText('Phone *'), '12312');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Incorrect phone number/i);
        });
    });

    it('should display error when invalid DOB is typed in DOB field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Date Of Birth'), '00/00');
        changeInputMaskValue(screen.getByLabelText('Date Of Birth'), '00/00');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText('Incorrect format of Date Of Birth (MM/DD)');
        });
    });
    it('should display error when nothing is typed in Zip Code field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Zip Code *'), '');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Zip Code is incomplete/i);
        });
    });
    it('should display error when invalid zipcode is typed in Zip Code field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Zip Code *'), '123');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText('Zip Code is invalid');
        });
    });

    it('should display error when nothing is typed in password field', async () => {
        render(<SignUp />);

        userEvent.type(screen.getByLabelText('Password *'), '');
        userEvent.click(screen.getByLabelText('First Name *'));

        await waitFor(() => {
            screen.getByText(/Password is incomplete/i);
        });
    });

    it('should call auth.signUpAndLogin method on submit when all fields are valid', async () => {
        const signupAndLoginMock = jest.fn();
        ((auth.signUpAndLogin as unknown) as jest.Mock) = signupAndLoginMock;

        render(<SignUp />);

        userEvent.type(screen.getByLabelText('First Name *'), 'FirstName');
        userEvent.type(screen.getByLabelText('Last Name *'), 'LastName');
        userEvent.type(screen.getByLabelText('Email *'), 'test@email.com');
        userEvent.type(screen.getByLabelText('Phone *'), '(123) 456-7890');
        changeInputMaskValue(screen.getByLabelText('Phone *'), '(123) 456-7890');
        userEvent.type(screen.getByLabelText('Date Of Birth'), '02-22');
        changeInputMaskValue(screen.getByLabelText('Date Of Birth'), '02-22');
        userEvent.type(screen.getByLabelText('Password *'), 'aB1!___8');
        userEvent.type(screen.getByLabelText('Zip Code *'), '44024');
        userEvent.click(screen.getByLabelText('Terms and Conditions', { exact: false }));

        userEvent.click(screen.getByRole('button', { name: 'CREATE AN ACCOUNT' }));
        expect(screen.getByRole('button', { name: 'CREATE AN ACCOUNT' })).not.toHaveAttribute('disabled');
        await waitFor(() =>
            expect(signupAndLoginMock).toHaveBeenCalledWith({
                birthDate: '02-22',
                email: 'test@email.com',
                firstName: 'FirstName',
                lastName: 'LastName',
                password: 'aB1!___8',
                phone: '123-456-7890',
                zipCode: '44024',
                termsAgreed: true,
                marketingOptIn: true,
            })
        );
    });

    it('should display error on submit when auth.signUpAndLogin throws AuthError', async () => {
        const signupAndLoginMock = jest.fn().mockRejectedValue(new AuthError('code', 'message'));
        ((auth.signUpAndLogin as unknown) as jest.Mock) = signupAndLoginMock;

        const { asFragment } = render(<SignUp />);

        userEvent.type(screen.getByLabelText('First Name *'), 'TestFirst');
        userEvent.type(screen.getByLabelText('Last Name *'), 'TestFirst');
        userEvent.type(screen.getByLabelText('Email *'), 'test@email.com');
        userEvent.type(screen.getByLabelText('Phone *'), '(123) 456-7890');
        changeInputMaskValue(screen.getByLabelText('Phone *'), '(123) 456-7890');
        userEvent.type(screen.getByLabelText('Date Of Birth'), '02-22');
        changeInputMaskValue(screen.getByLabelText('Date Of Birth'), '02-22');
        userEvent.type(screen.getByLabelText('Password *'), 'aB1!___8');
        userEvent.type(screen.getByLabelText('Zip Code *'), '44024');
        userEvent.click(screen.getByLabelText('Terms and Conditions', { exact: false }));

        userEvent.click(screen.getByRole('button', { name: 'CREATE AN ACCOUNT' }));

        await waitFor(() => expect(signupAndLoginMock).toHaveBeenCalled());
        await waitFor(() => expect(asFragment()).toMatchSnapshot());
    });

    it('should log error on submit when auth.signUpAndLogin throws not AuthError', async () => {
        const consoleSpy = jest.spyOn(console, 'error');
        const signupAndLoginMock = jest.fn().mockRejectedValue(new Error('message'));
        ((auth.signUpAndLogin as unknown) as jest.Mock) = signupAndLoginMock;

        render(<SignUp />);

        userEvent.type(screen.getByLabelText('First Name *'), 'TestFirst');
        userEvent.type(screen.getByLabelText('Last Name *'), 'TestFirst');
        userEvent.type(screen.getByLabelText('Email *'), 'test@email.com');
        userEvent.type(screen.getByLabelText('Phone *'), '(123) 456-7890');
        changeInputMaskValue(screen.getByLabelText('Phone *'), '(123) 456-7890');
        userEvent.type(screen.getByLabelText('Date Of Birth'), '02-22');
        changeInputMaskValue(screen.getByLabelText('Date Of Birth'), '02-22');
        userEvent.type(screen.getByLabelText('Password *'), 'aB1!___8');
        userEvent.type(screen.getByLabelText('Zip Code *'), '44024');
        userEvent.click(screen.getByLabelText('Terms and Conditions', { exact: false }));

        userEvent.click(screen.getByRole('button', { name: 'CREATE AN ACCOUNT' }));

        await waitFor(() => expect(signupAndLoginMock).toHaveBeenCalled());
        await waitFor(() => expect(consoleSpy).toBeCalledWith(new Error('message')));
    });

    // TODO: fix a11y
    it.skip('a11y', async () => {
        const { container } = render(<SignUp />);

        const results = await axe(container);

        expect(results).toHaveNoViolations();
    });

    it('should call the given function on click', () => {
        const loginWithFacebookFnMock = jest.fn();
        render(
            <InspireButton
                onClick={loginWithFacebookFnMock}
                type={'secondary'}
                text={'Sign in with Facebook'}
                tabIndex={0}
            />
        );
        const socialButtonFacebook = screen.getByText(/Sign in with Facebook/i);
        fireEvent.click(socialButtonFacebook);

        expect(loginWithFacebookFnMock).toHaveBeenCalled();
    });

    it('should call signInWithApple button after click', () => {
        const signInWithAppleFnMock = jest.fn();

        render(
            <InspireButton onClick={signInWithAppleFnMock} type="secondary" text="Sign in with Apple" tabIndex={0} />
        );

        const getAppleButton = screen.getByText(/Sign in with Apple/i);
        expect(getAppleButton).toBeInTheDocument();

        userEvent.click(getAppleButton);
        expect(signInWithAppleFnMock).toHaveBeenCalledTimes(1);
    });
});

describe('tooltips in sign up', () => {
    it('should display phone tooltip', async () => {
        render(<SignUp />);
        const phoneTooltipIcon = screen.getByLabelText('informational icon phone');
        userEvent.click(phoneTooltipIcon);

        await waitFor(() => {
            screen.getByText(/Your mobile phone will be used as your member ID/i);
        });
    });
});
