import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import Navigation from '../../../auth0/src/components/navigation';
import { auth } from '../../src/lib/auth';

jest.mock('../../src/lib/auth');

const mockReplace = jest.fn();
describe('navigation component', () => {
    it('should render link if logo prop is passed', async () => {
        render(<Navigation logo="/img.png" />);

        await screen.findByRole('link');
        await screen.findByLabelText(/Main Page/i);
    });

    it('should render link if logo and logoDesktop props are passed', async () => {
        render(<Navigation logo="/img.png" logoDesktop="/imgText.png" />);

        await screen.findByRole('link');
        await screen.findByLabelText(/Main Page/i);
    });

    it('should render link if pictureComponent prop is passed', async () => {
        render(<Navigation logo="/img.png" />);

        await screen.findByRole('link');
        await screen.findByLabelText(/Main Page/i);
    });

    it('close button should have "Close" aria-label', () => {
        render(<Navigation logo="/img.png" />);

        screen.getByLabelText(/Close/i);
    });

    it('should call onClick after click on icon', () => {
        delete window.location;
        (window.location as any) = { replace: mockReplace };
        render(<Navigation logo="/img.png" />);

        const closeIcon = screen.getByLabelText(/Close/i);
        fireEvent.click(closeIcon);
        expect(mockReplace).toBeCalledTimes(1);
    });

    it('should not display close icon when auth.isWeb returns false', () => {
        ((auth.isWeb as unknown) as jest.Mock).mockImplementation(() => false);

        render(<Navigation logo="/img.png" />);

        const closeIcon = screen.queryByLabelText(/Close/i);
        expect(closeIcon).not.toBeInTheDocument();
    });
});
