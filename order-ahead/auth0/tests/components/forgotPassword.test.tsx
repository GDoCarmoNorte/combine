import React from 'react';
import { axe } from 'jest-axe';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ForgotPassword from '../../../auth0/src/components/forgotPassword';
import { auth, AuthError } from '../../src/lib/auth';
import PasswordReset from '../../src/components/passwordReset';

describe('forgot password component', () => {
    it('should load and display forgot password', () => {
        render(<ForgotPassword />);

        screen.getByRole('heading', { name: /FORGOT PASSWORD/i });
        screen.getByLabelText('Email *');
        screen.getByRole('button', { name: /RECOVER PASSWORD/i });
    });

    it('should have recover password button disabled by default', async () => {
        render(<ForgotPassword />);

        await waitFor(() => {
            expect(screen.getByRole('button', { name: 'RECOVER PASSWORD' })).toHaveAttribute('disabled');
        });
    });

    it('should display error when nothing is typed in email field', async () => {
        render(<ForgotPassword />);

        userEvent.type(screen.getByLabelText('Email *'), '');
        userEvent.click(screen.getByRole('heading', { name: 'FORGOT PASSWORD' }));

        await screen.findByText('Email is incomplete');
    });

    it('should display error when invalid email is typed in email field', async () => {
        render(<ForgotPassword />);

        userEvent.type(screen.getByLabelText('Email *'), '12345');
        userEvent.click(screen.getByRole('heading', { name: 'FORGOT PASSWORD' }));

        await screen.findByText('Incorrect email');
    });

    it('should display "Something went wrong. Please try again." when auth.sendPasswordResetEmail throws AuthError', async () => {
        const sendPasswordResetEmailMock = jest.fn().mockRejectedValueOnce(new AuthError('code', 'message'));
        ((auth.sendPasswordResetEmail as unknown) as jest.Mock) = sendPasswordResetEmailMock;

        render(<ForgotPassword />);

        userEvent.type(screen.getByLabelText('Email *'), 'test@test.com');
        userEvent.click(screen.getByRole('button', { name: 'RECOVER PASSWORD' }));

        await screen.findByText(
            'We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly.'
        );
    });

    it('should display success block with provided email after email was sent', async () => {
        const sendPasswordResetEmailMock = jest.fn().mockResolvedValueOnce(null);
        ((auth.sendPasswordResetEmail as unknown) as jest.Mock) = sendPasswordResetEmailMock;

        render(<ForgotPassword />);

        userEvent.type(screen.getByLabelText('Email *'), 'test@test.com');
        userEvent.click(screen.getByRole('button', { name: 'RECOVER PASSWORD' }));

        await screen.findByRole('heading', { name: 'CHECK YOUR EMAIL' });
        await screen.findByText('We have sent you an email to', { exact: false });
        await screen.findByText('test@test.com');
        await screen.findByText('with a link to reset your password.', { exact: false });
    });

    it('should display "Check your email" page after email was sent', async () => {
        const sendPasswordResetEmailMock = jest.fn().mockResolvedValueOnce(null);
        ((auth.sendPasswordResetEmail as unknown) as jest.Mock) = sendPasswordResetEmailMock;

        render(<ForgotPassword />);

        userEvent.type(screen.getByLabelText('Email *'), 'test@test.com');
        userEvent.click(screen.getByRole('button', { name: /RECOVER PASSWORD/i }));

        await screen.findByRole('heading', { name: /CHECK YOUR EMAIL/i });
        screen.getByText(/If you don't see an email from us, check your spam or junk folder/i);
        screen.getByRole('button', { name: /sign in/i });
    });

    it('a11y', async () => {
        const { container } = render(<PasswordReset />);

        const results = await axe(container);

        expect(results).toHaveNoViolations();
    });
});
