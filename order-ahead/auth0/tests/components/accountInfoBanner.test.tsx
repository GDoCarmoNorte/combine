import React from 'react';
import { render, screen } from '@testing-library/react';
import AccountInfoBanner from '../../src/components/accountInfoBanner';
import AccountInfoText from '../../src/components/accountInfoBanner/accountInfoText';

describe('AccountInfoBanner', () => {
    it('should render info banner for signup', () => {
        render(
            <AccountInfoBanner type="signUp">
                <></>
            </AccountInfoBanner>
        );

        const signUpText = AccountInfoText['signUp'];
        expect(screen.getByText(signUpText.TITLE)).toBeInTheDocument();
        expect(screen.getByText(signUpText.DESCRIPTION)).toBeInTheDocument();
    });

    it('should render info banner for signin', () => {
        render(
            <AccountInfoBanner type="signIn">
                <></>
            </AccountInfoBanner>
        );

        const signInText = AccountInfoText['signIn'];
        expect(screen.getByText(signInText.TITLE)).toBeInTheDocument();
        expect(screen.getByText(signInText.MAIN_TEXT)).toBeInTheDocument();
    });

    it('should render info banner for forget password', () => {
        render(
            <AccountInfoBanner type="forgotPassword">
                <></>
            </AccountInfoBanner>
        );

        const forgotPasswordText = AccountInfoText['forgotPassword'];
        expect(screen.getByText(forgotPasswordText.TITLE)).toBeInTheDocument();
        expect(screen.getByText(forgotPasswordText.MAIN_TEXT)).toBeInTheDocument();
    });
});
