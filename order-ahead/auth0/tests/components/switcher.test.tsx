import React from 'react';
import { render, screen } from '@testing-library/react';
import Switcher from '../../../auth0/src/components/switcher';
import { StoreContext } from '../../../auth0/src/store';

const initialActions = {
    goToSignUp: null,
    goToSignIn: null,
    updateEmail: null,
    goToForgotPassword: null,
    setError: null,
    removeError: null,
};

jest.mock('../../src/lib/auth');

describe('switcher component', () => {
    it('should not render loader component when context screen value is null', async () => {
        const wrapper = (
            <StoreContext.Provider
                value={{
                    state: { screen: null, email: '', error: {} },
                    actions: initialActions,
                }}
            >
                <Switcher />
            </StoreContext.Provider>
        );
        render(wrapper);

        expect(screen.queryByRole('progressbar')).not.toBeInTheDocument();
    });

    it('should render signin component when context screen value is signin', () => {
        const wrapper = (
            <StoreContext.Provider
                value={{
                    state: { screen: 'signin', email: '', error: {} },
                    actions: initialActions,
                }}
            >
                <Switcher />
            </StoreContext.Provider>
        );
        render(wrapper);

        expect(screen.getByRole('button', { name: /^SIGN IN$/i })).toBeInTheDocument();
    });

    it('should render signup component when context screen value is signup', async () => {
        const wrapper = (
            <StoreContext.Provider
                value={{
                    state: { screen: 'signup', email: '', error: {} },
                    actions: initialActions,
                }}
            >
                <Switcher />
            </StoreContext.Provider>
        );
        render(wrapper);

        expect(screen.getByRole('button', { name: /^CREATE AN ACCOUNT$/i })).toBeInTheDocument();
    });

    it('should render forgotpassword component when context screen value is forgotpassword', async () => {
        const wrapper = (
            <StoreContext.Provider
                value={{
                    state: { screen: 'forgotpassword', email: '', error: {} },
                    actions: initialActions,
                }}
            >
                <Switcher />
            </StoreContext.Provider>
        );
        render(wrapper);

        expect(screen.getByRole('heading', { name: /FORGOT PASSWORD/i })).toBeInTheDocument();
    });
});
