import React from 'react';
import { render, screen } from '@testing-library/react';

import ErrorHandler from '../../../auth0/src/components/errorHandler/errorHandler';
import { NetworkErrorMessage, defaultHttpStatusCodeErrorMap } from '../../../common/services/createErrorWrapper';
import {
    ERRORS_MAP,
    USERNAME_EXISTS,
    PASSWORD_NO_USER_INFO_ERROR,
    TOO_MANY_ATTEMPTS,
    INVALID_USER_PASSWORD,
    PHONE_NUMBER_EXISTS,
    INVALID_POSTAL_CODE,
} from '../../../auth0/src/components/errorHandler/errors.constants';
import HttpStatusCode from '../../../common/services/httpStatusCode';

describe('ErrorHandler', () => {
    it(`sould be shown message ${NetworkErrorMessage}`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(NetworkErrorMessage);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${ERRORS_MAP[USERNAME_EXISTS]}" if error message equal "${USERNAME_EXISTS}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 'statusCode',
                    code: USERNAME_EXISTS,
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(ERRORS_MAP[USERNAME_EXISTS]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${ERRORS_MAP[PASSWORD_NO_USER_INFO_ERROR]}" if error message equal "${PASSWORD_NO_USER_INFO_ERROR}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 'statusCode',
                    code: PASSWORD_NO_USER_INFO_ERROR,
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(ERRORS_MAP[PASSWORD_NO_USER_INFO_ERROR]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${ERRORS_MAP[PHONE_NUMBER_EXISTS]}" if error message equal "${PHONE_NUMBER_EXISTS}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 'statusCode',
                    code: PHONE_NUMBER_EXISTS,
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(ERRORS_MAP[PHONE_NUMBER_EXISTS]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${ERRORS_MAP[INVALID_POSTAL_CODE]}" if error message equal "${INVALID_POSTAL_CODE}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 'statusCode',
                    code: INVALID_POSTAL_CODE,
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(ERRORS_MAP[INVALID_POSTAL_CODE]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${ERRORS_MAP[TOO_MANY_ATTEMPTS]}" if error message equal "${TOO_MANY_ATTEMPTS}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 'statusCode',
                    code: TOO_MANY_ATTEMPTS,
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(ERRORS_MAP[TOO_MANY_ATTEMPTS]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${ERRORS_MAP[INVALID_USER_PASSWORD]}" if error message equal "${INVALID_USER_PASSWORD}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 'statusCode',
                    code: INVALID_USER_PASSWORD,
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(ERRORS_MAP[INVALID_USER_PASSWORD]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${
        defaultHttpStatusCodeErrorMap[HttpStatusCode.BAD_REQUEST]
    }" if error message not from auth0 and status code ${HttpStatusCode.BAD_REQUEST}`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: HttpStatusCode.BAD_REQUEST,
                    code: 'Unknown',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(defaultHttpStatusCodeErrorMap[HttpStatusCode.BAD_REQUEST]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${
        defaultHttpStatusCodeErrorMap[HttpStatusCode.NOT_FOUND]
    }" if error message not from auth0 and status code "${HttpStatusCode.NOT_FOUND}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: HttpStatusCode.NOT_FOUND,
                    code: 'Unknown',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(defaultHttpStatusCodeErrorMap[HttpStatusCode.NOT_FOUND]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${
        defaultHttpStatusCodeErrorMap[HttpStatusCode.INTERNAL_SERVER_ERROR]
    }" if error message not from auth0 and status code "${HttpStatusCode.INTERNAL_SERVER_ERROR}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
                    code: 'Unknown',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(defaultHttpStatusCodeErrorMap[HttpStatusCode.INTERNAL_SERVER_ERROR]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${
        defaultHttpStatusCodeErrorMap[HttpStatusCode.INTERNAL_SERVER_ERROR]
    }" if error message not from auth0 and status code "${HttpStatusCode.INTERNAL_SERVER_ERROR}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
                    code: 'Unknown',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(defaultHttpStatusCodeErrorMap[HttpStatusCode.INTERNAL_SERVER_ERROR]);

        useContextSpy.mockRestore();
    });

    it(`sould be shown message "${
        defaultHttpStatusCodeErrorMap[HttpStatusCode.SERVICE_UNAVAILABLE]
    }" if error message not from auth0 and status code "${HttpStatusCode.SERVICE_UNAVAILABLE}"`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: HttpStatusCode.SERVICE_UNAVAILABLE,
                    code: 'Unknown',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(defaultHttpStatusCodeErrorMap[HttpStatusCode.SERVICE_UNAVAILABLE]);

        useContextSpy.mockRestore();
    });

    it(`should be shown message 'Unknown Error'`, () => {
        const useContextSpy = jest.spyOn(React, 'useContext').mockImplementation(() => ({
            state: {
                error: {
                    type: 'test',
                    statusCode: 418,
                    code: 'Unknown',
                },
            },
            actions: {
                removeError: jest.fn(),
            },
        }));
        render(<ErrorHandler />);

        screen.getByText(/Unknown Error/i);

        useContextSpy.mockRestore();
    });
});
