import React from 'react';
import { axe } from 'jest-axe';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import PasswordReset from '../../../auth0/src/components/passwordReset';
import { auth } from '../../src/lib/auth';

describe('PasswordReset component', () => {
    it('should load and display PasswordReset component', () => {
        const { container } = render(<PasswordReset />);

        expect(container).toMatchSnapshot();
    });

    it('should have submit button disabled initially', async () => {
        render(<PasswordReset />);

        await waitFor(() => {
            expect(screen.getByRole('button', { name: 'Save' })).toHaveAttribute('disabled');
        });
    });

    it('should display two password inputs', () => {
        render(<PasswordReset />);

        screen.getByLabelText('New Password *');
        screen.getByLabelText('Retype New Password *');
    });

    it('should display "Password is incomplete" when nothing is typed in password field', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '');
        userEvent.click(screen.getByLabelText('Retype New Password *'));

        await waitFor(() => {
            screen.getByText(/Password is incomplete/);
        });
    });

    it('should display "Password must be at least 8 characters" when too few characters is typed in password field', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '12345');
        userEvent.click(screen.getByLabelText('Retype New Password *'));

        await waitFor(() => {
            screen.getByText(/Password must be at least 8 characters/i);
        });
    });

    it('should display "At least 1 uppercase must be entered" when there is no uppercase letter typed in password field', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789q');
        userEvent.click(screen.getByLabelText('Retype New Password *'));

        await waitFor(() => {
            screen.getByText(/At least 1 uppercase must be entered/i);
        });
    });

    it('should display "At least 1 lowercase must be entered" when there is no lowercase letter typed in password field', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Q');
        userEvent.click(screen.getByLabelText('Retype New Password *'));

        await waitFor(() => {
            screen.getByText(/At least 1 lowercase must be entered/i);
        });
    });

    it('should display "At least 1 symbol must be entered (e.g. /#$)" when there is no symbol typed in password field', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Qq');
        userEvent.click(screen.getByLabelText('Retype New Password *'));

        await waitFor(() => {
            screen.getByText('At least 1 symbol must be entered (e.g. /#$)');
        });
    });

    it('should display "Passwords do not match" when value from password and retype password fields do not match', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Qq!');
        userEvent.type(screen.getByLabelText('Retype New Password *'), '12345');
        userEvent.click(screen.getByLabelText('New Password *'));

        await waitFor(() => {
            screen.getByText(/Passwords do not match/i);
        });
    });

    it('should have submit button enabled when passwords meet complexity requirement and match', async () => {
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Qq!');
        userEvent.type(screen.getByLabelText('Retype New Password *'), '123456789Qq!');
        userEvent.click(screen.getByLabelText('New Password *'));

        await waitFor(() => {
            expect(screen.getByRole('button', { name: 'Save' })).not.toHaveAttribute('disabled');
        });
    });

    it('should call auth.updatePassword method on submit when all fields are valid', async () => {
        const updatePasswordMock = jest.fn();
        ((auth.updatePassword as unknown) as jest.Mock) = updatePasswordMock;
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Qq!');
        userEvent.type(screen.getByLabelText('Retype New Password *'), '123456789Qq!');

        userEvent.click(screen.getByRole('button', { name: 'Save' }));
        await waitFor(() =>
            expect(updatePasswordMock).toHaveBeenCalledWith({
                _csrf: '{{csrf_token}}',
                ticket: '{{ticket}}',
                email: '{{email}}',
                password: '123456789Qq!',
            })
        );
    });

    it('should display user success info block after successful password change', async () => {
        const updatePasswordMock = jest.fn().mockResolvedValue(null);
        ((auth.updatePassword as unknown) as jest.Mock) = updatePasswordMock;
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Qq!');
        userEvent.type(screen.getByLabelText('Retype New Password *'), '123456789Qq!');

        userEvent.click(screen.getByRole('button', { name: 'Save' }));
        await waitFor(() => {
            screen.getByRole('heading', { name: /TIME FOR AN UPGRADE/i });
            screen.getByRole('heading', { name: /PASSWORD SUCCESSFULLY CHANGED/i });
            screen.getByText(/You can now login to the application with the new password./i);
            screen.getByRole('button', { name: /CLOSE/i });
        });
    });

    it('should display link with http://www.test.app.url after successful password change', async () => {
        const updatePasswordMock = jest.fn().mockResolvedValue(null);
        ((auth.updatePassword as unknown) as jest.Mock) = updatePasswordMock;
        render(<PasswordReset />);

        userEvent.type(screen.getByLabelText('New Password *'), '123456789Qq!');
        userEvent.type(screen.getByLabelText('Retype New Password *'), '123456789Qq!');

        userEvent.click(screen.getByRole('button', { name: 'Save' }));
        await waitFor(() => {
            expect(screen.getByRole('link')).toHaveAttribute('href', 'http://www.test.app.url');
        });
    });

    it('a11y', async () => {
        const { container } = render(<PasswordReset />);

        const results = await axe(container);

        expect(results).toHaveNoViolations();
    });
});
