import React from 'react';
import { render, screen } from '@testing-library/react';
import { ERRORS_MAP } from '../../src/components/errorHandler/errors.constants';

import ErrorMessage from '../../../auth0/src/components/errorMessage';

describe('error message component', () => {
    it('should load and display message if error code is found in messageMap', () => {
        render(<ErrorMessage code="code" messageMap={{ code: 'MESSAGE' }} />);

        screen.getByText(/MESSAGE/i);
    });

    it('should load and display "Something went wrong. Please try again." if error code is not found in messageMap', () => {
        render(<ErrorMessage code="code" messageMap={{ different_code: 'MESSAGE' }} />);

        screen.getByText(
            /We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly./i
        );
    });

    it('should load and display "Something went wrong. Please try again." if messageMap is not provided and code is provided', () => {
        render(<ErrorMessage code="code" />);

        screen.getByText(
            /We apologize, our system is unavailable at the moment. We are working hard to resolve. Please check back shortly./i
        );
    });

    it('should display "Enter a valid postal code"', () => {
        render(<ErrorMessage code="invalid_postal_code" messageMap={ERRORS_MAP} />);

        screen.getByText(/Enter a valid postal code/i);
    });
});
