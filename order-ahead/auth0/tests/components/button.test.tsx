import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { InspireButton } from '../../src/components/button';

describe('InspireButton component', () => {
    it('should render simple button with link prop', () => {
        const { container } = render(<InspireButton text="text" link="/test" />);

        expect(container).toMatchSnapshot();
    });

    it('should render simple button with external link prop', () => {
        const { container } = render(<InspireButton text="text" link="http://test.com" />);

        expect(container).toMatchSnapshot();
    });

    it('should render simple button with phone prop', () => {
        const { container } = render(<InspireButton text="text" phone="888-999-111" />);

        expect(container).toMatchSnapshot();
    });

    it('should render simple button properly', () => {
        const { container } = render(<InspireButton text="text" />);

        expect(container).toMatchSnapshot();
    });

    it('should render simple button with full width', () => {
        const { container } = render(<InspireButton text="text" fullWidth />);

        expect(container).toMatchSnapshot();
    });

    it('should render button with children', () => {
        const { container } = render(<InspireButton text="text">children</InspireButton>);

        expect(container).toMatchSnapshot();
    });

    it('should call onClick after click on button', async () => {
        const onClickMock = jest.fn();
        render(<InspireButton onClick={onClickMock} />);

        userEvent.click(screen.getByRole('button'));

        await waitFor(() => expect(onClickMock).toHaveBeenCalledTimes(1));
    });

    it('should call onKeyUp after key up on button', async () => {
        const onKeyUpMock = jest.fn();
        render(<InspireButton onKeyUp={onKeyUpMock} />);

        userEvent.type(screen.getByRole('button'), '{enter}');

        await waitFor(() => expect(onKeyUpMock).toHaveBeenCalledTimes(1));
    });
});
