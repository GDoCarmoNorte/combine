import Auth, { auth, AuthError } from '../../../auth0/src/lib/auth';
import { advanceTo, clear } from 'jest-date-mock';
import { FetchMock } from 'jest-fetch-mock';
import { SignInTypes } from '../../../common/services/gtmService/types';
import logger from '../../../common/services/logger';
import { maskEmail } from '../../src/utils/string';
import {
    AUTH0_PASSWORD_RESET,
    AUTH0_FORGOT_PASSWORD,
    AUTH0_SIGN_IN,
    AUTH0_SIGN_UP,
} from '../../constants/loggerEvents';

describe('Auth Helper', () => {
    describe('getInitialPage', () => {
        it('should throw when invoked before initialization with auth0 for getInitialPage', async () => {
            const auth = new Auth();

            expect(() => auth.getInitialPage()).toThrowErrorMatchingInlineSnapshot(`"Auth0 SDK is not configured"`);
        });

        it('should return correct page after initialization with auth0', async () => {
            const auth = new Auth();

            const webAuthSDK = { baseOptions: { page: 'signup' } };

            auth.initAuth0(webAuthSDK);

            expect(auth.getInitialPage()).toBe('signup');
        });
    });

    describe('signUpAndLogin', () => {
        it('should throw when invoked before initialization with auth0 for signUpAndLogin', async () => {
            const auth = new Auth();
            const formDataMock = {
                email: '',
                firstName: '',
                lastName: '',
                password: '',
                phone: '',
                marketingOptIn: false,
            };

            await expect(auth.signUpAndLogin(formDataMock)).rejects.toThrowErrorMatchingInlineSnapshot(
                `"Auth0 SDK is not configured"`
            );
        });

        it('should throw with AuthError when webauth.redirect.signupAndLogin throws for any reason and should log error', async () => {
            const auth = new Auth();
            const webAuthSignupMock = jest.fn().mockImplementation((_, callback) => {
                callback({ code: 'error_code', description: 'error_description' });
            });
            const webAuthSDK = {
                redirect: { signupAndLogin: webAuthSignupMock },
            };
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const formDataMock = {
                email: '',
                firstName: '',
                lastName: '',
                password: '',
                phone: '',
                marketingOptIn: false,
            };

            auth.initAuth0(webAuthSDK);

            await expect(auth.signUpAndLogin(formDataMock)).rejects.toThrow(
                new AuthError('error_code', 'error_description')
            );
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_SIGN_UP, {
                isSuccess: false,
                request: {
                    email: formDataMock.email,
                    firstName: formDataMock.firstName,
                    lastName: formDataMock.lastName,
                    password: formDataMock.password,
                    phone: formDataMock.phone,
                },
                error: {
                    code: 'error_code',
                    description: 'error_description',
                },
            });
        });

        it('should pass firstName, lastName, password, email, birthDate, phone, marketingOptIn to auth0 when creating user and should call logger with  specific params', async () => {
            advanceTo(new Date(2020, 1, 1));
            const auth = new Auth();
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const webAuthSignupMock = jest.fn().mockImplementation((_, callback) => {
                callback();
            });
            const webAuthSDK = {
                redirect: { signupAndLogin: webAuthSignupMock },
            };

            const formDataMock = {
                email: 'email',
                firstName: 'firstName',
                lastName: 'lastName',
                password: 'password',
                phone: 'phone',
                birthDate: 'birthDate',
                marketingOptIn: true,
            };

            auth.initAuth0(webAuthSDK);
            await auth.signUpAndLogin(formDataMock);

            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_SIGN_UP, { isSuccess: true });
            expect(webAuthSignupMock).toHaveBeenCalledWith(
                {
                    connection: 'firebase-auth',
                    email: 'email',
                    password: 'password',
                    given_name: 'firstName',
                    family_name: 'lastName',
                    user_metadata: {
                        birthDate: 'birthDate',
                        phone: 'phone',
                        termsLink: 'http://www.test.app.url/terms-of-use',
                        termsAcceptedAt: new Date().toISOString(),
                        marketingOptIn: 'on',
                    },
                },
                expect.anything()
            );

            clear();
        });
    });

    describe('login', () => {
        const push = jest.fn();
        beforeEach(() => {
            (window as any).dataLayer = { push };
            push.mockReset();
        });

        it('should throw when invoked before initialization with auth0 sdk for login', async () => {
            const auth = new Auth();
            const formDataMock = {
                email: '',
                password: '',
            };

            await expect(auth.login(formDataMock)).rejects.toThrowErrorMatchingInlineSnapshot(
                `"Auth0 SDK is not configured"`
            );
        });
        // TODO get better solution for Google Analytics
        it('should throw with AuthError when webauth.login throws for any reason and should call logger', async () => {
            const auth = new Auth();
            const webAuthLoginMock = jest.fn().mockImplementation((_, callback) => {
                callback({ code: 'error_code', description: 'error_description' });
            });
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const webAuthSDK = {
                login: webAuthLoginMock,
            };
            const formDataMock = {
                email: '',
                password: '',
            };

            auth.initAuth0(webAuthSDK);

            await expect(auth.login(formDataMock)).rejects.toThrow(new AuthError('error_code', 'error_description'));
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_SIGN_IN, {
                isSuccess: false,
                error: {
                    code: 'error_code',
                    description: 'error_description',
                },
                request: {
                    email: '',
                },
            });
            expect(push).toHaveBeenCalledWith({ event: 'signInFailure', signInType: 'email' });
        });

        it('should pass provided email and password to auth0 login method and call logger.LogEvent with specified prams', async () => {
            const auth = new Auth();
            const webAuthLoginMock = jest.fn().mockImplementation((options, callback) => {
                options.onRedirecting();
                callback();
            });
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const webAuthSDK = {
                login: webAuthLoginMock,
            };

            const formDataMock = {
                email: 'email',
                password: 'password',
            };

            auth.initAuth0(webAuthSDK);
            auth.login(formDataMock);

            expect(webAuthLoginMock).toHaveBeenCalledWith(
                {
                    email: 'email',
                    password: 'password',
                    realm: 'firebase-auth',
                    onRedirecting: expect.anything(),
                },
                expect.anything()
            );
            expect(push).toHaveBeenCalledWith({ event: 'signInSuccess', signInType: 'email' });
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_SIGN_IN, { isSuccess: true });
        });
    });

    describe('sendPasswordResetEmail', () => {
        it('should throw when invoked before initialization with auth0 sdk for sendPasswordResetEmail', async () => {
            const auth = new Auth();
            const params = {
                email: '',
            };

            await expect(auth.sendPasswordResetEmail(params)).rejects.toThrowErrorMatchingInlineSnapshot(
                `"Auth0 SDK is not configured"`
            );
        });

        it('should throw with AuthError when webauth.changePassword throws for any reason and call logger', async () => {
            const auth = new Auth();
            const changePasswordMock = jest.fn().mockImplementation((_, callback) => {
                callback({ code: 'error_code', description: 'error_description' });
            });
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const webAuthSDK = {
                changePassword: changePasswordMock,
            };
            const params = {
                email: '',
            };

            auth.initAuth0(webAuthSDK);

            await expect(auth.sendPasswordResetEmail(params)).rejects.toThrow(
                new AuthError('error_code', 'error_description')
            );
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_FORGOT_PASSWORD, {
                isSuccess: false,
                request: { email: maskEmail(params.email) },
                error: {
                    code: 'error_code',
                    description: 'error_description',
                },
            });
        });

        it('should pass provided email to auth0 changePassword method and call logger', async () => {
            const auth = new Auth();
            const changePasswordMock = jest.fn().mockImplementation((_, callback) => {
                callback();
            });
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const webAuthSDK = {
                changePassword: changePasswordMock,
            };

            const params = {
                email: 'email',
            };

            auth.initAuth0(webAuthSDK);
            await auth.sendPasswordResetEmail(params);

            expect(changePasswordMock).toHaveBeenCalledWith(
                {
                    email: 'email',
                    connection: 'firebase-auth',
                },
                expect.anything()
            );
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_FORGOT_PASSWORD, { isSuccess: true });
        });
    });

    describe('updatePassword', () => {
        it('should use window.location.origin, application/x-www-form-urlencoded Content-Type and POST method to call API', () => {
            const body = {
                _csrf: '_csrf',
                email: 'email',
                ticket: 'ticket',
                password: 'password',
            };

            auth.updatePassword(body);

            const origin = window.location.origin;

            expect(fetch).toBeCalledWith(
                `${origin}/lo/reset`,
                expect.objectContaining({
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                    method: 'POST',
                })
            );
        });

        it('should throw with AuthError when fetch does not return ok and call logger', async () => {
            const body = {
                _csrf: '_csrf',
                email: 'email',
                ticket: 'ticket',
                password: 'password',
            };
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;

            (fetch as FetchMock).mockResponseOnce('', { status: 404 });

            await expect(auth.updatePassword(body)).rejects.toThrow(new AuthError('unexpected_error', 'Error'));
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_PASSWORD_RESET, {
                isSuccess: false,
                request: { email: maskEmail(body.email) },
                error: {
                    code: 'unexpected_error',
                    description: 'error_description',
                },
            });
        });

        it('should call logger on success request', async () => {
            const loggerMock = jest.fn();
            ((logger.logEvent as unknown) as jest.Mock) = loggerMock;
            const body = {
                _csrf: '_csrf',
                email: 'email',
                ticket: 'ticket',
                password: 'password',
            };

            (fetch as FetchMock).mockResponseOnce('', { status: 200 });
            await auth.updatePassword(body);
            expect(logger.logEvent).toHaveBeenCalledWith(AUTH0_PASSWORD_RESET, { isSuccess: true });
        });
    });

    describe('AuthError class', () => {
        it('should set "code" and "message" fields', () => {
            const authError = new AuthError('someCode', 'someMessage');
            expect(authError.code).toEqual('someCode');
            expect(authError.message).toEqual('someMessage');
        });
    });

    describe('loginWithSocial', () => {
        it('should throw when invoked before initialization with auth0s', async () => {
            const auth = new Auth();
            const signInTypeMock = 'wrong provider' as SignInTypes;

            await expect(auth.loginWithSocial(signInTypeMock)).rejects.toThrowErrorMatchingInlineSnapshot(
                `"Auth0 SDK is not configured"`
            );
        });
        it('should throw with AuthError when webauth.redirect.signupAndLogin throws for any reason', async () => {
            const auth = new Auth();
            const webAuthSignInMock = jest.fn().mockImplementation((_, callback) => {
                callback({ code: 'error_code', description: 'error_description' });
            });
            const signInTypeMock = 'wrong provider' as SignInTypes;

            const webAuthSDK = { authorize: webAuthSignInMock };

            auth.initAuth0(webAuthSDK);

            await expect(auth.loginWithSocial(signInTypeMock)).rejects.toThrow(
                new AuthError('error_code', 'error_description')
            );
        });

        it('should pass facebook to auth0 when signing in', async () => {
            const auth = new Auth();
            const webAuthSignInMock = jest.fn().mockImplementation((_, callback) => {
                callback();
            });
            const webAuthSDK = { authorize: webAuthSignInMock };

            auth.initAuth0(webAuthSDK);
            await auth.loginWithSocial(SignInTypes.FACEBOOK);

            expect(webAuthSignInMock).toHaveBeenCalledWith(
                {
                    connection: SignInTypes.FACEBOOK,
                },
                expect.anything()
            );

            clear();
        });
        it('should pass apple to auth0 when signing in', async () => {
            const auth = new Auth();
            const webAuthSignInMock = jest.fn().mockImplementation((_, callback) => {
                callback();
            });
            const webAuthSDK = { authorize: webAuthSignInMock };

            auth.initAuth0(webAuthSDK);
            await auth.loginWithSocial(SignInTypes.APPLE);

            expect(webAuthSignInMock).toHaveBeenCalledWith(
                {
                    connection: SignInTypes.APPLE,
                },
                expect.anything()
            );

            clear();
        });
    });

    describe('isWeb', () => {
        it('should return true when NEXT_PUBLIC_AUTH_0_CLIENT_ID equals clientID from webAuth options', () => {
            const auth = new Auth();
            const webAuthSDK = { baseOptions: { clientID: 'AUTH0CLIENTID' } };

            auth.initAuth0(webAuthSDK);

            expect(auth.isWeb()).toEqual(true);
        });

        it("should return true when webAuth options don't initialize", () => {
            const auth = new Auth();

            expect(auth.isWeb()).toEqual(true);
        });

        it('should return false when NEXT_PUBLIC_AUTH_0_CLIENT_ID is not equal to clientID from webAuth options', () => {
            const auth = new Auth();
            const webAuthSDK = { baseOptions: { clientID: '___AUTH0CLIENTID' } };

            auth.initAuth0(webAuthSDK);

            expect(auth.isWeb()).toEqual(false);
        });
    });
});
