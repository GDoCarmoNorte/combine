import React from 'react';
import ReactDOM from 'react-dom';
import PasswordReset from '../../../src/pages/passwordReset/passwordReset';

jest.mock('react-dom', () => ({ render: jest.fn() }));

describe('Application container for PasswordReset', () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        div.id = 'container';
        document.body.appendChild(div);
        require('../../../src/pages/passwordReset/app');
        expect(ReactDOM.render).toHaveBeenCalledWith(<PasswordReset />, div);
    });
});
