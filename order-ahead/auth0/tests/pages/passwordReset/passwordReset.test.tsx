import React from 'react';
import { render, screen } from '@testing-library/react';

import PasswordReset from '../../../src/pages/passwordReset/passwordReset';
import { axe } from 'jest-axe';

jest.mock('../../../src/lib/auth');

describe('app component', () => {
    it('should render PasswordReset with password input fields', () => {
        render(<PasswordReset />);

        screen.getByRole('heading', { name: /TIME FOR AN UPGRADE/i });
        screen.getByRole('heading', { name: /CHANGE PASSWORD/i });
        screen.getByLabelText('New Password *');
        screen.getByLabelText('Retype New Password *');
        screen.getByRole('button', { name: /Save/i });
    });

    // TODO: fix a11y
    it.skip('a11y', async () => {
        const { container } = render(<PasswordReset />);

        const results = await axe(container);

        expect(results).toHaveNoViolations();
    });
});
