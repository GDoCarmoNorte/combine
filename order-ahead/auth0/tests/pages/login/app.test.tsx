import React from 'react';
import ReactDOM from 'react-dom';
import Login from '../../../src/components/login/login';

jest.mock('react-dom', () => ({ render: jest.fn() }));

describe('Application container for Login', () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        div.id = 'container';
        document.body.appendChild(div);
        require('../../../src/pages/login/app');
        expect(ReactDOM.render).toHaveBeenCalledWith(<Login />, div);
    });
});
