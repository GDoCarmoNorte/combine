import { maskString, maskEmail, maskPassword, maskPhone } from '../../src/utils/string';

describe('Utils', () => {
    describe('maskString', () => {
        it('should return expected masked string', () => {
            const testCases = [
                { input: '', expect: '' },
                { input: 'J', expect: 'x' },
                { input: 'Ju', expect: 'Jx' },
                { input: 'Jun', expect: 'Jxn' },
                { input: 'June', expect: 'Jxxe' },
                { input: 'Michael', expect: 'Mxxxxxl' },
            ];

            testCases.map((testCase) => expect(maskString(testCase.input)).toEqual(testCase.expect));
        });
    });

    describe('maskEmail', () => {
        it('should return expected masked string for valid email addresses', () => {
            const testCases = [
                { input: '', expect: '' },
                { input: 'J@a.com', expect: 'x@x.com' },
                { input: 'Ju@a.com', expect: 'Jx@x.com' },
                { input: 'Jun@a.com', expect: 'Jxn@x.com' },
                { input: 'June@a.com', expect: 'Jxxe@x.com' },
                { input: 'J@ab.com', expect: 'x@ax.com' },
                { input: 'Ju@ab.com', expect: 'Jx@ax.com' },
                { input: 'Jun@ab.com', expect: 'Jxn@ax.com' },
                { input: 'June@ab.com', expect: 'Jxxe@ax.com' },
                { input: 'J@abc.com', expect: 'x@axc.com' },
                { input: 'Ju@abc.com', expect: 'Jx@axc.com' },
                { input: 'Jun@abc.com', expect: 'Jxn@axc.com' },
                { input: 'June@abc.com', expect: 'Jxxe@axc.com' },
                { input: 'michael@somedomain.com', expect: 'mxxxxxl@sxxxxxxxxn.com' },
            ];

            testCases.map((testCase) => expect(maskEmail(testCase.input)).toEqual(testCase.expect));
        });

        it('should return expected masked string for invalid email addresses', () => {
            const testCases = [
                { input: 'incompletedemail', expect: 'ixxxxxxxxxxxxxxl' },
                { input: 'incompleted@email', expect: 'ixxxxxxxxxxxxxxxl' },
                { input: 'incompleted.email', expect: 'ixxxxxxxxxxxxxxxl' },
                { input: 'incompleted@.email', expect: 'ixxxxxxxxxxxxxxxxl' },
            ];

            testCases.map((testCase) => expect(maskEmail(testCase.input)).toEqual(testCase.expect));
        });
    });

    describe('maskPassword', () => {
        it('should return expected masked string for password value', () => {
            const testCases = [
                { input: '', expect: '' },
                { input: 'q', expect: '*' },
                { input: 'qw', expect: '**' },
                { input: 'qwe', expect: '***' },
                { input: 'qwer', expect: '****' },
                { input: 'qwert', expect: '*****' },
                { input: 'qwerty', expect: '******' },
                { input: '*****', expect: '*****' },
            ];

            testCases.map((testCase) => expect(maskPassword(testCase.input)).toEqual(testCase.expect));
        });
    });

    describe('maskPhone', () => {
        it('should return expected masked string for phone value', () => {
            const testCases = [
                { input: '', expect: '' },
                { input: '2148881111', expect: '214xxxxx11' },
                { input: '214 888 1111', expect: '214 xxx xx11' },
                { input: '214-888-1111', expect: '214-xxx-xx11' },
            ];

            testCases.map((testCase) => expect(maskPhone(testCase.input)).toEqual(testCase.expect));
        });
    });
});
