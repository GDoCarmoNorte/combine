import {
    GO_TO_SIGN_IN,
    GO_TO_SIGN_UP,
    IState,
    reducer,
    UPDATE_EMAIL,
    SET_ERROR,
    REMOVE_ERROR,
    GO_TO_FORGOT_PASSWORD,
} from '../../../auth0/src/store';

const initialState: IState = {
    screen: undefined,
    email: '',
    error: {},
};

describe('auth0 store reducer', () => {
    it('should return state if action is null', () => {
        const state: IState = {
            ...initialState,
            screen: 'signup',
        };

        const action: any = {};

        expect(reducer(state, action)).toMatchObject({
            screen: 'signup',
        });
    });

    it('should set active page to SIGNUP on GO_TO_SIGN_UP action', () => {
        const state: IState = {
            ...initialState,
            screen: 'signin',
        };

        const action: GO_TO_SIGN_UP = { type: 'GO_TO_SIGN_UP' };

        expect(reducer(state, action)).toMatchObject({
            screen: 'signup',
        });
    });

    it('should set active page to SIGNIN on GO_TO_SIGN_IN action', () => {
        const state: IState = {
            ...initialState,
            screen: 'signup',
        };

        const action: GO_TO_SIGN_IN = { type: 'GO_TO_SIGN_IN' };

        expect(reducer(state, action)).toMatchObject({
            screen: 'signin',
        });
    });

    it('should set email based on payload on UPDATE_EMAIL action', () => {
        const state: IState = {
            ...initialState,
        };

        const action: UPDATE_EMAIL = { type: 'UPDATE_EMAIL', payload: 'email' };

        expect(reducer(state, action)).toMatchObject({
            email: 'email',
        });
    });

    it('should call on SET_ERROR action', () => {
        const state: IState = {
            ...initialState,
        };

        const action: SET_ERROR = { type: 'SET_ERROR', payload: { message: 'Error' } };

        expect(reducer(state, action)).toMatchObject({
            error: { message: 'Error' },
        });
    });

    it('should call GO_TO_FORGOT_PASSWORD action', () => {
        const state: IState = {
            ...initialState,
        };

        const action: GO_TO_FORGOT_PASSWORD = { type: 'GO_TO_FORGOT_PASSWORD' };

        expect(reducer(state, action)).toMatchObject({
            screen: 'forgotpassword',
        });
    });

    it('should call REMOVE_ERROR action', () => {
        const state: IState = {
            ...initialState,
        };

        const action: REMOVE_ERROR = { type: 'REMOVE_ERROR' };

        expect(reducer(state, action)).toMatchObject({
            error: {},
        });
    });
});
