import React from 'react';
import { render } from '@testing-library/react';

import Store from '../../../auth0/src/store';

describe('app component', () => {
    it('should render app', () => {
        const { container } = render(
            <Store>
                <div>123</div>
                <div>123</div>
            </Store>
        );

        expect(container).toMatchSnapshot();
    });
});
