import { createSlice, PayloadAction, SliceCaseReducers } from '@reduxjs/toolkit';

import { OrderRequest, OrderResponseModel } from '../@generated/webExpApi';

const SUBMIT_ORDER = 'order/submit';

export interface ISubmitOrderState {
    lastOrder: OrderResponseModel;
    lastRequest: OrderRequest;
    isLoading: boolean;
    error: Error;
    unavailableItems: string[];
    isShowAlertModal: boolean;
}

export interface IFullfilledOrderPayload {
    response: OrderResponseModel;
    request: OrderRequest;
}

export const initialState = {
    lastOrder: null,
    lastRequest: null,
    isLoading: false,
    error: null,
    unavailableItems: null,
    isShowAlertModal: false,
};

const submitOrderSlice = createSlice<ISubmitOrderState, SliceCaseReducers<ISubmitOrderState>>({
    name: SUBMIT_ORDER,
    initialState,
    reducers: {
        pending: (state: ISubmitOrderState): ISubmitOrderState => ({
            ...state,
            isLoading: true,
        }),
        hideAlertModal: (state: ISubmitOrderState): ISubmitOrderState => ({
            ...state,
            isShowAlertModal: false,
        }),
        rejected: (_: ISubmitOrderState, action: PayloadAction<Error>): ISubmitOrderState => ({
            error: action.payload,
            isLoading: false,
            lastOrder: null,
            lastRequest: null,
            unavailableItems: null,
            isShowAlertModal: false,
        }),
        fulfilled: (_: ISubmitOrderState, action: PayloadAction<IFullfilledOrderPayload>): ISubmitOrderState => ({
            error: null,
            isLoading: false,
            lastOrder: action.payload.response,
            lastRequest: action.payload.request,
            unavailableItems: null,
            isShowAlertModal: false,
        }),
        setUnavailableItems: (_: ISubmitOrderState, action: PayloadAction<string[]>): ISubmitOrderState => ({
            error: null,
            isLoading: false,
            lastOrder: null,
            lastRequest: null,
            unavailableItems: action.payload,
            isShowAlertModal: true,
        }),

        reset: (state: ISubmitOrderState): ISubmitOrderState => ({
            ...initialState,
            isShowAlertModal: state.isShowAlertModal,
            unavailableItems: state.unavailableItems,
        }),
    },
});

export const { actions } = submitOrderSlice;

export default submitOrderSlice.reducer;
