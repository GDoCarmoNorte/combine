import { RootState } from '../store';

export const selectPointsBalance = (state: RootState): number => state.loyalty.pointsBalance;
