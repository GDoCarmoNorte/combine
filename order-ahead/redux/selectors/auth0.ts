import { RootState } from '../store';

export const selectAuth0 = (state: RootState): RootState['auth0'] => state.auth0;
