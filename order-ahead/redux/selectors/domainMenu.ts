import { createSelector, Dictionary } from '@reduxjs/toolkit';

import {
    ItemModel,
    ItemModifierModel,
    ItemGroupModel,
    ItemModifierGroupModel,
    SizeGroupModel,
    CategoryModel,
    IProductGroupModel,
    IOrderProductModel,
} from '../../@generated/webExpApi';

import {
    getCaloriesFromProduct,
    getCheapestPriceFromProduct,
    getModifierPrice,
    getProductItemsFromMenu,
    getProductItemsWithRootIdFromMenu,
    isModifierVisible,
} from '../../lib/domainProduct';
import {
    ISelectedModifier,
    IDefaultModifier,
    ISizeSelection,
    IDomainProductItem,
    IDisplayProduct,
    ISelectedSubModifier,
} from '../types';

import { PDPTallyItem, PDPTallyItemModifier, PDPTallyItemModifierGroup } from '../pdp';
import { RootState } from '../store';
import { getEmptyTallyItem } from '../../lib/tallyItem';
import { selectCurrenOrderLocation } from './orderLocation';
import getBrandInfo from '../../lib/brandInfo';
import { NO_SAUCE_CATEGORY_ID, ON_SIDE_SIZE_GROUP_ID, RECOMMENDED_CATEGORY_ID } from '../../common/constants/product';
import { getUnavailableDomainMenuCategories } from '../../common/helpers/getUnavailableDomainMenuCategories';
import GROUPS_WITH_FREE_MODIFIERS from '../../common/constants/groupsWithFreeModifiers';

// all item groups that are grouped NOT by size
export const otherItemGroupingNames = ['Protein', 'Flavor'];

const selectDomainMenu = (state: RootState) => state.domainMenu;

export const selectUnavailableCategories = (state: RootState, orderTime: string, timeZone: string): string[] => {
    const domainMenu = selectDomainMenu(state);
    if (!domainMenu.payload) {
        return [];
    }
    return getUnavailableDomainMenuCategories(domainMenu.payload.categories, orderTime, timeZone);
};

export const selectLoading = createSelector(selectDomainMenu, (state) => state.loading);

export const selectError = createSelector(selectDomainMenu, (state) => state.error);

export const selectRootProducts = createSelector(selectDomainMenu, (state) => state?.payload?.products);

export type IProducts = { [key: string]: IDomainProductItem };
export type IProductsWithRootProductId = { [key: string]: ItemModelRootProductId };

export interface ItemModelRootProductId extends ItemModel {
    rootProductId: string;
    menuItemName: string;
    isSaleable: boolean;
}

export const selectProducts = createSelector(
    selectDomainMenu,
    (state): IProducts => {
        if (!state.payload) {
            return {};
        }

        return getProductItemsFromMenu(state.payload);
    }
);

// TODO consider moving to web-exp-api
export const selectProductsWithRootProductId = createSelector(
    selectDomainMenu,
    (state): IProductsWithRootProductId => {
        if (!state.payload) {
            return {};
        }

        return getProductItemsWithRootIdFromMenu(state.payload);
    }
);

export const selectProductById = (state: RootState, id: string | null): IDomainProductItem | null => {
    if (!id) return null;
    const products = selectProducts(state);

    return products[id];
};

export const selectProductWithRootIdById = (state: RootState, id: string): ItemModelRootProductId => {
    const products = selectProductsWithRootProductId(state);

    return products[id];
};

export const selectProductsByIds = (state: RootState, ids: string[]): Dictionary<IDomainProductItem> => {
    const products = selectProducts(state);
    if (!products) {
        return {};
    }
    return ids.reduce((acc, item) => {
        return {
            ...acc,
            [item]: products[item],
        };
    }, {});
};

export const selectProductByIds = (state: RootState, ids: string[]): IDomainProductItem | null => {
    const products = selectProducts(state);

    for (const id of ids) {
        if (products[id]) {
            return products[id];
        }
    }

    return null;
};

export const selectProductByIdsWithRootId = (state: RootState, ids: string[]): ItemModelRootProductId | null => {
    const products = selectProductsWithRootProductId(state);

    for (const id of ids) {
        if (products[id]) {
            return products[id];
        }
    }
    return null;
};

export const selectNoSauceDomainProduct = (
    state: RootState,
    modifiers: IDisplayProduct[]
): IDomainProductItem | undefined => {
    const domainProducts = selectProductsByIds(
        state,
        modifiers.map((item) => item.displayProductDetails.productId)
    );

    return Object.values(domainProducts).find((item) =>
        item.categoryIds?.some((categoryId) => categoryId === NO_SAUCE_CATEGORY_ID)
    );
};

export const selectNoItemProductIds = (state: RootState, modifiersIds: string[]): string[] => {
    const domainProducts = selectProductsByIds(state, modifiersIds);

    return Object.values(domainProducts)
        .filter((item) => item?.categoryIds?.some((categoryId) => categoryId === NO_SAUCE_CATEGORY_ID))
        .map((i) => i.id);
};

export const selectProductGroups = createSelector(selectDomainMenu, (menu) => menu.payload?.productGroups || {});

export const selectProductGroupById = (state: RootState, productGroupId: string): IProductGroupModel => {
    const productGroups = selectProductGroups(state);
    return productGroups[productGroupId];
};

export const selectProductGroupByName = (state: RootState, productGroupName: string): IProductGroupModel => {
    const productGroups = selectProductGroups(state);
    return Object.values(productGroups).find((productGroup) => productGroup.name === productGroupName);
};

export const selectProductGroup = createSelector(
    selectProductById,
    selectProductGroups,
    (product, productGroups): IProductGroupModel => {
        return productGroups[product?.productGroupId];
    }
);

export const selectProductGroupByProductId = createSelector(
    selectProductById,
    selectProductGroups,
    (product, productGroups) => {
        return productGroups[product?.productGroupId];
    }
);

export const selectModifierGroupsByProductId = createSelector(
    selectProductById,
    (product): ItemModifierGroupModel[] => {
        return product?.itemModifierGroups || [];
    }
);

export const selectItemGroups = createSelector(selectDomainMenu, (menu) => menu.payload?.itemGroups || {});

const selectSizeGroups = createSelector(selectDomainMenu, (menu) => menu.payload?.sizeGroups || {});

export const selectItemGroup = createSelector(
    selectProductById,
    selectItemGroups,
    (product, groups): ItemGroupModel => {
        return groups[product?.itemGroupId];
    }
);

export const selectSizeGroup = createSelector(
    selectProductById,
    selectSizeGroups,
    (product, groups): SizeGroupModel => {
        return groups[product?.sizeGroupId];
    }
);

export const selectCategories = createSelector(selectDomainMenu, (menu) => menu.payload?.categories || {});

export const selectCategoriesList = createSelector(selectCategories, (categories): CategoryModel[] =>
    categories ? Object.values(categories) : []
);

export const selectCategoriesByProductId = createSelector(
    selectProductById,
    selectCategoriesList,
    (product, categories): CategoryModel[] => {
        const categoryArray = categories.filter((category) => product?.categoryIds?.includes(category.id));
        return categoryArray;
    }
);

export const selectTopCategory = createSelector(selectCategoriesByProductId, (categories) => {
    categories.sort((firstCategory, secondCategory) => firstCategory?.sequence - secondCategory?.sequence);
    return categories?.[0];
});

export const selectProductIsCombo = createSelector(selectProductGroupByProductId, (productGroup) => {
    return productGroup?.isCombo;
});

export const selectProductIsPromo = createSelector(selectProductGroupByProductId, (productGroup) => {
    return productGroup?.isPromo;
});

export const selectProductList = createSelector(selectProducts, (products): ItemModel[] =>
    products ? Object.values(products) : []
);

const selectInitialIsSauceOnSide = (state: RootState, productId: string, modifierGroupId: string): boolean => {
    const group = selectItemModifierGroup(state, productId, modifierGroupId);

    const itemModifiers = Object.values(group?.itemModifiers || {});

    return itemModifiers.some((it) => {
        const isSauceOnSide = selectBwwIsModifierOnSide(state, it.itemId);
        if (!isSauceOnSide) {
            return false;
        }

        return it.defaultQuantity > 0;
    });
};

export const selectDefaultTallyModifierGroups = (state: RootState, productId: string) => {
    const products = selectProducts(state);
    const itemModifierGroups = products[productId]?.itemModifierGroups;

    const { brandId } = getBrandInfo();
    const hasDefaultQuantity = (item: ItemModifierModel) => item.defaultQuantity > 0;

    // TODO add a way to toggle files other than components and have a two separate functions
    if (brandId === 'Bww') {
        return itemModifierGroups?.map((it) => {
            return {
                productId: it?.productGroupId,
                metadata: it?.metadata,
                isOnSideChecked: selectInitialIsSauceOnSide(state, productId, it.productGroupId),
                modifiers: Object.values(it.itemModifiers || [])
                    ?.filter(hasDefaultQuantity)
                    ?.map((modifier) => {
                        const productItemModifier = products[modifier?.itemId];
                        const price = getModifierPrice(modifier, productItemModifier);

                        return {
                            productId: modifier?.itemId,
                            price,
                            quantity: modifier?.defaultQuantity,
                            modifierGroups: selectDefaultTallyModifierGroups(state, modifier.itemId),
                        };
                    }),
            };
        });
    }

    return itemModifierGroups?.map((it) => {
        return {
            productId: it?.productGroupId, // modifier productId is productGroupId of modifiers (OMC name convension)
            metadata: it?.metadata,
            modifiers: Object.values(it.itemModifiers || [])
                ?.filter(hasDefaultQuantity)
                ?.map((modifier) => {
                    const productItemModifier = products[modifier?.itemId];
                    const price = getModifierPrice(modifier, productItemModifier);

                    return {
                        productId: modifier?.itemId,
                        price,
                        quantity: modifier?.defaultQuantity,
                    };
                }),
        };
    });
};

export const selectHistoryTallyModifierGroups = (
    state: RootState,
    historyProducts: IOrderProductModel[]
): PDPTallyItemModifierGroup[][] => {
    const modifierGroups = historyProducts.reduce((acc, histProduct) => {
        const filterModifiers = (item) => {
            return histProduct.modifiers.find((id) => id.id === item.itemId);
        };
        const modifierGroupsFromProductChildren = histProduct.children[0]?.modifierGroups;
        const domainProduct = selectProductById(state, histProduct.id);
        if (domainProduct?.itemModifierGroups) {
            const itemModifierGroups = domainProduct?.itemModifierGroups;
            const itemModifierGroup = itemModifierGroups?.map((it) => {
                const modifierGroupFromProductChildren = modifierGroupsFromProductChildren?.find(
                    (group) => group.productId === it.productGroupId
                );

                return {
                    productId: it.productGroupId,
                    metadata: it.metadata,
                    isOnSideChecked: selectInitialIsSauceOnSide(state, histProduct.id, it.productGroupId),
                    modifiers: Object.values(it.itemModifiers || [])
                        ?.filter(filterModifiers)
                        ?.map((modifier) => {
                            const productItemModifier = domainProduct[modifier?.itemId];
                            const price = getModifierPrice(modifier, productItemModifier);
                            const subModifierGroups = modifierGroupFromProductChildren?.modifiers?.find(
                                (m) => m.productId === modifier.itemId
                            )?.modifierGroups;
                            return {
                                productId: modifier?.itemId,
                                price: price,
                                quantity: histProduct.modifiers.find((id) => id.id === modifier.itemId).quantity,
                                ...(subModifierGroups && {
                                    modifierGroups: subModifierGroups.map((group) => ({
                                        ...group,
                                        isOnSideChecked: selectInitialIsSauceOnSide(
                                            state,
                                            modifier.itemId,
                                            group.productId
                                        ),
                                        modifiers:
                                            group.modifiers?.map(({ productId, quantity, price }) => ({
                                                productId,
                                                quantity,
                                                price,
                                            })) || [],
                                    })),
                                }),
                            };
                        }),
                };
            });
            return [...acc, itemModifierGroup];
        }
        return acc;
    }, []);

    return modifierGroups;
};

export const selectDefaultTallyItemForChildItem = (
    state: RootState,
    comboId: string,
    childItemId: string
): PDPTallyItem => {
    const itemModifier = selectItemModifierFromProduct(state, comboId, childItemId);
    const childProduct = selectProductById(state, childItemId);

    return {
        productId: childProduct.id,
        price: getModifierPrice(itemModifier, childProduct),
        quantity: 1,
        modifierGroups: selectDefaultTallyModifierGroups(state, childProduct.id),
        name: childProduct.name,
    };
};

const getDefaultСhild = (group: ItemModifierGroupModel) => {
    const { defaultItemModifierId } = group;

    if (group[defaultItemModifierId]) {
        return group[defaultItemModifierId];
    }

    // hack for situation when no defaultQuantity =1 case data issue
    const modifiersArray = Object.values(group.itemModifiers);

    return modifiersArray.find((item) => item.defaultQuantity === 1) || modifiersArray[0];
};

export const selectDefaultTallyItem = (state: RootState, productId: string): PDPTallyItem => {
    const product = selectProductById(state, productId);
    const location = selectCurrenOrderLocation(state);
    if (!product) {
        return getEmptyTallyItem();
    }
    const isMeal = selectProductIsCombo(state, productId);
    const isPromo = selectProductIsPromo(state, productId);

    if (isMeal || isPromo) {
        const childItems = product?.itemModifierGroups?.map((itemModifierGroup) => {
            const defaultItemModifier = getDefaultСhild(itemModifierGroup);
            return selectDefaultTallyItemForChildItem(state, product.id, defaultItemModifier.itemId);
        });

        return {
            name: product.name,
            productId: product.id,
            price: getCheapestPriceFromProduct(product, location),
            quantity: 1,
            childItems,
            childExtras: [],
        };
    }

    return {
        name: product.name,
        quantity: 1,
        productId: product?.id,
        price: getCheapestPriceFromProduct(product, location),
        modifierGroups: selectDefaultTallyModifierGroups(state, productId),
        childExtras: [],
    };
};

export const selectProductDiscountPriceAndCalories = createSelector(
    selectCurrenOrderLocation,
    selectProductById,
    (location, product) => {
        const price = getCheapestPriceFromProduct(product, location);
        const calories = getCaloriesFromProduct(product);

        return {
            price,
            calories,
        };
    }
);

export const selectIsProductEditable = createSelector(selectProductById, (product): boolean => {
    const hasEditable = product?.itemModifierGroups?.some((group) => {
        return Object.values(group.itemModifiers || {}).find(isModifierVisible);
    });

    return hasEditable || false;
});

export const selectProductSize = createSelector(selectProductById, selectSizeGroups, (product, sizeGroups) => {
    return sizeGroups[product?.sizeGroupId]?.name || '';
});

export const selectRootProductByItemId = createSelector(
    selectProductWithRootIdById,
    selectRootProducts,
    (product, rootProducts) => {
        return rootProducts?.[product?.rootProductId];
    }
);

export const selectProductNutrition = createSelector(selectProductById, (product) => {
    return product?.nutrition;
});

export const selectComboMainProduct = (state: RootState, id: string): ItemModel => {
    const isCombo = selectProductIsCombo(state, id);
    const isPromo = selectProductIsPromo(state, id);
    if (!isCombo && !isPromo) {
        return null;
    }
    const product = selectProductById(state, id);

    const mainGroup = product.itemModifierGroups?.find((it) => it.sequence === 1);
    const defaultItem = getDefaultСhild(mainGroup);

    return selectProductById(state, defaultItem.itemId);
};

const selectIsModifierQuantityBased = (state: RootState, productId: string) => {
    const product = selectProductById(state, productId);

    return product?.sizeGroupId === 'arb-sig-001-009';
};

export const selectModifierRelatedSelections = (
    state: RootState,
    productId: string,
    modifierId: string
): ItemModifierModel[] | null => {
    const modifierGroup = selectItemModifierGroupFromProduct(state, productId, modifierId);
    if (!modifierGroup) {
        return null;
    }

    const modifierItem = modifierGroup.itemModifiers[modifierId];
    const modifierItemProduct = selectProductWithRootIdById(state, modifierId);

    const selections = Object.values(modifierGroup.itemModifiers).filter((item) => {
        if (item.itemId === modifierItem.itemId) {
            return false;
        }
        const isModifierQuantityBased = selectIsModifierQuantityBased(state, item.itemId);
        if (isModifierQuantityBased) {
            return false;
        }

        const itemProduct = selectProductWithRootIdById(state, item.itemId);
        return itemProduct?.rootProductId === modifierItemProduct?.rootProductId;
    });

    if (!selections.length) {
        return null;
    }
    return selections;
};

export const selectDefaultModifiers = () => (state: RootState, productId: string): IDefaultModifier[] => {
    const product = selectProductById(state, productId);
    const isMeal = selectProductIsCombo(state, productId);
    const products = selectProducts(state);
    const comboMainDefaultProduct = selectComboMainProduct(state, productId);

    const getWhatsIncluded = (product: ItemModel, products: IProducts) => {
        const included: IDefaultModifier[] = [];

        product?.itemModifierGroups?.forEach((group) => {
            Object.values(group.itemModifiers || {}).forEach((modifier) => {
                if (modifier.defaultQuantity) {
                    const selections = selectModifierRelatedSelections(state, product.id, modifier.itemId);
                    const selectionAttributes: Pick<IDefaultModifier, 'selection' | 'relatedSelections'> = {};

                    if (selections?.length) {
                        const sizeGroup = selectSizeGroup(state, modifier.itemId);

                        selectionAttributes.selection = sizeGroup?.name;
                        selectionAttributes.relatedSelections = selections.map((it) => it.itemId);
                    }
                    const modifierProduct = products[modifier.itemId];

                    included.push({
                        name: modifierProduct?.name,
                        defaultQuantity: modifier.defaultQuantity,
                        minQuantity: modifier.min,
                        maxQuantity: modifier.max,
                        productId: modifier.itemId,
                        calories: getCaloriesFromProduct(modifierProduct),
                        metadata: group.metadata,
                        ...selectionAttributes,
                    });
                }
            });
        });

        return included;
    };

    if (isMeal) {
        return getWhatsIncluded(comboMainDefaultProduct, products);
    }

    return getWhatsIncluded(product, products);
};

export const selectSelectedModifiers = () => (state: RootState, tallyItem: PDPTallyItem): ISelectedModifier[] => {
    const getSelectedModifiers = (item: PDPTallyItem, products: IProducts) => {
        return (item?.modifierGroups || []).reduce<ISelectedModifier[]>((selected, tallyModifierGroup) => {
            const selectedModifiers = tallyModifierGroup?.modifiers?.filter((modifier) => modifier.quantity) || [];

            const selectedModifierData = selectedModifiers.map((selectedModifier) => {
                const product = products[selectedModifier.productId];
                const selections = selectModifierRelatedSelections(state, item.productId, selectedModifier.productId);

                const selectionAttributes: Pick<IDefaultModifier, 'selection' | 'relatedSelections'> = {};
                if (selections?.length) {
                    const sizeGroup = selectSizeGroup(state, selectedModifier.productId);
                    selectionAttributes.selection = sizeGroup?.name;
                    selectionAttributes.relatedSelections = selections.map((it) => it.itemId);
                }

                const selectedSubModifiers: ISelectedSubModifier[] = selectedModifier.modifierGroups?.flatMap(
                    (subModifiers) => {
                        return subModifiers.modifiers.map((subModifiersItem) => {
                            const product = products[subModifiersItem.productId];
                            return {
                                name: product?.name,
                                quantity: subModifiersItem?.quantity,
                                productId: subModifiersItem?.productId,
                                price: subModifiersItem?.price,
                                calories: getCaloriesFromProduct(product),
                            };
                        });
                    },
                    []
                );

                const isGroupWithFreeModifiers = !!GROUPS_WITH_FREE_MODIFIERS[tallyModifierGroup.productId];
                const price = isGroupWithFreeModifiers ? 0 : selectedModifier?.price;

                return {
                    name: product?.name,
                    quantity: selectedModifier?.quantity,
                    productId: selectedModifier?.productId,
                    price,
                    calories: getCaloriesFromProduct(product),
                    metadata: tallyModifierGroup?.metadata,
                    modifiers: selectedSubModifiers,
                    ...selectionAttributes,
                };
            });

            return [...selected, ...selectedModifierData];
        }, []);
    };

    const products = selectProducts(state);

    if (tallyItem?.childItems?.length > 0) {
        const mainProduct = selectComboMainProduct(state, tallyItem.productId);
        const tallyMain = tallyItem.childItems.find((it) => it.productId === mainProduct?.id);

        return getSelectedModifiers(tallyMain, products);
    }

    if (tallyItem?.modifierGroups) {
        return getSelectedModifiers(tallyItem, products);
    }

    return [];
};

export const selectSelectedSideAndDrinks = (state: RootState, tallyItem: PDPTallyItem): string[] => {
    const selectedSides = [];
    const mainProduct = selectComboMainProduct(state, tallyItem.productId);
    const isPromo = selectProductIsPromo(state, tallyItem.productId);

    if (!mainProduct) {
        return selectedSides;
    }

    if (tallyItem?.childItems) {
        tallyItem?.childItems.forEach((item) => {
            if (isPromo || item.productId !== mainProduct.id) {
                const product = selectProductById(state, item.productId);

                selectedSides.push(product.name);
            }
        });
    }

    return selectedSides;
};

export const selectProductSizes = (state: RootState, id: string): ISizeSelection[] | null => {
    const product = selectProductById(state, id);
    const rootProduct = selectRootProductByItemId(state, product?.id);

    if (!(product && rootProduct)) {
        return null;
    }

    const isGroupedBySize = isProductSizeGrouped(state, id);

    const byProductItemGroupId = (it: ItemModel) => it.itemGroupId === product.itemGroupId;
    const toSizeSelection = (product: ItemModel) => {
        const disabled = !product?.availability?.isAvailable;

        return { sizeGroup: selectSizeGroup(state, product.id), product, disabled, isGroupedBySize };
    };
    const bySequence = (a: ISizeSelection, b: ISizeSelection) => a?.sizeGroup?.sequence - b?.sizeGroup?.sequence;

    return Object.values(rootProduct.items).filter(byProductItemGroupId).map(toSizeSelection).sort(bySequence);
};

export const isProductSizeGrouped = (state: RootState, id: string): boolean => {
    const productGroup = selectItemGroup(state, id);

    return !otherItemGroupingNames.some((i) => i === productGroup?.name);
};

export const selectRelatedComboByMainProductIdAndSizeId = (
    state: RootState,
    id: string,
    comboSizeId: string
): ItemModel => {
    const rootProduct = selectRootProductByItemId(state, id);

    const relatedCombo = Object.values(rootProduct?.items || []).filter(
        (item) => !!Object.values(item?.itemModifierGroups[0]?.itemModifiers || []).find((item) => item.itemId === id)
    );

    return relatedCombo.find((combo) => combo.sizeGroupId === comboSizeId);
};

export const selectChildItemSizes = (state: RootState, childId: string, comboId: string): ISizeSelection[] | null => {
    const sizes = selectProductSizes(state, comboId);
    if (!sizes) {
        return null;
    }

    return sizes.map(({ sizeGroup, product, isGroupedBySize }) => {
        const productModifier = product.itemModifierGroups
            .flatMap((it) => Object.values(it.itemModifiers))
            .find((modifier) => {
                if (childId === modifier.itemId) {
                    return true;
                }

                const rootProduct = selectRootProductByItemId(state, modifier.itemId);

                return !!rootProduct.items[childId];
            });

        const productModifierItem = productModifier ? selectProductById(state, productModifier.itemId) : null;

        return {
            sizeGroup: sizeGroup,
            product: productModifierItem,
            disabled: !productModifierItem?.availability?.isAvailable,
            isGroupedBySize,
        };
    });
};

export const selectItemModifierGroup = (
    state: RootState,
    productId: string,
    productGroupId: string
): ItemModifierGroupModel => {
    const product = selectProductById(state, productId);

    return product.itemModifierGroups?.find((it) => it.productGroupId === productGroupId);
};

const selectItemModifierGroupFromProduct = (
    state: RootState,
    productId: string,
    itemModifierId: string
): ItemModifierGroupModel => {
    const product = selectProductById(state, productId);
    if (product?.itemModifierGroups) {
        for (const group of product.itemModifierGroups) {
            if (group.itemModifiers) {
                for (const modifierId in group.itemModifiers) {
                    if (modifierId === itemModifierId) {
                        return group;
                    }
                }
            }
        }
    }
    return null;
};

export const selectItemModifierFromProduct = (
    state: RootState,
    productId: string,
    itemModifierId: string
): ItemModifierModel | null => {
    const product = selectProductById(state, productId);

    if (product?.itemModifierGroups) {
        for (const group of product.itemModifierGroups) {
            const { itemModifiers } = group;

            if (itemModifiers) {
                for (const id in itemModifiers) {
                    if (id === itemModifierId) {
                        return itemModifiers[itemModifierId];
                    }
                }
            }
        }
    }

    return null;
};
const selectAppliedTallyModifiers = (
    state: RootState,
    tallyItem: PDPTallyItem,
    modifiersToApply: PDPTallyItemModifierGroup[]
): PDPTallyItemModifierGroup[] => {
    const verifiedModifiersToApply: { [productGroupId: string]: PDPTallyItemModifier[] } = {};

    if (!modifiersToApply) {
        return null;
    }

    modifiersToApply.forEach((group) => {
        group.modifiers?.forEach((modifier) => {
            const itemModifier = selectItemModifierFromProduct(state, tallyItem.productId, modifier.productId);

            if (itemModifier) {
                const isVisible = isModifierVisible(itemModifier);
                if (isVisible) {
                    const verifiedGroup = verifiedModifiersToApply[group.productId];

                    if (verifiedGroup) {
                        verifiedGroup.push(modifier);
                    } else {
                        verifiedModifiersToApply[group.productId] = [modifier];
                    }
                }
            }
        });
    });

    return tallyItem?.modifierGroups?.map((group) => {
        const itemsToApply = verifiedModifiersToApply[group.productId];

        if (itemsToApply) {
            const newGroup = {
                ...group,
                modifiers: group.modifiers,
            };

            while (itemsToApply.length) {
                const item = itemsToApply.pop();
                const index = newGroup.modifiers.findIndex((it) => it.productId === item.productId);

                if (index !== -1) {
                    newGroup.modifiers[index] = item;
                } else {
                    newGroup.modifiers.push(item);
                }
            }

            return newGroup;
        }
        return group;
    });
};

export const selectTallyItemForNewSize = (
    state: RootState,
    newSizeProductId: string,
    currentSizeTallyItem: PDPTallyItem
): PDPTallyItem => {
    const { brandId } = getBrandInfo();
    const newSizeDefaultTallyItem = selectDefaultTallyItem(state, newSizeProductId);

    if (newSizeDefaultTallyItem.childItems) {
        const newSizeProduct = selectProductById(state, newSizeProductId);

        const modifierGroupIdCurrentChildItemMap: {
            [productGroupId: string]: PDPTallyItem;
        } = currentSizeTallyItem.childItems.reduce((acc, item) => {
            const currentChildItemModifierGroup = selectItemModifierGroupFromProduct(
                state,
                currentSizeTallyItem.productId,
                item.productId
            );

            return {
                ...acc,
                [currentChildItemModifierGroup.productGroupId]: item,
            };
        }, {});

        const currentChildItemNewSizeProductMap: {
            [currentChildProductId: string]: ItemModel;
        } = currentSizeTallyItem.childItems.reduce((acc, item) => {
            const allSizes = selectChildItemSizes(state, item.productId, currentSizeTallyItem.productId);

            const newSize = allSizes.find((it) => it.sizeGroup.id === newSizeProduct.sizeGroupId);

            return { ...acc, [item.productId]: newSize.product };
        }, {});

        return {
            ...newSizeDefaultTallyItem,
            quantity: currentSizeTallyItem.quantity,
            childItems: newSizeDefaultTallyItem.childItems.map((newSizeDefaultChildTallyItem) => {
                const newChildItemModifiersGroup = selectItemModifierGroupFromProduct(
                    state,
                    newSizeProductId,
                    newSizeDefaultChildTallyItem.productId
                );

                const currentChildItem = modifierGroupIdCurrentChildItemMap[newChildItemModifiersGroup.productGroupId];

                if (currentChildItem) {
                    const newChildProduct = currentChildItemNewSizeProductMap[currentChildItem.productId];

                    if (newChildProduct) {
                        const tally = selectDefaultTallyItemForChildItem(state, newSizeProductId, newChildProduct.id);

                        return {
                            ...tally,
                            quantity: currentChildItem.quantity,
                            modifierGroups: selectAppliedTallyModifiers(state, tally, currentChildItem.modifierGroups),
                        };
                    }

                    return currentChildItem;
                }

                return newSizeDefaultChildTallyItem;
            }),
        };
    }

    // TODO add a way to toggle files other than components and have a two separate functions
    if (brandId === 'Bww') {
        return {
            ...newSizeDefaultTallyItem,
            quantity: currentSizeTallyItem.quantity,
        };
    }

    return {
        ...newSizeDefaultTallyItem,
        quantity: currentSizeTallyItem.quantity,
        modifierGroups:
            selectAppliedTallyModifiers(state, newSizeDefaultTallyItem, currentSizeTallyItem.modifierGroups) || [],
    };
};

export const selectBwwIsModifierOnSide = (state: RootState, modifierId: string): boolean => {
    const product = selectProductById(state, modifierId);

    return product?.sizeGroupId === ON_SIDE_SIZE_GROUP_ID;
};

export const selectBwwIsModifierNoSauce = (state: RootState, modifierId: string): boolean => {
    const product = selectProductById(state, modifierId);

    return product?.categoryIds?.some((it) => it === NO_SAUCE_CATEGORY_ID);
};

export const selectBwwIsRecommendedProduct = (state: RootState, modifierId: string): boolean => {
    const product = selectProductById(state, modifierId);

    return product?.categoryIds?.some((it) => it === RECOMMENDED_CATEGORY_ID);
};

export const selectIsProductHasRequiredModifiers = (state: RootState, productId: string) => {
    const products = selectProducts(state);

    const product = products[productId];

    const isModifierGroupHasRequiredModifiers = (modifierGroup: ItemModifierGroupModel) => {
        return (
            Object.keys(modifierGroup.itemModifiers || {}).reduce(
                (acc, key) => acc + modifierGroup.itemModifiers[key].defaultQuantity,
                0
            ) < modifierGroup.min
        );
    };

    const modifierGroups = product?.itemModifierGroups || [];
    for (const modifierGroup of modifierGroups) {
        if (isModifierGroupHasRequiredModifiers(modifierGroup)) {
            return true;
        }

        // check nested default modifiers
        for (const modifierId of Object.keys(modifierGroup.itemModifiers || {})) {
            if (modifierGroup.itemModifiers[modifierId]?.defaultQuantity) {
                const nestedModifierGroups = products[modifierId]?.itemModifierGroups || [];
                for (const nestedModifierGroup of nestedModifierGroups) {
                    if (isModifierGroupHasRequiredModifiers(nestedModifierGroup)) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
};
