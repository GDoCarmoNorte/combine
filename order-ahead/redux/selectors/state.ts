import { RootState } from '../store';

export const selectState = (state: RootState): RootState => state;
