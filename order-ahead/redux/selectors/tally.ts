import {
    ItemModel,
    ItemModifierGroupModel,
    ItemModifierModel,
    TallyModifierGroupModel,
    TallyModifierModel,
    TallyProductModel,
} from '../../@generated/webExpApi';
import { getCaloriesFromProduct, getCheapestPriceFromProduct, getModifierPrice } from '../../lib/domainProduct';
import { RootState } from '../store';
import { selectLineItems } from './bag';
import getBrandInfo from '../../lib/brandInfo';
import {
    selectDefaultModifiers,
    selectItemModifierFromProduct,
    selectProductById,
    selectProducts,
    selectProductsByIds,
} from './domainMenu';
import { selectCurrenOrderLocation } from './orderLocation';
import { PDPTallyItem } from '../pdp';
import { IDefaultModifier } from '../types';
import GROUPS_WITH_FREE_MODIFIERS from '../../common/constants/groupsWithFreeModifiers';

const getTallyItemModifiers = (tallyItem: TallyProductModel): TallyModifierModel[] => {
    return tallyItem?.modifierGroups?.reduce((modifiers, group) => [...modifiers, ...group.modifiers], []) || [];
};

const findDomainModifierGroup = (
    tallyItemGroup: TallyModifierGroupModel,
    domainProduct: ItemModel
): ItemModifierGroupModel => {
    return domainProduct?.itemModifierGroups?.find((group) => group.productGroupId === tallyItemGroup.productId);
};

const findDomainModifier = (
    tallyModifier: TallyModifierModel,
    domainModifierGroup: ItemModifierGroupModel
): ItemModifierModel => {
    return domainModifierGroup?.itemModifiers && domainModifierGroup?.itemModifiers[tallyModifier.productId];
};

const createTallyItemMap = (state: RootState, tallyItem: PDPTallyItem): Map<TallyProductModel, ItemModel> => {
    return (tallyItem?.childItems || [tallyItem]).reduce(
        (map: Map<TallyProductModel, ItemModel>, tallyItem: TallyProductModel) => {
            map.set(tallyItem, selectProductById(state, tallyItem?.productId));

            return map;
        },
        new Map<TallyProductModel, ItemModel>()
    );
};

interface ExtendedItemModifierModel extends ItemModifierModel {
    isFreeModifier?: boolean;
}

const createTallyItemModifiersMap = (
    tallyItem: TallyProductModel,
    domainProduct: ItemModel,
    defaultModifiers: IDefaultModifier[]
): Map<TallyModifierModel, ExtendedItemModifierModel> => {
    const modifiersMap = new Map<TallyModifierModel, ExtendedItemModifierModel>();

    tallyItem?.modifierGroups?.forEach((modifierGroup) => {
        const domainGroup = findDomainModifierGroup(modifierGroup, domainProduct);

        modifierGroup.modifiers?.forEach((modifier) => {
            const domainModifier = findDomainModifier(modifier, domainGroup);

            if (domainModifier) {
                modifiersMap.set(modifier, {
                    ...domainModifier,
                    isFreeModifier: !!GROUPS_WITH_FREE_MODIFIERS[modifierGroup.productId],
                });
            }
        });
    });

    /*
     * handle case for unselected default modifiers
     * default modifiers should be presented on map as they affect calories calculation
     */
    const usedModifierIds = [...modifiersMap.keys()].map((modifier) => modifier.productId);
    defaultModifiers.forEach((defaultModifier) => {
        if (!usedModifierIds.includes(defaultModifier.productId)) {
            modifiersMap.set({ quantity: 0, price: 0, productId: defaultModifier.productId }, defaultModifier);
        }
    });
    return modifiersMap;
};

const selectTallyItemPriceAndCalories = (
    state: RootState,
    tallyItem: PDPTallyItem,
    applyDiscount?: boolean
): { price: number; calories: number; totalPrice: number } => {
    let price = 0;
    let calories = 0;
    let totalPrice = 0;
    const location = selectCurrenOrderLocation(state);
    const domainProduct = selectProductById(state, tallyItem?.productId);

    if (tallyItem?.childItems?.length > 0) {
        totalPrice = tallyItem.childItems.reduce((price, item) => price + item.price, 0);
        price = tallyItem.childItems.reduce((price, item) => price + item.price, 0);
        calories = tallyItem.childItems.reduce((calories, item) => {
            const product = selectProductById(state, item.productId);
            return calories + (product?.nutrition?.totalCalories || 0);
        }, 0);
    } else {
        price = applyDiscount
            ? getCheapestPriceFromProduct(domainProduct, location) || tallyItem?.price
            : domainProduct?.price?.currentPrice;
        calories = getCaloriesFromProduct(domainProduct);
        totalPrice = tallyItem?.price;
    }

    return { price, calories, totalPrice };
};

const selectTallyItemModifiersPriceAndCalories = (
    state: RootState,
    tallyItem: PDPTallyItem
): { price: number; calories: number } => {
    const result = { price: 0, calories: 0 };
    const tallyItemMap = createTallyItemMap(state, tallyItem);

    tallyItemMap.forEach((domainProduct, item) => {
        const defaultModifiers = selectDefaultModifiers()(state, domainProduct?.id);
        const modifiersMap = createTallyItemModifiersMap(item, domainProduct, defaultModifiers);
        const modifierProducts = selectProductsByIds(state, [
            ...getTallyItemModifiers(item).map((m) => m.productId),
            ...defaultModifiers.map((m) => m.productId),
        ]);

        modifiersMap.forEach((domainModifier, modifier) => {
            if (!domainModifier.isFreeModifier) {
                result.price += modifier.price * Math.max(modifier.quantity - domainModifier.defaultQuantity, 0);
            }
            result.calories +=
                modifierProducts[modifier.productId]?.nutrition?.totalCalories *
                    (modifier.quantity - domainModifier.defaultQuantity) || 0;
        });
    });

    return result;
};

export const selectTallyPriceAndCalories = (
    state: RootState,
    tallyItem: PDPTallyItem,
    applyDiscount?: boolean
): { price: number; calories: number; totalPrice: number } => {
    const { brandId } = getBrandInfo();
    if (brandId === 'Bww') return selectBwwTallyPriceAndCalories(state, tallyItem, applyDiscount);

    const { price: itemPrice, calories: itemCalories, totalPrice: itemTotalPice } = selectTallyItemPriceAndCalories(
        state,
        tallyItem,
        applyDiscount
    );
    const { price: modifiersPrice, calories: modifiersCalories } = selectTallyItemModifiersPriceAndCalories(
        state,
        tallyItem
    );

    const totalCalories = Number.isFinite(itemCalories) ? itemCalories + modifiersCalories : undefined;

    return {
        price: itemPrice + modifiersPrice,
        calories: totalCalories,
        totalPrice: itemTotalPice + modifiersPrice,
    };
};

export const selectBwwTallyPriceAndCalories = (
    state: RootState,
    tallyItem: PDPTallyItem,
    applyDiscount?: boolean
): { price: number; calories: number; totalPrice: number } => {
    const { price: itemPrice, calories: itemCalories, totalPrice: itemTotalPice } = selectTallyItemPriceAndCalories(
        state,
        tallyItem,
        applyDiscount
    );

    const { price: modifiersPrice, calories: modifiersCalories } = selectTallyItemModifiersPriceAndCalories(
        state,
        tallyItem
    );

    const nestedModifiersPriceAndCalories: {
        price: number;
        calories: number;
    } = tallyItem?.modifierGroups?.reduce(
        (result, group) => {
            const { calories: nestedModifiersCalories, price: nestedModifiersPrice } = group?.modifiers?.reduce(
                (resultForGroup, modifier) => {
                    const {
                        price: groupModifiersPrice,
                        calories: groupModifiersCalories,
                    } = selectTallyItemModifiersPriceAndCalories(state, modifier);

                    return {
                        calories: resultForGroup.calories + groupModifiersCalories,
                        price: resultForGroup.price + groupModifiersPrice,
                    };
                },
                { price: 0, calories: 0 }
            );

            return {
                calories: result.calories + nestedModifiersCalories,
                price: result.price + nestedModifiersPrice,
            };
        },
        { price: 0, calories: 0 }
    );

    const extrasPriceAndCalories = tallyItem?.childExtras?.filter(Boolean)?.reduce(
        (acc, item) => {
            const extraItemPriceAndCalories = selectBwwTallyPriceAndCalories(state, item, applyDiscount);

            return {
                ...acc,
                calories: acc.calories + extraItemPriceAndCalories.calories,
                price: acc.price + extraItemPriceAndCalories.price,
                totalPrice: acc.totalPrice + extraItemPriceAndCalories.totalPrice,
            };
        },
        { price: 0, calories: 0, totalPrice: 0 }
    ) || { price: 0, calories: 0, totalPrice: 0 };

    const nestedModifiersCalories = nestedModifiersPriceAndCalories?.calories || 0;
    const nestedModifiersPrice = nestedModifiersPriceAndCalories?.price || 0;

    const totalCalories = Number.isFinite(itemCalories)
        ? itemCalories + modifiersCalories + nestedModifiersCalories + extrasPriceAndCalories.calories
        : undefined;

    return {
        price: itemPrice + modifiersPrice + nestedModifiersPrice + extrasPriceAndCalories.price,
        calories: totalCalories,
        totalPrice: itemTotalPice + modifiersPrice + nestedModifiersPrice + extrasPriceAndCalories.totalPrice,
    };
};

interface ISelectTallyItemsWithPricesAndCalories extends TallyProductModel {
    productData: {
        price: number;
        calories: number;
        totalPrice: number;
    };
}

export const selectTallyItemsWithPricesAndCalories = (
    state: RootState,
    tallyItems: TallyProductModel[],
    applyDiscount?: boolean
): ISelectTallyItemsWithPricesAndCalories[] => {
    return tallyItems.map((item) => ({
        ...item,
        productData: {
            ...selectTallyPriceAndCalories(state, item, applyDiscount),
        },
    }));
};

const selectTallyItemGroupModifiersUpdatePrices = (
    state: RootState,
    tallyItem: TallyProductModel
): TallyModifierGroupModel[] => {
    const domainProducts = selectProducts(state);

    return tallyItem?.modifierGroups?.map(
        (group): TallyModifierGroupModel => ({
            ...group,
            modifiers: group.modifiers?.map(
                (modifier): TallyModifierModel => {
                    const domainModifier = selectItemModifierFromProduct(
                        state,
                        tallyItem.productId,
                        modifier.productId
                    );

                    return { ...modifier, price: getModifierPrice(domainModifier, domainProducts[modifier.productId]) };
                }
            ),
        })
    );
};

const selectTallyItemUpdatePriceForChild = (
    state: RootState,
    tallyItem: TallyProductModel,
    childTallyItem: TallyProductModel
): TallyProductModel => {
    const childProduct = selectProductById(state, childTallyItem.productId);
    const childItemModifier = selectItemModifierFromProduct(state, tallyItem.productId, childTallyItem.productId);

    return {
        ...childTallyItem,
        price: getModifierPrice(childItemModifier, childProduct),
        modifierGroups: selectTallyItemGroupModifiersUpdatePrices(state, childTallyItem),
    };
};

export function selectTallyItemUpdatePrices(state: RootState, tallyItem: TallyProductModel): TallyProductModel {
    const domainProduct = selectProductById(state, tallyItem.productId);
    const location = selectCurrenOrderLocation(state);

    if (tallyItem?.childItems?.length > 0) {
        return {
            ...tallyItem,
            price: getCheapestPriceFromProduct(domainProduct, location),
            childItems: tallyItem.childItems.map((item) => selectTallyItemUpdatePriceForChild(state, tallyItem, item)),
        };
    }

    return {
        ...tallyItem,
        price: getCheapestPriceFromProduct(domainProduct, location),
        modifierGroups: selectTallyItemGroupModifiersUpdatePrices(state, tallyItem),
    };
}

export const selectTallyUpdateBagItems = (state: RootState): TallyProductModel[] => {
    const LineItems = selectLineItems(state);

    return LineItems?.map((LineItem) => selectTallyItemUpdatePrices(state, LineItem));
};
