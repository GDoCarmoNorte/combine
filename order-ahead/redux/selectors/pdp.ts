import { createSelector } from '@reduxjs/toolkit';
import {
    getCaloriesFromProduct,
    getModifierPrice,
    getQuantityByProductIdAndTallyItem,
    getModifierQuantityForSingleProduct,
    isModifierVisible,
    getCheapestPriceFromProduct,
    getTallyModifierGroupModifiersCount,
} from '../../lib/domainProduct';
import { RootState } from '../store';
import {
    IAboutSectionInfo,
    IDisplayFullProduct,
    IDisplayModifierGroup,
    IDisplayProductSection,
    ISizeSelection,
    INutritionInfoSizeSelection,
    IDisplayProduct,
    ProductTypesEnum,
    IExtraProductGroup,
    IExtraItemModel,
    ProductIntensityEnum,
    ISaucesDisplayModifierGroup,
    ISelectedExtraItem,
    ModifierGroupType,
    IDomainProductItem,
    IDefaultModifier,
    ItemGroupEnum,
} from '../types';
import {
    selectProducts,
    selectCategoriesByProductId,
    selectProductGroups,
    selectProductIsCombo,
    selectProductById,
    selectProductGroupById,
    selectProductGroupByProductId,
    selectDefaultModifiers,
    selectProductSizes,
    selectSizeGroup,
    selectChildItemSizes,
    selectComboMainProduct,
    selectProductIsPromo,
    selectModifierRelatedSelections,
    selectProductGroup,
    selectItemModifierGroup,
    selectBwwIsModifierOnSide,
    selectModifierGroupsByProductId,
    selectNoSauceDomainProduct,
    selectDefaultTallyModifierGroups,
    selectBwwIsModifierNoSauce,
    selectBwwIsRecommendedProduct,
    selectProductsByIds,
    isProductSizeGrouped,
    IProducts,
    selectProductsWithRootProductId,
    ItemModelRootProductId,
} from './domainMenu';

import { selectCurrenOrderLocation } from './orderLocation';

import { PDPState, PDPTallyItem, PDPTallyItemModifier, PDPTallyItemModifierGroup } from '../pdp';
import { IProductGroupModel, ItemModel, ItemModifierGroupModel, ItemModifierModel } from '../../@generated/webExpApi';
import { selectState } from './state';
import { selectTallyPriceAndCalories, selectBwwTallyPriceAndCalories } from './tally';
import { isWingTypeModifierGroup } from '../../common/helpers/isWingTypeModifierGroup';
import _find from 'lodash/find';
import GROUPS_WITH_FREE_MODIFIERS from '../../common/constants/groupsWithFreeModifiers';

const selectPdp = (state: RootState): PDPState => state.pdp;

const selectModifierIsAvailable = (state: RootState, id: string): boolean =>
    selectProductById(state, id)?.availability?.isAvailable;

export const selectPDPTallyItem = createSelector(selectPdp, (pdp: PDPState): PDPTallyItem => pdp.tallyItem);

const getProductGroupType = (
    displayName: string,
    group?: IProductGroupModel
): IDisplayProductSection['productSectionType'] => {
    if (isWingTypeModifierGroup(group)) {
        return ModifierGroupType.WINGTYPE;
    }

    const lowerName = displayName.toLowerCase();
    if (lowerName.includes('drink') || lowerName.includes('shake')) {
        return 'drink';
    }
    if (lowerName.includes('side')) {
        return 'side';
    }

    // do not remove space from 'sandwich '
    if (lowerName.includes('promo') || lowerName.includes('sandwich ')) {
        return 'promo';
    }

    if (lowerName.includes('sauce')) {
        return 'sauce';
    }

    return 'main';
};

const selectPDPProduct = createSelector(selectPDPTallyItem, selectProducts, (tallyItem, products) => {
    return products[tallyItem?.productId] || ({} as ItemModel); // TODO figure out how better handle empty returns (SSR)
});

const selectPDPProductWithMenuItem = createSelector(
    selectPDPTallyItem,
    selectProductsWithRootProductId,
    (tallyItem, products) => {
        return products[tallyItem?.productId] || ({} as ItemModelRootProductId);
    }
);

export type DisplayModifierGroupMap = { [key: string]: IDisplayModifierGroup };

export const selectDisplayModifierGroupsByProductIdAndProductGroupId = createSelector(
    (state: RootState, productId: string, productGroupId: string, sectionIndex: number): IDisplayModifierGroup[] => {
        const product = selectProductById(state, productId);
        const productGroup = selectProductGroupById(state, productGroupId);
        const products = selectProducts(state);
        const productGroups = selectProductGroups(state);
        const isCombo = selectProductIsCombo(state, productId);
        const isPromo = selectProductIsPromo(state, productId);
        const tallyItem = selectPDPTallyItem(state);

        if (!productGroup || !product) return [];

        const bySequence = (a: { sequence: number }, b: { sequence: number }) => a.sequence - b.sequence;

        const groupToDisplayGroup = (productId, group: ItemModifierGroupModel): IDisplayModifierGroup => {
            return {
                displayName: productGroups[group.productGroupId].name,
                minQuantity: group.min,
                maxQuantity: group.max,
                sequence: productGroups[group.productGroupId].sequence,
                modifierGroupId: group.productGroupId,
                modifiers: Object.values(group.itemModifiers || {})
                    .filter(isModifierVisible)
                    .filter((item) => selectModifierIsAvailable(state, item.itemId))
                    .sort(bySequence)
                    .reduce<IDisplayProduct[]>((acc, itemModifier) => {
                        const currentProduct = products[itemModifier.itemId];
                        const relatedSelections = selectModifierRelatedSelections(
                            state,
                            productId,
                            itemModifier.itemId
                        );

                        if (!relatedSelections) {
                            return acc.concat({
                                displayName: currentProduct.name,
                                displayProductDetails: {
                                    calories: getCaloriesFromProduct(currentProduct),
                                    defaultQuantity: itemModifier.defaultQuantity,
                                    displayName: currentProduct.name,
                                    minQuantity: itemModifier.min,
                                    maxQuantity: itemModifier.max,
                                    price: getModifierPrice(itemModifier, currentProduct),
                                    productId: currentProduct.id,
                                    quantity: getQuantityByProductIdAndTallyItem(
                                        currentProduct.id,
                                        tallyItem,
                                        sectionIndex
                                    ),
                                },
                            });
                        }

                        const allSelections = relatedSelections.concat(itemModifier).sort(bySequence);
                        const isPresent = acc.find((item) =>
                            allSelections.some((it) => it.itemId === item.displayProductDetails.productId)
                        );
                        if (isPresent) {
                            return acc;
                        }

                        const selectedModifier = allSelections.find((it) =>
                            getQuantityByProductIdAndTallyItem(it.itemId, tallyItem, sectionIndex)
                        );
                        const defaultModifier = allSelections.find((it) => it.defaultQuantity) || allSelections[0];

                        const activeSelectionItemModifier = selectedModifier || defaultModifier;
                        const activeSelectionProduct = selectProductById(state, activeSelectionItemModifier.itemId);

                        return acc.concat({
                            displayName: activeSelectionProduct.name,
                            displayProductDetails: {
                                calories: getCaloriesFromProduct(activeSelectionProduct),
                                defaultQuantity: activeSelectionItemModifier.defaultQuantity,
                                displayName: activeSelectionProduct.name,
                                minQuantity: activeSelectionItemModifier.min,
                                maxQuantity: activeSelectionItemModifier.max,
                                price: getModifierPrice(activeSelectionItemModifier, activeSelectionProduct),
                                productId: activeSelectionProduct.id,
                                quantity: getQuantityByProductIdAndTallyItem(
                                    activeSelectionProduct.id,
                                    tallyItem,
                                    sectionIndex
                                ),
                                selections: allSelections
                                    .map((item) => {
                                        const product = selectProductById(state, item.itemId);
                                        return {
                                            sizeGroup: selectSizeGroup(state, product.id),
                                            product,
                                            disabled: !product?.availability?.isAvailable,
                                            isGroupedBySize: isProductSizeGrouped(state, product.id),
                                        };
                                    })
                                    .sort((a, b) => a.sizeGroup.sequence - b.sizeGroup.sequence),
                            },
                        });
                    }, []),
            };
        };

        const byModifiersLength = (group: IDisplayModifierGroup) => group.modifiers.length;

        if (!isCombo && !isPromo) {
            return product.itemModifierGroups
                ?.map((group) => groupToDisplayGroup(product.id, group))
                .filter(byModifiersLength)
                .sort(bySequence);
        } else {
            const groupToProcess = product?.itemModifierGroups.find(
                (group) => group.productGroupId === productGroup.id
            );
            const productGroupType = getProductGroupType(productGroup?.name || '');

            if (productGroupType === 'main') {
                const mainProduct = products[groupToProcess.defaultItemModifierId];
                return mainProduct.itemModifierGroups
                    .map((group) => groupToDisplayGroup(mainProduct.id, group))
                    .filter(byModifiersLength)
                    .sort(bySequence);
            } else {
                const result = Object.values(groupToProcess.itemModifiers)
                    .sort(bySequence)
                    .filter(
                        (item) => (isPromo || isModifierVisible(item)) && selectModifierIsAvailable(state, item.itemId)
                    )
                    .reduce<DisplayModifierGroupMap>((prev, current) => {
                        const currentProduct = products[current.itemId];
                        const displayProduct = {
                            displayName: currentProduct.name,
                            displayProductDetails: {
                                calories: getCaloriesFromProduct(currentProduct),
                                defaultQuantity: 0,
                                displayName: currentProduct.name,
                                minQuantity: current.min,
                                maxQuantity: current.max,
                                price: getModifierPrice(current, currentProduct),
                                productId: currentProduct.id,
                                quantity: getQuantityByProductIdAndTallyItem(
                                    currentProduct.id,
                                    tallyItem,
                                    sectionIndex
                                ),
                            },
                        };

                        if (!prev[currentProduct.productGroupId]) {
                            return {
                                ...prev,
                                [currentProduct.productGroupId]: {
                                    displayName: productGroups[currentProduct.productGroupId].name,
                                    minQuantity: 0,
                                    sequence: productGroups[currentProduct.productGroupId].sequence,
                                    maxQuantity: 20,
                                    modifierGroupId: productGroups[currentProduct.productGroupId].id,
                                    modifiers: [displayProduct],
                                },
                            };
                        }

                        return {
                            ...prev,
                            [currentProduct.productGroupId]: {
                                ...prev[currentProduct.productGroupId],
                                modifiers: [...prev[currentProduct.productGroupId].modifiers, displayProduct],
                            },
                        };
                    }, {});

                return Object.values(result).filter(byModifiersLength);
            }
        }
    },
    (displayModifierGroups) => displayModifierGroups
);

const selectMainProductSectionName = (state: RootState): string => {
    const pdpProduct = selectPDPProduct(state);

    if (!pdpProduct?.id) {
        return null;
    }

    const isCombo = selectProductIsCombo(state, pdpProduct.id);
    if (isCombo) {
        const mainProduct = selectComboMainProduct(state, pdpProduct.id);
        const productGroup = selectProductGroupByProductId(state, mainProduct.id);

        return productGroup.name;
    }

    const productGroup = selectProductGroupByProductId(state, pdpProduct.id);

    return productGroup?.name;
};

export const selectMainComboDisplayProductById = (state: RootState, id: string): IDisplayProduct => {
    if (!id) return null;
    const products = selectProducts(state);
    const product = products[id];
    const selections = selectProductSizes(state, id);

    return {
        displayName: product.name,
        displayProductDetails: {
            productId: product.id,
            calories: product?.nutrition?.totalCalories || 0,
            displayName: product.name,
            quantity: 1, // by default for main combo product
            selections,
        },
    };
};

export const selectProductType = (state: RootState): ProductTypesEnum => {
    const tallyItem = selectPDPTallyItem(state);
    const isCombo = selectProductIsCombo(state, tallyItem?.productId);
    const isPromo = selectProductIsPromo(state, tallyItem?.productId);

    if (isCombo) {
        return ProductTypesEnum.Meal;
    }

    if (isPromo) {
        return ProductTypesEnum.Promo;
    }

    return ProductTypesEnum.Single;
};

export const selectDisplayProduct = createSelector(
    selectState,
    selectPDPTallyItem,
    selectPDPProduct,
    selectProducts,
    selectProductGroups,
    selectMainProductSectionName,
    selectProductType,
    (state, tallyItem, product, products, productGroups, mainProductSectionName, productType): IDisplayFullProduct => {
        const { price, calories, totalPrice } = selectTallyPriceAndCalories(state, tallyItem, true);

        const result = {
            productType,
            displayName: product.name,
            productId: product.id,
            mainProductId: product.id,
            mainProductSectionName,
            productSections: [],
            price,
            totalPrice,
            calories,
        };

        if (productType === ProductTypesEnum.Meal || productType === ProductTypesEnum.Promo) {
            const productSections: IDisplayProductSection[] =
                product.itemModifierGroups &&
                product.itemModifierGroups.map((item) => ({
                    productSectionType: getProductGroupType(productGroups[item.productGroupId].name),
                    productSectionDisplayName: productGroups[item.productGroupId].name,
                    productGroupId: item.productGroupId,
                }));

            return {
                ...result,
                productSections,
            };
        }

        if (productType === ProductTypesEnum.Single) {
            const productSections: IDisplayProductSection[] = [
                {
                    productSectionType: 'main',
                    productSectionDisplayName: mainProductSectionName,
                    productGroupId: product.productGroupId,
                },
            ];

            return {
                ...result,
                productSections,
            };
        }

        return result;
    }
);

const sortOrder = [
    'Serving Weight (g)',
    'Calories',
    'Calories from Fat',
    'Fat - Total (g)',
    'Saturated Fat (g)',
    'Trans Fat (g)',
    'Cholesterol (mg)',
    'Sodium (mg)',
    'Total Carbohydrates (g)',
    'Dietary Fiber (g)',
    'Sugars (g)',
    'Proteins (g)',
    'Allergens',
];

export const selectAboutSectionInfo = (state: RootState): IAboutSectionInfo | null => {
    const pdpTallyItem = selectPDPTallyItem(state);
    if (!pdpTallyItem?.productId) {
        return null;
    }
    const { productId: pdpProductId } = pdpTallyItem;

    const pdpProduct = selectPDPProduct(state);
    const isCombo = selectProductIsCombo(state, pdpProductId);
    const isPromo = selectProductIsPromo(state, pdpProductId);
    const defaultModifiers = selectDefaultModifiers()(state, pdpProductId);
    const productsFromModifiers = (pdpTallyItem.modifierGroups || []).reduce((acc, mg) => {
        if (mg.metadata?.MODIFIER_GROUP_TYPE === ModifierGroupType.SELECTIONS) {
            const products = mg?.modifiers
                ?.filter((modifier) => modifier.quantity > 0)
                .map((modifier) => selectProductById(state, modifier.productId));
            if (products) {
                return [...acc, ...products];
            }
        }

        return acc;
    }, []);

    const products =
        isCombo || isPromo
            ? pdpTallyItem.childItems.map(({ productId }) => selectProductById(state, productId))
            : [pdpProduct, ...productsFromModifiers];

    const productGroup = selectProductGroupByProductId(state, pdpProductId);
    if (!productGroup || !productGroup.name || !productGroup.name) {
        return null;
    }

    const uniqById = (item: ItemModel, index: number, arr: ItemModel[]) =>
        arr.findIndex((it) => it.id === item.id) === index;

    return {
        productKind: productGroup.name,
        description: pdpProduct.description,
        whatsIncluded: defaultModifiers.map((it) => it.name),
        nutritionInfo: products.filter(uniqById).map((product) => {
            const productSizes =
                isCombo || isPromo
                    ? selectChildItemSizes(state, product.id, pdpProductId)
                    : selectProductSizes(state, product.id);
            const toProduct = (item: ISizeSelection) => item.product;

            const toNutritionInfoSizeSection = (product: ItemModel): INutritionInfoSizeSelection => {
                const { macroNutrients = {}, allergenInformation } = product.nutrition || {};
                const sortedFields = macroNutrients
                    ? Object.values(macroNutrients).sort((a, b) =>
                          sortOrder.indexOf(a.label) < sortOrder.indexOf(b.label) ? -1 : 1
                      )
                    : null;

                if (!sortedFields && !allergenInformation) {
                    return null;
                }

                const sizeGroup = selectSizeGroup(state, product.id);
                return {
                    name: sizeGroup.name,
                    nutritionalFacts: sortedFields,
                    allergicInformation: allergenInformation,
                };
            };

            return {
                displayName: product.name,
                sizeSelections: productSizes
                    .map(toProduct)
                    .filter(Boolean)
                    .filter(uniqById)
                    .map(toNutritionInfoSizeSection)
                    .filter(Boolean),
            };
        }),
    };
};

const bySequence = (a: { sequence: number }, b: { sequence: number }) => a.sequence - b.sequence;

const removeExtras = (item: ItemModifierGroupModel) => {
    return item.metadata?.MODIFIER_GROUP_TYPE !== ModifierGroupType.EXTRAS;
};

export const selectBwwDisplayProduct = createSelector(
    selectState,
    selectPDPTallyItem,
    selectPDPProductWithMenuItem,
    selectProductGroups,
    selectMainProductSectionName,
    selectProductType,
    (state, tallyItem, product, productGroups, mainProductSectionName, productType): IDisplayFullProduct => {
        const { price, calories, totalPrice } = selectBwwTallyPriceAndCalories(state, tallyItem, true);

        const result = {
            productType,
            displayName: product.menuItemName || product.name,
            productId: product.id,
            mainProductId: product.id,
            mainProductSectionName,
            productSections: [],
            price,
            totalPrice,
            calories,
        };

        const productSections: IDisplayProductSection[] =
            product.itemModifierGroups &&
            product.itemModifierGroups
                .filter(removeExtras)
                .sort(bySequence)
                .map((item) => ({
                    productSectionType: getProductGroupType(productGroups[item.productGroupId].name, item),
                    productSectionDisplayName: productGroups[item.productGroupId].name,
                    productGroupId: item.productGroupId,
                }));

        return {
            ...result,
            productSections,
        };
    }
);

export const selectBwwRegularSauces = (state: RootState, modifierGroupId: string): ItemModifierModel[] => {
    const tallyItem = selectPDPTallyItem(state);
    const group = selectItemModifierGroup(state, tallyItem.productId, modifierGroupId);

    return Object.values(group.itemModifiers || {}).filter((it) => !selectBwwIsModifierOnSide(state, it.itemId));
};

export const selectBwwOnSideSauces = (state: RootState, modifierGroupId: string): ItemModifierModel[] => {
    const tallyItem = selectPDPTallyItem(state);
    const group = selectItemModifierGroup(state, tallyItem.productId, modifierGroupId);

    return Object.values(group.itemModifiers || {}).filter(
        (it) => selectBwwIsModifierOnSide(state, it.itemId) || selectBwwIsModifierNoSauce(state, it.itemId)
    );
};

export const selectBwwSauceDisplayModifierGroup = (
    state: RootState,
    modifierGroupId: string
): ISaucesDisplayModifierGroup => {
    const tallyItem = selectPDPTallyItem(state);
    const { productId } = tallyItem;
    const group = selectItemModifierGroup(state, productId, modifierGroupId);
    const productGroup = selectProductGroupById(state, group.productGroupId);

    const allItemModifiers = Object.values(group.itemModifiers || {});

    const hasSauceOnTheSideOption = allItemModifiers.some((it) => {
        return selectBwwIsModifierOnSide(state, it.itemId);
    });

    const currentModifierGroup = tallyItem.modifierGroups?.find((it) => it.productId === modifierGroupId);

    const isOnSideChecked = currentModifierGroup?.isOnSideChecked;

    let itemModifiers: ItemModifierModel[] = [];

    if (hasSauceOnTheSideOption) {
        if (isOnSideChecked) {
            itemModifiers = selectBwwOnSideSauces(state, modifierGroupId);
        } else {
            itemModifiers = selectBwwRegularSauces(state, modifierGroupId);
        }
    } else {
        itemModifiers = allItemModifiers;
    }

    const numberOfFreeModifiers = GROUPS_WITH_FREE_MODIFIERS[modifierGroupId];
    const isGroupWithFreeModifiers = !!numberOfFreeModifiers;
    const maxQuantity =
        isGroupWithFreeModifiers && group.max > numberOfFreeModifiers ? numberOfFreeModifiers : group.max;

    return {
        displayName: productGroup.name,
        minQuantity: group.min,
        maxQuantity,
        sequence: productGroup.sequence,
        modifierGroupId: group.productGroupId,
        hasOnSideOption: hasSauceOnTheSideOption,
        isOnSideChecked,
        isOnSideOptionDisabled: currentModifierGroup?.isOnSideOptionDisabled,
        modifiers: itemModifiers
            .filter(isModifierVisible)
            .filter((item) => selectModifierIsAvailable(state, item.itemId))
            .sort(bySequence)
            .map((itemModifier) => {
                const modifierProduct = selectProductById(state, itemModifier.itemId);
                const isRecommended = selectBwwIsRecommendedProduct(state, itemModifier.itemId);

                const price = isGroupWithFreeModifiers ? 0 : getModifierPrice(itemModifier, modifierProduct);

                return {
                    displayName: modifierProduct.name,
                    displayProductDetails: {
                        calories: getCaloriesFromProduct(modifierProduct),
                        defaultQuantity: itemModifier.defaultQuantity,
                        displayName: modifierProduct.name,
                        minQuantity: itemModifier.min,
                        maxQuantity: itemModifier.max,
                        price,
                        productId: modifierProduct.id,
                        quantity: getModifierQuantityForSingleProduct(modifierProduct.id, tallyItem),
                        intensity: selectSauceIntensity(state, itemModifier.itemId),
                        isRecommended,
                    },
                };
            }),
    };
};

export const selectBwwDisplayModifierGroup = (
    state: RootState,
    tallyItem: PDPTallyItem,
    modifierGroupId: string,
    parentModifierGroupId?: string,
    parentModifierId?: string
): IDisplayModifierGroup => {
    const isNestedGroup = parentModifierGroupId && parentModifierId;

    const modifierId = isNestedGroup ? parentModifierId : tallyItem.productId;

    const group = selectItemModifierGroup(state, modifierId, modifierGroupId);

    const productGroup = selectProductGroupById(state, group?.productGroupId);

    const numberOfFreeModifiers = GROUPS_WITH_FREE_MODIFIERS[modifierGroupId];
    const isGroupWithFreeModifiers = !!numberOfFreeModifiers;
    const maxQuantity =
        isGroupWithFreeModifiers && group.max > numberOfFreeModifiers ? numberOfFreeModifiers : group.max;

    return {
        displayName: productGroup.name,
        minQuantity: group.min,
        maxQuantity,
        sequence: productGroup.sequence,
        modifierGroupId: group.productGroupId,
        modifiers: Object.values(group.itemModifiers || {})
            .filter(isModifierVisible)
            .filter((item) => selectModifierIsAvailable(state, item.itemId))
            .sort(bySequence)
            .map((itemModifier) => {
                const modifierProduct = selectProductById(state, itemModifier.itemId);

                const tallyModifierGroup = tallyItem?.modifierGroups.find(
                    (group) => group.productId === modifierGroupId
                );

                const tallyModifier = tallyModifierGroup?.modifiers.find(
                    (modifier) => modifier.productId === itemModifier.itemId
                );

                const priceAndCalories = tallyModifier
                    ? selectBwwTallyPriceAndCalories(state, tallyModifier, true)
                    : {
                          price: getModifierPrice(itemModifier, modifierProduct),
                          calories: getCaloriesFromProduct(modifierProduct),
                      };

                const price = isGroupWithFreeModifiers ? 0 : priceAndCalories.price;

                return {
                    displayName: modifierProduct.name,
                    displayProductDetails: {
                        calories: priceAndCalories.calories,
                        defaultQuantity: itemModifier.defaultQuantity,
                        displayName: modifierProduct.name,
                        minQuantity: itemModifier.min,
                        maxQuantity: itemModifier.max,
                        price,
                        productId: modifierProduct.id,
                        quantity: getModifierQuantityForSingleProduct(
                            modifierProduct.id,
                            tallyItem,
                            parentModifierGroupId,
                            parentModifierId
                        ),
                    },
                };
            }),
    };
};

export const selectBwwDisplayModifiersGroups = (
    state: RootState,
    tallyItem: PDPTallyItem,
    parentModifierGroupId?: string,
    parentModifierId?: string
): IDisplayModifierGroup[] => {
    const isNestedGroups = parentModifierGroupId && parentModifierId;
    const groups = selectModifierGroupsByProductId(state, isNestedGroups ? parentModifierId : tallyItem.productId);

    return groups
        .map((group) =>
            selectBwwDisplayModifierGroup(
                state,
                tallyItem,
                group.productGroupId,
                parentModifierGroupId,
                parentModifierId
            )
        )
        .filter((group) => group.modifiers.length)
        .sort(bySequence);
};

const isModifierADefault = (modifierId: string, defaultModifiers: IDefaultModifier[]) => {
    return !!_find(defaultModifiers, (defaultModifier) => modifierId === defaultModifier.productId);
};

export const selectBwwNewModiferGroups = (
    state: RootState,
    modifierGroupId: string,
    modifierId: string,
    quantity: number,
    modifierGroups: PDPTallyItemModifierGroup[],
    defaultModifiers: IDefaultModifier[],
    parentModifierGroupId?: string,
    parentModifierId?: string
): PDPTallyItemModifierGroup[] => {
    const location = selectCurrenOrderLocation(state);
    const tallyItem = selectPDPTallyItem(state);
    const modifierGroup = selectBwwDisplayModifierGroup(
        state,
        tallyItem,
        modifierGroupId,
        parentModifierGroupId,
        parentModifierId
    );

    const { minQuantity, maxQuantity } = modifierGroup;
    const noSauceDomainProduct = selectNoSauceDomainProduct(state, modifierGroup.modifiers);

    const products = selectProducts(state);

    const nestedModifierGroups = selectDefaultTallyModifierGroups(state, modifierId);

    return modifierGroups.map((group) => {
        if (group.productId !== modifierGroupId) {
            return group;
        }

        const currentModifer = group.modifiers?.find((modifier) => modifier.productId === modifierId);
        const isRadioButton = minQuantity <= 1 && maxQuantity === 1;
        const isCheckBox = maxQuantity > 1;
        const isCurrentExist = !!currentModifer;
        const isNoSauce = modifierId === noSauceDomainProduct?.id;
        const isNoSauceSelected = !!group.modifiers?.find(
            (modifier) => modifier.productId === noSauceDomainProduct?.id
        );
        const modifierProduct = products[modifierId];

        if (isWingTypeModifierGroup(group)) {
            if (!modifierId) {
                return {
                    ...group,
                    modifiers: [],
                };
            }

            return {
                ...group,
                modifiers: [
                    {
                        productId: modifierId,
                        quantity: 1,
                        price: getCheapestPriceFromProduct(modifierProduct, location),
                        modifierGroups: nestedModifierGroups,
                    },
                ],
            };
        }

        if (isRadioButton) {
            return {
                ...group,
                modifiers: [
                    {
                        productId: modifierId,
                        quantity: 1,
                        price: getCheapestPriceFromProduct(modifierProduct, location),
                        modifierGroups: nestedModifierGroups,
                    },
                ],
            };
        }

        if (isCheckBox) {
            const modifiers =
                group.modifiers
                    ?.filter((modifier) => {
                        // deselect NO SAUCE and quantity = 0 for non default modifiers
                        if (isModifierADefault(modifier.productId, defaultModifiers)) return true;
                        if (isNoSauceSelected && !isNoSauce) {
                            return modifier.productId !== noSauceDomainProduct?.id && modifier.quantity;
                        }

                        return modifier.quantity;
                    })
                    ?.map((modifier) => {
                        const modifierProduct = products[modifier.productId];

                        return {
                            productId: modifier.productId,
                            quantity: modifier.quantity,
                            price: getCheapestPriceFromProduct(modifierProduct, location),
                            modifierGroups: nestedModifierGroups,
                        };
                    }) || [];

            if (isNoSauce) {
                return {
                    ...group,
                    isOnSideChecked: false,
                    isOnSideOptionDisabled: quantity > 0,
                    modifiers: [
                        {
                            productId: modifierId,
                            quantity,
                            price: getCheapestPriceFromProduct(modifierProduct, location),
                            modifierGroups: nestedModifierGroups,
                        },
                    ],
                };
            }

            if (isCurrentExist) {
                return quantity === 0 && !isModifierADefault(modifierId, defaultModifiers)
                    ? {
                          ...group,
                          modifiers: modifiers.filter((modifier) => modifier.productId !== modifierId),
                      }
                    : {
                          ...group,
                          modifiers: modifiers.map((modifier) => {
                              if (modifier.productId !== modifierId) {
                                  return modifier;
                              }

                              return {
                                  ...modifier,
                                  quantity,
                              };
                          }),
                      };
            }

            return {
                ...group,
                isOnSideOptionDisabled: false,
                modifiers: [
                    ...modifiers,
                    {
                        productId: modifierId,
                        quantity,
                        price: getCheapestPriceFromProduct(modifierProduct, location),
                        modifierGroups: nestedModifierGroups,
                    },
                ],
            };
        }

        return group;
    });
};

export const selectExtrasDisplayProducts = createSelector(
    (state: RootState): IExtraProductGroup[] => {
        const tallyItem = selectPDPTallyItem(state);

        const groups = selectModifierGroupsByProductId(state, tallyItem.productId);
        const selectedExtrasIds = new Set(tallyItem.childExtras.map((it) => it?.productId));

        const result = groups
            .filter((mg) => mg.metadata?.MODIFIER_GROUP_TYPE === ModifierGroupType.EXTRAS)
            .map((group) => {
                const productGroup = selectProductGroupById(state, group?.productGroupId);

                return {
                    displayName: productGroup.name,
                    products: Object.values(group.itemModifiers || {})
                        .filter(isModifierVisible)
                        .filter((item) => selectModifierIsAvailable(state, item.itemId))
                        .sort(bySequence)
                        .reduce<IExtraItemModel[]>((acc, itemModifier) => {
                            const relatedSelections = selectModifierRelatedSelections(
                                state,
                                tallyItem.productId,
                                itemModifier.itemId
                            );

                            if (!relatedSelections) {
                                const currentProduct = selectProductById(state, itemModifier.itemId);
                                const tallyExtrasItem = tallyItem.childExtras.find(
                                    (it) => it?.productId === currentProduct.id
                                );

                                const priceAndCalories = tallyExtrasItem
                                    ? selectBwwTallyPriceAndCalories(state, tallyExtrasItem, true)
                                    : {
                                          price: getModifierPrice(itemModifier, currentProduct),
                                          calories: getCaloriesFromProduct(currentProduct),
                                      };

                                return acc.concat({
                                    ...currentProduct,
                                    resultPrice: priceAndCalories.price,
                                    resultCalories: priceAndCalories.calories,
                                    quantity: selectedExtrasIds.has(itemModifier.itemId) ? 1 : 0,
                                });
                            }

                            const allSelections = relatedSelections.concat(itemModifier).sort(bySequence);
                            const isPresent = acc.find((item) => allSelections.some((it) => it.itemId === item.id));
                            if (isPresent) {
                                return acc;
                            }

                            const selectedModifier = allSelections.find((it) => selectedExtrasIds.has(it.itemId));
                            const defaultModifier =
                                allSelections.find((it) => it.defaultQuantity) ||
                                allSelections.find((item) => {
                                    const product = selectProductById(state, item.itemId);

                                    return product?.availability.isAvailable;
                                });

                            const activeSelectionItemModifier = selectedModifier || defaultModifier;
                            if (!activeSelectionItemModifier) {
                                return acc;
                            }
                            const activeSelectionProduct = selectProductById(state, activeSelectionItemModifier.itemId);

                            const tallyExtrasItem = tallyItem.childExtras.find(
                                (it) => it?.productId === activeSelectionProduct.id
                            );

                            const priceAndCalories = tallyExtrasItem
                                ? selectBwwTallyPriceAndCalories(state, tallyExtrasItem, true)
                                : {
                                      price: getModifierPrice(itemModifier, activeSelectionProduct),
                                      calories: getCaloriesFromProduct(activeSelectionProduct),
                                  };

                            return acc.concat({
                                ...activeSelectionProduct,
                                resultPrice: priceAndCalories.price,
                                resultCalories: priceAndCalories.calories,
                                quantity: selectedModifier ? 1 : 0,
                                sizeSelections: allSelections
                                    .map((item) => {
                                        const product = selectProductById(state, item.itemId);
                                        return {
                                            sizeGroup: selectSizeGroup(state, product.id),
                                            product,
                                            disabled: !product?.availability?.isAvailable,
                                            isGroupedBySize: isProductSizeGrouped(state, product.id),
                                        };
                                    })
                                    .sort((a, b) => a.sizeGroup.sequence - b.sizeGroup.sequence),
                            });
                        }, []),
                    sequence: group.sequence,
                };
            })
            .filter((group) => group.products.length)
            .sort(bySequence);

        return result;
    },
    (extrasDisplayProducts) => extrasDisplayProducts
);

export const selectSauceIntensity = createSelector(
    selectProductById,
    selectProductGroup,
    selectCategoriesByProductId,
    (product, productGroup, productCategories): ProductIntensityEnum | null => {
        if (productGroup?.name === 'SAUCE') {
            const INTENSITY_CATEGORIES = Object.keys(ProductIntensityEnum);
            const intensityCategory = productCategories.find((category) =>
                INTENSITY_CATEGORIES.includes(category?.name.toUpperCase())
            );

            return intensityCategory ? ProductIntensityEnum[intensityCategory.name.toUpperCase()] : null;
        }

        return null;
    }
);

export const selectBwwSelectionsText = (
    state: RootState,
    pdpTallyItem: PDPTallyItem,
    productItemGroupName: string
): string => {
    const sizeSelections = selectProductSizes(state, pdpTallyItem.productId);
    const modifierGroups = pdpTallyItem.modifierGroups || [];
    const selections = modifierGroups.reduce((acc, { modifiers, metadata }) => {
        if (modifiers && metadata) {
            if (metadata.MODIFIER_GROUP_TYPE === ModifierGroupType.SELECTIONS) {
                return [...acc, ...modifiers];
            }
        }
        return acc;
    }, []);

    const modifiersProductsIds = selections.map(({ productId }) => productId);
    const modifiersProducts = selectProductsByIds(state, modifiersProductsIds);

    const formatModifiersNames = (modifiers: PDPTallyItemModifier[]) => {
        return modifiers.reduce((acc, modifier) => {
            const name = modifiersProducts[modifier.productId]?.name;

            if (name) {
                return [
                    ...acc,
                    modifier.quantity && modifier.quantity != 1 ? `${modifier.quantity} ${name}` : `${name}`,
                ];
            }

            return acc;
        }, [] as string[]);
    };

    const formatMainProductName = () => {
        if (sizeSelections && sizeSelections.length > 1) {
            const sizeSelection = sizeSelections.find(({ product: { id } }) => id === pdpTallyItem.productId);
            if (sizeSelection && sizeSelection.sizeGroup) {
                if (productItemGroupName === ItemGroupEnum.BOGO || productItemGroupName === ItemGroupEnum.BOGO_50_OFF) {
                    return `${pdpTallyItem.name} (${Number(sizeSelection.sizeGroup.name) * 2} Total)`;
                }
                return sizeSelection.isGroupedBySize && sizeSelection.sizeGroup?.name?.toLowerCase() !== 'none'
                    ? `${sizeSelection.sizeGroup.name} ${pdpTallyItem.name}`
                    : pdpTallyItem.name;
            }
        }
        return pdpTallyItem.name;
    };

    return [formatMainProductName(), ...formatModifiersNames(selections)].join(', ');
};

export const selectSelectedExtras = createSelector(
    (state: RootState): ISelectedExtraItem[] | null => {
        const tallyItem = selectPDPTallyItem(state);
        if (!tallyItem?.childExtras) {
            return null;
        }

        const result = tallyItem.childExtras.filter(Boolean).map((it) => {
            const product = selectProductById(state, it.productId);
            const { price, calories } = selectBwwTallyPriceAndCalories(state, it, true);

            return {
                name: product.name,
                price,
                calories,
            };
        });
        if (!result.length) {
            return null;
        }

        return result;
    },
    (selectedExtras) => selectedExtras
);

export const selectIsPDPProductReadyToAddToBag = createSelector(
    selectPDPTallyItem,
    selectProducts,
    (tallyItem: PDPTallyItem, products: IProducts) => {
        const tallyModifierGroups = tallyItem.modifierGroups?.filter(removeExtras) || [];

        const selectMenuModifierGroup = (product: IDomainProductItem, productGroupId: string) => {
            return product?.itemModifierGroups?.find((it) => it.productGroupId === productGroupId);
        };

        const isMinModifiersAmountIsReached = (
            menuModifierGroup: ItemModifierGroupModel,
            tallyModifierGroup: PDPTallyItemModifierGroup
        ) => {
            return getTallyModifierGroupModifiersCount(tallyModifierGroup) >= (menuModifierGroup?.min || 0);
        };

        for (const tallyModifierGroup of tallyModifierGroups) {
            if (
                !isMinModifiersAmountIsReached(
                    selectMenuModifierGroup(products[tallyItem.productId], tallyModifierGroup.productId),
                    tallyModifierGroup
                )
            ) {
                return false;
            }

            // handle nested modifiers
            for (const modifier of tallyModifierGroup.modifiers) {
                const nestedTallyModifierGroups = modifier.modifierGroups || [];
                for (const nestedTallyModifierGroup of nestedTallyModifierGroups) {
                    if (
                        !isMinModifiersAmountIsReached(
                            selectMenuModifierGroup(products[modifier.productId], nestedTallyModifierGroup.productId),
                            nestedTallyModifierGroup
                        )
                    ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
);
