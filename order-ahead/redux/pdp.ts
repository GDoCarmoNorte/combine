import { createSlice, Dispatch, PayloadAction } from '@reduxjs/toolkit';
import { getEmptyTallyItem } from '../lib/tallyItem';
import { selectDefaultTallyItem, selectProductWithRootIdById } from './selectors/domainMenu';
import {
    selectBwwNewModiferGroups,
    selectBwwOnSideSauces,
    selectBwwRegularSauces,
    selectPDPTallyItem,
} from './selectors/pdp';

import { RootState } from './store';
import { getCheapestPriceFromProduct } from '../lib/domainProduct';
import { selectCurrenOrderLocation } from './selectors/orderLocation';
import { TallyModifierGroupModel, TallyModifierModel, TallyProductModel } from '../@generated/webExpApi';
import { IDefaultModifier } from './types';

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export interface PDPTallyItemModifier extends Omit<TallyModifierModel, 'priceType' | 'actionCode'> {
    modifierGroups?: PDPTallyItemModifierGroup[];
}

export interface PDPTallyItemModifierGroup extends TallyModifierGroupModel {
    isOnSideChecked?: boolean;
    modifiers?: PDPTallyItemModifier[];
    metadata?: { [key: string]: string };
    isOnSideOptionDisabled?: boolean;
}

// Don't add lineItemId unless in bag!
export interface PDPTallyItem extends Omit<TallyProductModel, 'lineItemId' | 'childItems'> {
    lineItemId?: number;
    childItems?: PDPTallyItem[];
    childExtras?: PDPTallyItem[];
    madeAMeal?: boolean;
    name?: string;
    modifierGroups?: PDPTallyItemModifierGroup[];
    reAddCart?: boolean;
}

export interface PDPState {
    tallyItem?: PDPTallyItem;
    initialTallyItemId?: string;
    modifyingFromBag?: boolean;
}

export type PutTallyItemPayload = { pdpTallyItem: PDPTallyItem; madeAMeal?: boolean; lineItemId?: number };
export type PutInitialTallyItemIdPayload = { defaultTallyItemId: PDPTallyItem['productId'] };

export type EditTallyItemSizePayload = {
    pdpTallyItem: PDPTallyItem;
};

export type EditTallyItemModifiersPayload = {
    pdpTallyItem: PDPTallyItem;
};

export type EditTallyItemCountPayload = {
    pageProductId: string;
    lineItemId: number;
    value: number;
};

export type OnSauceOnSideChangePayload = { productGroupId: string };

export type OnModifierChangePayload = {
    modifierGroupId: string;
    modifierId: string;
    parentModifierGroupId?: string;
    parentModifierId?: string;
    quantity: number;
};

export const initialState: PDPState = {
    tallyItem: getEmptyTallyItem(),
};

const pdpSlice = createSlice({
    name: 'pdp',
    initialState,
    reducers: {
        putTallyItem: (state, action: PayloadAction<PutTallyItemPayload>) => {
            const { pdpTallyItem, madeAMeal, lineItemId } = action.payload;

            if (lineItemId) {
                // "Modify From Bag" clicked, lineItemId passed
                state.tallyItem = { ...pdpTallyItem, madeAMeal };
                state.tallyItem.lineItemId = lineItemId;
                state.modifyingFromBag = true;
            } else {
                // exclude case with initialization when land on PDP from bag from other page
                if (!state.modifyingFromBag) {
                    state.tallyItem = { ...pdpTallyItem, madeAMeal };
                }
                state.modifyingFromBag = false;
            }
        },
        putInitialTallyItemId: (state, action: PayloadAction<PutInitialTallyItemIdPayload>) => {
            const { defaultTallyItemId } = action.payload;
            state.initialTallyItemId = defaultTallyItemId;
        },
        resetTallyItem: (state, action: PayloadAction<PutTallyItemPayload>) => {
            state.tallyItem = action.payload.pdpTallyItem;
            state.modifyingFromBag = false;
        },
        editTallyItemSize: (state, action: PayloadAction<EditTallyItemSizePayload>) => {
            const { pdpTallyItem } = action.payload;

            state.tallyItem = pdpTallyItem;
        },
        editTallyItemModifiers: (state, action: PayloadAction<EditTallyItemModifiersPayload>) => {
            const { pdpTallyItem } = action.payload;

            state.tallyItem = pdpTallyItem;
        },
        editTallyItemCount: (
            state,
            { payload: { pageProductId, value, lineItemId } }: PayloadAction<EditTallyItemCountPayload>
        ) => {
            const pageTallyItem = state.tallyItem;

            if (pageTallyItem?.productId !== pageProductId) {
                return state;
            }

            if (pageTallyItem?.lineItemId && pageTallyItem.lineItemId === lineItemId) {
                pageTallyItem.quantity += value;
            }
        },
    },
});

function resetPdpState() {
    return (dispatch: Dispatch, getState: () => RootState): void => {
        const state = getState();
        const defaultTalyItem = selectDefaultTallyItem(state, state.pdp.initialTallyItemId);

        dispatch(
            pdpSlice.actions.resetTallyItem({
                pdpTallyItem: defaultTalyItem,
            })
        );
    };
}

function onSauceOnSideChange(payload: OnSauceOnSideChangePayload) {
    return (dispatch: Dispatch, getState: () => RootState): void => {
        const { productGroupId } = payload;
        const state = getState();
        const tallyItem = selectPDPTallyItem(state);

        const newTallyItem = {
            ...tallyItem,
            modifierGroups: tallyItem.modifierGroups.map((group) => {
                if (group.productId !== productGroupId) {
                    return group;
                }

                const newIsOnSideChecked = !group.isOnSideChecked;
                const location = selectCurrenOrderLocation(state);

                const oppositModifiers = (newIsOnSideChecked ? selectBwwOnSideSauces : selectBwwRegularSauces)(
                    state,
                    productGroupId
                ).map((it) => selectProductWithRootIdById(state, it.itemId));

                return {
                    ...group,
                    isOnSideChecked: newIsOnSideChecked,
                    modifiers: group.modifiers
                        .filter((it) => it.quantity)
                        .reduce((acc, item) => {
                            const { rootProductId } = selectProductWithRootIdById(state, item.productId);
                            const opposite = oppositModifiers.find((it) => it.rootProductId === rootProductId);

                            if (opposite) {
                                return [
                                    ...acc,
                                    {
                                        productId: opposite.id,
                                        quantity: 1, // we persist only 1 item
                                        price: getCheapestPriceFromProduct(opposite, location),
                                    },
                                ];
                            }
                            return acc;
                        }, []),
                };
            }),
        };

        dispatch(
            pdpSlice.actions.editTallyItemModifiers({
                pdpTallyItem: newTallyItem,
            })
        );
    };
}

function onModifierChange(payload: OnModifierChangePayload & { defaultModifiers: IDefaultModifier[] }) {
    return (dispatch: Dispatch, getState: () => RootState): void => {
        const { modifierGroupId, modifierId, parentModifierGroupId, parentModifierId, quantity } = payload;
        const state = getState();
        const tallyItem = selectPDPTallyItem(state);

        if (parentModifierGroupId && parentModifierId) {
            const newModifierGroups = tallyItem.modifierGroups.map((group) => {
                if (group.productId !== parentModifierGroupId) {
                    return group;
                }

                return {
                    ...group,
                    modifiers: group?.modifiers.map((modifier) => {
                        if (modifier.productId !== parentModifierId) {
                            return modifier;
                        }

                        return {
                            ...modifier,
                            modifierGroups: selectBwwNewModiferGroups(
                                state,
                                modifierGroupId,
                                modifierId,
                                quantity,
                                modifier.modifierGroups,
                                payload.defaultModifiers,
                                parentModifierGroupId,
                                parentModifierId
                            ),
                        };
                    }),
                };
            });

            dispatch(
                pdpSlice.actions.editTallyItemModifiers({
                    pdpTallyItem: {
                        ...tallyItem,
                        modifierGroups: newModifierGroups,
                    },
                })
            );
        } else {
            dispatch(
                pdpSlice.actions.editTallyItemModifiers({
                    pdpTallyItem: {
                        ...tallyItem,
                        modifierGroups: selectBwwNewModiferGroups(
                            state,
                            modifierGroupId,
                            modifierId,
                            quantity,
                            tallyItem.modifierGroups,
                            payload.defaultModifiers,
                            parentModifierGroupId,
                            parentModifierId
                        ),
                    },
                })
            );
        }
    };
}

export const actions = {
    ...pdpSlice.actions,
    onSauceOnSideChange,
    onModifierChange,
    resetPdpState,
};

export default pdpSlice.reducer;
