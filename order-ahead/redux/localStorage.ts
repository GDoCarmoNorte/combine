import { DeepPartial } from '@reduxjs/toolkit';
import merge from 'lodash/merge';
// Follows Dan Abramov's (Redux author) recommendation:
// https://egghead.io/lessons/javascript-redux-persisting-the-state-to-the-local-storage

import { RootState } from './store';
import { MENU_VERSION } from '../common/constants/menuVersion';

import { OrderLocationMethod } from './orderLocation';
import isEmpty from 'lodash/isEmpty';
import { getCountryCodeFromLocale, getLocale } from '../common/helpers/locale';
import { DEFAULT_LOCALE } from '../common/constants/configDefaults';

// TODO add versioning inside to use updating logging/mechanism to prevent edge cases to grow uncontrollably
export const loadState = (initialState): DeepPartial<RootState> => {
    try {
        const serializedState = localStorage.getItem(getStateKeyName());

        if (serializedState === null) {
            return undefined;
        }

        const localStorageState = JSON.parse(serializedState);

        // TODO: remove later
        if (Array.isArray(localStorageState?.domainMenu?.payload?.products)) {
            delete localStorageState.domainMenu.payload;
        }

        // @depricated - TODO: remove later, menu 2.0 migration has been completed
        if (localStorageState?.bag?.LineItems?.some((item) => /^ARBYS-WEBOA/i.test(item.productId))) {
            localStorageState.bag.LineItems = [];
        }

        // migrating old localStorageState location info to new state(after adding delivery address)
        if (localStorageState?.location) {
            // persist saved store after location v2 migration
            if (localStorageState?.location.storeId) {
                localStorageState.location.id = localStorageState.location.storeId;
            }

            localStorageState.orderLocation = {
                /**
                 * previously we have only pickup address.
                 * So "localStorageState.location" existence means user selected PICKUP address previously.
                 */
                method: OrderLocationMethod.PICKUP,
                pickupAddress: { ...localStorageState.location },
                deliveryAddress: null,
            };
            localStorageState.location = null;
        }

        //cleanup obsolete version of delivery location structure
        const deliveryLocationHoursByDay =
            localStorageState?.orderLocation?.deliveryAddress?.pickUpLocation?.hoursByDay;
        if (deliveryLocationHoursByDay && !isEmpty(deliveryLocationHoursByDay)) {
            const daysOfWeek = Object.keys(deliveryLocationHoursByDay);

            if (daysOfWeek.length > 0 && deliveryLocationHoursByDay[daysOfWeek[0]].startTime) {
                localStorageState.orderLocation.deliveryAddress = null;
                localStorageState.orderLocation.method = localStorageState.orderLocation.pickupAddress
                    ? OrderLocationMethod.PICKUP
                    : OrderLocationMethod.NOT_SELECTED;
            }
        }

        // clean up bag if menu version changed
        const version = localStorageState?.bag?.version;
        if (typeof version !== 'undefined') {
            const menuVersion = parseInt(version);

            localStorageState.bag.version = isNaN(menuVersion) ? MENU_VERSION : menuVersion;
            if (localStorageState.bag.version < MENU_VERSION) {
                localStorageState.bag.LineItems = [];
                localStorageState.bag.version = MENU_VERSION;
            }
        }

        // merge with new empty object to avoid rewriting initialState object
        return merge({}, initialState, localStorageState);
    } catch (err) {
        return undefined;
    }
};

export const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem(getStateKeyName(), serializedState);
    } catch (err) {
        // Ignore write errors
    }
};

const getStateKeyName = (): string => {
    const locale = getLocale();
    return locale === DEFAULT_LOCALE ? 'state' : `state_${getCountryCodeFromLocale(locale)}`;
};
