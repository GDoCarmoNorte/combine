import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { MENU_VERSION } from '../common/constants/menuVersion';
import { isInRange, prepareWorkingHours } from '../common/helpers/checkoutHelpers';
import { IDateRange } from '../lib/locations';
import { PDPTallyItem } from './pdp';
import { updateStateBagItemCount } from '../common/helpers/bagHelper';
import { TallyProductModel } from '../@generated/webExpApi';
import { addLineItemId } from '../common/helpers/tallyHelper';
import { OrderLocationMethod } from './orderLocation';

/**
 * Modifier item with possible nested modifier groups
 */
interface ModifierItem {
    modifierItemId: string;
    quantity: number;
    modifierGroups?: ModifierGroup;
}

/**
 * Modifier group (ex. Bread, Cheese) with included modifier items
 */
export interface ModifierGroup {
    modifierGroupId: string;
    modifierItems: ModifierItem[];
}

/**
 * High level entry in bag
 */

export type WorkingHours = {
    day: string;
    timeRange: string[];
}[];

export type OrderTimeType = 'asap' | 'future';

export interface BagState {
    LineItems: TallyProductModel[];
    markedAsRemoved: number[];
    isOpen: boolean;
    tooltipOpen: boolean;
    entryCreated: boolean;
    entryUpdated: boolean;
    entryCreatedWithDeal: boolean;
    entryUpdatedWithDeal: boolean;
    pickupTime: string;
    pickupTimeType: OrderTimeType;
    deliveryTime: string;
    deliveryTimeType: OrderTimeType;
    pickupTimeValues: WorkingHours;
    version: number;
    dealId?: string;
    lastRemovedLineItemId: number;
    lastEdit: number;
}

export type AddToBagPayload = {
    name?: string;
    category?: string;
    udpRecommendationId?: string;
    bagEntry: Omit<TallyProductModel, 'lineItemId'>;
    sauce?: { [key: string]: string };
};

export type AddDealToBagPayload = {
    id: string;
};

export type AddToBagWithDefaultsPayload = {
    name?: string;
    category?: string;
    recommendationId?: string;
    productId: string;
};
export type PutToBagPayload = {
    name?: string;
    category?: string;
    pdpTallyItem: PDPTallyItem;
    sauce?: { [key: string]: string };
};
export type EditBagLineItem = { bagEntry: TallyProductModel };
export type MarkAsRemovedPayload = { lineItemId: number };
export type RemoveFromBagPayload = { lineItemId?: number };
export type RemoveAllFromBagPayload = { lineItemIdList: number[] };
export type ToggleIsOpenPayload = { isOpen: boolean };
export type UpdateTooltipPayload = {
    tooltipIsOpen: boolean;
    updated?: boolean;
    created?: boolean;
    createdWithDeal?: boolean;
    updatedWithDeal?: boolean;
};
export type ClearBagPayload = undefined;
export type UpdateBagItemPayload = { bagEntryIndex: number; value: TallyProductModel };
export type UpdateBagItemsPayload = { value: TallyProductModel[] };
export type UpdateBagItemCountPayload = { bagEntryIndex: number; value: number };
export type SetPickupTimePayload = { time?: string; asap?: boolean; method: OrderLocationMethod };
export type InitPickupTimeValuesPayload = { openedTimeRanges: IDateRange[]; timezone: string };
export type SetPickupTimeValuesPayload = WorkingHours;
export type ValidateItemsPayload = {
    tallyBagEntries: TallyProductModel[];
    time?: Date;
};

export const initialState: BagState = {
    LineItems: [],
    markedAsRemoved: [],
    isOpen: false,
    tooltipOpen: false,
    entryCreated: false,
    entryUpdated: false,
    entryCreatedWithDeal: false,
    entryUpdatedWithDeal: false,
    pickupTime: null,
    pickupTimeType: 'asap',
    deliveryTime: null,
    deliveryTimeType: 'asap',
    pickupTimeValues: null,
    dealId: null,
    version: MENU_VERSION,
    lastRemovedLineItemId: null,
    lastEdit: null,
};

const bagSlice = createSlice({
    name: 'bag',
    initialState,
    reducers: {
        addToBag: (state, action: PayloadAction<AddToBagPayload>): BagState => {
            const { bagEntry, ...rest } = action.payload;
            const lineItems = state.LineItems.length;

            const product = addLineItemId(bagEntry, lineItems);
            state.LineItems.push({
                ...product,
                ...rest,
            });

            return state;
        },
        addMultipleToBag: (state, action: PayloadAction<AddToBagPayload[]>): BagState => {
            state.LineItems.push(
                ...action.payload.map(({ bagEntry, ...rest }) => ({
                    ...bagEntry,
                    ...rest,
                }))
            );

            return state;
        },
        addDealToBag: (state, action: PayloadAction<AddDealToBagPayload>): BagState => {
            const { id } = action.payload;

            state.dealId = id;

            return state;
        },
        removeDealFromBag: (state): BagState => {
            state.dealId = null;

            return state;
        },
        editBagLineItem: (state, action: PayloadAction<EditBagLineItem>): BagState => {
            const updatedLineItem = action.payload.bagEntry;

            state.LineItems = state.LineItems.map((lineItem) => {
                if (lineItem.lineItemId === updatedLineItem.lineItemId) {
                    return updatedLineItem;
                }
                return lineItem;
            });

            return state;
        },
        markAsRemoved: (state, action: PayloadAction<MarkAsRemovedPayload>): BagState => {
            const { lineItemId } = action.payload;
            const index = state.markedAsRemoved.indexOf(lineItemId);
            const alreadyMarkedAsRemoved = index !== -1;

            if (!alreadyMarkedAsRemoved) {
                state.markedAsRemoved.push(lineItemId);
            } else {
                state.markedAsRemoved.splice(index, 1);
            }

            return state;
        },
        clearLastRemovedLineItemId: (state): BagState => {
            state.lastRemovedLineItemId = null;

            return state;
        },
        removeFromBag: (state, action: PayloadAction<RemoveFromBagPayload>): BagState => {
            const { lineItemId } = action.payload;

            const lineItemIndex = state.LineItems.findIndex((lineItem) => lineItem.lineItemId == lineItemId);

            state.LineItems.splice(lineItemIndex, 1);

            state.lastRemovedLineItemId = lineItemId;

            state.LineItems = state.LineItems.map((el, i) => {
                return addLineItemId(el, i);
            });

            return state;
        },
        removeAllFromBag: (state, action: PayloadAction<RemoveAllFromBagPayload>): BagState => {
            const { lineItemIdList } = action.payload;

            state.LineItems = state.LineItems.filter(
                (lineItem) => lineItemIdList.findIndex((lineItemId) => lineItem.lineItemId === lineItemId) < 0
            );
            state.dealId = null;

            return state;
        },
        clearBag: (): BagState => {
            return initialState;
        },
        updateBagItem: (state, action: PayloadAction<UpdateBagItemPayload>): BagState => {
            const { bagEntryIndex, value } = action.payload;

            if (bagEntryIndex < 0) return state;

            state.LineItems[bagEntryIndex] = value;

            return state;
        },
        updateBagItems: (state: BagState, action: PayloadAction<UpdateBagItemsPayload>): BagState => {
            const newLineItems = action.payload.value;
            const LineItems: TallyProductModel[] = state.LineItems.map(
                (LineItem): TallyProductModel => {
                    const newLineItem = newLineItems.find(
                        (item) => item.lineItemId === LineItem.lineItemId && item.productId === LineItem.productId
                    );

                    return newLineItem || LineItem;
                }
            );

            return {
                ...state,
                LineItems,
            };
        },
        updateBagItemCount: (state, action: PayloadAction<UpdateBagItemCountPayload>): BagState => {
            const { bagEntryIndex, value } = action.payload;
            updateStateBagItemCount(state, bagEntryIndex, value);
            return state;
        },
        updateBagItemsCount: (state, action: PayloadAction<UpdateBagItemCountPayload[]>): BagState => {
            action.payload.forEach(({ bagEntryIndex, value }) => {
                updateStateBagItemCount(state, bagEntryIndex, value);
            });

            return state;
        },
        toggleIsOpen: (state, action: PayloadAction<ToggleIsOpenPayload>): BagState => {
            state.isOpen = action.payload.isOpen;
            return state;
        },
        updateTooltip: (state, action: PayloadAction<UpdateTooltipPayload>): BagState => {
            state.tooltipOpen = action.payload.tooltipIsOpen;
            state.entryUpdated = action.payload.updated || false;
            state.entryCreated = action.payload.created || false;
            state.entryCreatedWithDeal = action.payload.createdWithDeal || false;
            state.entryUpdatedWithDeal = action.payload.updatedWithDeal || false;
            return state;
        },
        setPickupTime: (state, action: PayloadAction<SetPickupTimePayload>): BagState => {
            const { time, asap, method } = action.payload;

            if (method === OrderLocationMethod.PICKUP) {
                state.pickupTime = time !== undefined ? time : state.pickupTime;
                state.pickupTimeType = asap ? 'asap' : 'future';
            }

            if (method === OrderLocationMethod.DELIVERY) {
                state.deliveryTime = time !== undefined ? time : state.pickupTime;
                state.deliveryTimeType = asap ? 'asap' : 'future';
            }

            return state;
        },
        resetOrderTime: (state): BagState => {
            state.pickupTime = initialState.pickupTime;
            state.pickupTimeType = initialState.pickupTimeType;
            state.deliveryTime = initialState.deliveryTime;
            state.deliveryTimeType = initialState.deliveryTimeType;
            state.pickupTimeValues = initialState.pickupTimeValues;

            return state;
        },
        resetPickupTime: (state): BagState => {
            state.pickupTime = initialState.pickupTime;
            state.pickupTimeType = initialState.pickupTimeType;
            state.pickupTimeValues = initialState.pickupTimeValues;

            return state;
        },
        resetDeliveryTime: (state): BagState => {
            state.deliveryTime = initialState.deliveryTime;
            state.deliveryTimeType = initialState.deliveryTimeType;

            return state;
        },
        //TODO: remove after Arby's migrate to use fulfillment timeslots
        initPickupTimeValues: (state, action: PayloadAction<InitPickupTimeValuesPayload>): BagState => {
            const workingHours = prepareWorkingHours(action.payload.openedTimeRanges, action.payload.timezone);
            state.pickupTimeValues = workingHours;

            if (!state.pickupTime || !isInRange(state.pickupTime, workingHours)) {
                state.pickupTime = null;
                state.pickupTimeType = 'asap';
            } else {
                state.pickupTimeType = 'future';
            }

            return state;
        },
        setPickupTimeValues: (state, action: PayloadAction<SetPickupTimeValuesPayload>): BagState => {
            state.pickupTimeValues = action.payload;

            return state;
        },
        setLastEdit: (state, action: PayloadAction<number>): BagState => {
            state.lastEdit = action.payload;

            return state;
        },
        clearLastEdit: (state): BagState => {
            state.lastEdit = null;

            return state;
        },
    },
});

export const { actions } = bagSlice;

export default bagSlice.reducer;
