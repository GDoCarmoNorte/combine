import { Dispatch, Action, AnyAction } from 'redux';
import { EnhancedStore } from '@reduxjs/toolkit';
import { RootState } from '../store';
import gtmService from '../../common/services/gtmService/gtmService';
import {
    GTM_MENU_IMPRESSION,
    GTM_TRANSACTION_COMPLETE,
    GTM_TRANSACTION_INFO,
    GTM_CHECKOUT_PAYMENT,
    GTM_CHECKOUT_VIEW,
    GTM_CHECKOUT_CUSTOMER_INFO,
    GTM_CHECKOUT_PAYMENT_INFO,
    GTM_CHECKOUT_PICK_UP_TIME_SELECTION,
    GTM_PRODUCT_VIEW,
    GTM_REMOVE_FROM_CART,
    GTM_RESTORE_ITEM_TO_CART,
    GTM_COMBO_SELECTION,
    GTM_MODIFY_PRODUCT,
    GTM_QTY_DECREASE,
    GTM_QTY_INCREASE,
    GTM_SEARCH_NEW_LOCATION,
    GTM_START_PICKUP_ORDER,
    GTM_MAKE_MY_STORE,
    GTM_ADD_MORE_ITEMS,
    GTM_EMAIL_SIGN_UP,
    GTM_MODIFIER_CUSTOMIZATION,
    GTM_GIFT_CARD_BALANCE_FORM_SUBMIT_SUCCESS,
    GTM_MAP_CLICK,
    GTM_MAP_DOUBLE_CLICK,
    GTM_MAP_DRAG_START,
    GTM_MAP_DRAG,
    GTM_MAP_DRAG_END,
    GTM_MAP_LOCATION_CLICK,
    GTM_MAP_LIST_LOCATION_CLICK,
    GTM_USER_ID,
    GTM_ONLINE_ORDER_COMING_SOON,
    GTM_MODIFIER_SECTION,
    GTM_CHECKOUT_PAYMENT_METHOD_TYPE_EVENT,
    GTM_ERROR_EVENT,
    GTM_CHECKOUT_TIP_SELECTION_EVENT,
    GTM_LOCATION_ORDER,
    GTM_CHANGE_LOCATION,
    GTM_CHECK_IN,
    GTM_ACCOUNT_DELETED_FEEDBACK,
    GTM_ACCOUNT_DELETED_SUBMITTED,
} from '../../common/services/gtmService/constants';

const GTM_ACTIONS_MAP = {
    'bag/addToBag': gtmService.pushAddToCardEvent,
    'bag/putToBag': gtmService.pushAddToCardEvent,
    'bag/toggleIsOpen': gtmService.pushCartOpenEvent,
    [GTM_QTY_DECREASE]: gtmService.pushQtyDecreaseEvent,
    [GTM_QTY_INCREASE]: gtmService.pushQtyIncreaseEvent,
    [GTM_COMBO_SELECTION]: gtmService.pushComboSelectionEvent,
    [GTM_MODIFY_PRODUCT]: gtmService.pushModifyItemEvent,
    [GTM_MODIFIER_CUSTOMIZATION]: gtmService.pushModifierCustomizationEvent,
    [GTM_ADD_MORE_ITEMS]: gtmService.pushAddMoreItemsEvent,
    [GTM_REMOVE_FROM_CART]: gtmService.pushRemoveFromCartEvent,
    [GTM_RESTORE_ITEM_TO_CART]: gtmService.pushRestoreItemToCardEvent,
    [GTM_MENU_IMPRESSION]: gtmService.pushImpressionViewEvent,
    [GTM_TRANSACTION_COMPLETE]: gtmService.pushTransactionCompleteEvent,
    [GTM_TRANSACTION_INFO]: gtmService.pushTransactionInfoEvent,
    [GTM_CHECKOUT_PAYMENT]: gtmService.pushCheckoutPaymentEvent,
    [GTM_CHECKOUT_CUSTOMER_INFO]: gtmService.pushCheckoutPaymentEvent,
    [GTM_CHECKOUT_PAYMENT_INFO]: gtmService.pushCheckoutPaymentEvent,
    [GTM_CHECKOUT_VIEW]: gtmService.pushCheckoutViewEvent,
    [GTM_PRODUCT_VIEW]: gtmService.pushProductViewEvent,
    [GTM_SEARCH_NEW_LOCATION]: gtmService.pushSearchNewLocationEvent,
    [GTM_START_PICKUP_ORDER]: gtmService.pushStartPickupOrderEvent,
    [GTM_MAKE_MY_STORE]: gtmService.pushMakeMyStoreEvent,
    [GTM_ONLINE_ORDER_COMING_SOON]: gtmService.pushMakeOnlineOrderingComingSoonEvent,
    [GTM_EMAIL_SIGN_UP]: gtmService.pushEmailSignUp,
    [GTM_GIFT_CARD_BALANCE_FORM_SUBMIT_SUCCESS]: gtmService.pushGiftCardBalanceFormSubmitSuccess,
    [GTM_MAP_CLICK]: gtmService.pushMapClickEvent,
    [GTM_MAP_DOUBLE_CLICK]: gtmService.pushMapDoubleClickEvent,
    [GTM_MAP_DRAG_START]: gtmService.pushMapDragStartEvent,
    [GTM_MAP_DRAG]: gtmService.pushMapDragEvent,
    [GTM_MAP_DRAG_END]: gtmService.pushMapDragEndEvent,
    [GTM_MAP_LOCATION_CLICK]: gtmService.pushMapLocationClickEvent,
    [GTM_MAP_LIST_LOCATION_CLICK]: gtmService.pushMapListLocationClickEvent,
    [GTM_USER_ID]: gtmService.pushUserId,
    [GTM_CHECKOUT_PICK_UP_TIME_SELECTION]: gtmService.pushPickupTimeSelection,
    [GTM_MODIFIER_SECTION]: gtmService.pushModifierSelection,
    [GTM_CHECKOUT_PAYMENT_METHOD_TYPE_EVENT]: gtmService.pushCheckoutPaymentMethodType,
    [GTM_ERROR_EVENT]: gtmService.pushError,
    [GTM_CHECKOUT_TIP_SELECTION_EVENT]: gtmService.pushTipSelection,
    [GTM_LOCATION_ORDER]: gtmService.pushLocationOrder,
    [GTM_CHANGE_LOCATION]: gtmService.pushLocationChange,
    [GTM_CHECK_IN]: gtmService.pushCheckInEvent,
    [GTM_ACCOUNT_DELETED_SUBMITTED]: gtmService.pushAccountDeletedSubmitted,
    [GTM_ACCOUNT_DELETED_FEEDBACK]: gtmService.pushAccountDeletedFeedback,
};
const gtmMiddleware = (store: EnhancedStore<RootState>) => (next: Dispatch<AnyAction>) => (
    action: AnyAction
): Action<AnyAction> => {
    if (GTM_ACTIONS_MAP[action.type]) {
        GTM_ACTIONS_MAP[action.type](action.payload, store.getState());
        return next(action);
    }

    return next(action);
};

export default gtmMiddleware;
