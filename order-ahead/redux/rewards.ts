import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    ICertificateModel,
    IGetCustomerAccountRewardsResponseModel,
    IGetCustomerRewardsActivityHistoryResponseModel,
    IInactiveOfferModel,
    IOfferModel,
    IPurchaseCustomerAccountRewardResponseModel,
    TCatalogCertificateModel,
    TCatalogCertificatesByCategoryModel,
    TCertificateStatusModel,
    TRewardOfferStatusModel,
} from '../@generated/webExpApi';

export type IRewards = IGetCustomerAccountRewardsResponseModel;
export type IRewardsCatalog = TCatalogCertificatesByCategoryModel;

export type TRewardsRecommendations = Array<TCatalogCertificateModel>;

export type IRewardsState = IRewards & {
    loading: boolean;
    lastPurchasedCertificate: IPurchaseCustomerAccountRewardResponseModel;
} & {
    rewardsCatalog: IRewardsCatalog;
    rewardsCatalogLoading: boolean;
    rewardsRecommendations: TRewardsRecommendations;
} & {
    rewardsActivityHistory: IGetCustomerRewardsActivityHistoryResponseModel[];
    rewardsActivityHistoryLoading: boolean;
};

export type ISetRewardStatusPayload = {
    rewardId: string;
    status: TCertificateStatusModel | TRewardOfferStatusModel;
};
export type IRewardsActivityHistory = IGetCustomerRewardsActivityHistoryResponseModel[];
export type InactiveOffers = IInactiveOfferModel[];
export type Offers = (IOfferModel | IInactiveOfferModel)[];
export type Certificates = ICertificateModel[];
export type LastPurchasedCertificate = IPurchaseCustomerAccountRewardResponseModel | null;
export const initialState: IRewardsState = {
    totalCount: 0,
    certificates: [],
    offers: [],
    loading: false,
    rewardsCatalog: {},
    rewardsRecommendations: [],
    rewardsCatalogLoading: false,
    rewardsActivityHistory: [],
    rewardsActivityHistoryLoading: false,
    lastPurchasedCertificate: null,
};

const rewardsSlice = createSlice({
    name: 'rewards',
    initialState,
    reducers: {
        setRewards: (state, action: PayloadAction<IRewardsState>) => {
            state.totalCount = action.payload.totalCount;
            state.certificates = action.payload.certificates;
            state.offers = action.payload.offers;
            state.loading = false;
        },
        setRewardsLoading: (state, action: PayloadAction<boolean>) => {
            state.loading = action.payload;
        },
        setRewardStatus: (state, action: PayloadAction<ISetRewardStatusPayload>) => {
            const { status, rewardId } = action.payload;
            const certificate = state.certificates.find((certificate) => certificate.number === rewardId);
            const offer = state.offers.find((offer) => offer.id === rewardId);
            const reward = certificate || offer;

            if (!reward) return;
            reward.status = status;
        },
        setRewardsCatalog: (state, action: PayloadAction<IRewardsCatalog>) => {
            state.rewardsCatalog = action.payload;
        },
        setRewardsCatalogLoading: (state, action: PayloadAction<boolean>) => {
            state.rewardsCatalogLoading = action.payload;
        },
        setRewardsRecommendations: (state, action: PayloadAction<TRewardsRecommendations>) => {
            state.rewardsRecommendations = action.payload;
        },
        setRewardsActivityHistory: (state, action: PayloadAction<IRewardsActivityHistory>) => {
            state.rewardsActivityHistory = action.payload;
        },
        setRewardsActivityHistoryLoading: (state, action: PayloadAction<boolean>) => {
            state.rewardsActivityHistoryLoading = action.payload;
        },
        setLastPurchasedCertificate: (state, action: PayloadAction<IPurchaseCustomerAccountRewardResponseModel>) => {
            state.lastPurchasedCertificate = action.payload;
        },
    },
});

export const { actions, name } = rewardsSlice;

export default rewardsSlice.reducer;
