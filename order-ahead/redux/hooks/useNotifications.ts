import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../store';
import * as NotificationsStore from '../notifications';

type EnqueuePayload = Error | NotificationsStore.EnqueueNotificationPayload;

export interface IUseNotifications {
    notifications: NotificationsStore.Notification[];
    actions: {
        enqueue: (payload: EnqueuePayload) => void;
        enqueueError: (payload: EnqueuePayload) => void;
        enqueueSuccess: (payload: EnqueuePayload) => void;
        dequeue: () => void;
        remove: (payload: NotificationsStore.RemoveNotificationPayload) => void;
    };
}

export default function useNotifications(): IUseNotifications {
    const dispatch = useDispatch();

    const notifications = useSelector<RootState, NotificationsStore.Notification[]>((state) => state.notifications);

    const enqueue = (payload: EnqueuePayload) => dispatch(NotificationsStore.createEnqueueNotificationAction(payload));

    const enqueueError = (payload: EnqueuePayload) =>
        enqueue({ ...payload, type: NotificationsStore.NotificationType.ERROR });

    const enqueueSuccess = (payload: EnqueuePayload) =>
        enqueue({ ...payload, type: NotificationsStore.NotificationType.SUCCESS });

    const dequeue = () => dispatch(NotificationsStore.actions.dequeue({}));

    const remove = (payload: NotificationsStore.RemoveNotificationPayload) =>
        dispatch(NotificationsStore.actions.remove(payload));

    return {
        notifications,
        actions: {
            enqueue,
            enqueueError,
            enqueueSuccess,
            dequeue,
            remove,
        },
    };
}
