import { useDispatch } from 'react-redux';

import { PayloadAction } from '@reduxjs/toolkit';
import { useAppSelector } from '../store';

import * as OrderLocationStore from '../orderLocation';
import * as featureFlags from '../../lib/getFeatureFlags';
import useLocationAvailableTimes from '../../common/hooks/useLocationAvailableTimes';
import { OrderLocationMethod } from '../orderLocation';
import { selectCurrenOrderLocation, selectIsCurrentLocationOAAvailable } from '../selectors/orderLocation';
import { LocationWithDetailsModel } from '../../common/services/locationService/types';

interface UseOrderLocationHook {
    isPickUp: boolean;
    method: OrderLocationStore.OrderLocationMethod;
    pickupAddress: OrderLocationStore.PickupAddress;
    deliveryAddress: OrderLocationStore.DeliveryAddress;
    currentLocation: LocationWithDetailsModel;
    isCurrentLocationOAAvailable: boolean;
    pickupLocationTimeSlots: OrderLocationStore.LocationAvailableTimes;
    deliveryLocationTimeSlots: OrderLocationStore.LocationAvailableTimes;
    previousLocation: OrderLocationStore.PreviousLocation;
    actions: {
        setPickupLocation: (
            payload: OrderLocationStore.PickupAddress
        ) => PayloadAction<OrderLocationStore.PickupAddress>;
        setDeliveryLocation: (
            payload: OrderLocationStore.DeliveryAddress
        ) => PayloadAction<OrderLocationStore.DeliveryAddress>;
        setPickupLocationAvailableTimeSlots: (
            payload: OrderLocationStore.LocationAvailableTimes
        ) => PayloadAction<OrderLocationStore.LocationAvailableTimes>;
        setDeliveryLocationAvailableTimeSlots: (
            payload: OrderLocationStore.LocationAvailableTimes
        ) => PayloadAction<OrderLocationStore.LocationAvailableTimes>;
        flushSelectedLocation: () => PayloadAction;
        flushDeliveryLocation: () => void;
        setMethod: (
            payload: OrderLocationStore.OrderLocationMethod
        ) => PayloadAction<OrderLocationStore.OrderLocationMethod>;
        setPreviousLocation: (
            payload: OrderLocationStore.PreviousLocation
        ) => PayloadAction<OrderLocationStore.PreviousLocation>;
    };
}

export default function useOrderLocation(): UseOrderLocationHook {
    const dispatch = useDispatch();

    const {
        method,
        pickupAddress,
        deliveryAddress,
        pickupLocationTimeSlots,
        deliveryLocationTimeSlots,
        previousLocation,
    } = useAppSelector((state) => state.orderLocation);

    const { getLocationAvailableTimeSlots } = useLocationAvailableTimes();

    const currentLocation = useAppSelector(selectCurrenOrderLocation);
    const isCurrentLocationOAAvailable = useAppSelector(selectIsCurrentLocationOAAvailable);

    const fetchLocationTimeSlots = async (locationId, setAvailableTimeSlots) => {
        if (featureFlags.locationTimeSlotsEnabled() && locationId) {
            const availableTimeSlots = await getLocationAvailableTimeSlots({
                locationId,
            }).catch(() => setAvailableTimeSlots(null));

            setAvailableTimeSlots(availableTimeSlots);
        }
    };

    const setPickupLocation = (payload: OrderLocationStore.PickupAddress) => {
        fetchLocationTimeSlots(payload.id, setPickupLocationAvailableTimeSlots);

        return dispatch(OrderLocationStore.actions.setPickupLocation(payload));
    };

    const setDeliveryLocation = (payload: OrderLocationStore.DeliveryAddress) => {
        fetchLocationTimeSlots(payload.pickUpLocation.id, setDeliveryLocationAvailableTimeSlots);

        return dispatch(OrderLocationStore.actions.setDeliveryLocation(payload));
    };

    const flushSelectedLocation = () => dispatch(OrderLocationStore.actions.flushSelectedLocation());

    const flushDeliveryLocation = () => dispatch(OrderLocationStore.actions.flushDeliveryLocation());

    const setPickupLocationAvailableTimeSlots = (payload: OrderLocationStore.LocationAvailableTimes) =>
        dispatch(OrderLocationStore.actions.setPickupLocationAvailableTimeSlots(payload));

    const setDeliveryLocationAvailableTimeSlots = (payload: OrderLocationStore.LocationAvailableTimes) =>
        dispatch(OrderLocationStore.actions.setDeliveryLocationAvailableTimeSlots(payload));

    const setMethod = (payload: OrderLocationStore.OrderLocationMethod) =>
        dispatch(OrderLocationStore.actions.setMethod(payload));

    const setPreviousLocation = (payload: OrderLocationStore.PreviousLocation) =>
        dispatch(OrderLocationStore.actions.setPreviousLocation(payload));

    const isPickUp = method === OrderLocationMethod.PICKUP;

    return {
        isPickUp,
        method,
        pickupAddress,
        deliveryAddress,
        pickupLocationTimeSlots,
        deliveryLocationTimeSlots,
        currentLocation,
        isCurrentLocationOAAvailable,
        previousLocation,
        actions: {
            setPickupLocation,
            setDeliveryLocation,
            flushSelectedLocation,
            flushDeliveryLocation,
            setPickupLocationAvailableTimeSlots,
            setDeliveryLocationAvailableTimeSlots,
            setMethod,
            setPreviousLocation,
        },
    };
}
