import { IGlobalContentfulProps } from '../../common/services/globalContentfulProps';
import contentfulData from '../../contentfulData.json';

export default function useGlobalProps(): IGlobalContentfulProps | null {
    return (contentfulData as unknown) as IGlobalContentfulProps;
}
