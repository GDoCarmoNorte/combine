import globalProps from '../../../tests/mocks/globalProps.mock';
import { IGlobalContentfulProps } from '../../../common/services/globalContentfulProps';

export default function useGlobalProps(): IGlobalContentfulProps | null {
    return (globalProps as unknown) as IGlobalContentfulProps;
}
