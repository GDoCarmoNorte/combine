import { shallowEqual, useSelector } from 'react-redux';
import { PayloadAction } from '@reduxjs/toolkit';

import orderService from '../../common/services/orderService/orderService';

import { ICustomerInfo } from '../../components/organisms/checkout/customerInfo';

import { RootState, useAppDispatch } from '../store';

import { actions, IFullfilledOrderPayload, ISubmitOrderState } from '../submitOrder';
import { createSubmitOrderRequest } from '../../common/helpers/submitOrderHelper';
import { IDeliveryLocation, LocationWithDetailsModel } from '../../common/services/locationService/types';
import { IPaymentRequest } from '../../components/clientOnly/paymentInfoContainer/paymentInfo';
import { IPaymentState } from '../../components/clientOnly/paymentInfoContainer/paymentTypeDropdown/types';
import {
    TallyResponseModel,
    OrderRequest,
    OrderResponseModel,
    ITallyError500ExternalResponseModel,
    TTallyErrorCodeModel,
    TErrorCodeModel,
} from '../../@generated/webExpApi';
import { IGiftCard } from '../../components/clientOnly/paymentInfoContainer/types';
import { GTM_ERROR_EVENT } from '../../common/services/gtmService/constants';
import { GtmErrorCategory } from '../../common/services/gtmService/types';
import { getTallyError } from '../../common/helpers/getTallyError';

export interface IOrderPayload {
    tallyOrder: TallyResponseModel;
    customerInfo: ICustomerInfo;
    customerId?: string;
    paymentRequest?: IPaymentRequest;
    paymentInfo: IPaymentState;
    giftCards?: IGiftCard[];
    location: LocationWithDetailsModel;
    orderTime: Date;
    instructions?: string[];
    deliveryLocation?: IDeliveryLocation;
    driverTip?: number;
    serverTip?: number;
    deviceIdentifier?: string;
}
export interface ISubmitOrderHook {
    error: Error;
    isLoading: boolean;
    lastOrder: OrderResponseModel;
    lastRequest: OrderRequest;
    unavailableItems: string[] | null;
    isShowAlertModal: boolean;
    submitOrder: (order: IOrderPayload) => Promise<PayloadAction<IFullfilledOrderPayload>>;
    resetSubmitOrder: () => void;
    hideAlertModal: () => void;
    setUnavailableItems: (payload: string[]) => void;
}

const useSubmitOrder = (): ISubmitOrderHook => {
    const dispatch = useAppDispatch();
    const { error, isLoading, lastOrder, lastRequest, unavailableItems, isShowAlertModal } = useSelector<
        RootState,
        ISubmitOrderState
    >((state) => state.submitOrder, shallowEqual);

    const submitOrder = (order: IOrderPayload): Promise<PayloadAction<IFullfilledOrderPayload>> => {
        dispatch(actions.pending(null));

        const submitOrderRequest = createSubmitOrderRequest(order);
        return orderService
            .createOrder(submitOrderRequest)
            .then((response) => {
                const isSuccessPayload = (
                    response: OrderResponseModel | ITallyError500ExternalResponseModel
                ): response is OrderResponseModel => {
                    return 'requestStatus' in response;
                };

                if (isSuccessPayload(response)) {
                    return dispatch(actions.fulfilled({ response, request: submitOrderRequest }));
                }

                if (response.code === TTallyErrorCodeModel.ProductsNotAvailable) {
                    return dispatch(actions.setUnavailableItems(response.data.productIds));
                }

                const errorsMap = {
                    [TErrorCodeModel.DeliveryOrderInvalidPhone]:
                        'We apologize. Our delivery partner does not recognize your phone number. Please use a valid phone number for this delivery order.',
                    [TErrorCodeModel.PaymentFailure]: `Our payment system is unavailable. To continue placing your order, please contact us at ${order.location.contactDetails.phone}.`,
                    [TErrorCodeModel.PaymentAmountLower]:
                        'The payment amount does not match the total order amount. Please review the payments methods amounts and try again.',
                    [TErrorCodeModel.PaymentAmountHigher]:
                        'The payment amount does not match the total order amount. Please review the payments methods amounts and try again.',
                    [TErrorCodeModel.PaymentNotValid]:
                        'The selected payment method(s) are not valid. Please select different payment method(s) and try again.',
                };

                const errorMessage = errorsMap[response.code as TErrorCodeModel];

                if (errorMessage) {
                    return dispatch(actions.rejected(new Error(errorMessage)));
                }

                const tallyError = getTallyError(response, order.location.contactDetails.phone);

                return dispatch(actions.rejected(new Error(tallyError.message as string)));
            })
            .catch((e) => {
                const payload = {
                    ErrorCategory: GtmErrorCategory.CHECKOUT_SUBMIT,
                    ErrorDescription: e.message,
                };
                dispatch({ type: GTM_ERROR_EVENT, payload });
                return dispatch(actions.rejected(e));
            });
    };

    const resetSubmitOrder = (): void => {
        dispatch(actions.reset(null));
    };

    const hideAlertModal = (): void => {
        dispatch(actions.hideAlertModal(null));
    };

    const setUnavailableItems = (payload: string[]) => {
        dispatch(actions.setUnavailableItems(payload));
    };

    return {
        error,
        isLoading,
        lastOrder,
        lastRequest,
        unavailableItems,
        isShowAlertModal,
        submitOrder,
        resetSubmitOrder,
        hideAlertModal,
        setUnavailableItems,
    };
};

export default useSubmitOrder;
