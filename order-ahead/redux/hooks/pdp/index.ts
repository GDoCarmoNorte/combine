import {
    selectAboutSectionInfo,
    selectBwwDisplayProduct,
    selectBwwDisplayModifierGroup,
    selectBwwSauceDisplayModifierGroup,
    selectDisplayModifierGroupsByProductIdAndProductGroupId,
    selectDisplayProduct,
    selectExtrasDisplayProducts,
    selectMainComboDisplayProductById,
    selectPDPTallyItem,
    selectBwwDisplayModifiersGroups,
    selectBwwSelectionsText,
    selectSelectedExtras,
} from '../../selectors/pdp';
import { useAppSelector } from '../../store';
import {
    IAboutSectionInfo,
    IDisplayFullProduct,
    IDisplayModifierGroup,
    IDisplayProduct,
    IExtraProductGroup,
    ISaucesDisplayModifierGroup,
    ISelectedExtraItem,
} from '../../types';
import { getCheapestPriceFromProduct } from '../../../lib/domainProduct';
import { useDispatch } from 'react-redux';
import * as PDPStore from '../../pdp';
import { selectDefaultTallyItem, selectProducts } from '../../selectors/domainMenu';
import { useDefaultModifiers, useSelectedModifiers } from '../domainMenu';
import { getChangedModifiers, IGetChangedModifiers } from '../../../common/helpers/getChangedModifiers';
import { useOrderLocation } from '..';

export const useDisplayProduct = (): IDisplayFullProduct => useAppSelector(selectDisplayProduct);

export const useBwwDisplayProduct = (): IDisplayFullProduct => useAppSelector(selectBwwDisplayProduct);

export const useBwwDisplayModifierGroup = (
    productGroupId: string,
    parentProductGroupId?: string,
    parentId?: string
): IDisplayModifierGroup =>
    useAppSelector((state) => {
        const tallyItem = selectPDPTallyItem(state);
        return selectBwwDisplayModifierGroup(state, tallyItem, productGroupId, parentProductGroupId, parentId);
    });

export const useBwwSaucesDisplayModifierGroup = (productGroupId: string): ISaucesDisplayModifierGroup =>
    useAppSelector((state) => selectBwwSauceDisplayModifierGroup(state, productGroupId));

export const useBwwDisplayModifierGroupsForModifier = (
    modifierGroupId: string,
    modifierId: string
): IDisplayModifierGroup[] =>
    useAppSelector((state) => {
        const tallyItem = selectPDPTallyItem(state);
        return selectBwwDisplayModifiersGroups(state, tallyItem, modifierGroupId, modifierId);
    });

export const useBwwDisplayModifierGroupsForExtraItem = (
    productId: string,
    sectionIndex: number
): IDisplayModifierGroup[] =>
    useAppSelector((state) => {
        const tallyItem = selectPDPTallyItem(state);
        const extrasTallyItem = tallyItem.childExtras?.[sectionIndex] || selectDefaultTallyItem(state, productId);

        return selectBwwDisplayModifiersGroups(state, extrasTallyItem);
    });

export const useBwwSelectionsText = (pdpTallyItem: PDPStore.PDPTallyItem, productItemGroupName: string): string =>
    useAppSelector((state) => selectBwwSelectionsText(state, pdpTallyItem, productItemGroupName));

export const useDisplayModifierGroupsByProductId = (
    productId: string,
    productGroupId: string,
    sectionIndex: number
): IDisplayModifierGroup[] =>
    useAppSelector((state) =>
        selectDisplayModifierGroupsByProductIdAndProductGroupId(state, productId, productGroupId, sectionIndex)
    );

export const useMainComboDisplayProductByProductId = (productId: string): IDisplayProduct =>
    useAppSelector((state) => selectMainComboDisplayProductById(state, productId));

interface ISelectedItem {
    quantity: number;
}

interface ISelectedItems {
    [id: string]: ISelectedItem;
}

export const useProductTallyModifierGroup = (
    productGroupId: string,
    childProductId?: string,
    sectionIndex?: number
): {
    setSelectedItems: (selectedItems: ISelectedItems) => void;
    tallyModifierGroup: PDPStore.PDPTallyItemModifierGroup;
    totalCount: number;
    selectedItems: ISelectedItems;
} => {
    const dispatch = useDispatch();
    const rootTallyItem = useAppSelector(selectPDPTallyItem);
    const products = useAppSelector(selectProducts);
    const { currentLocation } = useOrderLocation();

    // Use root if not combo or child if combo
    const childItem = rootTallyItem?.childItems?.find(
        (childItem, index) => childItem.productId === childProductId && index === sectionIndex
    );

    const childExtrasItem = rootTallyItem?.childExtras?.find(
        (childItem, index) => childItem?.productId === childProductId && index === sectionIndex
    );

    const pdpTallyItem = childProductId ? childItem || childExtrasItem : rootTallyItem;

    const tallyModifierGroup = pdpTallyItem?.modifierGroups?.find(
        (modifierGroup) => modifierGroup.productId === productGroupId
    );

    const selectedItems: ISelectedItems = tallyModifierGroup?.modifiers?.reduce((selected, mod) => {
        return { ...selected, [mod.productId]: { quantity: mod.quantity || 0 } };
    }, {});

    const setSelectedItems = (selectedItems: ISelectedItems) => {
        const idList = Object.keys(selectedItems).map((id) => id);
        const selectedModifiers = idList
            .map((id) => products[id])
            ?.reduce((dict, product) => {
                return { ...dict, [product?.id]: product };
            }, {});

        const newModifierGroup: PDPStore.PDPTallyItemModifierGroup = {
            isOnSideChecked: tallyModifierGroup.isOnSideChecked,
            metadata: tallyModifierGroup.metadata,
            productId: productGroupId,
            modifiers: idList.map((id) => ({
                productId: id,
                quantity: selectedItems[id].quantity,
                price: getCheapestPriceFromProduct(selectedModifiers[id], currentLocation), // TODO double check with menu team
            })),
        };
        const newModifierGroups = pdpTallyItem?.modifierGroups?.map((mg) =>
            mg.productId === productGroupId ? newModifierGroup : mg
        );

        const newTallyItemState: PDPStore.PDPTallyItem = {
            ...rootTallyItem,
            modifierGroups: childProductId ? rootTallyItem.modifierGroups : newModifierGroups, // Set root mod group if not combo
            childItems:
                rootTallyItem.childItems &&
                rootTallyItem.childItems.map(
                    (child, index) =>
                        child.productId === childProductId && index === sectionIndex
                            ? { ...child, modifierGroups: newModifierGroups }
                            : child // Set child mod group if combo
                ),
            childExtras:
                rootTallyItem.childExtras &&
                rootTallyItem.childExtras.map(
                    (child, index) =>
                        child?.productId === childProductId && index === sectionIndex
                            ? {
                                  ...child,
                                  modifierGroups: child?.modifierGroups?.map((mg) =>
                                      mg.productId === productGroupId ? newModifierGroup : mg
                                  ),
                              }
                            : child // Set child mod group if combo
                ),
        };

        dispatch(PDPStore.actions.editTallyItemModifiers({ pdpTallyItem: newTallyItemState }));
    };

    const totalCount = tallyModifierGroup?.modifiers?.reduce((res, modifier) => {
        return res + modifier.quantity || 0;
    }, 0);

    return {
        tallyModifierGroup,
        totalCount,
        selectedItems,
        setSelectedItems,
    };
};

export const useAboutSection = (): IAboutSectionInfo | null => useAppSelector(selectAboutSectionInfo);

export const useChangedModifiersForChildItem = (childItemIndex: number, isExtras?: boolean): IGetChangedModifiers => {
    const pdpTallyItem = useAppSelector(selectPDPTallyItem);
    const tallyItem = isExtras ? pdpTallyItem.childExtras[childItemIndex] : pdpTallyItem.childItems[childItemIndex];
    const selectedModifiers = useSelectedModifiers(tallyItem);
    const defaultModifiers = useDefaultModifiers(tallyItem?.productId);

    return getChangedModifiers(selectedModifiers, defaultModifiers);
};

export const useExtraProducts = (): IExtraProductGroup[] => useAppSelector(selectExtrasDisplayProducts);

export const useSelectedExtras = (): ISelectedExtraItem[] => useAppSelector(selectSelectedExtras);
