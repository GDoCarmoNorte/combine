import { useDispatch, useSelector } from 'react-redux';
import { RootState, useAppSelector } from '../store';
import * as RewardsStore from '../rewards';

import { PayloadAction } from '@reduxjs/toolkit';
import {
    selectCertificates,
    selectCertificatesSortedByStatus,
    selectFirstExpiringOffer,
    selectInactiveOffers,
    selectLastPurchasedCertificate,
    selectOffers,
    selectRewardsActivityHistory,
    selectRewardsCatalog,
    selectRewardsRecommendations,
} from '../selectors/rewards';
import {
    ICertificateModel,
    IInactiveOfferModel,
    IOfferModel,
    IPurchaseCustomerAccountRewardResponseModel,
    TOffersUnionModel,
} from '../../@generated/webExpApi';

export const isOfferModel = (offer: IOfferModel | IInactiveOfferModel): offer is IOfferModel =>
    'isRedeemableInStoreOnly' in offer && 'userOfferId' in offer && typeof offer.userOfferId !== 'undefined';

interface IUseRewards {
    rewards: RewardsStore.IRewards;
    offers: RewardsStore.Offers;
    certificates: RewardsStore.Certificates;
    certificatesSortedByStatus: RewardsStore.Certificates;
    lastPurchasedCertificate: RewardsStore.LastPurchasedCertificate;
    rewardsCatalog: RewardsStore.IRewardsCatalog;
    rewardsRecommendations: RewardsStore.TRewardsRecommendations;
    firstExpiringOffer?: IOfferModel;
    getOfferById: (offerId: string) => TOffersUnionModel;
    getCertificateById: (certificateId: string) => ICertificateModel;
    totalCount: number;
    isLoading: boolean;
    isCatalogLoading: boolean;
    inactiveOffers: RewardsStore.InactiveOffers;
    rewardsActivityHistory: RewardsStore.IRewardsActivityHistory;
    rewardsActivityHistoryLoading: boolean;
    actions: {
        setRewards: (payload: RewardsStore.IRewards) => PayloadAction<RewardsStore.IRewardsState>;
        setRewardsCatalog: (payload: RewardsStore.IRewardsCatalog) => PayloadAction<RewardsStore.IRewardsCatalog>;
        setRewardsLoading: (payload: boolean) => void;
        setRewardStatus: (payload: RewardsStore.ISetRewardStatusPayload) => void;
        setRewardsCatalogLoading: (payload: boolean) => void;
        setRewardsRecommendations: (
            payload: RewardsStore.TRewardsRecommendations
        ) => PayloadAction<RewardsStore.TRewardsRecommendations>;
        setRewardsActivityHistory: (
            payload: RewardsStore.IRewardsActivityHistory
        ) => PayloadAction<RewardsStore.IRewardsActivityHistory>;
        setRewardsActivityHistoryLoading: (payload: boolean) => void;
        setLastPurchasedCertificate: (
            payload: RewardsStore.LastPurchasedCertificate
        ) => PayloadAction<RewardsStore.LastPurchasedCertificate>;
    };
}

export default function UseRewards(): IUseRewards {
    const dispatch = useDispatch();

    const rewards = useSelector<RootState, RewardsStore.IRewardsState>((state) => state.rewards);
    const offers = useAppSelector(selectOffers);
    const firstExpiringOffer = useAppSelector(selectFirstExpiringOffer);
    const certificates = useAppSelector(selectCertificates);
    const certificatesSortedByStatus = useAppSelector(selectCertificatesSortedByStatus);
    const lastPurchasedCertificate = useAppSelector(selectLastPurchasedCertificate);
    const rewardsCatalog = useAppSelector(selectRewardsCatalog);
    const rewardsRecommendations = useAppSelector(selectRewardsRecommendations);
    const inactiveOffers = useAppSelector(selectInactiveOffers);
    const rewardsActivityHistory = useAppSelector(selectRewardsActivityHistory);

    const setLastPurchasedCertificate = (payload: IPurchaseCustomerAccountRewardResponseModel) =>
        dispatch(RewardsStore.actions.setLastPurchasedCertificate(payload));
    const setRewards = (payload: RewardsStore.IRewardsState) => dispatch(RewardsStore.actions.setRewards(payload));
    const setRewardsCatalog = (payload: RewardsStore.IRewardsCatalog) =>
        dispatch(RewardsStore.actions.setRewardsCatalog(payload));
    const setRewardsRecommendations = (payload: RewardsStore.TRewardsRecommendations) =>
        dispatch(RewardsStore.actions.setRewardsRecommendations(payload));
    const setRewardsLoading = (payload: boolean) => dispatch(RewardsStore.actions.setRewardsLoading(payload));
    const setRewardsCatalogLoading = (payload: boolean) =>
        dispatch(RewardsStore.actions.setRewardsCatalogLoading(payload));

    const getOfferById = (userOfferId: string) => {
        return offers.find((item) => item.userOfferId === userOfferId);
    };

    const getCertificateById = (certificateId: string) => {
        return certificates.find((item) => item.number === certificateId);
    };

    const setRewardStatus = (payload: RewardsStore.ISetRewardStatusPayload) =>
        dispatch(RewardsStore.actions.setRewardStatus(payload));

    const setRewardsActivityHistory = (payload: RewardsStore.IRewardsActivityHistory) =>
        dispatch(RewardsStore.actions.setRewardsActivityHistory(payload));

    const setRewardsActivityHistoryLoading = (payload: boolean) =>
        dispatch(RewardsStore.actions.setRewardsActivityHistoryLoading(payload));

    return {
        rewards,
        offers,
        certificates,
        certificatesSortedByStatus,
        lastPurchasedCertificate,
        rewardsCatalog,
        rewardsRecommendations,
        firstExpiringOffer,
        getOfferById,
        getCertificateById,
        isCatalogLoading: rewards.rewardsCatalogLoading,
        isLoading: rewards.loading,
        totalCount: rewards.totalCount,
        inactiveOffers,
        rewardsActivityHistory,
        rewardsActivityHistoryLoading: rewards.rewardsActivityHistoryLoading,
        actions: {
            setRewards,
            setRewardsLoading,
            setRewardStatus,
            setRewardsCatalogLoading,
            setRewardsCatalog,
            setRewardsRecommendations,
            setRewardsActivityHistory,
            setRewardsActivityHistoryLoading,
            setLastPurchasedCertificate,
        },
    };
}
