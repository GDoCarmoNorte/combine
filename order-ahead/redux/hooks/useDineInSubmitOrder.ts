import { useState } from 'react';
import { IDineInOrderDetailsModel } from '../../@generated/webExpApi';
import { createDineInSubmitOrderRequest } from '../../common/helpers/submitOrderHelper';
import { submitDineInOrder } from '../../common/services/orderService/dineInService';
import { IPaymentRequest } from '../../components/clientOnly/paymentInfoContainer/paymentInfo';
import { IGiftCard } from '../../components/clientOnly/paymentInfoContainer/types';

export interface IDineInOrderDetailsWithOrderId extends IDineInOrderDetailsModel {
    orderId: string;
}

export interface IDineInOrderPayload {
    orderInfo: IDineInOrderDetailsWithOrderId;
    paymentRequest?: IPaymentRequest;
    giftCards?: IGiftCard[];
    deviceId?: string;
    tipAmount?: number;
}

export const useDineInSubmitOrder = () => {
    const [loading, setLoading] = useState(false);

    const submitOrder = async (order: IDineInOrderPayload) => {
        setLoading(true);

        try {
            return await submitDineInOrder(createDineInSubmitOrderRequest(order));
        } finally {
            setLoading(false);
        }
    };

    return {
        submitOrder,
        loading,
    };
};
