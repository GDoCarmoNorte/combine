import { useState } from 'react';
import { ITallyError500ExternalResponseModel } from '../../@generated/webExpApi';
import { IDineInOrderDetailsModel } from '../../@generated/webExpApi/models/IDineInOrderDetailsModel';
import { getDineInOrderDetails } from '../../common/services/orderService/dineInService';

export const useDineInOrder = () => {
    const [loading, setLoading] = useState(false);
    const [orderLocationId, setOrderLocationId] = useState<string>(null);
    const [error, setError] = useState(null);
    const [order, setOrder] = useState<IDineInOrderDetailsModel>(null);

    const getOrder = ({ orderId, locationId }) => {
        setOrderLocationId(locationId);
        setError(null);
        setLoading(true);

        const isSuccessPayload = (
            response: IDineInOrderDetailsModel | ITallyError500ExternalResponseModel
        ): response is IDineInOrderDetailsModel => {
            return 'products' in response;
        };

        getDineInOrderDetails({ orderId, locationId })
            .then((data) => {
                if (isSuccessPayload(data)) {
                    return setOrder(data);
                }

                setError(data);
            })
            .catch((err) => {
                setError(err);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    return {
        getOrder,
        loading,
        orderLocationId,
        error,
        order,
    };
};
