import { useSelector } from 'react-redux';
import { PayloadAction } from '@reduxjs/toolkit';
import { RootState, useAppDispatch, useAppSelector } from '../store';
import * as BagStore from '../bag';
import * as PdpStore from '../pdp';
import * as JustAddedToBagStore from '../justAddedToBag';
import { isInRange } from '../../common/helpers/checkoutHelpers';
import { findItemIndexInBag } from '../../common/helpers/bagHelper';
import {
    selectLineItems,
    selectBagEntriesCount,
    selectBagEntryForPDPTally,
    selectDeal,
    generateNextLineItemId,
    selectOrderTimeAndType,
} from '../selectors/bag';
import { selectDefaultTallyItem, selectProductById, selectTopCategory } from '../selectors/domainMenu';
import { selectTallyUpdateBagItems } from '../selectors/tally';
import { TallyProductModel } from '../../@generated/webExpApi';

export interface UseBagHook {
    bagEntries: TallyProductModel[];
    dealId: string;
    entriesMarkedAsRemoved: number[];
    isOpen: boolean;
    tooltipOpen: boolean;
    entryCreated: boolean;
    entryUpdated: boolean;
    entryCreatedWithDeal: boolean;
    entryUpdatedWithDeal: boolean;
    bagEntriesCount: number;
    justAddedToBag: JustAddedToBagStore.justAddedToBagPayload;
    orderTime: string;
    orderTimeType: BagStore.OrderTimeType;
    pickupTime: string;
    pickupTimeType: BagStore.OrderTimeType;
    deliveryTime: string;
    deliveryTimeType: BagStore.OrderTimeType;
    pickupTimeValues: BagStore.WorkingHours;
    pickupTimeIsValid: boolean;
    lastRemovedLineItemId: number;
    lastEdit: number;
    actions: {
        toggleIsOpen: (payload: BagStore.ToggleIsOpenPayload) => PayloadAction<BagStore.ToggleIsOpenPayload>;
        updateTooltip: (payload: BagStore.UpdateTooltipPayload) => PayloadAction<BagStore.UpdateTooltipPayload>;
        setJustAddedToBag: (payload: JustAddedToBagStore.justAddedToBagPayload) => void;
        editBagLineItem: (payload: BagStore.EditBagLineItem) => PayloadAction<BagStore.EditBagLineItem>;
        addDefaultToBag: (
            payload: BagStore.AddToBagWithDefaultsPayload | BagStore.AddToBagWithDefaultsPayload[]
        ) => void;
        markAsRemoved: (payload: BagStore.MarkAsRemovedPayload) => PayloadAction<BagStore.MarkAsRemovedPayload>;
        removeFromBag: (payload: BagStore.RemoveFromBagPayload) => PayloadAction<BagStore.RemoveFromBagPayload>;
        removeAllFromBag: (
            payload: BagStore.RemoveAllFromBagPayload
        ) => PayloadAction<BagStore.RemoveAllFromBagPayload>;
        clearBag: () => PayloadAction<BagStore.ClearBagPayload>;
        updateBagItems: () => PayloadAction<BagStore.UpdateBagItemsPayload>;
        updateBagItemCount: (
            payload: BagStore.UpdateBagItemCountPayload
        ) => PayloadAction<BagStore.UpdateBagItemCountPayload>;
        putToBag: (payload: BagStore.PutToBagPayload) => void;
        addDealToBag: (payload: BagStore.AddDealToBagPayload) => PayloadAction<BagStore.AddDealToBagPayload>;
        removeDealFromBag: () => void;
        clearLastRemovedLineItemId: () => void;
        setPickupTime: (payload: BagStore.SetPickupTimePayload) => PayloadAction<BagStore.SetPickupTimePayload>;
        resetOrderTime: () => void;
        resetPickupTime: () => void;
        resetDeliveryTime: () => void;
        initPickupTimeValues: (
            payload: BagStore.InitPickupTimeValuesPayload
        ) => PayloadAction<BagStore.InitPickupTimeValuesPayload>;
        setPickupTimeValues: (
            payload: BagStore.SetPickupTimeValuesPayload
        ) => PayloadAction<BagStore.SetPickupTimeValuesPayload>;
        setLastEdit: (payload: number) => PayloadAction<number>;
        clearLastEdit: () => void;
    };
}

export default function useBag(): UseBagHook {
    const dispatch = useAppDispatch();

    const bagEntries = useAppSelector(selectLineItems);
    const entriesMarkedAsRemoved = useSelector<RootState, number[]>((state) => state?.bag?.markedAsRemoved);
    const rootState = useSelector<RootState, RootState>((state) => state);

    const isOpen = useSelector<RootState, boolean>((state) => state?.bag?.isOpen);
    const tooltipOpen = useSelector<RootState, boolean>((state) => state?.bag?.tooltipOpen);
    const entryCreated = useSelector<RootState, boolean>((state) => state?.bag?.entryCreated);
    const entryUpdated = useSelector<RootState, boolean>((state) => state?.bag?.entryUpdated);
    const entryCreatedWithDeal = useSelector<RootState, boolean>((state) => state?.bag?.entryCreatedWithDeal);
    const entryUpdatedWithDeal = useSelector<RootState, boolean>((state) => state?.bag?.entryUpdatedWithDeal);
    const lastRemovedLineItemId = useSelector<RootState, number>((state) => state?.bag?.lastRemovedLineItemId);
    const lastEdit = useSelector<RootState, number>((state) => state?.bag?.lastEdit);
    const dealId = useAppSelector(selectDeal);

    const justAddedToBag = useSelector<RootState, JustAddedToBagStore.justAddedToBagPayload>(
        (state) => state.justAddedToBag
    );
    const bagEntriesCount = useAppSelector(selectBagEntriesCount);

    const { orderTime, orderTimeType } = useAppSelector(selectOrderTimeAndType) || {};
    const pickupTimeValues = useSelector<RootState, BagStore.WorkingHours>((state) => state.bag.pickupTimeValues);
    const pickupTime = useSelector<RootState, string | null>((state) => state.bag.pickupTime);
    const pickupTimeType = useSelector<RootState, BagStore.OrderTimeType>((state) => state.bag.pickupTimeType);
    const deliveryTime = useSelector<RootState, string | null>((state) => state.bag.deliveryTime);
    const deliveryTimeType = useSelector<RootState, BagStore.OrderTimeType>((state) => state.bag.deliveryTimeType);

    const pickupTimeIsValid = pickupTimeValues && orderTime ? isInRange(orderTime, pickupTimeValues) : false;

    const addDealToBag = (payload: BagStore.AddDealToBagPayload) => {
        return dispatch(BagStore.actions.addDealToBag(payload));
    };

    const removeDealFromBag = () => {
        return dispatch(BagStore.actions.removeDealFromBag());
    };

    const clearLastRemovedLineItemId = () => {
        return dispatch(BagStore.actions.clearLastRemovedLineItemId());
    };

    const addDefaultToBag = (
        payloadArg: BagStore.AddToBagWithDefaultsPayload | BagStore.AddToBagWithDefaultsPayload[]
    ) => {
        dispatch(BagStore.actions.updateTooltip({ tooltipIsOpen: true, created: true }));

        const payloadArgArray = !Array.isArray(payloadArg) ? [payloadArg] : payloadArg;

        payloadArgArray.forEach((payload) => {
            const bagEntry = selectDefaultTallyItem(rootState, payload.productId);
            const itemIndexInBag = findItemIndexInBag(bagEntries, bagEntry);

            if (itemIndexInBag > -1) {
                const targetItemInBag = bagEntries?.find((entry) => entry.productId === bagEntry.productId);
                if (isOpen && targetItemInBag && entriesMarkedAsRemoved.includes(targetItemInBag.lineItemId)) {
                    return dispatch(BagStore.actions.markAsRemoved({ lineItemId: targetItemInBag.lineItemId }));
                }
                return dispatch(
                    BagStore.actions.updateBagItemCount({
                        bagEntryIndex: itemIndexInBag,
                        value: bagEntry.quantity,
                    })
                );
            }

            return dispatch(
                BagStore.actions.addToBag({
                    name: payload.name,
                    category: payload.category,
                    udpRecommendationId: payload.recommendationId,
                    bagEntry: selectBagEntryForPDPTally(rootState, bagEntry),
                })
            );
        });
    };

    const editBagLineItem = (payload: BagStore.EditBagLineItem) => dispatch(BagStore.actions.editBagLineItem(payload));

    const setJustAddedToBag = (payload: JustAddedToBagStore.justAddedToBagPayload) =>
        dispatch(JustAddedToBagStore.actions.setJustAddedToBag(payload));

    const markAsRemoved = (payload: BagStore.MarkAsRemovedPayload) => dispatch(BagStore.actions.markAsRemoved(payload));
    const removeFromBag = (payload: BagStore.RemoveFromBagPayload) => dispatch(BagStore.actions.removeFromBag(payload));
    const removeAllFromBag = (payload: BagStore.RemoveAllFromBagPayload) =>
        dispatch(BagStore.actions.removeAllFromBag(payload));

    const clearBag = () => dispatch(BagStore.actions.clearBag());

    const updateBagItemCount = (payload: BagStore.UpdateBagItemCountPayload) =>
        dispatch(BagStore.actions.updateBagItemCount(payload));

    const toggleIsOpen = (payload: BagStore.ToggleIsOpenPayload) => {
        // when bag is closed remove items that are marked as removed and remove intermediate state for PDP pages
        if (!payload.isOpen) {
            entriesMarkedAsRemoved.forEach((entry) => {
                dispatch(PdpStore.actions.resetPdpState());
                dispatch(BagStore.actions.removeFromBag({ lineItemId: entry }));
                dispatch(BagStore.actions.markAsRemoved({ lineItemId: entry }));
            });
        }

        return dispatch(BagStore.actions.toggleIsOpen(payload));
    };

    const updateTooltip = (payload: BagStore.UpdateTooltipPayload) => dispatch(BagStore.actions.updateTooltip(payload));

    const setLastEdit = (payload: number) => dispatch(BagStore.actions.setLastEdit(payload));
    const clearLastEdit = () => dispatch(BagStore.actions.clearLastEdit());

    const putToBag = (payload: BagStore.PutToBagPayload) => {
        const { pdpTallyItem } = payload;
        const targetItemInBag = bagEntries.find((entry) => entry.lineItemId === pdpTallyItem.lineItemId);

        const targetItemInBagIndex = targetItemInBag ? findItemIndexInBag(bagEntries, targetItemInBag) : -1;
        const sameItemInBagIndex = findItemIndexInBag(bagEntries, pdpTallyItem);

        const tallyItemWithoutExtras = {
            ...pdpTallyItem,
            childExtras: [],
        };

        // Is the target item in the bag
        if (sameItemInBagIndex > -1 || targetItemInBagIndex > -1) {
            if (sameItemInBagIndex === targetItemInBagIndex) {
                // Do nothing if no changes applied
                dispatch(BagStore.actions.updateTooltip({ tooltipIsOpen: true, updated: true }));
            } else if (sameItemInBagIndex > -1 && targetItemInBagIndex > -1) {
                // Items consolidation
                dispatch(BagStore.actions.updateTooltip({ tooltipIsOpen: true, updated: true }));

                dispatch(BagStore.actions.removeFromBag({ lineItemId: targetItemInBag.lineItemId }));

                dispatch(
                    BagStore.actions.updateBagItemCount({
                        bagEntryIndex:
                            sameItemInBagIndex > targetItemInBagIndex ? sameItemInBagIndex - 1 : sameItemInBagIndex,
                        value: pdpTallyItem.quantity,
                    })
                );
            } else if (sameItemInBagIndex > -1) {
                // Update quantity only
                dispatch(BagStore.actions.updateTooltip({ tooltipIsOpen: true, created: true }));

                dispatch(
                    BagStore.actions.updateBagItemCount({
                        bagEntryIndex: sameItemInBagIndex,
                        value: pdpTallyItem.quantity,
                    })
                );
            } else {
                dispatch(BagStore.actions.updateTooltip({ tooltipIsOpen: true, updated: true }));

                dispatch(
                    BagStore.actions.editBagLineItem({
                        bagEntry: selectBagEntryForPDPTally(rootState, tallyItemWithoutExtras),
                    })
                );
            }

            putExtrasToBag(pdpTallyItem.childExtras);
        } else {
            dispatch(BagStore.actions.updateTooltip({ tooltipIsOpen: true, created: true }));

            const nextLineItemId = generateNextLineItemId(bagEntries);
            tallyItemWithoutExtras.lineItemId = nextLineItemId;

            dispatch(
                BagStore.actions.addToBag({
                    bagEntry: selectBagEntryForPDPTally(rootState, tallyItemWithoutExtras),
                    name: payload.name,
                    category: payload.category,
                    sauce: payload.sauce,
                })
            );

            putExtrasToBag(pdpTallyItem.childExtras, nextLineItemId + 1);
        }
    };

    const putExtrasToBag = (extras: PdpStore.PDPTallyItem[] = [], nextLineItemId?: number) => {
        let lineItemId = nextLineItemId ?? generateNextLineItemId(bagEntries);

        const toAddToBag: BagStore.AddToBagPayload[] = [];
        const toUpdateBagCount: BagStore.UpdateBagItemCountPayload[] = [];

        extras.forEach((item) => {
            if (item) {
                const sameItemInBagIndex = findItemIndexInBag(bagEntries, item);

                if (sameItemInBagIndex > -1) {
                    toUpdateBagCount.push({
                        bagEntryIndex: sameItemInBagIndex,
                        value: item.quantity,
                    });
                } else {
                    toAddToBag.push({
                        bagEntry: selectBagEntryForPDPTally(rootState, {
                            ...item,
                            childExtras: [],
                            lineItemId: lineItemId++,
                        }),
                        name: selectProductById(rootState, item.productId)?.name,
                        category: selectTopCategory(rootState, item.productId)?.name,
                    });
                }
            }
        });

        dispatch(BagStore.actions.addMultipleToBag(toAddToBag));
        dispatch(BagStore.actions.updateBagItemsCount(toUpdateBagCount));
    };

    const updateBagItems = () => {
        const payload: BagStore.UpdateBagItemsPayload = {
            value: selectTallyUpdateBagItems(rootState),
        };

        return dispatch(BagStore.actions.updateBagItems(payload));
    };

    const setPickupTime = (payload: BagStore.SetPickupTimePayload) => {
        return dispatch(BagStore.actions.setPickupTime(payload));
    };

    const resetOrderTime = () => {
        return dispatch(BagStore.actions.resetOrderTime());
    };

    const resetPickupTime = () => {
        return dispatch(BagStore.actions.resetPickupTime());
    };

    const resetDeliveryTime = () => {
        return dispatch(BagStore.actions.resetDeliveryTime());
    };

    const initPickupTimeValues = (payload: BagStore.InitPickupTimeValuesPayload) => {
        return dispatch(BagStore.actions.initPickupTimeValues(payload));
    };

    const setPickupTimeValues = (payload: BagStore.SetPickupTimeValuesPayload) => {
        return dispatch(BagStore.actions.setPickupTimeValues(payload));
    };

    return {
        isOpen,
        tooltipOpen,
        entryCreated,
        entryUpdated,
        entryCreatedWithDeal,
        entryUpdatedWithDeal,
        bagEntries,
        dealId,
        entriesMarkedAsRemoved,
        bagEntriesCount,
        justAddedToBag,
        orderTime,
        orderTimeType,
        pickupTime,
        pickupTimeType,
        deliveryTime,
        deliveryTimeType,
        pickupTimeValues,
        pickupTimeIsValid,
        lastRemovedLineItemId,
        lastEdit,
        actions: {
            toggleIsOpen,
            updateTooltip,
            addDealToBag,
            removeDealFromBag,
            addDefaultToBag,
            setJustAddedToBag,
            editBagLineItem,
            markAsRemoved,
            removeFromBag,
            removeAllFromBag,
            clearBag,
            updateBagItems,
            updateBagItemCount,
            putToBag,
            setPickupTime,
            resetOrderTime,
            resetPickupTime,
            resetDeliveryTime,
            initPickupTimeValues,
            setPickupTimeValues,
            clearLastRemovedLineItemId,
            setLastEdit,
            clearLastEdit,
        },
    };
}
