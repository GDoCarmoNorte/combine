import { useDispatch, useSelector } from 'react-redux';
import { PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import * as OrderHistoryStore from '../orderHistory';

interface IUseOrderHistory {
    orderHistory: OrderHistoryStore.IOrderHistory;
    orderHistoryFulfilled: OrderHistoryStore.IOrderHistory;
    isLoading: boolean;
    actions: {
        setOrderHistory: (
            payload: OrderHistoryStore.ISetOrderHistoryPayload
        ) => PayloadAction<OrderHistoryStore.ISetOrderHistoryPayload>;
        setOrderHistoryLoading: (payload: boolean) => void;
    };
}

export default function UseOrderHistory(): IUseOrderHistory {
    const dispatch = useDispatch();

    const orderHistory = useSelector<RootState, OrderHistoryStore.IOrderHistoryState>((state) => state.orderHistory);

    const setOrderHistory = (payload: OrderHistoryStore.ISetOrderHistoryPayload) =>
        dispatch(OrderHistoryStore.actions.setOrderHistory(payload));
    const setOrderHistoryLoading = (payload: boolean) =>
        dispatch(OrderHistoryStore.actions.setOrderHistoryLoading(payload));

    const orderHistoryFulfilled = orderHistory?.orderHistory?.filter((order) => order?.statusText === 'FULFILLED');

    return {
        orderHistory: orderHistory.orderHistory,
        isLoading: orderHistory.loading,
        orderHistoryFulfilled,
        actions: {
            setOrderHistory,
            setOrderHistoryLoading,
        },
    };
}
