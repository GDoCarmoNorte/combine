import { ItemModel, MacroNutrientModel, SizeGroupModel, TallyModifierModel } from '../@generated/webExpApi';

/***** IDisplayFullProduct ****/

export enum ProductTypesEnum {
    Single = 'SINGLE',
    Meal = 'MEAL',
    Promo = 'PROMO',
}
export interface IDisplayFullProduct {
    productType: ProductTypesEnum;
    displayName: string; // For simplicity.  e.g. Roast Beef Combo
    productId: string;
    mainProductId: string;
    mainProductSectionName: string;
    productSections: IDisplayProductSection[]; // ORDER MATTERS.  (e.g. Drink above Side)  This would contain all "Main" options, "Side" options, or "Drink" options
    upgradeProductId?: string;
    price: number;
    calories: number;
    comboSize?: string;
    comboSizeName?: string;
    totalPrice?: number;
}

export interface IDisplayProductSection {
    productSectionType?: 'main' | 'side' | 'drink' | 'promo' | 'sauce' | ModifierGroupType.WINGTYPE;
    productSectionDisplayName?: string; // e.g. Side or Drink or Kid's Side
    productGroupId?: string;
}

export interface IDisplayProduct {
    displayName: string; // e.g. Roast Beef Sandwich, Curly Fry, Coca-Cola
    displayProductDetails: IDisplayProductDetails; // AKA portion size
}

export interface IDisplayProductDetails {
    // Ideally we want to sort these by multiple attributes on same PDP.
    // e.g. SM, MD, LG for Roast Beef Classic,  Beef N Cheddar
    // e.g. Regular, Giant for Spicy East Coast Italian or Italian Night Club
    productId: string;
    // (not currently supported but future case)  For example, PDP could support 3 sizes * Classic, Double, Half Pound for a total of 9 products
    calories?: number;
    price?: number;
    displayName?: string;
    defaultQuantity?: number;
    maxQuantity?: number;
    minQuantity?: number;
    quantity?: number;
    selections?: ISizeSelection[];
    // Intensity of sauces, TODO create a separate type for BWW
    intensity?: ProductIntensityEnum;
    isRecommended?: boolean;
}

export interface IDisplayModifierGroup {
    displayName: string; // e.g. Meat, Cheese, Veggies
    modifiers: IDisplayProduct[]; // ORDER MATTERS
    modifierGroupId: string;
    sequence: number;
    minQuantity: number;
    maxQuantity: number;
}

export interface ISaucesDisplayModifierGroup extends IDisplayModifierGroup {
    hasOnSideOption: boolean;
    isOnSideChecked?: boolean;
    isOnSideOptionDisabled?: boolean;
}
/* ********** */

export interface ISelectedModifier {
    name: string;
    quantity: number;
    productId: string;
    price: number;
    calories?: number;
    selection?: string;
    relatedSelections?: string[];
    metadata?: { [key: string]: string };
    modifiers?: ISelectedSubModifier[];
    removedModifiers?: IDefaultModifier[];
}

export type ISelectedSubModifier = {
    name: string;
    calories?: number;
    modifiers?: TallyModifierModel[];
} & TallyModifierModel;

export interface IDefaultModifier {
    name: string;
    defaultQuantity: number;
    productId: string;
    minQuantity: number;
    maxQuantity: number;
    price?: number;
    calories?: number;
    selection?: string;
    relatedSelections?: string[];
    metadata?: { [key: string]: string };
    modifiers?: TallyModifierModel[];
}

export interface ISelectedExtraItem {
    name: string;
    price: number;
    calories?: number;
}

/* ********** */
export interface ISizeSelection {
    sizeGroup: SizeGroupModel;
    disabled: boolean;
    product: ItemModel;
    isGroupedBySize: boolean;
}

/* ********* */
export interface INutritionInfoSizeSelection {
    name: string;
    nutritionalFacts?: MacroNutrientModel[];
    allergicInformation?: string;
}
export interface INutritionInfo {
    displayName: string;
    sizeSelections: INutritionInfoSizeSelection[];
}

export interface IAboutSectionInfo {
    productKind: string;
    description: string;
    whatsIncluded: string[];
    nutritionInfo?: INutritionInfo[];
}

export interface IExtraItemModel extends ItemModel {
    resultPrice: number;
    resultCalories: number;
    quantity: number;
    sizeSelections?: ISizeSelection[];
}

export interface IExtraProductGroup {
    displayName: string;
    products: IExtraItemModel[];
    sequence: number;
}

export enum ProductIntensityEnum {
    MILD = 'Mild',
    MEDIUM = 'Medium',
    HOT = 'Hot',
    WILD = 'Wild',
}

export interface IDomainProductItem extends ItemModel {
    isSaleable: boolean;
}

export type ModifierCardSelectorType = 'default' | 'quantity' | 'size';

export enum ItemGroupEnum {
    BOGO = 'BOGO',
    BOG6 = 'BOG6',
    BOGO_50_OFF = 'BOGO 50%',
    PROTEIN = 'Protein',
    FLAVOR = 'Flavor',
    SIZE = 'Size',
}

export enum ModifierGroupType {
    MODIFICATIONS = 'Modifications',
    SELECTIONS = 'Selections',
    SAUCES = 'Sauces',
    EXTRAS = 'Extras',
    WINGTYPE = 'WingType',
}
