import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { IMenuModel } from '../@generated/webExpApi';
import { getLocationMenu, getNationalMenu } from '../common/services/domainMenu';
import { GTM_ERROR_EVENT } from '../common/services/gtmService/constants';
import { GtmErrorCategory, GtmErrorEvent } from '../common/services/gtmService/types';

import { InspireThunk } from './helpers';
import * as NotificationsStore from './notifications';

interface DomainMenuState {
    payload?: IMenuModel;
    loading: boolean;
    error: boolean;
}

export const initialState: DomainMenuState = { loading: false, error: false };
const sliceName = 'domainMenu';

export const getPosMenuByLocation = createAsyncThunk(
    'menu/fetchByLocationId',
    async (locationId: string | undefined, { dispatch }) => {
        if (locationId) {
            try {
                return await getLocationMenu(locationId);
            } catch (e) {
                if (e.status === 404) {
                    const errorMessage =
                        'Online ordering is not available at this location. Please select another location.';
                    dispatch(
                        NotificationsStore.createEnqueueNotificationAction({
                            message: errorMessage,
                        })
                    );
                    const payload = {
                        ErrorCategory: GtmErrorCategory.CHECKOUT_UNAVAILABLE_LOCATION,
                        ErrorDescription: errorMessage,
                    } as GtmErrorEvent;

                    dispatch({ type: GTM_ERROR_EVENT, payload });
                } else {
                    dispatch(NotificationsStore.createEnqueueNotificationAction(e));
                }
                return getNationalMenu();
            }
        }
        return getNationalMenu();
    }
);

const setDomainMenuReducer = (_: DomainMenuState, action: { payload: IMenuModel }) => {
    const payload = action.payload;

    return {
        payload,
        loading: false,
        error: false,
    };
};

const domainMenuSlice = createSlice({
    name: sliceName,
    initialState,
    reducers: {
        setDomainMenu: setDomainMenuReducer,
    },
    extraReducers: (builder) => {
        InspireThunk(builder, getPosMenuByLocation, setDomainMenuReducer);
    },
});

export const { actions, name } = domainMenuSlice;

export default domainMenuSlice.reducer;
