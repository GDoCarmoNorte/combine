import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const currentSectionTypes = [
        'infoBanner',
        'accountHeader',
        'benefitsOfJoining',
        'checkoutHeader',
        'descriptionSection',
        'dineTime',
        'emailSignup',
        'errorBanner',
        'frequentlyAskedQuestions',
        'giftCardBalanceSection',
        'goingAtTheBar',
        'imageBlockSection',
        'imageAsset',
        'infoBlockSection',
        'locationFailed',
        'locationNewsSection',
        'locationNotFound',
        'locationPromoBanner',
        'localTapList',
        'notificationBanner',
        'mainBanner',
        'mainProductBanner',
        'markup',
        'menuCategorySection',
        'miniBanners',
        'myDealsEmpty',
        'numberedListSection',
        'nearbyLocationsSection',
        'openTable',
        'padding',
        'pickupInstructions',
        'promoBlockSection',
        'secondaryBanner',
        'singleImageWithOverlay',
        'textBlockSection',
        'topPicks',
        'videoBlock',
        'action',
    ];

    const recentOrdersSection: ContentType = migration
        .createContentType('recentOrdersSection')
        .name('Recent Orders Section')
        .displayField('name');

    recentOrdersSection.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true,
    });

    recentOrdersSection.createField('mainText').name('Main Text').type('Symbol');

    recentOrdersSection.createField('secondaryText').name('Secondary Text').type('Symbol');

    recentOrdersSection
        .createField('backgroundColor')
        .name('Background Color')
        .type('Link')
        .linkType('Entry')
        .validations([
            {
                linkContentType: ['color'],
            },
        ]);

    recentOrdersSection.createField('image').name('Image').type('Link').linkType('Asset');

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',
        validations: [
            {
                linkContentType: [...currentSectionTypes, 'recentOrdersSection'],
            },
        ],
        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
