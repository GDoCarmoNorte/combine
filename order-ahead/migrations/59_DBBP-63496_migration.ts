import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const checkoutLegal: ContentType = migration
        .createContentType('checkoutLegal')
        .name('Checkout Legal')
        .description('');

    checkoutLegal.createField('deliveryFeeLegalMessage').name('Delivery Fee Legal Message').type('Symbol');
    checkoutLegal.createField('serviceFeeLegalMessage').name('Service Fee Legal Message').type('Symbol');
    checkoutLegal.createField('takeoutServiceFeeLegalMessage').name('Takeout Service Fee Legal Message').type('Symbol');
} as MigrationFunction;

module.exports = migrationFunction;
