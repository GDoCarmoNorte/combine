module.exports = function (migration) {
    const footerSecondaryBlock = migration
        .createContentType('footerSecondaryBlock')
        .name('Footer - Secondary Block')
        .description('')
        .displayField('label');
    footerSecondaryBlock
        .createField('label')
        .name('Label')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    footerSecondaryBlock
        .createField('links')
        .name('Links')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: [
                        'documentLink',
                        'externalLink',
                        'menuCategoryLink',
                        'pageLink',
                        'phoneNumberLink',
                        'product',
                    ],
                },
            ],

            linkType: 'Entry',
        });

    const markup = migration
        .createContentType('markup')
        .name('Markup')
        .description('Block for Markup')
        .displayField('name');
    markup
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    markup
        .createField('markup')
        .name('Markup')
        .type('RichText')
        .localized(false)
        .required(false)
        .validations([
            {
                nodes: {
                    'entry-hyperlink': [
                        {
                            linkContentType: [
                                'documentLink',
                                'externalLink',
                                'menuCategoryLink',
                                'pageLink',
                                'phoneNumberLink',
                                'product',
                            ],

                            message: null,
                        },
                    ],
                },
            },
            {
                enabledNodeTypes: [
                    'heading-1',
                    'heading-2',
                    'heading-3',
                    'heading-4',
                    'heading-5',
                    'heading-6',
                    'ordered-list',
                    'unordered-list',
                    'hr',
                    'blockquote',
                    'embedded-asset-block',
                    'hyperlink',
                    'entry-hyperlink',
                    'asset-hyperlink',
                ],

                message:
                    'Only heading 1, heading 2, heading 3, heading 4, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, asset, link to Url, link to entry, and link to asset nodes are allowed',
            },
        ])
        .disabled(false)
        .omitted(false);

    markup.changeFieldControl('name', 'builtin', 'singleLine', {});
    markup.changeFieldControl('markup', 'builtin', 'richTextEditor', {});
    const emailSignup = migration
        .createContentType('emailSignup')
        .name('Email Signup')
        .description('')
        .displayField('formTitle');

    emailSignup
        .createField('header')
        .name('Header')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['infoBanner'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    emailSignup
        .createField('formTitle')
        .name('Form Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    emailSignup
        .createField('formLinkText')
        .name('Form Link Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    emailSignup
        .createField('legalText')
        .name('Legal Text')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    emailSignup
        .createField('successMessageIcon')
        .name('Success Message Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    emailSignup
        .createField('successMessageHeaderText')
        .name('Success Message Header Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    emailSignup
        .createField('successMessageText')
        .name('Success Message Text')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    emailSignup
        .createField('formFooterRichText')
        .name('Form Footer Rich Text')
        .type('RichText')
        .localized(false)
        .required(false)
        .validations([
            {
                enabledNodeTypes: [
                    'heading-1',
                    'heading-2',
                    'heading-3',
                    'heading-4',
                    'heading-5',
                    'heading-6',
                    'ordered-list',
                    'unordered-list',
                    'hr',
                    'blockquote',
                    'hyperlink',
                    'entry-hyperlink',
                ],

                message:
                    'Only heading 1, heading 2, heading 3, heading 4, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, link to Url, and link to entry nodes are allowed',
            },
            {
                nodes: {
                    'entry-hyperlink': [
                        {
                            linkContentType: [
                                'documentLink',
                                'externalLink',
                                'menuCategoryLink',
                                'pageLink',
                                'phoneNumberLink',
                                'product',
                            ],

                            message: null,
                        },
                    ],
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    const frequentlyAskedQuestionsQuestionanswer = migration
        .createContentType('frequentlyAskedQuestionsQuestionanswer')
        .name('Frequently Asked Questions - Question+Answer')
        .description('')
        .displayField('question');
    frequentlyAskedQuestionsQuestionanswer
        .createField('question')
        .name('question')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    frequentlyAskedQuestionsQuestionanswer
        .createField('answer')
        .name('answer')
        .type('RichText')
        .localized(false)
        .required(true)
        .validations([
            {
                enabledNodeTypes: [
                    'heading-1',
                    'heading-2',
                    'heading-3',
                    'heading-4',
                    'heading-5',
                    'heading-6',
                    'ordered-list',
                    'unordered-list',
                    'hr',
                    'blockquote',
                    'hyperlink',
                    'entry-hyperlink',
                ],

                message:
                    'Only heading 1, heading 2, heading 3, heading 4, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, link to Url, and link to entry nodes are allowed',
            },
            {
                nodes: {
                    'entry-hyperlink': [
                        {
                            linkContentType: [
                                'documentLink',
                                'externalLink',
                                'menuCategoryLink',
                                'pageLink',
                                'phoneNumberLink',
                                'product',
                            ],

                            message: null,
                        },
                    ],
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    const textBlockSectionAccordionItem = migration
        .createContentType('textBlockSectionAccordionItem')
        .name('Text Block Section - Accordion Item')
        .description('')
        .displayField('title');
    textBlockSectionAccordionItem
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    textBlockSectionAccordionItem
        .createField('text')
        .name('Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    textBlockSectionAccordionItem
        .createField('content')
        .name('Content')
        .type('RichText')
        .localized(false)
        .required(true)
        .validations([
            {
                enabledNodeTypes: [
                    'heading-4',
                    'ordered-list',
                    'unordered-list',
                    'hr',
                    'blockquote',
                    'hyperlink',
                    'entry-hyperlink',
                ],

                message:
                    'Only heading 4, ordered list, unordered list, horizontal rule, quote, link to Url, and link to entry nodes are allowed',
            },
            {
                nodes: {
                    'entry-hyperlink': [
                        {
                            linkContentType: [
                                'documentLink',
                                'externalLink',
                                'menuCategoryLink',
                                'pageLink',
                                'phoneNumberLink',
                                'product',
                            ],

                            message: null,
                        },
                    ],
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    const textBlockSection = migration
        .createContentType('textBlockSection')
        .name('Text Block Section')
        .description('')
        .displayField('title');
    textBlockSection
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    textBlockSection
        .createField('subtitle')
        .name('Subtitle')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    textBlockSection
        .createField('text')
        .name('Text')
        .type('RichText')
        .localized(false)
        .required(true)
        .validations([
            {
                enabledNodeTypes: [
                    'heading-1',
                    'heading-2',
                    'heading-3',
                    'heading-4',
                    'heading-5',
                    'heading-6',
                    'ordered-list',
                    'unordered-list',
                    'hr',
                    'blockquote',
                    'hyperlink',
                    'entry-hyperlink',
                ],

                message:
                    'Only heading 1, heading 2, heading 3, heading 4, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, link to Url, and link to entry nodes are allowed',
            },
            {
                nodes: {
                    'entry-hyperlink': [
                        {
                            linkContentType: [
                                'documentLink',
                                'externalLink',
                                'menuCategoryLink',
                                'pageLink',
                                'phoneNumberLink',
                                'product',
                            ],

                            message: null,
                        },
                    ],
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    textBlockSection
        .createField('accordionItems')
        .name('Accordion items')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['textBlockSectionAccordionItem'],
                },
            ],

            linkType: 'Entry',
        });

    const mainProductBanner = migration
        .createContentType('mainProductBanner')
        .name('Main Product Banner')
        .description('Main Product Banner')
        .displayField('topText');
    mainProductBanner
        .createField('topIcon')
        .name('Top Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    mainProductBanner
        .createField('topText')
        .name('Top Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    mainProductBanner
        .createField('mainText')
        .name('Main Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainProductBanner
        .createField('mainTextLink')
        .name('Main Text Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainProductBanner
        .createField('bottomText')
        .name('Bottom Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainProductBanner
        .createField('rightImage')
        .name('Right Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },

                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    mainProductBanner
        .createField('rightImageLink')
        .name('Right Image Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainProductBanner
        .createField('productLink')
        .name('Product Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['product'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainProductBanner
        .createField('backupLinkCTA')
        .name('Backup Link CTA')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['documentLink', 'externalLink', 'menuCategoryLink', 'pageLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainProductBanner
        .createField('backgroundColorRef')
        .name('Background Color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainProductBanner.changeFieldControl('topIcon', 'builtin', 'assetLinkEditor', {});
    mainProductBanner.changeFieldControl('topText', 'builtin', 'singleLine', {});
    mainProductBanner.changeFieldControl('mainText', 'builtin', 'singleLine', {});
    mainProductBanner.changeFieldControl('mainTextLink', 'builtin', 'entryLinkEditor', {});
    mainProductBanner.changeFieldControl('bottomText', 'builtin', 'singleLine', {});
    mainProductBanner.changeFieldControl('rightImage', 'builtin', 'assetLinkEditor', {});
    mainProductBanner.changeFieldControl('rightImageLink', 'builtin', 'entryLinkEditor', {});
    mainProductBanner.changeFieldControl('productLink', 'builtin', 'entryLinkEditor', {});
    mainProductBanner.changeFieldControl('backupLinkCTA', 'builtin', 'entryLinkEditor', {});
    mainProductBanner.changeFieldControl('backgroundColorRef', 'builtin', 'entryLinkEditor', {});
    const mainBanner = migration
        .createContentType('mainBanner')
        .name('Main Banner')
        .description('Main Hero Banner')
        .displayField('topText');
    mainBanner
        .createField('topIcon')
        .name('Top Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    mainBanner
        .createField('topText')
        .name('Top Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    mainBanner
        .createField('mainText')
        .name('Main Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('mainTextLink')
        .name('Main Text Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainBanner
        .createField('bottomText')
        .name('Bottom Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('rightImage')
        .name('Right Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },

                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    mainBanner
        .createField('rightImageLink')
        .name('Right Image Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainBanner
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    mainBanner
        .createField('backgroundColor')
        .name('Background Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    mainBanner
        .createField('mainLinkText')
        .name('Main Link Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('mainLinkHref')
        .name('Main Link Href')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainBanner
        .createField('secondaryLink')
        .name('Secondary Link')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('secondaryLinkHref')
        .name('Secondary Link Href')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainBanner
        .createField('mainTextSize')
        .name('Main Text Size')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                in: ['h1', 'h2'],
            },
        ])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('mainLinkType')
        .name('Main Link Type')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                in: ['primary', 'secondary', 'large', 'small'],
            },
        ])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('leftImage')
        .name('Left Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    mainBanner
        .createField('leftImageLink')
        .name('Left Image Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainBanner
        .createField('verticallyCentered')
        .name('Vertically Centered')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('backgroundVideo')
        .name('Background Video')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['videoBlock'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    mainBanner.changeFieldControl('topIcon', 'builtin', 'assetLinkEditor', {});
    mainBanner.changeFieldControl('topText', 'builtin', 'singleLine', {});
    mainBanner.changeFieldControl('mainText', 'builtin', 'singleLine', {});

    mainBanner.changeFieldControl('mainTextLink', 'builtin', 'entryLinkEditor', {
        showLinkEntityAction: true,
        showCreateEntityAction: true,
    });

    mainBanner.changeFieldControl('bottomText', 'builtin', 'singleLine', {});
    mainBanner.changeFieldControl('rightImage', 'builtin', 'assetLinkEditor', {});
    mainBanner.changeFieldControl('rightImageLink', 'builtin', 'entryLinkEditor', {});
    mainBanner.changeFieldControl('backgroundImage', 'builtin', 'assetLinkEditor', {});
    mainBanner.changeFieldControl('backgroundColor', 'builtin', 'singleLine', {});
    mainBanner.changeFieldControl('mainLinkText', 'builtin', 'singleLine', {});
    mainBanner.changeFieldControl('mainLinkHref', 'builtin', 'entryLinkEditor', {});
    mainBanner.changeFieldControl('secondaryLink', 'builtin', 'singleLine', {});
    mainBanner.changeFieldControl('secondaryLinkHref', 'builtin', 'entryLinkEditor', {});
    mainBanner.changeFieldControl('mainTextSize', 'builtin', 'dropdown', {});
    mainBanner.changeFieldControl('mainLinkType', 'builtin', 'dropdown', {});
    mainBanner.changeFieldControl('leftImage', 'builtin', 'assetLinkEditor', {});
    mainBanner.changeFieldControl('leftImageLink', 'builtin', 'entryLinkEditor', {});
    mainBanner.changeFieldControl('verticallyCentered', 'builtin', 'boolean', {});
    mainBanner.changeFieldControl('backgroundVideo', 'builtin', 'entryLinkEditor', {});
    const siteMapCategory = migration
        .createContentType('siteMapCategory')
        .name('Site Map Category')
        .description('')
        .displayField('name');
    siteMapCategory
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    siteMapCategory
        .createField('mainLink')
        .name('Main Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['documentLink', 'externalLink', 'pageLink', 'socialMediaLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const secondaryBanner = migration
        .createContentType('secondaryBanner')
        .name('Secondary Banner')
        .description(
            'This component will be used when multiple or dual value props need to be communicated to customers'
        )
        .displayField('title');

    secondaryBanner
        .createField('hasBorder')
        .name('Has Border')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    secondaryBanner
        .createField('icon')
        .name('Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    secondaryBanner
        .createField('mainText')
        .name('Main Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    secondaryBanner
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    secondaryBanner
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    secondaryBanner
        .createField('linkText')
        .name('Link Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    secondaryBanner
        .createField('linkUrl')
        .name('Link URL')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    secondaryBanner
        .createField('mainTextColor')
        .name('Main Text Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                regexp: {
                    pattern: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$',
                    flags: null,
                },

                message: 'Color should be in hex format starting with #',
            },
        ])
        .disabled(false)
        .omitted(false);

    secondaryBanner
        .createField('backgroundColor')
        .name('Background Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                regexp: {
                    pattern: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$',
                    flags: null,
                },

                message: 'Color should be in hex format starting with #',
            },
        ])
        .disabled(false)
        .omitted(false);

    secondaryBanner
        .createField('descriptionTitleColor')
        .name('Description Title Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const promoBlockCardCta = migration
        .createContentType('promoBlockCardCta')
        .name('Promo Block Card - CTA')
        .description('')
        .displayField('name');
    promoBlockCardCta
        .createField('name')
        .name('name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    promoBlockCardCta
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const navigation = migration
        .createContentType('navigation')
        .name('Navigation')
        .description('')
        .displayField('name');
    navigation
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    navigation
        .createField('nameMobile')
        .name('Name - Mobile')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    navigation
        .createField('logo')
        .name('Logo')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    navigation
        .createField('links')
        .name('Links')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: [
                        'documentLink',
                        'externalLink',
                        'menuCategoryLink',
                        'pageLink',
                        'product',
                        'phoneNumberLink',
                    ],
                },
            ],

            linkType: 'Entry',
        });

    navigation.changeFieldControl('name', 'builtin', 'singleLine', {});
    navigation.changeFieldControl('nameMobile', 'builtin', 'singleLine', {});
    navigation.changeFieldControl('logo', 'builtin', 'assetLinkEditor', {});
    navigation.changeFieldControl('links', 'builtin', 'entryLinksEditor', {});
    const infoBlockCard = migration
        .createContentType('infoBlockCard')
        .name('Info Block Card')
        .description('')
        .displayField('headerText');
    infoBlockCard
        .createField('headerText')
        .name('Header Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    infoBlockCard
        .createField('headerColor')
        .name('Header Color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    infoBlockCard
        .createField('subheaderText')
        .name('Subheader Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    infoBlockCard
        .createField('subheaderColor')
        .name('Subheader Color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    infoBlockCard
        .createField('buttonText')
        .name('Button Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBlockCard
        .createField('icon')
        .name('Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    infoBlockCard
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    infoBlockCard
        .createField('backgroundColor')
        .name('Background Color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const imageBlockCardCta = migration
        .createContentType('imageBlockCardCta')
        .name('Image Block Card - CTA')
        .description('')
        .displayField('name');
    imageBlockCardCta
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    imageBlockCardCta
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const footerLinksBlock = migration
        .createContentType('footerLinksBlock')
        .name('Footer - Links Block')
        .description('')
        .displayField('title');
    footerLinksBlock
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    footerLinksBlock
        .createField('links')
        .name('Links')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: [
                        'documentLink',
                        'externalLink',
                        'menuCategoryLink',
                        'pageLink',
                        'phoneNumberLink',
                        'product',
                    ],
                },
            ],

            linkType: 'Entry',
        });

    footerLinksBlock.changeFieldControl('title', 'builtin', 'singleLine', {});
    footerLinksBlock.changeFieldControl('links', 'builtin', 'entryLinksEditor', {});
    const alertBanner = migration
        .createContentType('alertBanner')
        .name('Alert Banner')
        .description('')
        .displayField('text');
    alertBanner
        .createField('text')
        .name('Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    alertBanner
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    alertBanner
        .createField('backgroundColor')
        .name('Background Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                regexp: {
                    pattern: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$',
                    flags: null,
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    alertBanner
        .createField('textColor')
        .name('Text Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                regexp: {
                    pattern: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$',
                    flags: null,
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    alertBanner
        .createField('isDismissible')
        .name('Is Dismissible')
        .type('Boolean')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    alertBanner
        .createField('showOnCheckout')
        .name('Show On Checkout')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const menuCategory = migration
        .createContentType('menuCategory')
        .name('Menu Category')
        .description('')
        .displayField('categoryName');
    menuCategory
        .createField('categoryName')
        .name('Category Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    menuCategory
        .createField('products')
        .name('Products')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['product'],
                },
            ],

            linkType: 'Entry',
        });

    menuCategory
        .createField('subcategories')
        .name('Subcategories')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['menuSubcategory'],
                },
            ],

            linkType: 'Entry',
        });

    menuCategory
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },

                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    menuCategory
        .createField('icon')
        .name('Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    menuCategory
        .createField('taglineHeading')
        .name('Tagline Heading')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    menuCategory
        .createField('taglineDescription')
        .name('Tagline Description')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    menuCategory
        .createField('sections')
        .name('Sections')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['textBlockSection'],
                },
            ],

            linkType: 'Entry',
        });

    menuCategory
        .createField('metaDescription')
        .name('Meta Description')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    menuCategory
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['menuCategoryLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const menuCategoryLink = migration
        .createContentType('menuCategoryLink')
        .name('Menu Category Link')
        .displayField('name');
    menuCategoryLink
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    menuCategoryLink
        .createField('nameInUrl')
        .name('Name in URL')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                unique: true,
            },
            {
                regexp: {
                    pattern: '^[a-z0-9_-]*$',
                    flags: null,
                },

                message: 'URL can only contain letters, numbers, dashes, and underscores',
            },
        ])
        .disabled(false)
        .omitted(false);

    const page = migration.createContentType('page').name('Page').description('').displayField('name');
    page.createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    page.createField('section')
        .name('Section')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: [
                        'infoBanner',
                        'emailSignup',
                        'errorBanner',
                        'frequentlyAskedQuestions',
                        'giftCardBalanceSection',
                        'imageBlockSection',
                        'infoBlockSection',
                        'locationFailed',
                        'locationNotFound',
                        'mainBanner',
                        'mainProductBanner',
                        'markup',
                        'menuCategorySection',
                        'numberedListSection',
                        'pickupInstructions',
                        'promoBlockSection',
                        'secondaryBanner',
                        'singleImageWithOverlay',
                        'textBlockSection',
                        'topPicks',
                    ],
                },
            ],

            linkType: 'Entry',
        });

    page.createField('hideFooter')
        .name('hideFooter')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    page.createField('siteMapCategory')
        .name('Site Map Category')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['siteMapCategory'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    page.createField('metaDescription')
        .name('Meta Description')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    page.createField('metaTitle')
        .name('Meta title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                unique: true,
            },
        ])
        .disabled(false)
        .omitted(false);

    page.createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['pageLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const pageLink = migration.createContentType('pageLink').name('Page Link').displayField('name');
    pageLink
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    pageLink
        .createField('nameInUrl')
        .name('Name in URL')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                unique: true,
            },
            {
                regexp: {
                    pattern: '^[a-z0-9_-]*$',
                    flags: null,
                },

                message: 'URL can only contain letters, numbers, dashes, and underscores',
            },
        ])
        .disabled(false)
        .omitted(false);

    const videoBlock = migration
        .createContentType('videoBlock')
        .name('Video Block')
        .description('')
        .displayField('name');
    videoBlock
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    videoBlock
        .createField('webM')
        .name('WebM')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkMimetypeGroup: ['video'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    videoBlock
        .createField('mp4')
        .name('MP4')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkMimetypeGroup: ['video'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    videoBlock
        .createField('poster')
        .name('Poster')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    videoBlock.changeFieldControl('name', 'builtin', 'singleLine', {});
    videoBlock.changeFieldControl('webM', 'builtin', 'assetLinkEditor', {});
    videoBlock.changeFieldControl('mp4', 'builtin', 'assetLinkEditor', {});
    videoBlock.changeFieldControl('poster', 'builtin', 'assetLinkEditor', {});
    const locationFailed = migration
        .createContentType('locationFailed')
        .name('Location Failed')
        .description('when api returned 40* - 50* status code ')
        .displayField('header');
    locationFailed
        .createField('header')
        .name('header')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    locationFailed
        .createField('body')
        .name('body ')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    locationFailed
        .createField('icon')
        .name('icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    const product = migration.createContentType('product').name('Product').description('').displayField('name');
    product
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    product
        .createField('nameInUrl')
        .name('Name in URL')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                unique: true,
            },
            {
                regexp: {
                    pattern: '^[a-zA-Z0-9_-]*$',
                    flags: null,
                },

                message: 'URL can only contain letters, numbers, dashes, and underscores',
            },
        ])
        .disabled(false)
        .omitted(false);

    product
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },

                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    product
        .createField('productId')
        .name('Product Id')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                unique: true,
            },
        ])
        .disabled(false)
        .omitted(false);

    product
        .createField('metaDescription')
        .name('Meta Description')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    product
        .createField('isVisible')
        .name('Is Visible')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const infoBlockSection = migration
        .createContentType('infoBlockSection')
        .name('Info Block Section')
        .description('')
        .displayField('name');
    infoBlockSection
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBlockSection
        .createField('backgroundText')
        .name('Background Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBlockSection
        .createField('backgroundBottomText')
        .name('Background Bottom Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(true)
        .omitted(true);

    infoBlockSection
        .createField('cards')
        .name('Info Block Cards')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([
            {
                size: {
                    min: 1,
                    max: 3,
                },
            },
        ])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['infoBlockCard'],
                },
            ],

            linkType: 'Entry',
        });

    infoBlockSection.changeFieldControl('name', 'builtin', 'singleLine', {});
    infoBlockSection.changeFieldControl('backgroundText', 'builtin', 'singleLine', {});
    infoBlockSection.changeFieldControl('backgroundBottomText', 'builtin', 'singleLine', {});
    infoBlockSection.changeFieldControl('cards', 'builtin', 'entryLinksEditor', {});
    const socialMediaLink = migration
        .createContentType('socialMediaLink')
        .name('Social Media Link')
        .description('')
        .displayField('name');
    socialMediaLink
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(true);

    socialMediaLink
        .createField('url')
        .name('Url')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                regexp: {
                    pattern:
                        '^(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-/]))?$',
                    flags: null,
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    socialMediaLink
        .createField('icon')
        .name('Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    socialMediaLink
        .createField('siteMapCategory')
        .name('Site Map Category')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['siteMapCategory'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    socialMediaLink
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const documentLink = migration
        .createContentType('documentLink')
        .name('Document Link')
        .description('')
        .displayField('name');
    documentLink
        .createField('name')
        .name('name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    documentLink
        .createField('document')
        .name('document')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkMimetypeGroup: ['pdfdocument'],
                message: 'Please upload valid PDF file.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    documentLink
        .createField('siteMapCategory')
        .name('Site Map Category')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['siteMapCategory'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const frequentlyAskedQuestionsLineItems = migration
        .createContentType('frequentlyAskedQuestionsLineItems')
        .name('Frequently Asked Questions - line items')
        .description('')
        .displayField('title');
    frequentlyAskedQuestionsLineItems
        .createField('title')
        .name('title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    frequentlyAskedQuestionsLineItems
        .createField('items')
        .name('items')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['frequentlyAskedQuestionsQuestionanswer'],
                },
            ],

            linkType: 'Entry',
        });

    frequentlyAskedQuestionsLineItems.changeFieldControl('title', 'builtin', 'singleLine', {});
    frequentlyAskedQuestionsLineItems.changeFieldControl('items', 'builtin', 'entryLinksEditor', {});
    const giftCardBalanceSection = migration
        .createContentType('giftCardBalanceSection')
        .name('Gift Card Balance Section')
        .description('Gift Card Balance Section')
        .displayField('header');
    giftCardBalanceSection
        .createField('header')
        .name('Header')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    giftCardBalanceSection
        .createField('ctaText')
        .name('CTA text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    const errorBanner = migration
        .createContentType('errorBanner')
        .name('Error Banner')
        .description('Error Banner content model')
        .displayField('mainText');
    errorBanner
        .createField('mainText')
        .name('Main Text')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    errorBanner
        .createField('mainImage')
        .name('Main Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    errorBanner
        .createField('errorCodeText')
        .name('Error Code Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    errorBanner
        .createField('headerText')
        .name('Header Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    errorBanner
        .createField('homePageText')
        .name('Home Page Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const cardBalance = migration
        .createContentType('cardBalance')
        .name('Card Balance (DEPRECATED)')
        .description('DEPRECATED')
        .displayField('title');
    cardBalance
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    cardBalance
        .createField('cardNumberPlaceholder')
        .name('Card Number Placeholder')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    cardBalance
        .createField('pinPlaceholder')
        .name('Pin Placeholder')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    cardBalance
        .createField('pinHintText')
        .name('Pin Hint Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    cardBalance
        .createField('buttonText')
        .name('Button Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    cardBalance
        .createField('termsAndConditionsLinkText')
        .name('Terms And Conditions Link Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    cardBalance.changeFieldControl('title', 'builtin', 'singleLine', {});
    cardBalance.changeFieldControl('cardNumberPlaceholder', 'builtin', 'singleLine', {});
    cardBalance.changeFieldControl('pinPlaceholder', 'builtin', 'singleLine', {});
    cardBalance.changeFieldControl('pinHintText', 'builtin', 'singleLine', {});
    cardBalance.changeFieldControl('buttonText', 'builtin', 'singleLine', {});
    cardBalance.changeFieldControl('termsAndConditionsLinkText', 'builtin', 'singleLine', {});
    const promoBlockSection = migration
        .createContentType('promoBlockSection')
        .name('Promo Block Section')
        .description('')
        .displayField('name');
    promoBlockSection
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    promoBlockSection
        .createField('cards')
        .name('Cards')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([
            {
                size: {
                    min: 2,
                    max: 2,
                },

                message: 'The section must have 2 cards',
            },
        ])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['promoBlockCard'],
                },
            ],

            linkType: 'Entry',
        });

    promoBlockSection
        .createField('backgroundColor')
        .name('Background color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    promoBlockSection
        .createField('titleColor')
        .name('Title color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    promoBlockSection
        .createField('descriptionColor')
        .name('Description color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const promoBlockCard = migration
        .createContentType('promoBlockCard')
        .name('Promo Block Card')
        .description('')
        .displayField('title');
    promoBlockCard
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    promoBlockCard
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    promoBlockCard
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    promoBlockCard
        .createField('cta')
        .name('CTA')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['promoBlockCardCta'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const infoBanner = migration
        .createContentType('infoBanner')
        .name(' Info Banner')
        .description('')
        .displayField('title');
    infoBanner
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBanner
        .createField('header')
        .name('Header')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBanner
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBanner
        .createField('headerTextColor')
        .name('Header Text Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    infoBanner
        .createField('headerBackgroundColor')
        .name('Header Background Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const imageBlockSection = migration
        .createContentType('imageBlockSection')
        .name('Image Block Section')
        .description('')
        .displayField('name');
    imageBlockSection
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    imageBlockSection
        .createField('cards')
        .name('cards')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([
            {
                size: {
                    min: 1,
                    max: 3,
                },
            },
        ])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['imageBlockCard'],
                },
            ],

            linkType: 'Entry',
        });

    imageBlockSection
        .createField('backgroundColor')
        .name('Background color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    imageBlockSection
        .createField('titleColor')
        .name('Title color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    imageBlockSection
        .createField('descriptionColor')
        .name('Description color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const imageBlockCard = migration
        .createContentType('imageBlockCard')
        .name('Image Block Card')
        .description('')
        .displayField('title');
    imageBlockCard
        .createField('subtitle')
        .name('Subtitle')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    imageBlockCard
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    imageBlockCard
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    imageBlockCard
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    imageBlockCard
        .createField('cta')
        .name('CTA')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['imageBlockCardCta'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const pickupInstructionsItem = migration
        .createContentType('pickupInstructionsItem')
        .name('Pickup Instructions Item')
        .description('')
        .displayField('instruction');
    pickupInstructionsItem
        .createField('instruction')
        .name('instruction')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    pickupInstructionsItem.changeFieldControl('instruction', 'builtin', 'singleLine', {});
    const pickupInstructions = migration
        .createContentType('pickupInstructions')
        .name('Pickup Instructions')
        .description('');

    pickupInstructions
        .createField('instractions')
        .name('instractions')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['pickupInstructionsItem'],
                },
            ],

            linkType: 'Entry',
        });

    pickupInstructions.changeFieldControl('instractions', 'builtin', 'entryLinksEditor', {});
    const phoneNumberLink = migration
        .createContentType('phoneNumberLink')
        .name('Phone Number Link')
        .description('Phone Number Link')
        .displayField('name');
    phoneNumberLink
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    phoneNumberLink
        .createField('phoneNumber')
        .name('Phone Number')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                regexp: {
                    pattern: '^\\d[ -.]?\\(?\\d\\d\\d\\)?[ -.]?\\d\\d\\d[ -.]?\\d\\d\\d\\d$',
                    flags: null,
                },

                message: 'Must be valid US phone number.',
            },
        ])
        .disabled(false)
        .omitted(false);

    const locationNotFound = migration
        .createContentType('locationNotFound')
        .name('Location Not Found')
        .description('banner when location not found\n')
        .displayField('header');
    locationNotFound
        .createField('header')
        .name('header')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    locationNotFound
        .createField('body')
        .name('body')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    locationNotFound
        .createField('icon')
        .name('icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    const externalLink = migration
        .createContentType('externalLink')
        .name('External Link')
        .description('')
        .displayField('name');
    externalLink
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    externalLink
        .createField('nameInUrl')
        .name('url')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                regexp: {
                    pattern:
                        '^(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-/]))?$',
                    flags: null,
                },

                message: 'Must be a full external url.  If you want to link internally use the page instead.',
            },
        ])
        .disabled(false)
        .omitted(false);

    externalLink
        .createField('siteMapCategory')
        .name('Site Map Category')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['siteMapCategory'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const singleImageWithOverlay = migration
        .createContentType('singleImageWithOverlay')
        .name('Single Image with Overlay')
        .description('Single Image with Overlay')
        .displayField('header');
    singleImageWithOverlay
        .createField('header')
        .name('Header')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    singleImageWithOverlay
        .createField('contentHeader')
        .name('Content Header')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    singleImageWithOverlay
        .createField('contentDescription')
        .name('Content Description')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    singleImageWithOverlay
        .createField('textBlock1')
        .name('Text Block 1')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    singleImageWithOverlay
        .createField('textBlock1Value')
        .name('Text Block 1 Value')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    singleImageWithOverlay
        .createField('textBlock2')
        .name('Text Block 2')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    singleImageWithOverlay
        .createField('textBlock2Value')
        .name('Text Block 2 Value')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    singleImageWithOverlay
        .createField('buttonCta')
        .name('Button CTA')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['externalLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    singleImageWithOverlay
        .createField('phone')
        .name('Phone')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['phoneNumberLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    singleImageWithOverlay
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    singleImageWithOverlay
        .createField('imageLeftAligned')
        .name('Image Left Aligned')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const numberedListSectionListItem = migration
        .createContentType('numberedListSectionListItem')
        .name('Numbered list section - list item')
        .description('')
        .displayField('title');
    numberedListSectionListItem
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    numberedListSectionListItem
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    numberedListSectionListItem.changeFieldControl('title', 'builtin', 'singleLine', {});
    numberedListSectionListItem.changeFieldControl('description', 'builtin', 'multipleLine', {});
    const frequentlyAskedQuestions = migration
        .createContentType('frequentlyAskedQuestions')
        .name('Frequently Asked Questions')
        .description('')
        .displayField('title');
    frequentlyAskedQuestions
        .createField('title')
        .name('title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    frequentlyAskedQuestions
        .createField('items')
        .name('items')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['frequentlyAskedQuestionsLineItems'],
                },
            ],

            linkType: 'Entry',
        });

    frequentlyAskedQuestions
        .createField('withAccordion')
        .name('with accordion')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    const numberedListSection = migration
        .createContentType('numberedListSection')
        .name('Numbered list section')
        .description('')
        .displayField('title');
    numberedListSection
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    numberedListSection
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    numberedListSection
        .createField('items')
        .name('Items')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['numberedListSectionListItem'],
                },
            ],

            linkType: 'Entry',
        });

    numberedListSection.changeFieldControl('title', 'builtin', 'singleLine', {});
    numberedListSection.changeFieldControl('description', 'builtin', 'multipleLine', {});
    numberedListSection.changeFieldControl('items', 'builtin', 'entryLinksEditor', {});
    const productModifier = migration
        .createContentType('productModifier')
        .name('Product Modifier')
        .description('Product modifier')
        .displayField('name');
    productModifier
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    productModifier
        .createField('productId')
        .name('productId')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    productModifier
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },

                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    const topPicks = migration
        .createContentType('topPicks')
        .name('Top Picks')
        .description('')
        .displayField('leftSideTopText');
    topPicks
        .createField('leftSideTopText')
        .name('Left Side Top Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    topPicks
        .createField('leftSideMainText')
        .name('Left Side Main Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    topPicks
        .createField('orderIcon')
        .name('Order Icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    topPicks
        .createField('modifyIcon')
        .name('Modify Icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    topPicks
        .createField('backgroundColor')
        .name('Background Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    topPicks
        .createField('textColor')
        .name('Text Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    topPicks
        .createField('isSideScroll')
        .name('isSideScroll')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    topPicks
        .createField('category')
        .name('Category')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['menuCategory'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    topPicks.changeFieldControl('leftSideTopText', 'builtin', 'singleLine', {});
    topPicks.changeFieldControl('leftSideMainText', 'builtin', 'singleLine', {});
    topPicks.changeFieldControl('orderIcon', 'builtin', 'assetLinkEditor', {});
    topPicks.changeFieldControl('modifyIcon', 'builtin', 'assetLinkEditor', {});
    topPicks.changeFieldControl('backgroundColor', 'builtin', 'singleLine', {});
    topPicks.changeFieldControl('textColor', 'builtin', 'singleLine', {});
    topPicks.changeFieldControl('isSideScroll', 'builtin', 'boolean', {});
    topPicks.changeFieldControl('category', 'builtin', 'entryLinkEditor', {});
    const menuCategorySection = migration
        .createContentType('menuCategorySection')
        .name('Menu Category Section')
        .description('')
        .displayField('name');
    menuCategorySection
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    menuCategorySection
        .createField('backgroundText')
        .name('backgroundText')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    menuCategorySection
        .createField('categories')
        .name('categories')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['menuCategory'],
                },
            ],

            linkType: 'Entry',
        });

    menuCategorySection
        .createField('backgroundPosition')
        .name('backgroundPosition')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                in: ['front', 'back', 'hidden'],
            },
        ])
        .disabled(false)
        .omitted(false);

    menuCategorySection
        .createField('backgroundColor')
        .name('backgroundColor')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    menuCategorySection
        .createField('backgroundBottomText')
        .name('backgroundBottomText')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    menuCategorySection
        .createField('backgroundBottomTextSecondPart')
        .name('Background Bottom Text Second Part')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    menuCategorySection.changeFieldControl('name', 'builtin', 'singleLine', {});
    menuCategorySection.changeFieldControl('backgroundText', 'builtin', 'singleLine', {});
    menuCategorySection.changeFieldControl('categories', 'builtin', 'entryLinksEditor', {});
    menuCategorySection.changeFieldControl('backgroundPosition', 'builtin', 'dropdown', {});
    menuCategorySection.changeFieldControl('backgroundColor', 'builtin', 'entryLinkEditor', {});
    menuCategorySection.changeFieldControl('backgroundBottomText', 'builtin', 'singleLine', {});
    menuCategorySection.changeFieldControl('backgroundBottomTextSecondPart', 'builtin', 'singleLine', {});
    const footer = migration.createContentType('footer').name('Footer').description('').displayField('name');
    footer
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    footer
        .createField('hidden')
        .name('Hidden')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    footer
        .createField('linksBlocks')
        .name('Links Blocks')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['footerLinksBlock'],
                },
            ],

            linkType: 'Entry',
        });

    footer
        .createField('socialMediaLinksBlock')
        .name('Social Media Links Block')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['footerSocialMediaLinks'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    footer
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    footer
        .createField('footerIcons')
        .name('Footer Icons')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',
            validations: [],
            linkType: 'Asset',
        });

    footer
        .createField('secondaryBlock')
        .name('Secondary Block')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['footerSecondaryBlock'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const sectionFlex = migration
        .createContentType('sectionFlex')
        .name('Section Flex')
        .description('')
        .displayField('name');
    sectionFlex
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    sectionFlex
        .createField('content')
        .name('Content')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['sectionCardFlex'],
                },
            ],

            linkType: 'Entry',
        });

    sectionFlex.changeFieldControl('name', 'builtin', 'singleLine', {});
    sectionFlex.changeFieldControl('content', 'builtin', 'entryLinksEditor', {});
    const menuSubcategory = migration
        .createContentType('menuSubcategory')
        .name('Menu Subcategory')
        .description('')
        .displayField('name');
    menuSubcategory
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    menuSubcategory
        .createField('products')
        .name('Products')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['product'],
                },
            ],

            linkType: 'Entry',
        });

    const brandTheme = migration
        .createContentType('brandTheme')
        .name('Brand Theme')
        .description('')
        .displayField('name');
    brandTheme
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    brandTheme
        .createField('primary')
        .name('primary')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('primaryVariant')
        .name('primaryVariant')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('secondary')
        .name('secondary')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('secondaryVariant')
        .name('secondaryVariant')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('dark')
        .name('dark')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('light')
        .name('light')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('gray6')
        .name('gray6')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('gray5')
        .name('gray5')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('gray4')
        .name('gray4')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('gray3')
        .name('gray3')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('gray2')
        .name('gray2')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('gray1')
        .name('gray1')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const footerSocialMediaLinks = migration
        .createContentType('footerSocialMediaLinks')
        .name('Footer - Social Media Links Block')
        .description('')
        .displayField('title');
    footerSocialMediaLinks
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    footerSocialMediaLinks
        .createField('socialMediaLinks')
        .name('Social Media Links')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['socialMediaLink'],
                },
            ],

            linkType: 'Entry',
        });

    footerSocialMediaLinks.changeFieldControl('title', 'builtin', 'singleLine', {});
    footerSocialMediaLinks.changeFieldControl('socialMediaLinks', 'builtin', 'entryLinksEditor', {});
    const menu = migration
        .createContentType('menu')
        .name('Menu')
        .description('National Menu and possibly localized menus in future')
        .displayField('name');
    menu.createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    menu.createField('categories')
        .name('Categories')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['menuCategory'],
                },
            ],

            linkType: 'Entry',
        });

    menu.changeFieldControl('name', 'builtin', 'singleLine', {});
    menu.changeFieldControl('categories', 'builtin', 'entryLinksEditor', {});
    const color = migration.createContentType('color').name('Color').description('').displayField('name');

    color
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                unique: true,
            },
            {
                regexp: {
                    pattern: '^[a-z0-9_\\-]+$',
                    flags: null,
                },

                message: 'lower case letters only',
            },
        ])
        .disabled(false)
        .omitted(false);

    color
        .createField('hexColor')
        .name('Hex Color')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                regexp: {
                    pattern: '^[xX]?[0-9a-fA-F]{6}$',
                    flags: null,
                },

                message: 'must be hexidecimal color',
            },
        ])
        .disabled(false)
        .omitted(false);
};
