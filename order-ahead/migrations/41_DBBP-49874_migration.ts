import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const currentSectionTypes = [
        'infoBanner',
        'accountHeader',
        'benefitsOfJoining',
        'checkoutHeader',
        'descriptionSection',
        'dineTime',
        'emailSignup',
        'errorBanner',
        'frequentlyAskedQuestions',
        'giftCardBalanceSection',
        'goingAtTheBar',
        'imageBlockSection',
        'imageAsset',
        'infoBlockSection',
        'locationFailed',
        'locationNewsSection',
        'locationNotFound',
        'locationPromoBanner',
        'localTapList',
        'notificationBanner',
        'mainBanner',
        'mainProductBanner',
        'markup',
        'menuCategorySection',
        'miniBanners',
        'myDealsEmpty',
        'numberedListSection',
        'nearbyLocationsSection',
        'openTable',
        'padding',
        'pickupInstructions',
        'promoBlockSection',
        'secondaryBanner',
        'singleImageWithOverlay',
        'textBlockSection',
        'topPicks',
        'videoBlock',
    ];

    const action = migration
        .createContentType('action')
        .name('Action')
        .description('Action')
        .displayField('tableReference');

    action
        .createField('tableReference')
        .name('Table Reference')
        .type('Symbol')
        .localized(false)
        .required(true)
        .disabled(false)
        .omitted(false);

    action
        .createField('defaultSection')
        .name('Default Section')
        .type('Link')
        .linkType('Entry')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [...currentSectionTypes],
            },
        ])
        .disabled(false)
        .omitted(false);

    const actionParameter = migration
        .createContentType('actionParameter')
        .name('Action Parameter')
        .description('Action Parameter')
        .displayField('tableReference');

    actionParameter
        .createField('tableReference')
        .name('Table Reference')
        .type('Symbol')
        .localized(false)
        .required(true)
        .disabled(false)
        .omitted(false);

    actionParameter
        .createField('section')
        .name('Section')
        .type('Link')
        .linkType('Entry')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: [...currentSectionTypes],
            },
        ])
        .disabled(false)
        .omitted(false);

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',
        validations: [
            {
                linkContentType: [...currentSectionTypes, 'action'],
            },
        ],
        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
