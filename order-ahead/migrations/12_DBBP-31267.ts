import Migration from 'contentful-migration';

const migrationFunction = (migration: Migration): void => {
    const menuCategory = migration.editContentType('menuCategory');

    menuCategory
        .createField('categoryId')
        .name('Category Id')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                unique: true,
            },
        ])
        .disabled(false)
        .omitted(false);
};

module.exports = migrationFunction;
