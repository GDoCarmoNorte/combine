import { MigrationFunction } from 'contentful-migration';

const migrationFunction = async function (migration, { makeRequest }) {
    const siteMapCategory = migration.editContentType('siteMapCategory');
    siteMapCategory
        .createField('links')
        .name('Links')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['documentLink', 'externalLink', 'pageLink', 'socialMediaLink'],
                },
            ],

            linkType: 'Entry',
        });

    const [pages, externalLinks, documentLinks, socialMediaLink] = (await Promise.all([
        makeRequest({
            method: 'GET',
            url: '/entries?content_type=page',
        }),
        makeRequest({
            method: 'GET',
            url: '/entries?content_type=externalLink',
        }),
        makeRequest({
            method: 'GET',
            url: '/entries?content_type=documentLink',
        }),
        makeRequest({
            method: 'GET',
            url: '/entries?content_type=socialMediaLink&',
        }),
    ]).then((allLinks) => {
        return Promise.all(
            allLinks.map((it) => {
                return Promise.all(
                    it.items
                        .filter((link) => !!link.fields.siteMapCategory)
                        .map(async (link) => {
                            if (link.fields.siteMapCategory) {
                                const id = link.fields.siteMapCategory['en-US'].sys.id;
                                const siteMapCategory = await makeRequest({
                                    method: 'GET',
                                    url: `/entries/${id}`,
                                });

                                return {
                                    ...link,
                                    fields: {
                                        ...link.fields,
                                        siteMapCategory,
                                    },
                                };
                            }
                            return link;
                        })
                );
            })
        );
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    })) as any;

    migration.transformEntries({
        contentType: 'siteMapCategory',
        from: ['name'],
        to: ['links'],
        transformEntryForLocale: async (fromFields, currentLocale) => {
            const categoryName = fromFields.name[currentLocale];
            const isCategory = (it) => it.fields.siteMapCategory.fields.name[currentLocale] === categoryName;
            const toLink = (it) => ({
                sys: {
                    type: 'Link',
                    linkType: 'Entry',
                    id: it.sys.id,
                },
            });

            const categoryPageLinks = pages.filter(isCategory).map((it) => it.fields.link[currentLocale]);
            const categoryExternalLinks = externalLinks.filter(isCategory).map(toLink);
            const categoryDocumentLinks = documentLinks.filter(isCategory).map(toLink);
            const categorySocialMediaLinks = socialMediaLink.filter(isCategory).map(toLink);

            const links = [
                ...categoryPageLinks,
                ...categoryExternalLinks,
                ...categoryDocumentLinks,
                ...categorySocialMediaLinks,
            ];

            return {
                links,
            };
        },
    });

    migration.editContentType('page').deleteField('siteMapCategory');
    migration.editContentType('documentLink').deleteField('siteMapCategory');
    migration.editContentType('externalLink').deleteField('siteMapCategory');
    migration.editContentType('socialMediaLink').deleteField('siteMapCategory');
} as MigrationFunction;

module.exports = migrationFunction;
