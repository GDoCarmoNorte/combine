import Migration from 'contentful-migration';

const migrationFunction = (migration: Migration): void => {
    const locationNewsTile = migration.editContentType('locationNewsTile');

    locationNewsTile.editField('header').required(false);
    locationNewsTile.editField('icon').required(false);

    const locationNewsSection = migration.editContentType('locationNewsSection');

    locationNewsSection.editField('header').required(false);
    locationNewsSection.editField('description').required(false);
};

module.exports = migrationFunction;
