import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const benefitsOfJoining: ContentType = migration
        .createContentType('benefitsOfJoining')
        .name('Benefits of joining')
        .description('Benefits of joining section');
    benefitsOfJoining
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    benefitsOfJoining
        .createField('image')
        .name('Image')
        .type('Link')
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 104,
                        max: 104,
                    },
                    height: {
                        min: 56,
                        max: 56,
                    },
                },
                message: 'Image must be 128px wide and  45px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    benefitsOfJoining
        .createField('benefits')
        .name('Benefits')
        .required(false)
        .type('Array')
        .localized(false)
        .validations([
            {
                size: {
                    min: 1,
                },
            },
        ])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['benefit'],
                },
            ],

            linkType: 'Entry',
        });

    benefitsOfJoining
        .createField('secondaryTitle')
        .name('Secondary Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    benefitsOfJoining
        .createField('secondarySubTitle')
        .name('Secondary Sub Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    benefitsOfJoining
        .createField('secondaryDescription')
        .name('Secondary Description')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    const benefit: ContentType = migration.createContentType('benefit').name('Benefit').description('Benefit item');

    benefit
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    benefit
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    benefit
        .createField('icon')
        .name('Icon')
        .type('Link')
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 30,
                        max: 45,
                    },
                    height: {
                        min: 29,
                        max: 35,
                    },
                },
                message: 'Image must be 30-45px wide and  29-35px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    const page: ContentType = migration.editContentType('page');

    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'benefitsOfJoining',
                    'accountHeader',
                    'checkoutHeader',
                    'infoBanner',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNotFound',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'myDealsEmpty',
                    'numberedListSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
