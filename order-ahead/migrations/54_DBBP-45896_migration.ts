import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction: MigrationFunction = function (migration: Migration): void {
    const configuration: ContentType = migration.editContentType('configuration');

    configuration.createField('isMyTeamsEnabled', {
        name: 'isMyTeamsEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': false,
        },
    });
};

module.exports = migrationFunction;
