module.exports = (migration) => {
    const mainBanner = migration.editContentType('mainBanner');

    mainBanner.deleteField('bottomText');

    mainBanner.createField('bottomText')
        .name('Bottom Text')
        .type('RichText')
        .localized(false)
        .required(true)
        .validations([
            {
                enabledNodeTypes: [
                    'heading-1',
                    'heading-2',
                    'heading-3',
                    'heading-4',
                    'heading-5',
                    'heading-6',
                    'ordered-list',
                    'unordered-list',
                    'hr',
                    'blockquote',
                    'hyperlink',
                    'entry-hyperlink',
                ],

                message:
                    'Only heading 1, heading 2, heading 3, heading 4, heading 5, heading 6, ordered list, unordered list, horizontal rule, quote, link to Url, and link to entry nodes are allowed',
            },
            {
                nodes: {
                    'entry-hyperlink': [
                        {
                            linkContentType: [
                                'documentLink',
                                'externalLink',
                                'menuCategoryLink',
                                'pageLink',
                                'phoneNumberLink',
                                'product',
                            ],

                            message: null,
                        },
                    ],
                },
            },
        ])
        .disabled(false)
        .omitted(false);
};
