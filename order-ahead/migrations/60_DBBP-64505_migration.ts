import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const promoBlockCard = migration.editContentType('promoBlockCard');

    promoBlockCard
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const locationPromoBanner = migration.editContentType('locationPromoBanner');

    locationPromoBanner
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const eventCard = migration.editContentType('eventCard');

    eventCard
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
} as MigrationFunction;

module.exports = migrationFunction;
