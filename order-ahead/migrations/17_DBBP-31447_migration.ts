import Migration from 'contentful-migration';

const migrationFunction = (migration: Migration): void => {
    const topPicks = migration.editContentType('topPicks');

    topPicks
        .createField('leftSideImage')
        .name('Left Side Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 257,
                        max: 257,
                    },

                    height: {
                        min: 195,
                        max: 195,
                    },
                },

                message: 'Image must be 257px wide and 195px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
};

module.exports = migrationFunction;
