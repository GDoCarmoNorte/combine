import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const caloriesLegalInfo: ContentType = migration
        .createContentType('caloriesLegal')
        .name('Calories Legal')
        .description('');
    caloriesLegalInfo.createField('message').name('message').required(true).type('RichText');
} as MigrationFunction;

module.exports = migrationFunction;
