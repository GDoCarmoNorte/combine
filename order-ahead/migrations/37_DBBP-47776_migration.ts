import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const tapListMenuCategory = migration
        .createContentType('tapListMenuCategory')
        .name('Tap List Menu Category')
        .description('')
        .displayField('categoryName');

    tapListMenuCategory
        .createField('categoryName')
        .name('Category Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    tapListMenuCategory
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    tapListMenuCategory
        .createField('sections')
        .name('Sections')
        .type('Array')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: [
                        'infoBanner',
                        'accountHeader',
                        'benefitsOfJoining',
                        'checkoutHeader',
                        'descriptionSection',
                        'emailSignup',
                        'errorBanner',
                        'frequentlyAskedQuestions',
                        'giftCardBalanceSection',
                        'imageBlockSection',
                        'imageAsset',
                        'infoBlockSection',
                        'locationFailed',
                        'locationNewsSection',
                        'locationNotFound',
                        'localTapList',
                        'notificationBanner',
                        'mainBanner',
                        'mainProductBanner',
                        'markup',
                        'menuCategorySection',
                        'miniBanners',
                        'myDealsEmpty',
                        'numberedListSection',
                        'nearbyLocationsSection',
                        'padding',
                        'pickupInstructions',
                        'promoBlockSection',
                        'secondaryBanner',
                        'singleImageWithOverlay',
                        'textBlockSection',
                        'topPicks',
                        'videoBlock',
                    ],
                },
            ],

            linkType: 'Entry',
        });

    tapListMenuCategory
        .createField('metaDescription')
        .name('Meta Description')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    tapListMenuCategory
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['menuCategoryLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const menuCategorySection = migration.editContentType('menuCategorySection');
    menuCategorySection.editField('categories').items({
        type: 'Link',
        validations: [
            {
                linkContentType: ['menuCategory', 'tapListMenuCategory'],
            },
        ],
        linkType: 'Entry',
    });

    const menu = migration.editContentType('menu');
    menu.editField('categories').items({
        type: 'Link',
        validations: [
            {
                linkContentType: ['menuCategory', 'tapListMenuCategory'],
            },
        ],
        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
