import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const brandTheme = migration.editContentType('brandTheme');

    // remove old primary and secondary fields
    brandTheme.deleteField('primary');
    brandTheme.deleteField('primaryVariant');
    brandTheme.deleteField('secondary');
    brandTheme.deleteField('secondaryVariant');

    // --------------------------

    // 2 primary colors with variants
    brandTheme
        .createField('primary1')
        .name('Primary 1')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('primary1variant')
        .name('primary 1 Variant')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    brandTheme
        .createField('primary2')
        .name('Primary 2')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('primary2variant')
        .name('Primary 2 Variant')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    // --------------------------

    // 6 secondary colors
    brandTheme
        .createField('secondary1')
        .name('Secondary 1')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('secondary2')
        .name('Secondary 2')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('secondary3')
        .name('Secondary 3')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('secondary4')
        .name('Secondary 4')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('secondary5')
        .name('Secondary 5')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('secondary6')
        .name('Secondary 6')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    // --------------------------

    // 2 utility colors
    brandTheme
        .createField('utilitySuccess')
        .name('Utility Success')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    brandTheme
        .createField('utilityError')
        .name('Utility Error')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
} as MigrationFunction;

module.exports = migrationFunction;
