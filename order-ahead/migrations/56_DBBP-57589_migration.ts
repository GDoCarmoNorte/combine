import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction: MigrationFunction = function (migration: Migration): void {
    const configuration: ContentType = migration.editContentType('configuration');

    configuration.deleteField('isOAEnabled');
    configuration.deleteField('isDeliveryEnabled');
    configuration.deleteField('isApplePayEnabled');
    configuration.deleteField('isGooglePayEnabled');
    configuration.deleteField('isTippingEnabled');
    configuration.deleteField('isPayAtStoredEnabled');
    configuration.deleteField('isMyTeamsEnabled');

    configuration.editField('configurationRefreshFrequency', {
        type: 'Integer',
        name: 'Configuration Refresh Frequency',
    });

    configuration.editField('personalizationRefreshFrequency', {
        type: 'Integer',
        name: 'Personalization Refresh Frequency',
    });

    configuration.createField('disableOA', { name: 'Disable Order Ahead', type: 'Boolean' });
    configuration.changeFieldControl('disableOA', 'builtin', 'radio', {
        helpText:
            '"YES" will disable the feature | "NO" or no selection will have NO effect; instead value from configuration files will be used.',
    });

    configuration.createField('disableDelivery', { name: 'Disable Delivery', type: 'Boolean' });
    configuration.changeFieldControl('disableDelivery', 'builtin', 'radio', {
        helpText:
            '"YES" will disable the feature | "NO" or no selection will have NO effect; instead value from configuration files will be used.',
    });

    configuration.createField('disableApplePay', { name: 'Disable Apple Pay', type: 'Boolean' });
    configuration.changeFieldControl('disableApplePay', 'builtin', 'radio', {
        helpText:
            '"YES" will disable the feature | "NO" or no selection will have NO effect; instead value from configuration files will be used.',
    });

    configuration.createField('disableGooglePay', { name: 'Disable Google Pay', type: 'Boolean' });
    configuration.changeFieldControl('disableGooglePay', 'builtin', 'radio', {
        helpText:
            '"YES" will disable the feature | "NO" or no selection will have NO effect; instead value from configuration files will be used.',
    });

    configuration.createField('disableTips', { name: 'Disable Tips', type: 'Boolean' });
    configuration.changeFieldControl('disableTips', 'builtin', 'radio', {
        helpText:
            '"YES" will disable the feature | "NO" or no selection will have NO effect; instead value from configuration files will be used.',
    });

    configuration.createField('disablePayAtStore', { name: 'Disable Pay At Store', type: 'Boolean' });
    configuration.changeFieldControl('disablePayAtStore', 'builtin', 'radio', {
        helpText:
            '"YES" will disable the feature | "NO" or no selection will have NO effect; instead value from configuration files will be used.',
    });
};

module.exports = migrationFunction;
