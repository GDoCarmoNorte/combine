import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const legalMessage = migration
        .createContentType('legalMessage')
        .name('Legal Message')
        .description('')
        .displayField('name');
    legalMessage
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    legalMessage
        .createField('termsApplyTitle')
        .name('Terms Apply Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    legalMessage
        .createField('viewDetailsTitle')
        .name('View Details Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    legalMessage
        .createField('message')
        .name('Message')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    const mainBanner = migration.editContentType('mainBanner');
    mainBanner.deleteField('legalMessage');

    mainBanner
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const secondaryBanner = migration.editContentType('secondaryBanner');
    secondaryBanner.deleteField('legalMessage');

    secondaryBanner
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const infoBlockCard: ContentType = migration.editContentType('infoBlockCard');

    infoBlockCard
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const imageBlockCard: ContentType = migration.editContentType('imageBlockCard');

    imageBlockCard
        .createField('legalMessage')
        .name('Legal Message')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['legalMessage'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
} as MigrationFunction;

module.exports = migrationFunction;
