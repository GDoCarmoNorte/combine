module.exports = (migration) => {
    const padding = migration.createContentType('padding').name('Padding').description('').displayField('name');

    padding
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    padding
        .createField('height')
        .name('Height')
        .type('Integer')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    padding
        .createField('backgroundColor')
        .name('Background Color')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    padding
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
};
