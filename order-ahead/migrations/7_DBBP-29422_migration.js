module.exports = (migration) => {
    const checkoutHeader = migration.createContentType('checkoutHeader')
            .name('Checkout Header')
            .description('Page Header for Checkout Page');

        checkoutHeader.createField('backgroundColor')
            .name('Background Color')
            .type('Link')
            .localized(false)
            .required(true)
            .validations([
                {
                    linkContentType: ['color'],
                },
            ])
            .disabled(false)
            .omitted(false)
            .linkType('Entry');

        checkoutHeader.createField('backgroundImage')
            .name('Background Image')
            .type('Link')
            .localized(false)
            .required(false)
            .validations([
                {
                    assetImageDimensions: {
                        width: {
                            min: 1440,
                            max: 1440,
                        },

                        height: {
                            min: 530,
                            max: 530,
                        },
                    },

                    message: 'Image must be 1440px wide and 530px tall.',
                },
            ])
            .disabled(false)
            .omitted(false)
            .linkType('Asset');
    
    const page = migration.editContentType('page');

    page.editField('section')
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: [
                        'checkoutHeader',
                        'infoBanner',
                        'emailSignup',
                        'errorBanner',
                        'frequentlyAskedQuestions',
                        'giftCardBalanceSection',
                        'imageBlockSection',
                        'infoBlockSection',
                        'locationFailed',
                        'locationNotFound',
                        'mainBanner',
                        'mainProductBanner',
                        'markup',
                        'menuCategorySection',
                        'myDealsEmpty',
                        'numberedListSection',
                        'padding',
                        'pickupInstructions',
                        'promoBlockSection',
                        'secondaryBanner',
                        'singleImageWithOverlay',
                        'textBlockSection',
                        'topPicks',
                        'videoBlock',
                    ],
                },
            ],

            linkType: 'Entry',
        });
};
