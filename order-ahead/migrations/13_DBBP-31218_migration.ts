import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const userAccountMenuItemType: ContentType = migration
        .createContentType('userAccountMenuItemType')
        .name('User Account Menu Item Type')
        .description('')
        .displayField('type');

    userAccountMenuItemType
        .createField('type')
        .name('Type')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([{ unique: true }])
        .disabled(false)
        .omitted(false);

    const userAccountMenuItem: ContentType = migration
        .createContentType('userAccountMenuItem')
        .name('User Account Menu Item')
        .description('')
        .displayField('name');

    userAccountMenuItem
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    userAccountMenuItem
        .createField('type')
        .name('Type')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([{ linkContentType: ['userAccountMenuItemType'] }])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    userAccountMenuItem
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([{ linkContentType: ['pageLink'] }])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const userAccountMenu: ContentType = migration
        .createContentType('userAccountMenu')
        .name('User Account Menu')
        .description('')
        .displayField('name');

    userAccountMenu
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    userAccountMenu
        .createField('links')
        .name('Links')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['userAccountMenuItem'],
                },
            ],

            linkType: 'Entry',
        });
} as MigrationFunction;

module.exports = migrationFunction;
