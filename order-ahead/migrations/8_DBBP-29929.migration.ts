import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const productNutritionLinkType: ContentType = migration
        .createContentType('productNutritionLink')
        .name('Product Nutrition Link')
        .description('')
        .displayField('text');

    productNutritionLinkType
        .createField('text')
        .name('Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    productNutritionLinkType
        .createField('link')
        .name('Link')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([{ linkContentType: ['documentLink', 'externalLink', 'pageLink'] }])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
} as MigrationFunction;

module.exports = migrationFunction;
