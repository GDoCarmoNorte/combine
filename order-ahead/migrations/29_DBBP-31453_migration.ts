import Migration from 'contentful-migration';

const migrationFunction = (migration: Migration): void => {
    const singleImageWithOverlay = migration.editContentType('singleImageWithOverlay');

    singleImageWithOverlay.editField('header').required(false);

    singleImageWithOverlay
        .createField('secondaryButtonCta')
        .name('Secondary Button CTA')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['externalLink'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    singleImageWithOverlay.moveField('secondaryButtonCta').afterField('buttonCta');
};

module.exports = migrationFunction;
