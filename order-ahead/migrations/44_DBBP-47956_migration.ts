import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction: MigrationFunction = function (migration: Migration): void {
    migration.deleteContentType('cardBalance');
    const configuration: ContentType = migration.createContentType('configuration', {
        name: 'Configuration',
        displayField: 'name',
    });

    configuration.createField('name', {
        name: 'Name',
        type: 'Symbol',
        required: true,
        defaultValue: {
            'en-US': 'Global',
        },
    });

    configuration.createField('isOAEnabled', {
        name: 'isOAEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': true,
        },
    });

    configuration.createField('isDeliveryEnabled', {
        name: 'isDeliveryEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': true,
        },
    });

    configuration.createField('isApplePayEnabled', {
        name: 'isApplePayEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': true,
        },
    });

    configuration.createField('isGooglePayEnabled', {
        name: 'isGooglePayEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': true,
        },
    });

    configuration.createField('isTippingEnabled', {
        name: 'isTippingEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': true,
        },
    });

    configuration.createField('configurationRefreshFrequency', {
        name: 'configurationRefreshFrequency',
        type: 'Integer',
        required: true,
        defaultValue: {
            'en-US': 30,
        },
    });

    configuration.createField('personalizationRefreshFrequency', {
        name: 'personalizationRefreshFrequency',
        type: 'Integer',
        required: true,
        defaultValue: {
            'en-US': 30,
        },
    });
};

module.exports = migrationFunction;
