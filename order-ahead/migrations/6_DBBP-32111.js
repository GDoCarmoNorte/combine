module.exports = (migration) => {
    const deal = migration.createContentType('deal').name('Deal').description('').displayField('name');

    deal.createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    deal.createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },

                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    deal.createField('offerId')
        .name('Offer Id')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
};
