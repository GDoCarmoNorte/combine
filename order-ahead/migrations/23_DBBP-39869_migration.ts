import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'accountHeader',
                    'benefitsOfJoining',
                    'checkoutHeader',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNewsSection',
                    'locationNotFound',
                    'notificationBanner',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'miniBanners',
                    'myDealsEmpty',
                    'numberedListSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
