import Migration, { MigrationFunction } from 'contentful-migration';
import { logInit } from '../lib/cliLog';

const migrationFunction = function (migration: Migration): void {
    const secondaryBanner = migration.editContentType('secondaryBanner');

    secondaryBanner.deleteField('mainTextColor');
    secondaryBanner.deleteField('backgroundColor');
    secondaryBanner.deleteField('descriptionTitleColor');

    secondaryBanner
        .createField('mainTextColor')
        .name('Main Text Color')
        .type('Link')
        .validations([{ linkContentType: ['color'] }])
        .linkType('Entry');

    secondaryBanner
        .createField('titleColor')
        .name('Title Color')
        .type('Link')
        .validations([{ linkContentType: ['color'] }])
        .linkType('Entry');

    secondaryBanner
        .createField('descriptionColor')
        .name('Description Color')
        .type('Link')
        .validations([{ linkContentType: ['color'] }])
        .linkType('Entry');

    secondaryBanner
        .createField('backgroundColor')
        .name('Background Color')
        .type('Link')
        .validations([{ linkContentType: ['color'] }])
        .linkType('Entry');

    secondaryBanner
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
            {
                assetImageDimensions: {
                    width: {
                        min: 4000,
                        max: 4000,
                    },

                    height: {
                        min: 3000,
                        max: 3000,
                    },
                },
                message: 'Image must be 4000px wide and 3000px tall.',
            },
        ])
        .linkType('Asset');

    secondaryBanner.moveField('backgroundColor').beforeField('backgroundImage');

    const { logWarning } = logInit('Secondary Banner Migration');
    logWarning(
        `
            PLEASE PAY ATTENTION IF RUN THIS FOR UAT-STG OR MASTER ENVS.
            Main Text Color, Background Color, Description Title Color fields will be removed and re-created as links to Color. Please check secondary banner's entries and specify colors manually.
            To the date this script was written all these fields were empty in uat-stg and master envs. So most probably you don't need to change anything but please check.
            `
    );
} as MigrationFunction;

module.exports = migrationFunction;
