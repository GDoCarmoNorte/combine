import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const infoBlockSection: ContentType = migration.editContentType('infoBlockSection');

    infoBlockSection
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
} as MigrationFunction;

module.exports = migrationFunction;
