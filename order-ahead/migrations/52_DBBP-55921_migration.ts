import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const menuCategory = migration.editContentType('menuCategory');

    menuCategory
        .createField('categoryDescription')
        .name('Category Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    const tapListMenuCategory = migration.editContentType('tapListMenuCategory');

    tapListMenuCategory
        .createField('categoryDescription')
        .name('Category Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
} as MigrationFunction;

module.exports = migrationFunction;
