import { MigrationFunction } from 'contentful-migration';

const migrationFunction = async function (migration) {
    const sodiumWarning = migration
        .createContentType('sodiumWarning')
        .name('Sodium warning')
        .description('')
        .displayField('name');
    sodiumWarning
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    sodiumWarning
        .createField('type')
        .name('Type')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                in: ['NYC', 'Philadelphia'],
            },
        ])
        .disabled(false)
        .omitted(false);

    sodiumWarning
        .createField('message')
        .name('Message')
        .type('RichText')
        .localized(false)
        .required(true)
        .validations([
            {
                enabledMarks: [],
                message: 'Marks are not allowed',
            },
            {
                enabledNodeTypes: ['embedded-entry-inline'],
                message: 'Only inline entry nodes are allowed',
            },
            {
                nodes: {
                    'embedded-entry-inline': [
                        {
                            linkContentType: ['sodiumWarningIcon'],
                            message: null,
                        },
                    ],
                },
            },
        ])
        .disabled(false)
        .omitted(false);

    sodiumWarning
        .createField('icon')
        .name('Icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkContentType: ['sodiumWarningIcon'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    sodiumWarning.changeFieldControl('name', 'builtin', 'singleLine', {});
    sodiumWarning.changeFieldControl('message', 'builtin', 'richTextEditor', {});
    sodiumWarning.changeFieldControl('icon', 'builtin', 'entryLinkEditor', {});
    const sodiumWarningIcon = migration
        .createContentType('sodiumWarningIcon')
        .name('Sodium warning icon')
        .description('')
        .displayField('name');
    sodiumWarningIcon
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    sodiumWarningIcon
        .createField('icon')
        .name('Icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    sodiumWarningIcon.changeFieldControl('name', 'builtin', 'singleLine', {});
    sodiumWarningIcon.changeFieldControl('icon', 'builtin', 'assetLinkEditor', {});
} as MigrationFunction;

module.exports = migrationFunction;
