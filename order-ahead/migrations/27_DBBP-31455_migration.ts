import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const promoBlockCard = migration.editContentType('promoBlockCard');

    promoBlockCard
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    promoBlockCard
        .createField('backgroundColor')
        .name('Background Color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
} as MigrationFunction;

module.exports = migrationFunction;
