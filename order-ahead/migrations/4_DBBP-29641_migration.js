module.exports = (migration) => {
    const myDealsEmpty = migration
        .createContentType('myDealsEmpty')
        .name('My Deals Empty')
        .description('')
        .displayField('title');
    myDealsEmpty
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    myDealsEmpty
        .createField('topImage')
        .name('Top Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    myDealsEmpty
        .createField('mainText')
        .name('Main Text')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    myDealsEmpty
        .createField('ctaText')
        .name('CTAText')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    const page = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',
        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNotFound',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'myDealsEmpty',
                    'numberedListSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });

    const pageLink = migration.editContentType('pageLink');
    pageLink.editField('nameInUrl').validations([
        {
            unique: true,
        },
        {
            regexp: {
                pattern: '^[a-z0-9_-]+(\\/*[a-z0-9_-])*$',
                flags: null,
            },

            message: 'URL can only contain letters, numbers, dashes, and underscores',
        },
    ]);
};
