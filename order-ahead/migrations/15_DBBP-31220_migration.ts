import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const accountHeader: ContentType = migration
        .createContentType('accountHeader')
        .name('Account Header')
        .description('Account header background|color');
    accountHeader
        .createField('backgroundColor')
        .name('Background color')
        .type('Link')
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .linkType('Entry');

    accountHeader
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .required(false)
        .validations([
            {
                assetImageDimensions: {
                    width: {
                        min: 1440,
                        max: 1440,
                    },
                    height: {
                        min: 180,
                        max: 180,
                    },
                },
                message: 'Image must be 1440px wide and 180px tall.',
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    const page: ContentType = migration.editContentType('page');

    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'accountHeader',
                    'checkoutHeader',
                    'infoBanner',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNotFound',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'myDealsEmpty',
                    'numberedListSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
