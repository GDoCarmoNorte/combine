import Migration, { MigrationFunction, ContentType } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const eventCard = migration
        .createContentType('eventCard')
        .name('Event Card')
        .description('')
        .displayField('eventName');

    eventCard
        .createField('backgroundColor')
        .name('Background color')
        .type('Link')
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .linkType('Entry');

    eventCard
        .createField('eventIcon')
        .name('Event Icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    eventCard
        .createField('eventName')
        .name('Event Name')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    eventCard
        .createField('eventDescription')
        .name('Event Description')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    eventCard
        .createField('eventLinkText')
        .name('Event Link Text')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    eventCard
        .createField('eventLinkHref')
        .name('Event Link Href')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: [
                    'documentLink',
                    'externalLink',
                    'menuCategoryLink',
                    'pageLink',
                    'phoneNumberLink',
                    'product',
                ],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const goingAtTheBar = migration
        .createContentType('goingAtTheBar')
        .name('Going At The Bar')
        .description('')
        .displayField('title');

    goingAtTheBar
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    goingAtTheBar
        .createField('subtitle')
        .name('Subtitle')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    goingAtTheBar
        .createField('backgroundColor')
        .name('Background color')
        .type('Link')
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .linkType('Entry');

    goingAtTheBar
        .createField('eventCards')
        .name('Event Cards')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['eventCard'],
                },
            ],

            linkType: 'Entry',
        });

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'accountHeader',
                    'benefitsOfJoining',
                    'checkoutHeader',
                    'descriptionSection',
                    'dineTime',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'goingAtTheBar',
                    'imageBlockSection',
                    'imageAsset',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNewsSection',
                    'locationNotFound',
                    'locationPromoBanner',
                    'localTapList',
                    'notificationBanner',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'miniBanners',
                    'myDealsEmpty',
                    'numberedListSection',
                    'nearbyLocationsSection',
                    'openTable',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
