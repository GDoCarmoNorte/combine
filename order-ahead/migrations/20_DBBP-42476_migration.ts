import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const pageLink = migration.editContentType('pageLink');
    pageLink.editField('nameInUrl').validations([
        {
            unique: true,
        },
        {
            regexp: {
                pattern: '^[a-z0-9_-]+(\\/*[a-z0-9?=_-])*$',
                flags: null,
            },

            message: 'URL can only contain letters, numbers, dashes, underscores and question',
        },
    ]);
} as MigrationFunction;

module.exports = migrationFunction;
