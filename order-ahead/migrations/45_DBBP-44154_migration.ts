import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const singleImageWithOverlay: ContentType = migration.editContentType('singleImageWithOverlay');
    singleImageWithOverlay
        .createField('showBlazingRewardsButton')
        .name('Show Blazing Rewards button')
        .type('Boolean')
        .localized(false)
        .required(false)
        .disabled(false);
} as MigrationFunction;

module.exports = migrationFunction;
