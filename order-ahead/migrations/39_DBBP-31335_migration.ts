import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const locationMapping = migration
        .createContentType('locationMapping')
        .name('Location Mapping')
        .description('Mapping between IDP location IDs and external system location IDs\n')
        .displayField('idpLocationId');
    locationMapping
        .createField('idpLocationId')
        .name('IDP Location ID')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([
            {
                regexp: {
                    pattern: '^[0-9]*$',
                    flags: null,
                },

                message: 'Only numbers allowed!',
            },
        ])
        .disabled(false)
        .omitted(false);
    locationMapping
        .createField('externalLocationId')
        .name('External Location ID')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);

    const dineTime = migration
        .createContentType('dineTime')
        .name('Dine Time')
        .description('Section to configure DineTime widget\n')
        .displayField('name');
    dineTime
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    dineTime
        .createField('locationsMapping')
        .name('Location Mapping')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['locationMapping'],
                },
            ],

            linkType: 'Entry',
        });

    const openTable = migration
        .createContentType('openTable')
        .name('Open Table')
        .description('Section to configure DineTime widget\n')
        .displayField('name');
    openTable
        .createField('name')
        .name('Name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    openTable
        .createField('locationsMapping')
        .name('Location Mapping')
        .type('Array')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .items({
            type: 'Link',

            validations: [
                {
                    linkContentType: ['locationMapping'],
                },
            ],

            linkType: 'Entry',
        });

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'accountHeader',
                    'benefitsOfJoining',
                    'checkoutHeader',
                    'descriptionSection',
                    'dineTime',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'imageAsset',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNewsSection',
                    'locationNotFound',
                    'locationPromoBanner',
                    'localTapList',
                    'notificationBanner',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'miniBanners',
                    'myDealsEmpty',
                    'numberedListSection',
                    'nearbyLocationsSection',
                    'openTable',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
