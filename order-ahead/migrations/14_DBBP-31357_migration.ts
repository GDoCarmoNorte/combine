import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const emptyShoppingBag: ContentType = migration
        .createContentType('emptyShoppingBag')
        .name('Empty Shopping Bag')
        .description('')
        .displayField('header');

    emptyShoppingBag
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    emptyShoppingBag
        .createField('header')
        .name('Header')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([{ size: { max: 40 } }])
        .disabled(false)
        .omitted(false);

    emptyShoppingBag
        .createField('description')
        .name('Description')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([{ size: { max: 120 } }])
        .disabled(false)
        .omitted(false);
} as MigrationFunction;

module.exports = migrationFunction;
