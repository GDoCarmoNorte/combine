import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const FAQ = migration.editContentType('frequentlyAskedQuestions');

    FAQ.editField('title').required(false);
} as MigrationFunction;

module.exports = migrationFunction;
