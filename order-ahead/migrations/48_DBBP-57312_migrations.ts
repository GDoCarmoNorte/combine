import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const configuration: ContentType = migration.editContentType('configuration');

    configuration.createField('isPayAtStoredEnabled', {
        name: 'isPayAtStoredEnabled',
        type: 'Boolean',
        required: true,
        defaultValue: {
            'en-US': false,
        },
    });
} as MigrationFunction;

module.exports = migrationFunction;
