import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const product: ContentType = migration.editContentType('product');
    product
        .createField('productIdList')
        .name('Product Id List')
        .type('Array')
        .items({ type: 'Symbol' })
        .localized(false)
        .required(true)
        .disabled(false)
        .omitted(false);

    migration.transformEntries({
        contentType: 'product',
        from: ['productId'],
        to: ['productIdList'],
        transformEntryForLocale: (from, locale) => {
            return {
                productIdList: from?.productId ? [from.productId[locale]] : [],
            };
        },
    });
} as MigrationFunction;

module.exports = migrationFunction;
