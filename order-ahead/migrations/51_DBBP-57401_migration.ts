import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const mainBanner: ContentType = migration.editContentType('mainBanner');
    mainBanner
        .createField('legalMessage')
        .name('Legal Message')
        .type('Text')
        .localized(false)
        .required(false)
        .disabled(false)
        .omitted(false);

    const secondaryBanner: ContentType = migration.editContentType('secondaryBanner');
    secondaryBanner
        .createField('legalMessage')
        .name('Legal Message')
        .type('Text')
        .localized(false)
        .required(false)
        .disabled(false)
        .omitted(false);
} as MigrationFunction;

module.exports = migrationFunction;
