import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const mainBanner = migration.editContentType('mainBanner');

    mainBanner.editField('bottomText').required(false);

    mainBanner.editField('mainLinkText').required(false);

    mainBanner.editField('mainLinkHref').required(false);

    mainBanner
        .createField('centeredContent')
        .name('Centered Content')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    mainBanner
        .createField('overlayForBackgroundImage')
        .name('Overlay For Background Image')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
} as MigrationFunction;

module.exports = migrationFunction;
