import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const imageBlockCard: ContentType = migration.editContentType('imageBlockCard');

    imageBlockCard
        .createField('imageBlockCtaType')
        .name('Image Block CTA Type')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([
            {
                in: ['primary', 'secondary'],
            },
        ])
        .disabled(false)
        .omitted(false);
} as MigrationFunction;

module.exports = migrationFunction;
