import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const menuCategory: ContentType = migration.editContentType('menuCategory');
    menuCategory.editField('sections').items({
        type: 'Link',
        validations: [
            {
                linkContentType: ['textBlockSection', 'action'],
            },
        ],
        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
