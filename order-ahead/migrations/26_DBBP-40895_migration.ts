import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const infoBlockCard: ContentType = migration.editContentType('infoBlockCard');
    infoBlockCard
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
} as MigrationFunction;

module.exports = migrationFunction;
