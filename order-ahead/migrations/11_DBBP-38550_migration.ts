import Migration, { MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const footerSocialMediaLinks = migration.editContentType('footerSocialMediaLinks');
    footerSocialMediaLinks
        .createField('mobileTitle')
        .name('Mobile Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
} as MigrationFunction;

module.exports = migrationFunction;
