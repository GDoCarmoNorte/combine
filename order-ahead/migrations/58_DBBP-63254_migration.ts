import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const category: ContentType = migration.editContentType('menuCategory');
    category
        .createField('categoryIdList')
        .name('Category Id List')
        .type('Array')
        .items({ type: 'Symbol' })
        .localized(false)
        .required(true)
        .disabled(false)
        .omitted(false);

    migration.transformEntries({
        contentType: 'menuCategory',
        from: ['categoryId'],
        to: ['categoryIdList'],
        transformEntryForLocale: (from, locale) => {
            return {
                categoryIdList: from?.categoryId ? [from.categoryId[locale]] : [],
            };
        },
    });
} as MigrationFunction;

module.exports = migrationFunction;
