import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const banner: ContentType = migration.editContentType('secondaryBanner');

    banner
        .createField('mobileBackgroundImage')
        .name('Mobile Background Image')
        .type('Link')
        .validations([
            {
                linkMimetypeGroup: ['image'],
            },
            {
                assetImageDimensions: {
                    width: {
                        min: 2000,
                        max: 2000,
                    },

                    height: {
                        min: 2400,
                        max: 2400,
                    },
                },
                message: 'Image must be 2000px wide and 2400px tall.',
            },
        ])
        .linkType('Asset');

    banner.editField('backgroundImage').validations([
        {
            linkMimetypeGroup: ['image'],
        },
        {
            assetImageDimensions: {
                width: {
                    min: 4000,
                    max: 4000,
                },

                height: {
                    min: 1650,
                    max: 1650,
                },
            },
            message: 'Image must be 4000px wide and 1650px tall.',
        },
    ]);
} as MigrationFunction;

module.exports = migrationFunction;
