import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const locationPromoBanner = migration
        .createContentType('locationPromoBanner')
        .name('Location Promo banner')
        .description('settings for Location promo banner\n')
        .displayField('title');
    locationPromoBanner
        .createField('title')
        .name('Title')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    locationPromoBanner
        .createField('description')
        .name('Description')
        .type('Text')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    locationPromoBanner
        .createField('icon')
        .name('icon')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    locationPromoBanner
        .createField('backgroundImage')
        .name('Background Image')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    locationPromoBanner
        .createField('backgroundColor')
        .name('Background color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    locationPromoBanner
        .createField('titleColor')
        .name('Title color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    locationPromoBanner
        .createField('descriptionColor')
        .name('Description color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'accountHeader',
                    'benefitsOfJoining',
                    'checkoutHeader',
                    'descriptionSection',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'imageAsset',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNewsSection',
                    'locationNotFound',
                    'locationPromoBanner',
                    'notificationBanner',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'miniBanners',
                    'myDealsEmpty',
                    'numberedListSection',
                    'nearbyLocationsSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
