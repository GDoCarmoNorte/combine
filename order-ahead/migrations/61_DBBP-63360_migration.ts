import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const infoBlockSection: ContentType = migration.editContentType('infoBlockSection');

    infoBlockSection.editField('cards').validations([
        {
            size: {
                min: 1,
                max: 6,
            },
        },
    ]);
} as MigrationFunction;

module.exports = migrationFunction;
