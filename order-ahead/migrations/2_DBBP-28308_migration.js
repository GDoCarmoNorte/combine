module.exports = (migration) => {
    const mainBanner = migration.editContentType('mainBanner');

    mainBanner
        .createField('backgroundForMobile')
        .name('Background for mobile')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
};
