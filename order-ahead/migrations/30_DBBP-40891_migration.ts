import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const imageAsset = migration
        .createContentType('imageAsset')
        .name('Image Asset')
        .description('settings for image to be injecte on page\n')
        .displayField('name');
    imageAsset
        .createField('name')
        .name('name')
        .type('Symbol')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    imageAsset
        .createField('type')
        .name('type')
        .type('Symbol')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);
    imageAsset
        .createField('image')
        .name('Image')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');
    imageAsset
        .createField('imageLeftAligned')
        .name('Image Left Aligned')
        .type('Boolean')
        .localized(false)
        .required(false)
        .validations([])
        .disabled(false)
        .omitted(false);

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'accountHeader',
                    'benefitsOfJoining',
                    'checkoutHeader',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'imageAsset',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNewsSection',
                    'locationNotFound',
                    'notificationBanner',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'miniBanners',
                    'myDealsEmpty',
                    'numberedListSection',
                    'nearbyLocationsSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
