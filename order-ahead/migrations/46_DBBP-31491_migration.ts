import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const page: ContentType = migration.editContentType('page');
    const menuCategory = migration.editContentType('menuCategory');
    const product = migration.editContentType('product');
    const tapListMenuCategory = migration.editContentType('tapListMenuCategory');

    [page, menuCategory, product, tapListMenuCategory].forEach((contentType) => {
        contentType
            .createField('canonicalUrl')
            .name('Canonical URL')
            .type('Symbol')
            .localized(false)
            .required(false)
            .validations([])
            .disabled(false)
            .omitted(false);
    });
} as MigrationFunction;

module.exports = migrationFunction;
