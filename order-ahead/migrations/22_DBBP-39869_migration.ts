import Migration, { ContentType, MigrationFunction } from 'contentful-migration';

const migrationFunction = function (migration: Migration): void {
    const notificationBanner = migration
        .createContentType('notificationBanner')
        .name('Notification Banner')
        .description('content of notification banner\n')
        .displayField('text');
    notificationBanner
        .createField('text')
        .name('Text')
        .type('Text')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false);
    notificationBanner
        .createField('backgroundColor')
        .name('Background Color')
        .type('Link')
        .localized(false)
        .required(false)
        .validations([
            {
                linkContentType: ['color'],
            },
        ])
        .disabled(false)
        .omitted(false)
        .linkType('Entry');
    notificationBanner
        .createField('icon')
        .name('icon')
        .type('Link')
        .localized(false)
        .required(true)
        .validations([])
        .disabled(false)
        .omitted(false)
        .linkType('Asset');

    const page: ContentType = migration.editContentType('page');
    page.editField('section').items({
        type: 'Link',

        validations: [
            {
                linkContentType: [
                    'infoBanner',
                    'accountHeader',
                    'benefitsOfJoining',
                    'checkoutHeader',
                    'emailSignup',
                    'errorBanner',
                    'frequentlyAskedQuestions',
                    'giftCardBalanceSection',
                    'imageBlockSection',
                    'infoBlockSection',
                    'locationFailed',
                    'locationNotFound',
                    'notificationBanner',
                    'mainBanner',
                    'mainProductBanner',
                    'markup',
                    'menuCategorySection',
                    'miniBanners',
                    'myDealsEmpty',
                    'numberedListSection',
                    'padding',
                    'pickupInstructions',
                    'promoBlockSection',
                    'secondaryBanner',
                    'singleImageWithOverlay',
                    'textBlockSection',
                    'topPicks',
                    'videoBlock',
                ],
            },
        ],

        linkType: 'Entry',
    });
} as MigrationFunction;

module.exports = migrationFunction;
