module.exports = function (migration) {
    const product = migration.editContentType('product');

    product.createField('showProductBadge')
        .type('Boolean')
        .name('Show Product Badge')
        .required(false)
}